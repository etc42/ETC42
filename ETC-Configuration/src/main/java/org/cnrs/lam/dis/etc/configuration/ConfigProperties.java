/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.configuration;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import org.apache.log4j.Logger;

/**
 */
class ConfigProperties implements Config {
    
    private static final Logger logger = Logger.getLogger(ConfigProperties.class);
    
    private static final String EXPERIENCE_LEVEL_DEFAULT = "";
    private static final String IMPORT_URLS_DEFAULT = "https://projets.lam.fr/projects/etc/wiki/DataRepository";
    private static final String OFFICIAL_REPO_URL_DEFAULT = "https://cesam.lam.fr/Apps/ETC/presets";
    private static final String FIXTURES_URLS_DEFAULT = "https://cesam.lam.fr/Apps/ETC/fixtures";
    private static final String MAX_WEB_PAGE_SIZE_DEFAULT = "500000";
    private static final String RESULT_LEVEL_DEFAULT = "FINAL";
    private static final String RESULT_ORDER_FINAL_DEFAULT = "CALCULATED_EXPOSURE_TIME, SIGNAL_TO_NOISE";
    private static final String RESULT_ORDER_INTERMEDIATE_IMPORTANT_DEFAULT = "";
    private static final String RESULT_ORDER_INTERMEDIATE_UNIMPORTANT_DEFAULT = "";
    private static final String RESULT_ORDER_DEBUG_RESULT = "PSF_FWHM_METHOD, SPATIAL_BINNING_METHOD, " +
                "SPECTRAL_BINNING_METHOD, NUMBER_OF_SLICES_METHOD, NUMBER_OF_PIXELS_METHOD, " +
                "SURFACE_BRIGHTNESS_PROFILE_METHOD, NORMALIZATION_FACTOR_METHOD, FILTER_RESPONSE_METHOD, " +
                "GALACTIC_FLUX_METHOD, ZODIACAL_FLUX_METHOD, SOURCE_PIXEL_COVERAGE_METHOD, "
                + "ATMOSPHERIC_TRANSMISSION_METHOD, BACKGROUND_NOISE_METHOD, DELTA_LAMBDA_METHOD, "
                + "SIGNAL_FLUX_METHOD, SIGNAL_METHOD, SKY_FLUX_METHOD, SYSTEM_EFFICIENCY_METHOD, "
                + "TELESCOPE_AREA_METHOD";
    private static final String SPECTRUM_RESOLUTION_DEFAULT = "0.1";
    private static final String CONVOL_KERNEL_PER_SPECTRAL_RESOL_ELEMENT_DEFAULT = "FALSE";
    private static final String CACHE_DISABLED_DEFAULT = "FALSE";
    private static final String PSF_CONVOLUTION_SAMPLING_SIZE_DEFAULT = "64";
    private static final String CENTAL_PIXEL_FLAG_DEFAULT = "TRUE";
    private static final String PROJECTION_CONVOLUTION_FLAG_DEFAULT = "FALSE";
    
    private String etcHome = null;
    private String uiMode = null;
    private String fileSeparator = null;
    private String lineSeparator = null;
    private String pluginDir = null;
    private String tempDir = null;
    private Properties uiProperties = null;
    private Properties calculatorProperties = null;
    private String[] commandLineArguments = null;

    public ConfigProperties() {
    }

    private void readUiFile() {
        logger.info("Loading configuration from ui.properties file...");
        uiProperties = new Properties();
        File file = new File(getEtcHome() + getFileSeparator() + "ui.properties");
        if (file.exists()) {
            try {
                FileReader reader = new FileReader(file);
                uiProperties.load(reader);
                reader.close();
                logger.info("Loading configuration from ui.properties file finished successfully");
            } catch (FileNotFoundException ex) {
                // We did a check if the file exists, so we should never come here
                logger.error("Serious error... Failed to find the ui.properties file, even though it exists", ex);
            } catch (IOException ex) {
                logger.error("Failed to load the ui.properties file. Reason: " + ex.getMessage(), ex);
            }
        } else {
            logger.warn("The file " + file + " doesn't exists. Creating it with default values...");
            createDefaultUiFile();
        }
        logger.info("Loaded configuration from ui.properties file");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream stream = new PrintStream(baos);
        uiProperties.list(stream);
        logger.info(baos.toString());
        stream.close();
    }

    private void createDefaultUiFile() {
        uiProperties = new Properties();
        uiProperties.setProperty("EXPERIENCE_LEVEL", EXPERIENCE_LEVEL_DEFAULT);
        uiProperties.setProperty("IMPORT_URLS", IMPORT_URLS_DEFAULT);
        uiProperties.setProperty("OFFICIAL_REPO_URL", OFFICIAL_REPO_URL_DEFAULT);
        uiProperties.setProperty("FIXTURES_URLS", FIXTURES_URLS_DEFAULT);
        uiProperties.setProperty("MAX_WEB_PAGE_SIZE", MAX_WEB_PAGE_SIZE_DEFAULT);
        uiProperties.setProperty("RESULT_LEVEL", RESULT_LEVEL_DEFAULT);
        uiProperties.setProperty("RESULT_ORDER_FINAL", RESULT_ORDER_FINAL_DEFAULT);
        uiProperties.setProperty("RESULT_ORDER_INTERMEDIATE_IMPORTANT", RESULT_ORDER_INTERMEDIATE_IMPORTANT_DEFAULT);
        uiProperties.setProperty("RESULT_ORDER_INTERMEDIATE_UNIMPORTANT", RESULT_ORDER_INTERMEDIATE_UNIMPORTANT_DEFAULT);
        uiProperties.setProperty("RESULT_ORDER_DEBUG", RESULT_ORDER_DEBUG_RESULT);
        saveUiProperties();
    }

    private void saveUiProperties() {
        try {
            uiProperties.store(new FileWriter(getEtcHome() + getFileSeparator() + "ui.properties"), null);
        } catch (IOException ex) {
            logger.error("Failed to create the ui.properties file. Reason: " + ex.getMessage(), ex);
        }
    }

    private void readCalculatorFile() {
        logger.info("Loading configuration from calculator.properties file...");
        calculatorProperties = new Properties();
        File file = new File(getEtcHome() + getFileSeparator() + "calculator.properties");
        if (file.exists()) {
            try {
                FileReader reader = new FileReader(file);
                calculatorProperties.load(reader);
                reader.close();
                logger.info("Loading configuration from calculator.properties file finished successfully");
            } catch (FileNotFoundException ex) {
                // We did a check if the file exists, so we should never come here
                logger.error("Serious error... Failed to find the calculator.properties file, even though it exists", ex);
            } catch (IOException ex) {
                logger.error("Failed to load the calculator.properties file. Reason: " + ex.getMessage(), ex);
            }
        } else {
            logger.warn("The file " + file + " doesn't exists. Creating it with default values...");
            createDefaultCalculatorFile();
        }
        logger.info("Loaded configuration from calculator.properties file");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream stream = new PrintStream(baos);
        calculatorProperties.list(stream);
        logger.info(baos.toString());
        stream.close();
    }

    private void createDefaultCalculatorFile() {
        calculatorProperties = new Properties();
        calculatorProperties.setProperty("SPECTRUM_RESOLUTION", SPECTRUM_RESOLUTION_DEFAULT);
        calculatorProperties.setProperty("CONVOL_KERNEL_PER_SPECTRAL_RESOL_ELEMENT", CONVOL_KERNEL_PER_SPECTRAL_RESOL_ELEMENT_DEFAULT);
        calculatorProperties.setProperty("CACHE_DISABLED", CACHE_DISABLED_DEFAULT);
        calculatorProperties.setProperty("PSF_CONVOLUTION_SAMPLING_SIZE", PSF_CONVOLUTION_SAMPLING_SIZE_DEFAULT);
        calculatorProperties.setProperty("CENTAL_PIXEL_FLAG", CENTAL_PIXEL_FLAG_DEFAULT);
        calculatorProperties.setProperty("PROJECTION_CONVOLUTION_FLAG", PROJECTION_CONVOLUTION_FLAG_DEFAULT);
        saveCalculatorProperties();
    }

    private void saveCalculatorProperties() {
        try {
            calculatorProperties.store(new FileWriter(getEtcHome() + getFileSeparator() + "calculator.properties"), null);
        } catch (IOException ex) {
            logger.error("Failed to create the calculator.properties file. Reason: " + ex.getMessage(), ex);
        }
    }

    @Override
    public String getEtcHome() {
        if (etcHome == null) {
            etcHome = System.getProperty("etc.home");
            if (etcHome == null) {
                etcHome = System.getProperty("user.home") + getFileSeparator() + ".ETC";
            }
        }
        return etcHome;
    }

    @Override
    public String getUiMode() {
        if (uiMode == null) {
            uiMode = System.getProperty("etc.uiMode");
            if (uiMode == null) {
                uiMode = "GUI";
            }
        }
        return uiMode;
    }

    @Override
    public String getPluginDir() {
        if (pluginDir == null)
            pluginDir = getEtcHome() + getFileSeparator() + "plugins";
        return pluginDir;
    }

    @Override
    public String getTempDir() {
        if (tempDir == null)
            tempDir = getEtcHome() + getFileSeparator() + "temp";
        return tempDir;
    }
    
    @Override
    public String getVersion() {
        String path = "/version.prop";
        InputStream stream = getClass().getResourceAsStream(path);
        if (stream == null) return "UNKNOWN";
        Properties props = new Properties();
        try {
            props.load(stream);
            stream.close();
            return (String)props.get("version");
        } catch (IOException e) {
            return "UNKNOWN";
        }
    }
    
    @Override
    public double getSpectrumResolution() {
        if (calculatorProperties == null)
            readCalculatorFile();
        if (calculatorProperties.getProperty("SPECTRUM_RESOLUTION") == null) {
            logger.warn("calculator.properties file didn't contain the SPECTRUM_RESOLUTION");
            logger.warn("Setting it to default " + SPECTRUM_RESOLUTION_DEFAULT);
            setSpectrumResolution(Double.parseDouble(SPECTRUM_RESOLUTION_DEFAULT));
        }
        double result = Double.parseDouble(SPECTRUM_RESOLUTION_DEFAULT);
        try {
            result = Double.parseDouble(calculatorProperties.getProperty("SPECTRUM_RESOLUTION"));
        } catch (Exception ex) {
            logger.warn("The calculator.properties file contains an invalid spectrum resolution");
            logger.warn("Using the default spectrum resolution = " + result + " angstrom");
        }
        return result;
    }

    @Override
    public void setSpectrumResolution(double resolution) {
        if (calculatorProperties == null)
            readCalculatorFile();
        calculatorProperties.setProperty("SPECTRUM_RESOLUTION", Double.toString(resolution));
        saveCalculatorProperties();
    }

    @Override
    public boolean isPerSpectralResolutionElementForced() {
        if (calculatorProperties == null)
            readCalculatorFile();
        if (calculatorProperties.getProperty("CONVOL_KERNEL_PER_SPECTRAL_RESOL_ELEMENT") == null) {
            logger.warn("calculator.properties file didn't contain the CONVOL_KERNEL_PER_SPECTRAL_RESOL_ELEMENT");
            logger.warn("Setting it to default " + CONVOL_KERNEL_PER_SPECTRAL_RESOL_ELEMENT_DEFAULT);
            setPerSpectralResolutionElementForced(Boolean.parseBoolean(CONVOL_KERNEL_PER_SPECTRAL_RESOL_ELEMENT_DEFAULT));
        }
        boolean result = Boolean.parseBoolean(CONVOL_KERNEL_PER_SPECTRAL_RESOL_ELEMENT_DEFAULT);
        try {
            result = Boolean.parseBoolean(calculatorProperties.getProperty("CONVOL_KERNEL_PER_SPECTRAL_RESOL_ELEMENT"));
        } catch (Exception ex) {
            logger.warn("The calculator.properties file contains an invalid force per spectral resolution element flag");
            logger.warn("Using the default flag = " + result);
        }
        return result;
    }

    @Override
    public void setPerSpectralResolutionElementForced(boolean forced) {
        if (calculatorProperties == null)
            readCalculatorFile();
        calculatorProperties.setProperty("CONVOL_KERNEL_PER_SPECTRAL_RESOL_ELEMENT", Boolean.toString(forced));
        saveCalculatorProperties();
    }

    @Override
    public String getFileSeparator() {
        if (fileSeparator == null)
            fileSeparator = System.getProperty("file.separator");
        return fileSeparator;
    }

    @Override
    public String getLineSeparator() {
        if (lineSeparator == null)
            lineSeparator = System.getProperty("line.separator");
        return lineSeparator;
    }

    @Override
    public List<String> getImportUrlList() {
        if (uiProperties == null)
            readUiFile();
        if (uiProperties.getProperty("IMPORT_URLS") == null) {
            logger.warn("ui.properties file didn't contain the IMPORT_URLS");
            logger.warn("Setting it to default " + IMPORT_URLS_DEFAULT);
            uiProperties.setProperty("IMPORT_URLS", IMPORT_URLS_DEFAULT);
            saveUiProperties();
        }
        List<String> result = new ArrayList<String>();
        String importUrlString = uiProperties.getProperty("IMPORT_URLS");
        if (!importUrlString.trim().isEmpty()) {
            importUrlString = importUrlString.trim();
            for (String url : importUrlString.split(",")) {
                result.add(url.trim());
            }
        }
        return result;
    }

    @Override
    public void setImportUrlList(List<String> urlList) {
        if (uiProperties == null)
            readUiFile();
        StringBuilder urlString = new StringBuilder();
        for (String url : urlList) {
            urlString.append(url);
            urlString.append(", ");
        }
        String result = urlString.toString();
        if (!result.isEmpty()) {
            result = result.substring(0, result.length() - 2);
        }
        uiProperties.setProperty("IMPORT_URLS", result);
        saveUiProperties();
    }

    @Override
    public String getResultLevel() {
        if (uiProperties == null)
            readUiFile();
        if (uiProperties.getProperty("RESULT_LEVEL") == null) {
            logger.warn("ui.properties file didn't contain the RESULT_LEVEL");
            logger.warn("Setting it to default " + RESULT_LEVEL_DEFAULT);
            setResultLevel(RESULT_LEVEL_DEFAULT);
        }
        return uiProperties.getProperty("RESULT_LEVEL");
    }

    @Override
    public void setResultLevel(String level) {
        if (uiProperties == null)
            readUiFile();
        uiProperties.setProperty("RESULT_LEVEL", level);
        saveUiProperties();
    }

    @Override
    public List<String> getResultOrderFinal() {
        if (uiProperties == null)
            readUiFile();
        if (uiProperties.getProperty("RESULT_ORDER_FINAL") == null) {
            logger.warn("ui.properties file didn't contain the RESULT_ORDER_FINAL");
            logger.warn("Setting it to default " + RESULT_ORDER_FINAL_DEFAULT);
            uiProperties.setProperty("RESULT_ORDER_FINAL", RESULT_ORDER_FINAL_DEFAULT);
            saveUiProperties();
        }
        List<String> result = new ArrayList<String>();
        String orderString = uiProperties.getProperty("RESULT_ORDER_FINAL");
        if (!orderString.trim().isEmpty()) {
            orderString = orderString.trim();
            for (String resultItem : orderString.split(",")) {
                result.add(resultItem.trim());
            }
        }
        return result;
    }

    @Override
    public void setResultOrderFinal(List<String> order) {
        if (uiProperties == null)
            readUiFile();
        StringBuilder orderString = new StringBuilder();
        for (String resultItem : order) {
            orderString.append(resultItem);
            orderString.append(", ");
        }
        String result = orderString.toString();
        if (!result.isEmpty()) {
            result = result.substring(0, result.length() - 2);
        }
        uiProperties.setProperty("RESULT_ORDER_FINAL", result);
        saveUiProperties();
    }

    @Override
    public List<String> getResultOrderIntermediateImportant() {
        if (uiProperties == null)
            readUiFile();
        if (uiProperties.getProperty("RESULT_ORDER_INTERMEDIATE_IMPORTANT") == null) {
            logger.warn("ui.properties file didn't contain the RESULT_ORDER_INTERMEDIATE_IMPORTANT");
            logger.warn("Setting it to default " + RESULT_ORDER_INTERMEDIATE_IMPORTANT_DEFAULT);
            uiProperties.setProperty("RESULT_ORDER_INTERMEDIATE_IMPORTANT", RESULT_ORDER_INTERMEDIATE_IMPORTANT_DEFAULT);
            saveUiProperties();
        }
        List<String> result = new ArrayList<String>();
        String orderString = uiProperties.getProperty("RESULT_ORDER_INTERMEDIATE_IMPORTANT");
        if (!orderString.trim().isEmpty()) {
            orderString = orderString.trim();
            for (String resultItem : orderString.split(",")) {
                result.add(resultItem.trim());
            }
        }
        return result;
    }

    @Override
    public void setResultOrderIntermediateImportant(List<String> order) {
        if (uiProperties == null)
            readUiFile();
        StringBuilder orderString = new StringBuilder();
        for (String resultItem : order) {
            orderString.append(resultItem);
            orderString.append(", ");
        }
        String result = orderString.toString();
        if (!result.isEmpty()) {
            result = result.substring(0, result.length() - 2);
        }
        uiProperties.setProperty("RESULT_ORDER_INTERMEDIATE_IMPORTANT", result);
        saveUiProperties();
    }

    @Override
    public List<String> getResultOrderIntermediateUnimportant() {
        if (uiProperties == null)
            readUiFile();
        if (uiProperties.getProperty("RESULT_ORDER_INTERMEDIATE_UNIMPORTANT") == null) {
            logger.warn("ui.properties file didn't contain the RESULT_ORDER_INTERMEDIATE_UNIMPORTANT");
            logger.warn("Setting it to default " + RESULT_ORDER_INTERMEDIATE_UNIMPORTANT_DEFAULT);
            uiProperties.setProperty("RESULT_ORDER_INTERMEDIATE_UNIMPORTANT", RESULT_ORDER_INTERMEDIATE_UNIMPORTANT_DEFAULT);
            saveUiProperties();
        }
        List<String> result = new ArrayList<String>();
        String orderString = uiProperties.getProperty("RESULT_ORDER_INTERMEDIATE_UNIMPORTANT");
        if (!orderString.trim().isEmpty()) {
            orderString = orderString.trim();
            for (String resultItem : orderString.split(",")) {
                result.add(resultItem.trim());
            }
        }
        return result;
    }

    @Override
    public void setResultOrderIntermediateUnimportant(List<String> order) {
        if (uiProperties == null)
            readUiFile();
        StringBuilder orderString = new StringBuilder();
        for (String resultItem : order) {
            orderString.append(resultItem);
            orderString.append(", ");
        }
        String result = orderString.toString();
        if (!result.isEmpty()) {
            result = result.substring(0, result.length() - 2);
        }
        uiProperties.setProperty("RESULT_ORDER_INTERMEDIATE_UNIMPORTANT", result);
        saveUiProperties();
    }

    @Override
    public List<String> getResultOrderDebug() {
        if (uiProperties == null)
            readUiFile();
        if (uiProperties.getProperty("RESULT_ORDER_DEBUG") == null) {
            logger.warn("ui.properties file didn't contain the RESULT_ORDER_DEBUG");
            logger.warn("Setting it to default " + RESULT_ORDER_DEBUG_RESULT);
            uiProperties.setProperty("RESULT_ORDER_DEBUG", RESULT_ORDER_DEBUG_RESULT);
            saveUiProperties();
        }
        List<String> result = new ArrayList<String>();
        String orderString = uiProperties.getProperty("RESULT_ORDER_DEBUG");
        if (!orderString.trim().isEmpty()) {
            orderString = orderString.trim();
            for (String resultItem : orderString.split(",")) {
                result.add(resultItem.trim());
            }
        }
        return result;
    }

    @Override
    public void setResultOrderDebug(List<String> order) {
        if (uiProperties == null)
            readUiFile();
        StringBuilder orderString = new StringBuilder();
        for (String resultItem : order) {
            orderString.append(resultItem);
            orderString.append(", ");
        }
        String result = orderString.toString();
        if (!result.isEmpty()) {
            result = result.substring(0, result.length() - 2);
        }
        uiProperties.setProperty("RESULT_ORDER_DEBUG", result);
        saveUiProperties();
    }

    @Override
    public String[] getCommandLineArguments() {
        return commandLineArguments;
    }

    @Override
    public void setCommandLineArguments(String[] args) {
        commandLineArguments = args;
    }

    @Override
    public int getMaximumWebPageSizeShown() {
        if (uiProperties == null)
            readUiFile();
        if (uiProperties.getProperty("MAX_WEB_PAGE_SIZE") == null) {
            logger.warn("ui.properties file didn't contain the MAX_WEB_PAGE_SIZE");
            logger.warn("Setting it to default " + MAX_WEB_PAGE_SIZE_DEFAULT);
            setMaximumWebPageSizeShown(Integer.parseInt(MAX_WEB_PAGE_SIZE_DEFAULT));
        }
        int result = Integer.parseInt(MAX_WEB_PAGE_SIZE_DEFAULT);
        try {
            result = Integer.parseInt(calculatorProperties.getProperty("MAX_WEB_PAGE_SIZE"));
        } catch (Exception ex) {
            logger.warn("The ui.properties file contains an invalid maximum web page size");
            logger.warn("Using the default maximum = " + result + " bytes");
        }
        return result;
    }

    @Override
    public void setMaximumWebPageSizeShown(int bytes) {
        if (uiProperties == null)
            readUiFile();
        uiProperties.setProperty("MAX_WEB_PAGE_SIZE", Integer.toString(bytes));
        saveUiProperties();
    }

    @Override
    public boolean isCacheDisabled() {
        if (calculatorProperties == null)
            readCalculatorFile();
        if (calculatorProperties.getProperty("CACHE_DISABLED") == null) {
            logger.warn("calculator.properties file didn't contain the CACHE_DISABLED");
            logger.warn("Setting it to default " + CACHE_DISABLED_DEFAULT);
            setCacheDisabled(Boolean.parseBoolean(CACHE_DISABLED_DEFAULT));
        }
        boolean result = Boolean.parseBoolean(CACHE_DISABLED_DEFAULT);
        try {
            result = Boolean.parseBoolean(calculatorProperties.getProperty("CACHE_DISABLED"));
        } catch (Exception ex) {
            logger.warn("The calculator.properties file contains an invalid cache disabled flag");
            logger.warn("Using the default flag = " + result);
        }
        return result;
    }

    @Override
    public void setCacheDisabled(boolean forced) {
        if (calculatorProperties == null)
            readCalculatorFile();
        calculatorProperties.setProperty("CACHE_DISABLED", Boolean.toString(forced));
        saveCalculatorProperties();
    }

    @Override
    public int getPsfConvolutionSamplingSize() {
        if (calculatorProperties == null)
            readCalculatorFile();
        if (calculatorProperties.getProperty("PSF_CONVOLUTION_SAMPLING_SIZE") == null) {
            logger.warn("calculator.properties file didn't contain the PSF_CONVOLUTION_SAMPLING_SIZE");
            logger.warn("Setting it to default " + PSF_CONVOLUTION_SAMPLING_SIZE_DEFAULT);
            setPsfConvolutionSamplingSize(Integer.parseInt(PSF_CONVOLUTION_SAMPLING_SIZE_DEFAULT));
        }
        int result = Integer.parseInt(PSF_CONVOLUTION_SAMPLING_SIZE_DEFAULT);
        try {
            result = Integer.parseInt(calculatorProperties.getProperty("PSF_CONVOLUTION_SAMPLING_SIZE"));
        } catch (Exception ex) {
            logger.warn("The calculator.properties file contains an invalid PSF convolution sampling size");
            logger.warn("Using the default sampling size = " + result);
        }
        return result;
    }

    @Override
    public void setPsfConvolutionSamplingSize(int size) {
        if (calculatorProperties == null)
            readCalculatorFile();
        calculatorProperties.setProperty("PSF_CONVOLUTION_SAMPLING_SIZE", Integer.toString(size));
        saveCalculatorProperties();
    }

    @Override
    public boolean getCentralPixelFlag() {
        if (calculatorProperties == null)
            readCalculatorFile();
        if (calculatorProperties.getProperty("CENTAL_PIXEL_FLAG") == null) {
            logger.warn("calculator.properties file didn't contain the CENTAL_PIXEL_FLAG");
            logger.warn("Setting it to default " + CENTAL_PIXEL_FLAG_DEFAULT);
            setCentralPixelFlag(Boolean.parseBoolean(CENTAL_PIXEL_FLAG_DEFAULT));
        }
        boolean result = Boolean.parseBoolean(CENTAL_PIXEL_FLAG_DEFAULT);
        try {
            result = Boolean.parseBoolean(calculatorProperties.getProperty("CENTAL_PIXEL_FLAG"));
        } catch (Exception ex) {
            logger.warn("The calculator.properties file contains an invalid central pixel flag");
            logger.warn("Using the default flag = " + result);
        }
        return result;
    }

    @Override
    public void setCentralPixelFlag(boolean calculate) {
        if (calculatorProperties == null)
            readCalculatorFile();
        calculatorProperties.setProperty("CENTAL_PIXEL_FLAG", Boolean.toString(calculate));
        saveCalculatorProperties();
    }

    @Override
    public boolean getProjectionConvolutionFlag() {
        if (calculatorProperties == null)
            readCalculatorFile();
        if (calculatorProperties.getProperty("PROJECTION_CONVOLUTION_FLAG") == null) {
            logger.warn("calculator.properties file didn't contain the PROJECTION_CONVOLUTION_FLAG");
            logger.warn("Setting it to default " + PROJECTION_CONVOLUTION_FLAG_DEFAULT);
            setProjectionConvolutionFlag(Boolean.parseBoolean(PROJECTION_CONVOLUTION_FLAG_DEFAULT));
        }
        boolean result = Boolean.parseBoolean(PROJECTION_CONVOLUTION_FLAG_DEFAULT);
        try {
            result = Boolean.parseBoolean(calculatorProperties.getProperty("PROJECTION_CONVOLUTION_FLAG"));
        } catch (Exception ex) {
            logger.warn("The calculator.properties file contains an invalid projection convolution flag");
            logger.warn("Using the default flag = " + result);
        }
        return result;
    }

    @Override
    public void setProjectionConvolutionFlag(boolean projection) {
        if (calculatorProperties == null)
            readCalculatorFile();
        calculatorProperties.setProperty("PROJECTION_CONVOLUTION_FLAG", Boolean.toString(projection));
        saveCalculatorProperties();
    }
    
    @Override
    public String getFixturesUrlsDefault(){
        if (uiProperties == null)
            readUiFile();
        String url = uiProperties.getProperty("FIXTURES_URLS");
        if(url == null){
        	System.out.println("Unable to find FIXTURES_URLS in ui.properties file.");
			System.out.println("Delete this file or add FIXTURES_URLS="+FIXTURES_URLS_DEFAULT+" at the end of it.");
        }
        return url;
    }
    
    @Override
    public String getOfficialRepoUrlDefault(){
    	if (uiProperties == null)
            readUiFile();
        String url = uiProperties.getProperty("OFFICIAL_REPO_URL", OFFICIAL_REPO_URL_DEFAULT);
        if(url == null){
        	System.out.println("Unable to find OFFICIAL_REPO_URL in ui.properties file.");
        }
        return url;
    }
}
