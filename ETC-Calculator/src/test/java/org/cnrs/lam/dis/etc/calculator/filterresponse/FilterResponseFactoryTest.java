/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.filterresponse;

import java.util.Map;
import org.cnrs.lam.cesam.util.calculator.Calculator;
import org.cnrs.lam.dis.etc.calculator.DatasetProviderHolder;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.functions.BoundedUnivariateFunction;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Result;
import org.cnrs.lam.dis.etc.datamodel.ComponentInfo;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.cnrs.lam.dis.etc.datamodel.DatasetMockCreator;
import org.cnrs.lam.dis.etc.datamodel.DatasetProvider;
import org.cnrs.lam.dis.etc.datamodel.Instrument;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.javatuples.Tuple;
import org.javatuples.Unit;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.BDDMockito.*;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class FilterResponseFactoryTest {
    
    @Mock Instrument instrument;
    @Mock Session session;
    @Mock DatasetProvider datasetProvider;
    @Mock Dataset filterTransmissionDataset;
    
    @Before
    public void setUp() {
        // This is necessary to initialize all the mock annotated variables
        MockitoAnnotations.initMocks(this);
        
        when(session.getInstrument()).thenReturn(instrument);
        when(instrument.getInfo()).thenReturn(new ComponentInfo("test_instrument", null));
        
        filterTransmissionDataset = DatasetMockCreator.createFilterTransmission();
        when(datasetProvider.getDataset(eq(Dataset.Type.FILTER_TRANSMISSION), any(DatasetInfo.class))).thenReturn(filterTransmissionDataset);
        DatasetProviderHolder.setDatasetProvider(datasetProvider);
        
        ResultsHolder.resetResults();
    }
    
    /**
     * Tests that we get the filter transmission profile calculator when we have
     * imaging simulation with the filter transmission set.
     */
    @Test
    public void testImagingWithFilter() throws Exception {
        //given
        given(instrument.getInstrumentType()).willReturn(Instrument.InstrumentType.IMAGING);
        DatasetInfo filterInfo = filterTransmissionDataset.getInfo();
        given(instrument.getFilterTransmission()).willReturn(filterInfo);
        
        //when
        Calculator<Tuple, Unit<BoundedUnivariateFunction>> calculator = new FilterResponseFactory().getCalculator(new Unit<Session>(session));
        
        //then
        assertNotNull(calculator);
        assertTrue(calculator instanceof FilterTransmissionProfile);
        // Check that we got it at the results
        Map<String, Result> results = ResultsHolder.getResults().getResults(CalculationResults.Level.DEBUG);
        CalculationResults.StringResult result = (CalculationResults.StringResult) results.get("FILTER_RESPONSE_METHOD");
        assertNotNull(result);
        assertEquals("Filter Transmission Profile", result.getValue());
    }
    
    /**
     * Test that in imaging mode without filter we get the no filter calculator.
     */
    @Test
    public void testImagingWithoutFilter() throws Exception {
        //given
        given(instrument.getInstrumentType()).willReturn(Instrument.InstrumentType.IMAGING);
        given(instrument.getFilterTransmission()).willReturn(null);
        
        //when
        Calculator<Tuple, Unit<BoundedUnivariateFunction>> calculator = new FilterResponseFactory().getCalculator(new Unit<Session>(session));
        
        //then
        assertNotNull(calculator);
        assertTrue(calculator instanceof NoFilter);
        // Check that we got it at the results
        Map<String, Result> results = ResultsHolder.getResults().getResults(CalculationResults.Level.DEBUG);
        CalculationResults.StringResult result = (CalculationResults.StringResult) results.get("FILTER_RESPONSE_METHOD");
        assertNotNull(result);
        assertEquals("No Filter", result.getValue());
    }

    /**
     * Test that in spectroscopy mode with filter we get the no filter calculator.
     */
    @Test
    public void testSpectroscopyWithFilter() throws Exception {
        //given
        given(instrument.getInstrumentType()).willReturn(Instrument.InstrumentType.SPECTROGRAPH);
        DatasetInfo filterInfo = filterTransmissionDataset.getInfo();
        given(instrument.getFilterTransmission()).willReturn(filterInfo);
        
        //when
        Calculator<Tuple, Unit<BoundedUnivariateFunction>> calculator = new FilterResponseFactory().getCalculator(new Unit<Session>(session));
        
        //then
        assertNotNull(calculator);
        assertTrue(calculator instanceof NoFilter);
        // Check that we got it at the results
        Map<String, Result> results = ResultsHolder.getResults().getResults(CalculationResults.Level.DEBUG);
        CalculationResults.StringResult result = (CalculationResults.StringResult) results.get("FILTER_RESPONSE_METHOD");
        assertNotNull(result);
        assertEquals("No Filter", result.getValue());
    }

    /**
     * Test that in spectroscopy mode without filter we get the no filter calculator.
     */
    @Test
    public void testSpectroscopyWithoutFilter() throws Exception {
        //given
        given(instrument.getInstrumentType()).willReturn(Instrument.InstrumentType.SPECTROGRAPH);
        given(instrument.getFilterTransmission()).willReturn(null);
        
        //when
        Calculator<Tuple, Unit<BoundedUnivariateFunction>> calculator = new FilterResponseFactory().getCalculator(new Unit<Session>(session));
        
        //then
        assertNotNull(calculator);
        assertTrue(calculator instanceof NoFilter);
        // Check that we got it at the results
        Map<String, Result> results = ResultsHolder.getResults().getResults(CalculationResults.Level.DEBUG);
        CalculationResults.StringResult result = (CalculationResults.StringResult) results.get("FILTER_RESPONSE_METHOD");
        assertNotNull(result);
        assertEquals("No Filter", result.getValue());
    }
}
