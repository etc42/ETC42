/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.wavelengthrange;

import java.util.Map;
import java.util.ResourceBundle;
import java.util.TreeMap;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.CalculationListener;
import org.cnrs.lam.cesam.util.calculator.Calculator;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.calculator.util.datasets.LinearFunctionDataset;
import org.cnrs.lam.dis.etc.calculator.util.functions.BoundedUnivariateFunction;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.javatuples.Pair;
import org.javatuples.Tuple;
import org.javatuples.Unit;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author nikoapos
 */
public class ImagingTest {

    private static final ResourceBundle validationErrorsBundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");

    @Before
    public void setUp() throws Exception {
        ResultsHolder.resetResults();
    }

    /**
     * Test of validateConfiguration method, of class Imaging.
     */
    @Test
    public void testValidateConfiguration() throws Exception {
        // We check that if the two datasets overlap we don't get an exception
        Map<Double, Double> filterData = new TreeMap<Double, Double>();
        filterData.put(1., 1.);
        filterData.put(2., 0.);
        filterData.put(3., 0.);
        filterData.put(4., 0.);
        filterData.put(5., 0.);
        TestDatasetCalculator filterCalculator = new TestDatasetCalculator(filterData);
        Map<Double, Double> systemData = new TreeMap<Double, Double>();
        systemData.put(1., 0.);
        systemData.put(2., 1.);
        systemData.put(3., 0.);
        systemData.put(4., 0.);
        systemData.put(5., 0.);
        TestDatasetCalculator systemCalculator = new TestDatasetCalculator(systemData);
        Imaging instance = new Imaging();
        instance.validateConfiguration(new Pair<Calculator<Tuple, Unit<BoundedUnivariateFunction>>, Calculator<Tuple, Unit<BoundedUnivariateFunction>>>(filterCalculator, systemCalculator));

        // We check that when the datasets do not overlap we get an exception
        filterData = new TreeMap<Double, Double>();
        filterData.put(1., 1.);
        filterData.put(2., 0.);
        filterData.put(3., 0.);
        filterData.put(4., 0.);
        filterData.put(5., 0.);
        filterCalculator = new TestDatasetCalculator(filterData);
        systemData = new TreeMap<Double, Double>();
        systemData.put(1., 0.);
        systemData.put(2., 0.);
        systemData.put(3., 0.);
        systemData.put(4., 0.);
        systemData.put(5., 1.);
        systemCalculator = new TestDatasetCalculator(systemData);
        instance = new Imaging();
        try {
            instance.validateConfiguration(new Pair<Calculator<Tuple, Unit<BoundedUnivariateFunction>>, Calculator<Tuple, Unit<BoundedUnivariateFunction>>>(filterCalculator, systemCalculator));
            fail("No exception thrown for filter transmission and total efficiency not overlapping");
        } catch (ConfigurationException ex) {
            String expected = validationErrorsBundle.getString("FILTER_TOTAL_EFFICIENCY_NOT_OVERLAPPING");
            assertEquals(expected, ex.getMessage());
        }
    }

    /**
     * Test of performCalculation method, of class Imaging.
     */
    @Test
    public void testPerformCalculation() throws Exception {
        Map<Double, Double> filterData = new TreeMap<Double, Double>();
        filterData.put(1., 0.);
        filterData.put(2., 1.);
        filterData.put(3., 0.);
        filterData.put(4., 0.);
        filterData.put(5., 0.);
        TestDatasetCalculator filterCalculator = new TestDatasetCalculator(filterData);
        Map<Double, Double> systemData = new TreeMap<Double, Double>();
        systemData.put(1., 0.);
        systemData.put(2., 0.);
        systemData.put(3., 1.);
        systemData.put(4., 0.);
        systemData.put(5., 0.);
        TestDatasetCalculator systemCalculator = new TestDatasetCalculator(systemData);
        Imaging instance = new Imaging();
        instance.initialize(new Pair<Calculator<Tuple, Unit<BoundedUnivariateFunction>>, Calculator<Tuple, Unit<BoundedUnivariateFunction>>>(filterCalculator, systemCalculator));
        Pair<Double, Double> result = instance.performCalculation(null);
        assertEquals(2., result.getValue0(), 0.);
        assertEquals(3., result.getValue1(), 0.);
    }

    /**
     * Test of performForEveryCalculation method, of class Spectroscopy. It
     * expects that the results WAVELENGTH_MIN and WAVELENGTH_MAX will be in the
     * intermediate unimportant results.
     */
    @Test
    public void testPerformForEveryCalculation() {
        double minValue = 5000.;
        double maxValue = 10000.;
        Imaging instance = new Imaging();
        instance.performForEveryCalculation(null, new Pair<Double, Double>(minValue, maxValue));
        // Check for the min result
        Map<String, CalculationResults.Result> results = ResultsHolder.getResults().getResults(CalculationResults.Level.INTERMEDIATE_UNIMPORTANT);
        CalculationResults.DoubleValueResult minResult = (CalculationResults.DoubleValueResult) results.get("WAVELENGTH_MIN");
        assertNotNull(minResult);
        assertEquals(minValue, minResult.getValue(), 0);
        assertEquals(Units.ANGSTROM, minResult.getUnit());
        // Check for the max result
        CalculationResults.DoubleValueResult maxResult = (CalculationResults.DoubleValueResult) results.get("WAVELENGTH_MAX");
        assertNotNull(maxResult);
        assertEquals(maxValue, maxResult.getValue(), 0);
        assertEquals(Units.ANGSTROM, maxResult.getUnit());
    }

    private class TestDatasetCalculator implements Calculator<Tuple, Unit<BoundedUnivariateFunction>> {

        private LinearFunctionDataset function;

        public TestDatasetCalculator(Map<Double, Double> data) {
            function = new LinearFunctionDataset(data);
        }

        @Override
        public Unit<BoundedUnivariateFunction> calculate(Tuple input) throws CalculationException {
            return new Unit<BoundedUnivariateFunction>(function);
        }

        @Override
        public void setCalculationListener(CalculationListener calculationListener) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public CalculationListener getCalculationListener() {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }
}
