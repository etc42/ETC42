/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.util.datasets;

import java.util.Map;
import java.util.TreeMap;
import org.apache.commons.math.FunctionEvaluationException;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author nikoapos
 */
public class LinearFunctionDatasetTest {
    
    private LinearFunctionDataset dataset;
    
    public LinearFunctionDatasetTest() {
    }
    
    @Before
    public void setUp() {
        Map<Double, Double> nodes = new TreeMap<Double, Double>();
        nodes.put(3., 20.);
        nodes.put(4., 10.);
        nodes.put(1., 10.);
        nodes.put(2., 20.);
        dataset = new LinearFunctionDataset(nodes);
    }

    /**
     * Test of value method, of class LinearFunctionDataset.
     */
    @Test
    public void testValue() throws Exception {
        // Check that we get exceptions when we are out of bounds
        try {
            double result = dataset.value(0.5);
            fail("Value " + result + " returned for X below the function bounds");
        } catch (FunctionEvaluationException e) {
        }
        try {
            double result = dataset.value(4.5);
            fail("Value " + result + " returned for X below the function bounds");
        } catch (FunctionEvaluationException e) {
        }
        // Check that we get the correct value on a node
        double result = dataset.value(1.);
        assertEquals(10., result, 0.);
        // Check that we get the correct linear interpolation
        result = dataset.value(1.25);
        assertEquals(12.5, result, 0.0001);
    }

    /**
     * Test of integral method, of class LinearFunctionDataset.
     */
    @Test
    public void testIntegral() throws Exception {
        // Check that we get exception when X1 > X2
        try {
            double result = dataset.integral(3., 2.);
            fail("Integral " + result + " returned for X1 > X2");
        } catch (IllegalArgumentException e) {
        }
        // Check that we get exceptions when we are out of bounds
        try {
            double result = dataset.integral(0., 2.);
            fail("Integral " + result + " returned for X1 out of bounds");
        } catch (FunctionEvaluationException e) {
        }
        try {
            double result = dataset.integral(3., 5.);
            fail("Integral " + result + " returned for X2 out of bounds");
        } catch (FunctionEvaluationException e) {
        }
        // Check that we get 0 if X1 = X2
        double result = dataset.integral(2.5, 2.5);
        assertEquals(0., result, 0.);
        // Check that we get correct result when the X1 and X2 are between two nodes
        result = dataset.integral(1.5, 1.6);
        assertEquals(1.55, result, 0.0001);
        // Check that we get correct results when the X1 and X2 are on nodes
        result = dataset.integral(3., 4.);
        assertEquals(15., result, 0.0001);
        // Check that we get correct results when the X1 and X2 cover many nodes
        result = dataset.integral(1.1, 3.9);
        assertEquals(47.9, result, 0.0001);
    }
}
