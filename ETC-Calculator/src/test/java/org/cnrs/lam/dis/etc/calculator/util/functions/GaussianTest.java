/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.util.functions;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author nikoapos
 */
public class GaussianTest {
    
    @Test
    public void testGetStandardDeviation() {
        double sigma = 0.5;
        Gaussian instance = new Gaussian(sigma);
        assertEquals(sigma, instance.getStandardDeviation(), 0.);
    }
    
    @Test
    public void testGetScale() {
        double sigma = 0.5;
        double scale = 2.;
        Gaussian instance = new Gaussian(sigma, scale);
        assertEquals(scale, instance.getScale(), 0.);
    }

    /**
     * Test of value method, of class Gaussian.
     */
    @Test
    public void testValue() throws Exception {
        double error = 1E-8;
        double sigma = 0.5;
        double[] x = new double[19];
        double[] value = new double[19];
        x[0] = -100.; value[0] = 0.; 
        x[1] = -3.;   value[1] = 0.000000012;
        x[2] = -2.5;  value[2] = 0.000002973;
        x[3] = -2.;   value[3] = 0.000267661;
        x[4] = -1.5;  value[4] = 0.008863697;
        x[5] = -1.;   value[5] = 0.107981933;
        x[6] = -0.5;  value[6] = 0.483941449;
        x[7] = -0.3;  value[7] = 0.666449206;
        x[8] = -0.1;  value[8] = 0.782085388;
        x[9] = 0.;    value[9] = 0.797884561;
        x[10] = 100.; value[10] = 0.; 
        x[11] = 3.;   value[11] = 0.000000012;
        x[12] = 2.5;  value[12] = 0.000002973;
        x[13] = 2.;   value[13] = 0.000267661;
        x[14] = 1.5;  value[14] = 0.008863697;
        x[15] = 1.;   value[15] = 0.107981933;
        x[16] = 0.5;  value[16] = 0.483941449;
        x[17] = 0.3;  value[17] = 0.666449206;
        x[18] = 0.1;  value[18] = 0.782085388;
        // Check if a gaussian without scale factor works fine
        Gaussian gaussian = new Gaussian(sigma);
        for (int i = 0; i < x.length; i++) {
            assertEquals("Gaussian calculation failed for x = " + x[i], value[i], gaussian.value(x[i]), error);
        }
        // Check if the scale factor is working also
        double scale = 2.;
        gaussian = new Gaussian(sigma, scale);
        for (int i = 0; i < x.length; i++) {
            double expectedValue = scale * value[i];
            assertEquals("Scaled Gaussian calculation failed for x = " + x[i], expectedValue, gaussian.value(x[i]), error);
        }
    }

    /**
     * Test of integral method, of class Gaussian.
     */
    @Test
    public void testIntegral() throws Exception {
        // Check that we get an exception if x1 > x2
        double sigma = 0.5;
        Gaussian gaussian = new Gaussian(sigma);
        try {
            gaussian.integral(1., -1.);
            fail("Gaussian integration did not throw an exception for x1 > x2");
        } catch (Exception e) {
        }
        // Check that the calculated values are correct
        double error = 1E-3;
        double[] x1 = new double[18];
        double[] x2 = new double[18];
        double[] integral = new double[18];
        x1[0] = 0.;    x2[0] = 0.5;  integral[0] = 0.1915;
        x1[1] = 0.;    x2[1] = 1.;   integral[1] = 0.3413;
        x1[2] = 0.;    x2[2] = 1.5;  integral[2] = 0.4332;
        x1[3] = 0.;    x2[3] = 2.;   integral[3] = 0.4772;
        x1[4] = 0.;    x2[4] = 2.5;  integral[4] = 0.4938;
        x1[5] = 0.;    x2[5] = 3.;   integral[5] = 0.4987;
        x1[6] = -0.5;  x2[6] = 0.;   integral[6] = 0.1915;
        x1[7] = -1.;   x2[7] = 0.;   integral[7] = 0.3413;
        x1[8] = -1.5;  x2[8] = 0.;   integral[8] = 0.4332;
        x1[9] = -2.;   x2[9] = 0.;   integral[9] = 0.4772;
        x1[10] = -2.5; x2[10] = 0.;  integral[10] = 0.4938;
        x1[11] = -3.;  x2[11] = 0.;  integral[11] = 0.4987;
        x1[12] = -1.;  x2[12] = 1.;  integral[12] = 0.6826;
        x1[13] = -2.;  x2[13] = 2.;  integral[13] = 0.9544;
        x1[14] = -3.;  x2[14] = 3.;  integral[14] = 0.9974;
        x1[15] = 0.;   x2[15] = 0.;  integral[15] = 0.;
        x1[16] = 1.;   x2[16] = 1.;  integral[16] = 0.;
        x1[17] = -2.;  x2[17] = -2.; integral[17] = 0.;
        // Check if a gaussian without scale factor works fine
        for (int i = 0; i < x1.length; i++) {
            double value = gaussian.integral(sigma * x1[i], sigma * x2[i]);
            assertEquals("Gaussian integration failed for [x1,x2] = [" + x1[i] + "," + x2[i] + "]", integral[i], value, error);
        }
        // Check if the scale factor is working also
        double scale = 2.;
        gaussian = new Gaussian(sigma, scale);
        for (int i = 0; i < x1.length; i++) {
            double expectedValue = scale * integral[i];
            double value = gaussian.integral(sigma * x1[i], sigma * x2[i]);
            assertEquals("Scaled Gaussian integration failed for [x1,x2] = [" + x1[i] + "," + x2[i] + "]", expectedValue, value, error);
        }
    }

    /**
     * Test of equals method, of class Gaussian. Any two Gaussians with the same
     * standard deviation should be equal.
     */
    @Test
    public void testEquals() {
        Gaussian gaussian1 = new Gaussian(0.5);
        Gaussian gaussian2 = new Gaussian(0.5);
        assertNotSame(gaussian1, gaussian2);
        //assertTrue(gaussian1.equals(gaussian2));
        gaussian1 = new Gaussian(0.5);
        gaussian2 = new Gaussian(0.6);
        assertNotSame(gaussian1, gaussian2);
        assertFalse(gaussian1.equals(gaussian2));
    }
}
