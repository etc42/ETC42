/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.util.datasets;

import java.util.Map;
import java.util.TreeMap;
import org.javatuples.Pair;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class DatasetTest {
    
    /*
     * This method checks if two maps are the same.
     */
    private boolean compareMapEntries(Map<Double, Double> first, Map<Double, Double> second) {
        if (first.size() != second.size()) {
            return false;
        }
        for (Map.Entry<Double, Double> entry : first.entrySet()) {
            Double key = entry.getKey();
            Double value1 = entry.getValue();
            Double value2 = second.get(key);
            if (value2 == null || !value1.equals(value2)) {
                return false;
            }
        }
        return true;
    }
    
    /*
     * This method converts a map to a printable format, to be used for test
     * failure messages.
     */
    private String mapToPrint(Map<Double, Double> map) {
        StringBuilder result = new StringBuilder("[");
        for (Map.Entry<Double, Double> entry : map.entrySet()) {
            Double key = entry.getKey();
            Double value = entry.getValue();
            result.append("(").append(key).append(",").append(value).append(")");
        }
        result.append("]");
        return result.toString();
    }

    /**
     * Test that we get the same data nodes as the ones we entered, that they
     * are ordered and that the returned map is read only.
     */
    @Test
    public void testGetData() {
        //given
        Map<Double, Double> expResult = new TreeMap<Double, Double>();
        expResult.put(3., 20.);
        expResult.put(4., 10.);
        expResult.put(1., 10.);
        expResult.put(2., 20.);
        
        //when
        Dataset instance = new Dataset(expResult);
        Map<Double, Double> result = instance.getData();
        
        //then
        assertTrue("Expected map with nodes " + mapToPrint(expResult) + " but got " + mapToPrint(result), compareMapEntries(expResult, result));
        // Check that map is ordered
        double iNew;
        double iOld = Double.NEGATIVE_INFINITY;
        for (Double key : result.keySet()) {
            iNew = key;
            if (iNew < iOld) {
                fail("Returned map is not ordered " + mapToPrint(result));
            }
        }
        // Check the map is read only
        try {
            result.put(5., 50.);
            fail("Returned map is not read only");
        } catch (UnsupportedOperationException e) {
        }
    }
    
    /**
     * Test that the bounds of the dataset are calculated correctly.
     */
    @Test
    public void testGetBounds() {
        //given
        Map<Double, Double> expResult = new TreeMap<Double, Double>();
        expResult.put(3., 20.);
        expResult.put(5., 0.);
        expResult.put(4., 10.);
        expResult.put(0., 0.);
        expResult.put(-1., 0.);
        expResult.put(6., 0.);
        expResult.put(1., 10.);
        expResult.put(2., 20.);
        
        //when
        Dataset instance = new Dataset(expResult);
        Pair<Double, Double> result = instance.getBounds();
        
        //then
        assertEquals(-1., result.getValue0(), 0.);
        assertEquals(6., result.getValue1(), 0.);
    }
    
    /**
     * Test that leading or trailing zeroes in the dataset are detected correctly.
     */
    @Test
    public void testLeadingTrailingZeroes() {
        //given
        Map<Double, Double> expResult = new TreeMap<Double, Double>();
        expResult.put(3., 20.);
        expResult.put(5., 0.);
        expResult.put(4., 10.);
        expResult.put(0., 0.);
        expResult.put(-1., 0.);
        expResult.put(6., 0.);
        expResult.put(1., 10.);
        expResult.put(2., 20.);
        
        //when
        Dataset instance = new Dataset(expResult);
        Pair<Double, Double> result = instance.getNonZeroBounds();
        
        //then
        assertEquals(0., result.getValue0(), 0.);
        assertEquals(5., result.getValue1(), 0.);
    }
    
    /**
     * Test that the non-zero bounds of the dataset are calculated correctly.
     */
    @Test
    public void testGetNonZeroBounds() {
        //given
        Map<Double, Double> expResult = new TreeMap<Double, Double>();
        expResult.put(3., 20.);
        expResult.put(5., 0.);
        expResult.put(4., 10.);
        expResult.put(0., 0.);
        expResult.put(0.5, 0.);
        expResult.put(-1., 10.);
        expResult.put(6., 10.);
        expResult.put(1., 10.);
        expResult.put(2., 20.);
        
        //when
        Dataset instance = new Dataset(expResult);
        Pair<Double, Double> result = instance.getNonZeroBounds();
        
        //then
        assertEquals(-1., result.getValue0(), 0.);
        assertEquals(6., result.getValue1(), 0.);
    }
    
}
