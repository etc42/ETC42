/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.util.integration;

import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.analysis.BivariateRealFunction;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.cnrs.lam.dis.etc.calculator.util.functions.CircularlySymmetricBivariateFunction;
import org.cnrs.lam.dis.etc.calculator.util.functions.Gaussian;
import org.cnrs.lam.dis.etc.calculator.util.functions.IntegrableBivariateFunction;
import org.cnrs.lam.dis.etc.calculator.util.functions.DefaultPolarFunction;
import org.cnrs.lam.dis.etc.calculator.util.functions.SymmetricBivariateGaussian;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author nikoapos
 */
public class IntegrationToolTest {
    
    /**
     * Test of univariateIntegral method, of class IntegrationTool.
     */
    @Test
    public void testUnivariateIntegral() throws Exception {
        double err = 1E-4;
        UnivariateRealFunction function = new UnivariateRealFunction() {
            @Override
            public double value(double x) throws FunctionEvaluationException {
                return 5 + 2 * x;
            }
        };
        // Check that we get an exception if the limits are wrong
        try {
            IntegrationTool.univariateIntegral(null, 5., 4.);
            fail("Univariate integration didn't fail for invalid range.");
        } catch (IllegalArgumentException ex) {
        }
        // Check that we get zero if the limits are equal
        double integral = IntegrationTool.univariateIntegral(function, 1., 1.);
        assertEquals(0., integral, 0.);
        // Check that the integration works correctly
        integral = IntegrationTool.univariateIntegral(function, 0., 1.);
        assertEquals(6., integral, err);
        integral = IntegrationTool.univariateIntegral(function, 0., 1.5);
        assertEquals(9.75, integral, err);
        integral = IntegrationTool.univariateIntegral(function, -1., 1.);
        assertEquals(10., integral, err);
        integral = IntegrationTool.univariateIntegral(function, -1.5, 1.5);
        assertEquals(15., integral, err);
    }

    /**
     * Test of univariateIntegral method, of class IntegrationTool.
     */
    @Test
    public void testIntegrableUnivariateIntegral() throws Exception {
        double err = 1E-4;
        Gaussian gaussian = new Gaussian(0.5);
        double integral = IntegrationTool.univariateIntegral(gaussian, 0., 100.);
        assertEquals(0.5, integral, err);
        integral = IntegrationTool.univariateIntegral(gaussian, 0., 0.5);
        assertEquals(0.3413, integral, err);
        integral = IntegrationTool.univariateIntegral(gaussian, -100., 100.);
        assertEquals(1., integral, err);
        integral = IntegrationTool.univariateIntegral(gaussian, -0.5, 0.5);
        assertEquals(0.6826, integral, err);
    }

    /**
     * Test of bivariateIntegral method, of class IntegrationTool.
     */
    @Test
    public void testCircularBivariateIntegral() throws Exception {
        double err = 1E-4;
        CircularlySymmetricBivariateFunction function = new CircularlySymmetricBivariateFunction() {
            private UnivariateRealFunction projection = new UnivariateRealFunction() {
                @Override
                public double value(double x) throws FunctionEvaluationException {
                    return 0.5;
                }
            };
            @Override
            public double value(double r) throws FunctionEvaluationException {
                return 0.5;
            }
            @Override
            public UnivariateRealFunction projectionFunction() {
                return projection;
            }
            @Override
            public double value(double x, double y) throws FunctionEvaluationException {
                return 0.5;
            }
            @Override
            public UnivariateRealFunction polarFunction() {
                return new DefaultPolarFunction(projection);
            }
        };
        // Check that we get an exception when the radius is negative
        try {
            IntegrationTool.bivariateIntegral(function, -0.1);
            fail("Integration didn't fail for negative radius");
        } catch (IllegalArgumentException ex) {
        }
        // Check that we get zero if the radius is zero
        double integral = IntegrationTool.bivariateIntegral(function, 0.);
        assertEquals(0., integral, 0.);
        // Check that we get the correct integral values
        double r = 0.5;
        double expected = 0.5 * Math.PI * r * r;
        integral = IntegrationTool.bivariateIntegral(function, r);
        assertEquals(expected, integral, err);
        r = 1.;
        expected = 0.5 * Math.PI * r * r;
        integral = IntegrationTool.bivariateIntegral(function, r);
        assertEquals(expected, integral, err);
        r = 2.;
        expected = 0.5 * Math.PI * r * r;
        integral = IntegrationTool.bivariateIntegral(function, r);
        assertEquals(expected, integral, err);
        // Check also with a Gaussian and a cyrcle of 5 sigma radius
        SymmetricBivariateGaussian gaussian = new SymmetricBivariateGaussian(0.5);
        integral = IntegrationTool.bivariateIntegral(gaussian, 2.5);
        assertEquals(1., integral, err);
    }
    
    @Test
    public void testBivariateIntegralIntegrable() throws Exception {
        double err = 1E-4;
        IntegrableBivariateFunction function = new IntegrableBivariateFunction() {
            @Override
            public double integral(double x1, double x2, double y1, double y2) throws FunctionEvaluationException {
                return 0.5 * (x2 - x1) * (y2 - y1);
            }
            @Override
            public double value(double x, double y) throws FunctionEvaluationException {
                return 0.5;
            }
        };
        double integral = IntegrationTool.bivariateIntegral(function, 0., 1., 0., 1.);
        assertEquals(0.5, integral, err);
        integral = IntegrationTool.bivariateIntegral(function, 0., 1.5, 0., 1.5);
        assertEquals(1.125, integral, err);
        integral = IntegrationTool.bivariateIntegral(function, -1., 1., -1., 1.);
        assertEquals(2., integral, err);
        integral = IntegrationTool.bivariateIntegral(function, -1.5, 1.5, -1.5, 1.5);
        assertEquals(4.5, integral, err);
    }
    
    @Test
    public void testBivariateIntegralSymmetric() throws Exception {
        double err = 1E-2;
        CircularlySymmetricBivariateFunction function = new CircularlySymmetricBivariateFunction() {
            private UnivariateRealFunction projection = new UnivariateRealFunction() {
                @Override
                public double value(double x) throws FunctionEvaluationException {
                    return 0.5;
                }
            };
            @Override
            public double value(double r) throws FunctionEvaluationException {
                return 0.5;
            }
            @Override
            public UnivariateRealFunction projectionFunction() {
                return projection;
            }
            @Override
            public double value(double x, double y) throws FunctionEvaluationException {
                return 0.5;
            }
            @Override
            public UnivariateRealFunction polarFunction() {
                return new DefaultPolarFunction(projection);
            }
        };
        double integral = IntegrationTool.bivariateIntegral(function, 0., 1., 0., 1.);
        assertEquals(0.5, integral, err);
        integral = IntegrationTool.bivariateIntegral(function, 0., 1.5, 0., 1.5);
        assertEquals(1.125, integral, err);
        integral = IntegrationTool.bivariateIntegral(function, -1., 1., -1., 1.);
        assertEquals(2., integral, err);
        integral = IntegrationTool.bivariateIntegral(function, -1.5, 1.5, -1.5, 1.5);
        assertEquals(4.5, integral, err);
    }
    
    @Test
    public void testBivariateIntegralDefault() throws Exception {
        double err = 1E-4;
        BivariateRealFunction function = new BivariateRealFunction() {
            @Override
            public double value(double x, double y) throws FunctionEvaluationException {
                return 0.5;
            }
        };
        // Check that we get an exceptions if the limits are wrong
        try {
            IntegrationTool.bivariateIntegral(null, 5., 4., 4., 5.);
            fail("Bivariate integration didn't fail for invalid range of X.");
        } catch (IllegalArgumentException ex) {
        }
        try {
            IntegrationTool.bivariateIntegral(null, 4., 5., 6., 5.);
            fail("Bivariate integration didn't fail for invalid range of X.");
        } catch (IllegalArgumentException ex) {
        }
        // Check that we get zero if the limits are equal
        double integral = IntegrationTool.bivariateIntegral(function, 1., 1., 5., 6.);
        assertEquals(0., integral, 0.);
        integral = IntegrationTool.bivariateIntegral(function, 1., 2., 5., 5.);
        assertEquals(0., integral, 0.);
        // Check the values
        integral = IntegrationTool.bivariateIntegral(function, 0., 1., 0., 1.);
        assertEquals(0.5, integral, err);
        integral = IntegrationTool.bivariateIntegral(function, 0., 1.5, 0., 1.5);
        assertEquals(1.125, integral, err);
        integral = IntegrationTool.bivariateIntegral(function, -1., 1., -1., 1.);
        assertEquals(2., integral, err);
        integral = IntegrationTool.bivariateIntegral(function, -1.5, 1.5, -1.5, 1.5);
        assertEquals(4.5, integral, err);
        // Check also with a gaussian
        SymmetricBivariateGaussian gaussian = new SymmetricBivariateGaussian(0.5);
        integral = IntegrationTool.bivariateIntegral(gaussian, -5, 5, -5, 5);
        assertEquals(1., integral, 1E-3);
    }
}
