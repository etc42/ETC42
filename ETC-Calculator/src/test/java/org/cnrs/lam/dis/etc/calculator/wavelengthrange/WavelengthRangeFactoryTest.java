/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.wavelengthrange;

import java.util.Map;
import org.cnrs.lam.cesam.util.calculator.Calculator;
import org.cnrs.lam.dis.etc.calculator.DatasetProviderHolder;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.cnrs.lam.dis.etc.datamodel.ComponentInfo;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.cnrs.lam.dis.etc.datamodel.DatasetMockCreator;
import org.cnrs.lam.dis.etc.datamodel.DatasetProvider;
import org.cnrs.lam.dis.etc.datamodel.Instrument;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.javatuples.Pair;
import org.javatuples.Tuple;
import org.javatuples.Unit;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.BDDMockito.*;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class WavelengthRangeFactoryTest {
    
    @Mock Instrument instrument;
    @Mock Session session;
    @Mock DatasetProvider datasetProvider;
    @Mock Dataset filterTransmissionDataset;
    @Mock Dataset totalEfficiencyDataset;
    
    @Before
    public void setUp() throws Exception {
        // This is necessary to initialize all the mock annotated variables
        MockitoAnnotations.initMocks(this);
        
        filterTransmissionDataset = DatasetMockCreator.createFilterTransmission();
        when(datasetProvider.getDataset(eq(Dataset.Type.FILTER_TRANSMISSION), any(DatasetInfo.class))).thenReturn(filterTransmissionDataset);
        totalEfficiencyDataset = DatasetMockCreator.createTotalEfficiency();
        when(datasetProvider.getDataset(eq(Dataset.Type.TRANSMISSION), any(DatasetInfo.class))).thenReturn(totalEfficiencyDataset);
        DatasetProviderHolder.setDatasetProvider(datasetProvider);
        
        when(session.getInstrument()).thenReturn(instrument);
        when(instrument.getInfo()).thenReturn(new ComponentInfo("test_instrument", null));
        DatasetInfo filterInfo = filterTransmissionDataset.getInfo();
        when(instrument.getFilterTransmission()).thenReturn(filterInfo);
        DatasetInfo totalInfo = totalEfficiencyDataset.getInfo();
        when(instrument.getTransmission()).thenReturn(totalInfo);
        
        ResultsHolder.resetResults();
    }
    
    /**
     * Test that when we have imaging we get the Imaging calculator.
     * @throws Exception 
     */
    @Test
    public void testImaging() throws Exception {
        //given
        given(instrument.getInstrumentType()).willReturn(Instrument.InstrumentType.IMAGING);

        //when
        Calculator<Tuple, Pair<Double, Double>> calculator = new WavelengthRangeFactory().getCalculator(new Unit<Session>(session));
        
        //then
        assertNotNull(calculator);
        assertTrue(calculator instanceof Imaging);
        // Check that we got it at the results
        Map<String, CalculationResults.Result> results = ResultsHolder.getResults().getResults(CalculationResults.Level.DEBUG);
        CalculationResults.StringResult result = (CalculationResults.StringResult) results.get("WAVELENGTH_RANGE_METHOD");
        assertNotNull(result);
        assertEquals("Imaging", result.getValue());
    }

    /**
     * Test that when we have spectrograph we get the Spectroscopy calculator.
     */
    @Test
    public void testGetCalculator() throws Exception {
        //given
        given(instrument.getInstrumentType()).willReturn(Instrument.InstrumentType.SPECTROGRAPH);
        given(instrument.getRangeMin()).willReturn(1000.);
        given(instrument.getRangeMinUnit()).willReturn(Units.ANGSTROM);
        given(instrument.getRangeMax()).willReturn(2000.);
        given(instrument.getRangeMaxUnit()).willReturn(Units.ANGSTROM);
        
        //when
        Calculator<Tuple, Pair<Double, Double>> calculator = new WavelengthRangeFactory().getCalculator(new Unit<Session>(session));
        
        //then
        assertNotNull(calculator);
        assertTrue(calculator instanceof Spectroscopy);
        // Check that we got it at the results
        Map<String, CalculationResults.Result> results = ResultsHolder.getResults().getResults(CalculationResults.Level.DEBUG);
        CalculationResults.StringResult result = (CalculationResults.StringResult) results.get("WAVELENGTH_RANGE_METHOD");
        assertNotNull(result);
        assertEquals("Spectroscopy", result.getValue());
    }
}
