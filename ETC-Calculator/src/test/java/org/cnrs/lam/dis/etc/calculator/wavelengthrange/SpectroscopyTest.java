/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.wavelengthrange;

import java.text.MessageFormat;
import java.util.Map;
import java.util.ResourceBundle;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Result;
import org.javatuples.Pair;
import org.javatuples.Quartet;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author nikoapos
 */
public class SpectroscopyTest {
    
    private static final ResourceBundle validationErrorsBundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");
    
    @Before
    public void setUp() {
        ResultsHolder.resetResults();
    }

    /**
     * Test of validateConfiguration method, of class Spectroscopy.
     */
    @Test
    public void testValidateConfiguration() throws Exception {
        // Check that we do not get an exception if the min and max are positive,
        // they are expressed in Anstrom and the min is smaller than the max.
        Quartet<Double, String, Double, String> configuration =
                new Quartet<Double, String, Double, String>(5000., Units.ANGSTROM, 10000., Units.ANGSTROM);
        Spectroscopy instance = new Spectroscopy();
        instance.validateConfiguration(configuration);
        
        // Check that we get the correct exception if the minValue is negative
        try {
            configuration = new Quartet<Double, String, Double, String>(-5000., Units.ANGSTROM, 10000., Units.ANGSTROM);
            instance = new Spectroscopy();
            instance.validateConfiguration(configuration);
            fail("No exception thrown for negative minValue");
        } catch (ConfigurationException ex) {
            String expected = validationErrorsBundle.getString("RANGE_MIN_NEGATIVE");
            expected = MessageFormat.format(expected, -5000.);
            assertEquals(expected, ex.getMessage());
        }
        
        // Check that we get the correct exception if the maxValue is negative
        try {
            configuration = new Quartet<Double, String, Double, String>(5000., Units.ANGSTROM, -10000., Units.ANGSTROM);
            instance = new Spectroscopy();
            instance.validateConfiguration(configuration);
            fail("No exception thrown for negative maxValue");
        } catch (ConfigurationException ex) {
            String expected = validationErrorsBundle.getString("RANGE_MAX_NEGATIVE");
            expected = MessageFormat.format(expected, -10000.);
            assertEquals(expected, ex.getMessage());
        }
        
        // Check that we get the correct exception if the minValue is not in Angstrom
        try {
            configuration = new Quartet<Double, String, Double, String>(5000., Units.CM, 10000., Units.ANGSTROM);
            instance = new Spectroscopy();
            instance.validateConfiguration(configuration);
            fail("No exception thrown for minValue not in Angstrom");
        } catch (ConfigurationException ex) {
            String expected = validationErrorsBundle.getString("RANGE_MIN_WRONG_UNIT");
            expected = MessageFormat.format(expected, Units.ANGSTROM, Units.CM);
            assertEquals(expected, ex.getMessage());
        }
        
        // Check that we get the correct exception if the maxValue is not in Angstrom
        try {
            configuration = new Quartet<Double, String, Double, String>(5000., Units.ANGSTROM, 10000., Units.CM);
            instance = new Spectroscopy();
            instance.validateConfiguration(configuration);
            fail("No exception thrown for maxValue not in Angstrom");
        } catch (ConfigurationException ex) {
            String expected = validationErrorsBundle.getString("RANGE_MAX_WRONG_UNIT");
            expected = MessageFormat.format(expected, Units.ANGSTROM, Units.CM);
            assertEquals(expected, ex.getMessage());
        }
    }

    /**
     * Test of performCalculation method, of class Spectroscopy. It expects that
     * the output will have the same values with the configuration.
     */
    @Test
    public void testPerformCalculation() throws Exception {
        double minValue = 5000.;
        double maxValue = 10000.;
        Quartet<Double, String, Double, String> configuration =
                new Quartet<Double, String, Double, String>(minValue, Units.ANGSTROM, maxValue, Units.ANGSTROM);
        Spectroscopy instance = new Spectroscopy();
        instance.initialize(configuration);
        Pair<Double, Double> result = instance.performCalculation(null);
        assertEquals(minValue, result.getValue0(), 0.);
        assertEquals(maxValue, result.getValue1(), 0.);
    }

    /**
     * Test of performForEveryCalculation method, of class Spectroscopy. It expects
     * that the results WAVELENGTH_MIN and WAVELENGTH_MAX will be in the intermediate
     * unimportant results.
     */
    @Test
    public void testPerformForEveryCalculation() throws Exception {
        double minValue = 5000.;
        double maxValue = 10000.;
        Spectroscopy instance = new Spectroscopy();
        instance.performForEveryCalculation(null, new Pair<Double, Double>(minValue, maxValue));
        // Check for the min result
        Map<String, Result> results = ResultsHolder.getResults().getResults(CalculationResults.Level.INTERMEDIATE_UNIMPORTANT);
        CalculationResults.DoubleValueResult minResult = (CalculationResults.DoubleValueResult) results.get("WAVELENGTH_MIN");
        assertNotNull(minResult);
        assertEquals(minValue, minResult.getValue(), 0);
        assertEquals(Units.ANGSTROM, minResult.getUnit());
        // Check for the max result
        CalculationResults.DoubleValueResult maxResult = (CalculationResults.DoubleValueResult) results.get("WAVELENGTH_MAX");
        assertNotNull(maxResult);
        assertEquals(maxValue, maxResult.getValue(), 0);
        assertEquals(Units.ANGSTROM, maxResult.getUnit());
    }

    /**
     * Test of performForEveryRetrieval method, of class Spectroscopy. It expects
     * that the results RANGE_MIN and RANGE_MAX will be in the debug results.
     */
    @Test
    public void testPerformForEveryRetrieval() throws Exception {
        double minValue = 5000.;
        double maxValue = 10000.;
        Quartet<Double, String, Double, String> configuration =
                new Quartet<Double, String, Double, String>(minValue, Units.ANGSTROM, maxValue, Units.ANGSTROM);
        Spectroscopy instance = new Spectroscopy();
        instance.performForEveryRetrieval(configuration);
        // Check for the min result
        Map<String, Result> results = ResultsHolder.getResults().getResults(CalculationResults.Level.DEBUG);
        CalculationResults.DoubleValueResult minResult = (CalculationResults.DoubleValueResult) results.get("RANGE_MIN");
        assertNotNull(minResult);
        assertEquals(minValue, minResult.getValue(), 0);
        assertEquals(Units.ANGSTROM, minResult.getUnit());
        // Check for the max result
        CalculationResults.DoubleValueResult maxResult = (CalculationResults.DoubleValueResult) results.get("RANGE_MAX");
        assertNotNull(maxResult);
        assertEquals(maxValue, maxResult.getValue(), 0);
        assertEquals(Units.ANGSTROM, maxResult.getUnit());
    }
    
}
