/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.util.functions;

import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 */
public class ErrorFunctionTest {
    
    private static double[] x;
    private static double[] erf;

    @BeforeClass
    public static void setUpClass() throws Exception {
        x = new double[27];
        erf = new double[27];
        x[0] = -30;   erf[0] = -1.;
        x[1] = -1.2;  erf[1] = -0.910314;
        x[2] = -1.1;  erf[2] = -0.8802051;
        x[3] = -1.;   erf[3] = -0.8427008;
        x[4] = -0.9;  erf[4] = -0.7969082;
        x[5] = -0.8;  erf[5] = -0.742101;
        x[6] = -0.7;  erf[6] = -0.6778012;
        x[7] = -0.6;  erf[7] = -0.6038561;
        x[8] = -0.5;  erf[8] = -0.5204999;
        x[9] = -0.4;  erf[9] = -0.4283924;
        x[10] = -0.3; erf[10] = -0.3286268;
        x[11] = -0.2; erf[11] = -0.2227026;
        x[12] = -0.1; erf[12] = -0.1124629;
        x[13] = 0.;   erf[13] = 0.;
        x[14] = 30;   erf[14] = 1.;
        x[15] = 1.2;  erf[15] = 0.910314;
        x[16] = 1.1;  erf[16] = 0.8802051;
        x[17] = 1.;   erf[17] = 0.8427008;
        x[18] = 0.9;  erf[18] = 0.7969082;
        x[19] = 0.8;  erf[19] = 0.742101;
        x[20] = 0.7;  erf[20] = 0.6778012;
        x[21] = 0.6;  erf[21] = 0.6038561;
        x[22] = 0.5;  erf[22] = 0.5204999;
        x[23] = 0.4;  erf[23] = 0.4283924;
        x[24] = 0.3;  erf[24] = 0.3286268;
        x[25] = 0.2;  erf[25] = 0.2227026;
        x[26] = 0.1;  erf[26] = 0.1124629;
    }

    /**
     * Test of getCommonsMathErf method, of class ErrorFunction.
     */
    @Test
    public void testGetCommonsMathErf() throws Exception {
        double error = 1E-7;
        ErrorFunction commonsMathErf = ErrorFunction.getCommonsMathErf();
        for (int i = 0; i < x.length; i++) {
            assertEquals("Erf calculation failed for x = " + x[i], erf[i], commonsMathErf.value(x[i]), error);
        }
    }

    /**
     * Test of getTsayErf method, of class ErrorFunction.
     */
    @Test
    public void testGetTsayErf() throws Exception {
        double error = 1E-2;
        ErrorFunction tsayErf = ErrorFunction.getTsayErf();
        for (int i = 0; i < x.length; i++) {
            assertEquals("Erf calculation failed for x = " + x[i], erf[i], tsayErf.value(x[i]), error);
        }
    }
    
//    @Test
//    public void testTsaySpeed() throws Exception {
//        double expectedSpeed = 3.;
//        int repeats = 100000;
//        ErrorFunction commonsMathErf = ErrorFunction.getCommonsMathErf();
//        long time1 = System.nanoTime();
//        for (int i = 0; i < repeats; i++) {
//            commonsMathErf.value(1.*i/repeats);
//        }
//        long time2 = System.nanoTime();
//        long commonsMathTime = time2 - time1;
//        ErrorFunction tsayErf = ErrorFunction.getTsayErf();
//        time1 = System.nanoTime();
//        for (int i = 0; i < repeats; i++) {
//            tsayErf.value(1.*i/repeats);
//        }
//        time2 = System.nanoTime();
//        long tsayTime = time2 - time1;
//        double speed = 1. * commonsMathTime / tsayTime;
//        assertTrue("The Tsay method is not " + expectedSpeed + " times faster than the commons math method", speed > expectedSpeed);
//    }
}
