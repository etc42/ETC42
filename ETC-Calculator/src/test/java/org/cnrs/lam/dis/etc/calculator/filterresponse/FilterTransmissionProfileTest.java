/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.filterresponse;

import java.util.Collections;
import java.util.Map;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.dis.etc.calculator.DatasetProviderHolder;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.calculator.util.datasets.LinearFunctionDataset;
import org.cnrs.lam.dis.etc.calculator.util.functions.BoundedUnivariateFunction;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Result;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.cnrs.lam.dis.etc.datamodel.DatasetMockCreator;
import org.cnrs.lam.dis.etc.datamodel.DatasetProvider;
import org.javatuples.Pair;
import org.javatuples.Unit;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import static org.mockito.BDDMockito.*;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class FilterTransmissionProfileTest {
    
    @Mock private DatasetProvider datasetProvider;
    @Mock Dataset filterTransmissionDataset;
    
    @Before
    public void setUp() throws Exception {
        // This is necessary to initialize all the mock annotated variables
        MockitoAnnotations.initMocks(this);
        
        filterTransmissionDataset = DatasetMockCreator.createFilterTransmission();
        when(datasetProvider.getDataset(eq(Dataset.Type.FILTER_TRANSMISSION), any(DatasetInfo.class))).thenReturn(filterTransmissionDataset);
        DatasetProviderHolder.setDatasetProvider(datasetProvider);
        
        ResultsHolder.resetResults();
    }
    
    /**
     * Returns a valid configuration for the calculator to be used by the tests.
     * @return 
     */
    private Unit<DatasetInfo> validConfiguration() {
        return new Unit<DatasetInfo>(filterTransmissionDataset.getInfo());
    }
    
    /**
     * Test that a valid configuration does not throw an exception.
     */
    @Test
    public void testSuccessfulConfiguration() throws ConfigurationException {
        //when
        FilterTransmissionProfile instance = new FilterTransmissionProfile();
        instance.validateConfiguration(validConfiguration());
    }
    
    /**
     * Test that configuration with a null filter transmission throws an exception.
     * @throws ConfigurationException 
     */
    @Test(expected = ConfigurationException.class)
    public void testConfigurationNullFilterTransmission() throws ConfigurationException {
        //when
        FilterTransmissionProfile instance = new FilterTransmissionProfile();
        instance.validateConfiguration(new Unit<DatasetInfo>(null));
    }
    
    /**
     * Test that configuration when the filter transmission is missing throws
     * an exception.
     * @throws ConfigurationException 
     */
    @Test(expected = ConfigurationException.class)
    public void testConfigurationFilterTransmissionUnavailable() throws ConfigurationException {
        //given
        given(datasetProvider.getDataset(eq(Dataset.Type.FILTER_TRANSMISSION), any(DatasetInfo.class))).willReturn(null);
        
        //when
        FilterTransmissionProfile instance = new FilterTransmissionProfile();
        instance.validateConfiguration(validConfiguration());
    }
    
    /**
     * Test that when the transmission X axis is in wrong unit we get an exception.
     * @throws ConfigurationException 
     */
    @Test(expected = ConfigurationException.class)
    public void testConfigurationWrongUnitX() throws ConfigurationException {
        //given
        Dataset wrongXUnitDataset = DatasetMockCreator.createFilterTransmission();
        given(wrongXUnitDataset.getXUnit()).willReturn("wrong");
        given(datasetProvider.getDataset(eq(Dataset.Type.FILTER_TRANSMISSION), any(DatasetInfo.class))).willReturn(wrongXUnitDataset);
        
        //when
        FilterTransmissionProfile instance = new FilterTransmissionProfile();
        instance.validateConfiguration(validConfiguration());
    }
    
    /**
     * Test of performCalculation method, of class FilterTransmissionProfile.
     */
    @Test
    public void testPerformCalculation() throws Exception {
        //when
        FilterTransmissionProfile instance = new FilterTransmissionProfile();
        instance.initialize(validConfiguration());
        BoundedUnivariateFunction function = instance.performCalculation(null).getValue0();
        
        //then
        assertNotNull(function);
        // Check that the function has correct bounds
        Map<Double, Double> expectedData = filterTransmissionDataset.getData();
        double expectedMin = Collections.min(expectedData.keySet());
        double expectedMax = Collections.max(expectedData.keySet());
        Pair<Double, Double> bounds = function.getBounds();
        assertEquals(expectedMin, bounds.getValue0(), 0.00001);
        assertEquals(expectedMax, bounds.getValue1(), 0.00001);
        // Check that the function gives correct results
        for (Map.Entry<Double, Double> entry : expectedData.entrySet()) {
            Double key = entry.getKey();
            Double value = entry.getValue();
            assertEquals(value, function.value(key), 0.00001);
        }
    }

    /**
     * Test of performForEveryRetrieval method, of class FilterTransmissionProfile.
     */
    @Test
    public void testPerformForEveryRetrieval() {
        //when
        FilterTransmissionProfile instance = new FilterTransmissionProfile();
        instance.performForEveryRetrieval(validConfiguration());
        
        //then
        Map<String, Result> results = ResultsHolder.getResults().getResults(CalculationResults.Level.DEBUG);
        CalculationResults.StringResult result = (CalculationResults.StringResult) results.get("FILTER_TRANSMISSION_PROFILE");
        assertNotNull(result);
        assertEquals("test_instrument.filter_transmission", result.getValue());
    }

    /**
     * Test of performForEveryCalculation method, of class FilterTransmissionProfile.
     */
    @Test
    public void testPerformForEveryCalculation() throws Exception {
        //when
        FilterTransmissionProfile instance = new FilterTransmissionProfile();
        instance.initialize(validConfiguration());
        instance.performForEveryCalculation(null, instance.calculate(null));

        //then
        // Check for the filter response result
        Map<String, Result> results = ResultsHolder.getResults().getResults(CalculationResults.Level.INTERMEDIATE_IMPORTANT);
        CalculationResults.DoubleDatasetResult result = (CalculationResults.DoubleDatasetResult) results.get("FILTER_RESPONSE");
        assertNotNull(result);
        assertEquals(Units.ANGSTROM, result.getXUnit());
        assertEquals(null, result.getYUnit());
        // Check that the data are the expected ones
        LinearFunctionDataset resultFunction = new LinearFunctionDataset(result.getValues());
        Map<Double, Double> expectedData = filterTransmissionDataset.getData();
        for (Map.Entry<Double, Double> entry : expectedData.entrySet()) {
            Double key = entry.getKey();
            Double value = entry.getValue();
            assertEquals(value, resultFunction.value(key), 0.00001);
        }
    }
    
}
