/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.psfsize;

import java.text.MessageFormat;
import java.util.ResourceBundle;
import lombok.EqualsAndHashCode;
import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.analysis.BivariateRealFunction;
import org.apache.log4j.Logger;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.Cacheable;
import org.cnrs.lam.cesam.util.calculator.CachingPolicy;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.Calculator;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.calculator.util.functions.CircularlySymmetricBivariateFunction;
import org.cnrs.lam.dis.etc.calculator.util.integration.IntegrationTool;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.javatuples.Pair;
import org.javatuples.Unit;

/**
 * <p>The {@link Percentage} class is a calculator for computing the size
 * of the PSF as the diameter of the circle in which the PSF contains a specific
 * percentage of the flux.</p>
 * 
 * <p>The configuration of the calculator is a {@link org.javatuples.Pair},
 * with contains the following elements:</p>
 * <ol>
 *   <li>{@link Double}: The flux percentage</li>
 *   <li>{@link Calculator}: a calculator for retrieving the PSF 2D functions</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the wavelength for which the calculation
 * is done (expressed in {@latex.inline \\AA}).</p>
 * 
 * <p>The output of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link Double} representing the size of the PSF expressed in
 * {@latex.inline $arcsec$}.</p>
 */
// Caching explenation
// -------------------
// This calculator is set to cache everything because the calculation performs
// integrations, which is time consuming.
@Cacheable(CachingPolicy.ALL)
@EqualsAndHashCode(callSuper=false)
public class Percentage extends AbstractCalculator<
        Pair<Double, Calculator<Unit<Double>, Unit<BivariateRealFunction>>>,
        Unit<Double>, Unit<Double>> {
    
    private static final Logger logger = Logger.getLogger(Percentage.class);
    private static final ResourceBundle validationErrorsBundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");
    private double percentage;
    private Calculator<Unit<Double>, Unit<BivariateRealFunction>> psfCalculator;

    /**
     * Validates the given configuration. It requires that the PSF percentage
     * is in the range (0,1) (both excluded).
     * @param configuration the configuration to validate
     * @throws ConfigurationException if the configuration is invalid
     */
    @Override
    protected void validateConfiguration(Pair<Double,
            Calculator<Unit<Double>, Unit<BivariateRealFunction>>> configuration)
            throws ConfigurationException {
        if (configuration.getValue0() <= 0 || configuration.getValue0() >= 1) {
            String message = validationErrorsBundle.getString("PSF_SIZE_PERCENTAGE_OUT_OF_RANGE");
            message = MessageFormat.format(message, configuration.getValue0());
            throw new ConfigurationException(message);
        }
    }

    @Override
    protected void initialize(Pair<Double,
            Calculator<Unit<Double>, Unit<BivariateRealFunction>>> configuration)
            throws InitializationException {
        percentage = configuration.getValue0();
        psfCalculator = configuration.getValue1();
    }

    @Override
    protected Unit<Double> performCalculation(Unit<Double> input) throws CalculationException {
        BivariateRealFunction psfFunction = psfCalculator.calculate(input).getValue0();
        if (!(psfFunction instanceof CircularlySymmetricBivariateFunction)) {
            throw new CalculationException("Percentage PSF size is impelmented only for "
                    + "circularly symmetric PSFs");
        }
        CircularlySymmetricBivariateFunction psf = (CircularlySymmetricBivariateFunction) psfFunction;
        double size = 0;
        try {
            double low = 0;
            double high = 1;
            while (IntegrationTool.bivariateIntegral(psf, high) < percentage) {
                high = high * 2;
            }
            size = 2 * findRadiusInRange(low, high, psf, percentage, 1E-10);
        } catch (FunctionEvaluationException ex) {
            logger.error(ex.getMessage(), ex);
            throw new CalculationException(ex.getMessage());
        }
        return new Unit<Double>(size);
    }

    /**
     * It recursively finds the radius of a circle in the range [low,high],for
     * which the psf function has integral equal with the percentage, with
     * accuracy er.
     * @param low
     * @param high
     * @param psf
     * @param percentage
     * @param er
     * @return 
     */
    private double findRadiusInRange(double low, double high,
            CircularlySymmetricBivariateFunction psf, double percentage, double er)
            throws FunctionEvaluationException {
        double middle = (high + low) / 2;
        if (Math.abs(high - low) < er) {
            return middle;
        }
        double middleIntegral = IntegrationTool.bivariateIntegral(psf, middle);
        if (middleIntegral > percentage) {
            return findRadiusInRange(low, middle, psf, percentage, er);
        } else {
            return findRadiusInRange(middle, high, psf, percentage, er);
        }
    }

    /**
     * <p>For every calculation the calculator adds in the simulation results
     * the following outputs of the calculator:</p>
     * <ul>
     *   <li>The size of the PSF in intermediate unimportant results
     *       with code PSF_SIZE</li>
     * </ul>
     * @param input
     * @param output 
     */
    @Override
    protected void performForEveryCalculation(Unit<Double> input, Unit<Double> output) {
        ResultsHolder.getResults().addResult("PSF_SIZE", input.getValue0(), output.getValue0(), 
                Units.ANGSTROM, Units.ARCSEC, CalculationResults.Level.INTERMEDIATE_UNIMPORTANT);
    }
    /**
     * <p>For every calculator instantiation the following parts of its configuration
     * are added in the simulation results:</p>
     * <ul>
     *   <li>The PSF size flux percentage in debug results with code PSF_SIZE_PERCENTAGE</li>
     * </ul>
     * @param configuration The configuration of the calculator
     */

    @Override
    protected void performForEveryRetrieval(Pair<Double,
            Calculator<Unit<Double>, Unit<BivariateRealFunction>>> configuration) {
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("PSF_SIZE_PERCENTAGE",
                configuration.getValue0(), null), CalculationResults.Level.DEBUG);
    }

}
