/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.numberofpixels;

import java.util.ResourceBundle;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.sourcepixelcoverage.SourcePixelCoverageFactory;
import org.cnrs.lam.dis.etc.calculator.spatialbinning.SpatialBinningFactory;
import org.cnrs.lam.dis.etc.calculator.spectralbinning.SpectralBinningFactory;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.StringResult;
import org.cnrs.lam.dis.etc.datamodel.Image;
import org.cnrs.lam.dis.etc.datamodel.Instrument;
import org.cnrs.lam.dis.etc.datamodel.ObsParam;
import org.cnrs.lam.dis.etc.datamodel.ObsParam.SpectralQuantumType;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.javatuples.Triplet;
import org.javatuples.Unit;

/**
 * <p>The {@code NumberOfPixelsFactory} provides calculators for calculating the 
 * the number of active pixels (affected by the source light). The configuration
 * of the factory is a {@link org.cnrs.lam.dis.etc.datamodel.Session} object
 * containing all the configuration from the user.</p>
 */
public class NumberOfPixelsFactory implements Factory<Unit<Session>, Unit<Double>, Unit<Double>> {

    private static final ResourceBundle bundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");

    @Override
    public Calculator<Unit<Double>, Unit<Double>> getCalculator(Unit<Session> configuration) throws InitializationException, ConfigurationException {
        Session session = configuration.getValue0();
        Instrument instrument = session.getInstrument();
        ObsParam obsParam = session.getObsParam();
        
        switch (instrument.getInstrumentType()) {
            
            case IMAGING:
                ResultsHolder.getResults().addResult(
                        new StringResult("NUMBER_OF_PIXELS_METHOD", "Photometry"), Level.DEBUG);
                Calculator<Unit<Double>, Unit<Image<Byte>>> sourcePixelCoverage
                        = new SourcePixelCoverageFactory().getCalculator(configuration);
                Unit<Calculator<Unit<Double>, Unit<Image<Byte>>>> imagingConfiguration
                        = new Unit(sourcePixelCoverage);
                return EtcCalculatorManager.getManager(Photometry.class).getCalculator(imagingConfiguration);
            case SPECTROGRAPH:
                ResultsHolder.getResults().addResult(
                        new StringResult("NUMBER_OF_PIXELS_METHOD", "Spectroscopy"), Level.DEBUG);
                // The spectroscopy number of pixels requires as input the following:
                // 1: A Calculator for the spatial binning
                // 2: A Calculator for the spectral binning
                // 3: A boolean showing if we have calculation per spectral pixel
                //    or per spectral resolution element
                Calculator<Unit<Double>, Unit<Double>> spatialBinning =
                        new SpatialBinningFactory().getCalculator(configuration);
                Calculator<Unit<Double>, Unit<Double>> spectralBinning =
                        new SpectralBinningFactory().getCalculator(configuration);
                boolean isPerSpectralResolutionElement = 
                        obsParam.getSpectralQuantumType() == SpectralQuantumType.SPECTRAL_RESOLUTION_ELEMENT;
                Triplet<Calculator<Unit<Double>, Unit<Double>>, Calculator<Unit<Double>, Unit<Double>>, Boolean> spectroscopyConfiguration
                        = new Triplet(spatialBinning, spectralBinning, isPerSpectralResolutionElement);
                return EtcCalculatorManager.getManager(Spectroscopy.class).getCalculator(spectroscopyConfiguration);
        }
        String message = bundle.getString("UNKNOWN_NUMBER_OF_PIXELS_RANGE_METHOD");
        throw new ConfigurationException(message);
    }
    
}
