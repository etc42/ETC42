/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.psfsize;

import java.util.ResourceBundle;
import org.apache.commons.math.analysis.BivariateRealFunction;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.psf.PsfFactory;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.cnrs.lam.dis.etc.datamodel.Instrument;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.javatuples.Pair;
import org.javatuples.Unit;

/**
 *<p>The {@link PsfSizeFactory} provides calculators for calculating the PSF size,
 * expressed in {@latex.inline $arcsec$}, for given wavelength values, expressed
 * in {@latex.inline $\\AA$}. The configuration of the
 * factory is a {@link org.cnrs.lam.dis.etc.datamodel.Session} object containing
 * all the configuration from the user.</p>
 */
public class PsfSizeFactory implements Factory<Unit<Session>, Unit<Double>, Unit<Double>> {
 
    private static final ResourceBundle validationErrorsBundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");

    @Override
    public Calculator<Unit<Double>, Unit<Double>> getCalculator(Unit<Session> configuration) throws InitializationException, ConfigurationException {
        Instrument instrument = configuration.getValue0().getInstrument();
        // Get the PSF calculator, which we need for many of the cases
        Calculator<Unit<Double>, Unit<BivariateRealFunction>> psfCalculator = new PsfFactory().getCalculator(configuration);
        
        switch (instrument.getPsfSizeType()) {
            case AUTO:
                // If we have auto or profile PSF type we have a single Gaussian. If we
                // have adaprive optics we have a double Gaussian.
                switch (configuration.getValue0().getInstrument().getPsfType()) {
                    case AUTO:
                    case PROFILE:
                        ResultsHolder.getResults().addResult( new CalculationResults.StringResult(
                            "PSF_SIZE_METHOD", "Symmetric Gaussian"), CalculationResults.Level.DEBUG);
                        return EtcCalculatorManager.getManager(SymmetricGaussian.class).getCalculator(
                                new Unit<Calculator<Unit<Double>, Unit<BivariateRealFunction>>>(psfCalculator));
                    case AO:
                    case DOUBLE_GAUSSIAN:
                        ResultsHolder.getResults().addResult( new CalculationResults.StringResult(
                            "PSF_SIZE_METHOD", "Symmetric double Gaussian"), CalculationResults.Level.DEBUG);
                        return EtcCalculatorManager.getManager(SymmetricDoubleGaussian.class).getCalculator(
                                new Unit<Calculator<Unit<Double>, Unit<BivariateRealFunction>>>(psfCalculator));
                }
                break;
            case FIXED:
                ResultsHolder.getResults().addResult( new CalculationResults.StringResult(
                    "PSF_SIZE_METHOD", "Fixed PSF size"), CalculationResults.Level.DEBUG);
                return EtcCalculatorManager.getManager(FixedSize.class).getCalculator(
                        new Pair<Double, String>(instrument.getFixedPsfSize(), instrument.getFixedPsfSizeUnit()));
            case FUNCTION:
                ResultsHolder.getResults().addResult( new CalculationResults.StringResult(
                    "PSF_SIZE_METHOD", "PSF size function"), CalculationResults.Level.DEBUG);
                return EtcCalculatorManager.getManager(SizeFunction.class).getCalculator(
                        new Unit<DatasetInfo>(instrument.getPsfSizeFunction()));
            case PERCENTAGE:
                ResultsHolder.getResults().addResult( new CalculationResults.StringResult(
                    "PSF_SIZE_METHOD", "PSF size Flux percentage"), CalculationResults.Level.DEBUG);
                return EtcCalculatorManager.getManager(Percentage.class).getCalculator(
                        new Pair<Double, Calculator<Unit<Double>, Unit<BivariateRealFunction>>>
                        (instrument.getPsfSizePercentage(), psfCalculator));
        }
        String message = validationErrorsBundle.getString("UNKNOWN_PSF_SIZE_METHOD");
        throw new ConfigurationException(message);
    }
    
}
