/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.oldconvolutionkernel;

import lombok.EqualsAndHashCode;
import org.apache.log4j.Logger;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.Cacheable;
import org.cnrs.lam.cesam.util.calculator.CachingPolicy;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.Calculator;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.configuration.ConfigFactory;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.StringResult;
import org.javatuples.Triplet;
import org.javatuples.Unit;

/**
 */
@EqualsAndHashCode(callSuper=false)
public class Spectroscopy extends AbstractCalculator<Triplet<Calculator<Unit<Double>, Unit<Double>>, Calculator<Unit<Double>, Unit<Double>>, Boolean>, Unit<Double>, Unit<Double>> {

    private static final Logger logger = Logger.getLogger(Spectroscopy.class);
    
    private Calculator<Unit<Double>, Unit<Double>> spectralBinningCalculator;
    private Calculator<Unit<Double>, Unit<Double>> spectralResolutionCalculator;
    private boolean isPerSpectralResolutionElement;
    private boolean forcePerSpectralResolutionElement;

    @Override
    protected void validateConfiguration(Triplet<Calculator<Unit<Double>, Unit<Double>>, Calculator<Unit<Double>, Unit<Double>>, Boolean> configuration) throws ConfigurationException {
    }

    /**
     * Initializes the calculator instance with the given configuration. The first
     * value of the configuration is the spectral binning calculator, the second is
     * the spectral resolution dataset info, the third is a dataset calculator
     * representing the spectral resolution and the fourth one is true if the
     * calculation is per spectral resolution element and false if it is per
     * spectral pixel.
     * @param configuration the configuration to use for initialization
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(Triplet<Calculator<Unit<Double>, Unit<Double>>, Calculator<Unit<Double>, Unit<Double>>, Boolean> configuration) throws InitializationException {
        spectralBinningCalculator = configuration.getValue0();
        spectralResolutionCalculator = configuration.getValue1();
        isPerSpectralResolutionElement = configuration.getValue2();
        forcePerSpectralResolutionElement = ConfigFactory.getConfig().isPerSpectralResolutionElementForced();

    }

    /**
     * Returns a {@link org.javatuples.Unit} containing the delta lambda,
     * expressed in {@latex.inline \\AA}.
     * @param input The wavelength expressed in {@latex.inline \\AA}
     * @return The delta lambda expressed in {@latex.inline \\AA}
     * @throws CalculationException  if there is any error during the calculation
     */
    @Override
    protected Unit<Double> performCalculation(Unit<Double> input) throws CalculationException {
        double result;
        double pixels = (forcePerSpectralResolutionElement || isPerSpectralResolutionElement)
                ? spectralBinningCalculator.calculate(input).getValue0() : 1;
        double spectralResolution = spectralResolutionCalculator.calculate(input).getValue0();
        result = (input.getValue0() / spectralResolution) * pixels;
        return new Unit<Double>(result);
    }

    /**
     * Overridden to add in the results the delta lambda. This is done here, so
     * we have this results even if the result is retrieved from the cache.
     * @param input The wavelength expressed in {@latex.inline \\AA}
     * @param output The delta lambda expressed in {@latex.inline \\AA}
     */
    @Override
    protected void performForEveryCalculation(Unit<Double> input, Unit<Double> output) {
        ResultsHolder.getResults().addResult("CONVOLUTION_KERNEL", input.getValue0(),
                output.getValue0(), Units.ANGSTROM, Units.ANGSTROM, Level.INTERMEDIATE_UNIMPORTANT);
    }

    /**
     * Overridden to add in the results the spectral resolution and the spectral
     * quantum.
     * @param configuration The configuration of the calculator
     */
    @Override
    protected void performForEveryRetrieval(Triplet<Calculator<Unit<Double>, Unit<Double>>, Calculator<Unit<Double>, Unit<Double>>, Boolean> configuration) {
        String spectralQuantum = configuration.getValue2()
                ? "Spectral resolution element"
                : "Spectral pixel";
        ResultsHolder.getResults().addResult(new StringResult("SPECTRAL_QUANTUM"
                , spectralQuantum), Level.DEBUG);
    }
}
