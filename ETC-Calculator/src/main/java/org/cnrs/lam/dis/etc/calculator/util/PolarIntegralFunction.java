/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.util;

import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.Calculator;
import org.javatuples.Unit;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class PolarIntegralFunction implements UnivariateRealFunction {

    private UnivariateRealFunction function;

    public PolarIntegralFunction(UnivariateRealFunction function) {
        this.function = function;
    }
    
    public PolarIntegralFunction(final Calculator<Unit<Double>, Unit<Double>> calculator) {
        this.function = new UnivariateRealFunction() {
            @Override
            public double value(double x) throws FunctionEvaluationException {
                Unit<Double> input = new Unit<Double>(x);
                Unit<Double> output = null;
                try {
                    output = calculator.calculate(input);
                } catch (CalculationException e) {
                    throw new FunctionEvaluationException(e, x);
                }
                return output.getValue0();
            }
        };
    }

    @Override
    public double value(double r) throws FunctionEvaluationException {
        return function.value(r) * r;
    }

}
