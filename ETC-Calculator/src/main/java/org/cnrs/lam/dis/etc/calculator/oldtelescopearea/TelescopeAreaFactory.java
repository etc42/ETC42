/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.oldtelescopearea;

import org.cnrs.lam.dis.etc.calculator.oldtelescopearea.DiameterObstruction;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.StringResult;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.javatuples.Triplet;
import org.javatuples.Unit;

/**
 * <p>The {@code TelescopeAreaFactory} provides calculators for calculating the total
 * affective area of the primary mirror (the area which reflects light). The retrieved
 * calculator gets as input the wavelength for which the calculation is done (expressed
 * in {@latex.inline \\AA}) and returns the telescope area expressed in 
 * {@latex.inline $cm^2$}. The configuration of the factory is a
 * {@link org.cnrs.lam.dis.etc.datamodel.Session} object containing all the
 * configuration from the user.</p>
 *
 * @author Nikolaos Apostolakos
 */
public class TelescopeAreaFactory implements Factory<Unit<Session>, Unit<Double>, Unit<Double>> {

    /**
     * Returns a calculator for computing the telescope area, based on the given
     * configuration.
     * @param configuration The session object containing all the user configuration
     * @return The calculator instance
     * @throws InitializationException if there is any problem with initializing
     * the calculator
     * @throws ConfigurationException if the configuration is invalid
     */
    @Override
    public Calculator<Unit<Double>, Unit<Double>> getCalculator(Unit<Session> configuration) 
            throws InitializationException, ConfigurationException {
        // We currently have only the DiameterObstruction calculator
        ResultsHolder.getResults().addResult(
                new StringResult("TELESCOPE_AREA_METHOD", "Diameter and Obstruction"), Level.DEBUG);
        double diameter = configuration.getValue0().getInstrument().getDiameter();
        String diameterUnit = configuration.getValue0().getInstrument().getDiameterUnit();
        double obstruction = configuration.getValue0().getInstrument().getObstruction();
        Triplet<Double, String, Double> calculatorConfiguration = 
                new Triplet<Double, String, Double>(diameter, diameterUnit, obstruction);
        return EtcCalculatorManager.getManager(DiameterObstruction.class).getCalculator(calculatorConfiguration);
    }
    
}
