/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.galacticflux;

import java.text.MessageFormat;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TreeMap;
import lombok.EqualsAndHashCode;
import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.apache.log4j.Logger;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.DatasetProviderHolder;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.FunctionToDatasetResultConverter;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.calculator.util.datasets.TemplateFunctionDataset;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.DoubleDatasetResult;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.javatuples.Tuple;
import org.javatuples.Unit;

/**
 * <p>The {@link FluxTemplate} class is a calculator for computing the 
 * galactic flux from a dataset. The configuration of the calculator
 * is a {@link org.javatuples.Unit} which contains the following elements:</p>
 * <ol>
 *   <li>{@link DatasetInfo}: the information of the galactic flux template</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Tuple}, as this
 * calculator does not accept any input. It should always be null.</p>
 * 
 * <p>The output of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link UnivariateRealFunction} representing the galactic flux.</p>
 */
// Caching explenation
// -------------------
// This calculator is set to cache the instances because the configuration validation
// and the instantiation are retrieving the dataset, which is time consuming.
@Cacheable(CachingPolicy.INSTANCE)
@EqualsAndHashCode(callSuper=false, exclude={"fluxFunction"})
public class FluxTemplate extends AbstractCalculator<Unit<DatasetInfo>, Tuple, Unit<UnivariateRealFunction>> {
    
    private static final Logger logger = Logger.getLogger(FluxTemplate.class);
    private static final ResourceBundle validationErrorsBundle =
            ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");
    private DatasetInfo templateDatasetInfo;
    private TemplateFunctionDataset fluxFunction;

    /**
     * Validates the configuration. It requires that the galactic flux
     * template exists, its X axis unit is Angstrom and its Y axis
     * unit is erg/cm2/sec/Angstrom/arcsec2.
     * @param configuration
     * @throws ConfigurationException 
     */
    @Override
    protected void validateConfiguration(Unit<DatasetInfo> configuration) throws ConfigurationException {
        // Check the template dataset
        DatasetInfo info = configuration.getValue0();
        if (info == null) {
            String message = validationErrorsBundle.getString("GALACTIC_TEMPLATE_MISSING");
            throw new ConfigurationException(message);
        }
        Dataset dataset = DatasetProviderHolder.getDatasetProvider().getDataset(Dataset.Type.GALACTIC, info);
        if (dataset == null) {
            String message = validationErrorsBundle.getString("GALACTIC_TEMPLATE_NOT_AVAILABLE");
            message = MessageFormat.format(message, info);
            throw new ConfigurationException(message);
        }
        if (!Units.isAngstrom(dataset.getXUnit())) {
            String message = validationErrorsBundle.getString("GALACTIC_TEMPLATE_WRONG_X_UNIT");
            message = MessageFormat.format(message, Units.ANGSTROM, dataset.getXUnit());
            throw new ConfigurationException(message);
        }
        if (dataset.getDataType() == Dataset.DataType.FUNCTION && !Units.isErgPerCm2PerSecPerAngstromPerArcsec2(dataset.getYUnit())) {
            String message = validationErrorsBundle.getString("GALACTIC_TEMPLATE_WRONG_Y_UNIT");
            message = MessageFormat.format(message, Units.getErgPerCm2PerSecPerAngstromPerArcsec2(), dataset.getYUnit());
            throw new ConfigurationException(message);
        }
        if (dataset.getDataType() == Dataset.DataType.TEMPLATE && !Units.isErgPerCm2PerSecPerArcsec2(dataset.getYUnit())) {
            String message = validationErrorsBundle.getString("GALACTIC_TEMPLATE_WRONG_Y_UNIT");
            message = MessageFormat.format(message, Units.getErgPerCm2PerSecPerArcsec2(), dataset.getYUnit());
            throw new ConfigurationException(message);
        }
    }

    /**
     * Initializes the calculator instance with the given configuration.
     * @param configuration the configuration to use for initialization
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(Unit<DatasetInfo> configuration) throws InitializationException {
        templateDatasetInfo = configuration.getValue0();
        Dataset dataset = DatasetProviderHolder.getDatasetProvider().getDataset(Dataset.Type.GALACTIC, templateDatasetInfo);
        fluxFunction = new TemplateFunctionDataset(dataset.getData());
    }

    /**
     * Returns a {@link UnivariateRealFunction} which can be used to calculate
     * the galactic flux.
     * @param input The calculator has no input, so it can be always null
     * @return A {@link UnivariateRealFunction} which can be used to calculate
     * the galactic flux
     * @throws CalculationException if there is any error during the calculation
     */
    @Override
    protected Unit<UnivariateRealFunction> performCalculation(Tuple input) throws CalculationException {
        return new Unit<UnivariateRealFunction>(fluxFunction);
    }

    /**
     * <p>For every calculation the calculator adds in the simulation results
     * the following outputs of the calculator:</p>
     * <ul>
     *   <li>The galactic flux in intermediate unimportant results with code
     *       GALACTIC_FLUX</li>
     * </ul>
     * @param input
     * @param output 
     */
    @Override
    protected void performForEveryCalculation(Tuple input, Unit<UnivariateRealFunction> output) {
        // If the result already exists we do not add it to save time
        if (ResultsHolder.getResults().getResultByName("GALACTIC_FLUX") != null) {
            return;
        }
        UnivariateRealFunction function = output.getValue0();
        DoubleDatasetResult result = FunctionToDatasetResultConverter.convert(function,
                "GALACTIC_FLUX", Units.ANGSTROM, Units.getErgPerCm2PerSecPerAngstromPerArcsec2());
        ResultsHolder.getResults().addResult(result, CalculationResults.Level.INTERMEDIATE_UNIMPORTANT);
    }

    /**
     * <p>For every calculator instantiation the following parts of its configuration
     * are added in the simulation results:</p>
     * <ul>
     *   <li>The galactic template name in debug results with
     *       code GALACTIC_FLUX_PROFILE</li>
     * </ul>
     * @param configuration 
     */
    @Override
    protected void performForEveryRetrieval(Unit<DatasetInfo> configuration) {
        ResultsHolder.getResults().addResult(new CalculationResults.StringResult("GALACTIC_FLUX_PROFILE"
                , configuration.getValue0().toString()), CalculationResults.Level.DEBUG);
    }
    
}
