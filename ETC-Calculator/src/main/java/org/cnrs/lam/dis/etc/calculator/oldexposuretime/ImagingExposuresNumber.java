/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.oldexposuretime;

import java.util.Map;
import java.util.TreeMap;
import lombok.EqualsAndHashCode;
import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.MaxIterationsExceededException;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.Cacheable;
import org.cnrs.lam.cesam.util.calculator.CachingPolicy;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.Calculator;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.CalculationError;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.IntegrationUtil;
import org.cnrs.lam.dis.etc.calculator.util.InterpolatableDataset;
import org.cnrs.lam.dis.etc.calculator.util.Quadratic;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.DoubleValueResult;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.LongValueResult;
import org.javatuples.Octet;
import org.javatuples.Pair;
import org.javatuples.Tuple;
import org.javatuples.Unit;

/**
 */
@Cacheable(CachingPolicy.ALL)
@EqualsAndHashCode(callSuper=false, exclude={"minLambda", "maxLambda"})
public class ImagingExposuresNumber extends AbstractCalculator<
                Octet<
                      Calculator<Tuple, Pair<Double, Double>>,
                      Calculator<Unit<Double>, Unit<Double>>,
                      Calculator<Unit<Double>, Unit<Double>>,
                      Calculator<Unit<Double>, Unit<Double>>,
                      Pair<Double, String>,
                      Pair<Double, String>,
                      Integer,
                      Double
                >, Tuple, Unit<Double>> {
    
    private Calculator<Tuple, Pair<Double, Double>> wavelengthRange;
    private Calculator<Unit<Double>, Unit<Double>> signal;
    private Calculator<Unit<Double>, Unit<Double>> numberOfPixels;
    private Calculator<Unit<Double>, Unit<Double>> backgroundNoise;
    private double dark;
    private double readout;
    private int nExpo;
    private double fixedSnr;
    
    private double minLambda;
    private double maxLambda;

    @Override
    protected void validateConfiguration(Octet<
                      Calculator<Tuple, Pair<Double, Double>>,
                      Calculator<Unit<Double>, Unit<Double>>,
                      Calculator<Unit<Double>, Unit<Double>>,
                      Calculator<Unit<Double>, Unit<Double>>,
                      Pair<Double, String>,
                      Pair<Double, String>,
                      Integer,
                      Double> configuration) throws ConfigurationException {
        // Check the dark current
        Pair<Double, String> darkPair = configuration.getValue4();
        if (darkPair.getValue0() < 0) {
            throw new ConfigurationException("Dark current must be non negative "
                    + "but was " + darkPair.getValue0());
        }
        if (!Units.isElectronsPerPixelPerSec(darkPair.getValue1())) {
             throw new ConfigurationException("Dark current must be in " + Units.getElectronsPerPixelPerSec()
                    + " but was in " + darkPair.getValue1());
        }
        // Check the readout noise
        Pair<Double, String> readoutPair = configuration.getValue5();
        if (readoutPair.getValue0() < 0) {
            throw new ConfigurationException("Readout noise must be non negative "
                    + "but was " + readoutPair.getValue0());
        }
        if (!Units.isElectronsPerPixel(readoutPair.getValue1())) {
             throw new ConfigurationException("Readout noise must be in " + Units.getElectronsPerPixel()
                    + " but was in " + readoutPair.getValue1());
        }
        // Check the number of exposures
        if (configuration.getValue6() < 1) {
            throw new ConfigurationException("Number of exposures must be at least 1 "
                    + "but was " + configuration.getValue6());
        }
        // Check the SNR
        if (configuration.getValue7() <= 0) {
            throw new ConfigurationException("Fixed SNR must be non negative "
                    + "but was " + darkPair.getValue0());
        }
    }

    @Override
    protected void initialize(Octet<
                      Calculator<Tuple, Pair<Double, Double>>,
                      Calculator<Unit<Double>, Unit<Double>>,
                      Calculator<Unit<Double>, Unit<Double>>,
                      Calculator<Unit<Double>, Unit<Double>>,
                      Pair<Double, String>,
                      Pair<Double, String>,
                      Integer,
                      Double> configuration) throws InitializationException {
        wavelengthRange = configuration.getValue0();
        signal = configuration.getValue1();
        numberOfPixels = configuration.getValue2();
        backgroundNoise = configuration.getValue3();
        dark = configuration.getValue4().getValue0();
        readout = configuration.getValue5().getValue0();
        nExpo = configuration.getValue6();
        fixedSnr = configuration.getValue7();
        Pair<Double, Double> minMax = null;
        try {
            minMax = wavelengthRange.calculate(null);
        } catch (CalculationException e) {
            throw new InitializationException("Failed to calculate the wavelength range", e);
        }
        minLambda = minMax.getValue0();
        maxLambda = minMax.getValue1();
    }

    @Override
    protected Unit<Double> performCalculation(Tuple input) throws CalculationException {
        double s = calculateSignalIntegral();
        double bg = calculateBackgroundNoiseIntegral();
        double nPix = calculateNumberOfPixels();
        double a = s * s;
        double b = -1 * fixedSnr * fixedSnr * (s + bg + nPix * dark);
        double c = -1 * fixedSnr * fixedSnr * nPix * nExpo * readout * readout;
        double exposureTime = 0;
        for (Double time : Quadratic.solve(a, b, c)) {
            if (exposureTime < time) {
                exposureTime = time;
            }
        }
        return new Unit<Double>(exposureTime);
    }

    private double calculateSignalIntegral() throws CalculationException {
        return IntegrationUtil.templateIntegral(signal, minLambda, maxLambda);
    }

    private double calculateBackgroundNoiseIntegral() throws CalculationException {
        return IntegrationUtil.templateIntegral(backgroundNoise, minLambda, maxLambda);
    }

    private double calculateNumberOfPixels() throws CalculationException {
        int minLambdaInt = (int) Math.floor(minLambda);
        int maxLambdaInt = (int) Math.ceil(maxLambda);
        double result = 0;
        for (int lambda = minLambdaInt; lambda <= maxLambdaInt; lambda++) {
            Unit<Double> input = new Unit<Double>((double) lambda);
            double noOfPixels = numberOfPixels.calculate(input).getValue0();
            if (noOfPixels > result)
                result = noOfPixels;
        }
        return result;
    }

    @Override
    protected void performForEveryCalculation(Tuple input, Unit<Double> output) {
        ResultsHolder.getResults().addResult(new DoubleValueResult("CALCULATED_EXPOSURE_TIME",
                output.getValue0(), Units.SEC), Level.FINAL);
    }

    @Override
    protected void performForEveryRetrieval(Octet<
                      Calculator<Tuple, Pair<Double, Double>>,
                      Calculator<Unit<Double>, Unit<Double>>,
                      Calculator<Unit<Double>, Unit<Double>>,
                      Calculator<Unit<Double>, Unit<Double>>,
                      Pair<Double, String>,
                      Pair<Double, String>,
                      Integer,
                      Double> configuration) {
        Pair<Double, String> darkPair = configuration.getValue4();
        ResultsHolder.getResults().addResult(new DoubleValueResult("DARK_CURRENT", 
                darkPair.getValue0(), darkPair.getValue1()), Level.DEBUG);
        Pair<Double, String> readoutPair = configuration.getValue5();
        ResultsHolder.getResults().addResult(new DoubleValueResult("READOUT_NOISE", 
                readoutPair.getValue0(), readoutPair.getValue1()), Level.DEBUG);
        ResultsHolder.getResults().addResult(new LongValueResult("NUMBER_OF_EXPOSURES", 
                configuration.getValue6(), null), Level.DEBUG);
        ResultsHolder.getResults().addResult(new DoubleValueResult("FIXED_SNR", 
                configuration.getValue7(), null), Level.DEBUG);
    }
    
}
