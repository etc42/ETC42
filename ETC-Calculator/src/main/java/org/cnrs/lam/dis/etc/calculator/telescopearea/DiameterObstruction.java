/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.telescopearea;

import java.text.MessageFormat;
import java.util.ResourceBundle;
import lombok.EqualsAndHashCode;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.Cacheable;
import org.cnrs.lam.cesam.util.calculator.CachingPolicy;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.DoubleValueResult;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.javatuples.Pair;
import org.javatuples.Tuple;
import org.javatuples.Unit;

/**
 * <p>The {@link DiameterObstruction} class is a calculator for computing the total
 * area of the primary mirror which reflects light. The equation used for the 
 * calculation is:</p>
 * {@latex.inline $
 * A_{tel} = \\pi * \\left(\\frac{D_1}{2} \\right)^2 * (1-ob)
 * $}
 * <p>where</p>
 * <ul>
 *   <li>{@latex.inline $A_{tel}$}: is the calculated telescope area, expressed
 *       in {@latex.inline $cm^2$}</li>
 *   <li>{@latex.inline $D_1$}: is the diameter of the primary mirror expressed 
 *       in centimeters ({@latex.inline $cm$})</li>
 *   <li>{@latex.inline $ob$}: is the total light obstruction (a number in the
 *       range [0,1])</li>
 * </ul>
 * 
 * <p>The configuration of the calculator is a {@link Pair},
 * which contains the following elements:</p>
 * <ol>
 *   <li>{@link Pair}: The diameter of the primary mirror and its unit</li>
 *   <li>{@link java.lang.Double}: The total obstruction</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Tuple}, as this
 * calculator does not accept any input. It should always be null.</p>
 * 
 * <p>The output of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the telescope area (expressed in
 * {@latex.inline $cm^2$}).</p>
 * 
 * @author Nikolaos Apostolakos
 */
@EqualsAndHashCode(callSuper = false, exclude={"telescopeArea"})
public class DiameterObstruction extends AbstractCalculator<Pair<Pair<Double, String>, Double>, Tuple, Unit<Double>> {

    private static final ResourceBundle validationErrorsBundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");
    // We store the diameter and the obstruction values to be used from lombok
    // for the equals and the hashCode methods
    private double diameter;
    private double obstruction;
    // As the result will be the same for all the lambda, we do the calculation
    // only once and we store it here
    private Double telescopeArea;

    /**
     * Validates the given configuration. It requires that the diameter of the 
     * telescope is a positive number, that its unit is {@latex.inline $cm$} and 
     * that the obstruction is between 0 and 1 (inclusive).
     * @param configuration The configuration to validate
     * @throws ConfigurationException If the configuration is invalid
     */
    @Override
    protected void validateConfiguration(Pair<Pair<Double, String>, Double> configuration) throws ConfigurationException {
        // Check that the morror diameter is a positive number
        if (configuration.getValue0().getValue0() <= 0) {
            String message = validationErrorsBundle.getString("MIRROR_DIAMETER_NOT_POSITIVE");
            message = MessageFormat.format(message, configuration.getValue0().getValue0());
            throw new ConfigurationException(message);
        }
        // Check if the mirror diameter is in cm
        if (!Units.isCm(configuration.getValue0().getValue1())) {
            String message = validationErrorsBundle.getString("MIRROR_DIAMETER_WRONG_UNIT");
            message = MessageFormat.format(message, Units.CM, configuration.getValue0().getValue1());
            throw new ConfigurationException(message);
        }
        // Check that the obstruction is between 0 and 1
        if (configuration.getValue1() < 0 || configuration.getValue1() > 1) {
            String message = validationErrorsBundle.getString("MIRROR_OBSTRUCTION_WRONG_RANGE");
            message = MessageFormat.format(message, configuration.getValue1()*100);
            throw new ConfigurationException(message);
        }
    }

    /**
     * Initializes the calculator instance with the given configuration.
     * @param configuration The configuration to use for initialization
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(Pair<Pair<Double, String>, Double> configuration) throws InitializationException {
        diameter = configuration.getValue0().getValue0();
        obstruction = configuration.getValue1();
    }

    /**
     * Returns a {@link org.javatuples.Unit} containing the effective telescope
     * area, expressed in {@latex.inline $cm^2$}.
     * @param input 
     * @return The telescope area expressed in {@latex.inline $cm^2$}
     * @throws CalculationException  if there is any error during the calculation
     */
    @Override
    protected Unit<Double> performCalculation(Tuple input) throws CalculationException {
        // Because the telescope area is not depended on the wavelength we calculate
        // it only the first time
        if (telescopeArea == null) {
            double radius = diameter / 2;
            telescopeArea = Math.PI * radius * radius * (1 - obstruction);
        }
        return new Unit<Double>(telescopeArea);
    }

    /**
     * <p>For every calculator instantiation the following parts of its configuration
     * are added in the simulation results:</p>
     * <ul>
     *   <li>The primary mirror diameter in debug results with code PRIMARY_DIAMETER</li>
     *   <li>The area obstruction in debug results with code OBSTRUCTION</li>
     * </ul>
     * @param configuration The configuration of the calculator
     */
    @Override
    protected void performForEveryRetrieval(Pair<Pair<Double, String>, Double> configuration) {
        ResultsHolder.getResults().addResult(new DoubleValueResult("PRIMARY_DIAMETER",
                configuration.getValue0().getValue0(), configuration.getValue0().getValue1()), Level.DEBUG);
        ResultsHolder.getResults().addResult(new DoubleValueResult("OBSTRUCTION",
                configuration.getValue1() * 100, "%"), Level.DEBUG);
    }

    /**
     * <p>For every calculation the calculator adds in the simulation results
     * the following outputs of the calculator:</p>
     * <ul>
     *   <li>The telescope area in intermediate important results
     *       with code TELESCOPE_AREA</li>
     * </ul>
     * @param input
     * @param output 
     */
    @Override
    protected void performForEveryCalculation(Tuple input, Unit<Double> output) {
        ResultsHolder.getResults().addResult(new DoubleValueResult("TELESCOPE_AREA", 
                output.getValue0(), Units.CM2), Level.INTERMEDIATE_IMPORTANT);
    }
}
