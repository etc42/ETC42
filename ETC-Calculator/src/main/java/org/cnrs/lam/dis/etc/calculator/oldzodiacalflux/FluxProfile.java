/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.oldzodiacalflux;

import lombok.EqualsAndHashCode;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.Cacheable;
import org.cnrs.lam.cesam.util.calculator.CachingPolicy;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.Calculator;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.DatasetProviderHolder;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.StringResult;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.javatuples.Pair;
import org.javatuples.Unit;

/**
 * <p>The {@code FluxProfile} class is a calculator for computing the zodiacal flux
 * from a dataset. The configuration of the calculator
 * is a {@link org.javatuples.Pair} which contains the following elements:</p>
 * <ol>
 *   <li>{@link org.cnrs.lam.dis.etc.datamodel.DatasetInfo}: the information of the
 *       zodiacal flux dataset</li>
 *   <li>{@link org.cnrs.lam.cesam.util.calculator.Calculator}: a calculator
 *       representing the dataset of the zodiacal flux</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the wavelength expressed in {@latex.inline \\AA}.</p>
 * 
 * <p>The output of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the zodiacal flux, expressed in
 * {@latex.inline $erg/s/cm^2/$\\AA$/arcsec^2$}.</p>
 */

// Caching explenation
// -------------------
// This calculator is set to cache the instances because the configuration validation
// is retrieving the dataset, which is time consuming.
@Cacheable(CachingPolicy.INSTANCE)
@EqualsAndHashCode(callSuper=false)
public class FluxProfile extends AbstractCalculator<Pair<DatasetInfo, Calculator<Unit<Double>, Unit<Double>>>, Unit<Double>, Unit<Double>> {
    
    private Calculator<Unit<Double>, Unit<Double>> spectrumCalculator;

    /**
     * Validates the given configuration. It requires that the site has a
     * zodiacal flux dataset, that the data for the dataset is 
     * available, that the unit of X axis is {@latex.inline \\AA} and the unit
     * of Y axis is {@latex.inline $erg/s/cm^2/$\\AA$/arcsec^2$}.
     * @param configuration the configuration to validate
     * @throws ConfigurationException if the configuration is invalid
     */
    @Override
    protected void validateConfiguration(Pair<DatasetInfo, Calculator<Unit<Double>, Unit<Double>>> configuration) throws ConfigurationException {
        DatasetInfo info = configuration.getValue0();
        if (info == null) {
            throw new ConfigurationException("Site does not have zodiacal flux");
        }
        Dataset dataset = DatasetProviderHolder.getDatasetProvider().getDataset(Dataset.Type.ZODIACAL, info);
        if (dataset == null) {
            throw new ConfigurationException("Data for zodiacal flux "
                    + configuration.getValue1() + " is not available");
        }
        if (!Units.isAngstrom(dataset.getXUnit())) {
            throw new ConfigurationException("Unit of X axis of zodiacal flux must be "
                    + Units.ANGSTROM + " but was " + dataset.getXUnit());
        }
        if (!Units.isErgPerCm2PerSecPerAngstromPerArcsec2(dataset.getYUnit())) {
            throw new ConfigurationException("Unit of Y axis of zodiacal flux must be "
                    + Units.getErgPerCm2PerSecPerAngstromPerArcsec2() + " but was " + dataset.getYUnit());
        }
    }

    /**
     * Initializes the calculator instance with the given configuration. The first
     * value of the configuration is the zodiacal flux dataset info and the second
     * is a dataset calculator representing the zodiacal flux.
     * @param configuration the configuration to use for initialization
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(Pair<DatasetInfo, Calculator<Unit<Double>, Unit<Double>>> configuration) throws InitializationException {
        spectrumCalculator = configuration.getValue1();
    }

    /**
     * Returns a {@link org.javatuples.Unit} containing the zodiacal flux,
     * expressed in {@latex.inline $erg/s/cm^2/$\\AA$/arcsec^2$}.
     * @param input The wavelength expressed in {@latex.inline \\AA}
     * @return The zodiacal flux expressed in {@latex.inline $erg/s/cm^2/$\\AA$/arcsec^2$}
     * @throws CalculationException  if there is any error during the calculation
     */
    @Override
    protected Unit<Double> performCalculation(Unit<Double> input) throws CalculationException {
        double result = spectrumCalculator.calculate(input).getValue0();
        return new Unit<Double>(result);
    }

    /**
     * Overridden to add in the results the zodiacal flux. This is done here, so
     * we have this results even if the result is retrieved from the cache.
     * @param input The wavelength expressed in {@latex.inline \\AA}
     * @param output The zodiacal flux expressed in {@latex.inline $erg/s/cm^2/$\\AA$/arcsec^2$}
     */
    @Override
    protected void performForEveryCalculation(Unit<Double> input, Unit<Double> output) {
        ResultsHolder.getResults().addResult("ZODIACAL_FLUX", input.getValue0(),
                output.getValue0(), Units.ANGSTROM, Units.getErgPerCm2PerSecPerAngstrom()
                , Level.INTERMEDIATE_UNIMPORTANT);
    }

    /**
     * Overridden to add in the results the zodiacal flux dataset info.
     * @param configuration The configuration of the calculator
     */
    @Override
    protected void performForEveryRetrieval(Pair<DatasetInfo, Calculator<Unit<Double>, Unit<Double>>> configuration) {
        ResultsHolder.getResults().addResult(new StringResult("ZODIACAL_FLUX_PROFILE",
                configuration.getValue0().toString()), Level.DEBUG);
    }
    
}
