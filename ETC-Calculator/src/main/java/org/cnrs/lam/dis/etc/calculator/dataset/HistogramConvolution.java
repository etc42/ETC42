/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.dataset;

import java.util.Map;
import java.util.TreeMap;
import lombok.EqualsAndHashCode;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.Cacheable;
import org.cnrs.lam.cesam.util.calculator.CachingPolicy;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.DatasetProviderHolder;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.Dataset.Type;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.javatuples.Pair;
import org.javatuples.Quintet;
import org.javatuples.Unit;

/**
 * <p>The {@code HistrogramConvolution} class is a calculator which takes a
 * dataset, creates a new one with the a given resolution and returns as result
 * the values of this dataset convolved with a kernel. The kernel size is the number
 * in number of resolution elements and it can vary for different requested values,
 * so is given as a parameter
 * of the calculation. Note though that the values of the kernel are set in such
 * way so that the result will just be smoothed and not scaled. It is assumed that
 * the represented dataset is a histogram.</p>
 * 
 * <p>It must be noted that this is a calculator intended for internal use by the
 * {@link org.cnrs.lam.dis.etc.calculator.dataset.Histogram} calculator. (This
 * though doesn't forbid its use in other cases).</p>
 * 
 * <p>The configuration of the calculator is a {@link org.javatuples.Quintet},
 * which contains the following elements:</p>
 * <ol>
 *   <li>{@link org.cnrs.lam.dis.etc.datamodel.Dataset.Type}: The type of the
 *       dataset this calculator will represent</li>
 *   <li>{@link org.cnrs.lam.dis.etc.datamodel.DatasetInfo}: The information (name
 *       and namespace) of the dataset this calculator will represent</li>
 *   <li>{@link java.lang.Double}: The resolution</li>
 *   <li>{@link org.cnrs.lam.dis.etc.datamodel.Dataset.DataType}: The type of
 *       the data of the dataset, showing if we have a template or emission lines</li>
 *   <li>{@link java.lang.String}: The option of a multi-dataset or null for single
 *       dataset</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Pair},
 * which contains the following elements:</p>
 * <ol>
 *   <li>{@link java.lang.Integer}: The key for which the value will be returned</li>
 *   <li>{@link java.lang.Integer}: The size of the kernel for convolution</li>
 * </ol>
 *
 * @author Nikolaos Apostolakos
 */

// Caching explenation
// -------------------
// This calculator is set to cache everything because the configuration validation
// is retrieving the dataset (which is time consuming), the initialization is creating
// the unit resolution dataset and the calculation is doing lazy convolution.
@Cacheable(CachingPolicy.ALL)
@EqualsAndHashCode(callSuper=false,exclude={"data"})
public class HistogramConvolution extends AbstractCalculator<
        Quintet<Dataset.Type, DatasetInfo, Double, Dataset.DataType, String>
        , Pair<Integer, Integer>, Unit<Double>> {

    private Dataset.Type datasetType;
    private DatasetInfo datasetInfo;
    private String option;
    private double resolution;
    private Dataset.DataType histogramType;
    
    private Map<Integer, Double> data;
    
    /**
     * Validates the given configuration. It requires that the given dataset is
     * available via the dataset provider, that the resolution is positive and
     * the data type is supported.
     * @param configuration The configuration to validate
     * @throws ConfigurationException If configuration is invalid
     */
    @Override
    protected void validateConfiguration(
            Quintet<Dataset.Type, DatasetInfo, Double, Dataset.DataType, String> configuration
            ) throws ConfigurationException {
        Dataset dataset = DatasetProviderHolder.getDatasetProvider().getDataset(
                configuration.getValue0(), configuration.getValue1(), configuration.getValue4());
        if (dataset == null) {
            String optionString = (configuration.getValue4() == null || configuration.getValue4().equals(""))
                    ? "" : " and option " + configuration.getValue4();
            throw new ConfigurationException("The dataset of type" + configuration.getValue0()
                    + " and name " + configuration.getValue1() + optionString + " is not available");
        }
        if (configuration.getValue2() <= 0) {
            throw new ConfigurationException("The resolution of a histogram must be positive.");
        }
        switch (configuration.getValue3()) {
            case EMISSION_LINES:
            case TEMPLATE:
                break;
            default:
                throw new ConfigurationException("HistogramConvolution cannot be "
                        + "used for data with type " + configuration.getValue3());
        }
    }

    /**
     * Initializes the calculator instance with the given configuration. The 
     * first value of the configuration is the type of the dataset, the second
     * is the name of it, the third is the resolution and the fourth the data type.
     * The initialization includes the creation of the unit
     * resolution dataset.
     * @param configuration The configuration to use for initialization
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(
            Quintet<Dataset.Type, DatasetInfo, Double, Dataset.DataType, String> configuration
            ) throws InitializationException {
        datasetType = configuration.getValue0();
        datasetInfo = configuration.getValue1();
        option = configuration.getValue4();
        Dataset dataset = DatasetProviderHolder.getDatasetProvider().getDataset(
                datasetType, datasetInfo, option);
        resolution = configuration.getValue2();
        histogramType = configuration.getValue3();
        populateData(dataset);
    }

    /**
     * Generates the unit resolution dataset from the given histogram dataset.
     * @param dataset The original dataset
     */
    private void populateData(Dataset dataset) {
        // we use a tree map to have the entries in order
        data = new TreeMap<Integer, Double>();
        // First we get the keys and the values of the input data in arrays, to
        // make the implementation of the logic easier
        Double[] oldKeys = dataset.getData().keySet().toArray(new Double[dataset.getData().size()]);
        Double[] oldValues = dataset.getData().values().toArray(new Double[dataset.getData().size()]);
        double minKey = oldKeys[0] - (oldKeys[1] - oldKeys[0]) / 2;
        double maxKey = oldKeys[oldKeys.length - 1] + (oldKeys[oldKeys.length - 1] - oldKeys[oldKeys.length - 2]) / 2;
        minKey = Math.floor(minKey / resolution);
        maxKey = Math.ceil(maxKey / resolution);
        // Go through the old values and create the new ones
        double oldRangeMax = 0;
        for (int i = 0; i < oldKeys.length; i++) {
            // Calculate the range this value represents
            double rangeMin = 0;
            double rangeMax = 0;
            switch (histogramType) {
                case TEMPLATE:
                    rangeMin = (i == 0)
                            ? minKey
                            : Math.ceil((oldKeys[i - 1] + oldKeys[i]) / 2 / resolution);
                    rangeMin = Math.floor(Math.min(rangeMin, oldKeys[i] / resolution));
                    rangeMin = (oldRangeMax == rangeMin) ? rangeMin + 1 : rangeMin;
                    rangeMax = (i == oldKeys.length - 1)
                            ? maxKey
                            : Math.floor((oldKeys[i] + oldKeys[i + 1]) / 2 / resolution);
                    rangeMax = Math.ceil(Math.max(rangeMax, oldKeys[i] / resolution));
                    oldRangeMax = rangeMax;
                    break;
                case EMISSION_LINES:
                    rangeMin = Math.floor(oldKeys[i] / resolution);
                    rangeMax = Math.ceil(oldKeys[i] / resolution);
            }
            // Calculate the value the elements in the range should have
            double average = oldValues[i] / (rangeMax - rangeMin + 1);
            // Add the average to the new data
            for (int j = (int) rangeMin; j <= (int) rangeMax; j++) {
                Double oldValue = data.get(j);
                double newValue = (oldValue == null) ? 0 : oldValue;
                newValue += average;
                data.put(j, newValue);
            }
        }
        // Now we go through all the range and we add zeroes to the knots that
        // have no value. This will for sure be the case for emission lines data type
        for (int i = (int) minKey; i <= maxKey; i++) {
            if(data.get(i) == null) {
                data.put(i, 0.);
            }
        }
    }

    /**
     * Returns a {@link org.javatuples.Unit} containing the value of the function
     * for the given input. The first element of the input is the key to retrieve
     * the value for and the second is the length of the kernel to use for the
     * convolution (expressed in resolution elements).
     * @param input The key to retrieve the value for and the length of the kernel
     * @return The value of the function for the given key
     * @throws CalculationException if there is any error during the calculation
     */
    @Override
    protected Unit<Double> performCalculation(Pair<Integer, Integer> input) throws CalculationException {
        int key = input.getValue0();
        int kernel = input.getValue1();
        int minKey = (int) Math.floor(key - kernel/2.);
        int maxKey = (int) Math.ceil(key + kernel/2.);
        double sum = 0;
        double counter = 0;
        for (int i = minKey; i <= maxKey; i++) {
            Double value = data.get(i);
            if (value != null) {
                counter++;
                sum += value;
            }
        }
        // if the counter is 0 means that we do not have any values for the given
        // key, so it is out of range
        if (counter == 0) {
            throw new CalculationException("Tried to get a value from the " + datasetType
                    + " dataset, but the key " + (key / resolution) + " is out of range");
        }
        return new Unit<Double>(sum / counter / resolution);
    }
    
}
