/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.sourcepixelcoverage;

import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.dis.etc.calculator.spatialbinning.SpatialBinningFactory;
import org.cnrs.lam.dis.etc.datamodel.Image;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.javatuples.Unit;

/**
 * <p>the {@code SourcePixelCoverageFactory} provides calculators for producing
 * maps of the detector pixels which are affected by the source light, without
 * taking into consideration any slit losses or any grism or prism diffraction.
 * The configuration of the factory is a {@link org.cnrs.lam.dis.etc.datamodel.Session}
 * object containing all the configuration from the user.</p>
 */
public class SourcePixelCoverageFactory implements Factory<Unit<Session>, Unit<Double>, Unit<Image<Byte>>> {

    @Override
    public Calculator<Unit<Double>, Unit<Image<Byte>>> getCalculator(Unit<Session> configuration) throws InitializationException, ConfigurationException {
        Calculator<Unit<Double>, Unit<Double>> spatialBinningCalculator
                = new SpatialBinningFactory().getCalculator(configuration);
        return EtcCalculatorManager.getManager(Symmetric.class).getCalculator(new Unit(spatialBinningCalculator));
    }
    
}
