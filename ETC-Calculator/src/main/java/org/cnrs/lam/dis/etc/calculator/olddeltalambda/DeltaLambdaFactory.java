/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.olddeltalambda;

import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.oldspectralbinning.SpectralBinningFactory;
import org.cnrs.lam.dis.etc.calculator.oldspectralresolution.SpectralResolutionFactory;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.StringResult;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.cnrs.lam.dis.etc.datamodel.Instrument;
import org.cnrs.lam.dis.etc.datamodel.ObsParam;
import org.cnrs.lam.dis.etc.datamodel.ObsParam.SpectralQuantumType;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.javatuples.Quartet;
import org.javatuples.Triplet;
import org.javatuples.Unit;

/**
 * <p>The {@code DeltaLambdaFactory} provides calculators for calculating the 
 * range of wavelengths (expressed in {@latex.inline \\AA}) around a specific
 * wavelength which overlap with it. The configuration
 * of the factory is a {@link org.cnrs.lam.dis.etc.datamodel.Session} object
 * containing all the configuration from the user.</p>
 */
public class DeltaLambdaFactory implements Factory<Unit<Session>, Unit<Double>, Unit<Double>> {

    @Override
    public Calculator<Unit<Double>, Unit<Double>> getCalculator(Unit<Session> configuration) throws InitializationException, ConfigurationException {
        Session session = configuration.getValue0();
        Instrument instrument = session.getInstrument();
        ObsParam obsParam = session.getObsParam();
        
        // We are chosing one of the two types of spectralBinning, imaging or spectroscopy
        switch (instrument.getInstrumentType()) {
            
            case IMAGING:
                ResultsHolder.getResults().addResult(
                        new StringResult("DELTA_LAMBDA_METHOD", "Imaging"), Level.DEBUG);
                // The imaging spectralBinning does not need any configuration, so we pass null
                return EtcCalculatorManager.getManager(Imaging.class).getCalculator(null);
                
            case SPECTROGRAPH:
                ResultsHolder.getResults().addResult(
                        new StringResult("DELTA_LAMBDA_METHOD", "Spectroscopy"), Level.DEBUG);
                // The spectroscopy spectralBinning requires as input the following:
                // 1: A Calculator for the spectral binning
                // 3: A Calculator for the the spectral resolution
                // 4: A boolean showing if we have calculation per spectral pixel
                //    or per spectral resolution element
                
                // 1. The spectral binning calculator
                Calculator<Unit<Double>, Unit<Double>> spectralBinning =
                        new SpectralBinningFactory().getCalculator(configuration);
                // 2. The calculator for spectral resolution
                Calculator<Unit<Double>, Unit<Double>> spectralResolution =
                        new SpectralResolutionFactory().getCalculator(configuration);
                // 3. The spectral resolution element flag
                boolean isPerSpectralResolutionElement = 
                        obsParam.getSpectralQuantumType() == SpectralQuantumType.SPECTRAL_RESOLUTION_ELEMENT;
                // Create  and return the calculator
                Triplet<Calculator<Unit<Double>, Unit<Double>>, Calculator<Unit<Double>, Unit<Double>>, Boolean> calculatorConfiguration
                        = new Triplet<Calculator<Unit<Double>, Unit<Double>>, Calculator<Unit<Double>, Unit<Double>>, Boolean>
                                (spectralBinning, spectralResolution, isPerSpectralResolutionElement);
                return EtcCalculatorManager.getManager(Spectroscopy.class).getCalculator(calculatorConfiguration);
        }
        throw new UnsupportedOperationException("Delta Lambda calculation is "
                + "not supported for the given configuration");
    }
}
