/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.surfacebrightnessprofile;

import java.text.MessageFormat;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TreeMap;
import lombok.EqualsAndHashCode;
import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.analysis.BivariateRealFunction;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.apache.log4j.Logger;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.calculator.util.functions.CircularlySymmetricBivariateFunction;
import org.cnrs.lam.dis.etc.calculator.util.functions.IntegrableUnivariateFunction;
import org.cnrs.lam.dis.etc.calculator.util.integration.IntegrationTool;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.cnrs.lam.dis.etc.datamodel.Source;
import org.javatuples.Pair;
import org.javatuples.Tuple;
import org.javatuples.Unit;

/**
 *<p>The {@link DeVaucouleurs} class is a calculator for computing functions representing
 * the surface brightness profile, following the De Vaucouleurs distribution. The equation
 * used is:</p>
 * {@latex.inline $
 * I_{(r)}=e^{-7.67*\\left( \\frac{r}{r_{obj}}\\right) ^{1/4}}
 * $}
 * <p>where</p>
 * <ul>
 *   <li>{@latex.inline I_{(r)}}: is the surface brightness profile</li>
 *   <li>{@latex.inline $r_{obj}$}: is the effective radius of the extended source in
 *       {@latex.inline $arcsec$}</li>
 *   <li>{@latex.inline $r$}: is the distance from the center of the source, for
 *       which the value is calculated, expressed in {@latex.inline $arcsec$}</li>
 * </ul>
 * 
 * <p>Note that the above function has peak value equal with 1 at (0,0). This
 * corresponds to a given magnitude at peak value. If the user defines that the
 * given magnitude is for the total flux, then the returned function is normalized
 * so it has integral 1 for the area inside the effective radius.</p>
 * 
 * <p>The configuration of the calculator is a {@link org.javatuples.Pair},
 * with contains the following elements:</p>
 * <ol>
 *   <li>{@link Pair}: The effective radius of the source and its unit</li>
 *   <li>{@link Source.ExtendedMagnitudeType}: Shows if the profile should be
 *       normalized (total case) or not (peak value case</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Tuple}, as this
 * calculator does not accept any input. It should always be null.</p>
 * 
 * <p>The output of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link BivariateRealFunction} representing the surface brightness profile.</p>
 */
@EqualsAndHashCode(callSuper=false, of={"sourceRadius"})
public class DeVaucouleurs extends AbstractCalculator<
        Pair<Pair<Double, String>, Source.ExtendedMagnitudeType>,
        Tuple, Unit<BivariateRealFunction>> {
    
    private static final Logger logger = Logger.getLogger(DeVaucouleurs.class);
    private static final ResourceBundle validationErrorsBundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");
    private double sourceRadius;
    private Source.ExtendedMagnitudeType extendedMagnitudeType;

    /**
     * Validates the given configuration. It requires that the first value of the
     * tuple (the source radius) is a positive number and that the second
     * value (the unit of the source radius) is {@latex.inline $arcsec$}.
     * @param configuration The configuration to validate
     * @throws ConfigurationException If the configuration is invalid
     */
    @Override
    protected void validateConfiguration(Pair<Pair<Double, String>, Source.ExtendedMagnitudeType> configuration) throws ConfigurationException {
        // Check that the source radius is a positive number
        if (configuration.getValue0().getValue0() <= 0) {
            String message = validationErrorsBundle.getString("EXTENDED_SOURCE_RADIUS_NOT_POSITIVE");
            message = MessageFormat.format(message, configuration.getValue0().getValue0());
            throw new ConfigurationException(message);
        }
        // Check if the source radius is in arcsec
        if (!Units.isArcsec(configuration.getValue0().getValue1())) {
            String message = validationErrorsBundle.getString("EXTENDED_SOURCE_RADIUS_WRONG_UNIT");
            message = MessageFormat.format(message, Units.ARCSEC, configuration.getValue0().getValue1());
            throw new ConfigurationException(message);
        }
    }

    /**
     * Initializes the calculator instance with the given configuration. The first
     * value of the configuration is the source radius and the second is
     * the unit of the source radius.
     * @param configuration the configuration to use for initialization
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(Pair<Pair<Double, String>, Source.ExtendedMagnitudeType> configuration) throws InitializationException {
        sourceRadius = configuration.getValue0().getValue0();
        extendedMagnitudeType = configuration.getValue1();
    }

    /**
     * Returns a {@link org.javatuples.Unit} containing the surface brightness 
     * profile function.
     * @param input The distance from the source center expressed in {@latex.inline $arcsec$}
     * @return The surface brightness profile function
     * @throws CalculationException if there is any error during the calculation
     */
    @Override
    protected Unit<BivariateRealFunction> performCalculation(Tuple input) throws CalculationException {
        BivariateDeVaucouleursFunction profileFunction = new BivariateDeVaucouleursFunction(sourceRadius, 1);
        if (extendedMagnitudeType == Source.ExtendedMagnitudeType.TOTAL) {
            double multiplier = 0;
            try {
                multiplier = 1 / IntegrationTool.bivariateIntegral(profileFunction, sourceRadius);
            } catch (FunctionEvaluationException ex) {
                logger.error("Failed to calculate the surface brightness profile normalization factor", ex);
                throw new CalculationException("Failed to calculate the surface brightness profile normalization factor");
            }
            profileFunction = new BivariateDeVaucouleursFunction(sourceRadius, multiplier);
        }
        return new Unit<BivariateRealFunction>(profileFunction);
    }

    /**
     * <p>For every calculation the calculator adds in the simulation results
     * the following outputs of the calculator:</p>
     * <ul>
     *   <li>The projection of the surface brightness profile on the plane y=0
     *       in the intermediate unimportant results with code
     *       SURFACE_BRIGHTNESS_PROFILE_PROJECTION</li>
     * </ul>
     * @param input
     * @param output 
     */
    @Override
    protected void performForEveryCalculation(Tuple input, Unit<BivariateRealFunction> output) {
        // If the result already exists we do not add it to save time
        if (ResultsHolder.getResults().getResultByName("SURFACE_BRIGHTNESS_PROFILE_PROJECTION") != null) {
            return;
        }
        // We know we have a bivariate de Vaucouleurs function so the casting is safe
        DeVaucouleurs.BivariateDeVaucouleursFunction function = (DeVaucouleurs.BivariateDeVaucouleursFunction) output.getValue0();
        // We add the projection on the y = 0 plane in the results. We calculate
        // the valuse in the range [-sourceRadius, sourceRadius]
        UnivariateRealFunction projection = function.projectionFunction();
        Map<Double, Double> projectionData = new TreeMap<Double, Double>();
        // We will create 110 points to represent the plot
        double step = sourceRadius / 50;
        double low = -1.1 * sourceRadius;
        double high = 1.1 * sourceRadius;
        for (double x = low; x <= high; x += step) {
            double value = 0;
            try {
                value = projection.value(x);
            } catch (FunctionEvaluationException ex) {
                logger.warn("Failed to calculate the value of the surface brightness profile projection", ex);
            }
            projectionData.put(x, value);
        }
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleDatasetResult(
                "SURFACE_BRIGHTNESS_PROFILE_PROJECTION", projectionData, Units.ARCSEC, null),
                CalculationResults.Level.INTERMEDIATE_UNIMPORTANT);
    }

    /**
     * <p>For every calculator instantiation the following parts of its configuration
     * are added in the simulation results:</p>
     * <ul>
     *   <li>The extended source effective radius in debug results with code SOURCE_RADIUS</li>
     *   <li>The type the given flux or magnitude in debug results with code EXTENDED_MAG_FLUX_GIVEN_FOR</li>
     * </ul>
     * @param configuration The configuration of the calculator
     */
    @Override
    protected void performForEveryRetrieval(Pair<Pair<Double, String>, Source.ExtendedMagnitudeType> configuration) {
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("SOURCE_RADIUS",
                configuration.getValue0().getValue0(), configuration.getValue0().getValue1()), CalculationResults.Level.DEBUG);
        ResultsHolder.getResults().addResult(new CalculationResults.StringResult("EXTENDED_MAG_FLUX_GIVEN_FOR",
                configuration.getValue1().toString()), CalculationResults.Level.DEBUG);
    }
    
    /**
     * This class represents a circularly symmetric bivariate De Vaucouleurs function.
     */
    @EqualsAndHashCode(callSuper=false, of={"limit","multiplier"})
    private class BivariateDeVaucouleursFunction implements CircularlySymmetricBivariateFunction {
        
        private final double limit;
        private final double m;
        private final double multiplier;

        public BivariateDeVaucouleursFunction(double limit, double multiplier) {
            this.limit = limit;
            this.m = -7.67 / Math.pow(limit, 0.25);
            this.multiplier = multiplier;
        }

        @Override
        public double value(double r) throws FunctionEvaluationException {
            return multiplier * Math.exp(m * Math.pow(Math.abs(r), 0.25));
        }

        @Override
        public UnivariateRealFunction projectionFunction() {
            return new DeVaucouleursFunction(limit, multiplier);
        }

        @Override
        public double value(double x, double y) throws FunctionEvaluationException {
            return value(Math.sqrt(x * x + y * y));
        }

        @Override
        public UnivariateRealFunction polarFunction() {
            return new DeVaucouleursPolarFunction(limit, multiplier);
        }
        
    }
    
    /**
     * This class represents a univariate De Vaucouleurs polar function which is integrable.
     */
    @EqualsAndHashCode(callSuper=false, of={"limit","multiplier"})
    private static class DeVaucouleursPolarFunction implements IntegrableUnivariateFunction {
        
        private final double limit;
        private final double m;
        private final double multiplier;

        public DeVaucouleursPolarFunction(double limit, double multiplier) {
            this.limit = limit;
            this.m = -7.67 / Math.pow(limit, 0.25);
            this.multiplier = multiplier;
        }

        @Override
        public double value(double x) throws FunctionEvaluationException {
            if (x < 0) {
                throw new IllegalArgumentException("Polar function cannot be calculated for negative values");
            }
            return multiplier * x * Math.exp(m * Math.pow(Math.abs(x), 0.25));
        }

        @Override
        public double integral(double x1, double x2) throws FunctionEvaluationException {
            if (x1 > x2) {
                throw new IllegalArgumentException("Illegal integration range [" + x1 + "," + x2 + "]");
            }
            if (x1 < 0) {
                throw new IllegalArgumentException("Polar function cannot be calculated for negative values");
            }
            if (x1 == x2) {
                return 0;
            }
            return indefiniteIntegral(x2) - indefiniteIntegral(x1);
        }
        
        private double indefiniteIntegral(double x) {
            if (x < 0) {
                throw new IllegalArgumentException("Cannot calculate De Vaucouleurs polar "
                        + "indefinite integral for negative X");
            }
            return multiplier * 4 * Math.exp(m * Math.pow(x, 0.25)) * (Math.pow(m, 7) * Math.pow(x, 1.75) - 7 * Math.pow(m, 6) * Math.pow(x, 1.5) +
                    42 * Math.pow(m, 5) * Math.pow(x, 1.25) - 210 * Math.pow(m, 4) * x + 840 * Math.pow(m, 3) * Math.pow(x, 0.75) -
                    2520 * m * m * Math.sqrt(x) + 5040 * m * Math.pow(x, 0.25) - 5040) / Math.pow(m, 8);
        }
        
    }
    
    /**
     * This class represents a univariate De Vaucouleurs function which is integrable.
     */
    @EqualsAndHashCode(callSuper=false, of={"limit", "multiplier"})
    private static class DeVaucouleursFunction implements IntegrableUnivariateFunction {
        
        private final double limit;
        private final double m;
        private final double multiplier;

        public DeVaucouleursFunction(double limit, double multiplier) {
            this.limit = limit;
            this.m = -7.67 / Math.pow(limit, 0.25);
            this.multiplier = multiplier;
        }

        @Override
        public double value(double x) throws FunctionEvaluationException {
            return multiplier * Math.exp(m * Math.pow(Math.abs(x), 0.25));
        }

        @Override
        public double integral(double x1, double x2) throws FunctionEvaluationException {
            if (x1 > x2) {
                throw new IllegalArgumentException("Illegal integration range [" + x1 + "," + x2 + "]");
            }
            if (x1 == x2) {
                return 0;
            }
            // We cannot calculate the indefinite integral for negative values
            // but we use the fact that the function is symmetric
            if (x1 >= 0) {
                return indefiniteIntegral(x2) - indefiniteIntegral(x1);
            }
            if (x2 <= 0) {
                return indefiniteIntegral(-x1) - indefiniteIntegral(-x2);
            }
            double firstHalf = indefiniteIntegral(-x1) - indefiniteIntegral(0);
            double secondHalf = indefiniteIntegral(x2) - indefiniteIntegral(0);
            return firstHalf + secondHalf;
        }
        
        private double indefiniteIntegral(double x) {
            if (x < 0) {
                throw new IllegalArgumentException("Cannot calculate De Vaucouleurs "
                        + "indefinite integral for negative X");
            }
            return multiplier * 4 * Math.exp(m * Math.pow(x, 0.25)) * (Math.pow(x, 0.75) * 
                    Math.pow(m, 3) - 3 * Math.sqrt(x) * m * m + 6 * Math.pow(x, 0.25) * m - 6) / Math.pow(m, 4);
        }
        
    }
    
}
