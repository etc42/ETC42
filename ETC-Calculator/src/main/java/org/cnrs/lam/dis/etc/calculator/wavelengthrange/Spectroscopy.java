/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.wavelengthrange;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import lombok.EqualsAndHashCode;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.Cacheable;
import org.cnrs.lam.cesam.util.calculator.CachingPolicy;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.javatuples.Pair;
import org.javatuples.Quartet;
import org.javatuples.Tuple;

/**
 * <p>The {@code Spectroscopy} class is a calculator for computing the wavelength
 * range for a spectrograph simulation. For this kind of simulation the user provides
 * the range as an input. This calculator just stores and returns these values.
 * Note that these values must be expressed in Angstrom, they must be positive
 * and the maximum must be bigger than the minimum.</p>
 * 
 * <p>The configuration of the calculator is a {@link org.javatuples.Quartet},
 * witch contains the following elements:</p>
 * <ol>
 *   <li>{@link java.lang.Double}: The minimum wavelength value</li>
 *   <li>{@link java.lang.String}: The minimum wavelength unit</li>
 *   <li>{@link java.lang.Double}: The maximum wavelength value</li>
 *   <li>{@link java.lang.String}: The maximum wavelength unit</li>
 * </ol>
 * 
 * <p>The calculator does not need any input, so it accepts as an input
 * a {@link org.javatuples.Tuple} which will be ignored during the calculation
 * (it can be null).</p>
 * 
 * <p>The output of the calculator is a {@link org.javatuples.Pair} which contains
 * the following elements:</p>
 * <ol>
 *   <li>{@link java.lang.Double}: The minimum wavelength value (expressed in
 *       {@latex.inline \\AA})</li>
 *   <li>{@link java.lang.Double}: The maximum wavelength value (expressed in
 *       {@latex.inline \\AA})</li>
 * </ol>
 */
@Cacheable(CachingPolicy.INSTANCE)
@EqualsAndHashCode(callSuper=false)
public class Spectroscopy extends AbstractCalculator<Quartet<Double, String, Double, String>, Tuple, Pair<Double, Double>> {
    
    private static final ResourceBundle validationErrorsBundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");
    private double minLambda;
    private double maxLambda;

    /**
     * <p>Validates the configuration of the calculator. The checks done are:</p>
     * <ul>
     *   <li>The minimum and maximum values must be positive</li>
     *   <li>They must be expressed in {@latex.inline \\AA}</li>
     *   <li>The minimum cannot be bigger than the maximum</li>
     * </ul>
     * @param configuration The configuration of the calculator
     * @throws ConfigurationException If the validation fails
     */
    @Override
    protected void validateConfiguration(Quartet<Double, String, Double, String> configuration) throws ConfigurationException {
        double minValue = configuration.getValue0();
        String minUnit = configuration.getValue1();
        double maxValue = configuration.getValue2();
        String maxUnit = configuration.getValue3();
        if (minValue <= 0) {
            String message = validationErrorsBundle.getString("RANGE_MIN_NEGATIVE");
            message = MessageFormat.format(message, minValue);
            throw new ConfigurationException(message);
        }
        if (!Units.isAngstrom(minUnit)) {
            String message = validationErrorsBundle.getString("RANGE_MIN_WRONG_UNIT");
            message = MessageFormat.format(message, Units.ANGSTROM, minUnit);
            throw new ConfigurationException(message);
        }
        if (maxValue <= 0) {
            String message = validationErrorsBundle.getString("RANGE_MAX_NEGATIVE");
            message = MessageFormat.format(message, maxValue);
            throw new ConfigurationException(message);
        }
        if (!Units.isAngstrom(maxUnit)) {
            String message = validationErrorsBundle.getString("RANGE_MAX_WRONG_UNIT");
            message = MessageFormat.format(message, Units.ANGSTROM, maxUnit);
            throw new ConfigurationException(message);
        }
        if (minValue > maxValue) {
            String message = validationErrorsBundle.getString("RANGE_MIN_BIGGER_THAN_MAX");
            throw new ConfigurationException(message);
        }
    }

    /**
     * Initializes the calculator.
     * @param configuration The configuration of the calculator
     * @throws InitializationException 
     */
    @Override
    protected void initialize(Quartet<Double, String, Double, String> configuration) throws InitializationException {
        minLambda = configuration.getValue0();
        maxLambda = configuration.getValue2();
    }

    /**
     * <p>Returns a {@link org.javatuples.Pair} which contains the following elements:</p>
     * <ol>
     *   <li>{@link java.lang.Double}: The minimum wavelength value (expressed in
     *       {@latex.inline \\AA})</li>
     *   <li>{@link java.lang.Double}: The maximum wavelength value (expressed in
     *       {@latex.inline \\AA})</li>
     * </ol>
     * <p>Both values are the ones given as input by the user.</p>
     */
    @Override
    protected Pair<Double, Double> performCalculation(Tuple input) throws CalculationException {
		Pair<Double, Double> calculableRange = CommonRangeFactory.getCalculableRange(
			CommonRangeFactory.getSession(),
			new Pair<Double, Double>(minLambda, maxLambda)
		);
    	
        return new Pair<Double, Double>(calculableRange.getValue0(), calculableRange.getValue1());
    }
    
    /**
     * <p>For every calculation the calculator adds in the simulation results
     * the following outputs of the calculator:</p>
     * <ul>
     *   <li>The calculated minimum wavelength in intermediate unimportant results
     *       with code WAVELENGTH_MIN</li>
     *   <li>The calculated maximum wavelength in intermediate unimportant results
     *       with code WAVELENGTH_MAX</li>
     * @param input
     * </ul>
     * @param output 
     */
    @Override
    protected void performForEveryCalculation(Tuple input, Pair<Double, Double> output) {
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("WAVELENGTH_MIN", 
                output.getValue0(), Units.ANGSTROM), CalculationResults.Level.INTERMEDIATE_UNIMPORTANT);
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("WAVELENGTH_MAX", 
                output.getValue1(), Units.ANGSTROM), CalculationResults.Level.INTERMEDIATE_UNIMPORTANT);
    }

    /**
     * <p>For every calculator instantiation the following parts of its configuration
     * are added in the simulation results:</p>
     * <ul>
     *   <li>The minimum wavelength given by the user in debug results
     *       with code RANGE_MIN</li>
     *   <li>The maximum wavelength given by the user in debug results
     *       with code RANGE_MAX</li>
     * </ul>
     * @param configuration 
     */
    @Override
    protected void performForEveryRetrieval(Quartet<Double, String, Double, String> configuration) {
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("RANGE_MIN", 
                configuration.getValue0(), configuration.getValue1()), CalculationResults.Level.DEBUG);
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("RANGE_MAX", 
                configuration.getValue2(), configuration.getValue3()), CalculationResults.Level.DEBUG);
    }
    
}
