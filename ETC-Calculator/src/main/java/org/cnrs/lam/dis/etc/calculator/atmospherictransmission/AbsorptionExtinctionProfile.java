/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.atmospherictransmission;

import java.text.MessageFormat;
import java.util.Collections;
import java.util.ResourceBundle;
import lombok.EqualsAndHashCode;
import org.apache.commons.math.FunctionEvaluationException;
import org.apache.log4j.Logger;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.DatasetProviderHolder;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.FunctionToDatasetResultConverter;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.calculator.util.datasets.LinearFunctionDataset;
import org.cnrs.lam.dis.etc.calculator.util.functions.BoundedUnivariateFunction;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.DoubleDatasetResult;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.javatuples.Pair;
import org.javatuples.Triplet;
import org.javatuples.Tuple;
import org.javatuples.Unit;

/**
 * <p>The {@link AbsorptionExtinctionProfile} class is a calculator for computing 
 * the atmospheric transmission on ground. The equation used is:</p>
 * {@latex.inline $
 * \\xi_{(\\lambda)}=\\xi_{abs(\\lambda)}*10^{-0.4*X*k_{(\\lambda)}}
 * $}
 * <p>where</p>
 * <ul>
 *   <li>{@latex.inline $\\lambda$}: is the wavelength</li>
 *   <li>{@latex.inline $\\xi_{(\\lambda)}$}: is the atmospheric transmission</li>
 *   <li>{@latex.inline $P\\xi_{abs(\\lambda)}$}: is the atmospheric absorption</li>
 *   <li>{@latex.inline $X$}: is the air mass</li>
 *   <li>{@latex.inline $k_{(\\lambda)}$}: is the atmospheric extinction</li>
 * </ul>
 * 
 * <p>The configuration of the calculator is a {@link Triplet},
 * with contains the following elements:</p>
 * <ol>
 *   <li>{@link Double}: The air mass</li>
 *   <li>{@link DatasetInfo}: The information of the absorption dataset</li>
 *   <li>{@link DatasetInfo}: The information of the extinction dataset</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Tuple}, as this
 * calculator does not accept any input. It should always be null.</p>
 * 
 * <p>The output of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link BoundedUnivariateFunction} representing the atmospheric transmission.</p>
 */
// Caching explenation
// -------------------
// This calculator is set to cache the instances because the configuration validation
// and the instantiation are retrieving the dataset, which is time consuming.
@Cacheable(CachingPolicy.INSTANCE)
@EqualsAndHashCode(callSuper=false, of={"airMass", "absorptionDatasetInfo", "extinctionDatasetInfo"})
public class AbsorptionExtinctionProfile extends AbstractCalculator<
        Triplet<Double, DatasetInfo, DatasetInfo>, Tuple, Unit<BoundedUnivariateFunction>> {
    
    private static final Logger logger = Logger.getLogger(AbsorptionExtinctionProfile.class);
    private static final ResourceBundle validationErrorsBundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");
    private double airMass;
    private DatasetInfo absorptionDatasetInfo;
    private DatasetInfo extinctionDatasetInfo;
    private BoundedUnivariateFunction transmissionFunction;

    /**
     * Validates the given configuration. It requires that the air mass is not less
     * than one, that the site has an atmospheric absorption and extinction, that
     * their data are available and that the unit of their X axis is {@latex.inline \\AA}.
     * @param configuration the configuration to validate
     * @throws ConfigurationException if the configuration is invalid
     */
    @Override
    protected void validateConfiguration(Triplet<Double, DatasetInfo, DatasetInfo> configuration) throws ConfigurationException {
        if (configuration.getValue0() < 1) {
            String message = validationErrorsBundle.getString("AIR_MASS_LESS_THAN_ONE");
            message = MessageFormat.format(message, configuration.getValue0());
            throw new ConfigurationException(message);
        }
        DatasetInfo absorptionInfo = configuration.getValue1();
        if (absorptionInfo == null) {
            String message = validationErrorsBundle.getString("ATMOSPHERIC_ABSORPTION_MISSING");
            throw new ConfigurationException(message);
        }
        Dataset dataset = DatasetProviderHolder.getDatasetProvider().getDataset(Dataset.Type.SKY_ABSORPTION, absorptionInfo);
        if (dataset == null) {
            String message = validationErrorsBundle.getString("ATMOSPHERIC_ABSORPTION_NOT_AVAILABLE");
            message = MessageFormat.format(message, absorptionInfo);
            throw new ConfigurationException(message);
        }
        if (!Units.isAngstrom(dataset.getXUnit())) {
            String message = validationErrorsBundle.getString("ATMOSPHERIC_ABSORPTION_WRONG_X_UNIT");
            message = MessageFormat.format(message, Units.ANGSTROM, dataset.getXUnit());
            throw new ConfigurationException(message);
        }
        Double absorptionMin = Collections.min(dataset.getData().keySet());
        Double absorptionMax = Collections.max(dataset.getData().keySet());
        DatasetInfo extinctionInfo = configuration.getValue2();
        if (extinctionInfo == null) {
            String message = validationErrorsBundle.getString("ATMOSPHERIC_EXTINCTION_MISSING");
            throw new ConfigurationException(message);
        }
        dataset = DatasetProviderHolder.getDatasetProvider().getDataset(Dataset.Type.SKY_EXTINCTION, extinctionInfo);
        if (dataset == null) {
            String message = validationErrorsBundle.getString("ATMOSPHERIC_EXTINCTION_NOT_AVAILABLE");
            message = MessageFormat.format(message, absorptionInfo);
            throw new ConfigurationException(message);
        }
        if (!Units.isAngstrom(dataset.getXUnit())) {
            String message = validationErrorsBundle.getString("ATMOSPHERIC_EXTINCTION_WRONG_X_UNIT");
            message = MessageFormat.format(message, Units.ANGSTROM, dataset.getXUnit());
            throw new ConfigurationException(message);
        }
        Double extinctionMin = Collections.min(dataset.getData().keySet());
        Double extinctionMax = Collections.max(dataset.getData().keySet());
        if (absorptionMin >= extinctionMax || absorptionMax <= extinctionMin) {
            String message = validationErrorsBundle.getString("ATMOSPHERIC_ABSORPTION_EXTINCTION_NOT_OVERLAPPING");
            throw new ConfigurationException(message);
        }
    }

    /**
     * Initializes the calculator instance with the given configuration.
     * @param configuration the configuration to use for initialization
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(Triplet<Double, DatasetInfo, DatasetInfo> configuration) throws InitializationException {
        airMass = configuration.getValue0();
        absorptionDatasetInfo = configuration.getValue1();
        Dataset dataset = DatasetProviderHolder.getDatasetProvider().getDataset(Dataset.Type.SKY_ABSORPTION, absorptionDatasetInfo);
        final LinearFunctionDataset absorptionFunction = new LinearFunctionDataset(dataset.getData());
        extinctionDatasetInfo = configuration.getValue2();
        dataset = DatasetProviderHolder.getDatasetProvider().getDataset(Dataset.Type.SKY_EXTINCTION, extinctionDatasetInfo);
        final LinearFunctionDataset extinctionFunction = new LinearFunctionDataset(dataset.getData());
        transmissionFunction = new BoundedUnivariateFunction() {
            @Override
            public Pair<Double, Double> getBounds() {
                Pair<Double, Double> absorptionBounds = absorptionFunction.getBounds();
                Pair<Double, Double> extinctionBounds = extinctionFunction.getBounds();
                double min = Math.max(absorptionBounds.getValue0(), extinctionBounds.getValue0());
                double max = Math.min(absorptionBounds.getValue1(), extinctionBounds.getValue1());
                return new Pair<Double, Double>(min, max);
            }
            @Override
            public Pair<Double, Double> getNonZeroBounds() {
                Pair<Double, Double> absorptionBounds = absorptionFunction.getNonZeroBounds();
                Pair<Double, Double> extinctionBounds = extinctionFunction.getNonZeroBounds();
                double min = Math.max(absorptionBounds.getValue0(), extinctionBounds.getValue0());
                double max = Math.min(absorptionBounds.getValue1(), extinctionBounds.getValue1());
                return new Pair<Double, Double>(min, max);
            }
            @Override
            public double value(double x) throws FunctionEvaluationException {
//            	System.out.println("AbsorptionExtinctionProfile");
//            	System.out.println(x);
                double absoprtionValue = absorptionFunction.value(x);
                double extinctionValue = extinctionFunction.value(x);
                double value = absoprtionValue * Math.pow(10, -0.4 * airMass * extinctionValue);
                return value;
            }
        };
    }

    /**
     * Returns a {@link BoundedUnivariateFunction} which can be used to calculate
     * the atmospheric transmission.
     * @param input The calculator has no input, so it can be always null
     * @return A {@link BoundedUnivariateFunction} which can be used to calculate
     * the atmospheric transmission
     * @throws CalculationException if there is any error during the calculation
     */
    @Override
    protected Unit<BoundedUnivariateFunction> performCalculation(Tuple input) throws CalculationException {
        return new Unit<BoundedUnivariateFunction>(transmissionFunction);
    }

    /**
     * <p>For every calculation the calculator adds in the simulation results
     * the following outputs of the calculator:</p>
     * <ul>
     *   <li>The atmospheric transmission in intermediate important results with code
     *       ATMOSPHERIC_TRANSMISSION</li>
     * </ul>
     * @param input
     * @param output 
     */
    @Override
    protected void performForEveryCalculation(Tuple input, Unit<BoundedUnivariateFunction> output) {
        // If the result already exists we do not add it to save time
        if (ResultsHolder.getResults().getResultByName("ATMOSPHERIC_TRANSMISSION") != null) {
            return;
        }
        BoundedUnivariateFunction function = output.getValue0();
        DoubleDatasetResult result = FunctionToDatasetResultConverter.convert(function, "ATMOSPHERIC_TRANSMISSION", Units.ANGSTROM, null);
        ResultsHolder.getResults().addResult(result, CalculationResults.Level.INTERMEDIATE_IMPORTANT);
    }

    /**
     * <p>For every calculator instantiation the following parts of its configuration
     * are added in the simulation results:</p>
     * <ul>
     *   <li>The value of the Air Mass in debug results with code AIR_MASS</li>
     *   <li>The name of the sky absorption profile with code SKY_ABSORPTION_PROFILE</li>
     *   <li>The name of the sky extinction profile with code SKY_EXTINCTION_PROFILE</li>
     * </ul>
     * @param configuration The configuration of the calculator
     */
    @Override
    protected void performForEveryRetrieval(Triplet<Double, DatasetInfo, DatasetInfo> configuration) {
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("AIR_MASS",
                configuration.getValue0(), null), CalculationResults.Level.DEBUG);
        CalculationResults.StringResult result = new CalculationResults.StringResult(
                "SKY_ABSORPTION_PROFILE", configuration.getValue1().toString());
        ResultsHolder.getResults().addResult(result, CalculationResults.Level.DEBUG);
        result = new CalculationResults.StringResult(
                "SKY_EXTINCTION_PROFILE", configuration.getValue2().toString());
        ResultsHolder.getResults().addResult(result, CalculationResults.Level.DEBUG);
    }
    
}
