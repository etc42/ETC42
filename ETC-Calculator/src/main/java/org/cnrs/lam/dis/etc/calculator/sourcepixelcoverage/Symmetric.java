/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.sourcepixelcoverage;

import lombok.EqualsAndHashCode;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.cnrs.lam.dis.etc.datamodel.Image;
import org.javatuples.Unit;

/**
 * <p>The {@code Symmetric} class is a calculator for producing a map of the
 * detector pixels which are affected by the source light, without taking into
 * consideration any slit of fiber losses or any grism or prism diffraction. 
 * The calculator produces an image which has side with
 * length the same as the ceiling of the spatial binning. The value of each
 * pixel of the image is set by checking the distance between the center of the
 * image and the pixel corner closer to it. If this distance is bigger than the
 * half of the spatial binning the pixel gets the value 0. If is is smaller it
 * gets the value 1.</p>
 * 
 * <p>The configuration of the calculator is a {@link org.javatuples.Unit}, which
 * contains the calculator for calculating the spatial binning.</p>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the wavelength expressed in {@latex.inline \\AA}.</p>
 * 
 * <p>The output of the calculator is a {@link org.cnrs.lam.dis.etc.datamodel.Image}
 * representing the calculated map.</p>
 */

// Caching explenation
// -------------------
// This calculator is set to cache everything, as the calculation is producing a
// whole image, which is time consuming.
@Cacheable(CachingPolicy.ALL)
@EqualsAndHashCode(callSuper=false)
public class Symmetric extends AbstractCalculator<Unit<Calculator<Unit<Double>, Unit<Double>>>, Unit<Double>, Unit<Image<Byte>>> {
    
    private Calculator<Unit<Double>, Unit<Double>> spatialBinningCalculator;

    /**
     * Validates the given configuration. Currently it does nothing.
     * @param configuration The configuration to validate
     * @throws ConfigurationException If the configuration is invalid
     */
    @Override
    protected void validateConfiguration(Unit<Calculator<Unit<Double>, Unit<Double>>> configuration) throws ConfigurationException {
    }
    
    /**
     * Initializes the calculator instance with the given configuration. The
     * configuration contains a calculator for calculating the spatial binning.
     * @param configuration the configuration to use for initialization
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(Unit<Calculator<Unit<Double>, Unit<Double>>> configuration) throws InitializationException {
        spatialBinningCalculator = configuration.getValue0();
    }

    /**
     * Returns a {@link org.javatuples.Unit} containing an image with the affected
     * pixels from the source light.
     * @param input The wavelength expressed in {@latex.inline \\AA}
     * @return The image of the source pixel coverage
     * @throws CalculationException if there is any error during the calculation
     */
    @Override
    protected Unit<Image<Byte>> performCalculation(Unit<Double> input) throws CalculationException {
        double spatialBinning = spatialBinningCalculator.calculate(input).getValue0();
        double radius = spatialBinning / 2;
        int widthInPixels = (int) Math.ceil(spatialBinning);
        Byte[][] data = new Byte[widthInPixels][widthInPixels];
        double halfWidth = widthInPixels / 2.;
        // For the apper left part we check if the bottom right corner of the pixel
        // is closer to the center than the radius of the source
        for (int x = 0; x < halfWidth; x++) {
            for (int y = 0; y < halfWidth; y++) {
                // Check if the center is in this pixel
                if (x+1 >= halfWidth && y+1 >= halfWidth) {
                    data[x][y] = 1;
                    continue;
                }
                double distanceFromCenter = Math.sqrt((x+1 - halfWidth) * (x+1 - halfWidth) + (y+1 - halfWidth) * (y+1 - halfWidth));
                data[x][y] = (byte) (distanceFromCenter < radius ? 1 : 0);
            }
        }
        // For the apper right part we check if the bottom left corner of the pixel
        // is closer to the center than the radius of the source
        for (int x = (int) Math.ceil(halfWidth); x < widthInPixels; x++) {
            for (int y = 0; y < halfWidth; y++) {
                double distanceFromCenter = Math.sqrt((x - halfWidth) * (x - halfWidth) + (y+1 - halfWidth) * (y+1 - halfWidth));
                data[x][y] = (byte) (distanceFromCenter < radius ? 1 : 0);
            }
        }
        // For the lower left part we check if the upper right corner of the pixel
        // is closer to the center than the radius of the source
        for (int x = 0; x < halfWidth; x++) {
            for (int y = (int) Math.ceil(halfWidth); y < widthInPixels; y++) {
                double distanceFromCenter = Math.sqrt((x+1 - halfWidth) * (x+1 - halfWidth) + (y - halfWidth) * (y - halfWidth));
                data[x][y] = (byte) (distanceFromCenter < radius ? 1 : 0);
            }
        }
        // For the lower right part we check if the upper left corner of the pixel
        // is closer to the center than the radius of the source
        for (int x = (int) Math.ceil(halfWidth); x < widthInPixels; x++) {
            for (int y = (int) Math.ceil(halfWidth); y < widthInPixels; y++) {
                double distanceFromCenter = Math.sqrt((x - halfWidth) * (x - halfWidth) + (y - halfWidth) * (y - halfWidth));
                data[x][y] = (byte) (distanceFromCenter < radius ? 1 : 0);
            }
        }
        Image<Byte> result = new Image<Byte>(data);
        return new Unit<Image<Byte>>(result);
    
    /**
     * <p>For every calculation the calculator adds in the simulation results
     * the following outputs of the calculator:</p>
     * <ul>
     *   <li>The image of the source pixel coverage in intermediate unimportant results
     *       with code SOURCE_PIXEL_COVERAGE</li>
     * </ul>
     * @param input
     * @param output 
     */}

    @Override
    protected void performForEveryCalculation(Unit<Double> input, Unit<Image<Byte>> output) {
        ResultsHolder.getResults().addResult("SOURCE_PIXEL_COVERAGE", input.getValue0()
                , output.getValue0(), Units.ANGSTROM, Level.INTERMEDIATE_UNIMPORTANT);
    }
    
}
