/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.spatialbinning;

import java.text.MessageFormat;
import java.util.ResourceBundle;
import lombok.EqualsAndHashCode;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.javatuples.Triplet;
import org.javatuples.Unit;

/**
 * <p>The {@link PointSource} class is a calculator for computing the spatial
 * binning, in the case the source is a point source and the instrument is not
 * a fiber or slit spectrograph. The equation used for the calculation is:</p>
 * {@latex.inline $
 * SpatialBinning_{(\\lambda)}=\\frac{PSF_{size(\\lambda)}}{p}
 * $}
 * <p>where</p>
 * <ul>
 *   <li>{@latex.inline $SpatialBinning$}: is the spatial binning in 
 *       {@latex.inline $pixels$}</li>
 *   <li>{@latex.inline $\\lambda$}: is the wavelength expressed in angstrom 
 *       ({@latex.inline \\AA})</li>
 *   <li>{@latex.inline $PSF_{size(\\lambda)$}: is the size of the PSF in 
 *       {@latex.inline $arcsec$}</li>
 *   <li>{@latex.inline $p$}: is the pixel scale of the instrument expressed in
 *       {@latex.inline $arcsec/pixel$}</li>
 * </ul>
 * 
 * <p>The configuration of the calculator is a {@link org.javatuples.Triplet},
 * with contains the following elements:</p>
 * <ol>
 *   <li>{@link java.lang.Double}: The pixel scale of the instrument</li>
 *   <li>{@link java.lang.String}: The unit of the pixel scale of the instrument</li>
 *   <li>{@link Calculator}: a calculator for calculating the size of the PSF</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link Unit} which contains the wavelength
 * expressed in {@latex.inline \\AA}.</p>
 * 
 * <p>The output of the calculator is a {@link Unit} which contains the spatial
 * binning, expressed in {@latex.inline $pixels$}.</p>
 */
@Cacheable(CachingPolicy.INSTANCE)
@EqualsAndHashCode(callSuper=false)
public class PointSource extends AbstractCalculator<Triplet<Double, String, Calculator<Unit<Double>, Unit<Double>>>, Unit<Double>, Unit<Double>> {
    
    private static final ResourceBundle validationErrorsBundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");
    private double pixelScale;
    private Calculator<Unit<Double>, Unit<Double>> psfSizeCalculator;

    /**
     * Validates the given configuration. It requires that the first value of the
     * tuple (the pixel scale of the instrument) is a positive number and that the second
     * value (the unit of the pixel scale) is {@latex.inline $arcsec/pixel$}.
     * @param configuration The configuration to validate
     * @throws ConfigurationException If the configuration is invalid
     */
    @Override
    protected void validateConfiguration(Triplet<Double, String, Calculator<Unit<Double>, Unit<Double>>> configuration) throws ConfigurationException {
        // Check that the pixel scale is a positive number
        if (configuration.getValue0() <= 0) {
            String message = validationErrorsBundle.getString("PIXEL_SCALE_NOT_POSITIVE");
            message = MessageFormat.format(message, configuration.getValue0());
            throw new ConfigurationException(message);
        }
        // Check if the pixel scale is in arcsec/pixel
        if (!Units.getArcsecPerPixel().equals(configuration.getValue1())) {
            String message = validationErrorsBundle.getString("PIXEL_SCALE_WRONG_UNIT");
            message = MessageFormat.format(message, Units.getArcsecPerPixel(), configuration.getValue1());
            throw new ConfigurationException(message);
        }
    }

    /**
     * Initializes the calculator instance with the given configuration. The first
     * value of the configuration is the pixel scale of the instrument, the second is
     * the unit of the pixel scale and the third is a calculator for calculating the
     * size of the PSF.
     * @param configuration the configuration to use for initialization
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(Triplet<Double, String, Calculator<Unit<Double>, Unit<Double>>> configuration) throws InitializationException {
        pixelScale = configuration.getValue0();
        psfSizeCalculator = configuration.getValue2();
    }

    /**
     * Returns a {@link org.javatuples.Unit} containing the Spatial Binning
     * expressed in {@latex.inline $pixels$}.
     * @param input The wavelength expressed in {@latex.inline \\AA}
     * @return The spatial binning expressed in {@latex.inline $pixels$}
     * @throws CalculationException if there is any error during the calculation
     */
    @Override
    protected Unit<Double> performCalculation(Unit<Double> input) throws CalculationException {
        double psfSize = psfSizeCalculator.calculate(input).getValue0();
        double result = psfSize / pixelScale;
        return new Unit<Double>(result);
    }

    /**
     * <p>For every calculation the calculator adds in the simulation results
     * the following outputs of the calculator:</p>
     * <ul>
     *   <li>The spatial binning in intermediate important results
     *       with code SPATIAL_BINNING</li>
     * </ul>
     * @param input
     * @param output 
     */
    @Override
    protected void performForEveryCalculation(Unit<Double> input, Unit<Double> output) {
        ResultsHolder.getResults().addResult("SPATIAL_BINNING", input.getValue0(),
                output.getValue0(), Units.ANGSTROM, Units.PIXEL, Level.INTERMEDIATE_IMPORTANT);
    }

    /**
     * <p>For every calculator instantiation the following parts of its configuration
     * are added in the simulation results:</p>
     * <ul>
     *   <li>The pixel scale in debug results with code PIXEL_SCALE</li>
     * </ul>
     * @param configuration The configuration of the calculator
     */
    @Override
    protected void performForEveryRetrieval(Triplet<Double, String, Calculator<Unit<Double>, Unit<Double>>> configuration) {
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("PIXEL_SCALE",
                configuration.getValue0(), configuration.getValue1()), Level.DEBUG);
    }
    
}
