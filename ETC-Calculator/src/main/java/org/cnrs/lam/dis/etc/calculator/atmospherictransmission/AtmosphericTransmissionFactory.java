/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.atmospherictransmission;

import java.util.ResourceBundle;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.functions.BoundedUnivariateFunction;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.cnrs.lam.dis.etc.datamodel.Site;
import org.javatuples.Triplet;
import org.javatuples.Tuple;
import org.javatuples.Unit;

/**
 */
public class AtmosphericTransmissionFactory implements Factory<Unit<Session>, Tuple, Unit<BoundedUnivariateFunction>> {

    private static final ResourceBundle bundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");

    @Override
    public Calculator<Tuple, Unit<BoundedUnivariateFunction>> getCalculator(Unit<Session> configuration) throws InitializationException, ConfigurationException {
        Session session = configuration.getValue0();
        Site site = session.getSite();
        
        switch (site.getLocationType()) {
            
            case SPACE:
                ResultsHolder.getResults().addResult(new CalculationResults.StringResult(
                        "ATMOSPHERIC_TRANSMISSION_METHOD", "Space"), CalculationResults.Level.DEBUG);
//                System.out.println(Space.class);
                return EtcCalculatorManager.getManager(Space.class).getCalculator(null);
                
            case GROUND:
                switch (site.getAtmosphericTransmissionType()) {
                    case ABSORPTION_EXTINCTION:
                        ResultsHolder.getResults().addResult(new CalculationResults.StringResult(
                                "ATMOSPHERIC_TRANSMISSION_METHOD", "Absoption and extinction profiles"), CalculationResults.Level.DEBUG);
                        double airMass = site.getAirMass();
                        DatasetInfo absorptionInfo = site.getSkyAbsorption();
                        DatasetInfo extinctionInfo = site.getSkyExtinction();
                        Triplet<Double, DatasetInfo, DatasetInfo> calculatorConfiguration =
                                new Triplet<Double, DatasetInfo, DatasetInfo>(airMass, absorptionInfo, extinctionInfo);
//                        System.out.println(AbsorptionExtinctionProfile.class);
                        return EtcCalculatorManager.getManager(AbsorptionExtinctionProfile.class).getCalculator(calculatorConfiguration);
                    case TEMPLATE:
                        ResultsHolder.getResults().addResult(new CalculationResults.StringResult(
                                "ATMOSPHERIC_TRANSMISSION_METHOD", "Total atmospheric transmission profile"), CalculationResults.Level.DEBUG);
                        DatasetInfo totalInfo = site.getAtmosphericTransmission();
                        Unit<DatasetInfo> totalConfiguration = new Unit<DatasetInfo>(totalInfo);
//                        System.out.println(TotalProfile.class);
                        return EtcCalculatorManager.getManager(TotalProfile.class).getCalculator(totalConfiguration);
                }
        }
        String message = bundle.getString("UNKNOWN_ATMOSPHERIC_TRANSMISSION_METHOD");
        throw new ConfigurationException(message);
    }
    
}
