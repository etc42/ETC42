/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.util.functions;

import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.cnrs.lam.dis.etc.calculator.util.integration.IntegrationTool;

/**
 * <p>The {@link AveragingKernelConvolution} class represents the convolution of
 * a given function with a kernel of variable size and value (constant for each
 * size) such to average the values of the given function without scaling them.
 * If the size of the kernel is zero, then the values of the given function are
 * returned.</p>
 */
public class AveragingKernelConvolution implements UnivariateRealFunction {
    
    private final UnivariateRealFunction originalFunction;
    private final UnivariateRealFunction kernelSizeFunction;

    /**
     * Constructs a new {@link AveragingKernelConvolution} instance which convolves
     * the given original function with a kernel with variable size given by the
     * second function.
     * @param originalFunction The function to convolve
     * @param kernelSizeFunction The function giving the size of the kernel
     */
    public AveragingKernelConvolution(UnivariateRealFunction originalFunction,
            UnivariateRealFunction kernelSizeFunction) {
        this.originalFunction = originalFunction;
        this.kernelSizeFunction = kernelSizeFunction;
    }

    /**
     * Returns the average value of the original function for the range of the
     * kernel size around the given X value.
     * @param x The value to calculate the convolved function for
     * @return The result of the convolution
     * @throws FunctionEvaluationException 
     */
    @Override
    public double value(double x) throws FunctionEvaluationException {
        // We calculate the kernel size for the given x
        double kernelSize = kernelSizeFunction.value(x);
        // If the kernel size is zero we just return the value of the original function
        if (kernelSize == 0) {
            return originalFunction.value(x);
        }
        // We calculate the average value of the original function in this range
        double integral = IntegrationTool.univariateIntegral(originalFunction, x - kernelSize/2, x + kernelSize/2);
        double value = integral / kernelSize;
        return value;
    }
    
}
