/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.extrasignal;

import java.text.MessageFormat;
import java.util.ResourceBundle;
import lombok.EqualsAndHashCode;
import org.apache.log4j.Logger;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.DatasetProviderHolder;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.FunctionToDatasetResultConverter;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.calculator.util.datasets.LinearFunctionDataset;
import org.cnrs.lam.dis.etc.calculator.util.datasets.TemplateFunctionDataset;
import org.cnrs.lam.dis.etc.calculator.util.functions.BoundedUnivariateFunction;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.DoubleDatasetResult;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.javatuples.Tuple;
import org.javatuples.Unit;

/**
 *<p>The {@link ExtraSignalDataset} class is a calculator for computing the 
 * extra signal per Angstrom from a dataset. The configuration of the calculator
 * is a {@link Unit} which contains the following elements:</p>
 * <ol>
 *   <li>{@link DatasetInfo}: the information of the extra signal dataset</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Tuple}, as this
 * calculator does not accept any input. It should always be null.</p>
 * 
 * <p>The output of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link BoundedUnivariateFunction} representing the extra signal.</p>
 * 
 * <p>Note: This method should be used for spectrograph simulations only.</p>
 */

// Caching explenation
// -------------------
// This calculator is set to cache the instances because the configuration validation
// and the instantiation are retrieving the dataset, which is time consuming.
@Cacheable(CachingPolicy.INSTANCE)
@EqualsAndHashCode(callSuper=false, exclude={"extraSignalFunction"})
public class ExtraSignalDataset extends AbstractCalculator<Unit<DatasetInfo>,
        Tuple, Unit<BoundedUnivariateFunction>> {
    
    // The bundle with all the error translations
    private static final ResourceBundle validationErrorsBundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");
    private static final Logger logger = Logger.getLogger(ExtraSignalDataset.class);
    // This is a variable where the extra signal dataset info is stored.
    // This is used from lombok library for the equals and the hashCode methods.
    // We do not want to use the function we create, as this would be way too time
    // consuming.
    private DatasetInfo extraSignalDatasetInfo;
    // The dataset which represents the filter transmission
    private BoundedUnivariateFunction extraSignalFunction;

    /**
     * Validates the given configuration. It requires that the observing parameters
     * have extra signal dataset, that the data for the extra signal is 
     * available, that the unit of X axis is {@latex.inline \\AA} and that the
     * unit of Y axis is {@latex.inline $e^-/pixel/$\\AA}.
     * @param configuration the configuration to validate
     * @throws ConfigurationException if the configuration is invalid
     */
    @Override
    protected void validateConfiguration(Unit<DatasetInfo> configuration) throws ConfigurationException {
        DatasetInfo info = configuration.getValue0();
        if (info == null) {
            String message = validationErrorsBundle.getString("EXTRA_SIGNAL_DATASET_MISSING");
            throw new ConfigurationException(message);
        }
        Dataset dataset = DatasetProviderHolder.getDatasetProvider().getDataset(Dataset.Type.EXTRA_SIGNAL, info);
        if (dataset == null) {
            String message = validationErrorsBundle.getString("EXTRA_SIGNAL_DATASET_NOT_AVAILABLE");
            message = MessageFormat.format(message, info);
            throw new ConfigurationException(message);
        }
        if (!Units.isAngstrom(dataset.getXUnit())) {
            String message = validationErrorsBundle.getString("EXTRA_SIGNAL_DATASET_WRONG_X_UNIT");
            message = MessageFormat.format(message, Units.ANGSTROM, dataset.getXUnit());
            throw new ConfigurationException(message);
        }
        if (dataset.getDataType() == Dataset.DataType.FUNCTION && !Units.isElectronsPerSecPerAngstrom(dataset.getYUnit())) {
            String message = validationErrorsBundle.getString("EXTRA_SIGNAL_DATASET_WRONG_Y_UNIT");
            message = MessageFormat.format(message, Units.getElectronsPerSecPerAngstrom(), dataset.getYUnit());
            throw new ConfigurationException(message);
        }
        if (dataset.getDataType() == Dataset.DataType.TEMPLATE && !Units.isElectronsPerSec(dataset.getYUnit())) {
            String message = validationErrorsBundle.getString("EXTRA_SIGNAL_DATASET_WRONG_Y_UNIT");
            message = MessageFormat.format(message, Units.getElectronsPerSec(), dataset.getYUnit());
            throw new ConfigurationException(message);
        }
    }

    /**
     * Initializes the calculator instance with the given configuration.
     * @param configuration the configuration to use for initialization
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(Unit<DatasetInfo> configuration) throws InitializationException {
        extraSignalDatasetInfo = configuration.getValue0();
        Dataset dataset = DatasetProviderHolder.getDatasetProvider().getDataset(Dataset.Type.EXTRA_SIGNAL, extraSignalDatasetInfo);
        if (dataset.getDataType() == Dataset.DataType.FUNCTION) {
            extraSignalFunction = new LinearFunctionDataset(dataset.getData());
        } else {
            extraSignalFunction = new TemplateFunctionDataset(dataset.getData());
        }
    }

    /**
     * Returns a {@link BoundedUnivariateFunction} which can be used to calculate
     * the extra signal.
     * @param input The calculator has no input, so it can be always null
     * @return A {@link BoundedUnivariateRealFunction} which can be used to calculate
     * the extra signal
     * @throws CalculationException if there is any error during the calculation
     */
    @Override
    protected Unit<BoundedUnivariateFunction> performCalculation(Tuple input) throws CalculationException {
        return new Unit<BoundedUnivariateFunction>(extraSignalFunction);
    }

    /**
     * <p>For every calculation the calculator adds in the simulation results
     * the following outputs of the calculator:</p>
     * <ul>
     *   <li>The extra signal in intermediate important results with code
     *       EXTRA_SIGNAL</li>
     * </ul>
     * @param input
     * @param output 
     */
    @Override
    protected void performForEveryCalculation(Tuple input, Unit<BoundedUnivariateFunction> output) {
        // If the result already exists we do not add it to save time
        if (ResultsHolder.getResults().getResultByName("EXTRA_SIGNAL") != null) {
            return;
        }
        BoundedUnivariateFunction function = output.getValue0();
        DoubleDatasetResult result = FunctionToDatasetResultConverter.convert(function,
                "EXTRA_SIGNAL", Units.ANGSTROM, Units.getElectronsPerSecPerAngstrom());
        ResultsHolder.getResults().addResult(result, CalculationResults.Level.INTERMEDIATE_IMPORTANT);
    }

    /**
     * <p>For every calculator instantiation the following parts of its configuration
     * are added in the simulation results:</p>
     * <ul>
     *   <li>The name of the extra signal template with code EXTRA_SIGNAL_TEMPLATE</li>
     * </ul>
     * @param configuration 
     */
    @Override
    protected void performForEveryRetrieval(Unit<DatasetInfo> configuration) {
        CalculationResults.StringResult result = new CalculationResults.StringResult(
                "EXTRA_SIGNAL_TEMPLATE", configuration.getValue0().toString());
        ResultsHolder.getResults().addResult(result, CalculationResults.Level.DEBUG);
    }
    
}
