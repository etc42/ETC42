/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.oldspatialbinning;

import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.oldpsffwhm.PsfFwhmFactory;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.StringResult;
import org.cnrs.lam.dis.etc.datamodel.Instrument;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.cnrs.lam.dis.etc.datamodel.Source;
import org.javatuples.Quartet;
import org.javatuples.Quintet;
import org.javatuples.Triplet;
import org.javatuples.Unit;

/**
 * <p>The {@code SpatialBinningFactory} provides calculators for calculating the
 * number of pixels of the detector affected in the spatial direction. The retrieved
 * calculators get as input the wavelength for
 * which the calculation is done (expressed in {@latex.inline \\AA}) and returns
 * the spatial binning expressed in {@latex.inline $pixels$}. The configuration
 * of the factory is a {@link org.cnrs.lam.dis.etc.datamodel.Session} object 
 * containing all the configuration from the user.</p>
 */
public class SpatialBinningFactory implements Factory<Unit<Session>, Unit<Double>, Unit<Double>> {

    @Override
    public Calculator<Unit<Double>, Unit<Double>> getCalculator(Unit<Session> configuration)
            throws InitializationException, ConfigurationException {
        Session session = configuration.getValue0();
        Instrument instrument = session.getInstrument();
        double pixelScale = session.getInstrument().getPixelScale();
        String pixelScaleUnit = session.getInstrument().getPixelScaleUnit();
        Instrument.InstrumentType instrumentType = instrument.getInstrumentType();
        Instrument.SpectrographType spectrographType = instrument.getSpectrographType();
        // If we have a fiber spectrograph we return the Fiber calculator
        if (instrumentType == instrumentType.SPECTROGRAPH
                && spectrographType == spectrographType.FIBER) {
            ResultsHolder.getResults().addResult(
                    new StringResult("SPATIAL_BINNING_METHOD", "Fiber"), Level.DEBUG);
            double fiberDiameter = instrument.getFiberDiameter();
            String fiberDiameterUnit = instrument.getFiberDiameterUnit();
            Quartet<Double, String, Double, String> calculatorConfiguration
                    = new Quartet(pixelScale, pixelScaleUnit, fiberDiameter, fiberDiameterUnit);
            return EtcCalculatorManager.getManager(Fiber.class).getCalculator(calculatorConfiguration);
        } else {
            Calculator<Unit<Double>, Unit<Double>> fwhmCalculator = 
                    new PsfFwhmFactory().getCalculator(configuration);
            Source source = session.getSource();
            Source.SpatialDistributionType spatialDistributionType = source.getSpatialDistributionType();
            if (spatialDistributionType == spatialDistributionType.POINT_SOURCE) {
                    ResultsHolder.getResults().addResult(
                            new StringResult("SPATIAL_BINNING_METHOD", "Point Source"), Level.DEBUG);
                    Triplet<Double, String, Calculator<Unit<Double>, Unit<Double>>> calculatorConfiguration
                            = new Triplet(pixelScale, pixelScaleUnit, fwhmCalculator);
                    return EtcCalculatorManager.getManager(PointSource.class).getCalculator(calculatorConfiguration);
            } else {
                    ResultsHolder.getResults().addResult(
                            new StringResult("SPATIAL_BINNING_METHOD", "Extended Source"), Level.DEBUG);
                    double sourceRadius = source.getExtendedSourceRadius();
                    String sourceRadiusUnit = source.getExtendedSourceRadiusUnit();
                    Quintet<Double, String, Calculator<Unit<Double>, Unit<Double>>, Double, String> calculatorConfiguration
                            = new Quintet(pixelScale, pixelScaleUnit, fwhmCalculator, sourceRadius, sourceRadiusUnit);
                    return EtcCalculatorManager.getManager(ExtendedSource.class).getCalculator(calculatorConfiguration);
            }
        }
    }
    
}
