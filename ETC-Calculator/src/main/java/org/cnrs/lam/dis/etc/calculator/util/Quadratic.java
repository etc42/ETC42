/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.util;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class Quadratic {

    // Solves ax^2+bx+c=0
    public static List<Double> solve(double a, double b, double c) {
        double delta = Math.pow(b, 2) - 4 * a * c;
        List<Double> result = new ArrayList<Double>();
        if (delta > 0) {
            result.add((-b - Math.sqrt(delta)) / (2 * a));
            result.add((-b + Math.sqrt(delta)) / (2 * a));
        } else {
            if (delta == 0) {
                result.add((-b) / (2 * a));
            }
        }
        return result;
    }
}
