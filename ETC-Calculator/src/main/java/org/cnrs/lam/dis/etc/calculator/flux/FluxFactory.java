/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.flux;

import java.util.ResourceBundle;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.cnrs.lam.dis.etc.datamodel.Source;
import org.javatuples.Pair;
import org.javatuples.Quartet;
import org.javatuples.Tuple;
import org.javatuples.Unit;

/**
 * The class {@link FluxFactory} is a factory for creating calculators for computing
 * functions representing the flux of the source.
 */
public class FluxFactory implements Factory<Unit<Session>, Tuple, Unit<UnivariateRealFunction>> {

    private static final ResourceBundle bundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");

    @Override
    public Calculator<Tuple, Unit<UnivariateRealFunction>> getCalculator(Unit<Session> configuration) 
            throws InitializationException, ConfigurationException {
        Session session = configuration.getValue0();
        Source source = session.getSource();
        double magnitude = source.getMagnitude();
        double redshift = source.getRedshift();
        double magnitudeWavelength = source.getMagnitudeWavelength();
        String magnitudeWavelengthUnit = source.getMagnitudeWavelengthUnit();
        Pair<Double, String> magnitudeWavelengthPair = new Pair<Double, String>(magnitudeWavelength, magnitudeWavelengthUnit);
        switch (source.getSpectralDistributionType()) {
            case TEMPLATE:
                ResultsHolder.getResults().addResult(
                        new CalculationResults.StringResult("SIGNAL_FLUX_METHOD", "Restframe Template"), CalculationResults.Level.DEBUG);
                DatasetInfo templateInfo = source.getSpectralDistributionTemplate();
                Quartet<Double, Double, Pair<Double, String>, DatasetInfo> templateConfiguration =
                        new Quartet<Double, Double, Pair<Double, String>, DatasetInfo>
                        (magnitude, redshift, magnitudeWavelengthPair, templateInfo);
                return EtcCalculatorManager.getManager(RestFrameTemplate.class).getCalculator(templateConfiguration);
            case EMISSION_LINE:
                ResultsHolder.getResults().addResult(
                        new CalculationResults.StringResult("SIGNAL_FLUX_METHOD", "Emission line"), CalculationResults.Level.DEBUG);
                double fluxEm = source.getEmissionLineFlux();
                String fluxEmUnit = source.getEmissionLineFluxUnit();
                Pair<Double, String> fluxEmPair = new Pair<Double, String>(fluxEm, fluxEmUnit);
                double lambdaEm = source.getEmissionLineWavelength();
                String lambdaEmUnit = source.getEmissionLineWavelengthUnit();
                Pair<Double, String> lambdaEmPair = new Pair<Double, String>(lambdaEm, lambdaEmUnit);
                double fwhmEm = source.getEmissionLineFwhm();
                String fwhmEmUnit = source.getEmissionLineFwhmUnit();
                Pair<Double, String> fwhmEmPair = new Pair<Double, String>(fwhmEm, fwhmEmUnit);
                Quartet<Pair<Double, String>, Pair<Double, String>, Pair<Double, String>, Double> emissionLineConfiguration =
                        new Quartet<Pair<Double, String>, Pair<Double, String>, Pair<Double, String>, Double>
                        (fluxEmPair, lambdaEmPair, fwhmEmPair, redshift);
                return EtcCalculatorManager.getManager(EmissionLine.class).getCalculator(emissionLineConfiguration);
            case FLAT:
                ResultsHolder.getResults().addResult(
                        new CalculationResults.StringResult("SIGNAL_FLUX_METHOD", "Flat photon flux"), CalculationResults.Level.DEBUG);
                Pair<Double, Pair<Double, String>> flatPhotonConfiguration =
                        new Pair<Double, Pair<Double, String>>(magnitude, magnitudeWavelengthPair);
                return EtcCalculatorManager.getManager(FlatPhotonFlux.class).getCalculator(flatPhotonConfiguration);
            case FLAT_ENERGY:
                ResultsHolder.getResults().addResult(
                        new CalculationResults.StringResult("SIGNAL_FLUX_METHOD", "Flat energy flux"), CalculationResults.Level.DEBUG);
                Unit<Double> flatEnergyConfiguration = new Unit<Double>(magnitude);
                return EtcCalculatorManager.getManager(FlatEnergyFlux.class).getCalculator(flatEnergyConfiguration);
            case BLACK_BODY:
                ResultsHolder.getResults().addResult(
                        new CalculationResults.StringResult("SIGNAL_FLUX_METHOD", "Black Body"), CalculationResults.Level.DEBUG);
                double temperature = source.getTemperature();
                String temperatureUnit = source.getTemperatureUnit();
                Pair<Double, String> temperaturePair = new Pair<Double, String>(temperature, temperatureUnit);
                Quartet<Double, Pair<Double, String>, Pair<Double, String>, Double> blackBodyConfiguration =
                        new Quartet<Double, Pair<Double, String>, Pair<Double, String>, Double>
                        (magnitude, magnitudeWavelengthPair, temperaturePair, redshift);
                return EtcCalculatorManager.getManager(BlackBody.class).getCalculator(blackBodyConfiguration);
        }
        String message = bundle.getString("UNKNOWN_FLUX_METHOD");
        throw new ConfigurationException(message);
    }
    
}
