/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.flux;

import java.text.MessageFormat;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TreeMap;
import lombok.EqualsAndHashCode;
import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.apache.log4j.Logger;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.DatasetProviderHolder;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.FluxUtil;
import org.cnrs.lam.dis.etc.calculator.util.FunctionToDatasetResultConverter;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.calculator.util.datasets.TemplateFunctionDataset;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.javatuples.Pair;
import org.javatuples.Quartet;
import org.javatuples.Tuple;
import org.javatuples.Unit;

/**
 * <p>The {@link RestFrameTemplate} class is a calculator for computing functions
 * representing the flux of the source from a given template at rest frame. The
 * given template is redshifted and normalized based on the AB magnitude. The
 * equation used for converting the AB magnitude to flux is:</p>
 * {@latex.inline $
 * F_x=10^{-\\frac{m_{AB}+48.6}{2.5}}*\\frac{c}{\\lambda_x^2}
 * $}
 * <p>where</p>
 * <ul>
 *   <li>{@latex.inline $F_x$}: is the flux on the reference wavelength</li>
 *   <li>{@latex.inline $\\lambda_x$}: is the reference wavelength expressed 
 *       in {@latex.inline \\AA}</li>
 *   <li>{@latex.inline $m_{AB}$}: is the AB magnitude</li>
 *   <li>{@latex.inline $c$}: is the speed of light expressed in {@latex.inline \\AA$/sec$}</li>
 * </ul>
 * 
 * <p>The configuration of the calculator is a {@link Quartet},
 * witch contains the following elements:</p>
 * <ol>
 *   <li>{@link Double}: The AB magnitude</li>
 *   <li>{@link Double}: The redshift</li>
 *   <li>{@link Pair}: The reference wavelength and its unit (in this order)</li>
 *   <li>{@link DatasetInfo}: The information of the rest frame template</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Tuple}, as this
 * calculator does not accept any input. It should always be null.</p>
 * 
 * <p>The output of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link UnivariateRealFunction} representing the flux of the source.</p>
 */
// Caching explenation
// -------------------
// This calculator is set to cache the instances because the configuration validation
// and the instantiation are retrieving the dataset, which is time consuming.
@Cacheable(CachingPolicy.INSTANCE)
@EqualsAndHashCode(callSuper=false, exclude={"fluxFunction"})
public class RestFrameTemplate extends AbstractCalculator<
        Quartet<Double, Double, Pair<Double, String>, DatasetInfo>,
        Tuple, Unit<UnivariateRealFunction>> {
    
    private static final Logger logger = Logger.getLogger(RestFrameTemplate.class);
    private static final ResourceBundle validationErrorsBundle =
            ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");
    private double magnitude;
    private double redshift;
    private double magnitudeWavelength;
    private DatasetInfo templateDatasetInfo;
    private TemplateFunctionDataset fluxFunction;

    /**
     * Validates the configuration. It requires that the redshift is non negative,
     * the reference wavelength positive and expressed in Angstrom and that the
     * rest frame template exists, its X axis unit is Angstrom and its Y axis
     * unit is erg/cm2/sec/Angstrom.
     * @param configuration
     * @throws ConfigurationException 
     */
    @Override
    protected void validateConfiguration(Quartet<Double, Double, Pair<Double, String>, DatasetInfo> configuration) throws ConfigurationException {
        // Check that the redshift is a non negative number
        if (configuration.getValue1() < 0) {
            String message = validationErrorsBundle.getString("REDSHIFT_NEGATIVE");
            message = MessageFormat.format(message, configuration.getValue0());
            throw new ConfigurationException(message);
        }
        // Check that the magnitude wavelength is a positive number
        if (configuration.getValue2().getValue0() <= 0) {
            String message = validationErrorsBundle.getString("MAGNITUDE_WAVELENGTH_NOT_POSITIVE");
            message = MessageFormat.format(message, configuration.getValue2().getValue0());
            throw new ConfigurationException(message);
        }
        // Check if the magnitude wavelength is in angstrom
        if (!Units.isAngstrom(configuration.getValue2().getValue1())) {
            String message = validationErrorsBundle.getString("MAGNITUDE_WAVELENGTH_RADIUS_WRONG_UNIT");
            message = MessageFormat.format(message, Units.ANGSTROM, configuration.getValue2().getValue1());
            throw new ConfigurationException(message);
        }
        // Check the template dataset
        DatasetInfo info = configuration.getValue3();
        if (info == null) {
            String message = validationErrorsBundle.getString("RESTFRAME_TEMPLATE_MISSING");
            throw new ConfigurationException(message);
        }
        Dataset dataset = DatasetProviderHolder.getDatasetProvider().getDataset(Dataset.Type.SPECTRAL_DIST_TEMPLATE, info);
        if (dataset == null) {
            String message = validationErrorsBundle.getString("RESTFRAME_TEMPLATE_NOT_AVAILABLE");
            message = MessageFormat.format(message, info);
            throw new ConfigurationException(message);
        }
        if (!Units.isAngstrom(dataset.getXUnit())) {
            String message = validationErrorsBundle.getString("RESTFRAME_TEMPLATE_WRONG_X_UNIT");
            message = MessageFormat.format(message, Units.ANGSTROM, dataset.getXUnit());
            throw new ConfigurationException(message);
        }
        if (dataset.getDataType() == Dataset.DataType.FUNCTION && !Units.isErgPerCm2PerSecPerAngstrom(dataset.getYUnit())) {
            String message = validationErrorsBundle.getString("RESTFRAME_TEMPLATE_WRONG_Y_UNIT");
            message = MessageFormat.format(message, Units.getErgPerCm2PerSecPerAngstrom(), dataset.getYUnit());
            throw new ConfigurationException(message);
        }
        if (dataset.getDataType() == Dataset.DataType.TEMPLATE && !Units.isErgPerCm2PerSec(dataset.getYUnit())) {
            String message = validationErrorsBundle.getString("RESTFRAME_TEMPLATE_WRONG_Y_UNIT");
            message = MessageFormat.format(message, Units.getErgPerCm2PerSec(), dataset.getYUnit());
            throw new ConfigurationException(message);
        }
    }

    /**
     * Initializes the calculator instance with the given configuration.
     * @param configuration the configuration to use for initialization
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(Quartet<Double, Double, Pair<Double, String>, DatasetInfo> configuration) throws InitializationException {
        magnitude = configuration.getValue0();
        redshift = configuration.getValue1();
        magnitudeWavelength = configuration.getValue2().getValue0();
        templateDatasetInfo = configuration.getValue3();
        Dataset dataset = DatasetProviderHolder.getDatasetProvider().getDataset(Dataset.Type.SPECTRAL_DIST_TEMPLATE, templateDatasetInfo);
        try {
            fluxFunction = redshiftAndNormalizeTemplate(dataset.getData(), magnitude, magnitudeWavelength, redshift);
        } catch (FunctionEvaluationException ex) {
            logger.warn("Failed to redshift and normalize the source template", ex);
            throw new InitializationException("Failed to redshift and normalize the source template", ex);
        }
    }

    /**
     * Redshifts and normalizes the template represented by the given data.
     * @param data
     * @param magnitude
     * @param magnitudeWavelength
     * @param redshift
     * @return
     * @throws FunctionEvaluationException 
     */
    private TemplateFunctionDataset redshiftAndNormalizeTemplate(
            Map<Double, Double> data, double magnitude, double magnitudeWavelength, double redshift)
            throws FunctionEvaluationException {
        // Calculate the normalization factor
        double abFlux = FluxUtil.convertMagnitudeToFlux(magnitude, magnitudeWavelength);
        double redshiftedRefLambda = magnitudeWavelength / (1 + redshift);
        TemplateFunctionDataset restframe = new TemplateFunctionDataset(data);
        double templateFlux = restframe.value(redshiftedRefLambda);
        double normalizationFactor = abFlux / templateFlux;
        // Now go through the points and calcualte the redshifted normalized values
        Map<Double, Double> newData = new TreeMap<Double, Double>();
        for (Map.Entry<Double, Double> entry : data.entrySet()) {
            Double lambda = entry.getKey();
            Double flux = entry.getValue();
            double redshiftedLambda = lambda * (1 + redshift);
            double normalizedFlux = flux * normalizationFactor;
            newData.put(redshiftedLambda, normalizedFlux);
        }
        return new TemplateFunctionDataset(newData);
    }

    /**
     * Returns a {@link UnivariateRealFunction} which can be used to calculate
     * the source flux.
     * @param input The calculator has no input, so it can be always null
     * @return A {@link UnivariateRealFunction} which can be used to calculate
     * the source flux
     * @throws CalculationException if there is any error during the calculation
     */
    @Override
    protected Unit<UnivariateRealFunction> performCalculation(Tuple input) throws CalculationException {
        return new Unit<UnivariateRealFunction>(fluxFunction);
    }

    /**
     * <p>For every calculation the calculator adds in the simulation results
     * the following outputs of the calculator:</p>
     * <ul>
     *   <li>The source flux in intermediate unimportant results with code
     *       SIGNAL_FLUX</li>
     * </ul>
     * @param input
     * @param output 
     */
    @Override
    protected void performForEveryCalculation(Tuple input, Unit<UnivariateRealFunction> output) {
        // If the result already exists we do not add it to save time
        if (ResultsHolder.getResults().getResultByName("SIGNAL_FLUX") != null) {
            return;
        }
        UnivariateRealFunction function = output.getValue0();
        CalculationResults.DoubleDatasetResult result = FunctionToDatasetResultConverter.convert(function,
                "SIGNAL_FLUX", Units.ANGSTROM, Units.getErgPerCm2PerSecPerAngstrom());
        ResultsHolder.getResults().addResult(result, CalculationResults.Level.INTERMEDIATE_UNIMPORTANT);
    }

    /**
     * <p>For every calculator instantiation the following parts of its configuration
     * are added in the simulation results:</p>
     * <ul>
     *   <li>The AB magnitude of the source in debug results with code
     *       SOURCE_AB_MAGNITUDE</li>
     *   <li>The redshift of the source in debug results with code REDSHIFT</li>
     *   <li>The reference wavelength of the AB magnitude in debug results with
     *       code SOURCE_AB_MAGNITUDE_FILTER</li>
     *   <li>The rest frame template name in debug results with
     *       code SOURCE_RESTFRAME_TEMPLATE</li>
     * </ul>
     * @param configuration 
     */
    @Override
    protected void performForEveryRetrieval(Quartet<Double, Double, Pair<Double, String>, DatasetInfo> configuration) {
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("SOURCE_AB_MAGNITUDE"
                , configuration.getValue0(), null), CalculationResults.Level.DEBUG);
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("REDSHIFT"
                , configuration.getValue1(), null), CalculationResults.Level.DEBUG);
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("SOURCE_AB_MAGNITUDE_FILTER"
                , configuration.getValue2().getValue0(), configuration.getValue2().getValue1()), CalculationResults.Level.DEBUG);
        ResultsHolder.getResults().addResult(new CalculationResults.StringResult("SOURCE_RESTFRAME_TEMPLATE"
                , configuration.getValue3().toString()), CalculationResults.Level.DEBUG);
    }
    
}
