/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.util.functions;

import lombok.EqualsAndHashCode;
import org.apache.commons.math.FunctionEvaluationException;

/**
 * <p>The {@link Gaussian} class represents a scaled normal distribution with
 * peak value for X=0. Its values are given by the equation:</p>
 * {@latex.inline $
 * f{(x)}=\\displaystyle\\frac{scale}{\\sigma\\sqrt{2\\pi}}
 * e^{-\\frac{1}{2} \\left( \\frac{x}{\\sigma}} \\right) ^2
 * $}
 * <p> where {@latex.inline $\\sigma$} is the standard deviation and
 * {@latex.inline $scale$} is the factor to scale the Gaussian with. This function
 * is integrable and it calculates the integral in a fast way by using the error
 * function.</p>
 */
@EqualsAndHashCode(callSuper=false, of={"standardDeviation", "scale"})
public class Gaussian implements IntegrableUnivariateFunction {
    
    private final double standardDeviation; // sigma
    private final double scale; // The scale factor
    private final double variance; // sigma^2
    private static final double sqrtTwo = Math.sqrt(2.);
    private static final double sqrtTwoPi = sqrtTwo * Math.sqrt(Math.PI);
    private final double multiplier; // scale/(sigma*sqrt(2*pi))
    private final ErrorFunction errorFunction;
    
    /**
     * Creates a new {@link Gaussian} which has a peak at X = 0, the given
     * standard deviation {@latex.inline $\\sigma$} and scale factor = 1.
     * @param standardDeviation The standard deviation {@latex.inline $\\sigma$}
     */
    public Gaussian(double standardDeviation) {
        this(standardDeviation, 1.);
    }

    /**
     * Creates a new {@link Gaussian} which has a peak at X = 0 and the given
     * standard deviation {@latex.inline $\\sigma$} and scale factor.
     * @param standardDeviation The standard deviation {@latex.inline $\\sigma$}
     * @param scale The scale factor to multiply the Gaussian with
     */
    public Gaussian(double standardDeviation, double scale) {
        this.standardDeviation = standardDeviation;
        this.scale = scale;
        this.variance = standardDeviation * standardDeviation;
        this.multiplier = scale / (standardDeviation * sqrtTwoPi);
        this.errorFunction = ErrorFunction.getCommonsMathErf();
    }
    
    /**
     * Returns the standard deviation {@latex.inline $\\sigma$} of the gaussian.
     * @return The standard deviation
     */
    public double getStandardDeviation() {
        return standardDeviation;
    }
    
    /**
     * Returns the scale factor with which the Gaussian is multiplied with.
     * @return 
     */
    public double getScale() {
        return scale;
    }

    /**
     * Returns the value of the Gaussian for the distance x from the location of
     * the peak.
     * @param x The distance from the peak
     * @return The value of the Gaussian
     * @throws FunctionEvaluationException 
     */
    @Override
    public double value(double x) throws FunctionEvaluationException {
        double value = multiplier * Math.exp(-0.5 * x * x / variance);
        return value;
    }

    /**
     * Calculates the integral of the Gaussian by calculating the CDF of the two
     * points by using the error function.
     * @param x1 The low bound of the interval
     * @param x2 The high bound of the interval
     * @return the integral in the interval [x1,x2]
     * @throws FunctionEvaluationException if the integral evaluation fails
     * @throws IllegalArgumentException if x1 > x2
     */
    @Override
    public double integral(double x1, double x2) throws FunctionEvaluationException {
        // First check the x1 and x2. Note that if they are equal the integral is 0.
        if (x1 > x2) {
            throw new IllegalArgumentException("Illegal integration range [" + x1 + "," + x2 + "]");
        }
        if (x1 == x2) {
            return 0;
        }
        double integral = cdf(x2) - cdf(x1);
        // Scale the integral according the scale factor
        integral = scale * integral;
        return integral;
    }
    
    private double cdf(double x) throws FunctionEvaluationException {
        double cdf = 0.5 * (1 + errorFunction.value(x / (sqrtTwo * standardDeviation)));
        return cdf;
    }
    
}
