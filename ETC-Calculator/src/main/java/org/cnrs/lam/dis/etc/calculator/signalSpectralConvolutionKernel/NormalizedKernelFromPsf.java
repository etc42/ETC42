/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.signalSpectralConvolutionKernel;

import java.text.MessageFormat;
import java.util.ResourceBundle;
import lombok.EqualsAndHashCode;
import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.analysis.BivariateRealFunction;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.apache.log4j.Logger;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.Cacheable;
import org.cnrs.lam.cesam.util.calculator.CachingPolicy;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.Calculator;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.FunctionToDatasetResultConverter;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.calculator.util.functions.BoundedUnivariateFunction;
import org.cnrs.lam.dis.etc.calculator.util.functions.CircularlySymmetricBivariateFunction;
import org.cnrs.lam.dis.etc.calculator.util.functions.IntegrableUnivariateFunction;
import org.cnrs.lam.dis.etc.calculator.util.integration.IntegrationTool;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.javatuples.Pair;
import org.javatuples.Quartet;
import org.javatuples.Tuple;
import org.javatuples.Unit;

/**
 */
@Cacheable(CachingPolicy.INSTANCE)
@EqualsAndHashCode(callSuper=false, of={"profileCalculator","kernelSizeCalculator","spectralResolutionCalculator","pixelScale"})
public class NormalizedKernelFromPsf extends AbstractCalculator<
        Quartet<Calculator<Unit<Double>, Unit<BivariateRealFunction>>,
                Calculator<Unit<Double>, Unit<Double>>,
                Calculator<Tuple, Unit<UnivariateRealFunction>>,
                Pair<Double, String>>,
        Unit<Double>, Unit<BoundedUnivariateFunction>> {

    private static final Logger logger = Logger.getLogger(NormalizedKernelFromPsf.class);
    private static final ResourceBundle validationErrorsBundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");
    
    private Calculator<Unit<Double>, Unit<BivariateRealFunction>> profileCalculator;
    private Calculator<Unit<Double>, Unit<Double>> kernelSizeCalculator;
    private Calculator<Tuple, Unit<UnivariateRealFunction>> spectralResolutionCalculator;
    private double pixelScale;
    
    private UnivariateRealFunction spectralResolutionFunction;
    
    /**
     * Validates the given configuration. It requires that the pixel scale is
     * positive and that its unit is {@latex.inline $arcsec/pixel$}.
     * @param configuration The configuration to validate
     * @throws ConfigurationException If the configuration is invalid
     */
    @Override
    protected void validateConfiguration(
            Quartet<Calculator<Unit<Double>, Unit<BivariateRealFunction>>,
                    Calculator<Unit<Double>, Unit<Double>>,
                    Calculator<Tuple, Unit<UnivariateRealFunction>>,
                    Pair<Double, String>> configuration) throws ConfigurationException {
        // Check that the pixel scale is a positive number
        if (configuration.getValue3().getValue0() <= 0) {
            String message = validationErrorsBundle.getString("PIXEL_SCALE_NOT_POSITIVE");
            message = MessageFormat.format(message, configuration.getValue3().getValue0());
            throw new ConfigurationException(message);
        }
        // Check if the pixel scale is in arcsec/pixel
        if (!Units.getArcsecPerPixel().equals(configuration.getValue3().getValue1())) {
            String message = validationErrorsBundle.getString("PIXEL_SCALE_WRONG_UNIT");
            message = MessageFormat.format(message, Units.getArcsecPerPixel(), configuration.getValue3().getValue1());
            throw new ConfigurationException(message);
        }
    }

    /**
     * Initializes the calculator instance with the given configuration.
     * @param configuration the configuration to use for initialization
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(
            Quartet<Calculator<Unit<Double>, Unit<BivariateRealFunction>>,
                    Calculator<Unit<Double>, Unit<Double>>,
                    Calculator<Tuple, Unit<UnivariateRealFunction>>,
                    Pair<Double, String>> configuration) throws InitializationException {
        profileCalculator = configuration.getValue0();
        kernelSizeCalculator = configuration.getValue1();
        spectralResolutionCalculator = configuration.getValue2();
        pixelScale = configuration.getValue3().getValue0();
        try {
            spectralResolutionFunction = spectralResolutionCalculator.calculate(null).getValue0();
        } catch (CalculationException ex) {
            logger.error("Failed to retrieve the spectral resolution calculator.\nReason: " + ex.getMessage(), ex);
            throw new InitializationException("Failed to retrieve the spectral resolution calculator.\nReason: " + ex.getMessage(), ex);
        }
    }

    @Override
    protected Unit<BoundedUnivariateFunction> performCalculation(Unit<Double> input) throws CalculationException {
        double lambda = input.getValue0();
        BivariateRealFunction profile2D = profileCalculator.calculate(input).getValue0();
        if (!(profile2D instanceof CircularlySymmetricBivariateFunction)) {
            logger.error("Singal spectral convolution kernel calculation is implemented only for symmetric PSF");
            throw new CalculationException("Singal spectral convolution kernel calculation is implemented only for symmetric PSF");
        }
        UnivariateRealFunction profile = ((CircularlySymmetricBivariateFunction) profile2D).projectionFunction();
        double kernelSize = kernelSizeCalculator.calculate(input).getValue0();
        double spectralResolution = 0;
        KernelFunction kernelFunction = null;
        try {
            spectralResolution = spectralResolutionFunction.value(lambda);
            kernelFunction = new KernelFunction(lambda, profile, kernelSize, spectralResolution, pixelScale);
        } catch (FunctionEvaluationException ex) {
            logger.error(ex.getMessage(), ex);
            throw new CalculationException(ex.getMessage());
        }
        return new Unit<BoundedUnivariateFunction>(kernelFunction);
    }
    
    private class KernelFunction implements BoundedUnivariateFunction, IntegrableUnivariateFunction {
        
        double lambda;
        UnivariateRealFunction profile;
        double kernelSize;
        double spectralResolution;
        double pixelScale;
        double wavelengthConverter;
        double normalizer;

        public KernelFunction(double lambda, UnivariateRealFunction profile, double kernelSize, double spectralResolution, double pixelScale) throws FunctionEvaluationException {
            this.lambda = lambda;
            this.profile = profile;
            this.kernelSize = kernelSize;
            this.spectralResolution = spectralResolution;
            this.pixelScale = pixelScale;
            
            wavelengthConverter = pixelScale * spectralResolution / lambda;
            normalizer = 1 / IntegrationTool.univariateIntegral(profile, -kernelSize * wavelengthConverter / 2, kernelSize * wavelengthConverter / 2);
        }

        @Override
        public Pair<Double, Double> getBounds() {
            return new Pair<Double, Double>(lambda - kernelSize, lambda + kernelSize);
        }

        @Override
        public Pair<Double, Double> getNonZeroBounds() {
            return new Pair<Double, Double>(lambda - kernelSize/2, lambda + kernelSize/2);
        }

        @Override
        public double value(double x) throws FunctionEvaluationException {
            if (x < lambda - kernelSize/2 || x > lambda + kernelSize/2) {
                return 0;
            }
            return normalizer * profile.value((x - lambda) * wavelengthConverter);
        }

        @Override
        public double integral(double x1, double x2) throws FunctionEvaluationException {
            if (x1 > x2) {
                throw new FunctionEvaluationException(new double[] {x1,x2});
            }
            if (x1 == x2 || x2 < lambda - kernelSize/2 || x1 > lambda + kernelSize/2) {
                return 0;
            }
            double lowLim = (Math.max(x1, lambda - kernelSize/2) - lambda) * wavelengthConverter;
            double highLim = (Math.min(x2, lambda + kernelSize/2) - lambda) * wavelengthConverter;
            double integral = normalizer * IntegrationTool.univariateIntegral(profile, lowLim, highLim);
            return integral;
        }
        
    }

    /**
     * <p>For every calculation the calculator adds in the simulation results
     * the following outputs of the calculator:</p>
     * <ul>
     *   <li>The signal convolution kernel in intermediate unimportant results
     *       with code SIGNAL_SPECTRAL_CONVOLUTION_KERNEL</li>
     * </ul>
     * @param input
     * @param output 
     */
    @Override
    protected void performForEveryCalculation(Unit<Double> input, Unit<BoundedUnivariateFunction> output) {
        double lambda  = input.getValue0();
        BoundedUnivariateFunction function = output.getValue0();
        double min = function.getBounds().getValue0();
        double miax = function.getBounds().getValue1();
        CalculationResults.DoubleDatasetResult result =
                FunctionToDatasetResultConverter.convert(min, miax, function,
                "SIGNAL_SPECTRAL_CONVOLUTION_KERNEL", Units.ANGSTROM, null);
        ResultsHolder.getResults().addResult("SIGNAL_SPECTRAL_CONVOLUTION_KERNEL",
                lambda, result, Units.ANGSTROM, CalculationResults.Level.INTERMEDIATE_UNIMPORTANT);
    }

    /**
     * <p>For every calculator instantiation the following parts of its configuration
     * are added in the simulation results:</p>
     * <ul>
     *   <li>The pixel scale in debug results with code PIXEL_SCALE</li>
     * </ul>
     * @param configuration The configuration of the calculator
     */
    @Override
    protected void performForEveryRetrieval(
            Quartet<Calculator<Unit<Double>, Unit<BivariateRealFunction>>,
                    Calculator<Unit<Double>, Unit<Double>>,
                    Calculator<Tuple, Unit<UnivariateRealFunction>>,
                    Pair<Double, String>> configuration) {
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("PIXEL_SCALE",
                configuration.getValue3().getValue0(), configuration.getValue3().getValue1()), CalculationResults.Level.DEBUG);
    }

}
