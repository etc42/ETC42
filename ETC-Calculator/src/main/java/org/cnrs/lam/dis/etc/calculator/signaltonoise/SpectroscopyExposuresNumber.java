/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.signaltonoise;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TreeMap;
import lombok.EqualsAndHashCode;
import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.apache.log4j.Logger;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.Calculator;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.calculator.util.functions.BoundedUnivariateFunction;
import org.cnrs.lam.dis.etc.configuration.ConfigFactory;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.javatuples.Ennead;
import org.javatuples.Octet;
import org.javatuples.Pair;
import org.javatuples.Tuple;
import org.javatuples.Unit;

/**
 * <p>The class {@link SpectroscopyExposuresNumber} is a calculator for computing
 * the signal to noise (SNR) for the case of spectrograph simulation.</p>
 * 
 * <p>The configuration of the calculator is a {@link Ennead},
 * which contains the following elements:</p>
 * <ol>
 *   <li>{@link Calculator}: A calculator for computing the delta lambda</li>
 *   <li>{@link Pair}: A tuple containing the following:</li>
 *   <ol>
 *     <li>{@link Calculator}: A calculator for computing the simulated signal</li>
 *     <li>{@link Calculator}: A calculator for computing the extra signal</li>
 *   </ol>
 *   <li>{@link Pair}: A tuple containing the following:</li>
 *   <ul>
 *     <li>{@link Calculator}: A calculator for computing the simulated background</li>
 *     <li>{@link Calculator}: A calculator for computing the extra background</li>
 *   </ul>
 *   <li>{@link Calculator}: A calculator for computing the number of pixels</li>
 *   <li>{@link Pair}: The dark current and its unit</li>
 *   <li>{@link Pair}: The redout noise and its unit</li>
 *   <li>{@link Integer}: The number of exposures</li>
 *   <li>{@link Pair}: The exposure time and its unit</li>
 *     <li>{@link Calculator}: A calculator for computing the wavelength range</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Tuple}, as this
 * calculator does not accept any input. It should always be null.</p>
 * 
 * <p>The output of the calculator is an {@link Octet},
 * which contains the following elements:</p>
 * <ol>
 *   <li>{@link Map}: The central pixel SNR</li>
 *   <li>{@link Map}: The SNR</li>
 *   <li>{@link Map}: The central pixel total signal</li>
 *   <li>{@link Map}: The total signal</li>
 *   <li>{@link Map}: The central pixel total background</li>
 *   <li>{@link Map}: The total background</li>
 *   <li>{@link Map}: The central pixel total detector noise</li>
 *   <li>{@link Map}: The total detector noise</li>
 * </ol>
 */
@EqualsAndHashCode(callSuper=false, of={"deltaLambda","simulatedSignal","extraSignalCalculator",
        "simulatedBackground","extraBackgroundCalculator","numberOfPixels","dark","readout",
        "noExpo","exposureTime","wavelengthRange"})
public class SpectroscopyExposuresNumber extends AbstractCalculator<
        Ennead<
            Calculator<Unit<Double>, Unit<Double>>,
            Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>,
                 Calculator<Tuple, Unit<BoundedUnivariateFunction>>>,
            Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>,
                 Calculator<Tuple, Unit<BoundedUnivariateFunction>>>,
            Calculator<Unit<Double>, Unit<Double>>,
            Pair<Double, String>, Pair<Double, String>,
            Integer, Pair<Double, String>,
            Calculator<Tuple, Pair<Double, Double>>
        >, Tuple, 
        Octet<
            Map<Double, Double>, Map<Double, Double>,
            Map<Double, Double>, Map<Double, Double>,
            Map<Double, Double>, Map<Double, Double>,
            Map<Double, Double>, Map<Double, Double>
        >> {
    
    
    private static final Logger logger = Logger.getLogger(SpectroscopyExposuresNumber.class);
    private static final ResourceBundle validationErrorsBundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");
    
    private Calculator<Unit<Double>, Unit<Double>> deltaLambda;
    private Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>> simulatedSignal;
    private Calculator<Tuple, Unit<BoundedUnivariateFunction>> extraSignalCalculator;
    private Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>> simulatedBackground;
    private Calculator<Tuple, Unit<BoundedUnivariateFunction>> extraBackgroundCalculator;
    private Calculator<Unit<Double>, Unit<Double>> numberOfPixels;
    private double dark;
    private double readout;
    private double noExpo;
    private double exposureTime;
    private Calculator<Tuple, Pair<Double, Double>> wavelengthRange;

    private UnivariateRealFunction cpSimulatedSignalFunction;
    private UnivariateRealFunction simulatedSignalFunction;
    private UnivariateRealFunction cpSimulatedBackgroundFunction;
    private UnivariateRealFunction simulatedBackgroundFunction;
    private BoundedUnivariateFunction extraSignalFunction;
    private BoundedUnivariateFunction extraBackgroundFunction;
    private double rangeMin;
    private double rangeMax;
    private double detectorPerPixel;
    
    /**
     * Validates the given configuration. It requires that the dark current is 
     * non negative and expressed in e/pix/sec, that the readout noise is non
     * negative and expressed in e/pix,that the number of the exposures is positive
     * and that the exposure time is non negative and expressed in seconds.
     * @param configuration The configuration to validate
     * @throws ConfigurationException if the configuration is invalid
     */
    @Override
    protected void validateConfiguration(Ennead<
            Calculator<Unit<Double>, Unit<Double>>,
            Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>,
                 Calculator<Tuple, Unit<BoundedUnivariateFunction>>>,
            Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>,
                 Calculator<Tuple, Unit<BoundedUnivariateFunction>>>,
            Calculator<Unit<Double>, Unit<Double>>,
            Pair<Double, String>, Pair<Double, String>,
            Integer, Pair<Double, String>,
            Calculator<Tuple, Pair<Double, Double>>> configuration) throws ConfigurationException {
        // Check that the dark current is a non negative number
        if (configuration.getValue4().getValue0() < 0) {
            String message = validationErrorsBundle.getString("DARK_CURRENT_NEGATIVE");
            message = MessageFormat.format(message, configuration.getValue4().getValue0());
            throw new ConfigurationException(message);
        }
        // Check if the dark current is in e/pix/sec
        if (!Units.isElectronsPerPixelPerSec(configuration.getValue4().getValue1())) {
            String message = validationErrorsBundle.getString("DARK_CURRENT_WRONG_UNIT");
            message = MessageFormat.format(message, Units.getElectronsPerPixelPerSec(), configuration.getValue4().getValue1());
            throw new ConfigurationException(message);
        }
        // Check that the readout noise is a non negative number
        if (configuration.getValue5().getValue0() < 0) {
            String message = validationErrorsBundle.getString("READOUT_NOISE_NEGATIVE");
            message = MessageFormat.format(message, configuration.getValue5().getValue0());
            throw new ConfigurationException(message);
        }
        // Check if the readout noise is in e/pix
        if (!Units.isElectronsPerPixel(configuration.getValue5().getValue1())) {
            String message = validationErrorsBundle.getString("READOUT_NOISE_WRONG_UNIT");
            message = MessageFormat.format(message, Units.getElectronsPerPixel(), configuration.getValue5().getValue1());
            throw new ConfigurationException(message);
        }
        // Check that the number of exposures is positive
        if (configuration.getValue6() <= 0) {
            String message = validationErrorsBundle.getString("NUMBER_OF_EXPOSURES_NOT_POSITIVE");
            message = MessageFormat.format(message, configuration.getValue6());
            throw new ConfigurationException(message);
        }
        // Check that the exposure time is a non negative number
        if (configuration.getValue7().getValue0() < 0) {
            String message = validationErrorsBundle.getString("EXPOSURE_TIME_NEGATIVE");
            message = MessageFormat.format(message, configuration.getValue7().getValue0());
            throw new ConfigurationException(message);
        }
        // Check if the exposure time is in seconds
        if (!Units.isSec(configuration.getValue7().getValue1())) {
            String message = validationErrorsBundle.getString("EXPOSURE_TIME_WRONG_UNIT");
            message = MessageFormat.format(message, Units.SEC, configuration.getValue7().getValue1());
            throw new ConfigurationException(message);
        }
    }

    /**
     * Initializes the calculator instance with the given configuration.
     * @throws InitializationException 
     */
    @Override
    protected void initialize(Ennead<
            Calculator<Unit<Double>, Unit<Double>>,
            Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>,
                 Calculator<Tuple, Unit<BoundedUnivariateFunction>>>,
            Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>,
                 Calculator<Tuple, Unit<BoundedUnivariateFunction>>>,
            Calculator<Unit<Double>, Unit<Double>>,
            Pair<Double, String>, Pair<Double, String>,
            Integer, Pair<Double, String>,
            Calculator<Tuple, Pair<Double, Double>>> configuration) throws InitializationException {
        deltaLambda = configuration.getValue0();
        simulatedSignal = configuration.getValue1().getValue0();
        extraSignalCalculator = configuration.getValue1().getValue1();
        simulatedBackground = configuration.getValue2().getValue0();
        extraBackgroundCalculator = configuration.getValue2().getValue1();
        numberOfPixels = configuration.getValue3();
        dark = configuration.getValue4().getValue0();
        readout = configuration.getValue5().getValue0();
        noExpo = configuration.getValue6();
        exposureTime = configuration.getValue7().getValue0();
        wavelengthRange = configuration.getValue8();

        
        
        // Calculate the minimum and maximum of the wavelength range
        Pair<Double, Double> minMax = null;
        try {
            minMax = wavelengthRange.calculate(null);
        } catch (CalculationException e) {
            logger.error("Failed to calculate the wavelength range", e);
            throw new InitializationException(e.getMessage(), e);
        }
        rangeMin = minMax.getValue0();
        rangeMax = minMax.getValue1();
        
        // Retrieve the calculators for the simulated signal
        Pair<UnivariateRealFunction, UnivariateRealFunction> simSignalFunctionsPair = null;
        try {
            simSignalFunctionsPair = simulatedSignal.calculate(new Pair<Double, Double>(rangeMin, rangeMax));
        } catch (CalculationException ex) {
            logger.error(ex.getMessage(), ex);
            throw new InitializationException(ex.getMessage(), ex);
        }
        cpSimulatedSignalFunction = simSignalFunctionsPair.getValue0();
        simulatedSignalFunction = simSignalFunctionsPair.getValue1();
        
        // Retrieve the calculators for the simulated background
        Pair<UnivariateRealFunction, UnivariateRealFunction> simBackgroundFunctionsPair = null;
        try {
            simBackgroundFunctionsPair = simulatedBackground.calculate(new Pair<Double, Double>(rangeMin, rangeMax));
        } catch (CalculationException ex) {
            logger.error(ex.getMessage(), ex);
            throw new InitializationException(ex.getMessage(), ex);
        }
        cpSimulatedBackgroundFunction = simBackgroundFunctionsPair.getValue0();
        simulatedBackgroundFunction = simBackgroundFunctionsPair.getValue1();
        
        // Retrieve the calculators for the extra signal and background
        try {
            extraSignalFunction = extraSignalCalculator.calculate(null).getValue0();
        } catch (CalculationException ex) {
            logger.error("Failed to create the extra signal function", ex);
            throw new InitializationException(ex.getMessage(), ex);
        }
        try {
            extraBackgroundFunction = extraBackgroundCalculator.calculate(null).getValue0();
        } catch (CalculationException ex) {
            logger.error("Failed to create the extra background function", ex);
            throw new InitializationException(ex.getMessage(), ex);
        }
        
        // calcualte the detector noise per pixel
        detectorPerPixel = dark * exposureTime + noExpo * readout * readout;
    }

    /**
     * Returns a {@link org.javatuples.Octet} containing the signal to noise,
     * the total signal, the total background noise and the total detector noise,
     * both for the central pixel and the total affected area.
     * @param input The wavelength expressed in {@latex.inline \\AA}
     * @return
     * @throws CalculationException  if there is any error during the calculation
     */
    @Override
    protected Octet<
                    Map<Double, Double>, Map<Double, Double>,
                    Map<Double, Double>, Map<Double, Double>,
                    Map<Double, Double>, Map<Double, Double>,
                    Map<Double, Double>, Map<Double, Double>
              > performCalculation(Tuple input) throws CalculationException {
        // First build the list of the wavelengths we do the calculation for
        List<Unit<Double>> lambdaList = new ArrayList<Unit<Double>>();
        for (double lambda = rangeMin;
                lambda <= rangeMax;
                lambda += deltaLambda.calculate(new Unit<Double>(lambda)).getValue0()) {
            lambdaList.add(new Unit<Double>(lambda));
        }
        // Create the maps where the results are stored
        Map<Double, Double> signalToNoiseMap = new TreeMap<Double, Double>();
        Map<Double, Double> cpSignalToNoiseMap = new TreeMap<Double, Double>();
        Map<Double, Double> totalSignalMap = new TreeMap<Double, Double>();
        Map<Double, Double> cpTotalSignalMap = new TreeMap<Double, Double>();
        Map<Double, Double> totalBackgroundNoiseMap = new TreeMap<Double, Double>();
        Map<Double, Double> cpTotalBackgroundNoiseMap = new TreeMap<Double, Double>();
        Map<Double, Double> totalDetectorNoiseMap = new TreeMap<Double, Double>();
        Map<Double, Double> cpTotalDetectorNoiseMap = new TreeMap<Double, Double>();
        // We go through the wavelengths and we calculate the SNR. Note that the
        // signal and background noise are considered to be constant for such small
        // wavelength steps
        for (Unit<Double> lambda : lambdaList) {
            // We get the wavelength step of the calculation
            double dl = deltaLambda.calculate(lambda).getValue0();
            // We calculate the number of pixels
            double npix = numberOfPixels.calculate(lambda).getValue0();
            // We calculate the total signal for the central pixel and the total pixel coverage
            double cpSimulatedSignalValue = 0;
            double simulatedSignalValue = 0;
            try {
                if (ConfigFactory.getConfig().getCentralPixelFlag()) {
                    cpSimulatedSignalValue = cpSimulatedSignalFunction.value(lambda.getValue0());
                }
                simulatedSignalValue = simulatedSignalFunction.value(lambda.getValue0());
            } catch (FunctionEvaluationException ex) {
                logger.error(ex.getMessage(), ex);
                throw new CalculationException(ex.getMessage());
            }
            Double extraSignal = null;
            try {
                extraSignal = extraSignalFunction.value(lambda.getValue0());
            } catch (FunctionEvaluationException ex) {
                logger.error("Failed to calculate the extra signal value", ex);
                throw new CalculationException(ex.getMessage());
            }
            Double cpExtraSignal = extraSignal / npix;
            double centralPixelSignalPerLambda = (cpSimulatedSignalValue + extraSignal) * exposureTime;
            double signalPerLambda = (simulatedSignalValue + cpExtraSignal) * exposureTime;
            double cpS = centralPixelSignalPerLambda * dl;
            double s = signalPerLambda * dl;
            // We calculate the total background for the central pixel and the total pixel coverage
            double cpSimulatedBackgroundValue = 0;
            double simulatedBackgroundValue = 0;
            try {
                if (ConfigFactory.getConfig().getCentralPixelFlag()) {
                    cpSimulatedBackgroundValue = cpSimulatedBackgroundFunction.value(lambda.getValue0());
                }
                simulatedBackgroundValue = simulatedBackgroundFunction.value(lambda.getValue0());
            } catch (FunctionEvaluationException ex) {
                logger.error(ex.getMessage(), ex);
                throw new CalculationException(ex.getMessage());
            }
            Double extraBackground = null;
            try {
                extraBackground = extraBackgroundFunction.value(lambda.getValue0());
            } catch (FunctionEvaluationException ex) {
                logger.error("Failed to calculate the extra background value", ex);
                throw new CalculationException(ex.getMessage());
            }
            Double cpExtraBackground = extraBackground / npix;
            double centralPixelBgPerLambda = (cpSimulatedBackgroundValue + extraBackground) * exposureTime;
            double bgPerLambda = (simulatedBackgroundValue + cpExtraBackground) * exposureTime;
            double cpBg = centralPixelBgPerLambda * dl;
            double bg = bgPerLambda * dl;
            // We calculate the SNR for the central pixel
            double cpSignalToNoise = cpS / Math.sqrt(cpS + cpBg + detectorPerPixel);
            // We calculate the SNR for the total area
            double det = npix * detectorPerPixel;
            double signalToNoise = s / Math.sqrt(s + bg + det);
            
            cpTotalSignalMap.put(lambda.getValue0(), cpS);
            totalSignalMap.put(lambda.getValue0(), s);
            cpTotalBackgroundNoiseMap.put(lambda.getValue0(), cpBg);
            totalBackgroundNoiseMap.put(lambda.getValue0(), bg);
            cpTotalDetectorNoiseMap.put(lambda.getValue0(), detectorPerPixel);
            totalDetectorNoiseMap.put(lambda.getValue0(), det);
            cpSignalToNoiseMap.put(lambda.getValue0(), cpSignalToNoise);
            signalToNoiseMap.put(lambda.getValue0(), signalToNoise);
        }
        return new Octet<Map<Double, Double>, Map<Double, Double>,
                Map<Double, Double>, Map<Double, Double>,
                Map<Double, Double>, Map<Double, Double>,
                Map<Double, Double>, Map<Double, Double>>
                (cpSignalToNoiseMap, signalToNoiseMap, cpTotalSignalMap,
                totalSignalMap, cpTotalBackgroundNoiseMap, totalBackgroundNoiseMap,
                cpTotalDetectorNoiseMap, totalDetectorNoiseMap);
    }

    /**
     * <p>For every calculation the calculator adds in the simulation results
     * the following outputs of the calculator:</p>
     * <ul>
     *   <li>The central pixel SNR in final results with code CENTRAL_PIXEL_SIGNAL_TO_NOISE</li>
     *   <li>The SNR in final results with code SIGNAL_TO_NOISE</li>
     *   <li>The central pixel total signal in final results with code CENTRAL_PIXEL_TOTAL_SIGNAL</li>
     *   <li>The total signal in final results with code TOTAL_SIGNAL</li>
     *   <li>The central pixel total background in final results with code CENTRAL_PIXEL_TOTAL_BACKGROUND_NOISE</li>
     *   <li>The total background in final results with code TOTAL_BACKGROUND_NOISE</li>
     *   <li>The central pixel total detector noise in final results with code CENTRAL_PIXEL_TOTAL_DELTECTOR_NOISE</li>
     *   <li>The total detector noise in final results with code TOTAL_DELTECTOR_NOISE</li>
     * </ul>
     * @param input
     * @param output 
     */
    @Override
    protected void performForEveryCalculation(Tuple input, Octet<
            Map<Double, Double>, Map<Double, Double>, Map<Double, Double>,
            Map<Double, Double>, Map<Double, Double>, Map<Double, Double>,
            Map<Double, Double>, Map<Double, Double>> output) {
        if (ConfigFactory.getConfig().getCentralPixelFlag()) {
            ResultsHolder.getResults().addResult(new CalculationResults.DoubleDatasetResult("CENTRAL_PIXEL_SIGNAL_TO_NOISE",
                    output.getValue0(), Units.ANGSTROM, null), CalculationResults.Level.FINAL);
        }
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleDatasetResult("SIGNAL_TO_NOISE",
                output.getValue1(), Units.ANGSTROM, null), CalculationResults.Level.FINAL);
        if (ConfigFactory.getConfig().getCentralPixelFlag()) {
            ResultsHolder.getResults().addResult(new CalculationResults.DoubleDatasetResult("CENTRAL_PIXEL_TOTAL_SIGNAL",
                    output.getValue2(), Units.ANGSTROM, Units.ELECTRONS), CalculationResults.Level.FINAL);
        }
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleDatasetResult("TOTAL_SIGNAL",
                output.getValue3(), Units.ANGSTROM, Units.ELECTRONS), CalculationResults.Level.FINAL);
        if (ConfigFactory.getConfig().getCentralPixelFlag()) {
            ResultsHolder.getResults().addResult(new CalculationResults.DoubleDatasetResult("CENTRAL_PIXEL_TOTAL_BACKGROUND_NOISE",
                    output.getValue4(), Units.ANGSTROM, Units.ELECTRONS), CalculationResults.Level.FINAL);
        }
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleDatasetResult("TOTAL_BACKGROUND_NOISE",
                output.getValue5(), Units.ANGSTROM, Units.ELECTRONS), CalculationResults.Level.FINAL);
        if (ConfigFactory.getConfig().getCentralPixelFlag()) {
            ResultsHolder.getResults().addResult(new CalculationResults.DoubleDatasetResult("CENTRAL_PIXEL_TOTAL_DELTECTOR_NOISE",
                    output.getValue6(), Units.ANGSTROM, Units.ELECTRONS), CalculationResults.Level.FINAL);
        }
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleDatasetResult("TOTAL_DELTECTOR_NOISE",
                output.getValue7(), Units.ANGSTROM, Units.ELECTRONS), CalculationResults.Level.FINAL);
    }

    /**
     * <p>For every calculator instantiation the following parts of its configuration
     * are added in the simulation results:</p>
     * <ul>
     *   <li>The dark current in debug results with code DARK_CURRENT</li>
     *   <li>The readout in debug results with code READOUT_NOISE</li>
     *   <li>The number of exposures in debug results with code NUMBER_OF_EXPOSURES</li>
     *   <li>The exposure time in debug results with code EXPOSURE_TIME</li>
     * </ul>
     * @param configuration The configuration of the calculator
     */
    @Override
    protected void performForEveryRetrieval(Ennead<
            Calculator<Unit<Double>, Unit<Double>>,
            Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>,
                 Calculator<Tuple, Unit<BoundedUnivariateFunction>>>,
            Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>,
                 Calculator<Tuple, Unit<BoundedUnivariateFunction>>>, 
            Calculator<Unit<Double>, Unit<Double>>, Pair<Double, String>, 
            Pair<Double, String>, Integer, Pair<Double, String>,
            Calculator<Tuple, Pair<Double, Double>>> configuration) {
        Pair<Double, String> darkPair = configuration.getValue4();
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("DARK_CURRENT", 
                darkPair.getValue0(), darkPair.getValue1()), CalculationResults.Level.DEBUG);
        Pair<Double, String> readoutPair = configuration.getValue5();
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("READOUT_NOISE", 
                readoutPair.getValue0(), readoutPair.getValue1()), CalculationResults.Level.DEBUG);
        ResultsHolder.getResults().addResult(new CalculationResults.LongValueResult("NUMBER_OF_EXPOSURES", 
                configuration.getValue6(), null), CalculationResults.Level.DEBUG);
        Pair<Double, String> exposureTimePair = configuration.getValue7();
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("EXPOSURE_TIME", 
                exposureTimePair.getValue0(), exposureTimePair.getValue1()), CalculationResults.Level.DEBUG);
    }
    
}
