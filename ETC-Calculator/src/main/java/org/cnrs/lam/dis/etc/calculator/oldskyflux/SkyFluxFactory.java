/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.oldskyflux;

import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.dataset.DatasetFactory;
import org.cnrs.lam.dis.etc.calculator.util.ZeroCalculator;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.StringResult;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.cnrs.lam.dis.etc.datamodel.Site;
import org.javatuples.Quartet;
import org.javatuples.Triplet;
import org.javatuples.Unit;

/**
 * <p>The {@code SkyFluxFactory} provides calculators for calculating the sky
 * flux expressed in {@latex.inline $erg/s/cm^2/$\\AA$/arcsec^2$}. The configuration
 * of the factory is a {@link org.cnrs.lam.dis.etc.datamodel.Session} object
 * containing all the configuration from the user.</p>
 */
public class SkyFluxFactory implements Factory<Unit<Session>, Unit<Double>, Unit<Double>> {

    @Override
    public Calculator<Unit<Double>, Unit<Double>> getCalculator(Unit<Session> configuration) throws InitializationException, ConfigurationException {
        Session session = configuration.getValue0();
        Site site = session.getSite();
        // If we are in space and the sky is not contributing we return zero
        if (site.getLocationType() == Site.LocationType.SPACE) {
            ResultsHolder.getResults().addResult(
                    new StringResult("SKY_FLUX_METHOD", "No sky noise contribution"), Level.DEBUG);
            return EtcCalculatorManager.getManager(ZeroCalculator.class).getCalculator(null);
        }
        // Otherwise we return the SkySpectrum calculator
        ResultsHolder.getResults().addResult(
                        new StringResult("SKY_FLUX_METHOD", "Sky spectrum"), Level.DEBUG);
        DatasetInfo skyEmissionInfo = site.getSkyEmission();
        String option = site.getSkyEmissionSelectedOption();
        Quartet<Session, Dataset.Type, DatasetInfo, String> skyEmissionConfiguration
                = new Quartet<Session, Dataset.Type, DatasetInfo, String>
                        (session, Dataset.Type.SKY_EMISSION, skyEmissionInfo, option);
        Calculator<Unit<Double>, Unit<Double>> skyEmission
                = new DatasetFactory().getCalculator(skyEmissionConfiguration);
        Triplet<DatasetInfo, String, Calculator<Unit<Double>, Unit<Double>>> calculatorConfiguration
                = new Triplet<DatasetInfo, String, Calculator<Unit<Double>, Unit<Double>>>
                        (skyEmissionInfo, option, skyEmission);
        return EtcCalculatorManager.getManager(SkySpectrum.class).getCalculator(calculatorConfiguration);
    }
    
}
