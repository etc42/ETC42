/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.psf;

import java.util.Map;
import java.util.TreeMap;
import lombok.EqualsAndHashCode;
import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.analysis.BivariateRealFunction;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.apache.log4j.Logger;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.FunctionToDatasetResultConverter;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.calculator.util.functions.CircularlySymmetricBivariateFunction;
import org.cnrs.lam.dis.etc.calculator.util.functions.SymmetricBivariateDoubleGaussian;
import org.cnrs.lam.dis.etc.calculator.util.functions.SymmetricBivariateGaussian;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.DoubleDatasetResult;
import org.javatuples.Pair;
import org.javatuples.Unit;

/**
 * <p>The {@code InstrumentPlusSeeing} class is a calculator for computing 
 * the total PSF in the case none of the instrument or atmosphere PSFs are negligible.
 * The result of the calculation is the convolution of the two PSFs. Note that
 * currently only Gaussian and Double Gaussian PSFs are supported.</p>
 * 
 * <p>The configuration of the calculator is a {@link org.javatuples.Pair},
 * with contains the following elements:</p>
 * <ol>
 *   <li>{@link Calculator}: A calculator for the instrument PSF</li>
 *   <li>{@link Calculator}: A calculator for the atmosphere PSF</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the wavelength for which the calculation
 * is done (expressed in {@latex.inline \\AA}).</p>
 * 
 * <p>The output of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link BivariateRealFunction} representing the PSF (both axes are expressed in
 * {@latex.inline $arcsec$}).</p>
 */
@Cacheable(CachingPolicy.INSTANCE)
@EqualsAndHashCode(callSuper=false)
public class InstrumentPlusSeeing extends AbstractCalculator<
        Pair<Calculator<Unit<Double>, Unit<BivariateRealFunction>>,
             Calculator<Unit<Double>, Unit<BivariateRealFunction>>>, 
        Unit<Double>, Unit<BivariateRealFunction>> {
    
    private static final Logger logger = Logger.getLogger(InstrumentPlusSeeing.class);
    private Calculator<Unit<Double>, Unit<BivariateRealFunction>> instrumentPsf;
    private Calculator<Unit<Double>, Unit<BivariateRealFunction>> atmospherePsf;

    /**
     * This calculators input does not need any validation.
     * @param configuration
     * @throws ConfigurationException 
     */
    @Override
    protected void validateConfiguration(Pair<Calculator<Unit<Double>, Unit<BivariateRealFunction>>, 
                                              Calculator<Unit<Double>, Unit<BivariateRealFunction>>> configuration) throws ConfigurationException {
        // We do not need to validate anything
    }

    /**
     * Initializes the calculator instance with the given configuration. The first
     * value of the configuration is the calculator for the instrument PSF and the
     * second one is the calculator for the Seeing PSF.
     * @param configuration The configuration of the calculator
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(Pair<Calculator<Unit<Double>, Unit<BivariateRealFunction>>, 
                                   Calculator<Unit<Double>, Unit<BivariateRealFunction>>> configuration) throws InitializationException {
        instrumentPsf = configuration.getValue0();
        atmospherePsf = configuration.getValue1();
    }

    /**
     * Returns a {@link org.javatuples.Unit} containing a bivariate function, of
     * which both axes are expressed in {@latex.inline $arcsec$}, which represents
     * the PSF for the given wavelength.
     * @param input The wavelength expressed in {@latex.inline \\AA}
     * @return A function representing the PSF
     * @throws CalculationException if there is any error during the calculation
     */
    @Override
    protected Unit<BivariateRealFunction> performCalculation(Unit<Double> input) throws CalculationException {
        // We know that both the instrument and seeing PSF is implemented as
        // bivariate gaussian.
        BivariateRealFunction instrumentFunction = instrumentPsf.calculate(input).getValue0();
        SymmetricBivariateGaussian atmosphereFunction =
                (SymmetricBivariateGaussian) atmospherePsf.calculate(input).getValue0();
        // The instrument function can be one of Gaussian or Double Gaussian
        if (instrumentFunction instanceof SymmetricBivariateGaussian) {
            double instrumentSigma = ((SymmetricBivariateGaussian) instrumentFunction).getStandardDeviation();
            double sigma = instrumentSigma + atmosphereFunction.getStandardDeviation();
            SymmetricBivariateGaussian gaussian = new SymmetricBivariateGaussian(sigma);
            return new Unit<BivariateRealFunction>(gaussian);
        }
        if (instrumentFunction instanceof SymmetricBivariateDoubleGaussian) {
            Pair<Double, Double> instrumentSigmas = ((SymmetricBivariateDoubleGaussian) instrumentFunction).getStandardDeviations();
            double weight = ((SymmetricBivariateDoubleGaussian) instrumentFunction).getWeight();
            double sigma1 = instrumentSigmas.getValue0() + atmosphereFunction.getStandardDeviation();
            double sigma2 = instrumentSigmas.getValue1() + atmosphereFunction.getStandardDeviation();
            SymmetricBivariateDoubleGaussian doubleGaussian = new SymmetricBivariateDoubleGaussian(sigma1, sigma2, weight);
            return new Unit<BivariateRealFunction>(doubleGaussian);
        }
        throw new CalculationException("Calculation of total PSF for non Gaussian or Double Gaussian"
                + " Instrument and Seeing PSFs is not yet implemented");
    }

    /**
     * <p>For every calculation the calculator adds in the simulation results
     * the following outputs of the calculator:</p>
     * <ul>
     *   <li>The FWHM of the total PSF in intermediate unimportant results
     *       with code TOTAL_PSF_FWHM</li>
     *   <li> The projection of the PSF on the plane y=0 in the intermediate
     *       unimportant results with code TOTAL_PSF_PROJECTION</li>
     * </ul>
     * @param input
     * @param output 
     */
    @Override
    protected void performForEveryCalculation(Unit<Double> input, Unit<BivariateRealFunction> output) {
        double lambda = input.getValue0();
        
        CircularlySymmetricBivariateFunction psfFunction = (CircularlySymmetricBivariateFunction) output.getValue0();
        
        // We know that we have either a gaussian or a double gaussian. We calculate
        // the min and max values for which we will present the result
        double min = 0;
        double max = 0;
        if (output.getValue0() instanceof SymmetricBivariateGaussian) {
            SymmetricBivariateGaussian gaussian = (SymmetricBivariateGaussian) psfFunction;
            min = -5 * gaussian.getStandardDeviation();
            max = 5 * gaussian.getStandardDeviation();
        } else {
            SymmetricBivariateDoubleGaussian gaussian = (SymmetricBivariateDoubleGaussian) psfFunction;
            double bigSigma = Math.max( gaussian.getStandardDeviations().getValue0(),
                    gaussian.getStandardDeviations().getValue1());
            min = -5 * bigSigma;
            max = 5 * bigSigma;
        }
        
        // We add the projection on the y = 0 plane in the results. We calculate
        // the valuse in the range of 5 sigma, as all the values outside of this
        // are too close to zero.
        UnivariateRealFunction projetion = psfFunction.projectionFunction();
        DoubleDatasetResult projectionResult = FunctionToDatasetResultConverter
                .convert(min, max, projetion, "TOTAL_PSF_PROJECTION", Units.ARCSEC, null);
        ResultsHolder.getResults().addResult("TOTAL_PSF_PROJECTION", lambda, projectionResult,
                Units.ANGSTROM, CalculationResults.Level.INTERMEDIATE_UNIMPORTANT);
    }
    
}
