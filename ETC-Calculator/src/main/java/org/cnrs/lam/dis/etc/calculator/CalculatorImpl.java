/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator;

import org.apache.log4j.Logger;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.exposuretime.ExposureTimeFactory;
import org.cnrs.lam.dis.etc.calculator.signaltonoise.SignalToNoiseFactory;
import org.cnrs.lam.dis.etc.calculator.util.FunctionToDatasetResultConverter;
import org.cnrs.lam.dis.etc.calculator.wavelengthrange.CommonRangeFactory;
import org.cnrs.lam.dis.etc.calculator.wavelengthrange.Imaging;
import org.cnrs.lam.dis.etc.calculator.wavelengthrange.WavelengthRangeFactory;
import org.cnrs.lam.dis.etc.configuration.ConfigFactory;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.cnrs.lam.dis.etc.datamodel.DatasetProvider;
import org.cnrs.lam.dis.etc.datamodel.ObsParam.FixedParameter;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.javatuples.Pair;
import org.javatuples.Unit;
/**
 */
final class CalculatorImpl implements Calculator {
	    
    /** The logger of the class.*/
    private static Logger logger = Logger.getLogger(CalculatorImpl.class.getName());
        
    // The following code will set the property to disable the cache if the configuration
    // says so. Note that if the user has set the property from the command line
    // his option is overriding the one in the configuration. Also note that this
    // happens only once when the first instance of the class is initiated, so
    // a restart is necessary to see effects of changing the caching option.
    static {
        if (ConfigFactory.getConfig().isCacheDisabled()) {
            System.setProperty("calculator.disable.cache", "");
        }
    }
    
    private boolean useOldCalculator() {
        if (System.getProperty("etc.useOldCalculator", "NO").equals("YES")) {
            return true;
        } else {
            return false;
        }
    }

    @Override
	public CalculationResults performCalculation(Session session, DatasetProvider datasetProvider) throws CalculationError, ValidationError {
    	CommonRangeFactory.setSession(session);
    	PreviousResultsHolder.setResults(ResultsHolder.getResults());
    	ResultsHolder.resetResults();
        DatasetProviderHolder.setDatasetProvider(datasetProvider);
        

        // Set the range of the calculation
        try {
            Pair<Double, Double> range = new WavelengthRangeFactory().getCalculator(new Unit<Session>(session)).calculate(null);
            FunctionToDatasetResultConverter.setRange(range.getValue0(), range.getValue1());
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            throw new CalculationError("Failed to calculate the wavelength range of the simulation. Reason:\n" + ex.getMessage());
        }

        double oldExposureTime = session.getObsParam().getExposureTime();
        if (session.getObsParam().getFixedParameter() == FixedParameter.SNR) {
            double time;
            try {
                if (useOldCalculator()) {
                    time = new org.cnrs.lam.dis.etc.calculator.oldexposuretime.ExposureTimeFactory().getCalculator(new Unit<Session>(session)).calculate(null).getValue0();
                } else {
                    time = new ExposureTimeFactory().getCalculator(new Unit<Session>(session)).calculate(null).getValue0();
                }
            } catch (InitializationException ex) {
                logger.error(ex.getMessage(), ex);
                throw new ValidationError(ex.getMessage());
            } catch (ConfigurationException ex) {
                logger.error(ex.getMessage(), ex);
                throw new ValidationError(ex.getMessage());
            } catch (CalculationException ex) {
                logger.error(ex.getMessage(), ex);
                throw new CalculationError(ex.getMessage());
            }
            session.getObsParam().setExposureTime(time);
        }
        try {
            if (useOldCalculator()) {
                new org.cnrs.lam.dis.etc.calculator.oldsignaltonoise.SignalToNoiseFactory().getCalculator(new Unit<Session>(session)).calculate(null);
            } else {
                new SignalToNoiseFactory().getCalculator(new Unit<Session>(session)).calculate(null);
            }
        } catch (InitializationException ex) {
            logger.error(ex.getMessage(), ex);
            throw new ValidationError(ex.getMessage());
        } catch (ConfigurationException ex) {
            logger.error(ex.getMessage(), ex);
            throw new ValidationError(ex.getMessage());
        } catch (CalculationException ex) {
            logger.error(ex.getMessage(), ex);
            throw new CalculationError(ex.getMessage());
        } finally {
            session.getObsParam().setExposureTime(oldExposureTime);
        }
        

        return ResultsHolder.getResults();
    }
}
