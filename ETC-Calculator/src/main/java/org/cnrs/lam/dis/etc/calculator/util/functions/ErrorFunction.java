/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.util.functions;

import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.MathException;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.apache.commons.math.special.Erf;
import org.apache.log4j.Logger;

/**
 * <p>The {@link ErrorFunction} class represents the error function:</p>
 * {@latex.inline $
 * erf_{(x)}=\\displaystyle\\frac{2}{\\sqrt{\\pi}} \\int_0^x e^{-t^2}\\,\\mathrm{d}t
 * $}
 * <p>To retrieve an instance of the error function class one of the factory
 * methods must be used.</p>
 */
public abstract class ErrorFunction implements UnivariateRealFunction {
    
    private final static Logger logger = Logger.getLogger(ErrorFunction.class);
    
    /**
     * Returns an {@link ErrorFunction} which is based on the {@link Erf} class
     * from library apache commons math. This method provides a good approximation
     * of the error function, with accuracy up to the 7th decimal point.
     * @return 
     */
    public static ErrorFunction getCommonsMathErf() {
        return new ErrorFunction() {
            @Override
            public double value(double x) throws FunctionEvaluationException {
                double value = 0;
                try {
                    value = Erf.erf(x);
                } catch (MathException ex) {
                    logger.warn("The error function method of commons math failed to converge", ex);
                    throw new FunctionEvaluationException(ex, x);
                }
                return value;
            }
        };
    }
    
    /**
     * <p>Returns a {@link ErrorFunction} based on the Tsay approximation:</p>
     * {@latex.inline $
     * erf_{(x)}=sign_{(x)} * (1-e^{c_1z+c_2z^2}) \\\\
     * \\mathrm{where} \\\\
     * sign_{(x)} = \\mathrm{the}\\:\\mathrm{sign}\\:\\mathrm{of}\\:x \\\\
     * z = \\mathrm{the}\\:\\mathrm{absolute}\\:\\mathrm{value}\\:\\mathrm{of}\\:x \\\\
     * c_1 = -1.09500814703333 \\\\
     * c_2 = -0.75651138383854
     * $}
     * <p>This method provides a bad approximation with accuracy (up to 2nd decimal
     * point), but it is much faster than the commons math implementation (up to
     * three times faster).</p>
     * @return 
     */
    public static ErrorFunction getTsayErf() {
        return new ErrorFunction() {
            @Override
            public double value(double x) throws FunctionEvaluationException {
                if (x == 0) {
                    return 0;
                } else if (x > 10) {
                    return 1;
                } else if (x < -10) {
                    return -1;
                }
                double c1 = -1.09500814703333;
                double c2 = -0.75651138383854;
                double absX = Math.abs(x);
                double value = 1 - Math.exp(absX * (c1 + c2 * absX));
                if (x > 0) {
                    return value;
                } else {
                    return -value;
                }
            }
        };
    }
    
}
