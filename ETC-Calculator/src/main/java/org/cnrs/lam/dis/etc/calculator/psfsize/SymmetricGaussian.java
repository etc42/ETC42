/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.psfsize;

import lombok.EqualsAndHashCode;
import org.apache.commons.math.analysis.BivariateRealFunction;
import org.apache.log4j.Logger;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.calculator.util.functions.SymmetricBivariateGaussian;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.javatuples.Unit;

/**
 * <p>The {@link SymmetricGaussian} class is a calculator for computing the size
 * of Gaussian PSFs. The size of such PSFs is calculated as:</p>
 * {@latex.inline $
 * PSF_{size}=2*FWHM=2*2.3548*\\sigma
 * $}
 * <p>where</p>
 * <ul>
 *   <li>{@latex.inline $PSF_{size}$}: is the size of the PSF expressed in
 *       {@latex.inline $arcsec$}</li>
 *   <li>{@latex.inline $FWHM$}: is the full width half maximum of the PSF expressed in
 *       {@latex.inline $arcsec$}</li>
 *   <li>{@latex.inline $\\sigma$}: is the standard deviation of the PSF</li>
 * </ul>
 * 
 * <p>The configuration of the calculator is a {@link org.javatuples.Unit},
 * with contains a calculator for computing the PSF function.</p>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the wavelength for which the calculation
 * is done (expressed in {@latex.inline \\AA}).</p>
 * 
 * <p>The output of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link Double} representing the size of the PSF expressed in
 * {@latex.inline $arcsec$}.</p>
 */
@Cacheable(CachingPolicy.ALL)
@EqualsAndHashCode(callSuper=false)
public class SymmetricGaussian extends AbstractCalculator<
        Unit<Calculator<Unit<Double>, Unit<BivariateRealFunction>>>,
        Unit<Double>, Unit<Double>> {
    
    private static final Logger logger = Logger.getLogger(SymmetricGaussian.class);
    private Calculator<Unit<Double>, Unit<BivariateRealFunction>> psfCalculator;

    /**
     * Does nothing as this calculators configuration does not need any validation.
     * @param configuration
     * @throws ConfigurationException 
     */
    @Override
    protected void validateConfiguration(Unit<Calculator<Unit<Double>,
            Unit<BivariateRealFunction>>> configuration) throws ConfigurationException {
        // We do nothing here
    }

    /**
     * Initializes the calculator instance with the given configuration. The given
     * {@link Unit} contains a {@link Calculator} for computing the PSF function
     * for different wavelengths.
     * @param configuration The configuration of the calculator
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(Unit<Calculator<Unit<Double>,
            Unit<BivariateRealFunction>>> configuration) throws InitializationException {
        psfCalculator = configuration.getValue0();
    }

    /**
     * Returns a {@link org.javatuples.Unit} containing the size of the PSF,
     * expressed in {@latex.inline $arcsec$}. The size is considered to be twice
     * as big as the FWHM of the PSF.
     * @param input The wavelength expressed in {@latex.inline \\AA}
     * @return The size of the PSF expressed in {@latex.inline $arcsec$}
     * @throws CalculationException if there is any error during the calculation
     */
    @Override
    protected Unit<Double> performCalculation(Unit<Double> input) throws CalculationException {
        // First we retrieve the PSF function
        BivariateRealFunction function = psfCalculator.calculate(input).getValue0();
        // If we do not have a Gaussian it is a bug! The factory should not return
        // this calculator for sessions which produce non gaussian PSFs
        if (!(function instanceof SymmetricBivariateGaussian)) {
            logger.warn("Expected symmetric bivariate Gaussian PSF but got " + function.getClass());
            throw new CalculationException("Failed to perform the calculation because the PSF wasn't a Gaussian");
        }
        // We get the standard deviation of the gaussian and we calculate the FWHM
        SymmetricBivariateGaussian gaussian = (SymmetricBivariateGaussian) function;
        double sigma = gaussian.getStandardDeviation();
        double fwhm = 2.3548 * sigma;
        return new Unit<Double>(2 * fwhm);
    }

    /**
     * <p>For every calculation the calculator adds in the simulation results
     * the following outputs of the calculator:</p>
     * <ul>
     *   <li>The size of the PSF in intermediate unimportant results
     *       with code PSF_SIZE</li>
     * </ul>
     * @param input
     * @param output 
     */
    @Override
    protected void performForEveryCalculation(Unit<Double> input, Unit<Double> output) {
        ResultsHolder.getResults().addResult("PSF_SIZE", input.getValue0(), output.getValue0(), 
                Units.ANGSTROM, Units.ARCSEC, CalculationResults.Level.INTERMEDIATE_UNIMPORTANT);
    }
    
}
