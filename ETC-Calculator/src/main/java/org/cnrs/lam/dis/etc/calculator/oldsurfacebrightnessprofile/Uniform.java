/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.oldsurfacebrightnessprofile;

import lombok.EqualsAndHashCode;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.DoubleValueResult;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.javatuples.Pair;
import org.javatuples.Unit;

/**
 * <p>The {@code Uniform} class is a calculator for computing the surface
 * brightness profile, following the uniform distribution. The equation
 * used is:</p>
 * {@latex.inline $
 * I_{(r)} = 
 * \\left \\|
 * \\begin{array}{ll}
 * 1 & , r \\le r_{obj} \\\\
 * 0 & , r > r_{obj}
 * \\end{array}
 * \\right .
 * $}
 * <p>where</p>
 * <ul>
 *   <li>{@latex.inline I_{(r)}}: is the surface brightness profile</li>
 *   <li>{@latex.inline $r_{obj}$}: is the effective radius of the extended source in
 *       {@latex.inline $arcsec$}</li>
 *   <li>{@latex.inline $r$}: is the distance from the center of the source, for
 *       which the value is calculated, expressed in {@latex.inline $arcsec$}</li>
 * </ul>
 * 
 * <p>The configuration of the calculator is a {@link org.javatuples.Pair},
 * with contains the following elements:</p>
 * <ol>
 *   <li>{@link java.lang.Double}: The effective radius of the source</li>
 *   <li>{@link java.lang.String}: The unit of the effective radius</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the distance from the center of the
 * source for which the calculation is done (expressed in {@latex.inline $arcsec$}).</p>
 * 
 * <p>The output of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the surface brightness profile.</p>
 */
@EqualsAndHashCode(callSuper=false)
public class Uniform extends AbstractCalculator<Pair<Double, String>, Unit<Double>, Unit<Double>> {
    
    private double sourceRadius;

    /**
     * Validates the given configuration. It requires that the first value of the
     * tuple (the source radius) is a positive number and that the second
     * value (the unit of the source radius) is {@latex.inline $arcsec$}.
     * @param configuration The configuration to validate
     * @throws ConfigurationException If the configuration is invalid
     */
    @Override
    protected void validateConfiguration(Pair<Double, String> configuration) throws ConfigurationException {
        // Check that the source radius is a possitive number
        if (configuration.getValue0() <= 0) {
            throw new ConfigurationException("Extended source radius must be a possitive "
                    + "number but was " + configuration.getValue0());
        }
        // Check if the source radius is in arcsec
        if (!Units.isArcsec(configuration.getValue1())) {
            throw new ConfigurationException("Extended source radius must be in " 
                    + Units.ARCSEC + " but was in " + configuration.getValue1());
        }
    }

    /**
     * Initializes the calculator instance with the given configuration. The first
     * value of the configuration is the source radius and the second is
     * the unit of the source radius.
     * @param configuration the configuration to use for initialization
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(Pair<Double, String> configuration) throws InitializationException {
        sourceRadius = configuration.getValue0();
    }

    /**
     * Returns a {@link org.javatuples.Unit} containing the surface brightness 
     * profile.
     * @param input The distance from the source center expressed in {@latex.inline $arcsec$}
     * @return The surface brightness profile
     * @throws CalculationException if there is any error during the calculation
     */
    @Override
    protected Unit<Double> performCalculation(Unit<Double> input) throws CalculationException {
        double radius = Math.abs(input.getValue0());
        double result = (radius <= sourceRadius) ? 1 : 0;
        return new Unit<Double>(result);
    }

    /**
     * Overridden to add in the results the surface brightness profile.
     * @param input The distance from the source center expressed in {@latex.inline $arcsec$}
     * @param output The surface brightness profile
     */
    @Override
    protected void performForEveryCalculation(Unit<Double> input, Unit<Double> output) {
        ResultsHolder.getResults().addResult("SURFACE_BRIGHTNESS_PROFILE", input.getValue0(),
                output.getValue0(), Units.ARCSEC, null, Level.INTERMEDIATE_UNIMPORTANT);
    }

    /**
     * Overridden to add in the results the source radius.
     * @param configuration The configuration of the calculator
     */
    @Override
    protected void performForEveryRetrieval(Pair<Double, String> configuration) {
        ResultsHolder.getResults().addResult(new DoubleValueResult("SOURCE_RADIUS",
                configuration.getValue0(), configuration.getValue1()), Level.DEBUG);
    }
    
}
