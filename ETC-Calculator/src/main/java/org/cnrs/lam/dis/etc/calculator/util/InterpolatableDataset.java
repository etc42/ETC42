/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.util;

import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.WeakHashMap;
import org.apache.commons.math.ArgumentOutsideDomainException;
import org.apache.commons.math.analysis.interpolation.LinearInterpolator;
import org.apache.commons.math.analysis.interpolation.SplineInterpolator;
import org.apache.commons.math.analysis.polynomials.PolynomialSplineFunction;
import org.cnrs.lam.dis.etc.calculator.CalculationError;
import org.cnrs.lam.dis.etc.datamodel.Dataset;

/**
 *
 * @author nikoapos
 */
public class InterpolatableDataset {

    private Map<Double, WeakReference<Double>> cache;
    private PolynomialSplineFunction function;

    public InterpolatableDataset(Map<Double, Double> data) {
        cache = new WeakHashMap<Double, WeakReference<Double>>();
        double[] x = new double[data.size()];
        double[] y = new double[data.size()];
        int i = 0;
        for (Map.Entry<Double, Double> entry : data.entrySet()) {
            cache.put(entry.getKey(), new WeakReference<Double>(entry.getValue()));
            x[i] = entry.getKey();
            y[i] = entry.getValue();
            i++;
        }
        // The SPlineInterpolator is doing interpolation with 3rd degree polynomials.
        // It was returning negative values for flux (which is obviously bad) so I
        // replaced it with linear interpolation (at least for now)
        // function = new SplineInterpolator().interpolate(x, y);
        function = new LinearInterpolator().interpolate(x, y);
    }

    public InterpolatableDataset(Dataset dataset) {
        this(dataset.getData());
    }

    public double getValue(double x) throws CalculationError {
        WeakReference<Double> cachedValue = cache.get(Double.valueOf(x));
        if (cachedValue != null) {
            return cachedValue.get();
        }
        Double value = null;
        try {
            value = function.value(x);
        } catch (ArgumentOutsideDomainException ex) {
            throw new CalculationError("Interpolation failed because " + x + " is out of the limits [" +
                    function.getKnots()[0] + "," + function.getKnots()[function.getKnots().length] + "]");
        }
        cache.put(Double.valueOf(x), new WeakReference<Double>(value));
        return value;
    }

}
