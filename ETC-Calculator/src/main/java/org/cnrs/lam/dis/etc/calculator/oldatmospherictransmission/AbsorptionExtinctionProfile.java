/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.oldatmospherictransmission;

import lombok.EqualsAndHashCode;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.Cacheable;
import org.cnrs.lam.cesam.util.calculator.CachingPolicy;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.Calculator;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.DatasetProviderHolder;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.DoubleValueResult;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.StringResult;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.javatuples.Quintet;
import org.javatuples.Unit;

/**
 */

// Caching explenation
// -------------------
// This calculator is set to cache the instances because the configuration validation
// is retrieving the dataset, which is time consuming.
@Cacheable(CachingPolicy.INSTANCE)
@EqualsAndHashCode(callSuper=false)
public class AbsorptionExtinctionProfile extends AbstractCalculator<Quintet<Double, DatasetInfo, Calculator<Unit<Double>, Unit<Double>>, DatasetInfo, Calculator<Unit<Double>, Unit<Double>>>, Unit<Double>, Unit<Double>> {
    
    private double airMass;
    private Calculator<Unit<Double>, Unit<Double>> absorption;
    private Calculator<Unit<Double>, Unit<Double>> extinction;

    @Override
    protected void validateConfiguration(Quintet<Double, DatasetInfo, Calculator<Unit<Double>, Unit<Double>>, DatasetInfo, Calculator<Unit<Double>, Unit<Double>>> configuration) throws ConfigurationException {
        if (configuration.getValue0() < 1) {
            throw new ConfigurationException("Air Mass must be bigger than one " +
                    "but was " + configuration.getValue1());
        }
        DatasetInfo datasetInfo = configuration.getValue1();
        if (datasetInfo == null) {
            throw new ConfigurationException("Site does not have sky absorption");
        }
        Dataset dataset = DatasetProviderHolder.getDatasetProvider().getDataset(Dataset.Type.SKY_ABSORPTION, datasetInfo);
        if (dataset == null) {
            throw new ConfigurationException("Data for sky absorption "
                    + datasetInfo + " is not available");
        }
        if (!Units.isAngstrom(dataset.getXUnit())) {
            throw new ConfigurationException("Unit of X axis of sky absorption must be "
                    + Units.ANGSTROM + " but was " + dataset.getXUnit());
        }
        datasetInfo = configuration.getValue3();
        if (datasetInfo == null) {
            throw new ConfigurationException("Site does not have sky extinction");
        }
        dataset = DatasetProviderHolder.getDatasetProvider().getDataset(Dataset.Type.SKY_EXTINCTION, datasetInfo);
        if (dataset == null) {
            throw new ConfigurationException("Data for sky extinction "
                    + datasetInfo + " is not available");
        }
        if (!Units.isAngstrom(dataset.getXUnit())) {
            throw new ConfigurationException("Unit of X axis of sky extinction must be "
                    + Units.ANGSTROM + " but was " + dataset.getXUnit());
        }
    }

    @Override
    protected void initialize(Quintet<Double, DatasetInfo, Calculator<Unit<Double>, Unit<Double>>, DatasetInfo, Calculator<Unit<Double>, Unit<Double>>> configuration) throws InitializationException {
        airMass = configuration.getValue0();
        absorption = configuration.getValue2();
        extinction = configuration.getValue4();
    }

    @Override
    protected Unit<Double> performCalculation(Unit<Double> input) throws CalculationException {
        double absoprtionValue = absorption.calculate(input).getValue0();
        double extinctionValue = extinction.calculate(input).getValue0();
        double result = absoprtionValue * Math.pow(10, -0.4 * airMass * extinctionValue);
        return new Unit<Double>(result);
    }

    @Override
    protected void performForEveryCalculation(Unit<Double> input, Unit<Double> output) {
        ResultsHolder.getResults().addResult("ATMOSPHERIC_TRANSMISSION", input.getValue0(),
                output.getValue0(), Units.ANGSTROM, null, Level.INTERMEDIATE_IMPORTANT);
    }

    @Override
    protected void performForEveryRetrieval(Quintet<Double, DatasetInfo, Calculator<Unit<Double>, Unit<Double>>, DatasetInfo, Calculator<Unit<Double>, Unit<Double>>> configuration) {
        ResultsHolder.getResults().addResult(new DoubleValueResult("AIR_MASS"
                , configuration.getValue0(), null), Level.DEBUG);
        ResultsHolder.getResults().addResult(new StringResult("SKY_ABSORPTION_PROFILE"
                , configuration.getValue1().toString()), Level.DEBUG);
        ResultsHolder.getResults().addResult(new StringResult("SKY_EXTINCTION_PROFILE"
                , configuration.getValue3().toString()), Level.DEBUG);
    }
    
}
