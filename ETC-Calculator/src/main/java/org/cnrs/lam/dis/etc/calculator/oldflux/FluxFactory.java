/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.oldflux;

import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.oldconvolutionkernel.ConvolutionKernelFactory;
import org.cnrs.lam.dis.etc.calculator.dataset.DatasetFactory;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.StringResult;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.cnrs.lam.dis.etc.datamodel.Source;
import org.javatuples.Pair;
import org.javatuples.Quartet;
import org.javatuples.Quintet;
import org.javatuples.Septet;
import org.javatuples.Unit;

/**
 */
public class FluxFactory implements Factory<Unit<Session>, Unit<Double>, Unit<Double>> {


    @Override
    public Calculator<Unit<Double>, Unit<Double>> getCalculator(Unit<Session> configuration) throws InitializationException, ConfigurationException {
        Session session = configuration.getValue0();
        Source source = session.getSource();
        double magnitude = source.getMagnitude();
        double redshift = source.getRedshift();
        double magnitudeWavelength = source.getMagnitudeWavelength();
        switch (source.getSpectralDistributionType()) {
            case TEMPLATE:
                ResultsHolder.getResults().addResult(
                        new StringResult("SIGNAL_FLUX_METHOD", "Restframe Template"), Level.DEBUG);
                DatasetInfo templateInfo = source.getSpectralDistributionTemplate();
                Quartet<Session, Dataset.Type, DatasetInfo, String> datasetConfiguration
                        = new Quartet<Session, Dataset.Type, DatasetInfo, String>
                                (session, Dataset.Type.SPECTRAL_DIST_TEMPLATE, templateInfo, null);
                Calculator<Unit<Double>, Unit<Double>> template = new DatasetFactory().getCalculator(datasetConfiguration);
                Quintet<Double, Double, Double, DatasetInfo, Calculator<Unit<Double>, Unit<Double>>> templateConfiguration
                        = new Quintet(magnitude, redshift, magnitudeWavelength, templateInfo, template);
                return EtcCalculatorManager.getManager(RestFrameTemplate.class).getCalculator(templateConfiguration);
            case EMISSION_LINE:
                ResultsHolder.getResults().addResult(
                        new StringResult("SIGNAL_FLUX_METHOD", "Emission line"), Level.DEBUG);
                double fluxEm = source.getEmissionLineFlux();
                double lambdaEm = source.getEmissionLineWavelength();
                String lambdaEmUnit = source.getEmissionLineWavelengthUnit();
                double fwhmEm = source.getEmissionLineFwhm();
                String fwhmEmUnit = source.getEmissionLineFwhmUnit();
                Calculator<Unit<Double>, Unit<Double>> kernel = new ConvolutionKernelFactory().getCalculator(configuration);
                Septet<Double, Double, String, Double, String, Calculator<Unit<Double>, Unit<Double>>, Double> 
                        emissionLineConfiguration = new Septet
                            <Double, Double, String, Double, String, Calculator<Unit<Double>, Unit<Double>>, Double>
                            (fluxEm, lambdaEm, lambdaEmUnit, fwhmEm, fwhmEmUnit, kernel, redshift);
                return EtcCalculatorManager.getManager(EmissionLine.class).getCalculator(emissionLineConfiguration);
            case FLAT:
                ResultsHolder.getResults().addResult(
                        new StringResult("SIGNAL_FLUX_METHOD", "Flat photon flux"), Level.DEBUG);
                Pair<Double, Double> flatConfiguration = new Pair(magnitude, magnitudeWavelength);
                return EtcCalculatorManager.getManager(Continuum.class).getCalculator(flatConfiguration);
            case FLAT_ENERGY:
                ResultsHolder.getResults().addResult(
                        new StringResult("SIGNAL_FLUX_METHOD", "Flat energy flux"), Level.DEBUG);
                Unit<Double> flatEnergyConfiguration = new Unit<Double>(magnitude);
                return EtcCalculatorManager.getManager(FlatEnergyFlux.class).getCalculator(flatEnergyConfiguration);
            case BLACK_BODY:
                ResultsHolder.getResults().addResult(
                        new StringResult("SIGNAL_FLUX_METHOD", "Black Body"), Level.DEBUG);
                double temperature = source.getTemperature();
                String temperatureUnit = source.getTemperatureUnit();
                Quintet<Double, Double, Double, String, Double> blackBodyConfiguration
                        = new Quintet(magnitude, magnitudeWavelength, temperature, temperatureUnit, redshift);
                return EtcCalculatorManager.getManager(BlackBody.class).getCalculator(blackBodyConfiguration);
        }
        throw new UnsupportedOperationException("Signal Flux calculation is not supported for the given configuration");
    }
}
