/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.oldbackgroundnoise;

import lombok.EqualsAndHashCode;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.DatasetProviderHolder;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.DoubleValueResult;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.javatuples.Pair;
import org.javatuples.Triplet;
import org.javatuples.Unit;

/**
 */
@EqualsAndHashCode(callSuper=false)
public class ExtraBackgroundNoise extends AbstractCalculator<Triplet<Pair<Double, String>,
                                                                     Calculator<Unit<Double>, Unit<Double>>,
                                                                     Pair<DatasetInfo, Calculator<Unit<Double>, Unit<Double>>>>,
                                                            Unit<Double>, Unit<Double>> {
    
    private double pixelScale;
    private Calculator<Unit<Double>, Unit<Double>> spatialBinningCalculator;
    private Calculator<Unit<Double>, Unit<Double>> spectrumCalculator;

    @Override
    protected void validateConfiguration(Triplet<Pair<Double, String>, Calculator<Unit<Double>, Unit<Double>>, Pair<DatasetInfo, Calculator<Unit<Double>, Unit<Double>>>> configuration) throws ConfigurationException {
        // Check the pixel scale
        if (configuration.getValue0().getValue0() <= 0) {
            throw new ConfigurationException("Pixel scale must be a possitive "
                    + "number but was " + configuration.getValue0());
        }
        if (!Units.getArcsecPerPixel().equals(configuration.getValue0().getValue1())) {
            throw new ConfigurationException("Pixel scale must be in " + Units.getArcsecPerPixel()
                    + " but was in " + configuration.getValue1());
        }
        // Check the dataset of the extra noise
        DatasetInfo info = configuration.getValue2().getValue0();
        if (info == null) {
            throw new ConfigurationException("Observing parameters does not have extra background noise");
        }
        Dataset dataset = DatasetProviderHolder.getDatasetProvider().getDataset(Dataset.Type.EXTRA_BACKGROUND_NOISE, info);
        if (dataset == null) {
            throw new ConfigurationException("Data for extra background noise "
                    + info + " is not available");
        }
        if (!Units.isAngstrom(dataset.getXUnit())) {
            throw new ConfigurationException("Unit of X axis of extra background noise must be "
                    + Units.ANGSTROM + " but was " + dataset.getXUnit());
        }
        if (!Units.isElectronsPerArcsecPerAngstromPerSec(dataset.getYUnit())) {
            throw new ConfigurationException("Unit of Y axis of extra background noise must be "
                    + Units.getElectronsPerArcsecPerAngstromPerSec() + " but was " + dataset.getYUnit());
        }
    }

    @Override
    protected void initialize(Triplet<Pair<Double, String>, Calculator<Unit<Double>, Unit<Double>>, Pair<DatasetInfo, Calculator<Unit<Double>, Unit<Double>>>> configuration) throws InitializationException {
        pixelScale = configuration.getValue0().getValue0();
        spatialBinningCalculator = configuration.getValue1();
        spectrumCalculator = configuration.getValue2().getValue1();
    }

    @Override
    protected Unit<Double> performCalculation(Unit<Double> input) throws CalculationException {
        double spatialBinning = spatialBinningCalculator.calculate(input).getValue0();
        double coveredArcsec = Math.ceil(spatialBinning) * pixelScale;
        double noisePerArcsec = spectrumCalculator.calculate(input).getValue0();
        double noise = noisePerArcsec * coveredArcsec;
        return new Unit<Double>(noise);
    }

    @Override
    protected void performForEveryCalculation(Unit<Double> input, Unit<Double> output) {
        ResultsHolder.getResults().addResult("BACKGROUND_NOISE", input.getValue0(),
                output.getValue0(), Units.ANGSTROM, Units.getElectronsPerSecPerAngstrom(), Level.INTERMEDIATE_UNIMPORTANT);
    }

    @Override
    protected void performForEveryRetrieval(Triplet<Pair<Double, String>, Calculator<Unit<Double>, Unit<Double>>, Pair<DatasetInfo, Calculator<Unit<Double>, Unit<Double>>>> configuration) {
        ResultsHolder.getResults().addResult(new DoubleValueResult("PIXEL_SCALE",
                configuration.getValue0().getValue0(), configuration.getValue0().getValue1()), Level.DEBUG);
        ResultsHolder.getResults().addResult(new CalculationResults.StringResult("EXTRA_BACKGROUND_NOISE_TEMPLATE"
                , configuration.getValue2().getValue0().toString()), CalculationResults.Level.DEBUG);
    }
    
}
