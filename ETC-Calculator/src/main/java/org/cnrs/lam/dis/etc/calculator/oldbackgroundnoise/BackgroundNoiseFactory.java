/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.oldbackgroundnoise;

import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.oldbackgroundflux.BackgroundFluxFactory;
import org.cnrs.lam.dis.etc.calculator.dataset.DatasetFactory;
import org.cnrs.lam.dis.etc.calculator.oldfilterresponse.FilterResponseFactory;
import org.cnrs.lam.dis.etc.calculator.oldlambdarange.LambdaRangeFactory;
import org.cnrs.lam.dis.etc.calculator.oldskyarea.SkyAreaFactory;
import org.cnrs.lam.dis.etc.calculator.oldspatialbinning.SpatialBinningFactory;
import org.cnrs.lam.dis.etc.calculator.oldspectralresolution.SpectralResolutionFactory;
import org.cnrs.lam.dis.etc.calculator.oldsystemefficiency.SystemEfficiencyFactory;
import org.cnrs.lam.dis.etc.calculator.oldtelescopearea.TelescopeAreaFactory;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.StringResult;
import org.cnrs.lam.dis.etc.datamodel.ObsParam.ExtraBackgroundNoiseType;
import org.cnrs.lam.dis.etc.datamodel.*;
import org.javatuples.*;

/**
 */
public class BackgroundNoiseFactory implements Factory<Unit<Session>, Unit<Double>, Unit<Double>> {

    @Override
    public Calculator<Unit<Double>, Unit<Double>> getCalculator(Unit<Session> configuration) throws InitializationException, ConfigurationException {
        Session session = configuration.getValue0();
        Instrument instrument = session.getInstrument();
        ObsParam obsParam = session.getObsParam();
        ExtraBackgroundNoiseType extraBackgroundNoiseType = obsParam.getExtraBackgroundNoiseType();
        
        // If we are going to use the extra background noise calculator we create it
        Calculator<Unit<Double>, Unit<Double>> extraCalculator = null;
        if (extraBackgroundNoiseType == ExtraBackgroundNoiseType.ONLY_EXTRA_BACKGROUND_NOISE
                || extraBackgroundNoiseType == ExtraBackgroundNoiseType.CALCULATED_AND_EXTRA_BACKGROUND_NOISE) {
            Pair<Double, String> pixelScale = new Pair<Double, String>(instrument.getPixelScale(), instrument.getPixelScaleUnit());
            Calculator<Unit<Double>, Unit<Double>> spatialBinningCalculator = new SpatialBinningFactory().getCalculator(configuration);
            DatasetInfo templateInfo = obsParam.getExtraBackgroundNoiseDataset();
            Quartet<Session, Dataset.Type, DatasetInfo, String> datasetConfiguration 
                    = new Quartet<Session, Dataset.Type, DatasetInfo, String>
                    (session, Dataset.Type.EXTRA_BACKGROUND_NOISE, templateInfo, null);
            Calculator<Unit<Double>, Unit<Double>> template = new DatasetFactory().getCalculator(datasetConfiguration);
            Pair<DatasetInfo, Calculator<Unit<Double>, Unit<Double>>> tempatePair =
                    new Pair<DatasetInfo, Calculator<Unit<Double>, Unit<Double>>>(templateInfo, template);
            
            Triplet<Pair<Double, String>, Calculator<Unit<Double>, Unit<Double>>, Pair<DatasetInfo, Calculator<Unit<Double>, Unit<Double>>>> calculatorConfiguration
                    = new Triplet<Pair<Double, String>, Calculator<Unit<Double>, Unit<Double>>, Pair<DatasetInfo, Calculator<Unit<Double>, Unit<Double>>>>
                    (pixelScale, spatialBinningCalculator, tempatePair);
            
            extraCalculator = EtcCalculatorManager.getManager(ExtraBackgroundNoise.class).getCalculator(calculatorConfiguration);
            // If we have only the extra background noise we return it
            if (extraBackgroundNoiseType == ExtraBackgroundNoiseType.ONLY_EXTRA_BACKGROUND_NOISE) {
                ResultsHolder.getResults().addResult(
                    new StringResult("BACKGROUND_NOISE_METHOD", "Extra background noise only"), Level.DEBUG);
                return extraCalculator;
            }
        }
        
        // If we are going to use the calculated background noise calculator we create it
        Calculator<Unit<Double>, Unit<Double>> calculatedCalculator = null;
        String calculatedMethod = "";
        if (extraBackgroundNoiseType == ExtraBackgroundNoiseType.ONLY_CALCULATED_BACKGROUND_NOISE
                || extraBackgroundNoiseType == ExtraBackgroundNoiseType.CALCULATED_AND_EXTRA_BACKGROUND_NOISE) {
            Calculator<Unit<Double>, Unit<Double>> flux = new BackgroundFluxFactory().getCalculator(configuration);
            Calculator<Unit<Double>, Unit<Double>> systemEfficiency = new SystemEfficiencyFactory().getCalculator(configuration);
            Calculator<Unit<Double>, Unit<Double>> filterResponse = new FilterResponseFactory().getCalculator(configuration);
            Calculator<Unit<Double>, Unit<Double>> telescopeArea = new TelescopeAreaFactory().getCalculator(configuration);
        
            if (instrument.getInstrumentType() == Instrument.InstrumentType.SPECTROGRAPH
                    && instrument.getSpectrographType() == Instrument.SpectrographType.SLITLESS) {
                calculatedMethod = "Slitness";
                Pair<Double, String> pixelScalePair = new Pair<Double, String>
                        (instrument.getPixelScale(), instrument.getPixelScaleUnit());
                Calculator<Unit<Double>, Unit<Double>> spectralResolution = new SpectralResolutionFactory().getCalculator(configuration);
                Calculator<Unit<Double>, Unit<Double>> spatialBinningCalculator = new SpatialBinningFactory().getCalculator(configuration);
                Calculator<Tuple, Pair<Double, Double>> integrationRange = new LambdaRangeFactory().getRangeOfTransmissionCalculator(configuration);
                Octet<Calculator<Unit<Double>, Unit<Double>>, Calculator<Unit<Double>, Unit<Double>>,
                        Calculator<Unit<Double>, Unit<Double>>, Pair<Double, String>,
                        Calculator<Unit<Double>, Unit<Double>>,
                        Calculator<Unit<Double>, Unit<Double>>, Calculator<Unit<Double>, Unit<Double>>,
                        Calculator<Tuple, Pair<Double, Double>>> slitlessConfiguration
                    = new Octet<Calculator<Unit<Double>, Unit<Double>>, Calculator<Unit<Double>, Unit<Double>>,
                        Calculator<Unit<Double>, Unit<Double>>, Pair<Double, String>,
                        Calculator<Unit<Double>, Unit<Double>>,
                        Calculator<Unit<Double>, Unit<Double>>, Calculator<Unit<Double>, Unit<Double>>,
                        Calculator<Tuple, Pair<Double, Double>>>
                            (flux, systemEfficiency, filterResponse, pixelScalePair, spectralResolution
                            , spatialBinningCalculator, telescopeArea, integrationRange);
                calculatedCalculator = EtcCalculatorManager.getManager(Slitless.class).getCalculator(slitlessConfiguration);

            } else {
                Calculator<Unit<Double>, Unit<Double>> skyArea = new SkyAreaFactory().getCalculator(configuration);
                calculatedMethod = "Background flux";
                Quintet<Calculator<Unit<Double>, Unit<Double>>, Calculator<Unit<Double>, Unit<Double>>
                        , Calculator<Unit<Double>, Unit<Double>>, Calculator<Unit<Double>, Unit<Double>>
                        , Calculator<Unit<Double>, Unit<Double>>> fluxConfiguration
                        = new Quintet(flux, skyArea, systemEfficiency, filterResponse, telescopeArea);
                calculatedCalculator = EtcCalculatorManager.getManager(Flux.class).getCalculator(fluxConfiguration);
            }
            
            // If we have only the calculated background noise we return it
            if (extraBackgroundNoiseType == ExtraBackgroundNoiseType.ONLY_CALCULATED_BACKGROUND_NOISE) {
                ResultsHolder.getResults().addResult(
                    new StringResult("BACKGROUND_NOISE_METHOD", calculatedMethod), Level.DEBUG);
                return calculatedCalculator;
            }
        } 

        // If we are here means that we have calculated and exta background noise
        ResultsHolder.getResults().addResult(
                new StringResult("BACKGROUND_NOISE_METHOD", calculatedMethod + " and extra background noise"), Level.DEBUG);
        Pair<Calculator<Unit<Double>, Unit<Double>>, Calculator<Unit<Double>, Unit<Double>>> calculatorConfiguration = 
                new Pair<Calculator<Unit<Double>, Unit<Double>>, Calculator<Unit<Double>, Unit<Double>>>(calculatedCalculator, extraCalculator);
        return EtcCalculatorManager.getManager(SumOfCalculatedAndExtra.class).getCalculator(calculatorConfiguration);
    }
}
