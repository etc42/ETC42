/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.util.datasets;

import java.util.Collections;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.SortedMap;
import java.util.TreeMap;
import org.javatuples.Pair;

/**
 * The {@code Dataset} class represents a set of key-value pairs. It provides the
 * basic access to these data.
 */
public class Dataset {
    
    private final NavigableMap<Double, Double> data;
    private final double minRange;
    private final double maxRange;
    private final double minZeroRange;
    private final double maxZeroRange;
    
    private static double globalMinRange = Double.NEGATIVE_INFINITY;
    private static double globalMaxRange = Double.POSITIVE_INFINITY;
    
    public static Pair<Double, Double> getGlobalBounds() {
    	return new Pair<Double, Double>(globalMinRange, globalMaxRange);
    }
    
    /**
     * Constructs a new {@code Dataset} object, representing the given data. Note
     * that the order of the given data will be ignored and the data will be stored
     * ordered by their key values.
     * @param data The data of the dataset
     */
    public Dataset(Map<Double, Double> data) {
        // NOTE TO DEVELOPERS: The data map must be an ordered map like TreeMap.
        // Do not change it to any type of map that is not ordered! (like HashMap)
        this.data = new TreeMap<Double, Double>();
        double min = Double.POSITIVE_INFINITY;
        double max = Double.NEGATIVE_INFINITY;
        for (Map.Entry<Double, Double> entry : data.entrySet()) {
            double key = entry.getKey();
            double value = entry.getValue();
            this.data.put(key, value);
            if (key < min) {
                min = key;
            }
            if (key > max) {
                max = key;
            }
        }
        minRange = min;
        maxRange = max;
        
        // global update
        if (minRange > globalMinRange)
        	globalMinRange = minRange;
        	
		if (maxRange < globalMaxRange)
			globalMaxRange = maxRange;
        
        //System.out.println("Datalength: " + data.size() + " - minRange : " + minRange + " - maxRange : " + maxRange + " - globalMinRange : " + globalMinRange + " - globalMaxRange : " + globalMaxRange);
        
        // Find the nodes for the zero range. We use the local map which is sorted.
        double minZero = min;
        double maxZero = max;
        boolean minZeroFound = false;
        boolean previousNotZero = false;
        for (Map.Entry<Double, Double> entry : this.data.entrySet()) {
            double key = entry.getKey();
            double value = entry.getValue();
            // The minZero will be taking the values of the keys until while we
            // have met only zeroes. Like this it will end up on the last node
            // of these zeroes. If we do not have starting zeroes then the minZero
            // will keep the default falue (the min of the dataset bounds)
            if (value == 0) {
                if (!minZeroFound) {
                    minZero = key;
                }
            } else {
                minZeroFound = true;
            }
            // We set the maxZero to the key of a node if the value of the node
            // is zero and the value of the previous node was not zero, or if the
            // value of the node is not zero. Like this the maxZero will be the
            // key of the first node of the last zero nodes or the max of the dataset
            // bounds if there are no ending zeroes
            if (value == 0) {
                if (previousNotZero) {
                    maxZero = key;
                }
                previousNotZero = false;
            } else {
                maxZero = key;
                previousNotZero = true;
            }
        }
        minZeroRange = minZero;
        maxZeroRange = maxZero;
    }
    
    /**
     * Returns the data of the dataset. Note that the returned map is designed
     * to be read only and any attempt to modify its contents will result to
     * an exception. The returned map is an ordered map and an iteration will
     * happen with based on increasing key values.
     * @return The data of the dataset
     */
    public SortedMap<Double, Double> getData() {
        return Collections.unmodifiableSortedMap(data);
    }

    /**
     * Returns the data node with the greatest key less than or equal with the
     * given key, or null if there is no such key.
     * @param key the key
     * @return  the data node with the greatest key less than or equal to key,
     * or null if there is no such key
     */
    public Entry<Double, Double> getFloorDataNode(double key) {
    	Entry<Double, Double> floorEntry = data.floorEntry(key);
        return floorEntry;
    }

    /**
     * Returns the data node with the least key greater than or equal with the
     * given key, or null if there is no such key.
     * @param key the key
     * @return  the data node with the least key greater than or equal to key,
     * or null if there is no such key
     */
    public Entry<Double, Double> getCeilingDataNode(double key) {
    	Entry<Double, Double> ceilingEntry = data.ceilingEntry(key);
        return ceilingEntry;
    }

    /**
     * Returns the data node with the greatest key strictly less than the
     * given key, or null if there is no such key.
     * @param key the key
     * @return  the data node with the greatest key strictly less than the key,
     * or null if there is no such key
     */
    public Entry<Double, Double> getLowerDataNode(double key) {
    	Entry<Double, Double> lowerEntry = data.lowerEntry(key);
        return lowerEntry;
    }

    /**
     * Returns the data node with the least key strictly greater than the
     * given key, or null if there is no such key.
     * @param key the key
     * @return  the data node with the least key strictly greater than the key,
     * or null if there is no such key
     */
    public Entry<Double, Double> getHigherDataNode(double key) {
    	Entry<Double, Double> higherEntry = data.higherEntry(key);
        return higherEntry;
    }
    
    /**
     * Returns a {@link org.javatuples.Pair} containing the minimum and maximum
     * key values of the dataset (its range).
     * @return The range of the dataset
     */
    public Pair<Double, Double> getBounds() {
        return new Pair<Double, Double>(minRange, maxRange);
    }
    
    /**
     * Returns a {@link org.javatuples.Pair} containing the minimum and maximum
     * keys between of which the values are non zero. Outside of this range any
     * value will be zero. Note that the values of these keys will be zero if the
     * non-zero bounds are inside the dataset bounds and they will have the dataset
     * bounds values if the two bounds are overlapping.
     * @return The non zero range of the dataset
     */
    public Pair<Double, Double> getNonZeroBounds() {
        return new Pair<Double, Double>(minZeroRange, maxZeroRange);
    }
    
}
