/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.simulatedsignal;

import java.util.ResourceBundle;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.atmospherictransmission.AtmosphericTransmissionFactory;
import org.cnrs.lam.dis.etc.calculator.filterresponse.FilterResponseFactory;
import org.cnrs.lam.dis.etc.calculator.flux.FluxFactory;
import org.cnrs.lam.dis.etc.calculator.normalizationfactor.NormalizationFactorFactory;
import org.cnrs.lam.dis.etc.calculator.signalSpectralConvolutionKernel.SignalSpectralConvolutionKernelFactory;
import org.cnrs.lam.dis.etc.calculator.systemefficiency.SystemEfficiencyFactory;
import org.cnrs.lam.dis.etc.calculator.telescopearea.TelescopeAreaFactory;
import org.cnrs.lam.dis.etc.calculator.util.functions.BoundedUnivariateFunction;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.cnrs.lam.dis.etc.datamodel.ObsParam;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.javatuples.Pair;
import org.javatuples.Septet;
import org.javatuples.Tuple;
import org.javatuples.Unit;

/**
 */
public class SimulatedSignalFactory
		implements Factory<Unit<Session>, Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>> {

	private static final ResourceBundle bundle = ResourceBundle
			.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");


	@Override
	public Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>> getCalculator(
			Unit<Session> configuration) throws InitializationException, ConfigurationException {
		ObsParam obsParam = configuration.getValue0().getObsParam();

		switch (obsParam.getExtraSignalType()) {
		case ONLY_EXTRA_SIGNAL:
			ResultsHolder.getResults().addResult(
					new CalculationResults.StringResult("SIMULATED_SIGNAL_METHOD", "Simulated signal ignored"),
					CalculationResults.Level.DEBUG);
			// System.out.println(ZeroSignal.class);
			return EtcCalculatorManager.getManager(ZeroSignal.class).getCalculator(configuration);
		case ONLY_CALCULATED_SIGNAL:
		case CALCULATED_AND_EXTRA_SIGNAL:
			ResultsHolder.getResults().addResult(
					new CalculationResults.StringResult("SIMULATED_SIGNAL_METHOD", "Signal Flux"),
					CalculationResults.Level.DEBUG);
			// Calculator<Unit<Double>, Unit<Double>> convolutionKernel =
			// new ConvolutionKernelSizeFactory().getCalculator(configuration);
			Calculator<Unit<Double>, Unit<BoundedUnivariateFunction>> convolutionKernelProducer = new SignalSpectralConvolutionKernelFactory()
					.getCalculator(configuration);
			
			
			Calculator<Tuple, Unit<UnivariateRealFunction>> flux = new FluxFactory().getCalculator(configuration);
			Calculator<Tuple, Pair<UnivariateRealFunction, UnivariateRealFunction>> normalizationFactor = new NormalizationFactorFactory()
					.getCalculator(configuration);
			Calculator<Tuple, Unit<BoundedUnivariateFunction>> atmosphericTransmission = new AtmosphericTransmissionFactory()
					.getCalculator(configuration);
			
			
			
			Calculator<Tuple, Unit<BoundedUnivariateFunction>> systemEfficiency = new SystemEfficiencyFactory()
					.getCalculator(configuration);
			
			
			
			Calculator<Tuple, Unit<BoundedUnivariateFunction>> filterResponse = new FilterResponseFactory()
					.getCalculator(configuration);
			
			
			
			Calculator<Tuple, Unit<Double>> telescopeArea = new TelescopeAreaFactory().getCalculator(configuration);
			Septet<Calculator<Unit<Double>, Unit<BoundedUnivariateFunction>>, Calculator<Tuple, Unit<UnivariateRealFunction>>, Calculator<Tuple, Pair<UnivariateRealFunction, UnivariateRealFunction>>, Calculator<Tuple, Unit<BoundedUnivariateFunction>>, Calculator<Tuple, Unit<BoundedUnivariateFunction>>, Calculator<Tuple, Unit<BoundedUnivariateFunction>>, Calculator<Tuple, Unit<Double>>> signalFluxConfiguration = new Septet<Calculator<Unit<Double>, Unit<BoundedUnivariateFunction>>, Calculator<Tuple, Unit<UnivariateRealFunction>>, Calculator<Tuple, Pair<UnivariateRealFunction, UnivariateRealFunction>>, Calculator<Tuple, Unit<BoundedUnivariateFunction>>, Calculator<Tuple, Unit<BoundedUnivariateFunction>>, Calculator<Tuple, Unit<BoundedUnivariateFunction>>, Calculator<Tuple, Unit<Double>>>(
					convolutionKernelProducer, flux, normalizationFactor, atmosphericTransmission, systemEfficiency,
					filterResponse, telescopeArea);
			

			// System.out.println(SignalFlux.class);

			EtcCalculatorManager<Septet<Calculator<Unit<Double>, Unit<BoundedUnivariateFunction>>, Calculator<Tuple, Unit<UnivariateRealFunction>>, Calculator<Tuple, Pair<UnivariateRealFunction, UnivariateRealFunction>>, Calculator<Tuple, Unit<BoundedUnivariateFunction>>, Calculator<Tuple, Unit<BoundedUnivariateFunction>>, Calculator<Tuple, Unit<BoundedUnivariateFunction>>, Calculator<Tuple, Unit<Double>>>, Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>, SignalFlux> manager;
			manager = EtcCalculatorManager.getManager(SignalFlux.class);
			SignalFlux calculator = manager.getCalculator(signalFluxConfiguration);
			return calculator;
		}
		String message = bundle.getString("UNKNOWN_SIMULATED_SIGNAL_METHOD");
		throw new ConfigurationException(message);
	}

}
