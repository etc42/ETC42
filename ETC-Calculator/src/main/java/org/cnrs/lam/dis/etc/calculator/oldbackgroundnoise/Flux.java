/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.oldbackgroundnoise;

import lombok.EqualsAndHashCode;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.Calculator;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Constants;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.javatuples.Quintet;
import org.javatuples.Unit;

/**
 */
@EqualsAndHashCode(callSuper=false)
public class Flux extends AbstractCalculator<Quintet<Calculator<Unit<Double>, Unit<Double>>,
                                                      Calculator<Unit<Double>, Unit<Double>>,
                                                      Calculator<Unit<Double>, Unit<Double>>,
                                                      Calculator<Unit<Double>, Unit<Double>>,
                                                      Calculator<Unit<Double>, Unit<Double>>>, 
                                               Unit<Double>, Unit<Double>> {
    
    private Calculator<Unit<Double>, Unit<Double>> flux;
    private Calculator<Unit<Double>, Unit<Double>> skyArea;
    private Calculator<Unit<Double>, Unit<Double>> systemEfficiency;
    private Calculator<Unit<Double>, Unit<Double>> filterResponse;
    private Calculator<Unit<Double>, Unit<Double>> telescopeArea;

    @Override
    protected void validateConfiguration(Quintet<Calculator<Unit<Double>, Unit<Double>>,
                                                  Calculator<Unit<Double>, Unit<Double>>,
                                                  Calculator<Unit<Double>, Unit<Double>>,
                                                  Calculator<Unit<Double>, Unit<Double>>,
                                                  Calculator<Unit<Double>, Unit<Double>>> configuration) throws ConfigurationException {
    }

    @Override
    protected void initialize(Quintet<Calculator<Unit<Double>, Unit<Double>>,
                                                  Calculator<Unit<Double>, Unit<Double>>,
                                                  Calculator<Unit<Double>, Unit<Double>>,
                                                  Calculator<Unit<Double>, Unit<Double>>,
                                                  Calculator<Unit<Double>, Unit<Double>>> configuration) throws InitializationException {
        flux = configuration.getValue0();
        skyArea = configuration.getValue1();
        systemEfficiency = configuration.getValue2();
        filterResponse = configuration.getValue3();
        telescopeArea = configuration.getValue4();
    }

    @Override
    protected Unit<Double> performCalculation(Unit<Double> input) throws CalculationException {
        double skyFluxValue = flux.calculate(input).getValue0();
        double skyAreaValue = skyArea.calculate(input).getValue0();
        double systemEfficiencyValue = systemEfficiency.calculate(input).getValue0();
        double filterResponseValue = filterResponse.calculate(input).getValue0();
        double telescopeAreaValue = telescopeArea.calculate(input).getValue0();
        double lambda = input.getValue0();
        
        double result = skyFluxValue * systemEfficiencyValue * skyAreaValue * filterResponseValue
                * telescopeAreaValue * lambda / (Constants.SPEED_OF_LIGHT * Constants.PLANK);
        
        return new Unit<Double>(result);
    }

    @Override
    protected void performForEveryCalculation(Unit<Double> input, Unit<Double> output) {
        ResultsHolder.getResults().addResult("BACKGROUND_NOISE", input.getValue0(),
                output.getValue0(), Units.ANGSTROM, Units.getElectronsPerSecPerAngstrom(), Level.INTERMEDIATE_UNIMPORTANT);
    }
    
}
