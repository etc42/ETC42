/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.oldspatialbinning;

import lombok.EqualsAndHashCode;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.Calculator;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.DoubleValueResult;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.javatuples.Quintet;
import org.javatuples.Unit;

/**
 * <p>The {@code ExtendedSource} class is a calculator for computing the spatial
 * binning, in the case the source is an extended source and the instrument is not
 * a fiber spectrograph. The equation used for the calculation is:</p>
 * {@latex.inline $
 * SpatialBinning_{(\\lambda)} = 2 * \\displaystyle
 * \\frac{\\sqrt{r_{obj}^2+FWHM_{(\\lambda)}^2}}{p}
 * $}
 * <p>where</p>
 * <ul>
 *   <li>{@latex.inline $SpatialBinning$}: is the spatial binning in 
 *       {@latex.inline $pixels$}</li>
 *   <li>{@latex.inline $\\lambda$}: is the wavelength expressed in angstrom 
 *       ({@latex.inline \\AA})</li>
 *   <li>{@latex.inline $FWHM$}: is the FWHM of the PSF in {@latex.inline $arcsec$}
 *       (see package {@code org.cnrs.lam.dis.etc.calculator.psffwhm})</li>
 *   <li>{@latex.inline $p$}: is the pixel scale of the instrument expressed in
 *       {@latex.inline $arcsec/pixel$}</li>
 *   <li>{@latex.inline $r_{obj}$}: is the effective radius of the target expressed in
 *       {@latex.inline $arcsec/pixel$}</li>
 * </ul>
 * 
 * <p>The configuration of the calculator is a {@link org.javatuples.Quintet},
 * with contains the following elements:</p>
 * <ol>
 *   <li>{@link java.lang.Double}: The pixel scale of the instrument</li>
 *   <li>{@link java.lang.String}: The unit of the pixel scale of the instrument</li>
 *   <li>{@link org.cnrs.lam.cesam.util.calculator.Calculator}: a calculator
 *       for calculating the FWHM of the PSF</li>
 *   <li>{@link java.lang.Double}: The effective radius of the target</li>
 *   <li>{@link java.lang.String}: The unit of the effective radius of the target</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the wavelength expressed in {@latex.inline \\AA}.</p>
 * 
 * <p>The output of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the spatial binning, expressed in
 * {@latex.inline $pixels$}.</p>
 */
@EqualsAndHashCode(callSuper=false)
public class ExtendedSource extends AbstractCalculator<Quintet<Double, String, Calculator<Unit<Double>, Unit<Double>>, Double, String>, Unit<Double>, Unit<Double>> {
    
    private double pixelScale;
    private Calculator<Unit<Double>, Unit<Double>> fwhmCalculator;
    private double sourceRadius;

    /**
     * Validates the given configuration. It requires that the first value of the
     * tuple (the pixel scale of the instrument) is a positive number, that the second
     * value (the unit of the pixel scale) is {@latex.inline $arcsec/pixel$}, that
     * the fourth value (the source radius) is a positive number and that the fifth
     * (the unit of the source radius) is {@latex.inline $arcsec$}.
     * @param configuration The configuration to validate
     * @throws ConfigurationException If the configuration is invalid
     */
    @Override
    protected void validateConfiguration(Quintet<Double, String, Calculator<Unit<Double>, Unit<Double>>, Double, String> configuration) throws ConfigurationException {
        // Check that the pixel scale is a possitive number
        if (configuration.getValue0() <= 0) {
            throw new ConfigurationException("Pixel scale must be a possitive "
                    + "number but was " + configuration.getValue0());
        }
        // Check if the pixel scale is in arcsec/pixel
        if (!Units.getArcsecPerPixel().equals(configuration.getValue1())) {
            throw new ConfigurationException("Pixel scale must be in " + Units.ARCSEC + "/"
                    + Units.PIXEL + " but was in " + configuration.getValue1());
        }
        // Check that the source radius is a possitive number
        if (configuration.getValue3() <= 0) {
            throw new ConfigurationException("Source radius must be a possitive "
                    + "number but was " + configuration.getValue3());
        }
        // Check if the source radius is in arcsec
        if (!Units.isArcsec(configuration.getValue4())) {
            throw new ConfigurationException("Source radius must be in " + Units.ARCSEC
                    + " but was in " + configuration.getValue4());
        }
    }

    /**
     * Initializes the calculator instance with the given configuration. The first
     * value of the configuration is the pixel scale of the instrument, the second is
     * the unit of the pixel scale, the third is a calculator for calculating the
     * FWHM of the PSF, the fourth is the source radius and the fifth the source
     * radius unit.
     * @param configuration the configuration to use for initialization
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(Quintet<Double, String, Calculator<Unit<Double>, Unit<Double>>, Double, String> configuration) throws InitializationException {
        pixelScale = configuration.getValue0();
        fwhmCalculator = configuration.getValue2();
        sourceRadius = configuration.getValue3();
    }

    /**
     * Returns a {@link org.javatuples.Unit} containing the Spatial Binning
     * expressed in {@latex.inline $pixels$}.
     * @param input The wavelength expressed in {@latex.inline \\AA}
     * @return The spatial binning expressed in {@latex.inline $pixels$}
     * @throws CalculationException if there is any error during the calculation
     */
    @Override
    protected Unit<Double> performCalculation(Unit<Double> input) throws CalculationException {
        double fwhm = fwhmCalculator.calculate(input).getValue0();
        double result = 2 * Math.sqrt((sourceRadius * sourceRadius) + (fwhm * fwhm)) / pixelScale;
        return new Unit<Double>(result);
    }

    /**
     * Overridden to add in the results the spatial binning.
     * @param input The wavelength expressed in {@latex.inline \\AA}
     * @param output The spatial binning expressed in {@latex.inline $pixels$}
     */
    @Override
    protected void performForEveryCalculation(Unit<Double> input, Unit<Double> output) {
        ResultsHolder.getResults().addResult("SPATIAL_BINNING", input.getValue0(),
                output.getValue0(), Units.ANGSTROM, Units.PIXEL, Level.INTERMEDIATE_IMPORTANT);
    }

    /**
     * Overridden to add in the results the pixel scale and the source radius.
     * @param configuration The configuration of the calculator
     */
    @Override
    protected void performForEveryRetrieval(Quintet<Double, String, Calculator<Unit<Double>, Unit<Double>>, Double, String> configuration) {
        ResultsHolder.getResults().addResult(new DoubleValueResult("PIXEL_SCALE",
                configuration.getValue0(), configuration.getValue1()), Level.DEBUG);
        ResultsHolder.getResults().addResult(new DoubleValueResult("SOURCE_RADIUS",
                configuration.getValue3(), configuration.getValue4()), Level.DEBUG);
    }
    
}
