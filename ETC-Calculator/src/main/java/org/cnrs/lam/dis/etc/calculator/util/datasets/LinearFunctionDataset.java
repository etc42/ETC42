/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.util.datasets;

import java.util.Map;
import java.util.SortedMap;
import org.apache.commons.math.FunctionEvaluationException;
import org.cnrs.lam.dis.etc.calculator.util.functions.BoundedUnivariateFunction;
import org.cnrs.lam.dis.etc.calculator.util.functions.IntegrableUnivariateFunction;
import org.javatuples.Pair;

/**
 * The {@link LinearFunctionDataset} is a {@link Dataset} the nodes of which
 * represent a function. It implements the {@link BoundedUnivariateRealFunction}
 * and {@link IntegrableUnivariateRealFunction} interfaces by performing linear
 * interpolation for the values of the function between the nodes.
 */
public class LinearFunctionDataset extends Dataset implements
        BoundedUnivariateFunction, IntegrableUnivariateFunction {

    /**
     * Constructs a new instance of {@link LinearFunctionDataset} with the given
     * data representing the nodes of the function. The keys of the map represent
     * the values of the X axis and the values of the map the Y axis. The values
     * of the function between the nodes are computed by linear interpolation.
     * @param data The nodes of the function
     */
    public LinearFunctionDataset(Map<Double, Double> data) {
        super(data);
    }
    
    /**
     * Returns the value of the function for the given X value. If the requested
     * value is out of range a {@link FunctionEvaluationException} is thrown.
     * @param x The X value for which the function is calculated
     * @return The value of the function for the given X value
     * @throws FunctionEvaluationException if the given X value is out of range
     */
    @Override
    public double value(double x) throws FunctionEvaluationException {
        // First make a check that we are inside the bounds
        Pair<Double, Double> bounds = getBounds();
        if (x < bounds.getValue0() || x > bounds.getValue1()) {
        	throw new FunctionEvaluationException(x);
        }
        
        // We get the floor entry from the data. Note that if there is a node
        // with the exact same key we return its value directly
        Map.Entry<Double, Double> floorDataNode = getFloorDataNode(x);
        if (floorDataNode == null) {
            throw new FunctionEvaluationException(x);
        }
        if (x == floorDataNode.getKey()) {
            return floorDataNode.getValue();
        }
        double floorX = floorDataNode.getKey();
        double floorValue = floorDataNode.getValue();
        // We get the ceiling entry from the data.
        Map.Entry<Double, Double> ceilingDataNode = getCeilingDataNode(x);
        if (ceilingDataNode == null) {
            throw new FunctionEvaluationException(x);
        }
        double ceilingX = ceilingDataNode.getKey();
        double ceilingValue = ceilingDataNode.getValue();
        // We return the linear interpolation of the requested X.
        double value = floorValue + (x - floorX) * (ceilingValue - floorValue) / (ceilingX - floorX);
        return value;
    }

    /**
     * Returns the integral of the represented function between the values X1 and X2.
     * Note that the integral is calculated by calculating the area of the intermediate
     * trapezoids.
     * @param x1 The low bound of the integration
     * @param x2 The high bound of the integration
     * @return The integral of the function in the range [x1,x2]
     * @throws FunctionEvaluationException if the range is out of bounds or x1 > x2
     * @throws IllegalArgumentException if the X1 is greater than X2
     */
    @Override
    public double integral(double x1, double x2) throws FunctionEvaluationException {
        // First check the x1 and x2. Note that if they are equal the integral is 0.
        if (x1 > x2) {
            throw new IllegalArgumentException("Illegal integration range [" + x1 + "," + x2 + "]");
        }
        if (x1 == x2) {
            return 0;
        }
        // Check that the integration range is inside the bounds of the function
        Pair<Double, Double> bounds = getBounds();
        if (x1 < bounds.getValue0()) {
            throw new FunctionEvaluationException(x1);
        }
        if (x2 > bounds.getValue1()) {
            throw new FunctionEvaluationException(x2);
        }
        
        // We get all the nodes which are between the x1 and x2. If there are no
        // nodes between x1 and x2 then we return directly the trapezoid they create
        SortedMap<Double, Double> nodesBetweenX1X2 = getData().subMap(x1, x2);
        if (nodesBetweenX1X2.isEmpty()) {
            return (value(x1) + value(x2)) * (x2 - x1) / 2;
        }
        
        // Here we know we must add a number of trapezoids.
        double integral = 0;
        
        // We add the trapezoid which is before the rest of nodes
        double lowestNodeX = nodesBetweenX1X2.firstKey();
        double lowestNodeValue = nodesBetweenX1X2.get(lowestNodeX);
        integral += (value(x1) + lowestNodeValue) * (lowestNodeX - x1) / 2;
        
        // We add the trapezoid which is after the rest of nodes
        double highestNodeX = nodesBetweenX1X2.lastKey();
        double highestNodeValue = nodesBetweenX1X2.get(highestNodeX);
        integral += (value(x2) + highestNodeValue) * (x2 - highestNodeX) / 2;
        
        // For each pair of nodes between the x1 and x2 we add in the integral
        // the trapezoid they create
        double lowX = Double.POSITIVE_INFINITY;
        double lowValue = Double.NaN;
        double highX;
        double highValue;
        for (Map.Entry<Double, Double> entry : nodesBetweenX1X2.entrySet()) {
            double key = entry.getKey();
            double value = entry.getValue();
            // Set the high X value
            highX = key;
            highValue = value;
            // We make a check that we are not on the first iteration (for which we do nothing)
            if (lowX != Double.POSITIVE_INFINITY) {
                integral += (lowValue + highValue) * (highX - lowX) / 2;
            }
            // Set the low node for the next iteration
            lowX = highX;
            lowValue = highValue;
        }
        return integral;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public Pair<Double, Double> getBounds() {
		Pair<Double, Double> bounds = super.getBounds();
        return bounds;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Pair<Double, Double> getNonZeroBounds() {
        return super.getNonZeroBounds();
    }
    
}
