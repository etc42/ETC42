/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.simulatedsignal;

import lombok.EqualsAndHashCode;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.Cacheable;
import org.cnrs.lam.cesam.util.calculator.CachingPolicy;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.util.functions.ZeroUnivariateFunction;
import org.cnrs.lam.dis.etc.configuration.ConfigFactory;
import org.javatuples.Pair;
import org.javatuples.Tuple;
import org.javatuples.Unit;

/**
 * <p>The {@link ZeroSignal} class is a calculator for computing the 
 * number of electrons counted by the instrument from the simulated observed
 * source. In this case the signal is always zero.</p>
 * 
 * <p>The configuration of the calculator is a {@link org.javatuples.Tuple}, as this
 * calculator does not accept any input. It should always be null.</p>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Pair} which contains
 * two {@link Double} values representing the wavelength range (expressed in
 * {@latex.inline \\AA}) for which the returned calculators perform the calculation.</p>
 * 
 * <p>The output of the calculator is a {@link Pair} which contains two 
 * {@link BoundedUnivariateFunction} for computing the signal in the central 
 * pixel and the total spatial binning accordingly.</p>
 */
@Cacheable(CachingPolicy.ALL)
@EqualsAndHashCode(callSuper=false)
public class ZeroSignal extends AbstractCalculator<Tuple, Pair<Double, Double>,
        Pair<UnivariateRealFunction, UnivariateRealFunction>> {

    /**
     * Does nothing as this calculator does not accept any configuration.
     * @param configuration
     * @throws ConfigurationException 
     */
    @Override
    protected void validateConfiguration(Tuple configuration) throws ConfigurationException {
        // Do nothing here as we do not have any configuration
    }

    /**
     * Does nothing as this calculator does not accept any configuration.
     * @param configuration
     * @throws InitializationException 
     */
    @Override
    protected void initialize(Tuple configuration) throws InitializationException {
        // Do nothing here as we do not have any configuration
    }

    /**
     * Returns a {@link Pair} containing the functions to calculate the signal
     * in the central pixel and the total spatial binning accordingly.
     * @param input The wavelength range expressed in Angstrom
     * @return A {@link Pair} containing the central pixel and total signal functions
     * @throws CalculationException if there is any error during the calculation
     */
    @Override
    protected Pair<UnivariateRealFunction, UnivariateRealFunction>
            performCalculation(Pair<Double, Double> input) throws CalculationException {
        UnivariateRealFunction cpFunction = ConfigFactory.getConfig().getCentralPixelFlag()
                ? new ZeroUnivariateFunction() : null;
        UnivariateRealFunction totalFunction = new ZeroUnivariateFunction();
        return new Pair<UnivariateRealFunction, UnivariateRealFunction>(cpFunction, totalFunction);
    }

}
