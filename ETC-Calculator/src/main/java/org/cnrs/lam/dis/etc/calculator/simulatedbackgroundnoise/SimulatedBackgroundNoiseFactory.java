/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.simulatedbackgroundnoise;

import java.util.ResourceBundle;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.backgroundflux.BackgroundFluxFactory;
import org.cnrs.lam.dis.etc.calculator.convolutionkernelsize.ConvolutionKernelSizeFactory;
import org.cnrs.lam.dis.etc.calculator.deltalambda.DeltaLambdaFactory;
import org.cnrs.lam.dis.etc.calculator.filterresponse.FilterResponseFactory;
import org.cnrs.lam.dis.etc.calculator.numberofpixels.NumberOfPixelsFactory;
import org.cnrs.lam.dis.etc.calculator.skyarea.SkyAreaFactory;
import org.cnrs.lam.dis.etc.calculator.spectralresolution.SpectralResolutionFactory;
import org.cnrs.lam.dis.etc.calculator.systemefficiency.SystemEfficiencyFactory;
import org.cnrs.lam.dis.etc.calculator.telescopearea.TelescopeAreaFactory;
import org.cnrs.lam.dis.etc.calculator.util.functions.BoundedUnivariateFunction;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.cnrs.lam.dis.etc.datamodel.Instrument;
import org.cnrs.lam.dis.etc.datamodel.ObsParam;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.javatuples.Ennead;
import org.javatuples.Pair;
import org.javatuples.Sextet;
import org.javatuples.Tuple;
import org.javatuples.Unit;

/**
 */
public class SimulatedBackgroundNoiseFactory implements Factory<Unit<Session>, Pair<Double, Double>,
        Pair<UnivariateRealFunction, UnivariateRealFunction>> {

    private static final ResourceBundle bundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");

    @Override
    public Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>
            getCalculator(Unit<Session> configuration) throws InitializationException, ConfigurationException {
        ObsParam obsParam = configuration.getValue0().getObsParam();
        Instrument instrument = configuration.getValue0().getInstrument();
//        System.out.println(obsParam.getExtraBackgroundNoiseType());
        switch (obsParam.getExtraBackgroundNoiseType()) {
            case ONLY_EXTRA_BACKGROUND_NOISE:
                ResultsHolder.getResults().addResult(new CalculationResults.StringResult(
                        "SIMULATED_BACKGROUND_NOISE_METHOD", "Simulated background noise ignored"), CalculationResults.Level.DEBUG);
//                System.out.println(Zero.class);
                return EtcCalculatorManager.getManager(Zero.class).getCalculator(null);
            case ONLY_CALCULATED_BACKGROUND_NOISE:
            case CALCULATED_AND_EXTRA_BACKGROUND_NOISE:
                Calculator<Unit<Double>, Unit<Double>> convolutionKernel =
                        new ConvolutionKernelSizeFactory().getCalculator(configuration);
                Calculator<Tuple, Unit<UnivariateRealFunction>> backgroundFlux =
                        new BackgroundFluxFactory().getCalculator(configuration);
                Calculator<Tuple, Unit<BoundedUnivariateFunction>> systemEfficiency =
                        new SystemEfficiencyFactory().getCalculator(configuration);
                Calculator<Tuple, Unit<BoundedUnivariateFunction>> filterResponse =
                        new FilterResponseFactory().getCalculator(configuration);
                Calculator<Tuple, Unit<Double>> telescopeArea =
                        new TelescopeAreaFactory().getCalculator(configuration);
                if (instrument.getInstrumentType() == Instrument.InstrumentType.SPECTROGRAPH
                        && instrument.getSpectrographType() == Instrument.SpectrographType.SLITLESS) {
                    ResultsHolder.getResults().addResult(new CalculationResults.StringResult(
                            "SIMULATED_BACKGROUND_NOISE_METHOD", "Slitless Spectrograph"), CalculationResults.Level.DEBUG);
                    Calculator<Unit<Double>, Unit<Double>> numberOfPixels = new NumberOfPixelsFactory().getCalculator(configuration);
                    double pixelScale = instrument.getPixelScale();
                    String pixelScaleUnit = instrument.getPixelScaleUnit();
                    Pair<Double, String> pixelScalePair = new Pair<Double, String>(pixelScale, pixelScaleUnit);
                    Calculator<Unit<Double>, Unit<Double>> deltaLambda = new DeltaLambdaFactory().getCalculator(configuration);
                    Calculator<Tuple, Unit<UnivariateRealFunction>> spectralResolution = new SpectralResolutionFactory().getCalculator(configuration);
                    Ennead<Calculator<Unit<Double>, Unit<Double>>, Calculator<Tuple, Unit<UnivariateRealFunction>>,
                            Calculator<Tuple, Unit<BoundedUnivariateFunction>>, Calculator<Tuple, Unit<BoundedUnivariateFunction>>,
                            Calculator<Tuple, Unit<Double>>, Calculator<Unit<Double>, Unit<Double>>, Pair<Double, String>,
                            Calculator<Unit<Double>, Unit<Double>>, Calculator<Tuple, Unit<UnivariateRealFunction>>> slitlessConfiguration =
                    new Ennead<Calculator<Unit<Double>, Unit<Double>>, Calculator<Tuple, Unit<UnivariateRealFunction>>,
                            Calculator<Tuple, Unit<BoundedUnivariateFunction>>, Calculator<Tuple, Unit<BoundedUnivariateFunction>>,
                            Calculator<Tuple, Unit<Double>>, Calculator<Unit<Double>, Unit<Double>>, Pair<Double, String>,
                            Calculator<Unit<Double>, Unit<Double>>, Calculator<Tuple, Unit<UnivariateRealFunction>>>
                    (convolutionKernel, backgroundFlux, systemEfficiency, filterResponse, telescopeArea, numberOfPixels, pixelScalePair, deltaLambda, spectralResolution);
//                    System.out.println(Slitless.class);
                    return EtcCalculatorManager.getManager(Slitless.class).getCalculator(slitlessConfiguration);
                } else {
                    ResultsHolder.getResults().addResult(new CalculationResults.StringResult(
                            "SIMULATED_BACKGROUND_NOISE_METHOD", "Background Flux"), CalculationResults.Level.DEBUG);
                    Calculator<Tuple, Pair<UnivariateRealFunction, UnivariateRealFunction>> skyArea
                            = new SkyAreaFactory().getCalculator(configuration);
                    Sextet<Calculator<Unit<Double>, Unit<Double>>, Calculator<Tuple, Unit<UnivariateRealFunction>>,
                            Calculator<Tuple, Unit<BoundedUnivariateFunction>>, Calculator<Tuple, Unit<BoundedUnivariateFunction>>,
                            Calculator<Tuple, Unit<Double>>, Calculator<Tuple, Pair<UnivariateRealFunction, UnivariateRealFunction>>> fluxConfiguration =
                    new Sextet<Calculator<Unit<Double>, Unit<Double>>, Calculator<Tuple, Unit<UnivariateRealFunction>>,
                            Calculator<Tuple, Unit<BoundedUnivariateFunction>>, Calculator<Tuple, Unit<BoundedUnivariateFunction>>,
                            Calculator<Tuple, Unit<Double>>, Calculator<Tuple, Pair<UnivariateRealFunction, UnivariateRealFunction>>>
                    (convolutionKernel, backgroundFlux, systemEfficiency, filterResponse, telescopeArea, skyArea);
//                    System.out.println(Flux.class);
                    return EtcCalculatorManager.getManager(Flux.class).getCalculator(fluxConfiguration);
                }
        }
        String message = bundle.getString("UNKNOWN_SIMULATED_BACKGROUND_NOISE_METHOD");
        throw new ConfigurationException(message);
    }
    
}
