/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.util;

import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.apache.log4j.Logger;
import org.cnrs.lam.dis.etc.calculator.util.datasets.LinearFunctionDataset;
import org.cnrs.lam.dis.etc.calculator.util.datasets.TemplateFunctionDataset;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;

/**
 */
public class FunctionToDatasetResultConverter {
    
    private final static Logger logger = Logger.getLogger(FunctionToDatasetResultConverter.class);
    private static Double minRange;
    private static Double maxRange;
    
    
    public static void setRange(double min, double max) {
        minRange = min;
        maxRange = max;
    }
    
    /**
     * Converts the given function to a result set in the given range.
     * @param min The minimum of the range
     * @param max The maximum of the range
     * @param function The function to convert
     * @param name The name of the result
     * @param xUnit The unit of the X axis of the result
     * @param yUnit The unit of the Y axis of the result
     * @return The converted function in the given range
     */
    public static CalculationResults.DoubleDatasetResult convert(double min, double max,
            UnivariateRealFunction function, String name, String xUnit, String yUnit) {
        Map<Double, Double> data = null;
        try {
        	
            data = getData(min, max, function);
        } catch (FunctionEvaluationException ex) {
            logger.error(ex.getMessage(), ex);
        }
        return new CalculationResults.DoubleDatasetResult(name, data, xUnit, yUnit);
    }
    
    /**
     * Converts the given function to a result. The range of the result is best
     * calculated based on the given function
     * @param min The minimum of the range
     * @param max The maximum of the range
     * @param function The function to convert
     * @param name The name of the result
     * @param xUnit The unit of the X axis of the result
     * @param yUnit The unit of the Y axis of the result
     * @return The converted function in the given range
     */
    public static CalculationResults.DoubleDatasetResult convert(UnivariateRealFunction function, String name, String xUnit, String yUnit) {
        Map<Double, Double> data = null;
        
        try {
            data = getData(function);
        } catch (FunctionEvaluationException ex) {
            logger.error(ex.getMessage(), ex);
        }

        return new CalculationResults.DoubleDatasetResult(name, data, xUnit, yUnit);
    }
    
    private static Map<Double, Double> getData(double min, double max, UnivariateRealFunction function) throws FunctionEvaluationException {
        if (function instanceof TemplateFunctionDataset) {
            return getDataTemplateFunctionDataset(min, max, (TemplateFunctionDataset) function);
        }
        if (function instanceof LinearFunctionDataset) {
            return getDataLinearFunctionDataset(min, max, (LinearFunctionDataset) function);
        }
        Map<Double, Double> resultData = new TreeMap<Double, Double>();
        double step = (max - min) / 1000;
        for (double x = min; x <= max; x += step) {
            resultData.put(x, function.value(x));
        }

        return resultData;
    }
    
    private static Map<Double, Double> getData(UnivariateRealFunction function) throws FunctionEvaluationException {
        if (function instanceof TemplateFunctionDataset) {
            return getDataTemplateFunctionDataset((TemplateFunctionDataset) function);
        }
        if (function instanceof LinearFunctionDataset) {
            return getDataLinearFunctionDataset((LinearFunctionDataset) function);
        }
        return getData(minRange, maxRange, function);
    }
    
    private static Map<Double, Double> getDataTemplateFunctionDataset(double min, double max, TemplateFunctionDataset function) throws FunctionEvaluationException {
        Map<Double, Double> resultData = new TreeMap<Double, Double>();
        for (Map.Entry<Double, Double> entry : function.getData().entrySet()) {
            double key = entry.getKey();
            if (entry.getKey() < min && entry.getKey() > max) {
                continue;
            }
            resultData.put(key, function.value(key));
            Entry<Double, Double> lowerDataNode = function.getLowerDataNode(key);
            Entry<Double, Double> higherDataNode = function.getHigherDataNode(key);
            double lowMiddle = (lowerDataNode == null)
                    ? key - (higherDataNode.getKey() - key) / 2
                    : key - (key - lowerDataNode.getKey()) / 2;
            double highMiddle = (higherDataNode == null)
                    ? key + (key - lowerDataNode.getKey()) / 2
                    : key + (higherDataNode.getKey() - key) / 2;
            resultData.put(lowMiddle, function.value(lowMiddle));
            resultData.put(highMiddle, function.value(highMiddle));
        }
        return resultData;
    }
    
    private static Map<Double, Double> getDataTemplateFunctionDataset(TemplateFunctionDataset function) throws FunctionEvaluationException {
        Map<Double, Double> resultData = new TreeMap<Double, Double>();
        for (Map.Entry<Double, Double> entry : function.getData().entrySet()) {
            double key = entry.getKey();
            resultData.put(key, function.value(key));
            Entry<Double, Double> lowerDataNode = function.getLowerDataNode(key);
            Entry<Double, Double> higherDataNode = function.getHigherDataNode(key);
            double lowMiddle = (lowerDataNode == null)
                    ? key - (higherDataNode.getKey() - key) / 2
                    : key - (key - lowerDataNode.getKey()) / 2;
            double highMiddle = (higherDataNode == null)
                    ? key + (key - lowerDataNode.getKey()) / 2
                    : key + (higherDataNode.getKey() - key) / 2;
            resultData.put(lowMiddle, function.value(lowMiddle));
            resultData.put(highMiddle, function.value(highMiddle));
        }
        return resultData;
    }
    
    private static Map<Double, Double> getDataLinearFunctionDataset(double min, double max, LinearFunctionDataset function) {
        Map<Double, Double> resultData = new TreeMap<Double, Double>();
        for (Map.Entry<Double, Double> entry : function.getData().entrySet()) {
            if (entry.getKey() >= min && entry.getKey() <= max) {
                resultData.put(entry.getKey(), entry.getValue());
            }
        }
        return resultData;
    }
    
    private static Map<Double, Double> getDataLinearFunctionDataset(LinearFunctionDataset function) {
        return function.getData();
    }
    
    public static void main(String[] args) throws Exception {
        Map<Double, Double> data = new TreeMap<Double, Double>();
        data.put(1., 1.);
        data.put(2., 10.);
        data.put(3., 5.);
        LinearFunctionDataset lfd = new LinearFunctionDataset(data);
        FunctionToDatasetResultConverter.convert(lfd, "Linear Function Dataset", "xUnit", "yUnit");
    }
    
}
