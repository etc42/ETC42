/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.signalSpectralConvolutionKernel;

import java.util.ResourceBundle;
import org.apache.commons.math.analysis.BivariateRealFunction;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.dis.etc.calculator.convolutionkernelsize.ConvolutionKernelSizeFactory;
import org.cnrs.lam.dis.etc.calculator.covolvedsurfacebrightnessprofile.ConvolvedSurfaceBrightnessProfileFactory;
import org.cnrs.lam.dis.etc.calculator.psf.PsfFactory;
import org.cnrs.lam.dis.etc.calculator.spectralresolution.SpectralResolutionFactory;
import org.cnrs.lam.dis.etc.calculator.util.functions.BoundedUnivariateFunction;
import org.cnrs.lam.dis.etc.datamodel.Instrument;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.javatuples.Pair;
import org.javatuples.Quartet;
import org.javatuples.Tuple;
import org.javatuples.Unit;

/**
 */
public class SignalSpectralConvolutionKernelFactory implements Factory<Unit<Session>, Unit<Double>, Unit<BoundedUnivariateFunction>> {

    
    private static final ResourceBundle bundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");
    @Override
    public Calculator<Unit<Double>, Unit<BoundedUnivariateFunction>> getCalculator(Unit<Session> configuration) throws InitializationException, ConfigurationException {
        Session session = configuration.getValue0();
        if (session.getInstrument().getInstrumentType() == Instrument.InstrumentType.IMAGING) {
            return EtcCalculatorManager.getManager(UnitKernel.class).getCalculator(null);
        }
        Calculator<Unit<Double>, Unit<BivariateRealFunction>> profile = null;
        switch (session.getSource().getSpatialDistributionType()) {
            case POINT_SOURCE:
                profile = new PsfFactory().getCalculator(configuration);
                break;
            case EXTENDED_SOURCE:
                profile = new ConvolvedSurfaceBrightnessProfileFactory().getCalculator(configuration);
        }
        Calculator<Unit<Double>, Unit<Double>> kernelSize = new ConvolutionKernelSizeFactory().getCalculator(configuration);
        Calculator<Tuple, Unit<UnivariateRealFunction>> spectralResolution = new SpectralResolutionFactory().getCalculator(configuration);
        Pair<Double, String> pixelScale = new Pair<Double, String>(session.getInstrument().getPixelScale(), session.getInstrument().getPixelScaleUnit());
        
        Quartet<Calculator<Unit<Double>, Unit<BivariateRealFunction>>,
                Calculator<Unit<Double>, Unit<Double>>,
                Calculator<Tuple, Unit<UnivariateRealFunction>>,
                Pair<Double, String>> calculatorConfiguration
            = new Quartet<Calculator<Unit<Double>, Unit<BivariateRealFunction>>,
                    Calculator<Unit<Double>, Unit<Double>>,
                    Calculator<Tuple, Unit<UnivariateRealFunction>>,
                    Pair<Double, String>>(profile, kernelSize, spectralResolution, pixelScale);
        return EtcCalculatorManager.getManager(NormalizedKernelFromPsf.class).getCalculator(calculatorConfiguration);
    }

}
