/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.psf;

import java.text.MessageFormat;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TreeMap;
import lombok.EqualsAndHashCode;
import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.analysis.BivariateRealFunction;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.apache.log4j.Logger;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.DatasetProviderHolder;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.calculator.util.datasets.LinearFunctionDataset;
import org.cnrs.lam.dis.etc.calculator.util.functions.SymmetricBivariateGaussian;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.javatuples.Unit;

/**
 * <p>The {@code InstrumentPsfFwhmProfile} class is a calculator for computing the PSF
 * of the instrument following a circularly symmetric bivariate gaussian distribution 
 * defined by a FWHM given with a template.</p>
 * 
 * <p>The configuration of the calculator is a {@link org.javatuples.Unit},
 * with contains the following elements:</p>
 * <ol>
 *   <li>{@link org.cnrs.lam.dis.etc.datamodel.DatasetInfo}: the information of the
 *       instruments PSF FWHM dataset</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the wavelength for which the calculation
 * is done (expressed in {@latex.inline \\AA}).</p>
 * 
 * <p>The output of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link BivariateRealFunction} representing the PSF (both axes are expressed in
 * {@latex.inline $arcsec$}).</p>
 */

// Caching explenation
// -------------------
// This calculator is set to cache the instances because the configuration validation
// and the instantiation are retrieving the dataset, which is time consuming.
@Cacheable(CachingPolicy.INSTANCE)
@EqualsAndHashCode(callSuper=false, exclude={"psfFwhmFunction"})
public class InstrumentPsfFwhmProfile extends AbstractCalculator<Unit<DatasetInfo>, Unit<Double>, Unit<BivariateRealFunction>> {
    
    private static final Logger logger = Logger.getLogger(InstrumentPsfFwhmProfile.class);
    // The bundle with all the error translations
    private static final ResourceBundle validationErrorsBundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");
    // This is a variable where the PSF FWHM profile dataset info is stored.
    // This is used from lombok library for the equals and the hashCode methods.
    // We do not want to use the function we create, as this would be way too time
    // consuming.
    private DatasetInfo psfFwhmDatasetInfo;
    // The dataset which represents the PSF FWHM
    private LinearFunctionDataset psfFwhmFunction;

    /**
     * Validates the given configuration. It requires that the instrument has a
     * PSF FWHM profile, that the data for the profile is available, that the 
     * unit of X axis is {@latex.inline \\AA} and the unit of Y axis is {@latex.inline $arcsec$}.
     * @param configuration the configuration to validate
     * @throws ConfigurationException if the configuration is invalid
     */
    @Override
    protected void validateConfiguration(Unit<DatasetInfo> configuration) throws ConfigurationException {
        DatasetInfo info = configuration.getValue0();
        if (info == null) {
            String message = validationErrorsBundle.getString("PSF_FWHM_PROFILE_MISSING");
            throw new ConfigurationException(message);
        }
        Dataset dataset = DatasetProviderHolder.getDatasetProvider().getDataset(Dataset.Type.FWHM, info);
        if (dataset == null) {
            String message = validationErrorsBundle.getString("PSF_FWHM_PROFILE_NOT_AVAILABLE");
            message = MessageFormat.format(message, info);
            throw new ConfigurationException(message);
        }
        if (!Units.isAngstrom(dataset.getXUnit())) {
            String message = validationErrorsBundle.getString("PSF_FWHM_PROFILE_WRONG_X_UNIT");
            message = MessageFormat.format(message, Units.ANGSTROM, dataset.getXUnit());
            throw new ConfigurationException(message);
        }
        if (!Units.isArcsec(dataset.getYUnit())) {
            String message = validationErrorsBundle.getString("PSF_FWHM_PROFILE_WRONG_Y_UNIT");
            message = MessageFormat.format(message, Units.ARCSEC, dataset.getXUnit());
            throw new ConfigurationException(message);
        }
    }

    /**
     * Initializes the calculator instance with the given configuration. The
     * configuration contains the PSF FWHM profile dataset info.
     * @param configuration the configuration to use for initialization
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(Unit<DatasetInfo> configuration) throws InitializationException {
        psfFwhmDatasetInfo = configuration.getValue0();
        Dataset dataset = DatasetProviderHolder.getDatasetProvider().getDataset(Dataset.Type.FWHM, psfFwhmDatasetInfo);
        psfFwhmFunction = new LinearFunctionDataset(dataset.getData());
    }

    /**
     * Returns a {@link org.javatuples.Unit} containing a bivariate function, of
     * which both axes are expressed in {@latex.inline $arcsec$}, which represents
     * the PSF for the given wavelength.
     * @param input The wavelength expressed in {@latex.inline \\AA}
     * @return A function representing the PSF
     * @throws CalculationException if there is any error during the calculation
     */
    @Override
    protected Unit<BivariateRealFunction> performCalculation(Unit<Double> input) throws CalculationException {
        double lambda = input.getValue0();
        double fwhm = 0;
        try {
            fwhm = this.psfFwhmFunction.value(lambda);
        } catch (FunctionEvaluationException ex) {
            logger.warn("Failed to calculate the FWHM of the PSF from the profile", ex);
        }
        double sigma = fwhm / 2.3548;
        SymmetricBivariateGaussian gaussian = new SymmetricBivariateGaussian(sigma);
        return new Unit<BivariateRealFunction>(gaussian);
    }

    /**
     * <p>For every calculation the calculator adds in the simulation results
     * the following outputs of the calculator:</p>
     * <ul>
     *   <li>The FWHM of the instruments PSF in intermediate unimportant results
     *       with code INSTRUMENT_PSF_FWHM</li>
     *   <li> The projection of the PSF on the plane y=0 in the intermediate
     *       unimportant results with code INSTRUMENT_PSF_PROJECTION</li>
     * </ul>
     * @param input
     * @param output 
     */
    @Override
    protected void performForEveryCalculation(Unit<Double> input, Unit<BivariateRealFunction> output) {
        double lambda = input.getValue0();
        // We know that we have a symmetric bivariate gaussian so the casting is safe
        SymmetricBivariateGaussian gaussian = (SymmetricBivariateGaussian) output.getValue0();
        double sigma = gaussian.getStandardDeviation();
        
        // We add the FWHM of the PSF in the results
        double fwhm = sigma * 2.3548;
        ResultsHolder.getResults().addResult("INSTRUMENT_PSF_FWHM", lambda, fwhm, 
                Units.ANGSTROM, Units.ARCSEC, CalculationResults.Level.INTERMEDIATE_UNIMPORTANT);
        
        // We add the projection on the y = 0 plane in the results. We calculate
        // the valuse in the range of 5 sigma, as all the values outside of this
        // are too close to zero.
        UnivariateRealFunction projetion = gaussian.projectionFunction();
        Map<Double, Double> projectionData = new TreeMap<Double, Double>();
        // We will create 100 points to represent the plot
        double step = sigma / 20;
        for (double x = -5 * sigma; x <= 5 * sigma; x += step) {
            double value = 0;
            try {
                value = projetion.value(x);
            } catch (FunctionEvaluationException ex) {
                logger.warn("Failed to calculate the value of the PSF projection", ex);
            }
            projectionData.put(x, value);
        }
        CalculationResults.DoubleDatasetResult projectionResult = 
                new CalculationResults.DoubleDatasetResult("INSTRUMENT_PSF_PROJECTION", projectionData, Units.ARCSEC, null);
        ResultsHolder.getResults().addResult("INSTRUMENT_PSF_PROJECTION", lambda, projectionResult,
                Units.ANGSTROM, CalculationResults.Level.INTERMEDIATE_UNIMPORTANT);
    }

    /**
     * <p>For every calculator instantiation the following parts of its configuration
     * are added in the simulation results:</p>
     * <ul>
     *   <li>The name of the instruments PSF FWHM  profile with code INSTRUMENT_PSF_FWHM_PROFILE</li>
     * </ul>
     * @param configuration 
     */
    @Override
    protected void performForEveryRetrieval(Unit<DatasetInfo> configuration) {
        CalculationResults.StringResult result = new CalculationResults.StringResult(
                "INSTRUMENT_PSF_FWHM_PROFILE", configuration.getValue0().toString());
        ResultsHolder.getResults().addResult(result, CalculationResults.Level.DEBUG);
    }
    
}
