/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.covolvedsurfacebrightnessprofile;

import java.util.ResourceBundle;
import org.apache.commons.math.analysis.BivariateRealFunction;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.dis.etc.calculator.psf.PsfFactory;
import org.cnrs.lam.dis.etc.calculator.psfsize.PsfSizeFactory;
import org.cnrs.lam.dis.etc.calculator.surfacebrightnessprofile.SurfaceBrightnessProfileFactory;
import org.cnrs.lam.dis.etc.configuration.ConfigFactory;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.cnrs.lam.dis.etc.datamodel.Source;
import org.javatuples.Pair;
import org.javatuples.Quintet;
import org.javatuples.Tuple;
import org.javatuples.Unit;

/**
 * The {@link ConvolvedSurfaceBrightnessProfileFactory} creates calculators for
 * computing the convolution of the surface brightness profile with the PSF. Note
 * that currently only circularly symmetric functions are supported.
 */
public class ConvolvedSurfaceBrightnessProfileFactory implements Factory<Unit<Session>, Unit<Double>, Unit<BivariateRealFunction>> {
    
    private static final ResourceBundle bundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");

    @Override
    public Calculator<Unit<Double>, Unit<BivariateRealFunction>> getCalculator(Unit<Session> configuration) throws InitializationException, ConfigurationException {
        Session session = configuration.getValue0();
        Source source = session.getSource();
        Pair<Double, String> sourceRadiusPair = new Pair<Double, String>(source.getExtendedSourceRadius(), source.getExtendedSourceRadiusUnit());
        Calculator<Tuple, Unit<BivariateRealFunction>> sbpCalculator = new SurfaceBrightnessProfileFactory().getCalculator(configuration);
        Calculator<Unit<Double>, Unit<BivariateRealFunction>> psfCalculator = new PsfFactory().getCalculator(configuration);
        Calculator<Unit<Double>, Unit<Double>> psfSizeCalculator = new PsfSizeFactory().getCalculator(configuration);
        Integer samplingSize = ConfigFactory.getConfig().getPsfConvolutionSamplingSize();
        Quintet<Pair<Double, String>, Calculator<Tuple, Unit<BivariateRealFunction>>,
                    Calculator<Unit<Double>, Unit<BivariateRealFunction>>,
                    Calculator<Unit<Double>, Unit<Double>>,
                    Integer> calculatorConfiguration = 
                new Quintet<Pair<Double, String>, Calculator<Tuple, Unit<BivariateRealFunction>>,
                            Calculator<Unit<Double>, Unit<BivariateRealFunction>>,
                            Calculator<Unit<Double>, Unit<Double>>, Integer>
                (sourceRadiusPair, sbpCalculator, psfCalculator, psfSizeCalculator, samplingSize);
        if (ConfigFactory.getConfig().getProjectionConvolutionFlag()) {
            return EtcCalculatorManager.getManager(FftConvolutionOfProjection.class).getCalculator(calculatorConfiguration);
        } else {
            return EtcCalculatorManager.getManager(FftConvolution.class).getCalculator(calculatorConfiguration);
        }
    }
    
}
