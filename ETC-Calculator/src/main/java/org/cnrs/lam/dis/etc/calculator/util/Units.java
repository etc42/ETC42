/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.util;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 */
public class Units {
    
    public static List<String> getAvailableUnitList() {
        List<String> list = new LinkedList<String>();
        list.add(ANGSTROM);
        list.add(getAngstromPerPixel());
        list.add(ARCSEC);
        list.add(CM);
        list.add(CM2);
        list.add(ELECTRONS);
        list.add(ERG);
        list.add(KELVINS);
        list.add(PHOTONS);
        list.add(PIXEL);
        list.add(SEC);
        list.add(getArcsec2());
        list.add(getArcsecPerPixel());
        list.add(getElectronsPerPixel());
        list.add(getElectronsPerPixelPerSec());
        list.add(getElectronsPerSecPerAngstrom());
        list.add(getElectronsPerSec());
        list.add(getErgPerCm2PerSec());
        list.add(getErgPerCm2PerSecPerAngstrom());
        list.add(getErgPerCm2PerSecPerAngstromPerArcsec2());
        list.add(getErgPerCm2PerSecPerArcsec2());
        list.add(getElectronsPerArcsecPerAngstromPerSec());
        Collections.sort(list);
        list.add(0, "unitless");
        return list;
    }

    /** Centimeter (meter * 10^-2).*/
    public static final String CM = "cm";

    /**
     * Checks if the given unit is cm or not.
     * @param unit The unit to test
     * @return true if the unit is cm, false otherwise
     */
    public static boolean isCm(String unit) {
        return CM.equals(unit);
    }
    
    /** Centimeter squared.*/
    public static final String CM2 = "cm^2";
    
    /**
     * Checks if the given unit is cm^2 or not.
     * @param unit The unit to test
     * @return true if the unit is cm^2, false otherwise
     */
    public static boolean isCm2(String unit) {
        return CM2.equals(unit);
    }

    /** Angstrom (meter * 10^-10).*/
    public static final String ANGSTROM = "\u00C5";

    /**
     * Checks if the given unit is \u00C5ngstr\u00F6m or not.
     * @param unit The unit to test
     * @return true if the unit is \u00C5ngstr\u00F6m, false otherwise
     */
    public static boolean isAngstrom(String unit) {
        return ANGSTROM.equals(unit) || 
                "\u00C5ngstr\u00F6m".equalsIgnoreCase(unit) ||
                "angstrom".equalsIgnoreCase(unit);
    }

    /** Arcsecond (1/3600 of a degree or 1/206265 radian).*/
    public static final String ARCSEC = "arcsec";

    /**
     * Checks if the given unit is arcsecond or not.
     * @param unit The unit to test
     * @return true if the unit is arcsecond, false otherwise
     */
    public static boolean isArcsec(String unit) {
        return ARCSEC.equals(unit);
    }

    public static final String PIXEL = "pixel";

    public static boolean isPixel(String unit) {
        return PIXEL.equals(unit);
    }

    public static final String SEC = "s";

    public static boolean isSec(String unit) {
        return SEC.equals(unit) || "sec".equals(unit);
    }
    
    public static final String KELVINS = "K";
    
    public static boolean isKelvins(String unit) {
        return KELVINS.equals(unit) || "kelvins".equals(unit);
    }

    public static final String PHOTONS = "photons";
    
    public static boolean isPhotons(String unit) {
        return PHOTONS.equals(unit);
    }

    public static final String ELECTRONS = "e\u207B";

    public static boolean isElectrons(String unit) {
        return ELECTRONS.equals(unit) || "e".equals(unit) ||
                "electrons".equals(unit);
    }

    public static String getArcsecPerPixel() {
        return ARCSEC + "/" + PIXEL;
    }
    
    public static final String ERG = "erg";

    public static String getErgPerCm2PerSecPerAngstrom() {
        return ERG + "/" + CM2 + "/" +SEC + "/" + ANGSTROM;
    }
    
    public static boolean isErgPerCm2PerSecPerAngstrom(String unit) {
        return getErgPerCm2PerSecPerAngstrom().equals(unit);
    }
    
    public static String getArcsec2() {
        return ARCSEC + "^2";
    }
    
    public static boolean isElectronsPerSecPerAngstrom(String unit) {
        return getElectronsPerSecPerAngstrom().equals(unit);
    }
    
    public static String getElectronsPerSecPerAngstrom() {
        return ELECTRONS + "/" +SEC + "/" + ANGSTROM;
    }
    
    public static boolean isElectronsPerPixelPerSec(String unit) {
        return getElectronsPerPixelPerSec().equals(unit)
                || "e/pixel/s".equals(unit);
    }
    
    public static String getElectronsPerPixelPerSec() {
        return ELECTRONS + "/" +PIXEL + "/" + SEC;
    }
    
    public static boolean isElectronsPerPixel(String unit) {
        return getElectronsPerPixel().equals(unit)
                || "e/pixel".equals(unit);
    }
    
    public static String getElectronsPerPixel() {
        return ELECTRONS + "/" +PIXEL;
    }

    public static String getErgPerCm2PerSecPerArcsec2() {
        return ERG + "/" + CM2 + "/" +SEC + "/" + ARCSEC + "^2";
    }
    
    public static boolean isErgPerCm2PerSecPerArcsec2(String unit) {
        return getErgPerCm2PerSecPerArcsec2().equals(unit);
    }

    public static String getErgPerCm2PerSecPerAngstromPerArcsec2() {
        return ERG + "/" + CM2 + "/" +SEC + "/" + ANGSTROM + "/" + ARCSEC + "^2";
    }
    
    public static boolean isErgPerCm2PerSecPerAngstromPerArcsec2(String unit) {
        return getErgPerCm2PerSecPerAngstromPerArcsec2().equals(unit);
    }
    
    public static String getAngstromPerPixel() {
        return ANGSTROM + "/" + PIXEL;
    }
    
    public static boolean isAngstromPerPixel(String unit) {
        return getAngstromPerPixel().equals(unit);
    }
    
    public static String getElectronsPerArcsecPerAngstromPerSec() {
        return ELECTRONS + "/" + ARCSEC + "/" + ANGSTROM + "/" + SEC;
    }
    
    public static boolean isElectronsPerArcsecPerAngstromPerSec(String unit) {
        return getElectronsPerArcsecPerAngstromPerSec().equals(unit);
    }

    public static String getErgPerCm2PerSec() {
        return ERG + "/" + CM2 + "/" +SEC;
    }
    
    public static boolean isErgPerCm2PerSec(String unit) {
        return getErgPerCm2PerSec().equals(unit);
    }
    
    public static String getElectronsPerSec() {
        return ELECTRONS + "/" + SEC;
    }
    
    public static boolean isElectronsPerSec(String unit) {
        return getElectronsPerSec().equals(unit);
    }
}
