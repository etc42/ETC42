/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.skyarea;

import java.text.MessageFormat;
import java.util.ResourceBundle;
import lombok.EqualsAndHashCode;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.apache.log4j.Logger;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.Cacheable;
import org.cnrs.lam.cesam.util.calculator.CachingPolicy;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.FunctionToDatasetResultConverter;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.calculator.util.functions.ConstantUnivariateFunction;
import org.cnrs.lam.dis.etc.configuration.ConfigFactory;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.javatuples.Pair;
import org.javatuples.Tuple;

/**
 * <p>The {@link Fiber} class is a calculator for computing the area of the sky
 * contributing at the background noise for the central pixel and the total area
 * affected by the source flux. This calculator takes into consideration that
 * any background flux not passing through the fiber is lost.</p>
 * 
 * <p>The configuration of the calculator is a {@link Pair},
 * with contains the following elements:</p>
 * <ol>
 *   <li>{@link Pair}: The pixel scale of the instrument and its unit</li>
 *   <li>{@link Pair}: The fiber diameter and its unit</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Tuple}, as this
 * calculator does not accept any input. It should always be null.</p>
 * 
 * <p>The output of the calculator is a {@link Pair} which contains the following:</p>
 * <ol>
 *   <li>{@link UnivariateRealFunction}: A function for computing the sky area 
 *       contributing on the background noise of the central pixel, expressed
 *       in {@latex.inline $arcsec^2$}</li>
 *   <li>{@link UnivariateRealFunction}: A function for computing the sky area 
 *       contributing on the total background noise, expressed
 *       in {@latex.inline $arcsec^2$}</li>
 * </ol>
 */
@Cacheable(CachingPolicy.INSTANCE)
@EqualsAndHashCode(callSuper=false, of={"pixelScale","fiberDiameter"})
public class Fiber extends AbstractCalculator<
        Pair<Pair<Double,String>, Pair<Double, String>>,
        Tuple, Pair<UnivariateRealFunction, UnivariateRealFunction>> {
    
    private static final Logger logger = Logger.getLogger(Fiber.class);
    private static final ResourceBundle validationErrorsBundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");
    private double pixelScale;
    private double fiberDiameter;
    private UnivariateRealFunction centralPixelFunction;
    private UnivariateRealFunction totalFunction;

    @Override
    protected void validateConfiguration(Pair<Pair<Double, String>, Pair<Double, String>> configuration) throws ConfigurationException {
        // Check that the pixel scale is a positive number
        if (configuration.getValue0().getValue0() <= 0) {
            String message = validationErrorsBundle.getString("PIXEL_SCALE_NOT_POSITIVE");
            message = MessageFormat.format(message, configuration.getValue0().getValue0());
            throw new ConfigurationException(message);
        }
        // Check if the pixel scale is in arcsec/pixel
        if (!Units.getArcsecPerPixel().equals(configuration.getValue0().getValue1())) {
            String message = validationErrorsBundle.getString("PIXEL_SCALE_WRONG_UNIT");
            message = MessageFormat.format(message, Units.getArcsecPerPixel(), configuration.getValue0().getValue1());
            throw new ConfigurationException(message);
        }
        // Check that the fiber diameter is a positive number
        if (configuration.getValue1().getValue0() <= 0) {
            String message = validationErrorsBundle.getString("FIBER_DIAMETER_NOT_POSITIVE");
            message = MessageFormat.format(message, configuration.getValue1().getValue0());
            throw new ConfigurationException(message);
        }
        // Check if the fiber diameter is in arcsec
        if (!Units.isArcsec(configuration.getValue1().getValue1())) {
            String message = validationErrorsBundle.getString("FIBER_DIAMETER_WRONG_UNIT");
            message = MessageFormat.format(message, Units.ARCSEC, configuration.getValue1().getValue1());
            throw new ConfigurationException(message);
        }
    }

    /**
     * Initializes the calculator instance with the given configuration.
     * @param configuration the configuration to use for initialization
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(Pair<Pair<Double, String>, Pair<Double, String>> configuration) throws InitializationException {
        pixelScale = configuration.getValue0().getValue0();
        fiberDiameter = configuration.getValue1().getValue0();
        // We initialize the two functions
        double totalSkyArea = Math.PI * fiberDiameter * fiberDiameter / 4;
        double centralPixelSkyArea;
        if (fiberDiameter <= pixelScale) {
            centralPixelSkyArea = totalSkyArea;
        } else if (fiberDiameter >= Math.sqrt(2) * pixelScale) {
            centralPixelSkyArea = pixelScale * pixelScale;
        } else {
            // Here there is some part of the fiber area outside of the central
            // pixel but also a part of the pixel area ouside the fiber. We calculate
            // the area of the fiber sections which are outside the central pixel.
            double angleInRadians = 2 * Math.acos(pixelScale / fiberDiameter);
            double sectionArea = (angleInRadians - Math.sin(angleInRadians)) * fiberDiameter * fiberDiameter / 8;
            centralPixelSkyArea = totalSkyArea * (1 - 4 * sectionArea / totalSkyArea);
        }
        centralPixelFunction = ConfigFactory.getConfig().getCentralPixelFlag()
                ? new ConstantUnivariateFunction(centralPixelSkyArea) : null;
        totalFunction = new ConstantUnivariateFunction(totalSkyArea);
    }

    /**
     * Returns a {@link Pair} containing the functions to compute sky area of the central
     * pixel and the total sky area (in this order)
     * expressed in {@latex.inline $arcsec^2$}.
     * @param input 
     * @return The central pixel and total sky area functions
     * @throws CalculationException if there is any error during the calculation
     */
    @Override
    protected Pair<UnivariateRealFunction, UnivariateRealFunction>
            performCalculation(Tuple input) throws CalculationException {
        return new Pair<UnivariateRealFunction, UnivariateRealFunction>
                (centralPixelFunction, totalFunction);
    }

    /**
     * <p>For every calculation the calculator adds in the simulation results
     * the following outputs of the calculator:</p>
     * <ul>
     *   <li>The central pixel sky area in intermediate unimportant results
     *       with code CENTRAL_PIXEL_SKY_AREA</li>
     *   <li>The total sky area in intermediate unimportant results
     *       with code SKY_AREA</li>
     * </ul>
     * @param input
     * @param output 
     */
    @Override
    protected void performForEveryCalculation(Tuple input, Pair<UnivariateRealFunction, UnivariateRealFunction> output) {
        if (ConfigFactory.getConfig().getCentralPixelFlag()) {
            CalculationResults.DoubleDatasetResult centralPixelResult = FunctionToDatasetResultConverter.convert
                    (output.getValue0(), "CENTRAL_PIXEL_SKY_AREA", Units.ANGSTROM, Units.getArcsec2());
            ResultsHolder.getResults().addResult(centralPixelResult, CalculationResults.Level.INTERMEDIATE_UNIMPORTANT);
        }
        CalculationResults.DoubleDatasetResult totalResult = FunctionToDatasetResultConverter.convert
                (output.getValue1(), "SKY_AREA", Units.ANGSTROM, Units.getArcsec2());
        ResultsHolder.getResults().addResult(totalResult, CalculationResults.Level.INTERMEDIATE_UNIMPORTANT);
    }

    /**
     * <p>For every calculator instantiation the following parts of its configuration
     * are added in the simulation results:</p>
     * <ul>
     *   <li>The pixel scale in debug results with code PIXEL_SCALE</li>
     *   <li>The fiber diameter in debug results with code FIBER_DIAMETER</li>
     * </ul>
     * @param configuration The configuration of the calculator
     */
    @Override
    protected void performForEveryRetrieval(Pair<Pair<Double, String>, Pair<Double, String>> configuration) {
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("PIXEL_SCALE",
                configuration.getValue0().getValue0(), configuration.getValue0().getValue1()), CalculationResults.Level.DEBUG);
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("FIBER_DIAMETER",
                configuration.getValue1().getValue0(), configuration.getValue1().getValue1()), CalculationResults.Level.DEBUG);
    }
    
}
