/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.dataset;

import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.dis.etc.calculator.oldconvolutionkernel.ConvolutionKernelFactory;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.javatuples.Quartet;
import org.javatuples.Unit;

/**
 * <p>The {@code DatasetFactory} provides calculators for accessing datasets. These
 * calculators get as input keys and they return the values represented by the
 * datasets for the given keys. Note that as this is a generic dataset calculator
 * there is no check of the units of the datasets. This has to be done in a
 * different place. The configuration of the factory contains the following:</p>
 * <ol>
 *   <li>{@link org.cnrs.lam.dis.etc.datamodel.Session}: The session object for
 *       which the calculation is performed</li>
 *   <li>{@link org.cnrs.lam.dis.etc.datamodel.Dataset.Type}: The type of the
 *       dataset this calculator will represent</li>
 *   <li>{@link org.cnrs.lam.dis.etc.datamodel.DatasetInfo}: The information (name
 *       and namespace) of the dataset this calculator will represent</li>
 *   <li>{@link java.lang.String}: The option of a multi-dataset or null for single
 *       dataset</li>
 * </ol>
 *
 * @author Nikolaos Apostolakos
 */
public class DatasetFactory implements Factory<Quartet<Session, Dataset.Type, DatasetInfo, String>, Unit<Double>, Unit<Double>> {
    
    /**
     * Returns a calculator for retrieving values from datasets.
     * @param configuration The active session, the type of the dataset, the info
     * of the dataset and the option
     * @return The calculator instance
     * @throws InitializationException if there is any problem with initializing
     * the calculator
     * @throws ConfigurationException if the configuration is invalid
     */
    @Override
    public Calculator<Unit<Double>, Unit<Double>> getCalculator(Quartet<Session, Dataset.Type, DatasetInfo, String> configuration) throws InitializationException, ConfigurationException {
        Session session = configuration.getValue0();
        Dataset.Type type = configuration.getValue1();
        DatasetInfo datasetInfo = configuration.getValue2();
        String option = configuration.getValue3();

        Calculator<Unit<Double>, Unit<Double>> kernel =
                new ConvolutionKernelFactory().getCalculator(new Unit(session));
        return EtcCalculatorManager.getManager(DatasetCalculator.class).getCalculator(
                new Quartet<Dataset.Type, DatasetInfo, String, Calculator<Unit<Double>, Unit<Double>>>(type, datasetInfo, option, kernel));
    }
    
}
