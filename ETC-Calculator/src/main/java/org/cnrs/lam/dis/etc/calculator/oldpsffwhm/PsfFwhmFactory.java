/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.oldpsffwhm;

import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.dataset.DatasetFactory;
import org.cnrs.lam.dis.etc.calculator.util.ZeroCalculator;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.StringResult;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.cnrs.lam.dis.etc.datamodel.Instrument;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.cnrs.lam.dis.etc.datamodel.Site;
import org.cnrs.lam.dis.etc.datamodel.Site.LocationType;
import org.javatuples.Pair;
import org.javatuples.Quartet;
import org.javatuples.Triplet;
import org.javatuples.Unit;

/**
 * <p>The {@code PsfFwhmFactory} provides calculators for calculating the FWHM
 * of the PSF, expressed in {@latex.inline $arcsec$}. The configuration of the
 * factory is a {@link org.cnrs.lam.dis.etc.datamodel.Session} object containing
 * all the configuration from the user.</p>
 */
public class PsfFwhmFactory implements Factory<Unit<Session>, Unit<Double>, Unit<Double>> {

    @Override
    public Calculator<Unit<Double>, Unit<Double>> getCalculator(Unit<Session> configuration) throws InitializationException, ConfigurationException {
        Session session = configuration.getValue0();
        Instrument instrument = session.getInstrument();
        Instrument.PsfType psfType = instrument.getPsfType();
        Site site = session.getSite();
        Site.LocationType locationType = site.getLocationType();
        boolean seeingLimited = site.getSeeingLimited();
        
        // According the PSF type we return the correct calculator
        switch (psfType) {
            case AUTO:
                // If we are in auto mode there are two cases, diffraction limited or seeing limited.
                if (locationType == LocationType.GROUND && seeingLimited == true) {
                    ResultsHolder.getResults().addResult(
                        new StringResult("PSF_FWHM_METHOD", "Seeing"), Level.DEBUG);
                    // The seeing calculator requires as input the following:
                    // 1: The seeing at zenith
                    // 2: The unit of the seeing
                    // 3: The air mass
                    Double seeing = site.getSeeing();
                    String seeingUnit = site.getSeeingUnit();
                    Double airMass = site.getAirMass();
                    Triplet<Double, String, Double> calculatorConfiguration
                            = new Triplet(seeing, seeingUnit, airMass);
                    return EtcCalculatorManager.getManager(Seeing.class).getCalculator(calculatorConfiguration);
                } else {
                    ResultsHolder.getResults().addResult(
                        new StringResult("PSF_FWHM_METHOD", "Diffraction limited"), Level.DEBUG);
                    // The diffraction limited calculator requires as input the following:
                    // 1: The diameter of the mirror
                    // 2: The unit of the diameter
                    Double diameter = instrument.getDiameter();
                    String diameterUnit = instrument.getDiameterUnit();
                    Pair<Double, String> calculatorConfiguration = new Pair(diameter, diameterUnit);
                    return EtcCalculatorManager.getManager(DiffractionLimited.class)
                            .getCalculator(calculatorConfiguration);
                }
            case PROFILE:
                ResultsHolder.getResults().addResult(
                    new StringResult("PSF_FWHM_METHOD", "PSF FWHM profile"), Level.DEBUG);
                // The Psf profile calculator requires as input the following:
                // 1: The psf profile dataset info
                // 2: A calculator representing the PSF profile dataset
                // 3: A calculator for the atmospheric contribution
                
                // 1. The psf profile info
                DatasetInfo profileInfo = instrument.getFwhm();
                // 2. The dataset calculator for the psf fwhm
                Quartet<Session, Dataset.Type, DatasetInfo, String> profileConfiguration
                        = new Quartet<Session, Dataset.Type, DatasetInfo, String>
                                (session, Dataset.Type.FWHM, profileInfo, null);
                Calculator<Unit<Double>, Unit<Double>> profile
                        = new DatasetFactory().getCalculator(profileConfiguration);
                // 3. If we are seeing limited we use the seeing calculator for
                // atmospheric contribution, otherwise we use a zero calculator
                Calculator<Unit<Double>, Unit<Double>> atmosphere = null;
                if (locationType == LocationType.GROUND && seeingLimited == true) {
                    Double seeing = site.getSeeing();
                    String seeingUnit = site.getSeeingUnit();
                    Double airMass = site.getAirMass();
                    Triplet<Double, String, Double> calculatorConfiguration
                            = new Triplet(seeing, seeingUnit, airMass);
                    atmosphere = EtcCalculatorManager.getManager(Seeing.class)
                            .getCalculator(calculatorConfiguration);
                } else {
                    atmosphere = EtcCalculatorManager.getManager(ZeroCalculator.class).getCalculator(null);
                }
                // Create and return the calculator
                return EtcCalculatorManager.getManager(PsfProfile.class).getCalculator(
                        new Triplet(profileInfo, profile, atmosphere));
        }
        throw new UnsupportedOperationException("PSF FWHM calculation is "
                + "not supported for the given configuration");
    }
    
}
