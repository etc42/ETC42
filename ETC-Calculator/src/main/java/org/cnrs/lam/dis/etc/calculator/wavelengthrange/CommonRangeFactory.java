package org.cnrs.lam.dis.etc.calculator.wavelengthrange;

import java.util.ArrayList;

import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.dis.etc.calculator.DatasetProviderHolder;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.ValidationError;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.cnrs.lam.dis.etc.datamodel.Dataset.Type;
import org.javatuples.Pair;
import org.javatuples.Triplet;

public class CommonRangeFactory {

	private static Session session;

	public static void setSession(Session _session) {
		CommonRangeFactory.session = _session;
	}

	public static Session getSession() {
		return CommonRangeFactory.session;
	}

	/*
	 * // To avoid multiple error on calculation range private static boolean
	 * canDisplayRangeError = true;
	 * 
	 * public static void setCanDisplayRangeError(boolean canDisplayRangeError)
	 * { CommonRangeFactory.canDisplayRangeError = canDisplayRangeError; }
	 * 
	 * public static boolean getCanDisplayRangeError() { return
	 * canDisplayRangeError; }
	 */

	public static Pair<Double, Double> getCalculableRange(Session session, Pair<Double, Double> calculableRange)
			throws CalculationException {
		ArrayList<Triplet<Double, Double, String>> ranges = new ArrayList<Triplet<Double, Double, String>>();

		/* Instrumental limitations */

		// Transmision
		if (session != null) {
			Triplet<Double, Double, String> instrumentTransmissionRange = getRange(Type.TRANSMISSION,
					session.getInstrument().getTransmission(), null, "Instrument > Transmission");
			if (instrumentTransmissionRange != null)
				ranges.add(instrumentTransmissionRange);
			
			// Resolving power
			Triplet<Double, Double, String> spectralResolutionRange = getRange(Type.SPECTRAL_RESOLUTION,
					session.getInstrument().getSpectralResolution(), null, "Instrument > Resolving power");
			if (spectralResolutionRange != null)
				ranges.add(spectralResolutionRange);
			
			/* Sites limitations */
			
			/*
		// Sky brightness
		Triplet<Double, Double, String> skyBrightnessRange = getRange(Type.SKY_EMISSION,
				session.getSite().getSkyEmission(), null,
				"Site > Sky emission");
		if (skyBrightnessRange != null)
			ranges.add(skyBrightnessRange);
		System.out.println(skyBrightnessRange);
			 */
			
			// Sky absorption
			Triplet<Double, Double, String> skyAbsorptionRange = getRange(Type.SKY_ABSORPTION,
					session.getSite().getSkyAbsorption(), null, "Site > Sky absorption");
			if (skyAbsorptionRange != null)
				ranges.add(skyAbsorptionRange);
			
			// Sky extinction
			Triplet<Double, Double, String> skyExtinctionRange = getRange(Type.SKY_EXTINCTION,
					session.getSite().getSkyExtinction(), null, "Site > Sky extinction");
			if (skyExtinctionRange != null)
				ranges.add(skyExtinctionRange);
			/*
			 */
			
			/* Sources limitations */
			
			// Spectral distribution
			Triplet<Double, Double, String> spectralDistributionRange = getRange(Type.SPECTRAL_DIST_TEMPLATE,
					session.getSource().getSpectralDistributionTemplate(), null, "Source > Spectral distribution");
			if (spectralDistributionRange != null)
				ranges.add(spectralDistributionRange);
			
			// Setting range
			ArrayList<String> rangeErrors = new ArrayList<String>();
			
			for (Triplet<Double, Double, String> range : ranges) {
				
				Double newMin = Math.max(calculableRange.getValue0(), range.getValue0());
				Double newMax = Math.min(calculableRange.getValue1(), range.getValue1());
				
				if (newMin > calculableRange.getValue0() || newMax < calculableRange.getValue1()) {
					String errorSource = range.getValue2();
					String errorMessage = errorSource + " : [" + range.getValue0() + ", " + range.getValue1() + "]";
					rangeErrors.add(errorMessage);
				}
				
				calculableRange = new Pair<Double, Double>(newMin, newMax);
				
			}
			
			if (rangeErrors.size() > 0) {
				ResultsHolder.getResults().addResult(
						new CalculationResults.StringResult("TRUNCATED_CALCULATION_RANGE",
								"[" + calculableRange.getValue0() + ", " + calculableRange.getValue1() + "]"),
						CalculationResults.Level.FINAL);
				for (String rangeError : rangeErrors) {
					ResultsHolder.getResults().addResult(
							new CalculationResults.StringResult("TRUNCATED_CALCULATION_RANGE_CAUSE", rangeError),
							CalculationResults.Level.FINAL);
				}
			}
		}

		return calculableRange;
	}

	public static Triplet<Double, Double, String> getRange(Type type, DatasetInfo info, String option,
			String errorMessage) {
		Triplet<Double, Double, String> lowHigh = null;

		if (info != null) {
			org.cnrs.lam.dis.etc.datamodel.Dataset dataSet = null;

			if (option != null) {
				dataSet = DatasetProviderHolder.getDatasetProvider().getDataset(type, info, option);
			} else {
				dataSet = DatasetProviderHolder.getDatasetProvider().getDataset(type, info);
			}
			if (dataSet != null) {
				Object[] keys = dataSet.getData().keySet().toArray();
				lowHigh = new Triplet<Double, Double, String>((Double) keys[0], (Double) keys[keys.length - 1],
						errorMessage);
			}
		}

		return lowHigh;
	}
}
