/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.spectralresolution;

import java.text.MessageFormat;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TreeMap;
import lombok.EqualsAndHashCode;
import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.apache.log4j.Logger;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.Cacheable;
import org.cnrs.lam.cesam.util.calculator.CachingPolicy;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.FunctionToDatasetResultConverter;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.calculator.util.functions.IntegrableUnivariateFunction;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.DoubleDatasetResult;
import org.javatuples.Pair;
import org.javatuples.Tuple;
import org.javatuples.Unit;

/**
 *<p>The {@link FixedDeltaLambdaPerPixel} class is a calculator for computing the 
 * spectral resolution per pixel for a fixed {@latex.inline $\\Delta\\lambda$}.
 * The configuration of the calculator
 * is a {@link org.javatuples.Pair} which contains the following elements:</p>
 * <ol>
 *   <li>{@link Double}: The fixed {@latex.inline $\\Delta\\lambda$}</li>
 *   <li>{@link String}: The {@latex.inline $\\Delta\\lambda$} unit</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Tuple}, as this
 * calculator does not accept any input. It should always be null.</p>
 * 
 * <p>The output of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link UnivariateRealFunction} representing the spectral resolution.</p>
 */
@Cacheable(CachingPolicy.INSTANCE)
@EqualsAndHashCode(callSuper=false)
public class FixedDeltaLambdaPerPixel extends AbstractCalculator<Pair<Double, String>, Tuple, Unit<UnivariateRealFunction>> {
    
    private static final Logger logger = Logger.getLogger(FixedDeltaLambdaPerPixel.class);
    private static final ResourceBundle validationErrorsBundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");
    private double fixedDeltaLambda;

    /**
     * Validates the given configuration. It requires that the first value of the 
     * tuple (the fixed delta lambda) is a positive number and that the second
     * value (its unit) is {@latex,inline $\\AA/pixel$}.
     * @param configuration The configuration to validate
     * @throws ConfigurationException if the configuration is invalid
     */
    @Override
    protected void validateConfiguration(Pair<Double, String> configuration) throws ConfigurationException {
        // Check that the fixed delta lambda is a positive number
        if (configuration.getValue0() <= 0) {
            String message = validationErrorsBundle.getString("FIXED_DELTA_LAMBDA_NOT_POSITIVE");
            message = MessageFormat.format(message, configuration.getValue0());
            throw new ConfigurationException(message);
        }
        // Check if the fixed delta lambda is in Angstrom
        if (!Units.isAngstromPerPixel(configuration.getValue1())) {
            String message = validationErrorsBundle.getString("FIXED_DELTA_LAMBDA_WRONG_UNIT");
            message = MessageFormat.format(message, Units.getAngstromPerPixel(), configuration.getValue1());
            throw new ConfigurationException(message);
        }
    }

    /**
     * Initializes the calculator instance with the given configuration.
     * @param configuration the configuration to use for initialization
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(Pair<Double, String> configuration) throws InitializationException {
        fixedDeltaLambda = configuration.getValue0();
    }

    @Override
    protected Unit<UnivariateRealFunction> performCalculation(Tuple input) throws CalculationException {
        return new Unit<UnivariateRealFunction>(new FixedDeltaLambdaFunction(fixedDeltaLambda));
    }
    
    private static class FixedDeltaLambdaFunction implements IntegrableUnivariateFunction {
        
        private double fixedDeltaLambda;

        public FixedDeltaLambdaFunction(double fixedDeltaLambda) {
            this.fixedDeltaLambda = fixedDeltaLambda;
        }

        @Override
        public double integral(double x1, double x2) throws FunctionEvaluationException {
            return (x2 * x2 - x1 * x1) / (2 * fixedDeltaLambda);
        }

        @Override
        public double value(double x) throws FunctionEvaluationException {
            return x / fixedDeltaLambda;
        }
    }

    /**
     * <p>For every calculation the calculator adds in the simulation results
     * the following outputs of the calculator:</p>
     * <ul>
     *   <li>The spectral resolution in intermediate unimportant results with code
     *       SPECTRAL_RESOLUTION</li>
     * </ul>
     * @param input
     * @param output 
     */
    @Override
    protected void performForEveryCalculation(Tuple input, Unit<UnivariateRealFunction> output) {
        // If the result already exists we do not add it to save time
        if (ResultsHolder.getResults().getResultByName("SPECTRAL_RESOLUTION") != null) {
            return;
        }
        UnivariateRealFunction function = output.getValue0();
        DoubleDatasetResult result = FunctionToDatasetResultConverter.convert(function, 
                "SPECTRAL_RESOLUTION", Units.ANGSTROM, Units.ANGSTROM);
        ResultsHolder.getResults().addResult(result, CalculationResults.Level.INTERMEDIATE_UNIMPORTANT);
    }

    /**
     * <p>For every calculator instantiation the following parts of its configuration
     * are added in the simulation results:</p>
     * <ul>
     *   <li>The fixed delta lambda per pixel with code FIXED_DELTA_LAMBDA_PER_PIXEL</li>
     * </ul>
     * @param configuration 
     */
    @Override
    protected void performForEveryRetrieval(Pair<Double, String> configuration) {
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("FIXED_DELTA_LAMBDA_PER_PIXEL",
                configuration.getValue0(), configuration.getValue1()), CalculationResults.Level.DEBUG);
    }
    
}
