/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.util.functions;

import lombok.EqualsAndHashCode;
import org.apache.commons.math.FunctionEvaluationException;

/**
 *<p>The {@link PolarGaussian} class represents a scaled normal distribution with
 * peak value for X=0, multiplied by x.</p>
 * {@latex.inline $
 * f{(x)}=x*\\displaystyle\\frac{scale}{\\sigma\\sqrt{2\\pi}}
 * e^{-\\frac{1}{2} \\left( \\frac{x}{\\sigma}} \\right) ^2
 * $}
 * <p> where {@latex.inline $\\sigma$} is the standard deviation and
 * {@latex.inline $scale$} is the factor to scale the Gaussian with.</p>
 */
@EqualsAndHashCode(callSuper=false, of={"standardDeviation", "scale"})
public class PolarGaussian implements IntegrableUnivariateFunction {
    
    private final double standardDeviation; // sigma
    private final double scale; // The scale factor
    private final double variance; // sigma^2
    private static final double sqrtTwo = Math.sqrt(2.);
    private static final double sqrtTwoPi = sqrtTwo * Math.sqrt(Math.PI);
    private final double multiplier; // scale/(sigma*sqrt(2*pi))
    
    /**
     * Creates a new {@link PolarGaussian} which has a peak at X = 0, the given
     * standard deviation {@latex.inline $\\sigma$} and scale factor = 1.
     * @param standardDeviation The standard deviation {@latex.inline $\\sigma$}
     */
    public PolarGaussian(double standardDeviation) {
        this(standardDeviation, 1.);
    }

    /**
     * Creates a new {@link PolarGaussian} which has a peak at X = 0 and the given
     * standard deviation {@latex.inline $\\sigma$} and scale factor.
     * @param standardDeviation The standard deviation {@latex.inline $\\sigma$}
     * @param scale The scale factor to multiply the Gaussian with
     */
    public PolarGaussian(double standardDeviation, double scale) {
        this.standardDeviation = standardDeviation;
        this.scale = scale;
        this.variance = standardDeviation * standardDeviation;
        this.multiplier = scale / (standardDeviation * sqrtTwoPi);
    }
    
    /**
     * Returns the standard deviation {@latex.inline $\\sigma$} of the gaussian.
     * @return The standard deviation
     */
    public double getStandardDeviation() {
        return standardDeviation;
    }
    
    /**
     * Returns the scale factor with which the Gaussian is multiplied with.
     * @return 
     */
    public double getScale() {
        return scale;
    }

    /**
     * Returns the value of the polar Gaussian for the distance x from the location of
     * the peak.
     * @param x The distance from the peak
     * @return The value of the Gaussian
     * @throws FunctionEvaluationException 
     */
    @Override
    public double value(double x) throws FunctionEvaluationException {
        if (x < 0) {
            throw new IllegalArgumentException("Polar function cannot be calculated for negative values");
        }
        double value = x * multiplier * Math.exp(-0.5 * x * x / variance);
        return value;
    }

    @Override
    public double integral(double x1, double x2) throws FunctionEvaluationException {
        if (x1 > x2) {
            throw new IllegalArgumentException("Illegal integration range [" + x1 + "," + x2 + "]");
        }
        if (x1 < 0) {
            throw new IllegalArgumentException("Polar function cannot be calculated for negative values");
        }
        if (x1 == x2) {
            return 0;
        }
        return indefiniteIntegral(x2) - indefiniteIntegral(x1);
    }

    private double indefiniteIntegral(double x) {
        if (x < 0) {
            throw new IllegalArgumentException("Cannot calculate De Vaucouleurs polar "
                    + "indefinite integral for negative X");
        }
        return -scale * standardDeviation * Math.exp(-0.5 * x * x / variance) / Math.sqrt(2 * Math.PI);
    }

}
