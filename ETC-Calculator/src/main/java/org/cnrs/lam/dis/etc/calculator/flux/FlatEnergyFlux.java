/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.flux;

import java.util.Map;
import java.util.ResourceBundle;
import java.util.TreeMap;
import lombok.EqualsAndHashCode;
import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.apache.log4j.Logger;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Constants;
import org.cnrs.lam.dis.etc.calculator.util.FunctionToDatasetResultConverter;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.calculator.util.functions.IntegrableUnivariateFunction;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.javatuples.Tuple;
import org.javatuples.Unit;

/**
 * <p>The {@link FlatEnergyFlux} class is a calculator for computing functions
 * representing flat energy flux (constant over the frequency). The flux
 * value is calculated as:</p>
 * {@latex.inline $
 * F_{(\\lambda)}=10^{-\\frac{m_{AB}+48.6}{2.5}}*\\frac{c}{\\lambda^2}
 * $}
 * <p>where</p>
 * <ul>
 *   <li>{@latex.inline $\\lambda_x$}: is the wavelength expressed 
 *       in {@latex.inline \\AA}</li>
 *   <li>{@latex.inline $m_{AB}$}: is the AB magnitude</li>
 *   <li>{@latex.inline $c$}: is the speed of light expressed in {@latex.inline \\AA$/sec$}</li>
 * </ul>
 * 
 * <p>The configuration of the calculator is a {@link Unit} which contains the
 * AB magnitude.</p>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Tuple}, as this
 * calculator does not accept any input. It should always be null.</p>
 * 
 * <p>The output of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link UnivariateRealFunction} representing the flux of the source.</p>
 */
// Caching explenation
// -------------------
// This calculator is set to cache the instances because during instantiation
// it creates the flux function.
@Cacheable(CachingPolicy.INSTANCE)
@EqualsAndHashCode(callSuper=false, exclude={"fluxFunction"})
public class FlatEnergyFlux extends AbstractCalculator<
        Unit<Double>, Tuple, Unit<UnivariateRealFunction>> {
    
    private static final Logger logger = Logger.getLogger(FlatEnergyFlux.class);
    private static final ResourceBundle validationErrorsBundle =
            ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");
    private double magnitude;
    private FlatEnergyFluxFunction fluxFunction;

    /**
     * Validates the configuration. Currently it checks nothing.
     * @param configuration
     * @throws ConfigurationException 
     */
    @Override
    protected void validateConfiguration(Unit<Double> configuration) throws ConfigurationException {
        // There is nothing to validate.
    }

    /**
     * Initializes the calculator instance with the given configuration.
     * @param configuration the configuration to use for initialization
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(Unit<Double> configuration) throws InitializationException {
        magnitude = configuration.getValue0();
        fluxFunction = new FlatEnergyFluxFunction(magnitude);
    }

    /**
     * Returns a {@link UnivariateRealFunction} which can be used to calculate
     * the source flux.
     * @param input The calculator has no input, so it can be always null
     * @return A {@link UnivariateRealFunction} which can be used to calculate
     * the source flux
     * @throws CalculationException if there is any error during the calculation
     */
    @Override
    protected Unit<UnivariateRealFunction> performCalculation(Tuple input) throws CalculationException {
    	System.out.println(fluxFunction);
        return new Unit<UnivariateRealFunction>(fluxFunction);
    }

    /**
     * <p>For every calculation the calculator adds in the simulation results
     * the following outputs of the calculator:</p>
     * <ul>
     *   <li>The source flux in intermediate unimportant results with code
     *       SIGNAL_FLUX</li>
     * </ul>
     * @param input
     * @param output 
     */
    @Override
    protected void performForEveryCalculation(Tuple input, Unit<UnivariateRealFunction> output) {
        // If the result already exists we do not add it to save time
        if (ResultsHolder.getResults().getResultByName("SIGNAL_FLUX") != null) {
            return;
        }
        UnivariateRealFunction function = output.getValue0();
        CalculationResults.DoubleDatasetResult result = FunctionToDatasetResultConverter.convert(function,
                "SIGNAL_FLUX", Units.ANGSTROM, Units.getErgPerCm2PerSecPerAngstrom());
        ResultsHolder.getResults().addResult(result, CalculationResults.Level.INTERMEDIATE_UNIMPORTANT);
    }

    /**
     * <p>For every calculator instantiation the following parts of its configuration
     * are added in the simulation results:</p>
     * <ul>
     *   <li>The AB magnitude of the source in debug results with code
     *       SOURCE_AB_MAGNITUDE</li>
     * </ul>
     * @param configuration 
     */
    @Override
    protected void performForEveryRetrieval(Unit<Double> configuration) {
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("SOURCE_AB_MAGNITUDE"
                , configuration.getValue0(), null), CalculationResults.Level.DEBUG);
    }
    
    /**
     * Represents a flat energy flux (constant over wavelength) function.
     */
    @EqualsAndHashCode(callSuper=false)
    private static class FlatEnergyFluxFunction implements IntegrableUnivariateFunction {
        
        private double multiplier;

        public FlatEnergyFluxFunction(double magnitude) {
            multiplier = Math.pow(10., -(magnitude + 48.6) / 2.5) * Constants.SPEED_OF_LIGHT;
        }

        @Override
        public double value(double x) throws FunctionEvaluationException {
            return multiplier / (x * x);
        }

        @Override
        public double integral(double x1, double x2) throws FunctionEvaluationException {
            return indefiniteIntegral(x2) - indefiniteIntegral(x1);
        }
        
        private double indefiniteIntegral(double x) {
            return -multiplier / x;
        }
        
    }
    
}
