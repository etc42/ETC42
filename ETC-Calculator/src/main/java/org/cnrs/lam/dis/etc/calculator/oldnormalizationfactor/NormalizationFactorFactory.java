/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.oldnormalizationfactor;

import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.oldpsffwhm.PsfFwhmFactory;
import org.cnrs.lam.dis.etc.calculator.oldsurfacebrightnessprofile.SurfaceBrightnessProfileFactory;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.StringResult;
import org.cnrs.lam.dis.etc.datamodel.Instrument;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.cnrs.lam.dis.etc.datamodel.Source;
import org.javatuples.Quintet;
import org.javatuples.Triplet;
import org.javatuples.Unit;

/**
 * <p>The {@code NormalizationFactorFactory} provides calculators for calculating the
 * normalization factor. The retrieved calculators get as input the wavelength for
 * which the calculation is done (expressed in {@latex.inline \\AA}) and returns
 * the normalization factor. The configuration
 * of the factory is a {@link org.cnrs.lam.dis.etc.datamodel.Session} object 
 * containing all the configuration from the user.</p>
 */
public class NormalizationFactorFactory implements Factory<Unit<Session>, Unit<Double>, Unit<Double>> {

    @Override
    public Calculator<Unit<Double>, Unit<Double>> getCalculator(Unit<Session> configuration) 
            throws InitializationException, ConfigurationException {
        Session session = configuration.getValue0();
        Instrument instrument = session.getInstrument();
        Instrument.InstrumentType instrumentType = instrument.getInstrumentType();
        Instrument.SpectrographType spectographType = instrument.getSpectrographType();
        Source source = session.getSource();
        Source.SpatialDistributionType spatialType = source.getSpatialDistributionType();
        
        double slitWidth = instrument.getSlitWidth();
        String slitWidthUnit = instrument.getSlitWidthUnit();
        double fiberDiameter = instrument.getFiberDiameter();
        String fiberDiameterUnit = instrument.getFiberDiameterUnit();
        
        if (spatialType == spatialType.POINT_SOURCE) {
            Calculator<Unit<Double>, Unit<Double>> fwhmCalculator
                    = new PsfFwhmFactory().getCalculator(configuration);
            if (instrumentType == instrumentType.SPECTROGRAPH && spectographType == spectographType.SLIT) {
                ResultsHolder.getResults().addResult(
                        new StringResult("NORMALIZATION_FACTOR_METHOD", "Point source / Slit"), Level.DEBUG);
                Triplet<Double, String, Calculator<Unit<Double>, Unit<Double>>> calculatorConfiguration = 
                        new Triplet(slitWidth, slitWidthUnit, fwhmCalculator);
                return EtcCalculatorManager.getManager(PointSourceSlit.class).getCalculator(calculatorConfiguration);
            } else if (instrumentType == instrumentType.SPECTROGRAPH && spectographType == spectographType.FIBER) {
                ResultsHolder.getResults().addResult(
                        new StringResult("NORMALIZATION_FACTOR_METHOD", "Point source / Fiber"), Level.DEBUG);
                Triplet<Double, String, Calculator<Unit<Double>, Unit<Double>>> calculatorConfiguration = 
                        new Triplet(fiberDiameter, fiberDiameterUnit, fwhmCalculator);
                return EtcCalculatorManager.getManager(PointSourceFiber.class).getCalculator(calculatorConfiguration);
            } else {
                ResultsHolder.getResults().addResult(
                        new StringResult("NORMALIZATION_FACTOR_METHOD", "Point source"), Level.DEBUG);
                return EtcCalculatorManager.getManager(PointSource.class).getCalculator(null);
            }
        }
        if (spatialType == spatialType.EXTENDED_SOURCE) {
            Calculator<Unit<Double>, Unit<Double>> sbpCalculator =
                    new SurfaceBrightnessProfileFactory().getCalculator(configuration);
            double sourceRadius = source.getExtendedSourceRadius();
            String sourceRadiusUnit = source.getExtendedSourceRadiusUnit();
            if (instrumentType == instrumentType.SPECTROGRAPH && spectographType == spectographType.SLIT) {
                ResultsHolder.getResults().addResult(
                        new StringResult("NORMALIZATION_FACTOR_METHOD", "Extended source / Slit"), Level.DEBUG);
                Quintet<Double, String, Double, String, Calculator<Unit<Double>, Unit<Double>>> calculatorConfiguration = 
                        new Quintet(sourceRadius, sourceRadiusUnit, slitWidth, slitWidthUnit, sbpCalculator);
                return EtcCalculatorManager.getManager(ExtendedSourceSlit.class).getCalculator(calculatorConfiguration);
            } else if (instrumentType == instrumentType.SPECTROGRAPH && spectographType == spectographType.FIBER) {
                ResultsHolder.getResults().addResult(
                        new StringResult("NORMALIZATION_FACTOR_METHOD", "Extended source / Fiber"), Level.DEBUG);
                Triplet<Double, String, Calculator<Unit<Double>, Unit<Double>>> calculatorConfiguration = 
                        new Triplet(fiberDiameter, fiberDiameterUnit, sbpCalculator);
                return EtcCalculatorManager.getManager(ExtendedSourceFiber.class).getCalculator(calculatorConfiguration);
            } else {
                ResultsHolder.getResults().addResult(
                        new StringResult("NORMALIZATION_FACTOR_METHOD", "Extended source"), Level.DEBUG);
                Triplet<Double, String, Calculator<Unit<Double>, Unit<Double>>> calculatorConfiguration = 
                        new Triplet(sourceRadius, sourceRadiusUnit, sbpCalculator);
                return EtcCalculatorManager.getManager(ExtendedSource.class).getCalculator(calculatorConfiguration);
            }
        }
        // If we are here means that we cannot create a calculator for this configuration
        throw new UnsupportedOperationException("Normalizaton factor calculation is "
                + "not supported for the given configuration");
    }
    
}
