/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.surfacebrightnessprofile;

import java.text.MessageFormat;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TreeMap;
import lombok.EqualsAndHashCode;
import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.analysis.BivariateRealFunction;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.apache.log4j.Logger;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.calculator.util.functions.CircularlySymmetricBivariateFunction;
import org.cnrs.lam.dis.etc.calculator.util.functions.IntegrableBivariateFunction;
import org.cnrs.lam.dis.etc.calculator.util.functions.IntegrableUnivariateFunction;
import org.cnrs.lam.dis.etc.calculator.util.integration.IntegrationTool;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.cnrs.lam.dis.etc.datamodel.Source;
import org.javatuples.Pair;
import org.javatuples.Tuple;
import org.javatuples.Unit;

/**
 * <p>The {@link Uniform} class is a calculator for computing functions representing
 * the surface brightness profile, following the uniform distribution. The equation
 * used is:</p>
 * {@latex.inline $
 * I_{(r)} = 
 * \\left \\|
 * \\begin{array}{ll}
 * 1 & , r \\le r_{obj} \\\\
 * 0 & , r > r_{obj}
 * \\end{array}
 * \\right .
 * $}
 * <p>where</p>
 * <ul>
 *   <li>{@latex.inline I_{(r)}}: is the surface brightness profile</li>
 *   <li>{@latex.inline $r_{obj}$}: is the effective radius of the extended source in
 *       {@latex.inline $arcsec$}</li>
 *   <li>{@latex.inline $r$}: is the distance from the center of the source, for
 *       which the value is calculated, expressed in {@latex.inline $arcsec$}</li>
 * </ul>
 * 
 * <p>Note that the above function has peak value equal with 1 at (0,0). This
 * corresponds to a given magnitude at peak value. If the user defines that the
 * given magnitude is for the total flux, then the returned function is normalized
 * so it has integral 1 for the area inside the effective radius.</p>
 * 
 * <p>The configuration of the calculator is a {@link org.javatuples.Pair},
 * with contains the following elements:</p>
 * <ol>
 *   <li>{@link Pair}: The effective radius of the source and its unit</li>
 *   <li>{@link Source.ExtendedMagnitudeType}: Shows if the profile should be
 *       normalized (total case) or not (peak value case</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Tuple}, as this
 * calculator does not accept any input. It should always be null.</p>
 * 
 * <p>The output of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link BivariateRealFunction} representing the surface brightness profile.</p>
 */
@EqualsAndHashCode(callSuper=false)
public class Uniform extends AbstractCalculator<
        Pair<Pair<Double, String>, Source.ExtendedMagnitudeType>,
        Tuple, Unit<BivariateRealFunction>> {
    
    private static final Logger logger = Logger.getLogger(Uniform.class);
    private static final ResourceBundle validationErrorsBundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");
    private double sourceRadius;
    private Source.ExtendedMagnitudeType extendedMagnitudeType;

    /**
     * Validates the given configuration. It requires that the first value of the
     * tuple (the source radius) is a positive number and that the second
     * value (the unit of the source radius) is {@latex.inline $arcsec$}.
     * @param configuration The configuration to validate
     * @throws ConfigurationException If the configuration is invalid
     */
    @Override
    protected void validateConfiguration(Pair<Pair<Double, String>, Source.ExtendedMagnitudeType> configuration) throws ConfigurationException {
        // Check that the source radius is a positive number
        if (configuration.getValue0().getValue0() <= 0) {
            String message = validationErrorsBundle.getString("EXTENDED_SOURCE_RADIUS_NOT_POSITIVE");
            message = MessageFormat.format(message, configuration.getValue0().getValue0());
            throw new ConfigurationException(message);
        }
        // Check if the source radius is in arcsec
        if (!Units.isArcsec(configuration.getValue0().getValue1())) {
            String message = validationErrorsBundle.getString("EXTENDED_SOURCE_RADIUS_WRONG_UNIT");
            message = MessageFormat.format(message, Units.ARCSEC, configuration.getValue0().getValue1());
            throw new ConfigurationException(message);
        }
    }

    /**
     * Initializes the calculator instance with the given configuration. The first
     * value of the configuration is the source radius and the second is
     * the unit of the source radius.
     * @param configuration the configuration to use for initialization
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(Pair<Pair<Double, String>, Source.ExtendedMagnitudeType> configuration) throws InitializationException {
        sourceRadius = configuration.getValue0().getValue0();
        extendedMagnitudeType = configuration.getValue1();
    }

    /**
     * Returns a {@link org.javatuples.Unit} containing the surface brightness 
     * profile function.
     * @param input The distance from the source center expressed in {@latex.inline $arcsec$}
     * @return The surface brightness profile function
     * @throws CalculationException if there is any error during the calculation
     */
    @Override
    protected Unit<BivariateRealFunction> performCalculation(Tuple input) throws CalculationException {
        BivariateUniformFunction profileFunction = new BivariateUniformFunction(sourceRadius, 1);
        if (extendedMagnitudeType == Source.ExtendedMagnitudeType.TOTAL) {
            double multiplier = 0;
            try {
                multiplier = 1 / IntegrationTool.bivariateIntegral(profileFunction, sourceRadius);
            } catch (FunctionEvaluationException ex) {
                logger.error("Failed to calculate the surface brightness profile normalization factor", ex);
                throw new CalculationException("Failed to calculate the surface brightness profile normalization factor");
            }
            profileFunction = new BivariateUniformFunction(sourceRadius, multiplier);
        }
        return new Unit<BivariateRealFunction>(profileFunction);
    }

    /**
     * <p>For every calculation the calculator adds in the simulation results
     * the following outputs of the calculator:</p>
     * <ul>
     *   <li>The projection of the surface brightness profile on the plane y=0
     *       in the intermediate unimportant results with code
     *       SURFACE_BRIGHTNESS_PROFILE_PROJECTION</li>
     * </ul>
     * @param input
     * @param output 
     */
    @Override
    protected void performForEveryCalculation(Tuple input, Unit<BivariateRealFunction> output) {
        // If the result already exists we do not add it to save time
        if (ResultsHolder.getResults().getResultByName("SURFACE_BRIGHTNESS_PROFILE_PROJECTION") != null) {
            return;
        }
        // We know we have a bivariate uniform function so the casting is safe
        BivariateUniformFunction function = (BivariateUniformFunction) output.getValue0();
        // We add the projection on the y = 0 plane in the results. We calculate
        // the valuse in the range [-sourceRadius, sourceRadius]
        UnivariateRealFunction projection = function.projectionFunction();
        Map<Double, Double> projectionData = new TreeMap<Double, Double>();
        // We will create 110 points to represent the plot
        double step = sourceRadius / 50;
        double low = -1.1 * sourceRadius;
        double high = 1.1 * sourceRadius;
        for (double x = low; x <= high; x += step) {
            double value = 0;
            try {
                value = projection.value(x);
            } catch (FunctionEvaluationException ex) {
                logger.warn("Failed to calculate the value of the surface brightness profile projection", ex);
            }
            projectionData.put(x, value);
        }
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleDatasetResult(
                "SURFACE_BRIGHTNESS_PROFILE_PROJECTION", projectionData, Units.ARCSEC, null),
                CalculationResults.Level.INTERMEDIATE_UNIMPORTANT);
    }

    /**
     * <p>For every calculator instantiation the following parts of its configuration
     * are added in the simulation results:</p>
     * <ul>
     *   <li>The extended source effective radius in debug results with code SOURCE_RADIUS</li>
     *   <li>The type the given flux or magnitude in debug results with code EXTENDED_MAG_FLUX_GIVEN_FOR</li>
     * </ul>
     * @param configuration The configuration of the calculator
     */
    @Override
    protected void performForEveryRetrieval(Pair<Pair<Double, String>, Source.ExtendedMagnitudeType> configuration) {
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("SOURCE_RADIUS",
                configuration.getValue0().getValue0(), configuration.getValue0().getValue1()), CalculationResults.Level.DEBUG);
        ResultsHolder.getResults().addResult(new CalculationResults.StringResult("EXTENDED_MAG_FLUX_GIVEN_FOR",
                configuration.getValue1().toString()), CalculationResults.Level.DEBUG);
    }
    
    /**
     * This class represents a circularly symmetric bivariate uniform function.
     */
    @EqualsAndHashCode(callSuper=false, of={"limit","multiplier"})
    private class BivariateUniformFunction implements CircularlySymmetricBivariateFunction, IntegrableBivariateFunction {
        
        private final double limit;
        private final double multiplier;

        public BivariateUniformFunction(double limit, double multiplier) {
            this.limit = limit;
            this.multiplier = multiplier;
        }

        @Override
        public double value(double r) throws FunctionEvaluationException {
            return (Math.abs(r) <= limit) ? multiplier : 0;
        }

        @Override
        public UnivariateRealFunction projectionFunction() {
            return new UniformFunction(limit, multiplier);
        }

        @Override
        public double value(double x, double y) throws FunctionEvaluationException {
            return value(Math.sqrt(x * x + y * y));
        }

        @Override
        public double integral(double x1, double x2, double y1, double y2) throws FunctionEvaluationException {
            // Check the limits
            if (x1 > x2 || y1 > y2) {
                throw new IllegalArgumentException("Wrong double integral limits [("+x1+","+x2+"),("+y1+","+y2+")]");
            }
            // Check if the limits cover only zeroes
            if (x1 >= limit || x2 <= -limit || y1 >= limit || y2 <= -limit) {
                return 0;
            }
            // Shrink the integration limits to remove zero values
            x1 = (x1 < -limit) ? -limit : x1;
            x2 = (x2 > limit) ? limit : x2;
            y1 = (y1 < -limit) ? -limit : y1;
            y2 = (y2 > limit) ? limit : y2;
            // If any of the limits are equal the integral is zero
            if (x1 == x2 || y1 == y2) {
                return 0;
            }
            // Check if the area covers all the non zero values
            if (x1 == -limit && x2 == limit && y1 == -limit && y2 == limit) {
                return multiplier * Math.PI * limit * limit;
            }
            // Calculate the rectangle area
            double rectangleArea = (x2 - x1) * (y2 - y1);
            // Calculate the distances of the rectangle corners
            double r1_1 = Math.sqrt(x1 * x1 + y1 * y1);
            double r1_2 = Math.sqrt(x1 * x1 + y2 * y2);
            double r2_1 = Math.sqrt(x2 * x2 + y1 * y1);
            double r2_2 = Math.sqrt(x2 * x2 + y2 * y2);
            // Check if all the rectangle is inside the limit
            if (r1_1 <= limit && r1_2 <= limit && r2_1 <= limit && r2_2 <= limit) {
                return multiplier * rectangleArea;
            }
            
            // We check for all four corners outside the limit in the same quarter
            // quarter x>0,y>0
            if (r1_1 >= limit && r1_2 >= limit && r2_1 >= limit && r2_2 >=limit &&
                    x1 > 0 && x2 > 0 && y1 > 0 && y2 > 0) {
                return 0;
            }
            // quarter x>0,y<0
            if (r1_1 >= limit && r1_2 >= limit && r2_1 >= limit && r2_2 >=limit &&
                    x1 > 0 && x2 > 0 && y1 < 0 && y2 < 0) {
                return 0;
            }
            // quarter x<0,y>0
            if (r1_1 >= limit && r1_2 >= limit && r2_1 >= limit && r2_2 >=limit &&
                    x1 < 0 && x2 < 0 && y1 > 0 && y2 > 0) {
                return 0;
            }
            // quarter x<0,y<0
            if (r1_1 >= limit && r1_2 >= limit && r2_1 >= limit && r2_2 >=limit &&
                    x1 < 0 && x2 < 0 && y1 < 0 && y2 < 0) {
                return 0;
            }
            
            // Here we know that we have an overlapping. We try to calculate the
            // area of the rectangle that is outside the circle with radius limit
            // to suctract it for the total rectangle area
            double zeroArea = 0;
            
            // We check for single corners outside the limit in any quarter
            // quarter x>0,y>0
            if ((r1_1 <= limit || x1 < 0 || y1 < 0) && // checks the (x1,y1)
                    (r1_2 <= limit || x1 < 0 || y2 < 0) && // checks the (x1,y2)
                    (r2_1 <= limit || x2 < 0 || y1 < 0) && // checks the (x2,y1)
                    (r2_2 > limit && x2 >= 0 && y2 >= 0) // checks the (x2,y2)
                    ) {
                zeroArea += zeroAreaForOnePoint(x1, x2, y1, y2);
            }
            // quarter x>0,y<0
            if ((r1_1 <= limit || x1 < 0 || y1 > 0) && // checks the (x1,y1)
                    (r1_2 <= limit || x1 < 0 || y2 > 0) && // checks the (x1,y2)
                    (r2_1 > limit && x2 >= 0 && y1 <= 0) && // checks the (x2,y1)
                    (r2_2 <= limit || x2 < 0 || y2 > 0) // checks the (x2,y2)
                    ) {
                // We convert the limits to make the calculation in the first quarter
                zeroArea += zeroAreaForOnePoint(x1, x2, -y2, -y1);
            }
            // quater x<0,y>0
            if ((r1_1 <= limit || x1 > 0 || y1 < 0) && // checks the (x1,y1)
                    (r1_2 > limit && x1 <= 0 && y2 >= 0) && // checks the (x1,y2)
                    (r2_1 <= limit || x2 > 0 || y1 < 0) && // checks the (x2,y1)
                    (r2_2 <= limit || x2 > 0 || y2 < 0) // checks the (x2,y2)
                    ) {
                // We convert the limits to make the calculation in the first quarter
                zeroArea += zeroAreaForOnePoint(-x2, -x1, y1, y2);
            }
            // quarter x<0,y<0
            if ((r1_1 > limit && x1 <= 0 && y1 <= 0) && // checks the (x1,y1)
                    (r1_2 <= limit || x1 > 0 || y2 > 0) && // checks the (x1,y2)
                    (r2_1 <= limit || x2 > 0 || y1 > 0) && // checks the (x2,y1)
                    (r2_2 <= limit || x2 > 0 || y2 > 0) // checks the (x2,y2)
                    ) {
                // We convert the limits to make the calculation in the first quarter
                zeroArea += zeroAreaForOnePoint(-x2, -x1, -y2, -y1);
            }
            
            // We check for three corners outside the limit in any quarter. Note
            // that the fourth corner has to be also in the same quarter
            // quarter x>0,y>0
            if (x1 > 0 && x2 > 0 && y1 > 0 && y2 > 0 &&
                    r1_1 < limit && r1_2 > limit && r2_1 > limit && r2_2 > limit) {
                zeroArea += zeroAreaForThreePoints(x1, x2, y1, y2);
            }
            // quarter x>0,y<0
            if (x1 > 0 && x2 > 0 && y1 < 0 && y2 < 0 &&
                    r1_1 > limit && r1_2 < limit && r2_1 > limit && r2_2 > limit) {
                // We convert the limits to make the calculation in the first quarter
                zeroArea += zeroAreaForThreePoints(x1, x2, -y2, -y1);
            }
            // quater x<0,y>0
            if (x1 < 0 && x2 < 0 && y1 > 0 && y2 > 0 &&
                    r1_1 > limit && r1_2 > limit && r2_1 < limit && r2_2 > limit) {
                // We convert the limits to make the calculation in the first quarter
                zeroArea += zeroAreaForThreePoints(-x2, -x1, y1, y2);
            }
            // quater x<0,y<0
            if (x1 < 0 && x2 < 0 && y1 < 0 && y2 < 0 &&
                    r1_1 > limit && r1_2 > limit && r2_1 > limit && r2_2 < limit) {
                // We convert the limits to make the calculation in the first quarter
                zeroArea += zeroAreaForThreePoints(-x2, -x1, -y2, -y1);
            }
            
            // We check for two corners outside the limit in the same quarter. We
            // don't care about the other two, so we set them to zero.
            // quarter x>0,y>0
            if (r2_1 > limit && r2_2 > limit && // assure the two corners are outside the limit
                    x2 > 0 && y1 > 0 && y2 > 0 && // assure the two corners are in the correct quarter
                    ((r1_1 < limit && r1_2 < limit) || x1 < 0) // check the other two corners
                    ) {
                zeroArea += zeroAreaForTwoPoints(0, x2, y1, y2);
            }
            // quarter x>0,y>0 mirrored
            if (r1_2 >limit && r2_2 > limit && // assure the two corners are outside the limit
                    x1 > 0 && x2 > 0 && y2 > 0 && // assure the two corners are in the correct quarter
                    ((r1_1 < limit && r2_1 < limit) || y1 < 0) // check the other two corners
                    ) {
                // We convert the limits to fit the input of the method in the first quarter
                zeroArea += zeroAreaForTwoPoints(0, y2, x1, x2);
            }
            // quarter x>0,y<0
            if (r2_1 > limit && r2_2 > limit && // assure the two corners are outside the limit
                    x2 > 0 && y1 < 0 && y2 < 0 && // assure the two corners are in the correct quarter
                    ((r1_1 < limit && r1_2 < limit) || x1 < 0) // check the other two corners
                    ) {
                // We convert the limits to fit the input of the method in the first quarter
                zeroArea += zeroAreaForTwoPoints(0, x2, -y2, -y1);
            }
            // quarter x>0,y<0 mirrored
            if (r1_1 >limit && r2_1 > limit && // assure the two corners are outside the limit
                    x1 > 0 && x2 > 0 && y1 < 0 && // assure the two corners are in the correct quarter
                    ((r1_2 < limit && r2_2 < limit) || y2 > 0) // check the other two corners
                    ) {
                // We convert the limits to fit the input of the method in the first quarter
                zeroArea += zeroAreaForTwoPoints(0, -y1, x1, x2);
            }
            // quarter x<0,y>0
            if (r1_1 > limit && r1_2 > limit && // assure the two corners are outside the limit
                    x1 < 0 && y1 > 0 && y2 > 0 && // assure the two corners are in the correct quarter
                    ((r2_1 < limit && r2_2 < limit) || x2 > 0) // check the other two corners
                    ) {
                // We convert the limits to fit the input of the method in the first quarter
                zeroArea += zeroAreaForTwoPoints(0, -x1, y1, y2);
            }
            // quarter x>0,y<0 mirrored
            if (r1_2 >limit && r2_2 > limit && // assure the two corners are outside the limit
                    x1 < 0 && x2 < 0 && y2 > 0 && // assure the two corners are in the correct quarter
                    ((r1_1 < limit && r2_1 < limit) || y1 < 0) // check the other two corners
                    ) {
                // We convert the limits to fit the input of the method in the first quarter
                zeroArea += zeroAreaForTwoPoints(0, y2, -x2, -x1);
            }
            // quarter x<0,y<0
            if (r1_1 > limit && r1_2 > limit && // assure the two corners are outside the limit
                    x1 < 0 && y1 < 0 && y2 < 0 && // assure the two corners are in the correct quarter
                    ((r2_1 < limit && r2_2 < limit) || x2 > 0) // check the other two corners
                    ) {
                // We convert the limits to fit the input of the method in the first quarter
                zeroArea += zeroAreaForTwoPoints(0, -x1, -y2, -y1);
            }
            // quarter x<0,y<0 mirrored
            if (r1_1 >limit && r1_2 > limit && // assure the two corners are outside the limit
                    x1 < 0 && x2 < 0 && y1 < 0 && // assure the two corners are in the correct quarter
                    ((r1_2 < limit && r2_2 < limit) || y2 > 0) // check the other two corners
                    ) {
                // We convert the limits to fit the input of the method in the first quarter
                zeroArea += zeroAreaForTwoPoints(0, -y1, -x2, -x1);
            }
            
            return multiplier * (rectangleArea - zeroArea);
        }
        
        /*
         * Returns the area outside of the limit for the first quarter when the
         * the (x2,y2), (x2,y1) and (x1,y2) corners are out of the limit in the 
         * first quarter.
         */
        private double zeroAreaForThreePoints(double x1, double x2, double y1, double y2) {
            double sideA = Math.sqrt(x1 * x1 + y1 * y1);
            double sideB = limit - sideA;
            double xA = Math.sqrt(limit * limit - y1 * y1);
            double sideC = xA - x1;
            double angleA = Math.atan(y1 / x1);
            double angleB = Math.atan(y1 / xA);
            double angleC = angleA - angleB;
            double areaA = 0.25 * limit * limit * (2 * angleC - Math.sin(2 * angleC));
            double sideD = sideC * Math.sin(angleA);
            double sideE = sideC * Math.cos(angleA);
            double areaB = sideD * sideE / 2;
            double yA = Math.sqrt(limit * limit - x1 * x1);
            double sideF = yA - y1;
            double angleD = Math.PI / 2 - angleA;
            double angleE = Math.atan(x1 / yA);
            double angleF = angleD - angleE;
            double areaC = 0.25 * limit * limit * (2 * angleF - Math.sin(2 * angleF));
            double sideG = sideF * Math.sin(angleD);
            double sideH = sideF * Math.cos(angleD);
            double areaD = sideG * sideH /2;
            double areaE = (x2 - x1) * (y2 - y1);
            return areaE - areaA - areaB - areaC - areaD;
        }
        
        /*
         * Returns the area outside of the limit for the first quarter when the
         * the (x2,y2) and (x2,y1) corners are out of the limit in the  first quarter
         * and the (x1,y2) and (x1,y1) corners are in the limit.
         */
        private double zeroAreaForTwoPoints(double x1, double x2, double y1, double y2) {
            double xA = Math.sqrt(limit * limit - y2 * y2);
            double xB = Math.sqrt(limit * limit - y1 * y1);
            double angleA = Math.atan(y2 / xA);
            double angleB = Math.atan(y1 / xB);
            double angleC = angleA - angleB;
            double areaA = 0.5 * limit * limit * (angleC - Math.sin(angleC));
            double areaB = (xB - xA) * (y2 - y1) / 2;
            double areaC = (xA - x1) * (y2 - y1);
            double areaD = (x2 - x1) * (y2 - y1);
            return areaD - areaA - areaB - areaC;
        }
    
        /*
         * Returns the area outside of the limit for the first quarter when only
         * the x2,y2 corner is out of the limit in the first quarter.
         */
        private double zeroAreaForOnePoint(double x1, double x2, double y1, double y2) {
            double angleA = Math.acos(x2 / limit);
            double angleB = Math.atan(y2 / x2);
            double angleC = angleB - angleA;
            double areaA = 0.25 * limit * limit * (2 * angleC - Math.sin(2 * angleC));
            double sideA = limit * Math.sin(angleC);
            double sideB = Math.sqrt(x2 * x2 + y2 * y2);
            double sideC = sideB - limit * Math.cos(angleC);
            double areaB = sideA * sideC / 2;
            double areaC = areaB - areaA;
            double angleD = Math.acos(y2 / limit);
            double angleE = Math.PI / 2 - angleB - angleD;
            double areaD = 0.25 * limit * limit * (2 * angleE - Math.sin(2 * angleE));
            double sideD = limit * Math.sin(angleE);
            double sideE = sideB - limit * Math.cos(angleE);
            double areaE = sideD * sideE / 2;
            double areaF = areaE - areaD;
            return areaC + areaF;
        }

        @Override
        public UnivariateRealFunction polarFunction() {
            return new UniformPolarFunction(limit, multiplier);
        }
        
    }
    
    /**
     * This class represents a univariate uniform polar function which is integrable.
     */
    @EqualsAndHashCode(callSuper=false, of={"limit","multiplier"})
    private static class UniformPolarFunction implements IntegrableUnivariateFunction {
        
        private final double limit;
        private final double multiplier;

        public UniformPolarFunction(double limit, double multiplier) {
            this.limit = limit;
            this.multiplier = multiplier;
        }

        @Override
        public double value(double x) throws FunctionEvaluationException {
            if (x < 0) {
                throw new IllegalArgumentException("Polar function cannot be calculated for negative values");
            }
            return (Math.abs(x) <= limit) ? (multiplier * x) : 0;
        }

        @Override
        public double integral(double x1, double x2) throws FunctionEvaluationException {
            if (x1 > x2) {
                throw new IllegalArgumentException("Illegal integration range [" + x1 + "," + x2 + "]");
            }
            if (x1 < 0) {
                throw new IllegalArgumentException("Polar function cannot be calculated for negative values");
            }
            if (x1 == x2 || x1 > limit) {
                return 0;
            }
            double up = Math.min(x2,limit);
            return multiplier * (up - x1) * (up + x1) / 2.;
        }
        
    }
    
    /**
     * This class represents a univariate uniform function which is integrable.
     */
    @EqualsAndHashCode(callSuper=false, of={"limit","multiplier"})
    private static class UniformFunction implements IntegrableUnivariateFunction {
        
        private final double limit;
        private final double multiplier;

        public UniformFunction(double limit, double multiplier) {
            this.limit = limit;
            this.multiplier = multiplier;
        }

        @Override
        public double integral(double x1, double x2) throws FunctionEvaluationException {
            if (x1 > x2) {
                throw new IllegalArgumentException("Illegal integration range [" + x1 + "," + x2 + "]");
            }
            if (x1 == x2 || x1 > limit || x2 < -limit) {
                return 0;
            }
            double lowLimit = Math.max(x1, -limit);
            double highLimit = Math.min(x2, limit);
            return multiplier * (highLimit - lowLimit);
        }

        @Override
        public double value(double x) throws FunctionEvaluationException {
            return (Math.abs(x) <= limit) ? multiplier : 0;
        }
        
    }
    
}
