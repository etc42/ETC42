/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.CalculationListener;
import org.cnrs.lam.cesam.util.calculator.Calculator;
import org.cnrs.lam.cesam.util.calculator.CalculatorManager;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.ConstructionListener;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Result;
import org.javatuples.Tuple;

/**
 * This class is a wrapper around the CalculatorManager, which introduces aspects
 * (AOP) before and after the produced calculators methods. Currently these
 * aspects guarantee that if a calculators result is retrieved from the cache,
 * the results of its sub-calculators will be also included in the final results.
 * For more details see the javadoc of {@link CalculatorManager}.
 *
 * @author Nikolaos Apostolakos
 */
public class EtcCalculatorManager <C extends Tuple, I extends Tuple, O extends Tuple, T extends AbstractCalculator<C,I,O>> {
    
    Logger logger = Logger.getLogger(EtcCalculatorManager.class);
    
    //private static Map<Class, SubResultsFix> subResultsFixMap = new HashMap<Class, SubResultsFix>();
    private CalculatorManager<C, T> manager;
    Class<T> type;

    private EtcCalculatorManager(CalculatorManager<C, T> manager, Class<T> type) {
        this.manager = manager;
        this.type = type;
    }
    
    /**
     * See {@link CalculatorManager#getManager(java.lang.Class)}.
     */
    public static <C extends Tuple, I extends Tuple, O extends Tuple, T extends AbstractCalculator<C,I,O>> EtcCalculatorManager<C,I,O,T> getManager(Class<T> type) {
        EtcCalculatorManager<C, I, O, T> etcManager = new EtcCalculatorManager<C, I, O, T>(CalculatorManager.getManager(type), type);
        return etcManager;
    }
    
    /**
     * See {@link CalculatorManager#getCalculator(org.javatuples.Tuple)}.
     */
    public T getCalculator(C configuration) throws InitializationException, ConfigurationException {
        // We create the real calculator and the cglib object
        ConstructionSubResultsFix<C, I, O> constructionListener = new ConstructionSubResultsFix<C,I,O>();
        T calculator = manager.getCalculator(configuration, constructionListener);
        if (calculator.getCalculationListener() == null) {
            calculator.setCalculationListener(new CalculationSubResultsFix<I, O>(constructionListener.getResultNameMap()));
        }
        return calculator;
    }
    
    private static class ConstructionSubResultsFix <C extends Tuple, I extends Tuple, O extends Tuple> implements ConstructionListener<C, I, O> {
        
        private Map<CalculationResults.Level, Set<String>> resultNameMap;
        private Map<CalculationResults.Level, Set<String>> resultsBefore;
        private static Map<Calculator, Map<CalculationResults.Level, Set<String>>> existingResultNameMaps =
                new HashMap<Calculator, Map<CalculationResults.Level, Set<String>>>();

        public Map<CalculationResults.Level, Set<String>> getResultNameMap() {
            return resultNameMap;
        }

        @Override
        public void beforeConstruction(C configuration) {
            resultsBefore = getResultNamesMap(ResultsHolder.getResults());
        }

        @Override
        public void instanceCreated(C configuration, Calculator<I, O> calculator) {
            resultNameMap = new EnumMap<CalculationResults.Level, Set<String>>(CalculationResults.Level.class);
        }

        @Override
        public void configurationValidated(C configuration, Calculator<I, O> calculator) {
        }

        @Override
        public void instanceInitialized(C configuration, Calculator<I, O> calculator) {
            Map<CalculationResults.Level, Set<String>> resultsAfter = getResultNamesMap(ResultsHolder.getResults());
            resultNameMap = new EnumMap<CalculationResults.Level, Set<String>>(CalculationResults.Level.class);
            for (CalculationResults.Level level : CalculationResults.Level.values()) {
                Set<String> newResults = new HashSet<String>(resultsAfter.get(level));
                newResults.removeAll(resultsBefore.get(level));
                resultNameMap.put(level, newResults);
            }
            existingResultNameMaps.put(calculator, resultNameMap);
        }

        @Override
        public void instanceRetrievedFromCache(C configuration, Calculator<I, O> calculator) {
            resultNameMap = existingResultNameMaps.get(calculator);
        }

        @Override
        public void afterConstruction(C configuration, Calculator<I, O> calculator) {
        }
        
    }
    
    private static class CalculationSubResultsFix <I extends Tuple, O extends Tuple> implements CalculationListener<I, O> {
        
        private boolean copyNeeded = false;
        private Map<CalculationResults.Level, Set<String>> defaultNameMap;
        private Map<CalculationResults.Level, Set<String>> resultNameMap;
        private Map<CalculationResults.Level, Set<String>> resultsBefore;

        public CalculationSubResultsFix(Map<CalculationResults.Level, Set<String>> defaultNameMap) {
            this.defaultNameMap = defaultNameMap;
        }

        @Override
        public void beforeCalculation(I input) {
            resultsBefore = getResultNamesMap(ResultsHolder.getResults());
        }

        @Override
        public void outputCalculated(I input, O output) {
            Map<CalculationResults.Level, Set<String>> resultsAfter = getResultNamesMap(ResultsHolder.getResults());
            resultNameMap = new EnumMap<CalculationResults.Level, Set<String>>(CalculationResults.Level.class);
            for (CalculationResults.Level level : CalculationResults.Level.values()) {
                Set<String> newResults = new HashSet<String>(resultsAfter.get(level));
                newResults.removeAll(resultsBefore.get(level));
                if (defaultNameMap != null && defaultNameMap.get(level) != null) {
                    newResults.addAll(defaultNameMap.get(level));
                }
                resultNameMap.put(level, newResults);
            }
            copyNeeded = false;
        }

        @Override
        public void outputRetrievedFromCache(I input, O output) {
            copyNeeded = true;
        }

        @Override
        public void afterCalculation(I input, O output) {
            if (copyNeeded) {
                for (CalculationResults.Level level : CalculationResults.Level.values()) {
                    for (String name : resultNameMap.get(level)) {
                    	Result result = PreviousResultsHolder.getResults().getResultByName(name);
                    	if (result != null) {                    		
                    		ResultsHolder.getResults().addResult(result, level);
                    	}
                    }
                }
            }
        }
    }
        
    private static Map<CalculationResults.Level, Set<String>> getResultNamesMap(CalculationResults results) {
        Map<CalculationResults.Level, Set<String>> map = new EnumMap<CalculationResults.Level, Set<String>>(CalculationResults.Level.class);
        for (CalculationResults.Level level : CalculationResults.Level.values()) {
            Set<String> levelSet = new HashSet<String>();
            for (CalculationResults.Result res : results.getResults(level).values()) {
                levelSet.add(res.getName());
            }
            map.put(level, levelSet);
        }
        return map;
    }

}
