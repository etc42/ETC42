/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.util.functions;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;
import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.apache.commons.math.transform.FastFourierTransformer;
import org.cnrs.lam.dis.etc.calculator.util.datasets.LinearFunctionDataset;
import org.cnrs.lam.dis.etc.calculator.util.integration.IntegrationTool;
import org.cnrs.lam.dis.etc.configuration.ConfigFactory;
import org.javatuples.Pair;

/**
 */
public class FunctionKernelConvolution implements UnivariateRealFunction {
    
    private final UnivariateRealFunction originalFunction;
    private final BoundedUnivariateFunction kernelFunction;
    
    private final Map<Pair<Double, Double>, UnivariateRealFunction> cache;
    FastFourierTransformer fft = new FastFourierTransformer();

    public FunctionKernelConvolution(UnivariateRealFunction originalFunction, BoundedUnivariateFunction kernelFunction) {
        this.originalFunction = originalFunction;
        // We use a kernel function with only non zero values
        double min = kernelFunction.getBounds().getValue0();
        double max = kernelFunction.getBounds().getValue1();
        double nonZeroMin = kernelFunction.getNonZeroBounds().getValue0();
        double nonZeroMax = kernelFunction.getNonZeroBounds().getValue1();
        double finalNonZeroMin = Math.max(min, nonZeroMin);
        double finalNonZeroMax = Math.min(max, nonZeroMax);
        double finalMin = finalNonZeroMin - (finalNonZeroMax - finalNonZeroMin) / 2;
        double finalMax = finalNonZeroMax + (finalNonZeroMax - finalNonZeroMin) / 2;
        this.kernelFunction = new BoundedFunctionWrapper(finalMin, finalMax, finalNonZeroMin, finalNonZeroMax, kernelFunction);
        cache = new LinkedHashMap<Pair<Double, Double>, UnivariateRealFunction>();
    }

    @Override
    public double value(double x) throws FunctionEvaluationException {
        UnivariateRealFunction function = getFunctionFromCache(x);
        if (function == null) {
            function = createConvolutionFunction(x);
        }
        return function.value(x);
    }
    
    /**
     * Searches if the cache contains a function for calculating the given value.
     * If it does, it returns the function. Otherwise it returns null.
     * @param x The value to search for
     * @return The cached function or null if there is none
     */
    private UnivariateRealFunction getFunctionFromCache(double x) {
        for (Map.Entry<Pair<Double, Double>, UnivariateRealFunction> entry : cache.entrySet()) {
            Pair<Double, Double> range = entry.getKey();
            if (x >= range.getValue0() && x <= range.getValue1()) {
                return entry.getValue();
            }
        }
        return null;
    }
    
    private UnivariateRealFunction createConvolutionFunction(double rangeCenter)
            throws FunctionEvaluationException {
        int n = ConfigFactory.getConfig().getPsfConvolutionSamplingSize();
        Pair<Double, Double> kernelBounds = kernelFunction.getNonZeroBounds();
        double kernelMin = kernelBounds.getValue0();
        double kernelMax = kernelBounds.getValue1();
        double step = (kernelMax - kernelMin) / n;
        double functionMin = rangeCenter - (kernelMax - kernelMin) / 2;
        double functionMax = rangeCenter + (kernelMax - kernelMin) / 2;
        
        // Get the kernel integrals
        double[] kernelIntegrals = getFunctionIntegrals(kernelFunction, kernelMin, step, n + 1);
        double[] originalIntegrals = getFunctionIntegrals(originalFunction, functionMin - (kernelMax - kernelMin) / 2, step, 2*n + 1);
        
        double[] covolutionIntegrals = new double[n+1];
        for (int i = 0; i <= n; i++) {
            double value = 0;
            for(int j = 0; j <= n; j++) {
                value += kernelIntegrals[j] * originalIntegrals[i + j];
            }
            covolutionIntegrals[i] = value;
        }
        
        Map<Double, Double> resultData = new TreeMap<Double, Double>();
        for (int i = 0; i <= n; i++) {
            double x = functionMin + i * step;
            resultData.put(x, covolutionIntegrals[i] / step);
        }
        LinearFunctionDataset resultFunction = new LinearFunctionDataset(resultData);
        
        // We create a bounded function to return and we added in the cache
        BoundedFunctionWrapper boundedResult = new BoundedFunctionWrapper(functionMin, functionMax, resultFunction);
        cache.put(new Pair<Double, Double>(functionMin, functionMax), boundedResult);
        return boundedResult;
    }
    
    private double[] getFunctionIntegrals(UnivariateRealFunction function,
            double min, double step, int n) throws FunctionEvaluationException {
        double[] result = new double[n];
        for (int i = 0; i < n; i ++) {
            double x = min + i * step;
            result[i] = IntegrationTool.univariateIntegral(function, x - step/2, x + step/2);
        }
        return result;
    }

}
