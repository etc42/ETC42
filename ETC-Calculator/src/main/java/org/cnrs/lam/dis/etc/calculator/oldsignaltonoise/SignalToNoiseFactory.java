/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.oldsignaltonoise;

import java.util.Map;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.dis.etc.calculator.oldbackgroundnoise.BackgroundNoiseFactory;
import org.cnrs.lam.dis.etc.calculator.olddeltalambda.DeltaLambdaFactory;
import org.cnrs.lam.dis.etc.calculator.oldlambdarange.LambdaRangeFactory;
import org.cnrs.lam.dis.etc.calculator.oldnumberofpixels.NumberOfPixelsFactory;
import org.cnrs.lam.dis.etc.calculator.oldsignal.SignalFactory;
import org.cnrs.lam.dis.etc.datamodel.Instrument;
import org.cnrs.lam.dis.etc.datamodel.ObsParam;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.javatuples.Ennead;
import org.javatuples.Octet;
import org.javatuples.Pair;
import org.javatuples.Quartet;
import org.javatuples.Tuple;
import org.javatuples.Unit;

/**
 */
public class SignalToNoiseFactory implements Factory<Unit<Session>, Tuple, Quartet<
                       Map<Double, Double>,
                       Map<Double, Double>,
                       Map<Double, Double>,
                       Map<Double, Double>
                >> {

    @Override
    public Calculator<Tuple, Quartet<
                       Map<Double, Double>,
                       Map<Double, Double>,
                       Map<Double, Double>,
                       Map<Double, Double>
                >> getCalculator(Unit<Session> configuration) throws InitializationException, ConfigurationException {
        Session session = configuration.getValue0();
        Instrument instrument = session.getInstrument();
        ObsParam obsParam = session.getObsParam();
        
        Calculator<Unit<Double>, Unit<Double>> signal
                = new SignalFactory().getCalculator(configuration);
        Calculator<Unit<Double>, Unit<Double>> numberOfPixels
                = new NumberOfPixelsFactory().getCalculator(configuration);
        Calculator<Unit<Double>, Unit<Double>> backgroundNoise
                = new BackgroundNoiseFactory().getCalculator(configuration);
        Pair<Double, String> dark
                = new Pair(instrument.getDark(), instrument.getDarkUnit());
        Pair<Double, String> readout
                = new Pair(instrument.getReadout(), instrument.getReadoutUnit());
        Pair<Double, String> dit 
                = new Pair(obsParam.getDit(), obsParam.getDitUnit());
        Integer nExpo = (obsParam.getFixedParameter() == ObsParam.FixedParameter.SNR)
                ? 1 : obsParam.getNoExpo();
        Pair<Double, String> exposureTime
                = new Pair(obsParam.getExposureTime(), obsParam.getExposureTimeUnit());
        Calculator<Tuple, Pair<Double,Double>> wavelengthRange
                = new LambdaRangeFactory().getCalculator(configuration);
        
        
        switch (instrument.getInstrumentType()) {
            case SPECTROGRAPH:
                Calculator<Unit<Double>, Unit<Double>> deltaLambda
                        = new DeltaLambdaFactory().getCalculator(configuration);
                switch (obsParam.getTimeSampleType()) {
                    case DIT:
                        Ennead<Calculator<Unit<Double>, Unit<Double>>,
                               Calculator<Unit<Double>, Unit<Double>>,
                               Calculator<Unit<Double>, Unit<Double>>,
                               Calculator<Unit<Double>, Unit<Double>>,
                               Pair<Double, String>, Pair<Double, String>,
                               Pair<Double, String>, Pair<Double, String>,
                               Calculator<Tuple, Pair<Double,Double>>> spectroscopyDitConfiguration
                            = new Ennead(deltaLambda, signal, numberOfPixels, backgroundNoise
                                , dark, readout, dit, exposureTime, wavelengthRange);
                        return EtcCalculatorManager.getManager(SpectroscopyDit.class)
                                .getCalculator(spectroscopyDitConfiguration);
                    case NO_EXPO:
                        Ennead<Calculator<Unit<Double>, Unit<Double>>,
                               Calculator<Unit<Double>, Unit<Double>>,
                               Calculator<Unit<Double>, Unit<Double>>,
                               Calculator<Unit<Double>, Unit<Double>>,
                               Pair<Double, String>, Pair<Double, String>,
                               Integer, Pair<Double, String>,
                               Calculator<Tuple, Pair<Double,Double>>> spectroscopyNExpoConfiguration
                            = new Ennead(deltaLambda, signal, numberOfPixels, backgroundNoise
                                , dark, readout, nExpo, exposureTime, wavelengthRange);
                        return EtcCalculatorManager.getManager(SpectroscopyExposuresNumber.class)
                                .getCalculator(spectroscopyNExpoConfiguration);
                }
            case IMAGING:
                switch (obsParam.getTimeSampleType()) {
                    case DIT:
                        Octet<Calculator<Tuple, Pair<Double,Double>>,
                               Calculator<Unit<Double>, Unit<Double>>,
                               Calculator<Unit<Double>, Unit<Double>>,
                               Calculator<Unit<Double>, Unit<Double>>,
                               Pair<Double, String>, Pair<Double, String>,
                               Pair<Double, String>, Pair<Double, String>> imagingDitConfiguration
                            = new Octet(wavelengthRange, signal, numberOfPixels, backgroundNoise
                                , dark, readout, dit, exposureTime);
                        return EtcCalculatorManager.getManager(ImagingDit.class)
                                .getCalculator(imagingDitConfiguration);
                    case NO_EXPO:
                        Octet<Calculator<Tuple, Pair<Double,Double>>,
                               Calculator<Unit<Double>, Unit<Double>>,
                               Calculator<Unit<Double>, Unit<Double>>,
                               Calculator<Unit<Double>, Unit<Double>>,
                               Pair<Double, String>, Pair<Double, String>,
                               Integer, Pair<Double, String>> imagingNExpoConfiguration
                            = new Octet(wavelengthRange, signal, numberOfPixels, backgroundNoise
                                , dark, readout, nExpo, exposureTime);
                        return EtcCalculatorManager.getManager(ImagingExposuresNumber.class)
                                .getCalculator(imagingNExpoConfiguration);
                }
        }
        throw new UnsupportedOperationException("SNR calculation is not"
                + " supported for the given configuration");
    }
    
}
