/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.util.functionmultiplication;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NavigableSet;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.apache.log4j.Logger;
import org.cnrs.lam.dis.etc.calculator.util.datasets.LinearFunctionDataset;
import org.cnrs.lam.dis.etc.calculator.util.datasets.TemplateFunctionDataset;
import org.cnrs.lam.dis.etc.calculator.util.functions.BoundedUnivariateFunction;
import org.cnrs.lam.dis.etc.calculator.util.functions.IntegrableUnivariateFunction;
import org.javatuples.Pair;

/**
 * The {@link FunctionMultiplicationTool} is a tool for creating functions
 * representing the product of a set of functions.
 */
public class FunctionMultiplicationTool {
    
    private static final Logger logger = Logger.getLogger(FunctionMultiplicationTool.class);
    
    /**
     * Returns a function which represents the product of the given functions.
     * The returned function bounds are set to the range where all the given
     * {@link BoundedUnivariateFunction} functions are valid. If there are no
     * {@link BoundedUnivariateFunction} given, the bounds are set to infinity.
     * Note that this method
     * takes in consideration the implementation type of the functions to speed
     * up the multiplication and it returns the best possible implementation.
     * @param functions The functions to multiply
     * @return A function representing the product of the given functions
     * @throws FunctionBoundsTooSmallException If any of the functions has non
     * infinite bounds
     * @throws FunctionEvaluationException  If a function evaluation fails
     */
    public static BoundedUnivariateFunction multiply(UnivariateRealFunction... functions) 
            throws FunctionEvaluationException {
        return multiply(1., functions);
    }
    
    /**
     * Returns a function which represents the product of the given functions.
     * The returned function bounds are set to the range where all the given
     * {@link BoundedUnivariateFunction} functions are valid. If there are no
     * {@link BoundedUnivariateFunction} given, the bounds are set to infinity.
     * Note that this method does not
     * take in consideration the implementation type of the functions to speed
     * up the multiplication and it returns a function that will call the given
     * functions each time it will be used.
     * @param functions The functions to multiply
     * @return A function representing the product of the given functions
     * @throws FunctionBoundsTooSmallException If any of the functions has non
     * infinite bounds
     * @throws FunctionEvaluationException  If a function evaluation fails
     */
    public static BoundedUnivariateFunction multiplyUnoptimized(UnivariateRealFunction... functions) 
            throws FunctionEvaluationException {
        double min = Double.NEGATIVE_INFINITY;
        double max = Double.POSITIVE_INFINITY;
        for (UnivariateRealFunction function : functions) {
            if (function instanceof BoundedUnivariateFunction) {
                BoundedUnivariateFunction bounded = (BoundedUnivariateFunction) function;
                Pair<Double, Double> bounds = bounded.getBounds();
                if (bounds.getValue0() > min) {
                    min = bounds.getValue0();
                }
                if (bounds.getValue1() < max) {
                    max = bounds.getValue1();
                }
            }
        }
        List<UnivariateRealFunction> funcList = new LinkedList<UnivariateRealFunction>();
        funcList.addAll(Arrays.asList(functions));
        return new ProductFunction(min, max, 1., funcList);
    }
    
    /**
     * Returns a function which represents the product of the given functions.
     * The returned function bounds are set to the range where all the given
     * {@link BoundedUnivariateFunction} functions are valid. If there are no
     * {@link BoundedUnivariateFunction} given, the bounds are set to infinity.
     * Note that this method
     * takes in consideration the implementation type of the functions to speed
     * up the multiplication and it returns the best possible implementation.
     * @param multiplier A constant value with which the product is multiplied
     * @param functions The functions to multiply
     * @return A function representing the product of the given functions
     * @throws FunctionBoundsTooSmallException If any of the functions has non
     * infinite bounds
     * @throws FunctionEvaluationException  If a function evaluation fails
     */
    public static BoundedUnivariateFunction multiply(double multiplier, UnivariateRealFunction... functions) 
            throws FunctionEvaluationException {
        double min = Double.NEGATIVE_INFINITY;
        double max = Double.POSITIVE_INFINITY;
        for (UnivariateRealFunction function : functions) {
            if (function instanceof BoundedUnivariateFunction) {
                BoundedUnivariateFunction bounded = (BoundedUnivariateFunction) function;
                Pair<Double, Double> bounds = bounded.getBounds();
                if (bounds.getValue0() > min) {
                    min = bounds.getValue0();
                }
                if (bounds.getValue1() < max) {
                    max = bounds.getValue1();
                }
            }
        }
        BoundedUnivariateFunction result = null;
        try {
            result = multiply(min, max, multiplier, functions);
        } catch (FunctionBoundsTooSmallException ex) {
            // This should never happen, as we calculated the min and max based
            // on the functions bounds!!! Because of that we throw a runtime exception
            logger.error("Problem with bounds of function with index " + ex.getFunctionIndex(), ex);
            throw new RuntimeException("Problem with bounds of function with index " + ex.getFunctionIndex(), ex);
        }
        return result;
    }
    
    /**
     * Returns a function which represents the product of the given functions.
     * The returned function is a {@link BoundedUnivariateFunction} with limits
     * the given range. If any of the given functions is also
     * {@link BoundedUnivariateFunction} and its bounds are smaller than the
     * given ones a {@link FunctionBoundsTooSmallException} is thrown, which
     * contains the index of the problematic function. Note that this method
     * takes in consideration the implementation type of the functions to speed
     * up the multiplication and it returns the best possible implementation.
     *
     * @param rangeMin The minimum bound of the multiplication
     * @param rangeMax The maximum bound of the multiplication
     * @param functions The functions to multiply
     * @return A function representing the product of the given functions
     */
    public static BoundedUnivariateFunction multiply(double rangeMin, double rangeMax,
            UnivariateRealFunction... functions) throws FunctionBoundsTooSmallException, FunctionEvaluationException {
        return multiply(rangeMin, rangeMax, 1., functions);
    }

    /**
     * Returns a function which represents the product of the given functions.
     * The returned function is a {@link BoundedUnivariateFunction} with limits
     * the given range. If any of the given functions is also
     * {@link BoundedUnivariateFunction} and its bounds are smaller than the
     * given ones a {@link FunctionBoundsTooSmallException} is thrown, which
     * contains the index of the problematic function. Note that this method
     * takes in consideration the implementation type of the functions to speed
     * up the multiplication and it returns the best possible implementation.
     *
     * @param rangeMin The minimum bound of the multiplication
     * @param rangeMax The maximum bound of the multiplication
     * @param multiplier A constant value with which the product is multiplied
     * @param functions The functions to multiply
     * @return A function representing the product of the given functions
     */
    public static BoundedUnivariateFunction multiply(double rangeMin, double rangeMax, double multiplier,
            UnivariateRealFunction... functions) throws FunctionBoundsTooSmallException, FunctionEvaluationException {
        // We create a list with the functions. We do that because we are going to modify it.
        List<UnivariateRealFunction> functionList = new LinkedList<UnivariateRealFunction>(Arrays.asList(functions));

        // We check the bounds of all the functions
        for (UnivariateRealFunction function : functionList) {
            if (function instanceof BoundedUnivariateFunction) {
                BoundedUnivariateFunction boundedFunction = (BoundedUnivariateFunction) function;
                Pair<Double, Double> bounds = boundedFunction.getBounds();
                if (bounds.getValue0() > rangeMin || bounds.getValue1() < rangeMax) {
                    throw new FunctionBoundsTooSmallException(functionList.indexOf(function));
                }
            }
        }

        // We group the functions in types that we know how to multiply
        List<LinearFunctionDataset> linearFunctionDatasetList = new LinkedList<LinearFunctionDataset>();
        List<TemplateFunctionDataset> templateFunctionDatasetList = new LinkedList<TemplateFunctionDataset>();
        for (UnivariateRealFunction function : functionList) {
            if (function instanceof LinearFunctionDataset) {
                LinearFunctionDataset linearFunctionDataset = (LinearFunctionDataset) function;
                linearFunctionDatasetList.add(linearFunctionDataset);
            } else if (function instanceof TemplateFunctionDataset) {
                TemplateFunctionDataset templateFunctionDataset = (TemplateFunctionDataset) function;
                templateFunctionDatasetList.add(templateFunctionDataset);
            }
        }
        // We remove the functions that are going to be handled in a special way
        functionList.removeAll(linearFunctionDatasetList);
        functionList.removeAll(templateFunctionDatasetList);

        // We create the products of the functions of same types
        LinearFunctionDataset linearFunctionDatasetProduct = (!linearFunctionDatasetList.isEmpty())
                ? multiplyLinearFunctionDatasets(rangeMin, rangeMax, linearFunctionDatasetList) : null;
        TemplateFunctionDataset templateFunctionDatasetProduct = (!templateFunctionDatasetList.isEmpty())
                ? multiplyTemplateFunctionDatasets(rangeMin, rangeMax, templateFunctionDatasetList) : null;

        // If we have a template function we multiply all the other functions with
        // it, as we know how to do that in an efficient way.
        if (templateFunctionDatasetProduct != null) {
            if (linearFunctionDatasetProduct != null) {
                functionList.add(linearFunctionDatasetProduct);
            }
            List<IntegrableUnivariateFunction> integrableFunctionList = new LinkedList<IntegrableUnivariateFunction>();
            for (UnivariateRealFunction function : functionList) {
                if (function instanceof IntegrableUnivariateFunction) {
                    IntegrableUnivariateFunction integrableFunction = (IntegrableUnivariateFunction) function;
                    integrableFunctionList.add(integrableFunction);
                }
            }
            functionList.removeAll(integrableFunctionList);
            TemplateFunctionDataset result = multiplyWithTemplateFunctionDataset(rangeMin, rangeMax, templateFunctionDatasetProduct, integrableFunctionList.toArray(new IntegrableUnivariateFunction[] {}));
            result = multiplyWithTemplateFunctionDataset(rangeMin, rangeMax, result, functionList.toArray(new UnivariateRealFunction[] {}));
            return result;
        }
        // If we have a linear function dataset we multiply the other functions
        // with it as we know how to do that efficiently
        if (linearFunctionDatasetProduct != null) {
            return multiplyWithLinearFunctionDataset(rangeMin, rangeMax, linearFunctionDatasetProduct, functionList.toArray(new UnivariateRealFunction[] {}));
        }

        // Here we do not have any option for better calculation left, so we return
        // a bounded function which just multiplies the values of the functions
        return new ProductFunction(rangeMin, rangeMax, multiplier, functionList);
    }
    
    private TemplateFunctionDataset multiplyTemplateFunctionDatasetWithConstant
            (TemplateFunctionDataset dataset, double multiplier) {
        Map<Double, Double> productData = new TreeMap<Double, Double>();
        for (Map.Entry<Double, Double> entry : dataset.getData().entrySet()) {
            productData.put(entry.getKey(), entry.getValue() * multiplier);
        }
        return new TemplateFunctionDataset(productData);
    }
    
    private LinearFunctionDataset multiplyLinearFunctionDatasetWithConstant
            (LinearFunctionDataset dataset, double multiplier) {
        Map<Double, Double> productData = new TreeMap<Double, Double>();
        for (Map.Entry<Double, Double> entry : dataset.getData().entrySet()) {
            productData.put(entry.getKey(), entry.getValue() * multiplier);
        }
        return new LinearFunctionDataset(productData);
    }
    
    private static class ProductFunction implements BoundedUnivariateFunction {
        
        private double rangeMin;
        private double rangeMax;
        private double rangeNonZeroMin;
        private double rangeNonZeroMax;
        private double multiplier;
        private List<UnivariateRealFunction> functionList;

        public ProductFunction(double rangeMin, double rangeMax, double multiplier, List<UnivariateRealFunction> functionList) {
            this.rangeMin = rangeMin;
            this.rangeMax = rangeMax;
            this.multiplier = multiplier;
            this.functionList = functionList;
            rangeNonZeroMin = Double.NEGATIVE_INFINITY;
            rangeNonZeroMax = Double.POSITIVE_INFINITY;
            for (UnivariateRealFunction function : functionList) {
                if (function instanceof BoundedUnivariateFunction) {
                    BoundedUnivariateFunction bounded = (BoundedUnivariateFunction) function;
                    Pair<Double, Double> nonZeroBounds = bounded.getNonZeroBounds();
                    if (nonZeroBounds.getValue0() > rangeNonZeroMin) {
                        rangeNonZeroMin = nonZeroBounds.getValue0();
                    }
                    if (nonZeroBounds.getValue1() > rangeNonZeroMax) {
                        rangeNonZeroMax = nonZeroBounds.getValue1();
                    }
                }
            }
        }

        @Override
        public Pair<Double, Double> getBounds() {
            return new Pair<Double, Double>(rangeMin, rangeMax);
        }

        @Override
        public Pair<Double, Double> getNonZeroBounds() {
            return new Pair<Double, Double>(rangeNonZeroMin, rangeNonZeroMax);
        }

        @Override
        public double value(double x) throws FunctionEvaluationException {
            double value = 1;
            for (UnivariateRealFunction function : functionList) {
                value = value * function.value(x);
            }
            return value * multiplier;
        }
        
    }
    
    private static LinearFunctionDataset multiplyWithLinearFunctionDataset(double rangeMin, double rangeMax,
            LinearFunctionDataset mainFunction, UnivariateRealFunction ... functionList) 
            throws FunctionEvaluationException {
    	
        Map<Double, Double> productData = new TreeMap<Double, Double>();
        for (Map.Entry<Double, Double> entry : mainFunction.getData().entrySet()) {
            double key = entry.getKey();
            double value = entry.getValue();
            
//            System.out.println(key);
            if (key > rangeMax || key < rangeMin) {
                continue;
            }
            for (UnivariateRealFunction function : functionList) {
//            	System.out.println(function.getClass());
//            	System.out.println(function.value(key));
                value = value * function.value(key);
            }
            productData.put(key, value);
        }
        return new LinearFunctionDataset(productData);
    }

    private static TemplateFunctionDataset multiplyWithTemplateFunctionDataset(double rangeMin, double rangeMax,
            TemplateFunctionDataset mainFunction, UnivariateRealFunction ... functionList)
            throws FunctionEvaluationException {
        Map<Double, Double> productData = new TreeMap<Double, Double>();
        for (Map.Entry<Double, Double> entry : mainFunction.getData().entrySet()) {
            double key = entry.getKey();
            double value = entry.getValue();
            Entry<Double, Double> lowerDataNode = mainFunction.getLowerDataNode(key);
            Entry<Double, Double> higherDataNode = mainFunction.getHigherDataNode(key);
            double lowX;
            double highX;
            if (lowerDataNode == null) {
                lowX = key;
                highX = (higherDataNode.getKey() + key) / 2;
            } else if (higherDataNode == null) {
                lowX = (key + lowerDataNode.getKey()) / 2;
                highX = key;
            } else {
                lowX = (key + lowerDataNode.getKey()) / 2;
                highX = (higherDataNode.getKey() + key) / 2;
            }
            if (lowX > rangeMax || highX < rangeMin) {
                continue;
            }
            for (UnivariateRealFunction function : functionList) {
                value = value * function.value(key);
            }
            productData.put(key, value);
        }
        return new TemplateFunctionDataset(productData);
    }

    private static TemplateFunctionDataset multiplyWithTemplateFunctionDataset(double rangeMin, double rangeMax,
            TemplateFunctionDataset mainFunction, IntegrableUnivariateFunction ... functionList)
            throws FunctionEvaluationException {
        Map<Double, Double> productData = new TreeMap<Double, Double>();
        for (Map.Entry<Double, Double> entry : mainFunction.getData().entrySet()) {
            double key = entry.getKey();
            double value = entry.getValue();
            Entry<Double, Double> lowerDataNode = mainFunction.getLowerDataNode(key);
            Entry<Double, Double> higherDataNode = mainFunction.getHigherDataNode(key);
            double lowX;
            double highX;
            if (lowerDataNode == null) {
                lowX = key;
                highX = (higherDataNode.getKey() + key) / 2;
            } else if (higherDataNode == null) {
                lowX = (key + lowerDataNode.getKey()) / 2;
                highX = key;
            } else {
                lowX = (key + lowerDataNode.getKey()) / 2;
                highX = (higherDataNode.getKey() + key) / 2;
            }
            double range = highX - lowX;
            if (lowX > rangeMax || highX < rangeMin) {
                continue;
            }
            for (IntegrableUnivariateFunction function : functionList) {
                value = value * function.integral(lowX, highX) / range;
            }
            productData.put(key, value);
        }
        return new TemplateFunctionDataset(productData);
    }

    private static TemplateFunctionDataset multiplyTemplateFunctionDatasets(double rangeMin, double rangeMax,
            List<TemplateFunctionDataset> functionList) throws FunctionEvaluationException {
        if (functionList.isEmpty()) {
            throw new IllegalArgumentException("Function list cannot be empty");
        }
        // We need to create one node for each area where the product has constant
        // value, so for each area where all functions have constant values.

        // First we find the keys for which each function might change value.
        // These are in the middles of the nodes of each function, plus the limits
        // of each function. On these points also the product can change value.
        Set<Double> valueChangingKeySet = new TreeSet<Double>();
        for (TemplateFunctionDataset function : functionList) {
            Double first = null;
            for (Double second : function.getData().keySet()) {
                if (first != null) {
                    valueChangingKeySet.add((second + first) / 2);
                }
                first = second;
            }
            Pair<Double, Double> bounds = function.getBounds();
            valueChangingKeySet.add(bounds.getValue0());
            valueChangingKeySet.add(bounds.getValue1());
        }

        // We find the smallest distance between two value changing keys
        double smallestDistance = Double.POSITIVE_INFINITY;
        Double first = null;
        for (Double second : valueChangingKeySet) {
            if (first != null && second - first < smallestDistance) {
                smallestDistance = second - first;
            }
            first = second;
        }
        double halfOfSmallestDistance = smallestDistance / 2;

        // Between two value changing nodes we will put three nodes for the new
        // dataset, two from distance smallestDistance / 2 from the two nodes and
        // another one in the middle fo covering this area after the removal of
        // the nodes because of the limits
        NavigableSet<Double> keySet = new TreeSet<Double>();
        first = null;
        for (Double second : valueChangingKeySet) {
            if (first != null) {
                keySet.add(first + halfOfSmallestDistance);
                keySet.add(second - halfOfSmallestDistance);
                keySet.add((second + first) / 2);
            }
            first = second;
        }

        // We remove any nodes which are outside of the requested range
        NavigableSet<Double> keysToRemove = new TreeSet<Double>();
        for (Double key : keySet) {
            if (key < rangeMin || key > rangeMax) {
                keysToRemove.add(key);
            }
        }
        keySet.removeAll(keysToRemove);

        // We create the data for the new dataset.
        Map<Double, Double> productData = new TreeMap<Double, Double>();
        for (Double key : keySet) {
            Double lowerKey = keySet.lower(key);
            Double higherKey = keySet.higher(key);
            double lowerHalf = (lowerKey != null)
                    ? (key - lowerKey) / 2
                    : (higherKey - key) / 2;
            double higherHalf = (higherKey != null)
                    ? (higherKey - key) / 2
                    : (key - lowerKey) / 2;
            double value = 1;
            for (TemplateFunctionDataset function : functionList) {
                value = value * function.value(key);
            }
            value = value * (lowerHalf + higherHalf);
            productData.put(key, value);
        }

        return new TemplateFunctionDataset(productData);
    }

    private static LinearFunctionDataset multiplyLinearFunctionDatasets(double rangeMin, double rangeMax,
            List<LinearFunctionDataset> functionList) throws FunctionEvaluationException {
        if (functionList.isEmpty()) {
            throw new IllegalArgumentException("Function list cannot be empty");
        }
        // We will create a node for each unique node of the functions which is
        // inside the required range
        Set<Double> keySet = new TreeSet<Double>();
        for (LinearFunctionDataset function : functionList) {
            for (Double key : function.getData().keySet()) {
                if (key >= rangeMin && key <= rangeMax) {
                    keySet.add(key);
                }
            }
        }

        // To cover completely the rangeMin and rangeMax we must also make calculation
        // for these values, so we add them in the list
        keySet.add(rangeMin);
        keySet.add(rangeMax);

        // We create the data for the new dataset
        Map<Double, Double> productData = new TreeMap<Double, Double>();
        for (Double key : keySet) {
            double value = 1;
            for (LinearFunctionDataset function : functionList) {
                value *= function.value(key);
            }
            productData.put(key, value);
        }

        return new LinearFunctionDataset(productData);
    }
}
