/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.signaltonoise;

import java.text.MessageFormat;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TreeMap;
import lombok.EqualsAndHashCode;
import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.apache.log4j.Logger;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.Calculator;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.calculator.util.integration.IntegrationTool;
import org.cnrs.lam.dis.etc.configuration.ConfigFactory;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.javatuples.Octet;
import org.javatuples.Pair;
import org.javatuples.Tuple;
import org.javatuples.Unit;

/**
 * <p>The class {@link ImagingExposuresNumber} is a calculator for computing
 * the signal to noise (SNR) for the case of spectrograph simulation.</p>
 * 
 * <p>The configuration of the calculator is a {@link Octet},
 * which contains the following elements:</p>
 * <ol>
 *   <li>{@link Calculator}: A calculator for computing the wavelength range</li>
 *   <li>{@link Pair}: A tuple containing the following:</li>
 *   <ol>
 *     <li>{@link Calculator}: A calculator for computing the simulated signal</li>
 *     <li>{@link Pair}: The extra signal and its unit</li>
 *   </ol>
 *   <li>{@link Pair}: A tuple containing the following:</li>
 *   <ul>
 *     <li>{@link Calculator}: A calculator for computing the simulated background</li>
 *     <li>{@link Pair}: The extra background and its unit</li>
 *   </ul>
 *   <li>{@link Calculator}: A calculator for computing the number of pixels</li>
 *   <li>{@link Pair}: The dark current and its unit</li>
 *   <li>{@link Pair}: The redout noise and its unit</li>
 *   <li>{@link Integer}: The number of exposures</li>
 *   <li>{@link Pair}: The exposure time and its unit</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Tuple}, as this
 * calculator does not accept any input. It should always be null.</p>
 * 
 * <p>The output of the calculator is an {@link Octet},
 * which contains the following elements:</p>
 * <ol>
 *   <li>{@link Map}: The central pixel SNR</li>
 *   <li>{@link Map}: The SNR</li>
 *   <li>{@link Map}: The central pixel total signal</li>
 *   <li>{@link Map}: The total signal</li>
 *   <li>{@link Map}: The central pixel total background</li>
 *   <li>{@link Map}: The total background</li>
 *   <li>{@link Map}: The central pixel total detector noise</li>
 *   <li>{@link Map}: The total detector noise</li>
 * </ol>
 * <p>Note that all the returned maps contain only one entry.</p>
 */
@EqualsAndHashCode(callSuper=false, of={"wavelengthRange","simulatedSignal","extraSignal",
        "simulatedBackground","extraBackground","numberOfPixels","dark","readout",
        "noExpo","exposureTime"})
public class ImagingExposuresNumber extends AbstractCalculator<
        Octet<
            Calculator<Tuple, Pair<Double, Double>>,
            Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>,
                 Pair<Double, String>>,
            Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>,
                 Pair<Double, String>>,
            Calculator<Unit<Double>, Unit<Double>>,
            Pair<Double, String>, Pair<Double, String>,
            Integer, Pair<Double, String>
        >, Tuple, 
        Octet<
            Map<Double, Double>, Map<Double, Double>,
            Map<Double, Double>, Map<Double, Double>,
            Map<Double, Double>, Map<Double, Double>,
            Map<Double, Double>, Map<Double, Double>
        >> {
    
    
    private static final Logger logger = Logger.getLogger(ImagingExposuresNumber.class);
    private static final ResourceBundle validationErrorsBundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");
    
    private Calculator<Tuple, Pair<Double, Double>> wavelengthRange;
    private Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>> simulatedSignal;
    private double extraSignal;
    private Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>> simulatedBackground;
    private double extraBackground;
    private Calculator<Unit<Double>, Unit<Double>> numberOfPixels;
    private double dark;
    private double readout;
    private double noExpo;
    private double exposureTime;

    private double minLambda;
    private double maxLambda;
    private double detectorPerPixel;
    private double centralWavelength;
    
    /**
     * Validates the given configuration. It requires that the extra signal is
     * not negative and expressed in electrons per sec, that the extra background
     * is not negative and expressed in electrons per sec, that the dark current is 
     * non negative and expressed in e/pix/sec, that the readout noise is non
     * negative and expressed in e/pix,that the number of the exposures is positive
     * and that the exposure time is non negative and expressed in seconds.
     * @param configuration The configuration to validate
     * @throws ConfigurationException if the configuration is invalid
     */
    @Override
    protected void validateConfiguration(Octet<
            Calculator<Tuple, Pair<Double, Double>>,
            Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>,
                 Pair<Double, String>>,
            Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>,
                 Pair<Double, String>>,
            Calculator<Unit<Double>, Unit<Double>>,
            Pair<Double, String>, Pair<Double, String>,
            Integer, Pair<Double, String>> configuration) throws ConfigurationException {
        // Check that the extra signal is a non negative number
        if (configuration.getValue1().getValue1().getValue0() < 0) {
            String message = validationErrorsBundle.getString("EXTRA_SIGNAL_NEGATIVE");
            message = MessageFormat.format(message, configuration.getValue1().getValue1().getValue0());
            throw new ConfigurationException(message);
        }
        // Check if the extra signal is in electrons per sec
        if (!Units.isElectronsPerSec(configuration.getValue1().getValue1().getValue1())) {
            String message = validationErrorsBundle.getString("EXTRA_SIGNAL_WRONG_UNIT");
            message = MessageFormat.format(message, Units.getElectronsPerSec(), configuration.getValue1().getValue1().getValue1());
            throw new ConfigurationException(message);
        }
        // Check that the extra background is a non negative number
        if (configuration.getValue2().getValue1().getValue0() < 0) {
            String message = validationErrorsBundle.getString("EXTRA_BACKGROUND_NEGATIVE");
            message = MessageFormat.format(message, configuration.getValue2().getValue1().getValue0());
            throw new ConfigurationException(message);
        }
        // Check if the extra background is in electrons per sec
        if (!Units.isElectronsPerSec(configuration.getValue2().getValue1().getValue1())) {
            String message = validationErrorsBundle.getString("EXTRA_BACKGROUND_WRONG_UNIT");
            message = MessageFormat.format(message, Units.getElectronsPerSec(), configuration.getValue2().getValue1().getValue1());
            throw new ConfigurationException(message);
        }
        // Check that the dark current is a non negative number
        if (configuration.getValue4().getValue0() < 0) {
            String message = validationErrorsBundle.getString("DARK_CURRENT_NEGATIVE");
            message = MessageFormat.format(message, configuration.getValue4().getValue0());
            throw new ConfigurationException(message);
        }
        // Check if the dark current is in e/pix/sec
        if (!Units.isElectronsPerPixelPerSec(configuration.getValue4().getValue1())) {
            String message = validationErrorsBundle.getString("DARK_CURRENT_WRONG_UNIT");
            message = MessageFormat.format(message, Units.getElectronsPerPixelPerSec(), configuration.getValue4().getValue1());
            throw new ConfigurationException(message);
        }
        // Check that the readout noise is a non negative number
        if (configuration.getValue5().getValue0() < 0) {
            String message = validationErrorsBundle.getString("READOUT_NOISE_NEGATIVE");
            message = MessageFormat.format(message, configuration.getValue5().getValue0());
            throw new ConfigurationException(message);
        }
        // Check if the readout noise is in e/pix
        if (!Units.isElectronsPerPixel(configuration.getValue5().getValue1())) {
            String message = validationErrorsBundle.getString("READOUT_NOISE_WRONG_UNIT");
            message = MessageFormat.format(message, Units.getElectronsPerPixel(), configuration.getValue5().getValue1());
            throw new ConfigurationException(message);
        }
        // Check that the number of exposures is positive
        if (configuration.getValue6() <= 0) {
            String message = validationErrorsBundle.getString("NUMBER_OF_EXPOSURES_NOT_POSITIVE");
            message = MessageFormat.format(message, configuration.getValue6());
            throw new ConfigurationException(message);
        }
        // Check that the exposure time is a non negative number
        if (configuration.getValue7().getValue0() < 0) {
            String message = validationErrorsBundle.getString("EXPOSURE_TIME_NEGATIVE");
            message = MessageFormat.format(message, configuration.getValue7().getValue0());
            throw new ConfigurationException(message);
        }
        // Check if the exposure time is in seconds
        if (!Units.isSec(configuration.getValue7().getValue1())) {
            String message = validationErrorsBundle.getString("EXPOSURE_TIME_WRONG_UNIT");
            message = MessageFormat.format(message, Units.SEC, configuration.getValue7().getValue1());
            throw new ConfigurationException(message);
        }
    }

    /**
     * Initializes the calculator instance with the given configuration.
     * @throws InitializationException 
     */
    @Override
    protected void initialize(Octet<
            Calculator<Tuple, Pair<Double, Double>>,
            Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>,
                 Pair<Double, String>>,
            Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>,
                 Pair<Double, String>>,
            Calculator<Unit<Double>, Unit<Double>>,
            Pair<Double, String>, Pair<Double, String>,
            Integer, Pair<Double, String>> configuration) throws InitializationException {
        wavelengthRange = configuration.getValue0();
        simulatedSignal = configuration.getValue1().getValue0();
        extraSignal = configuration.getValue1().getValue1().getValue0();
        simulatedBackground = configuration.getValue2().getValue0();
        extraBackground = configuration.getValue2().getValue1().getValue0();
        numberOfPixels = configuration.getValue3();
        dark = configuration.getValue4().getValue0();
        readout = configuration.getValue5().getValue0();
        noExpo = configuration.getValue6();
        exposureTime = configuration.getValue7().getValue0();
        
        // Calculate the minimum and maximum of the wavelength range
        Pair<Double, Double> minMax = null;
        try {
            minMax = wavelengthRange.calculate(null);
        } catch (CalculationException e) {
            logger.error("Failed to calculate the wavelength range", e);
            throw new InitializationException(e.getMessage(), e);
        }
        minLambda = minMax.getValue0();
        maxLambda = minMax.getValue1();
        
        // Calculate the central wavelength
        centralWavelength = (maxLambda + minLambda) / 2;
        
        // calcualte the detector noise per pixel
        detectorPerPixel = dark * exposureTime + noExpo * readout * readout;
    }

    /**
     * Calculates the maximum number of pixels for all the given wavelength range.
     * @param numberOfPixels
     * @param minLambda
     * @param maxLambda
     * @return 
     */
    private static double calculateNumberOfPixels(
            Calculator<Unit<Double>, Unit<Double>> numberOfPixels,
            double minLambda, double maxLambda) throws CalculationException {
        int minLambdaInt = (int) Math.floor(minLambda);
        int maxLambdaInt = (int) Math.ceil(maxLambda);
        double result = 0;
        for (int lambda = minLambdaInt; lambda <= maxLambdaInt; lambda++) {
            Unit<Double> input = new Unit<Double>((double) lambda);
            double noOfPixels = numberOfPixels.calculate(input).getValue0();
            if (noOfPixels > result) {
                result = noOfPixels;
            }
        }
        return result;
    }

    /**
     * Returns a {@link org.javatuples.Octet} containing the signal to noise,
     * the total signal, the total background noise and the total detector noise,
     * both for the central pixel and the total affected area.
     * @param input The wavelength expressed in {@latex.inline \\AA}
     * @return
     * @throws CalculationException  if there is any error during the calculation
     */
    @Override
    protected Octet<
                    Map<Double, Double>, Map<Double, Double>,
                    Map<Double, Double>, Map<Double, Double>,
                    Map<Double, Double>, Map<Double, Double>,
                    Map<Double, Double>, Map<Double, Double>
              > performCalculation(Tuple input) throws CalculationException {
        // We calculate the number of pixels. This is the bigest number for all
        // the wavelengths
        double numberOfPixelsValue = calculateNumberOfPixels(numberOfPixels, minLambda, maxLambda);
        // We calculate the integrals of the signals both for total and for central pixel
        Pair<UnivariateRealFunction, UnivariateRealFunction> simulatedSignalFunctionsPair
                = simulatedSignal.calculate(new Pair<Double, Double>(minLambda, maxLambda));
        double cpSimulatedSignalIntegral = 0;
        double simulatedSignalIntegral = 0;
        try {
            if (ConfigFactory.getConfig().getCentralPixelFlag()) {
                cpSimulatedSignalIntegral = IntegrationTool.univariateIntegral(simulatedSignalFunctionsPair.getValue0(), minLambda, maxLambda);
            }
            simulatedSignalIntegral = IntegrationTool.univariateIntegral(simulatedSignalFunctionsPair.getValue1(), minLambda, maxLambda);
        } catch (FunctionEvaluationException ex) {
            logger.error(ex.getMessage(), ex);
            throw new CalculationException(ex.getMessage());
        }
        double cpExtraSignalIntegral = extraSignal / numberOfPixelsValue;
        // We calculate the integrals of the background noise both for total and for central pixel
        Pair<UnivariateRealFunction, UnivariateRealFunction> simulatedBgFunctionsPair
                = simulatedBackground.calculate(new Pair<Double, Double>(minLambda, maxLambda));
        double cpSimulatedBgIntegral = 0;
        double simulatedBgIntegral = 0;
        try {
            if (ConfigFactory.getConfig().getCentralPixelFlag()) {
                cpSimulatedBgIntegral = IntegrationTool.univariateIntegral(simulatedBgFunctionsPair.getValue0(), minLambda, maxLambda);
            }
            simulatedBgIntegral = IntegrationTool.univariateIntegral(simulatedBgFunctionsPair.getValue1(), minLambda, maxLambda);
        } catch (FunctionEvaluationException ex) {
            logger.error(ex.getMessage(), ex);
            throw new CalculationException(ex.getMessage());
        }
        double cpExtraBgIntegral = extraBackground / numberOfPixelsValue;
        
        // We calculate the SNRs
        double cpS = (cpSimulatedSignalIntegral + cpExtraSignalIntegral) * exposureTime;
        double s = (simulatedSignalIntegral + extraSignal) * exposureTime;
        double cpBg = (cpSimulatedBgIntegral + cpExtraBgIntegral) * exposureTime;
        double bg = (simulatedBgIntegral + extraBackground) * exposureTime;
        double det = numberOfPixelsValue * detectorPerPixel;
        double cpSignalToNoise = cpS / Math.sqrt(cpS + cpBg + detectorPerPixel);
        double signalToNoise = s / Math.sqrt(s + bg + det);
        
        // Create the maps where the results are stored
        Map<Double, Double> signalToNoiseMap = new TreeMap<Double, Double>();
        Map<Double, Double> cpSignalToNoiseMap = new TreeMap<Double, Double>();
        Map<Double, Double> totalSignalMap = new TreeMap<Double, Double>();
        Map<Double, Double> cpTotalSignalMap = new TreeMap<Double, Double>();
        Map<Double, Double> totalBackgroundNoiseMap = new TreeMap<Double, Double>();
        Map<Double, Double> cpTotalBackgroundNoiseMap = new TreeMap<Double, Double>();
        Map<Double, Double> totalDetectorNoiseMap = new TreeMap<Double, Double>();
        Map<Double, Double> cpTotalDetectorNoiseMap = new TreeMap<Double, Double>();
        
        // Populate the maps
        signalToNoiseMap.put(centralWavelength, signalToNoise);
        cpSignalToNoiseMap.put(centralWavelength, cpSignalToNoise);
        totalSignalMap.put(centralWavelength, s);
        cpTotalSignalMap.put(centralWavelength, cpS);
        totalBackgroundNoiseMap.put(centralWavelength, bg);
        cpTotalBackgroundNoiseMap.put(centralWavelength, cpBg);
        totalDetectorNoiseMap.put(centralWavelength, det);
        cpTotalDetectorNoiseMap.put(centralWavelength, detectorPerPixel);
        
        return new Octet<Map<Double, Double>, Map<Double, Double>,
                Map<Double, Double>, Map<Double, Double>,
                Map<Double, Double>, Map<Double, Double>,
                Map<Double, Double>, Map<Double, Double>>
                (cpSignalToNoiseMap, signalToNoiseMap, cpTotalSignalMap,
                totalSignalMap, cpTotalBackgroundNoiseMap, totalBackgroundNoiseMap,
                cpTotalDetectorNoiseMap, totalDetectorNoiseMap);
    }

    /**
     * <p>For every calculation the calculator adds in the simulation results
     * the following outputs of the calculator:</p>
     * <ul>
     *   <li>The filter central wavelength in final results with code FILTER_CENTRAL_WAVELENGTH</li>
     *   <li>The central pixel SNR in final results with code CENTRAL_PIXEL_SIGNAL_TO_NOISE</li>
     *   <li>The SNR in final results with code SIGNAL_TO_NOISE</li>
     *   <li>The central pixel total signal in final results with code CENTRAL_PIXEL_TOTAL_SIGNAL</li>
     *   <li>The total signal in final results with code TOTAL_SIGNAL</li>
     *   <li>The central pixel total background in final results with code CENTRAL_PIXEL_TOTAL_BACKGROUND_NOISE</li>
     *   <li>The total background in final results with code TOTAL_BACKGROUND_NOISE</li>
     *   <li>The central pixel total detector noise in final results with code CENTRAL_PIXEL_TOTAL_DELTECTOR_NOISE</li>
     *   <li>The total detector noise in final results with code TOTAL_DELTECTOR_NOISE</li>
     * </ul>
     * @param input
     * @param output 
     */
    @Override
    protected void performForEveryCalculation(Tuple input, Octet<
            Map<Double, Double>, Map<Double, Double>, Map<Double, Double>,
            Map<Double, Double>, Map<Double, Double>, Map<Double, Double>,
            Map<Double, Double>, Map<Double, Double>> output) {
        double lambda = output.getValue0().keySet().iterator().next();
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("FILTER_CENTRAL_WAVELENGTH",
                lambda, Units.ANGSTROM), CalculationResults.Level.FINAL);
        if (ConfigFactory.getConfig().getCentralPixelFlag()) {
            double cpSnr = output.getValue0().values().iterator().next();
            ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("CENTRAL_PIXEL_SIGNAL_TO_NOISE",
                    cpSnr, null), CalculationResults.Level.FINAL);
        }
        double snr = output.getValue1().values().iterator().next();
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("SIGNAL_TO_NOISE",
                snr, null), CalculationResults.Level.FINAL);
        if (ConfigFactory.getConfig().getCentralPixelFlag()) {
            double cpS = output.getValue2().values().iterator().next();
            ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("CENTRAL_PIXEL_TOTAL_SIGNAL",
                    cpS, Units.ELECTRONS), CalculationResults.Level.FINAL);
        }
        double s = output.getValue3().values().iterator().next();
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("TOTAL_SIGNAL",
                s, Units.ELECTRONS), CalculationResults.Level.FINAL);
        if (ConfigFactory.getConfig().getCentralPixelFlag()) {
            double cpBg = output.getValue4().values().iterator().next();
            ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("CENTRAL_PIXEL_TOTAL_BACKGROUND_NOISE",
                    cpBg, Units.ELECTRONS), CalculationResults.Level.FINAL);
        }
        double bg = output.getValue5().values().iterator().next();
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("TOTAL_BACKGROUND_NOISE",
                bg, Units.ELECTRONS), CalculationResults.Level.FINAL);
        if (ConfigFactory.getConfig().getCentralPixelFlag()) {
            double cpDet = output.getValue6().values().iterator().next();
            ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("CENTRAL_PIXEL_TOTAL_DELTECTOR_NOISE",
                    cpDet, Units.ELECTRONS), CalculationResults.Level.FINAL);
        }
        double det = output.getValue7().values().iterator().next();
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("TOTAL_DELTECTOR_NOISE",
                det, Units.ELECTRONS), CalculationResults.Level.FINAL);
    }

    /**
     * <p>For every calculator instantiation the following parts of its configuration
     * are added in the simulation results:</p>
     * <ul>
     *   <li>The extra signal in debug results with code EXTRA_SIGNAL</li>
     *   <li>The extra background in debug results with code EXTRA_BACKGROUND</li>
     *   <li>The dark current in debug results with code DARK_CURRENT</li>
     *   <li>The readout in debug results with code READOUT_NOISE</li>
     *   <li>The number of exposures in debug results with code NUMBER_OF_EXPOSURES</li>
     *   <li>The exposure time in debug results with code EXPOSURE_TIME</li>
     * </ul>
     * @param configuration The configuration of the calculator
     */
    @Override
    protected void performForEveryRetrieval(Octet<
            Calculator<Tuple, Pair<Double, Double>>,
            Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>,
                 Pair<Double, String>>,
            Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>,
                 Pair<Double, String>>, 
            Calculator<Unit<Double>, Unit<Double>>, Pair<Double, String>, 
            Pair<Double, String>, Integer, Pair<Double, String>> configuration) {
        Pair<Double, String> extraSignalPair = configuration.getValue1().getValue1();
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("EXTRA_SIGNAL", 
                extraSignalPair.getValue0(), extraSignalPair.getValue1()), CalculationResults.Level.DEBUG);
        Pair<Double, String> extraBackgroundPair = configuration.getValue2().getValue1();
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("EXTRA_BACKGROUND", 
                extraBackgroundPair.getValue0(), extraBackgroundPair.getValue1()), CalculationResults.Level.DEBUG);
        Pair<Double, String> darkPair = configuration.getValue4();
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("DARK_CURRENT", 
                darkPair.getValue0(), darkPair.getValue1()), CalculationResults.Level.DEBUG);
        Pair<Double, String> readoutPair = configuration.getValue5();
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("READOUT_NOISE", 
                readoutPair.getValue0(), readoutPair.getValue1()), CalculationResults.Level.DEBUG);
        ResultsHolder.getResults().addResult(new CalculationResults.LongValueResult("NUMBER_OF_EXPOSURES", 
                configuration.getValue6(), null), CalculationResults.Level.DEBUG);
        Pair<Double, String> exposureTimePair = configuration.getValue7();
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("EXPOSURE_TIME", 
                exposureTimePair.getValue0(), exposureTimePair.getValue1()), CalculationResults.Level.DEBUG);
    }
    
}
