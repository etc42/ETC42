/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.normalizationfactor;

import java.util.ResourceBundle;
import org.apache.commons.math.analysis.BivariateRealFunction;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.covolvedsurfacebrightnessprofile.ConvolvedSurfaceBrightnessProfileFactory;
import org.cnrs.lam.dis.etc.calculator.psf.PsfFactory;
import org.cnrs.lam.dis.etc.calculator.psfsize.PsfSizeFactory;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.StringResult;
import org.cnrs.lam.dis.etc.datamodel.Instrument;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.cnrs.lam.dis.etc.datamodel.Source;
import org.javatuples.*;

/**
 * <p>The {@link NormalizationFactorFactory} provides calculators for computing the
 * normalization factor.</p>
 */
public class NormalizationFactorFactory implements Factory<Unit<Session>,
        Tuple, Pair<UnivariateRealFunction, UnivariateRealFunction>> {

    private static final ResourceBundle bundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");

    @Override
    public Calculator<Tuple, Pair<UnivariateRealFunction, UnivariateRealFunction>> 
            getCalculator(Unit<Session> configuration) throws InitializationException, ConfigurationException {
        Session session = configuration.getValue0();
        Instrument instrument = session.getInstrument();
        Instrument.InstrumentType instrumentType = instrument.getInstrumentType();
        Instrument.SpectrographType spectographType = instrument.getSpectrographType();
        Source source = session.getSource();
        Source.SpatialDistributionType spatialType = source.getSpatialDistributionType();
        
        double pixelScale = instrument.getPixelScale();
        String pixelScaleUnit = instrument.getPixelScaleUnit();
        Pair<Double, String> pixelScalePair = new Pair<Double, String>(pixelScale, pixelScaleUnit);
        double slitWidth = instrument.getSlitWidth();
        String slitWidthUnit = instrument.getSlitWidthUnit();
        Pair<Double, String> slitWidthPair = new Pair<Double, String>(slitWidth, slitWidthUnit);
        double slitLength = instrument.getSlitLength();
        String slitLengthUnit = instrument.getSlitLengthUnit();
        Pair<Double, String> slitLengthPair = new Pair<Double, String>(slitLength, slitLengthUnit);
        double fiberDiameter = instrument.getFiberDiameter();
        String fiberDiameterUnit = instrument.getFiberDiameterUnit();
        Pair<Double, String> fiberDiameterPair = new Pair<Double, String>(fiberDiameter, fiberDiameterUnit);
        Calculator<Unit<Double>, Unit<Double>> psfSize = new PsfSizeFactory().getCalculator(configuration);
        
        if (spatialType == spatialType.POINT_SOURCE) {
            Calculator<Unit<Double>, Unit<BivariateRealFunction>> psf = new PsfFactory().getCalculator(configuration);
            if (instrumentType == instrumentType.SPECTROGRAPH && spectographType == spectographType.SLIT) {
                ResultsHolder.getResults().addResult(
                        new StringResult("NORMALIZATION_FACTOR_METHOD", "Point source / Slit"), Level.DEBUG);
                Quintet<Pair<Double, String>, Pair<Double, String>, Pair<Double, String>,
                        Calculator<Unit<Double>, Unit<BivariateRealFunction>>,
                        Calculator<Unit<Double>, Unit<Double>>> calculatorConfiguration =
                        new Quintet<Pair<Double, String>, Pair<Double, String>, Pair<Double, String>,
                        Calculator<Unit<Double>, Unit<BivariateRealFunction>>, 
                        Calculator<Unit<Double>, Unit<Double>>>(pixelScalePair, slitWidthPair, slitLengthPair, psf, psfSize);
                return EtcCalculatorManager.getManager(PointSourceSlit.class).getCalculator(calculatorConfiguration);
            } else if (instrumentType == instrumentType.SPECTROGRAPH && spectographType == spectographType.FIBER) {
                ResultsHolder.getResults().addResult(
                        new StringResult("NORMALIZATION_FACTOR_METHOD", "Point source / Fiber"), Level.DEBUG);
                 Quartet<Pair<Double, String>, Pair<Double, String>,
                        Calculator<Unit<Double>, Unit<BivariateRealFunction>>,
                        Calculator<Unit<Double>, Unit<Double>>> calculatorConfiguration =
                        new Quartet<Pair<Double, String>, Pair<Double, String>,
                        Calculator<Unit<Double>, Unit<BivariateRealFunction>>, 
                        Calculator<Unit<Double>, Unit<Double>>>(pixelScalePair, fiberDiameterPair, psf, psfSize);
                return EtcCalculatorManager.getManager(PointSourceFiber.class).getCalculator(calculatorConfiguration);
            } else {
                ResultsHolder.getResults().addResult(
                        new StringResult("NORMALIZATION_FACTOR_METHOD", "Point source / Imaging"), Level.DEBUG);
                Triplet<Pair<Double, String>, Calculator<Unit<Double>,
                        Unit<BivariateRealFunction>>, Calculator<Unit<Double>, Unit<Double>>>
                        calculatorConfiguration = new Triplet<Pair<Double, String>,
                        Calculator<Unit<Double>, Unit<BivariateRealFunction>>,
                        Calculator<Unit<Double>, Unit<Double>>>(pixelScalePair, psf, psfSize);
                return EtcCalculatorManager.getManager(PointSourceImaging.class).getCalculator(calculatorConfiguration);
            }
        }
        if (spatialType == spatialType.EXTENDED_SOURCE) {
            Calculator<Unit<Double>, Unit<BivariateRealFunction>> profileCalculator =
                    new ConvolvedSurfaceBrightnessProfileFactory().getCalculator(configuration);
            double sourceRadius = source.getExtendedSourceRadius();
            String sourceRadiusUnit = source.getExtendedSourceRadiusUnit();
            Pair<Double, String> sourceRadiusPair = new Pair<Double, String>(sourceRadius, sourceRadiusUnit);
            if (instrumentType == instrumentType.SPECTROGRAPH && spectographType == spectographType.SLIT) {
                ResultsHolder.getResults().addResult(
                        new StringResult("NORMALIZATION_FACTOR_METHOD", "Extended source / Slit"), Level.DEBUG);
                Sextet<Pair<Double, String>, Pair<Double, String>,
                        Calculator<Unit<Double>, Unit<BivariateRealFunction>>,
                        Calculator<Unit<Double>, Unit<Double>>,
                        Pair<Double, String>, Pair<Double, String>> calculatorConfiguration 
                        = new Sextet<Pair<Double, String>, Pair<Double, String>,
                        Calculator<Unit<Double>, Unit<BivariateRealFunction>>,
                        Calculator<Unit<Double>, Unit<Double>>,
                        Pair<Double, String>, Pair<Double, String>>
                        (pixelScalePair, sourceRadiusPair, profileCalculator, psfSize, slitWidthPair, slitLengthPair);
                return EtcCalculatorManager.getManager(ExtendedSourceSlit.class).getCalculator(calculatorConfiguration);
            } else if (instrumentType == instrumentType.SPECTROGRAPH && spectographType == spectographType.FIBER) {
                ResultsHolder.getResults().addResult(
                        new StringResult("NORMALIZATION_FACTOR_METHOD", "Extended source / Fiber"), Level.DEBUG);
                Quintet<Pair<Double, String>, Pair<Double, String>,
                        Calculator<Unit<Double>, Unit<BivariateRealFunction>>,
                        Calculator<Unit<Double>, Unit<Double>>, Pair<Double, String>> calculatorConfiguration
                        = new Quintet<Pair<Double, String>, Pair<Double, String>,
                        Calculator<Unit<Double>, Unit<BivariateRealFunction>>,
                        Calculator<Unit<Double>, Unit<Double>>, Pair<Double, String>>
                        (pixelScalePair, sourceRadiusPair, profileCalculator, psfSize, fiberDiameterPair);
                return EtcCalculatorManager.getManager(ExtendedSourceFiber.class).getCalculator(calculatorConfiguration);
            } else {
                ResultsHolder.getResults().addResult(
                        new StringResult("NORMALIZATION_FACTOR_METHOD", "Extended source / Imaging"), Level.DEBUG);
                Quartet<Pair<Double, String>, Pair<Double, String>,
                        Calculator<Unit<Double>, Unit<BivariateRealFunction>>,
                        Calculator<Unit<Double>, Unit<Double>>> calculatorConfiguration 
                        = new Quartet<Pair<Double, String>, Pair<Double, String>,
                        Calculator<Unit<Double>, Unit<BivariateRealFunction>>,
                        Calculator<Unit<Double>, Unit<Double>>>
                        (pixelScalePair, sourceRadiusPair, profileCalculator, psfSize);
                return EtcCalculatorManager.getManager(ExtendedSourceImaging.class).getCalculator(calculatorConfiguration);
            }
        }
        // If we are here means that we cannot create a calculator for this configuration
        String message = bundle.getString("UNKNOWN_NORMALIZATION_FACTOR_METHOD");
        throw new ConfigurationException(message);
    }
    
}
