/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator;

import java.util.List;
import org.cnrs.lam.dis.etc.datamodel.Instrument;
import org.cnrs.lam.dis.etc.datamodel.ObsParam;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.cnrs.lam.dis.etc.datamodel.Site;
import org.cnrs.lam.dis.etc.datamodel.Source;

/**
 *
 * @author nikoapos
 */
public interface Validator {

    /**
     * Validates the given session and returns a list with the validation
     * errors or an empty list if there are no validation errors. The validation
     * should include validation of the instrument, source, site and observing
     * parameters assigned to the session.
     * @param session The session to validate
     * @return A list with the validation errors
     */
    public List<String> validateSession(Session session);

    /**
     * Validates the given instrument and returns a list with the validation
     * errors or an empty list if there are no validation errors.
     * @param instrument The instrument to validate
     * @return A list with the validation errors
     */
    public List<String> validateInstrument(Instrument instrument);

    /**
     * Validates the given source and returns a list with the validation
     * errors or an empty list if there are no validation errors.
     * @param source The source to validate
     * @return A list with the validation errors
     */
    public List<String> validateSource(Source source);

    /**
     * Validates the given site and returns a list with the validation
     * errors or an empty list if there are no validation errors.
     * @param site The site to validate
     * @return A list with the validation errors
     */
    public List<String> validateSite(Site site);

    /**
     * Validates the given observing parameters and returns a list with the validation
     * errors or an empty list if there are no validation errors.
     * @param obsParam The observing parameters to validate
     * @return A list with the validation errors
     */
    public List<String> validateObsParam(ObsParam obsParam);
}
