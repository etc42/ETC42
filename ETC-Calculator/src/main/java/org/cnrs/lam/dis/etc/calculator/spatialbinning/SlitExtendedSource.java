/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.spatialbinning;

import java.text.MessageFormat;
import java.util.ResourceBundle;
import lombok.EqualsAndHashCode;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.javatuples.Septet;
import org.javatuples.Unit;

/**
 * <p>The {@code ExtendedSource} class is a calculator for computing the spatial
 * binning, in the case the source is an extended source and the instrument is
 * a slit spectrograph. The equation used for the calculation is:</p>
 * {@latex.inline $
 * SpatialBinning_{(\\lambda)} = \\displaystyle
 * \\frac{min\\left(l,\\sqrt{(2*r_{obj})^2+PSF_{size(\\lambda)}^2}\\right)}{p}
 * $}
 * <p>where</p>
 * <ul>
 *   <li>{@latex.inline $SpatialBinning$}: is the spatial binning in 
 *       {@latex.inline $pixels$}</li>
 *   <li>{@latex.inline $\\lambda$}: is the wavelength expressed in angstrom 
 *       ({@latex.inline \\AA})</li>
 *   <li>{@latex.inline $l$}: is the length of the slit expressed in
 *       ({@latex.inline $arcsec$})</li>
 *   <li>{@latex.inline $PSF_{size(\\lambda)}$}: is the size of the PSF in
 *       {@latex.inline $arcsec$}</li>
 *   <li>{@latex.inline $p$}: is the pixel scale of the instrument expressed in
 *       {@latex.inline $arcsec/pixel$}</li>
 *   <li>{@latex.inline $r_{obj}$}: is the effective radius of the target expressed in
 *       {@latex.inline $arcsec/pixel$}</li>
 * </ul>
 * 
 * <p>The configuration of the calculator is a {@link org.javatuples.Septet},
 * with contains the following elements:</p>
 * <ol>
 *   <li>{@link java.lang.Double}: The pixel scale of the instrument</li>
 *   <li>{@link java.lang.String}: The unit of the pixel scale of the instrument</li>
 *   <li>{@link org.cnrs.lam.cesam.util.calculator.Calculator}: a calculator
 *       for calculating the size of the PSF</li>
 *   <li>{@link java.lang.Double}: The effective radius of the target</li>
 *   <li>{@link java.lang.String}: The unit of the effective radius of the target</li>
 *   <li>{@link java.lang.Double}: The length of the slit</li>
 *   <li>{@link java.lang.String}: The unit of the length of the slit</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the wavelength expressed in {@latex.inline \\AA}.</p>
 * 
 * <p>The output of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the spatial binning, expressed in
 * {@latex.inline $pixels$}.</p>
 */
@EqualsAndHashCode(callSuper=false)
public class SlitExtendedSource extends AbstractCalculator<
        Septet<Double, String, Calculator<Unit<Double>, Unit<Double>>,
               Double, String, Double, String>,
        Unit<Double>, Unit<Double>> {
    
    private static final ResourceBundle validationErrorsBundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");
    private double pixelScale;
    private Calculator<Unit<Double>, Unit<Double>> psfCalculator;
    private double sourceRadius;
    private double slitLength;

    /**
     * Validates the given configuration. It requires that the first value of the
     * tuple (the pixel scale of the instrument) is a positive number, that the second
     * value (the unit of the pixel scale) is {@latex.inline $arcsec/pixel$}, that
     * the fourth value (the source radius) is a positive number, that the fifth
     * (the unit of the source radius) is {@latex.inline $arcsec$}, that
     * the sixth value (the slit length) is a positive number and that the seventh
     * value (the unit of the slit length) is {@latex.inline $arcsec$}.
     * @param configuration The configuration to validate
     * @throws ConfigurationException If the configuration is invalid
     */
    @Override
    protected void validateConfiguration(Septet<Double, String, Calculator<Unit<Double>, Unit<Double>>,
            Double, String, Double, String> configuration) throws ConfigurationException {
        // Check that the pixel scale is a positive number
        if (configuration.getValue0() <= 0) {
            String message = validationErrorsBundle.getString("PIXEL_SCALE_NOT_POSITIVE");
            message = MessageFormat.format(message, configuration.getValue0());
            throw new ConfigurationException(message);
        }
        // Check if the pixel scale is in arcsec/pixel
        if (!Units.getArcsecPerPixel().equals(configuration.getValue1())) {
            String message = validationErrorsBundle.getString("PIXEL_SCALE_WRONG_UNIT");
            message = MessageFormat.format(message, Units.getArcsecPerPixel(), configuration.getValue1());
            throw new ConfigurationException(message);
        }
        // Check that the source radius is a positive number
        if (configuration.getValue3() <= 0) {
            String message = validationErrorsBundle.getString("EXTENDED_SOURCE_RADIUS_NOT_POSITIVE");
            message = MessageFormat.format(message, configuration.getValue3());
            throw new ConfigurationException(message);
        }
        // Check if the source radius is in arcsec
        if (!Units.isArcsec(configuration.getValue4())) {
            String message = validationErrorsBundle.getString("EXTENDED_SOURCE_RADIUS_WRONG_UNIT");
            message = MessageFormat.format(message, Units.ARCSEC, configuration.getValue4());
            throw new ConfigurationException(message);
        }
        // Check that the slit length is a positive number
        if (configuration.getValue5() <= 0) {
            String message = validationErrorsBundle.getString("SLIT_LENGTH_NOT_POSITIVE");
            message = MessageFormat.format(message, configuration.getValue5());
            throw new ConfigurationException(message);
        }
        // Check if the slit length is in arcsec/pixel
        if (!Units.isArcsec(configuration.getValue6())) {
            String message = validationErrorsBundle.getString("SLIT_LENGTH_WRONG_UNIT");
            message = MessageFormat.format(message, Units.ARCSEC, configuration.getValue6());
            throw new ConfigurationException(message);
        }
    }

    /**
     * Initializes the calculator instance with the given configuration. The first
     * value of the configuration is the pixel scale of the instrument, the second is
     * the unit of the pixel scale, the third is a calculator for calculating the
     * FWHM of the PSF, the fourth is the source radius and the fifth the source
     * radius unit.
     * @param configuration the configuration to use for initialization
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(Septet<Double, String, Calculator<Unit<Double>, Unit<Double>>,
            Double, String, Double, String> configuration) throws InitializationException {
        pixelScale = configuration.getValue0();
        psfCalculator = configuration.getValue2();
        sourceRadius = configuration.getValue3();
        slitLength = configuration.getValue5();
    }

    /**
     * Returns a {@link org.javatuples.Unit} containing the Spatial Binning
     * expressed in {@latex.inline $pixels$}.
     * @param input The wavelength expressed in {@latex.inline \\AA}
     * @return The spatial binning expressed in {@latex.inline $pixels$}
     * @throws CalculationException if there is any error during the calculation
     */
    @Override
    protected Unit<Double> performCalculation(Unit<Double> input) throws CalculationException {
        double psfSize = psfCalculator.calculate(input).getValue0();
        double sizeInArcsec = Math.sqrt((4 * sourceRadius * sourceRadius) + (psfSize * psfSize));
        sizeInArcsec = Math.min(sizeInArcsec, slitLength);
        double result = sizeInArcsec / pixelScale;
        return new Unit<Double>(result);
    }

    /**
     * <p>For every calculation the calculator adds in the simulation results
     * the following outputs of the calculator:</p>
     * <ul>
     *   <li>The spatial binning in intermediate important results
     *       with code SPATIAL_BINNING</li>
     * </ul>
     * @param input
     * @param output 
     */
    @Override
    protected void performForEveryCalculation(Unit<Double> input, Unit<Double> output) {
        ResultsHolder.getResults().addResult("SPATIAL_BINNING", input.getValue0(),
                output.getValue0(), Units.ANGSTROM, Units.PIXEL, CalculationResults.Level.INTERMEDIATE_IMPORTANT);
    }

    /**
     * <p>For every calculator instantiation the following parts of its configuration
     * are added in the simulation results:</p>
     * <ul>
     *   <li>The pixel scale in debug results with code PIXEL_SCALE</li>
     *   <li>The extended source effective radius in debug results with code SOURCE_RADIUS</li>
     *   <li>The slit length in debug results with code SLIT_LENGTH</li>
     * </ul>
     * @param configuration The configuration of the calculator
     */
    @Override
    protected void performForEveryRetrieval(Septet<Double, String, Calculator<Unit<Double>, Unit<Double>>,
            Double, String, Double, String> configuration) {
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("PIXEL_SCALE",
                configuration.getValue0(), configuration.getValue1()), CalculationResults.Level.DEBUG);
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("SOURCE_RADIUS",
                configuration.getValue3(), configuration.getValue4()), CalculationResults.Level.DEBUG);
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("SLIT_LENGTH",
                configuration.getValue5(), configuration.getValue6()), CalculationResults.Level.DEBUG);
    }
    
}
