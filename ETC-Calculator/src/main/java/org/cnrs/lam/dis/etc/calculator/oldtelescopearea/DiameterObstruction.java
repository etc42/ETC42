/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.oldtelescopearea;

import lombok.EqualsAndHashCode;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.DoubleValueResult;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.javatuples.Triplet;
import org.javatuples.Unit;

/**
 * <p>The {@code DiameterObstruction} class is a calculator for computing the total
 * area of the primary mirror which reflects light. The equation used for the 
 * calculation is:</p>
 * {@latex.inline $
 * A_{tel} = \\pi * \\left(\\frac{D_1}{2} \\right)^2 * (1-ob)
 * $}
 * <p>where</p>
 * <ul>
 *   <li>{@latex.inline $A_{tel}$}: is the calculated telescope area, expressed
 *       in {@latex.inline $cm^2$}</li>
 *   <li>{@latex.inline $D_1$}: is the diameter of the primary mirror expressed 
 *       in centimeters ({@latex.inline $cm$})</li>
 *   <li>{@latex.inline $ob$}: is the total light obstruction (a number in the
 *       range [0,1])</li>
 * </ul>
 * 
 * <p>The configuration of the calculator is a {@link org.javatuples.Triplet},
 * which contains the following elements:</p>
 * <ol>
 *   <li>{@link java.lang.Double}: The diameter of the primary mirror</li>
 *   <li>{@link java.lang.String}: The unit of the diameter of the primary mirror</li>
 *   <li>{@link java.lang.Double}: The total obstruction</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the wavelength for which the calculation
 * is done (expressed in {@latex.inline \\AA}). Note that as this calculation
 * is not depended on the wavelength, the input is completely ignored.</p>
 * 
 * <p>The output of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the telescope area (expressed in
 * {@latex.inline $cm^2$}).</p>
 * 
 * @author Nikolaos Apostolakos
 */
@EqualsAndHashCode(callSuper = false, exclude={"telescopeArea"})
public class DiameterObstruction extends AbstractCalculator<Triplet<Double, String, Double>, Unit<Double>, Unit<Double>> {

    // We store the diameter and the obstruction values to be used from lombok
    // for the equals and the hashCode methods
    private double diameter;
    private double obstruction;
    // As the result will be the same for all the lambda, we do the calculation
    // only once and we store it here
    private Double telescopeArea;

    /**
     * Validates the given configuration. It requires that the first value of the
     * tuple (the diameter of the telescope) is a positive number, that the second
     * value (the unit of the diameter) is {@latex.inline $cm$} and that the
     * third (the obstruction) is between 0 and 1 (inclusive).
     * @param configuration The configuration to validate
     * @throws ConfigurationException If the configuration is invalid
     */
    @Override
    protected void validateConfiguration(Triplet<Double, String, Double> configuration) throws ConfigurationException {
        // Check that the diameter is a possitive number
        if (configuration.getValue0() <= 0) {
            throw new ConfigurationException("Mirror diameter must be a possitive "
                    + "number but was " + configuration.getValue0());
        }
        // Check if the mirror diameter is in cm
        if (!Units.isCm(configuration.getValue1())) {
            throw new ConfigurationException("Mirror diameter must be in " + Units.CM
                    + " but was in " + configuration.getValue1());
        }
        // Check that the obstruction is between 0 and 1
        if (configuration.getValue2() < 0 || configuration.getValue2() > 1) {
            throw new ConfigurationException("Obstruction must be between 0 and 1 "
                    + "but was " + configuration.getValue2());
        }
    }

    /**
     * Initializes the calculator instance with the given configuration. The first
     * value of the configuration is the diameter of the telescope, the second is
     * the unit of the diameter and the third is the obstruction.
     * @param configuration The configuration to use for initialization
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(Triplet<Double, String, Double> configuration) throws InitializationException {
        diameter = configuration.getValue0();
        obstruction = configuration.getValue2();
    }

    /**
     * Returns a {@link org.javatuples.Unit} containing the effective telescope
     * area, expressed in {@latex.inline $cm^2$}.
     * @param input The wavelength expressed in {@latex.inline \\AA}
     * @return The telescope area expressed in {@latex.inline $cm^2$}
     * @throws CalculationException  if there is any error during the calculation
     */
    @Override
    protected Unit<Double> performCalculation(Unit<Double> input) throws CalculationException {
        // Because the telescope area is not depended on the wavelength we calculate
        // it only the first time
        if (telescopeArea == null) {
            double radius = diameter / 2;
            telescopeArea = Math.PI * radius * radius * (1 - obstruction);
        }
        return new Unit<Double>(telescopeArea);
    }

    /**
     * Overridden to add in the results the primary mirror diameter and the obstruction.
     * This is done here, so we have these results even if the calculator is retrieved
     * from the cache.
     * @param configuration The configuration of the calculator
     */
    @Override
    protected void performForEveryRetrieval(Triplet<Double, String, Double> configuration) {
        ResultsHolder.getResults().addResult(new DoubleValueResult("PRIMARY_DIAMETER",
                configuration.getValue0(), configuration.getValue1()), Level.DEBUG);
        ResultsHolder.getResults().addResult(new DoubleValueResult("OBSTRUCTION",
                configuration.getValue2() * 100, "%"), Level.DEBUG);
    }

    /**
     * Overridden to add in the results the telescope area. This is done here, so
     * we have this results even if the result is retrieved from the cache.
     * @param input The wavelength expressed in {@latex.inline \\AA}
     * @param output The telescope area expressed in {@latex.inline $cm^2$}
     */
    @Override
    protected void performForEveryCalculation(Unit<Double> input, Unit<Double> output) {
        ResultsHolder.getResults().addResult(new DoubleValueResult("TELESCOPE_AREA", 
                output.getValue0(), Units.CM2), Level.INTERMEDIATE_IMPORTANT);
    }
}
