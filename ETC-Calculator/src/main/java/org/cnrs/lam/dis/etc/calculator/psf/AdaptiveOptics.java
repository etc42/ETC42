/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.psf;

import java.text.MessageFormat;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TreeMap;
import lombok.EqualsAndHashCode;
import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.analysis.BivariateRealFunction;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.apache.log4j.Logger;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.calculator.util.functions.SymmetricBivariateDoubleGaussian;
import org.cnrs.lam.dis.etc.calculator.util.functions.SymmetricBivariateGaussian;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.javatuples.Pair;
import org.javatuples.Quintet;
import org.javatuples.Unit;

/**
 * <p>The {@link AdaptiveOptics} class is a calculator for computing 
 * the total PSF in the case an Adaptive Optics system is in place.
 * The result of the calculation is a circularly symmetric bivariate double
 * Gaussian following the equation:</p>
 * {@latex.inline $
 * PSF_{AO(\\lambda)}=SR_{(\\lambda)}*PSF_{dl(\\lambda)}+(1-SR_{(\\lambda)})*PSF_{s(\\lambda)}
 * $}
 * <p>where</p>
 * <ul>
 *   <li>{@latex.inline $PSF_{AO(\\lambda)}$}: is the final adaptive optics PSF</li>
 *   <li>{@latex.inline $PSF_{dl(\\lambda)}$}: is the diffraction limited PSF</li>
 *   <li>{@latex.inline $PSF_{s(\\lambda)}$}: is the seeing limited PSF</li>
 *   <li>{@latex.inline $SR_{(\\lambda)}$}: is the strehl ratio
 *       {@latex.inline $SR_{(\\lambda)} = SR_{ref}^{\\left(\\frac{\\lambda_{ref}}{\\lambda}\\right)^2}$}</li>
 *   <li>{@latex.inline $SR_{ref}$}: is the reference strehl ratio</li>
 *   <li>{@latex.inline $\\lambda_{ref}$}: is the reference strehl ratio wavelength
 *       expressed in {@latex.inline \\AA}</li>
 *   <li>{@latex.inline $\\lambda$}: is the wavelength for which the calculation is done
 *       expressed in {@latex.inline \\AA}</li>
 * </ul>
 * 
 * <p>The configuration of the calculator is a {@link org.javatuples.Quintet},
 * with contains the following elements:</p>
 * <ol>
 *   <li>{@link Calculator}: A calculator for the diffraction limited PSF</li>
 *   <li>{@link Calculator}: A calculator for the seeing PSF</li>
 *   <li>{@link Double}: The reference strethl ratio</li>
 *   <li>{@link Double}: The strethl ratio wavelength</li>
 *   <li>{@link String}: The strethl ratio wavelength unit</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the wavelength for which the calculation
 * is done (expressed in {@latex.inline \\AA}).</p>
 * 
 * <p>The output of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link BivariateRealFunction} representing the PSF (both axes are expressed in
 * {@latex.inline $arcsec$}).</p>
 */
@Cacheable(CachingPolicy.INSTANCE)
@EqualsAndHashCode(callSuper=false)
public class AdaptiveOptics extends AbstractCalculator<
        Quintet<Calculator<Unit<Double>, Unit<BivariateRealFunction>>,
                Calculator<Unit<Double>, Unit<BivariateRealFunction>>,
                Double, Double, String>, 
        Unit<Double>, Unit<BivariateRealFunction>> {
    
    private static final Logger logger = Logger.getLogger(InstrumentPlusSeeing.class);
    private static final ResourceBundle validationErrorsBundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");
    private Calculator<Unit<Double>, Unit<BivariateRealFunction>> diffractionLimitedPsf;
    private Calculator<Unit<Double>, Unit<BivariateRealFunction>> seeingPsf;
    private double referenceStrehlRatio;
    private double referenceWavelength;

    /**
     * Checks that the reference strehl ratio is between 0 and 1, that the reference
     * wavelength is positive and that its unit is Angstrom.
     * @param configuration
     * @throws ConfigurationException 
     */
    @Override
    protected void validateConfiguration(Quintet<Calculator<Unit<Double>, Unit<BivariateRealFunction>>, 
                                              Calculator<Unit<Double>, Unit<BivariateRealFunction>>,
                                              Double, Double, String> configuration) throws ConfigurationException {
        // Check that the reference strehl ratio is between 0 and 1
        if (configuration.getValue2() < 0 || configuration.getValue2() >1) {
            String message = validationErrorsBundle.getString("STREHL_RATIO_OUT_OF_BOUNDS");
            message = MessageFormat.format(message, configuration.getValue2());
            throw new ConfigurationException(message);
        }
        // Check that the reference wavelength is a positive number
        if (configuration.getValue3() <= 0) {
            String message = validationErrorsBundle.getString("AO_WAVELENGTH_NOT_POSITIVE");
            message = MessageFormat.format(message, configuration.getValue3());
            throw new ConfigurationException(message);
        }
        // Check if the reference wavelength is in Angstrom
        if (!Units.isAngstrom(configuration.getValue4())) {
            String message = validationErrorsBundle.getString("AO_WAVELENGTH_WRONG_UNIT");
            message = MessageFormat.format(message, Units.ANGSTROM, configuration.getValue4());
            throw new ConfigurationException(message);
        }
    }

    /**
     * Initializes the calculator instance with the given configuration. The first
     * value of the configuration is the calculator for the diffraction limited PSF, the
     * second one is the calculator for the seeing PSF, the third the reference SR,
     * the fourth the reference wavelength and the fifth the wavelengths unit.
     * @param configuration The configuration of the calculator
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(Quintet<Calculator<Unit<Double>, Unit<BivariateRealFunction>>, 
                                    Calculator<Unit<Double>, Unit<BivariateRealFunction>>,
                                    Double, Double, String> configuration) throws InitializationException {
        diffractionLimitedPsf = configuration.getValue0();
        seeingPsf = configuration.getValue1();
        referenceStrehlRatio = configuration.getValue2();
        referenceWavelength = configuration.getValue3();
    }

    /**
     * Returns a {@link org.javatuples.Unit} containing a bivariate function, of
     * which both axes are expressed in {@latex.inline $arcsec$}, which represents
     * the PSF for the given wavelength.
     * @param input The wavelength expressed in {@latex.inline \\AA}
     * @return A function representing the PSF
     * @throws CalculationException if there is any error during the calculation
     */
    @Override
    protected Unit<BivariateRealFunction> performCalculation(Unit<Double> input) throws CalculationException {
        double lambda = input.getValue0();
        // Calculate the strehl ratio for the given wavelngth
        double lambdaRatio = referenceWavelength / lambda;
        double strehlRatio = Math.pow(referenceStrehlRatio, lambdaRatio * lambdaRatio);
        // We know that both the diffraction limited and seeing PSFs are implemented as
        // bivariate gaussians.
        SymmetricBivariateGaussian diffractionLimitedFunction =
                (SymmetricBivariateGaussian) diffractionLimitedPsf.calculate(input).getValue0();
        SymmetricBivariateGaussian seeingFunction =
                (SymmetricBivariateGaussian) seeingPsf.calculate(input).getValue0();
        double diffractionLimitedSigma = diffractionLimitedFunction.getStandardDeviation();
        double seeingSigma = seeingFunction.getStandardDeviation();
        SymmetricBivariateDoubleGaussian aoPsf = new SymmetricBivariateDoubleGaussian(diffractionLimitedSigma, seeingSigma, strehlRatio);
        return new Unit<BivariateRealFunction>(aoPsf);
    }

    /**
     * <p>For every calculation the calculator adds in the simulation results
     * the following outputs of the calculator:</p>
     * <ul>
     *   <li> The Strehl Ratio in the intermediate
     *       unimportant results with code STREHL_RATIO</li>
     *   <li> The projection of the PSF on the plane y=0 in the intermediate
     *       unimportant results with code TOTAL_PSF_PROJECTION</li>
     * </ul>
     * @param input
     * @param output 
     */
    @Override
    protected void performForEveryCalculation(Unit<Double> input, Unit<BivariateRealFunction> output) {
        double lambda = input.getValue0();
        //We know that we have a symmetric bivariate double gaussian so the casting is safe
        SymmetricBivariateDoubleGaussian doubleGaussian = (SymmetricBivariateDoubleGaussian) output.getValue0();
        
        // We add the Strehl Ratio in the results
        double strehlRatio = doubleGaussian.getWeight();
        ResultsHolder.getResults().addResult("STREHL_RATIO", lambda, strehlRatio, 
                Units.ANGSTROM, Units.ARCSEC, CalculationResults.Level.INTERMEDIATE_UNIMPORTANT);
        
        // We add the projection on the y = 0 plane in the results. We calculate
        // the values in the range of 5 maxSigma, as all the values outside of this
        // are too close to zero.
        Pair<Double, Double> standardDeviations = doubleGaussian.getStandardDeviations();
        double maxSigma = Math.max(standardDeviations.getValue0(), standardDeviations.getValue1());
        double minSigma = Math.min(standardDeviations.getValue0(), standardDeviations.getValue1());
        UnivariateRealFunction projetion = doubleGaussian.projectionFunction();
        Map<Double, Double> projectionData = new TreeMap<Double, Double>();
        // We will try to create at least 100 points inside the smaller Gaussian
        // but we will not exceed the 1000 points in total
        double step =  Math.max(maxSigma / 200., minSigma / 20.);
        for (double x = -5 * maxSigma; x <= 5 * maxSigma; x += step) {
            double value = 0;
            try {
                value = projetion.value(x);
            } catch (FunctionEvaluationException ex) {
                logger.warn("Failed to calculate the value of the PSF projection", ex);
            }
            projectionData.put(x, value);
        }
        CalculationResults.DoubleDatasetResult projectionResult = 
                new CalculationResults.DoubleDatasetResult("TOTAL_PSF_PROJECTION", projectionData, Units.ARCSEC, null);
        ResultsHolder.getResults().addResult("TOTAL_PSF_PROJECTION", lambda, projectionResult,
                Units.ANGSTROM, CalculationResults.Level.INTERMEDIATE_UNIMPORTANT);
    }

    /**
     * <p>For every calculator instantiation the following parts of its configuration
     * are added in the simulation results:</p>
     * <ul>
     *   <li>The reference strehl ratio in debug results with code REFERENCE_STREHL_RATIO</li>
     *   <li>The reference strehl ratio wavelength in debug results with code REFERENCE_STREHL_RATIO_WAVELENGTH</li>
     * </ul>
     * @param configuration The configuration of the calculator
     */
    @Override
    protected void performForEveryRetrieval(Quintet<Calculator<Unit<Double>, Unit<BivariateRealFunction>>, 
                                    Calculator<Unit<Double>, Unit<BivariateRealFunction>>,
                                    Double, Double, String> configuration) {
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("REFERENCE_STREHL_RATIO",
                configuration.getValue2(), null), CalculationResults.Level.DEBUG);
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("REFERENCE_STREHL_RATIO_WAVELENGTH",
                configuration.getValue3(), configuration.getValue4()), CalculationResults.Level.DEBUG);
    }
    
}
