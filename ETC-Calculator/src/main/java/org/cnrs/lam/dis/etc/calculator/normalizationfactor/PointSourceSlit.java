/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.normalizationfactor;

import java.text.MessageFormat;
import java.util.ResourceBundle;
import lombok.EqualsAndHashCode;
import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.analysis.BivariateRealFunction;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.apache.log4j.Logger;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.FunctionToDatasetResultConverter;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.calculator.util.integration.IntegrationTool;
import org.cnrs.lam.dis.etc.configuration.ConfigFactory;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.javatuples.Pair;
import org.javatuples.Quintet;
import org.javatuples.Tuple;
import org.javatuples.Unit;

/**
 * <p> The {@link PointSourceSlit} class is a calculator for computing the central
 * pixel and total normalization factors. For point sources and slit spectrograph
 * the total normalization factor is calculated as:</p>
 * {@latex.inline $
 * C_{t(\\lambda)}=\\int\\limits_{y_1}^{y_2}\\int\\limits_{x_1}^{x_2}PSF_{(x,y;\\lambda)}dxdy
 * $}
 * <p>where</p>
 * <ul>
 *   <li>{@latex.inline $C_{t(\\lambda)}$}: is the total normalization factor</li>
 *   <li>{@latex.inline $\\lambda$}: is the wavelength expressed in {@latex.inline \\AA}</li>
 *   <li>{@latex.inline $PSF_{(x,y;\\lambda)}$}: is the function representing the PSF</li>
 *   <li>{@latex.inline $x_2=min(\\frac{PSF_{size}}{2},\\frac{w}{2})$}</li>
 *   <li>{@latex.inline $x_1=-x_2$}</li>
 *   <li>{@latex.inline $y_2=min(\\frac{PSF_{size}}{2},\\frac{l}{2})$}</li>
 *   <li>{@latex.inline $y_1=-y_2$}</li>
 *   <li>{@latex.inline $PSF_{size}$}: is the size of the PSF</li>
 *   <li>{@latex.inline $w$}: is the width of the slit expressed in {@latex.inline $arcsec$}</li>
 *   <li>{@latex.inline $l$}: is the length of the slit expressed in {@latex.inline $arcsec$}</li>
 * </ul>
 * <p>The central pixel normalization factor is calculated as:</p>
 * {@latex.inline $
 * C_{cp(\\lambda)}=\\int\\limits_{y_1}^{y_2}\\int\\limits_{x_1}^{x_2}PSF_{(x,y;\\lambda)}dxdy
 * $}
 * <p>where</p>
 * <ul>
 *   <li>{@latex.inline $C_{cp(\\lambda)}$}: is the central pixel normalization factor</li>
 *   <li>{@latex.inline $\\lambda$}: is the wavelength expressed in {@latex.inline \\AA}</li>
 *   <li>{@latex.inline $PSF_{(x,y;\\lambda)}$}: is the function representing the PSF</li>
 *   <li>{@latex.inline $x_2=min(\\frac{PSF_{size}}{2},\\frac{w}{2})$}</li>
 *   <li>{@latex.inline $x_1=-x_2$}</li>
 *   <li>{@latex.inline $y_2=min(\\frac{PSF_{size}}{2},\\frac{l}{2}, \\frac{p}{2})$}</li>
 *   <li>{@latex.inline $y_1=-y_2$}</li>
 *   <li>{@latex.inline $PSF_{size}$}: is the size of the PSF</li>
 *   <li>{@latex.inline $w$}: is the width of the slit expressed in {@latex.inline $arcsec$}</li>
 *   <li>{@latex.inline $l$}: is the length of the slit expressed in {@latex.inline $arcsec$}</li>
 *   <li>{@latex.inline $p$}: is the pixel scale of the slit expressed in {@latex.inline $arcsec/pixel$}</li>
 * </ul>
 * 
 * <p>The configuration of the calculator is a {@link Quintet},
 * witch contains the following elements:</p>
 * <ol>
 *   <li>{@link Pair}: The pixel scale of the instrument and its unit (in this order)</li>
 *   <li>{@link Pair}: The slit width and its unit (in this order)</li>
 *   <li>{@link Pair}: The slit length and its unit (in this order)</li>
 *   <li>{@link Calculator}: A calculator for computing the PSF functions</li>
 *   <li>{@link Calculator}: A calculator for computing the PSF size</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Tuple}, as this
 * calculator does not accept any input. It should always be null.</p>
 * 
 * <p>The output of the calculator is a {@link Pair} which contains the following:</p>
 * <ol>
 *   <li>{@link UnivariateRealFunction}: A function for computing the central
 *       pixel normalization factor</li>
 *   <li>{@link UnivariateRealFunction}: A function for computing the total
 *       normalization factor</li>
 * </ol>
 */
// Caching explenation
// -------------------
// This calculator is set to cache the instances and the results because the
// calculation of the results is done by a time consuming two dimentional integration
@Cacheable(CachingPolicy.ALL)
@EqualsAndHashCode(callSuper=false)
public class PointSourceSlit extends AbstractCalculator<
        Quintet<Pair<Double, String>, Pair<Double, String>, Pair<Double, String>,
                Calculator<Unit<Double>, Unit<BivariateRealFunction>>,
                Calculator<Unit<Double>, Unit<Double>>>,
        Tuple, Pair<UnivariateRealFunction, UnivariateRealFunction>> {
    
    private static Logger logger = Logger.getLogger(PointSourceSlit.class);
    private static final ResourceBundle validationErrorsBundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");
    private double pixelScale;
    private double slitWidth;
    private double slitLength;
    private Calculator<Unit<Double>, Unit<BivariateRealFunction>> psfCalculator;
    private Calculator<Unit<Double>, Unit<Double>> psfSizeCalculator;

    /**
     * validates the given configuration. It requires that the pixel scale of the
     * instrument is a positive number, that its unit is {@latex.inline $arcsec/pixel$},
     * that the slit width and length are positive numbers and that their units
     * are {$latex.inline $arcsec$}. 
     * @param configuration The configuration to validate
     * @throws ConfigurationException if the configuration is invalid
     */
    @Override
    protected void validateConfiguration(Quintet<Pair<Double, String>, Pair<Double, String>,
            Pair<Double, String>, Calculator<Unit<Double>, Unit<BivariateRealFunction>>,
            Calculator<Unit<Double>, Unit<Double>>> configuration) throws ConfigurationException {
        // Check that the pixel scale is a positive number
        if (configuration.getValue0().getValue0() <= 0) {
            String message = validationErrorsBundle.getString("PIXEL_SCALE_NOT_POSITIVE");
            message = MessageFormat.format(message, configuration.getValue0().getValue0());
            throw new ConfigurationException(message);
        }
        // Check if the pixel scale is in arcsec/pixel
        if (!Units.getArcsecPerPixel().equals(configuration.getValue0().getValue1())) {
            String message = validationErrorsBundle.getString("PIXEL_SCALE_WRONG_UNIT");
            message = MessageFormat.format(message, Units.getArcsecPerPixel(), configuration.getValue0().getValue1());
            throw new ConfigurationException(message);
        }
        // Check that the slit width is a positive number
        if (configuration.getValue1().getValue0() <= 0) {
            String message = validationErrorsBundle.getString("SLIT_WIDTH_NOT_POSITIVE");
            message = MessageFormat.format(message, configuration.getValue1().getValue0());
            throw new ConfigurationException(message);
        }
        // Check if the slit width is in arcsec
        if (!Units.isArcsec(configuration.getValue1().getValue1())) {
            String message = validationErrorsBundle.getString("SLIT_WIDTH_WRONG_UNIT");
            message = MessageFormat.format(message, Units.ARCSEC, configuration.getValue1().getValue1());
            throw new ConfigurationException(message);
        }
        // Check that the slit length is a positive number
        if (configuration.getValue2().getValue0() <= 0) {
            String message = validationErrorsBundle.getString("SLIT_LENGTH_NOT_POSITIVE");
            message = MessageFormat.format(message, configuration.getValue2().getValue0());
            throw new ConfigurationException(message);
        }
        // Check if the slit length is in arcsec
        if (!Units.isArcsec(configuration.getValue2().getValue1())) {
            String message = validationErrorsBundle.getString("SLIT_LENGTH_WRONG_UNIT");
            message = MessageFormat.format(message, Units.ARCSEC, configuration.getValue2().getValue1());
            throw new ConfigurationException(message);
        }
    }

    /**
     * Initializes the calculator instance with the given configuration. The first
     * value of the configuration is a {@link Pair} containing the pixel scale and
     * its unit, the second is a {@link Pair} containing the slit width and its
     * unit, the third is a {@link Pair} containing the slit length and its
     * unit, the fourth is a calculator for computing the PSF function and the fifth
     * is a calculator for computing the PSF size.
     * @param configuration
     * @throws InitializationException 
     */
    @Override
    protected void initialize(Quintet<Pair<Double, String>, Pair<Double, String>,
            Pair<Double, String>, Calculator<Unit<Double>, Unit<BivariateRealFunction>>, 
            Calculator<Unit<Double>, Unit<Double>>> configuration) throws InitializationException {
        pixelScale = configuration.getValue0().getValue0();
        slitWidth = configuration.getValue1().getValue0();
        slitLength = configuration.getValue2().getValue0();
        psfCalculator = configuration.getValue3();
        psfSizeCalculator = configuration.getValue4();
    }

    /**
     * Returns a {@link Pair} containing the functions to compute the central pixel
     * and total normalization factors (in this order).
     * @param input
     * @return The central pixel and total normalization factor functions
     * @throws CalculationException if there is any error during the calculation
     */
    @Override
    protected Pair<UnivariateRealFunction, UnivariateRealFunction>
            performCalculation(Tuple input) throws CalculationException {
        CentralPixelFunction centralPixelFunction =
                ConfigFactory.getConfig().getCentralPixelFlag()
                ? new CentralPixelFunction() : null;
        return new Pair<UnivariateRealFunction, UnivariateRealFunction>
                (centralPixelFunction, new TotalFunction());
    }
    
    private class TotalFunction implements UnivariateRealFunction {

        @Override
        public double value(double lambda) throws FunctionEvaluationException {
            // Calculate the total normalization factor. If the psf fits in the
            // slit then it is one.
            double totalFactor = 1.;
            try {
                double psfSize = psfSizeCalculator.calculate(new Unit<Double>(lambda)).getValue0();
                if (psfSize > slitWidth || psfSize > slitLength) {
                    BivariateRealFunction psf = psfCalculator.calculate(new Unit<Double>(lambda)).getValue0();
                    double x2 = Math.min(psfSize / 2., slitWidth / 2.);
                    double x1 = -x2;
                    double y2 = Math.min(psfSize / 2., slitLength / 2.);
                    double y1 = -y2;
                    totalFactor = IntegrationTool.bivariateIntegral(psf, x1, x2, y1, y2);
                }
            } catch (CalculationException ex) {
                logger.error(ex.getMessage(), ex);
                throw new FunctionEvaluationException(ex, lambda);
            }
            return totalFactor;
        }
        
    }
    
    private class CentralPixelFunction implements UnivariateRealFunction {

        @Override
        public double value(double lambda) throws FunctionEvaluationException {
            // Calculate the central pixel normalization factor. If the psf fits
            // in the slit and in the central pixel then it is one.
            double centralPixelFactor = 1.;
            try {
                double psfSize = psfSizeCalculator.calculate(new Unit<Double>(lambda)).getValue0();
                if (psfSize > pixelScale || psfSize > slitWidth || psfSize > slitLength) {
                    BivariateRealFunction psf = psfCalculator.calculate(new Unit<Double>(lambda)).getValue0();
                    double x2 = Math.min(psfSize / 2., slitWidth / 2.);
                    double x1 = -x2;
                    double y2 = Math.min(psfSize / 2., slitLength / 2.);
                    y2 = Math.min(y2, pixelScale / 2.);
                    double y1 = -y2;
                    centralPixelFactor = IntegrationTool.bivariateIntegral(psf, x1, x2, y1, y2);
                }
            } catch (CalculationException ex) {
                logger.error(ex.getMessage(), ex);
                throw new FunctionEvaluationException(ex, lambda);
            }
            return centralPixelFactor;
        }
        
    }

    /**
     * <p>For every calculation the calculator adds in the simulation results
     * the following outputs of the calculator:</p>
     * <ul>
     *   <li>The central pixel normalization factor in intermediate important results
     *       with code CENTRAL_PIXEL_NORM_FACTOR</li>
     *   <li>The total normalization factor in intermediate important results
     *       with code TOTAL_NORMALIZATION_FACTOR</li>
     * </ul>
     * @param input
     * @param output 
     */
    @Override
    protected void performForEveryCalculation(Tuple input, Pair<UnivariateRealFunction, UnivariateRealFunction> output) {
        if (ConfigFactory.getConfig().getCentralPixelFlag()) {
            CalculationResults.DoubleDatasetResult cpNormFact = FunctionToDatasetResultConverter.convert
                    (output.getValue0(), "CENTRAL_PIXEL_NORM_FACTOR", Units.ANGSTROM, null);
            ResultsHolder.getResults().addResult(cpNormFact, CalculationResults.Level.INTERMEDIATE_IMPORTANT);
        }
        CalculationResults.DoubleDatasetResult totalNormFact = FunctionToDatasetResultConverter.convert
                (output.getValue1(), "TOTAL_NORMALIZATION_FACTOR", Units.ANGSTROM, null);
        ResultsHolder.getResults().addResult(totalNormFact, CalculationResults.Level.INTERMEDIATE_IMPORTANT);
    }

    /**
     * <p>For every calculator instantiation the following parts of its configuration
     * are added in the simulation results:</p>
     * <ul>
     *   <li>The pixel scale in debug results with code PIXEL_SCALE</li>
     *   <li>The slit width in debug results with code SLIT_WIDTH</li>
     *   <li>The slit length in debug results with code SLIT_LENGTH</li>
     * </ul>
     * @param configuration The configuration of the calculator
     */
    @Override
    protected void performForEveryRetrieval(Quintet<Pair<Double, String>, Pair<Double, String>, Pair<Double, String>, Calculator<Unit<Double>, Unit<BivariateRealFunction>>, Calculator<Unit<Double>, Unit<Double>>> configuration) {
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("PIXEL_SCALE",
                configuration.getValue0().getValue0(), configuration.getValue0().getValue1()),
                CalculationResults.Level.DEBUG);
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("SLIT_WIDTH",
                configuration.getValue1().getValue0(), configuration.getValue1().getValue1()),
                CalculationResults.Level.DEBUG);
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("SLIT_LENGTH",
                configuration.getValue2().getValue0(), configuration.getValue2().getValue1()),
                CalculationResults.Level.DEBUG);
    }
    
}
