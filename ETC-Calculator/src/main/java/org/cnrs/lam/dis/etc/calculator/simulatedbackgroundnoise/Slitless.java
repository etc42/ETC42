/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.simulatedbackgroundnoise;

import java.text.MessageFormat;
import java.util.ResourceBundle;
import lombok.EqualsAndHashCode;
import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.apache.log4j.Logger;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.Cacheable;
import org.cnrs.lam.cesam.util.calculator.CachingPolicy;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.Calculator;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Constants;
import org.cnrs.lam.dis.etc.calculator.util.FunctionToDatasetResultConverter;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.calculator.util.functionmultiplication.FunctionBoundsTooSmallException;
import org.cnrs.lam.dis.etc.calculator.util.functionmultiplication.FunctionMultiplicationTool;
import org.cnrs.lam.dis.etc.calculator.util.functions.AveragingKernelConvolution;
import org.cnrs.lam.dis.etc.calculator.util.functions.BoundedFunctionWrapper;
import org.cnrs.lam.dis.etc.calculator.util.functions.BoundedUnivariateFunction;
import org.cnrs.lam.dis.etc.calculator.util.functions.CalculatorWrapperFunction;
import org.cnrs.lam.dis.etc.calculator.util.functions.PolynomialFunction;
import org.cnrs.lam.dis.etc.calculator.util.integration.IntegrationTool;
import org.cnrs.lam.dis.etc.configuration.ConfigFactory;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.javatuples.Ennead;
import org.javatuples.Pair;
import org.javatuples.Tuple;
import org.javatuples.Unit;

/**
 * <p>The {@link Zero} class is a calculator for computing the per Angstrom
 * number of electrons counted by the instrument from the simulated background.
 * In this case the all the spectrum range of the background contributes to
 * each point of the CCD. Because of that the background noise is calculated
 * using the equation:</p>
 * {@latex.inline $
 * BN_{(\\lambda)} = Factor * \\int\\limits_{min}^{max} \\phi_{(\\lambda)} *
 * \\varepsilon_{(\\lambda)} * Filter_{(\\lambda)} * A_{tel} *
 * \\frac{\\lambda}{h*c} \\,\\,\\mathrm{d}\\lambda
 * $}
 * <p>where</p>
 * <ul>
 *   <li>{@latex.inline $BN_{(\\lambda)}$}: is the calculated background expressed in
 *       {@latex.inline $e_-/s/$\\AA}</li>
 *   <li>{@latex.inline $\\lambda$}: is the wavelength</li>
 *   <li>{@latex.inline $\\phi_{(\\lambda)}$}: is the simulated background flux</li>
 *   <li>{@latex.inline $\\varepsilon_{(\\lambda)}$}: is the total system efficiency
 *       excluding the filter</li>
 *   <li>{@latex.inline $Filter_{(\\lambda)}$}: is the filter response</li>
 *   <li>{@latex.inline $A_{tel}$}: is the effective area of the primary mirror</li>
 *   <li>{@latex.inline $h$}: is the Planck constant expressed in {@latex.inline $erg*s$}</li>
 *   <li>{@latex.inline $c$}: is the speed of light expressed in {@latex.inline \\AA$/sec$}</li>
 * </ul>
 * 
 * <p>The Factor parameter contains the area information and converts the calculation
 * to per Anstrom. It differs for the central pixel and for the total spatial binning
 * and it is calculated as:</p>
 * {@latex.inline $
 * Factor=\\left[\\begin{array}{l l}
 * \\frac{N_{pix(\\lambda)} * p^2}{\\Delta\\lambda_{(\\lambda)}} & $, for the total spatial binning$\\\\
 * p^2*\\frac{R_{pix(\\lambda)}}{\\lambda} & $, for the central pixel$
 * \\end{array}\\right.
 * $}
 * <p>where</p>
 * <ul>
 *   <li>{@latex.inline $N_{pix(\\lambda)}$}: is the number of affected pixels</li>
 *   <li>{@latex.inline $p$}: is the pixel scale</li>
 *   <li>{@latex.inline $\\Delta\\lambda_{(\\lambda)}$}: is the wavelength step
 *       of the calculation</li>
 *   <li>{@latex.inline $R_{pix(\\lambda)}$}: is the spectral
 *       resolution per pixel</li>
 * </ul>
 * 
 * <p>The configuration of the calculator is an {@link Ennead},
 * witch contains the following elements:</p>
 * <ol>
 *   <li>{@link Calculator}: A calculator computing the size of the convolution
 *       kernel in the spectral direction</li>
 *   <li>{@link Calculator}: A calculator computing the simulated background flux</li>
 *   <li>{@link Calculator}: A calculator computing the total system efficiency</li>
 *   <li>{@link Calculator}: A calculator computing the filter response</li>
 *   <li>{@link Calculator}: A calculator computing the telescope area</li>
 *   <li>{@link Calculator}: A calculator computing the number of pixels</li>
 *   <li>{@link Pair}: The pixel scale and its unit</li>
 *   <li>{@link Calculator}: A calculator computing the wavelength step of the
 *       calculation (delta lambda)</li>
 *   <li>{@link Calculator}: A calculator computing the spectral resolution
 *       per pixel</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Pair} which contains
 * two {@link Double} values representing the wavelength range (expressed in
 * {@latex.inline \\AA}) for which the returned calculators perform the calculation.</p>
 * 
 * <p>The output of the calculator is a {@link Pair} which contains two 
 * {@link BoundedUnivariateFunction} for computing the background in the central 
 * pixel and the total spatial binning accordingly.</p>
 */
// Caching explenation
// -------------------
// This calculator is set to cache the instances and the results because the
// calculation of the results contains time consuming integrations
@Cacheable(CachingPolicy.ALL)
@EqualsAndHashCode(callSuper=false, of={"convolutionKernelSizeCalculator","backgroundFluxCalculator",
    "systemEfficiencyCalculator","filterResponseCalculator", "telescopeAreaCalculator",
    "numberOfPixelsCalculator","pixelScale","deltaLambdaCalculator","spectralResolutionCalculator"})
public class Slitless extends AbstractCalculator<
        Ennead<Calculator<Unit<Double>, Unit<Double>>,
               Calculator<Tuple, Unit<UnivariateRealFunction>>,
               Calculator<Tuple, Unit<BoundedUnivariateFunction>>,
               Calculator<Tuple, Unit<BoundedUnivariateFunction>>,
               Calculator<Tuple, Unit<Double>>,
               Calculator<Unit<Double>, Unit<Double>>,
               Pair<Double, String>,
               Calculator<Unit<Double>, Unit<Double>>,
               Calculator<Tuple, Unit<UnivariateRealFunction>>>,
        Pair<Double, Double>,
        Pair<UnivariateRealFunction, UnivariateRealFunction>> {
    
    private static final ResourceBundle validationErrorsBundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");
    private final static Logger logger = Logger.getLogger(Slitless.class);

    // The calculators which are used by lombok for the equals method
    Calculator<Unit<Double>, Unit<Double>> convolutionKernelSizeCalculator;
    Calculator<Tuple, Unit<UnivariateRealFunction>> backgroundFluxCalculator;
    Calculator<Tuple, Unit<BoundedUnivariateFunction>> systemEfficiencyCalculator;
    Calculator<Tuple, Unit<BoundedUnivariateFunction>> filterResponseCalculator;
    Calculator<Tuple, Unit<Double>> telescopeAreaCalculator;
    Calculator<Unit<Double>, Unit<Double>> numberOfPixelsCalculator;
    double pixelScale;
    Calculator<Unit<Double>, Unit<Double>> deltaLambdaCalculator;
    Calculator<Tuple, Unit<UnivariateRealFunction>> spectralResolutionCalculator;
    // The functions retrieved during initialization and used for the calculation
    UnivariateRealFunction spectralResolutionFunction;
    double constantIntegral;

    /**
     * Validates the configuration of the calculator
     * @param configuration
     * @throws ConfigurationException 
     */
    @Override
    protected void validateConfiguration(Ennead<Calculator<Unit<Double>, Unit<Double>>,
               Calculator<Tuple, Unit<UnivariateRealFunction>>,
               Calculator<Tuple, Unit<BoundedUnivariateFunction>>,
               Calculator<Tuple, Unit<BoundedUnivariateFunction>>,
               Calculator<Tuple, Unit<Double>>,
               Calculator<Unit<Double>, Unit<Double>>,
               Pair<Double, String>,
               Calculator<Unit<Double>, Unit<Double>>,
               Calculator<Tuple, Unit<UnivariateRealFunction>>> configuration) throws ConfigurationException {
        // Check that the pixel scale is a positive number
        if (configuration.getValue6().getValue0() <= 0) {
            String message = validationErrorsBundle.getString("PIXEL_SCALE_NOT_POSITIVE");
            message = MessageFormat.format(message, configuration.getValue6().getValue0());
            throw new ConfigurationException(message);
        }
        // Check if the pixel scale is in arcsec/pixel
        if (!Units.getArcsecPerPixel().equals(configuration.getValue6().getValue1())) {
            String message = validationErrorsBundle.getString("PIXEL_SCALE_WRONG_UNIT");
            message = MessageFormat.format(message, Units.getArcsecPerPixel(), configuration.getValue6().getValue1());
            throw new ConfigurationException(message);
        }
    }

    /**
     * Initializes the calculator instance with the given configuration.
     * @param configuration the configuration to use for initialization
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(Ennead<Calculator<Unit<Double>, Unit<Double>>,
               Calculator<Tuple, Unit<UnivariateRealFunction>>,
               Calculator<Tuple, Unit<BoundedUnivariateFunction>>,
               Calculator<Tuple, Unit<BoundedUnivariateFunction>>,
               Calculator<Tuple, Unit<Double>>,
               Calculator<Unit<Double>, Unit<Double>>,
               Pair<Double, String>,
               Calculator<Unit<Double>, Unit<Double>>,
               Calculator<Tuple, Unit<UnivariateRealFunction>>> configuration) throws InitializationException {
    	System.out.println("convolutionKernelSizeCalculator");
        // We get the convolution kernel calculator and we create a function to use for the convolution
        convolutionKernelSizeCalculator = configuration.getValue0();
        System.out.println("convolutionKernelSizeFunction");
        UnivariateRealFunction convolutionKernelSizeFunction = new CalculatorWrapperFunction(convolutionKernelSizeCalculator);
        // We retrieve the flux function
        System.out.println("backgroundFluxCalculator");
        backgroundFluxCalculator = configuration.getValue1();
        UnivariateRealFunction fluxFunction = null;
        try {
            fluxFunction = backgroundFluxCalculator.calculate(null).getValue0();
        } catch (CalculationException ex) {
            logger.error("Failed to calculate the flux flunction", ex);
            throw new InitializationException(ex.getMessage(), ex);
        }
        System.out.println("convolvedfluxFunction");
        // We create a function which performs the convolution
        AveragingKernelConvolution convolvedfluxFunction = new AveragingKernelConvolution(fluxFunction, convolutionKernelSizeFunction);
        System.out.println("systemEfficiencyCalculator");
        // We retrieve the system efficiency function
        systemEfficiencyCalculator = configuration.getValue2();
        BoundedUnivariateFunction systemEfficiencyFunction;
        try {
            systemEfficiencyFunction = systemEfficiencyCalculator.calculate(null).getValue0();
        } catch (CalculationException ex) {
            logger.error("Failed to calculate the system efficiency flunction", ex);
            throw new InitializationException(ex.getMessage(), ex);
        }
        System.out.println("filterResponseCalculator");
        // We retrieve the filter response function
        filterResponseCalculator = configuration.getValue3();
        BoundedUnivariateFunction filterResponseFunction;
        try {
            filterResponseFunction =filterResponseCalculator.calculate(null).getValue0();
        } catch (CalculationException ex) {
            logger.error("Failed to calculate the filter response flunction", ex);
            throw new InitializationException(ex.getMessage(), ex);
        }
        System.out.println("telescopeAreaCalculator");
        // We calculate the telescoe area
        telescopeAreaCalculator = configuration.getValue4();
        double telescopeArea;
        try {
            telescopeArea = telescopeAreaCalculator.calculate(null).getValue0();
        } catch (CalculationException ex) {
            logger.error("Failed to calculate the telescope area", ex);
            throw new InitializationException(ex.getMessage(), ex);
        }
        // We retrieve the number of pixels calculator
        numberOfPixelsCalculator = configuration.getValue5();
        // We retrieve the pixel scale
        pixelScale = configuration.getValue6().getValue0();
        // We retrieve the delta lambda function
        deltaLambdaCalculator = configuration.getValue7();
        
        System.out.println("spectralResolutionCalculator");
        // We retrieve the spectral resolution function
        spectralResolutionCalculator = configuration.getValue8();
        try {
            spectralResolutionFunction =spectralResolutionCalculator.calculate(null).getValue0();
        } catch (CalculationException ex) {
            logger.error("Failed to calculate the spectral resolution flunction", ex);
            throw new InitializationException(ex.getMessage(), ex);
        }
        System.out.println("constantIntegral");
        // Now we calculate the constant integral
        constantIntegral = calculateIntegral(convolvedfluxFunction, systemEfficiencyFunction,
                filterResponseFunction, telescopeArea);
    }

    private static double calculateIntegral(final AveragingKernelConvolution convolvedfluxFunction,
            final BoundedUnivariateFunction systemEfficiencyFunction,
            final BoundedUnivariateFunction filterResponseFunction, final double telescopeArea)
            throws InitializationException{
    	System.out.println("calculateIntegral");
        // first we calculate the overlap of the filter and the system efficiency
        Pair<Double, Double> systemEfficiencyBounds = systemEfficiencyFunction.getNonZeroBounds();
        Pair<Double, Double> filterBounds = filterResponseFunction.getNonZeroBounds();
        
        double minLambda = Math.max(systemEfficiencyBounds.getValue0(), filterBounds.getValue0());
        double maxLambda = Math.min(systemEfficiencyBounds.getValue1(), filterBounds.getValue1());
        // If there is no overlap the integral will be zero
        if (minLambda >= maxLambda) {
            return 0;
        }
        // We create the polynomial (Atel/h*c)*lambda
        double factor = telescopeArea / (Constants.SPEED_OF_LIGHT * Constants.PLANK);
        PolynomialFunction polynomialFunction = new PolynomialFunction(0, factor);
        System.out.println(minLambda);
        System.out.println(maxLambda);
        System.out.println("functionToIntegrate");
        // We create the function to integrate
        BoundedUnivariateFunction functionToIntegrate;
        try {
            functionToIntegrate = FunctionMultiplicationTool.multiply(minLambda, maxLambda, 
                    convolvedfluxFunction, systemEfficiencyFunction, filterResponseFunction, polynomialFunction);
        } catch (FunctionBoundsTooSmallException ex) {
            String functionName = null;
            switch (ex.getFunctionIndex()) {
                case 0:
                    functionName = "convolved background flux";
                    break;
                case 1:
                    functionName = "system efficiency";
                    break;
                case 2:
                    functionName = "filter response";
                    break;
            }
            String message = (functionName == null) ? ex.getMessage()
                    : "The " + functionName + " cannot be calculated in the range ["
                      + minLambda + "," + maxLambda + "]";
            logger.error(message, ex);
            throw new InitializationException(message);
        } catch (FunctionEvaluationException ex) {
            logger.error(ex.getMessage(), ex);
            throw new InitializationException(ex.getMessage(), ex);
        }
        
        System.out.println("IntegrationTool.univariateIntegral");
        // We calculate and return the integral
        double integral = 0;
        try {
            integral = IntegrationTool.univariateIntegral(functionToIntegrate, minLambda, maxLambda);
        } catch (FunctionEvaluationException ex) {
            throw new InitializationException(ex.getMessage(), ex);
        }
        System.out.println("END OF IntegrationTool.univariateIntegral");
        return integral;
    }

    /**
     * Returns a {@link Pair} containing the functions to calculate the background
     * in the central pixel and the total spatial binning accordingly.
     * @param input The wavelength range expressed in Angstrom
     * @return A {@link Pair} containing the central pixel and total background functions
     * @throws CalculationException if there is any error during the calculation
     */
    @Override
    protected Pair<UnivariateRealFunction, UnivariateRealFunction>
            performCalculation(Pair<Double, Double> input) throws CalculationException {
        double min = input.getValue0();
        double max = input.getValue1();
        BoundedFunctionWrapper centralPixelFunction = ConfigFactory.getConfig().getCentralPixelFlag()
                ? new BoundedFunctionWrapper(min, max, new CentralPixelFunction()) : null;
        TotalFunction totalFunction = new TotalFunction();
        return new Pair<UnivariateRealFunction, UnivariateRealFunction>(
                centralPixelFunction, new BoundedFunctionWrapper(min, max, totalFunction));
    }
    
    private class TotalFunction implements UnivariateRealFunction {

        @Override
        public double value(double lambda) throws FunctionEvaluationException {
            Unit<Double> lambdaInput = new Unit<Double>(lambda);
            double numberOfPixels = 0;
            double deltaLambda = 0;
            try {
                numberOfPixels = numberOfPixelsCalculator.calculate(lambdaInput).getValue0();
                deltaLambda = deltaLambdaCalculator.calculate(lambdaInput).getValue0();
            } catch (CalculationException ex) {
                logger.error(ex.getMessage(), ex);
                throw new FunctionEvaluationException(ex, lambda);
            }
            double totalfactor = numberOfPixels * pixelScale * pixelScale / deltaLambda;
            return totalfactor * constantIntegral;
        }
        
    }
    
    private class CentralPixelFunction implements UnivariateRealFunction {

        @Override
        public double value(double lambda) throws FunctionEvaluationException {
            double spectralResolution = spectralResolutionFunction.value(lambda);
            double centralPixelfactor = pixelScale * pixelScale * spectralResolution / lambda;
            return centralPixelfactor * constantIntegral;
        }
        
    }

    /**
     * <p>For every calculation the calculator adds in the simulation results
     * the following outputs of the calculator:</p>
     * <ul>
     *   <li>The central pixel simulated background noise in intermediate important results
     *       with code CENTRAL_PIXEL_SIMULATED_BACKGROUND_NOISE</li>
     *   <li>The total simulated background noise in intermediate important results
     *       with code SIMULATED_BACKGROUND_NOISE</li>
     * </ul>
     * @param input
     * @param output 
     */
    @Override
    protected void performForEveryCalculation(Pair<Double, Double> input, 
                Pair<UnivariateRealFunction, UnivariateRealFunction> output) {
        double min = input.getValue0();
        double max = input.getValue1();
        if (ConfigFactory.getConfig().getCentralPixelFlag()) {
            CalculationResults.DoubleDatasetResult cpResult = FunctionToDatasetResultConverter.convert(min, max, output.getValue0(),
                    "CENTRAL_PIXEL_SIMULATED_BACKGROUND_NOISE", Units.ANGSTROM, Units.getElectronsPerSecPerAngstrom());
            ResultsHolder.getResults().addResult(cpResult, CalculationResults.Level.INTERMEDIATE_IMPORTANT);
        }
        CalculationResults.DoubleDatasetResult totalResult = FunctionToDatasetResultConverter.convert(min, max, output.getValue1(), 
                "SIMULATED_BACKGROUND_NOISE", Units.ANGSTROM, Units.getElectronsPerSecPerAngstrom());
        ResultsHolder.getResults().addResult(totalResult, CalculationResults.Level.INTERMEDIATE_IMPORTANT);
    }

    /**
     * <p>For every calculator instantiation the following parts of its configuration
     * are added in the simulation results:</p>
     * <ul>
     *   <li>The pixel scale in debug results with code PIXEL_SCALE</li>
     * </ul>
     * @param configuration The configuration of the calculator
     */
    @Override
    protected void performForEveryRetrieval(Ennead<Calculator<Unit<Double>, Unit<Double>>,
               Calculator<Tuple, Unit<UnivariateRealFunction>>,
               Calculator<Tuple, Unit<BoundedUnivariateFunction>>,
               Calculator<Tuple, Unit<BoundedUnivariateFunction>>,
               Calculator<Tuple, Unit<Double>>,
               Calculator<Unit<Double>, Unit<Double>>,
               Pair<Double, String>,
               Calculator<Unit<Double>, Unit<Double>>,
               Calculator<Tuple, Unit<UnivariateRealFunction>>> configuration) {
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("PIXEL_SCALE",
                configuration.getValue6().getValue0(), configuration.getValue6().getValue1()), CalculationResults.Level.DEBUG);
    }
    
}
