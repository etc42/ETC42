/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.oldsignaltonoise;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import lombok.EqualsAndHashCode;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.Calculator;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.DoubleDatasetResult;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.DoubleValueResult;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.javatuples.Ennead;
import org.javatuples.Pair;
import org.javatuples.Quartet;
import org.javatuples.Tuple;
import org.javatuples.Unit;

/**
 */
@EqualsAndHashCode(callSuper=false, exclude={"rangeMin","rangeMax"})
public class SpectroscopyDit extends AbstractCalculator<
                Ennead<
                       Calculator<Unit<Double>, Unit<Double>>,
                       Calculator<Unit<Double>, Unit<Double>>,
                       Calculator<Unit<Double>, Unit<Double>>,
                       Calculator<Unit<Double>, Unit<Double>>,
                       Pair<Double, String>,
                       Pair<Double, String>,
                       Pair<Double, String>,
                       Pair<Double, String>,
                       Calculator<Tuple, Pair<Double, Double>>
                >, Tuple, 
                Quartet<
                       Map<Double, Double>,
                       Map<Double, Double>,
                       Map<Double, Double>,
                       Map<Double, Double>
                >> {
    
    private Calculator<Unit<Double>, Unit<Double>> deltaLambda;
    private Calculator<Unit<Double>, Unit<Double>> signal;
    private Calculator<Unit<Double>, Unit<Double>> numberOfPixels;
    private Calculator<Unit<Double>, Unit<Double>> backgroundNoise;
    private double dark;
    private double readout;
    private double dit;
    private double exposureTime;
    private Calculator<Tuple, Pair<Double, Double>> wavelengthRange;
    
    private double rangeMin;
    private double rangeMax;

    @Override
    protected void validateConfiguration(Ennead<
                       Calculator<Unit<Double>, Unit<Double>>,
                       Calculator<Unit<Double>, Unit<Double>>,
                       Calculator<Unit<Double>, Unit<Double>>,
                       Calculator<Unit<Double>, Unit<Double>>,
                       Pair<Double, String>,
                       Pair<Double, String>,
                       Pair<Double, String>,
                       Pair<Double, String>,
                       Calculator<Tuple, Pair<Double, Double>>
            > configuration) throws ConfigurationException {
        // Check the dark current
        Pair<Double, String> darkPair = configuration.getValue4();
        if (darkPair.getValue0() < 0) {
            throw new ConfigurationException("Dark current must be non negative "
                    + "but was " + darkPair.getValue0());
        }
        if (!Units.isElectronsPerPixelPerSec(darkPair.getValue1())) {
             throw new ConfigurationException("Dark current must be in " + Units.getElectronsPerPixelPerSec()
                    + " but was in " + darkPair.getValue1());
        }
        // Check the readout noise
        Pair<Double, String> readoutPair = configuration.getValue5();
        if (readoutPair.getValue0() < 0) {
            throw new ConfigurationException("Readout noise must be non negative "
                    + "but was " + readoutPair.getValue0());
        }
        if (!Units.isElectronsPerPixel(readoutPair.getValue1())) {
             throw new ConfigurationException("Readout noise must be in " + Units.getElectronsPerPixel()
                    + " but was in " + readoutPair.getValue1());
        }
        // Check the dit
        Pair<Double, String> ditPair = configuration.getValue6();
        if (ditPair.getValue0() < 0) {
            throw new ConfigurationException("DIT must be non negative "
                    + "but was " + ditPair.getValue0());
        }
        if (!Units.isSec(ditPair.getValue1())) {
             throw new ConfigurationException("DIT must be in " + Units.SEC
                    + " but was in " + ditPair.getValue1());
        }
        // Check the exposure time
        Pair<Double, String> exposureTimePair = configuration.getValue7();
        if (exposureTimePair.getValue0() <= 0) {
            throw new ConfigurationException("Exposure time must be positive "
                    + "but was " + exposureTimePair.getValue0());
        }
        if (!Units.isSec(exposureTimePair.getValue1())) {
             throw new ConfigurationException("Exposure time must be in " + Units.SEC
                    + " but was in " + exposureTimePair.getValue1());
        }
    }

    @Override
    protected void initialize(Ennead<
                       Calculator<Unit<Double>, Unit<Double>>,
                       Calculator<Unit<Double>, Unit<Double>>,
                       Calculator<Unit<Double>, Unit<Double>>,
                       Calculator<Unit<Double>, Unit<Double>>,
                       Pair<Double, String>,
                       Pair<Double, String>,
                       Pair<Double, String>,
                       Pair<Double, String>,
                       Calculator<Tuple, Pair<Double, Double>>
            > configuration) throws InitializationException {
        deltaLambda = configuration.getValue0();
        signal = configuration.getValue1();
        numberOfPixels = configuration.getValue2();
        backgroundNoise = configuration.getValue3();
        dark = configuration.getValue4().getValue0();
        readout = configuration.getValue5().getValue0();
        dit = configuration.getValue6().getValue0();
        exposureTime = configuration.getValue7().getValue0();
        wavelengthRange = configuration.getValue8();
        Pair<Double, Double> minMax = null;
        try {
            minMax = wavelengthRange.calculate(null);
        } catch (CalculationException e) {
            throw new InitializationException("Failed to calculate the wavelength range", e);
        }
        rangeMin = minMax.getValue0();
        rangeMax = minMax.getValue1();
    }

    @Override
    protected Quartet<
                       Map<Double, Double>,
                       Map<Double, Double>,
                       Map<Double, Double>,
                       Map<Double, Double>
                > performCalculation(Tuple input) throws CalculationException {
        // First build the list of the wavelengths we do the calculation for
        List<Unit<Double>> lambdaList = new ArrayList<Unit<Double>>();
        for (double lambda = rangeMin;
                lambda <= rangeMax;
                lambda += deltaLambda.calculate(new Unit<Double>(lambda)).getValue0()) {
            lambdaList.add(new Unit<Double>(lambda));
        }
        double detectorPerPixel = dark * exposureTime + (exposureTime / dit) * readout * readout;
        Map<Double, Double> signalToNoiseMap = new TreeMap<Double, Double>();
        Map<Double, Double> totalSignalMap = new TreeMap<Double, Double>();
        Map<Double, Double> totalBackgroundNoiseMap = new TreeMap<Double, Double>();
        Map<Double, Double> totalDetectorNoiseMap = new TreeMap<Double, Double>();
        for (Unit<Double> lambda : lambdaList) {
            double signalPerLambda = signal.calculate(lambda).getValue0() * exposureTime;
            double s = signalPerLambda * deltaLambda.calculate(lambda).getValue0();
            double bgPerLambda = backgroundNoise.calculate(lambda).getValue0() * exposureTime;
            double bg = bgPerLambda * deltaLambda.calculate(lambda).getValue0();
            double npix = numberOfPixels.calculate(lambda).getValue0();
            double det = npix * detectorPerPixel;
            double signalToNoise = s / Math.sqrt(s + bg + det);
            
            totalSignalMap.put(lambda.getValue0(), s);
            totalBackgroundNoiseMap.put(lambda.getValue0(), bg);
            totalDetectorNoiseMap.put(lambda.getValue0(), det);
            signalToNoiseMap.put(lambda.getValue0(), signalToNoise);
        }
        return new Quartet<Map<Double, Double>, Map<Double, Double>, Map<Double, Double>, Map<Double, Double>>
                (signalToNoiseMap, totalSignalMap, totalBackgroundNoiseMap, totalDetectorNoiseMap);
    }

    @Override
    protected void performForEveryCalculation(Tuple input,
            Quartet<Map<Double, Double>, Map<Double, Double>, Map<Double, Double>, Map<Double, Double>> output) {
        ResultsHolder.getResults().addResult(new DoubleDatasetResult("SIGNAL_TO_NOISE",
                output.getValue0(), Units.ANGSTROM, null), Level.FINAL);
        ResultsHolder.getResults().addResult(new DoubleDatasetResult("TOTAL_SIGNAL",
                output.getValue1(), Units.ANGSTROM, Units.ELECTRONS), Level.FINAL);
        ResultsHolder.getResults().addResult(new DoubleDatasetResult("TOTAL_BACKGROUND_NOISE",
                output.getValue2(), Units.ANGSTROM, Units.ELECTRONS), Level.FINAL);
        ResultsHolder.getResults().addResult(new DoubleDatasetResult("TOTAL_DELTECTOR_NOISE",
                output.getValue3(), Units.ANGSTROM, Units.ELECTRONS), Level.FINAL);
    }

    @Override
    protected void performForEveryRetrieval(Ennead<
                       Calculator<Unit<Double>, Unit<Double>>,
                       Calculator<Unit<Double>, Unit<Double>>,
                       Calculator<Unit<Double>, Unit<Double>>,
                       Calculator<Unit<Double>, Unit<Double>>,
                       Pair<Double, String>,
                       Pair<Double, String>,
                       Pair<Double, String>,
                       Pair<Double, String>,
                       Calculator<Tuple, Pair<Double, Double>>> configuration) {
        Pair<Double, String> darkPair = configuration.getValue4();
        ResultsHolder.getResults().addResult(new DoubleValueResult("DARK_CURRENT", 
                darkPair.getValue0(), darkPair.getValue1()), Level.DEBUG);
        Pair<Double, String> readoutPair = configuration.getValue5();
        ResultsHolder.getResults().addResult(new DoubleValueResult("READOUT_NOISE", 
                readoutPair.getValue0(), readoutPair.getValue1()), Level.DEBUG);
        Pair<Double, String> ditPair = configuration.getValue6();
        ResultsHolder.getResults().addResult(new DoubleValueResult("DIT", 
                ditPair.getValue0(), ditPair.getValue1()), Level.DEBUG);
        Pair<Double, String> exposureTimePair = configuration.getValue7();
        ResultsHolder.getResults().addResult(new DoubleValueResult("EXPOSURE_TIME", 
                exposureTimePair.getValue0(), exposureTimePair.getValue1()), Level.DEBUG);
    }

}
