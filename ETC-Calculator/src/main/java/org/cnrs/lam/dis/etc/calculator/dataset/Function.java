/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.dataset;

import java.util.Map;
import lombok.EqualsAndHashCode;
import org.apache.commons.math.ArgumentOutsideDomainException;
import org.apache.commons.math.analysis.interpolation.SplineInterpolator;
import org.apache.commons.math.analysis.polynomials.PolynomialSplineFunction;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.Cacheable;
import org.cnrs.lam.cesam.util.calculator.CachingPolicy;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.DatasetProviderHolder;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.javatuples.Triplet;
import org.javatuples.Unit;

/**
 * <p>The {@code Function} class is a calculator for computing values of functions
 * represented by a dataset. It works by using interpolation for calculating the
 * values of the function for keys between the ones of the dataset. Note that this
 * class is <b>not</b> doing any kind of convolution and it uses the data of the
 * dataset as are. This shouldn't create any problem to the usage of the class,
 * as the datasets representing functions wouldn't benefit by convolution.</p>
 * 
 * <p>The configuration of the calculator is a {@link org.javatuples.Triplet}, which
 * contains the following elements:</p>
 * <ol>
 *   <li>{@link org.cnrs.lam.dis.etc.datamodel.Dataset.Type}: The type of the
 *       dataset this calculator will represent</li>
 *   <li>{@link org.cnrs.lam.dis.etc.datamodel.DatasetInfo}: The information (name
 *       and namespace) of the dataset this calculator will represent</li>
 *   <li>{@link java.lang.String}: The option of a multi-dataset or null for single
 *       dataset</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the key for which the value will be
 * interpolated.</p>
 * 
 * <p>The output of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the interpolated value.</p>
 *
 * @author Nikolaos Apostolakos
 */

// Caching explenation
// -------------------
// This calculator is set to cache the instances because the configuration validation
// is retrieving the dataset (which is time consuming) and the initialization is creating 
// the interpolator, which is also time consuming. There is no need for result caching
// as the calculation of the result is done really fast, by using the already created
// interpolator.
@Cacheable(CachingPolicy.INSTANCE)
@EqualsAndHashCode(callSuper=false, exclude={"function"})
public class Function extends AbstractCalculator<Triplet<Dataset.Type, DatasetInfo, String>, Unit<Double>, Unit<Double>> {
    
    private Dataset.Type datasetType;
    private DatasetInfo datasetInfo;
    private String option;
    private PolynomialSplineFunction function;

    /**
     * Validates the given configuration. It requires that the given dataset is
     * available via the dataset provider.
     * @param configuration The configuration to validate
     * @throws ConfigurationException If configuration is invalid
     */
    @Override
    protected void validateConfiguration(Triplet<Dataset.Type, DatasetInfo, String> configuration) throws ConfigurationException {
        Dataset dataset = DatasetProviderHolder.getDatasetProvider().getDataset(
                configuration.getValue0(), configuration.getValue1(), configuration.getValue2());
        if (dataset == null) {
            String optionString = (configuration.getValue2() == null || configuration.getValue2().equals(""))
                    ? "" : " and option " + configuration.getValue2();
            throw new ConfigurationException("The dataset of type" + configuration.getValue0()
                    + " and name " + configuration.getValue1() + optionString + " is not available");
        }
    }

    /**
     * Initializes the calculator instance with the given configuration. The
     * initialization includes the setup of the interpolation. The first value
     * of the configuration is the type of the dataset and the second is the
     * name of it.
     * @param configuration The configuration to use for initialization
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(Triplet<Dataset.Type, DatasetInfo, String> configuration) throws InitializationException {
        datasetType = configuration.getValue0();
        datasetInfo = configuration.getValue1();
        option = configuration.getValue2();
        Dataset dataset = DatasetProviderHolder.getDatasetProvider().getDataset(
                datasetType, datasetInfo, option);
        
        // Create an interpolator with the data from the dataset
        double[] x = new double[dataset.getData().size()];
        double[] y = new double[dataset.getData().size()];
        int i = 0;
        for (Map.Entry<Double, Double> entry : dataset.getData().entrySet()) {
            x[i] = entry.getKey();
            y[i] = entry.getValue();
            i++;
        }
        // We use SpLine interpolation. This means the values are calculated with
        // third degree polynomials between the knots
        function = new SplineInterpolator().interpolate(x, y);
    }

    /**
     * Returns a {@link org.javatuples.Unit} containing the value of the function
     * for the given input.
     * @param input The key to interpolate for
     * @return The value of the function for the given key
     * @throws CalculationException if there is any error during the calculation
     */
    @Override
    protected Unit<Double> performCalculation(Unit<Double> input) throws CalculationException {
        try {
            double value = function.value(input.getValue0());
            return new Unit<Double>(value);
        } catch (ArgumentOutsideDomainException ex) {
            throw new CalculationException("Tried to get a value from the " + datasetType
                    + " dataset, but the key " + input.getValue0() + " is out of range");
        }
    }
    
}
