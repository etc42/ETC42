/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.util.functions;

import lombok.EqualsAndHashCode;
import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.javatuples.Pair;

/**
 * <p>The {@link SymmetricBivariateDoubleGaussian} class represents a scaled 
 * bivariate double Gaussian with peak value at (0,0). The double Gaussian is
 * calculated as:</p>
 * {@latex.inline $
 * f_{(x,y)}=scale*(w*f_{\\sigma_1(x,y)}+(1-w)*f_{\\sigma_2(x,y)})
 * $}
 * <p>where</p>
 * <ul>
 *   <li>{@latex.inline $scale$}: is the scale factor of the double Gaussian</li>
 *   <li>{@latex.inline $w$}: is the weight of the double Gaussian</li>
 *   <li>{@latex.inline $f_{\\sigma_1(x,y)}$}: is a symmetric bivariate Gaussian
 *       with standard deviation {@latex.inline $\\sigma_1$}</li>
 *   <li>{@latex.inline $f_{\\sigma_2(x,y)}$}: is a symmetric bivariate Gaussian
 *       with standard deviation {@latex.inline $\\sigma_2$}</li>
 * </ul>
 */
@EqualsAndHashCode(callSuper=false)
public class SymmetricBivariateDoubleGaussian implements CircularlySymmetricBivariateFunction {
    
    private final SymmetricBivariateGaussian firstGaussian;
    private final SymmetricBivariateGaussian secondGaussian;
    private final double weight;
    private final double scale;
    
    /**
     * Constructs a new {@link SymmetricBivariateDoubleGaussian} with Gaussians
     * with the given standard deviations and weight of the first the given one
     * and weight of the second one minus the given one. Note that the scale
     * factor used is one.
     * @param firstStandardDeviation  The standard deviation of the first Gaussian
     * @param secondStandardDeviation The standard deviation of the second Gaussian
     * @param weight The weight with which the first Gaussian is multiplied. The
     * second is multiplied with 1-weight.
     * @throws IllegalArgumentException if the weight is out of the range [0,1]
     */
    public SymmetricBivariateDoubleGaussian(double firstStandardDeviation,
            double secondStandardDeviation, double weight) {
        this(firstStandardDeviation, secondStandardDeviation, weight, 1.);
    }

    /**
     * Constructs a new {@link SymmetricBivariateDoubleGaussian} with scaled Gaussians
     * with the given standard deviations and weight of the first the given one
     * and weight of the second one minus the given one.
     * @param firstStandardDeviation  The standard deviation of the first Gaussian
     * @param secondStandardDeviation The standard deviation of the second Gaussian
     * @param weight The weight with which the first Gaussian is multiplied. The
     * second is multiplied with 1-weight.
     * @param scale The scale factor of the double gaussian
     * @throws IllegalArgumentException if the weight is out of the range [0,1]
     */
    public SymmetricBivariateDoubleGaussian(double firstStandardDeviation,
            double secondStandardDeviation, double weight, double scale) {
        if (weight < 0 || weight > 1) {
            throw new IllegalArgumentException("Double Gaussian weight must be in range [0,1] but was " + weight);
        }
        this.weight = weight;
        this.scale = scale;
        firstGaussian = new SymmetricBivariateGaussian(firstStandardDeviation, scale);
        secondGaussian = new SymmetricBivariateGaussian(secondStandardDeviation, scale);
    }
    
    /**
     * Returns a {@link Pair} containing the standard deviations of the two Gaussians.
     * The first value is the standard deviation of the Gaussian multiplied with
     * the weight and the second of the one multiplied by 1-weight.
     * @return
     */
    public Pair<Double, Double> getStandardDeviations() {
        double firstSigma = firstGaussian.getStandardDeviation();
        double secondSigma = secondGaussian.getStandardDeviation();
        return new Pair<Double, Double>(firstSigma, secondSigma);
    }
    
    /**
     * Returns the weight with which the first Gaussian is multiplied. The weight
     * of the second one is 1-weight.
     * @return 
     */
    public double getWeight() {
        return weight;
    }
    
    /**
     * Returns the scale factor with which the double Gaussian is multiplied with.
     * @return 
     */
    public double getScale() {
        return scale;
    }

    /**
     * Returns the value of the Bivariate Double Gaussian for the distance r from the
     * location of the peak.
     * @param r The distance from the peak
     * @return The value of the double Gaussian
     * @throws FunctionEvaluationException 
     */
    @Override
    public double value(double r) throws FunctionEvaluationException {
        double firstValue = firstGaussian.value(r);
        double secondValue = secondGaussian.value(r);
        // Note that the gaussians are already scaled, so we do not multiply again with the scale
        double value = weight * firstValue + (1 - weight) * secondValue;
        return value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UnivariateRealFunction projectionFunction() {
        // The projection is a univariate double gaussian with the same sigma1
        // and sigma 2 but with different weight and scale factor.
        double sigma1 = firstGaussian.getStandardDeviation();
        double sigma2 = secondGaussian.getStandardDeviation();
        double sqrtTwoPi = Math.sqrt(2 * Math.PI);
        double t = 1 / (weight / (sigma1 * sqrtTwoPi) + (1-weight) / (sigma2 * sqrtTwoPi));
        double newWeight = t * weight / (sigma1 * sqrtTwoPi);
        double newScale = scale / t;
        return new DoubleGaussian(sigma1, sigma2, newWeight, newScale);
    }

    /**
     * Returns the value of the Bivariate dDouble Gaussian for the point (x,y) from the
     * location of the peak.
     * @param x The distance from the peak on the X axis
     * @param y The distance from the peak on the Y axis
     * @return The value of the double Gaussian
     * @throws FunctionEvaluationException 
     */
    @Override
    public double value(double x, double y) throws FunctionEvaluationException {
        double firstValue = firstGaussian.value(x, y);
        double secondValue = secondGaussian.value(x, y);
        // Note that the gaussians are already scaled, so we do not multiply again with the scale
        double value = weight * firstValue + (1 - weight) * secondValue;
        return value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UnivariateRealFunction polarFunction() {
        // The projection is a univariate double gaussian with the same sigma1
        // and sigma 2 but with different weight and scale factor.
        double sigma1 = firstGaussian.getStandardDeviation();
        double sigma2 = secondGaussian.getStandardDeviation();
        double sqrtTwoPi = Math.sqrt(2 * Math.PI);
        double t = 1 / (weight / (sigma1 * sqrtTwoPi) + (1-weight) / (sigma2 * sqrtTwoPi));
        double newWeight = t * weight / (sigma1 * sqrtTwoPi);
        double newScale = scale / t;
        return new PolarDoubleGaussian(sigma1, sigma2, newWeight, newScale);
    }
    
}
