/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.covolvedsurfacebrightnessprofile;

import java.text.MessageFormat;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TreeMap;
import lombok.EqualsAndHashCode;
import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.analysis.BivariateRealFunction;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.apache.log4j.Logger;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.calculator.util.datasets.Dataset;
import org.cnrs.lam.dis.etc.calculator.util.datasets.LinearFunctionDataset;
import org.cnrs.lam.dis.etc.calculator.util.functions.BivariateFunctionFromProjection;
import org.cnrs.lam.dis.etc.calculator.util.functions.CircularlySymmetricBivariateFunction;
import org.cnrs.lam.dis.etc.calculator.util.integration.IntegrationTool;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.javatuples.Pair;
import org.javatuples.Quintet;
import org.javatuples.Tuple;
import org.javatuples.Unit;

/**
 * <p>The {@link CircularlySymmetric} class is a calculator for computing functions
 * representing the surface brightness profile convolved with the PSF. Currently
 * both the PSF and the surface brightness profile have to be circularly symmetric
 * functions.</p>
 * 
 * <p>The configuration of the calculator is a {@link Quintet},
 * with contains the following elements:</p>
 * <ol>
 *   <li>{@link Pair}: The extended source radius and its unit</li>
 *   <li>{@link Pair}: The pixel scale and its unit</li>
 *   <li>{@link Calculator}: A calculator for computing the surface brightness profile functions</li>
 *   <li>{@link Calculator}: A calculator for computing the PSF functions</li>
 *   <li>{@link Calculator}: A calculator for computing the PSF size</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the wavelength for which the calculation
 * is done (expressed in {@latex.inline \\AA}).</p>
 * 
 * <p>The output of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link BivariateRealFunction} representing the convolved surface brigthness
 * profile.</p>
 */
// Caching explenation
// -------------------
// This calculator is set to cache the instances and the results because the
// calculation of the results is done by a time consuming convolution
@Cacheable(CachingPolicy.ALL)
@EqualsAndHashCode(callSuper=false, exclude="profileFunction")
public class CircularlySymmetric extends AbstractCalculator<
        Quintet<Pair<Double, String>, Pair<Double, String>,
                Calculator<Tuple, Unit<BivariateRealFunction>>,
                Calculator<Unit<Double>, Unit<BivariateRealFunction>>,
                Calculator<Unit<Double>, Unit<Double>>>, 
        Unit<Double>, Unit<BivariateRealFunction>> {
    
    private static final Logger logger = Logger.getLogger(CircularlySymmetric.class);
    private static final ResourceBundle validationErrorsBundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");
    private double sourceRadius;
    private double pixelScale;
    private Calculator<Tuple, Unit<BivariateRealFunction>> surfaceBrightnessProfileCalculator;
    BivariateRealFunction profileFunction;
    private Calculator<Unit<Double>, Unit<BivariateRealFunction>> psfCalculator;
    private Calculator<Unit<Double>, Unit<Double>> psfSizeCalculator;

    /**
     * Validates the given configuration. It requires that the  source radius is 
     * a positive number and that its unit is {@latex.inline $arcsec$}.
     * @param configuration The configuration to validate
     * @throws ConfigurationException If the configuration is invalid
     */
    @Override
    protected void validateConfiguration(Quintet<
            Pair<Double, String>, Pair<Double, String>,
            Calculator<Tuple, Unit<BivariateRealFunction>>,
            Calculator<Unit<Double>, Unit<BivariateRealFunction>>,
            Calculator<Unit<Double>, Unit<Double>>> configuration)
            throws ConfigurationException {
        // Check that the source radius is a positive number
        if (configuration.getValue0().getValue0() <= 0) {
            String message = validationErrorsBundle.getString("EXTENDED_SOURCE_RADIUS_NOT_POSITIVE");
            message = MessageFormat.format(message, configuration.getValue0().getValue0());
            throw new ConfigurationException(message);
        }
        // Check if the source radius is in arcsec
        if (!Units.isArcsec(configuration.getValue0().getValue1())) {
            String message = validationErrorsBundle.getString("EXTENDED_SOURCE_RADIUS_WRONG_UNIT");
            message = MessageFormat.format(message, Units.ARCSEC, configuration.getValue0().getValue1());
            throw new ConfigurationException(message);
        }
        // Check that the pixel scale is a positive number
        if (configuration.getValue1().getValue0() <= 0) {
            String message = validationErrorsBundle.getString("PIXEL_SCALE_NOT_POSITIVE");
            message = MessageFormat.format(message, configuration.getValue1().getValue0());
            throw new ConfigurationException(message);
        }
        // Check if the pixel scale is in arcsec/pixel
        if (!Units.getArcsecPerPixel().equals(configuration.getValue1().getValue1())) {
            String message = validationErrorsBundle.getString("PIXEL_SCALE_WRONG_UNIT");
            message = MessageFormat.format(message, Units.getArcsecPerPixel(), configuration.getValue1().getValue1());
            throw new ConfigurationException(message);
        }
    }

    /**
     * Initializes the calculator instance with the given configuration.
     * @param configuration the configuration to use for initialization
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(Quintet<
            Pair<Double, String>, Pair<Double, String>,
            Calculator<Tuple, Unit<BivariateRealFunction>>,
            Calculator<Unit<Double>, Unit<BivariateRealFunction>>,
            Calculator<Unit<Double>, Unit<Double>>> configuration)
            throws InitializationException {
        sourceRadius = configuration.getValue0().getValue0();
        pixelScale = configuration.getValue1().getValue0();
        surfaceBrightnessProfileCalculator = configuration.getValue2();
        try {
            profileFunction = surfaceBrightnessProfileCalculator.calculate(null).getValue0();
        } catch (CalculationException ex) {
            logger.warn("Failed to calculate the surface brightness profile function", ex);
            throw new InitializationException(ex.getMessage(), ex);
        }
        if (!(profileFunction instanceof CircularlySymmetricBivariateFunction)) {
            throw new InitializationException("Only circularly symmetric surface brightness profiles "
                    + "are currently supported");
        }
        psfCalculator = configuration.getValue3();
        psfSizeCalculator = configuration.getValue4();
    }

    @Override
    protected Unit<BivariateRealFunction> performCalculation(Unit<Double> input) throws CalculationException {
        BivariateRealFunction psf = psfCalculator.calculate(input).getValue0();
        if (!(psf instanceof CircularlySymmetricBivariateFunction)) {
            throw new CalculationException("Only circularly symmetric PSF functions "
                    + "are currently supported for convolution with the surface brightness profile");
        }
        Double psfSize = psfSizeCalculator.calculate(input).getValue0();
        
        // We are going to make a discrete convolution. We calculate the sampling
        // scale based on the sampling factor
        double samplingFactor = 0.1;
        double samplingScale = samplingFactor * pixelScale;
        if (samplingScale > samplingFactor * sourceRadius) {
            samplingScale = samplingFactor * sourceRadius;
        }
        if (samplingScale > samplingFactor * psfSize / 2.) {
            samplingScale = samplingFactor * psfSize / 2.;
        }
        
        // We construct the data representing the psf
        double[][] psfData;
        try {
            psfData = makeSampleDataFromFunction(psf, psfSize, samplingScale);
        } catch (FunctionEvaluationException ex) {
            logger.warn("Failed to calculate the PSF value during convolution", ex);
            throw new CalculationException("Failed to calculate the PSF value during convolution");
        }
        
        // We construct the data representing the profile. Note that we do not need
        // any data away from the X axis, so we set the maxYSize
        double[][] profileData;
        try {
            profileData = makeSampleDataFromFunction(profileFunction, 2. * (sourceRadius + 2*psfSize), samplingScale, psfData[0].length);
        } catch (FunctionEvaluationException ex) {
            logger.warn("Failed to calculate the surface brightness profile value for convolution", ex);
            throw new CalculationException("Failed to calculate the surface brightness profile value during convolution");
        }
        
        // We convolve the psf with the profile. Note that we just want the values
        // for y = 0 because the result is circularly symmetric.
        Map<Double, Double> convolvedData = convolve(profileData, psfData, sourceRadius + psfSize, samplingScale);
        UnivariateRealFunction convolvedProjectionFunction = new LinearFunctionDataset(convolvedData);
        CircularlySymmetricBivariateFunction convolvedFunction = new BivariateFunctionFromProjection(convolvedProjectionFunction);
        return new Unit<BivariateRealFunction>(convolvedFunction);
    }

    /**
     * Returns the values for y = 0 of the convolution of the PSF with the profile.
     * @param psfData
     * @param profileData
     * @param samplingScale
     * @return 
     */
    private Map<Double, Double> convolve(double[][] input, double[][] kernel, double radius, double samplingScale) {
        Map<Double, Double> result = new TreeMap<Double, Double>();
        int length = (int) Math.floor(radius/samplingScale);
        for (int x = 0; x <= length; x++) {
            double totalValue = 0;
            double totalKernel = 0;
            for (int i = -kernel.length + 1; i < kernel.length; i++) {
                for (int j = -kernel.length + 1; j < kernel.length; j++) {
                    int kernelX = Math.abs(i);
                    int kernelY = Math.abs(j);
                    double kernelValue = kernel[kernelX][kernelY];
                    totalKernel += kernelValue;
                    int inputX = Math.abs(x + i);
                    int inputY = Math.abs(j);
                    if (inputX < input.length && inputY < input.length) {
                        double inputValue = input[inputX][inputY] / samplingScale / samplingScale;
                        totalValue += inputValue * kernelValue;
                    }
                }
            }
            totalValue = totalValue / totalKernel;
            double realX = x * samplingScale;
            result.put(realX, totalValue);
            result.put(-realX, totalValue);
        }
        return result;
    }
    private double[][] makeSampleDataFromFunction(BivariateRealFunction psf,
            double size, double samplingScale) throws FunctionEvaluationException {
        return makeSampleDataFromFunction(psf, size, samplingScale, Integer.MAX_VALUE);
    }

    /*
     * Returns the sampled data for the function as a two dimentional array. The
     * returned data are representing only the (x >= 0, y >= 0) quartet and the indexes
     * have to be scaled with the samplingScale. The values are the integrals of the
     * function in the limits x-samplingScale/2., x+samplingScale/2., y-samplingScale/2., y+samplingScale/2.
     */
    private double[][] makeSampleDataFromFunction(BivariateRealFunction function,
            double size, double samplingScale, int maxYSize) throws FunctionEvaluationException {
        int arraySize = (int) Math.ceil(size / 2. / samplingScale);
        double[][] data = new double[arraySize][Math.min(arraySize, maxYSize)];
        for (int x = 0; x < arraySize; x++) {
            for (int y = 0; y <  Math.min(arraySize, maxYSize); y++) {
                double realX = x * samplingScale;
                double realY = y * samplingScale;
                data[x][y] = IntegrationTool.bivariateIntegral(function, realX-samplingScale/2., realX+samplingScale/2., realY-samplingScale/2., realY+samplingScale/2.);
            }
        }
        return data;
    }

    /**
     * <p>For every calculation the calculator adds in the simulation results
     * the following outputs of the calculator:</p>
     * <ul>
     *   <li>The projection of the convolved surface brightness profile on the plane y=0
     *       in the intermediate important results with code
     *       CONVOLVED_SURFACE_BRIGHTNESS_PROFILE_PROJECTION</li>
     * </ul>
     * @param input
     * @param output 
     */
    @Override
    protected void performForEveryCalculation(Unit<Double> input, Unit<BivariateRealFunction> output) {
        // We know we have a circularly symmetric function so the casting is safe
        CircularlySymmetricBivariateFunction function = (CircularlySymmetricBivariateFunction) output.getValue0();
        // We know that the projection is a Dataset, so the casting is safe
        Dataset projection = (Dataset) function.projectionFunction();
        Map<Double, Double> projectionData = projection.getData();
        
        double lambda = input.getValue0();
        CalculationResults.DoubleDatasetResult projectionResult = 
                new CalculationResults.DoubleDatasetResult("CONVOLVED_SURFACE_BRIGHTNESS_PROFILE_PROJECTION", projectionData, Units.ARCSEC, null);
        ResultsHolder.getResults().addResult("CONVOLVED_SURFACE_BRIGHTNESS_PROFILE_PROJECTION", lambda, projectionResult,
                Units.ANGSTROM, CalculationResults.Level.INTERMEDIATE_IMPORTANT);
    }

    /**
     * <p>For every calculator instantiation the following parts of its configuration
     * are added in the simulation results:</p>
     * <ul>
     *   <li>The extended source effective radius in debug results with code SOURCE_RADIUS</li>
     *   <li>The pixel scale in debug results with code PIXEL_SCALE</li>
     * </ul>
     * @param configuration The configuration of the calculator
     */
    @Override
    protected void performForEveryRetrieval(Quintet<
            Pair<Double, String>, Pair<Double, String>,
            Calculator<Tuple, Unit<BivariateRealFunction>>,
            Calculator<Unit<Double>, Unit<BivariateRealFunction>>,
            Calculator<Unit<Double>, Unit<Double>>> configuration) {
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("SOURCE_RADIUS",
                configuration.getValue0().getValue0(), configuration.getValue0().getValue1()), CalculationResults.Level.DEBUG);
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("PIXEL_SCALE",
                configuration.getValue1().getValue0(), configuration.getValue1().getValue1()), CalculationResults.Level.DEBUG);
    }
    
}
