/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.psfsize;

import java.text.MessageFormat;
import java.util.ResourceBundle;
import lombok.EqualsAndHashCode;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.Cacheable;
import org.cnrs.lam.cesam.util.calculator.CachingPolicy;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.javatuples.Pair;
import org.javatuples.Unit;

/**
 * <p>The {@link FixedSize} class is a calculator for computing the size
 * of the PSF. It always returns the same fixed value, given by the user.</p>
 * 
 * <p>The configuration of the calculator is a {@link org.javatuples.Pair},
 * with contains the fixed PSF size value and its unit.</p>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the wavelength for which the calculation
 * is done (expressed in {@latex.inline \\AA}).</p>
 * 
 * <p>The output of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link Double} representing the size of the PSF expressed in
 * {@latex.inline $arcsec$}.</p>
 */
@Cacheable(CachingPolicy.ALL)
@EqualsAndHashCode(callSuper=false)
public class FixedSize extends AbstractCalculator<Pair<Double, String>, Unit<Double>, Unit<Double>> {
    
    private static final ResourceBundle validationErrorsBundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");
    private double fixedSize;

    /**
     * Validates the given configuration. It requires that the PSF fixed size
     * is positive and that its unit is {@latex.inline $arcsec$}.
     * @param configuration the configuration to validate
     * @throws ConfigurationException if the configuration is invalid
     */
    @Override
    protected void validateConfiguration(Pair<Double, String> configuration) throws ConfigurationException {
        // Check that the fixed PSF size is a positive number
        if (configuration.getValue0() <= 0) {
            String message = validationErrorsBundle.getString("FIXED_PSF_SIZE_NOT_POSITIVE");
            message = MessageFormat.format(message, configuration.getValue0());
            throw new ConfigurationException(message);
        }
        // Check if the fixed PSF size is in arcsec
        if (!Units.ARCSEC.equals(configuration.getValue1())) {
            String message = validationErrorsBundle.getString("FIXED_PSF_SIZE_WRONG_UNIT");
            message = MessageFormat.format(message, Units.ARCSEC, configuration.getValue1());
            throw new ConfigurationException(message);
        }
    }

    @Override
    protected void initialize(Pair<Double, String> configuration) throws InitializationException {
        fixedSize = configuration.getValue0();
    }

    @Override
    protected Unit<Double> performCalculation(Unit<Double> input) throws CalculationException {
        return new Unit<Double>(fixedSize);
    }

    /**
     * <p>For every calculation the calculator adds in the simulation results
     * the following outputs of the calculator:</p>
     * <ul>
     *   <li>The size of the PSF in intermediate unimportant results
     *       with code PSF_SIZE</li>
     * </ul>
     * @param input
     * @param output 
     */
    @Override
    protected void performForEveryCalculation(Unit<Double> input, Unit<Double> output) {
        ResultsHolder.getResults().addResult("PSF_SIZE", input.getValue0(), output.getValue0(), 
                Units.ANGSTROM, Units.ARCSEC, CalculationResults.Level.INTERMEDIATE_UNIMPORTANT);
    }

    /**
     * <p>For every calculator instantiation the following parts of its configuration
     * are added in the simulation results:</p>
     * <ul>
     *   <li>The fixed PSF size in debug results with code PSF_FIXED_SIZE</li>
     * </ul>
     * @param configuration The configuration of the calculator
     */
    @Override
    protected void performForEveryRetrieval(Pair<Double, String> configuration) {
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("PSF_FIXED_SIZE",
                configuration.getValue0(), configuration.getValue1()), CalculationResults.Level.DEBUG);
    }

}
