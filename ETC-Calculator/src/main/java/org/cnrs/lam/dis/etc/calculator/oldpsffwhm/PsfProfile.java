/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.oldpsffwhm;

import lombok.EqualsAndHashCode;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.Cacheable;
import org.cnrs.lam.cesam.util.calculator.CachingPolicy;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.Calculator;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.DatasetProviderHolder;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.StringResult;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.javatuples.Pair;
import org.javatuples.Triplet;
import org.javatuples.Unit;

/**
 * <p>The {@code PsfProfile} class is a calculator for computing the FWHM
 * of the PSF. The data are retrieved from a dataset representing the FWHM of the
 * PSF.</p>
 * 
 * <p>The configuration of the calculator is a {@link org.javatuples.Pair},
 * with contains the following elements:</p>
 * <ol>
 *   <li>{@link org.cnrs.lam.dis.etc.datamodel.DatasetInfo}: the information of the
 *       instruments PSF FWHM dataset</li>
 *   <li>{@link org.cnrs.lam.cesam.util.calculator.Calculator}: a calculator
 *       representing the dataset of the instruments PSF FWHM</li>
 *   <li>{@link org.cnrs.lam.cesam.util.calculator.Calculator}: a calculator
 *       for calculating the atmosphere contribution to the PSF FWHM.</li>
 * </ol>
 * 
 * <p>Note that the given profile is the PSF of the instrument ONLY. Any contribution
 * of the atmosphere (for example seeing) is given with the second calculator.</p>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the wavelength for which the calculation
 * is done (expressed in {@latex.inline \\AA}).</p>
 * 
 * <p>The output of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the FWHM of the PSF (expressed in
 * {@latex.inline $arcsec$}).</p>
 */

// Caching explenation
// -------------------
// This calculator is set to cache the instances because the configuration validation
// is retrieving the dataset, which is time consuming.
@Cacheable(CachingPolicy.INSTANCE)
@EqualsAndHashCode(callSuper=false)
public class PsfProfile extends AbstractCalculator<Triplet<DatasetInfo, Calculator<Unit<Double>, Unit<Double>>, Calculator<Unit<Double>, Unit<Double>>>, Unit<Double>, Unit<Double>> {
    
    private Calculator<Unit<Double>, Unit<Double>> profileCalculator;
    private Calculator<Unit<Double>, Unit<Double>> atmosphericContributionCalculator;

    /**
     * validates the given configuration. It requires that the instrument has a
     * PSF FWHM profile with available data, that the unit of the X axis is
     * {@latex.inline \\AA} and that the unit of the Y axis is {@latex.inline $arcsec$}.
     * @param configuration the configuration to validate
     * @throws ConfigurationException if the configuration is invalid
     */
    @Override
    protected void validateConfiguration(Triplet<DatasetInfo, Calculator<Unit<Double>, Unit<Double>>, Calculator<Unit<Double>, Unit<Double>>> configuration) throws ConfigurationException {
        DatasetInfo profileInfo = configuration.getValue0();
        if (profileInfo == null) {
            throw new ConfigurationException("Instrument does not have PSF FWHM profile");
        }
        Dataset dataset = DatasetProviderHolder.getDatasetProvider().getDataset(
                Dataset.Type.FWHM, profileInfo);
        if (dataset == null) {
            throw new ConfigurationException("Data for PSF FWHM profile "
                    + profileInfo + " is not available");
        }
        if (!Units.isAngstrom(dataset.getXUnit())) {
            throw new ConfigurationException("Unit of X axis of PSF FWHMprofile must be "
                    + Units.ANGSTROM + " but was " + dataset.getXUnit());
        }
        if (!Units.isArcsec(dataset.getYUnit())) {
            throw new ConfigurationException("Unit of Y axis of PSF FWHM profile must be "
                    + Units.ARCSEC + " but was " + dataset.getYUnit());
        }
    }

    /**
     * Initializes the calculator instance with the given configuration. The first
     * value of the configuration is the PSF FWHM dataset info, the second is a
     * dataset calculator representing the PSF FWHM of the telescope and the third
     * one is a calculator for calculating the atmosphere contribution to the PSF.
     * @param configuration the configuration to use for initialization
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(Triplet<DatasetInfo, Calculator<Unit<Double>, Unit<Double>>, Calculator<Unit<Double>, Unit<Double>>> configuration) throws InitializationException {
        profileCalculator = configuration.getValue1();
        atmosphericContributionCalculator = configuration.getValue2();
    }

    /**
     * Returns a {@link org.javatuples.Unit} containing the FWHM of the PSF
     * expressed in {@latex.inline $arcsec$}.
     * @param input The wavelength expressed in {@latex.inline \\AA}
     * @return The FWHM of the PSF expressed in {@latex.inline $arcsec$}
     * @throws CalculationException if there is any error during the calculation
     */
    @Override
    protected Unit<Double> performCalculation(Unit<Double> input) throws CalculationException {
        double instrumentPsf = profileCalculator.calculate(input).getValue0();
        double atmospherePsf = atmosphericContributionCalculator.calculate(input).getValue0();
        double totalPsf = instrumentPsf + atmospherePsf;
        return new Unit<Double>(totalPsf);
    }

    /**
     * Overridden to add in the results the FWHM of the PSF.
     * @param input The wavelength expressed in {@latex.inline \\AA}
     * @param output The FWHM of the PSF expressed in {@latex.inline $arcsec$}
     */
    @Override
    protected void performForEveryCalculation(Unit<Double> input, Unit<Double> output) {
        ResultsHolder.getResults().addResult("PSF_FWHM", input.getValue0(), output.getValue0(), 
                Units.ANGSTROM, Units.ARCSEC, Level.INTERMEDIATE_UNIMPORTANT);
    }

    /**
     * Overridden to add in the results the PSF FWHM profile name.
     * @param configuration The configuration of the calculator
     */
    @Override
    protected void performForEveryRetrieval(Triplet<DatasetInfo, Calculator<Unit<Double>, Unit<Double>>, Calculator<Unit<Double>, Unit<Double>>> configuration) {
        ResultsHolder.getResults().addResult(new StringResult("PSF_FWHM_PROFILE",
                configuration.getValue0().toString()), Level.DEBUG);
    }
    
}
