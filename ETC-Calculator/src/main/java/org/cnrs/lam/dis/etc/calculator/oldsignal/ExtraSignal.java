/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.oldsignal;

import lombok.EqualsAndHashCode;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.DatasetProviderHolder;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.javatuples.Pair;
import org.javatuples.Unit;

/**
 */
@EqualsAndHashCode(callSuper=false)
public class ExtraSignal extends AbstractCalculator<
        Pair<DatasetInfo, 
                Calculator<Unit<Double>, Unit<Double>>
        >, Unit<Double>, Unit<Double>> {
    
    
    private Calculator<Unit<Double>, Unit<Double>> spectrumCalculator;

    @Override
    protected void validateConfiguration(Pair<DatasetInfo, Calculator<Unit<Double>, Unit<Double>>> configuration) throws ConfigurationException {
        DatasetInfo info = configuration.getValue0();
        if (info == null) {
            throw new ConfigurationException("Observing parameters does not have extra signal");
        }
        Dataset dataset = DatasetProviderHolder.getDatasetProvider().getDataset(Dataset.Type.EXTRA_SIGNAL, info);
        if (dataset == null) {
            throw new ConfigurationException("Data for extra signal "
                    + info + " is not available");
        }
        if (!Units.isAngstrom(dataset.getXUnit())) {
            throw new ConfigurationException("Unit of X axis of extra signal must be "
                    + Units.ANGSTROM + " but was " + dataset.getXUnit());
        }
        if (!Units.isElectronsPerSecPerAngstrom(dataset.getYUnit())) {
            throw new ConfigurationException("Unit of Y axis of extra signal must be "
                    + Units.getElectronsPerSecPerAngstrom() + " but was " + dataset.getYUnit());
        }
    }

    @Override
    protected void initialize(Pair<DatasetInfo, Calculator<Unit<Double>, Unit<Double>>> configuration) throws InitializationException {
        spectrumCalculator = configuration.getValue1();
    }

    @Override
    protected Unit<Double> performCalculation(Unit<Double> input) throws CalculationException {
        return spectrumCalculator.calculate(input);
    }

    @Override
    protected void performForEveryCalculation(Unit<Double> input, Unit<Double> output) {
        ResultsHolder.getResults().addResult("SIGNAL", input.getValue0(),
                output.getValue0(), Units.ANGSTROM, Units.getElectronsPerSecPerAngstrom(), CalculationResults.Level.INTERMEDIATE_UNIMPORTANT);
    }

    @Override
    protected void performForEveryRetrieval(Pair<DatasetInfo, Calculator<Unit<Double>, Unit<Double>>> configuration) {
        ResultsHolder.getResults().addResult(new CalculationResults.StringResult("EXTRA_SIGNAL_TEMPLATE"
                , configuration.getValue0().toString()), CalculationResults.Level.DEBUG);
    }
    
}
