/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.oldspectralresolution;

import lombok.EqualsAndHashCode;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.Cacheable;
import org.cnrs.lam.cesam.util.calculator.CachingPolicy;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.Calculator;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.DatasetProviderHolder;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.StringResult;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.javatuples.Pair;
import org.javatuples.Unit;

/**
 * <p>The {@code SpectralResolutionProfile} class is a calculator for computing the 
 * spectral resolution from a dataset. The configuration of the calculator
 * is a {@link org.javatuples.Pair} which contains the following elements:</p>
 * <ol>
 *   <li>{@link org.cnrs.lam.dis.etc.datamodel.DatasetInfo}: the information of the
 *       spectral resolution dataset</li>
 *   <li>{@link org.cnrs.lam.cesam.util.calculator.Calculator}: a calculator
 *       representing the dataset of the spectral resolution</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the wavelength expressed in {@latex.inline \\AA}.</p>
 * 
 * <p>The output of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the spectral resolution.</p>
 */

// Caching explenation
// -------------------
// This calculator is set to cache the instances because the configuration validation
// is retrieving the dataset, which is time consuming.
@Cacheable(CachingPolicy.INSTANCE)
@EqualsAndHashCode(callSuper=false)
public class SpectralResolutionProfile extends AbstractCalculator<Pair<DatasetInfo, Calculator<Unit<Double>, Unit<Double>>>, Unit<Double>, Unit<Double>> {
    
    private Calculator<Unit<Double>, Unit<Double>> datasetCalculator;

    /**
     * Validates the given configuration. It requires that the instrument has a
     * spectral resolution, that the data for the spectral resolution is 
     * available, that the unit of X axis is {@latex.inline \\AA} and that the
     * unit of Y axis is {@latex.inline \\AA/pixel}.
     * @param configuration the configuration to validate
     * @throws ConfigurationException if the configuration is invalid
     */
    @Override
    protected void validateConfiguration(Pair<DatasetInfo, Calculator<Unit<Double>, Unit<Double>>> configuration) throws ConfigurationException {
        DatasetInfo info = configuration.getValue0();
        if (info == null) {
            throw new ConfigurationException("Instrument does not have spectral resolution");
        }
        Dataset dataset = DatasetProviderHolder.getDatasetProvider().getDataset(Dataset.Type.SPECTRAL_RESOLUTION, info);
        if (dataset == null) {
            throw new ConfigurationException("Data for spectral resolution "
                    + configuration.getValue0() + " is not available");
        }
        if (!Units.isAngstrom(dataset.getXUnit())) {
            throw new ConfigurationException("Unit of X axis of spectral resolution must be "
                    + Units.ANGSTROM + " but was " + dataset.getXUnit());
        }
        if (!Units.isAngstromPerPixel(dataset.getYUnit())) {
            throw new ConfigurationException("Unit of Y axis of spectral resolution must be "
                    + Units.getArcsecPerPixel() + " but was " + dataset.getYUnit());
        }
    }

    /**
     * Initializes the calculator instance with the given configuration. The first
     * value of the configuration is the spectral resolution dataset info and the second
     * is a dataset calculator representing the spectral resolution.
     * @param configuration the configuration to use for initialization
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(Pair<DatasetInfo, Calculator<Unit<Double>, Unit<Double>>> configuration) throws InitializationException {
        datasetCalculator = configuration.getValue1();
    }

    /**
     * Returns a {@link org.javatuples.Unit} containing the spectral resolution.
     * @param input The wavelength expressed in {@latex.inline \\AA}
     * @return The spectral resolution
     * @throws CalculationException if there is any error during the calculation
     */
    @Override
    protected Unit<Double> performCalculation(Unit<Double> input) throws CalculationException {
        double result = datasetCalculator.calculate(input).getValue0();
        return new Unit<Double>(result);
    }

    /**
     * Overridden to add in the results the spectral resolution. This is done here, so
     * we have this results even if the result is retrieved from the cache.
     * @param input The wavelength expressed in {@latex.inline \\AA}
     * @param output The spectral resolution
     */
    @Override
    protected void performForEveryCalculation(Unit<Double> input, Unit<Double> output) {
        ResultsHolder.getResults().addResult("SPECTRAL_RESOLUTION", input.getValue0(),
                output.getValue0(), Units.ANGSTROM, null, Level.INTERMEDIATE_UNIMPORTANT);
    }

    /**
     * Overridden to add in the results the spectral resolution.
     * @param configuration The configuration of the calculator
     */
    @Override
    protected void performForEveryRetrieval(Pair<DatasetInfo, Calculator<Unit<Double>, Unit<Double>>> configuration) {
        ResultsHolder.getResults().addResult(new StringResult("SPECTRAL_RESOLUTION_FUNCTION",
                configuration.getValue0().toString()), Level.DEBUG);
    }
    
}
