/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.convolutionkernelsize;

import lombok.EqualsAndHashCode;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.Cacheable;
import org.cnrs.lam.cesam.util.calculator.CachingPolicy;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.javatuples.Tuple;
import org.javatuples.Unit;

/**
 * <p>The {@link Imaging} class is a calculator for computing the convolution
 * kernel size in the spectral direction. In the case of imaging this kernel is
 * always set to zero. The calculator does not accept any configuration.</p>
 * 
 * <p>The input of the calculator is a {@link Double}, which represents the
 * wavelength (expressed in {@latex.inline \\AA}) for which the spectral direction
 * is calculated.</p>
 * 
 * <p>The output of the calculator is a {@link Double} representing the convolution
 * kernel, expressed in {@latex.inline \\AA}.</p>
 */
@Cacheable(CachingPolicy.INSTANCE)
@EqualsAndHashCode(callSuper=false)
public class Imaging extends AbstractCalculator<Tuple, Unit<Double>, Unit<Double>> {
    
    /**
     * Does nothing as this calculator does not accept any configuration.
     * @param configuration
     * @throws ConfigurationException 
     */
    @Override
    protected void validateConfiguration(Tuple configuration) throws ConfigurationException {
    }

    /**
     * Does nothing as this calculator does not accept any configuration.
     * @param configuration
     * @throws InitializationException 
     */
    @Override
    protected void initialize(Tuple configuration) throws InitializationException {
    }

    /**
     * Returns the convolution kernel size for the given wavelength.
     * @param input
     * @return
     * @throws CalculationException 
     */
    @Override
    protected Unit<Double> performCalculation(Unit<Double> input) throws CalculationException {
        return new Unit<Double>(0.);
    }

    /**
     * <p>For every calculation the calculator adds in the simulation results
     * the following outputs of the calculator:</p>
     * <ul>
     *   <li>The spectral convolution kernel size in intermediate unimportant results with code
     *       CONVOLUTION_KERNEL</li>
     * </ul>
     * @param input
     * @param output 
     */
    @Override
    protected void performForEveryCalculation(Unit<Double> input, Unit<Double> output) {
        ResultsHolder.getResults().addResult("CONVOLUTION_KERNEL", input.getValue0(),
                output.getValue0(), Units.ANGSTROM, Units.ANGSTROM, Level.INTERMEDIATE_UNIMPORTANT);
    }
    
}
