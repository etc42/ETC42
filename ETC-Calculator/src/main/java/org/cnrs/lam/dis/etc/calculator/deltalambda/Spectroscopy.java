/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.deltalambda;

import lombok.EqualsAndHashCode;
import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.apache.log4j.Logger;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.Cacheable;
import org.cnrs.lam.cesam.util.calculator.CachingPolicy;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.Calculator;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.configuration.ConfigFactory;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.StringResult;
import org.javatuples.Triplet;
import org.javatuples.Tuple;
import org.javatuples.Unit;

/**
 * <p>The {@link Spectroscopy} class is a calculator for computing the
 * {@latex.inline $\\Delta\\lambda_{(\\lambda)}$}. It is calculated as:</p>
 * {@latex.inline $
 * \\Delta\\lambda_{(\\lambda)} = \\frac{\\lambda}{R_{pix(\\lambda)}}*Pixels
 * $}
 * <p>where</p>
 * <ul>
 *   <li>{@latex.inline $\\Delta\\lambda_{(\\lambda)}$}: is the size of the wavelength step</li>
 *   <li>{@latex.inline $\\lambda$}: is the wavelength</li>
 *   <li>{@latex.inline $R_{pix(\\lambda)}$}: is the spectral resolution per pixel</li>
 * </ul>
 * 
 * <p>The Pixels is either 1 in the case the spectral quantum is "per Spectral Pixel"
 * or the spectral binning if the spectral quantum is "per Spectral Resolution Element".</p>
 * 
 * <p>The configuration of the calculator is a {@link Triplet},
 * with contains the following elements:</p>
 * <ol>
 *   <li>{@link Calculator}: A calculator for computing the spectral binning</li>
 *   <li>{@link Calculator}: A calculator for computing functions representing the
 *       spectral resolution</li>
 *   <li>{@link Boolean}: true if the spectral quantum is per spectral resolution
 *       element, false if it is per spectral pixel</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link Double}, which represents the
 * wavelength (expressed in {@latex.inline \\AA}) for which the spectral direction
 * is calculated.</p>
 * 
 * <p>The output of the calculator is a {@link Double} representing the
 * {@latex.inline $\\Delta\\lambda_{(\\lambda)}$}, expressed in {@latex.inline \\AA}.</p>
 */
@Cacheable(CachingPolicy.INSTANCE)
@EqualsAndHashCode(callSuper=false, of={"spectralBinningCalculator","spectralResolutionCalculator",
    "isPerSpectralResolutionElement"})
public class Spectroscopy extends AbstractCalculator<
        Triplet<Calculator<Unit<Double>, Unit<Double>>, 
                Calculator<Tuple, Unit<UnivariateRealFunction>>, Boolean>,
        Unit<Double>, Unit<Double>> {
    
    private final static Logger logger = Logger.getLogger(Spectroscopy.class);
    
    private Calculator<Unit<Double>, Unit<Double>> spectralBinningCalculator;
    private Calculator<Tuple, Unit<UnivariateRealFunction>> spectralResolutionCalculator;
    private boolean isPerSpectralResolutionElement;
    private UnivariateRealFunction spectralResolutionFunction;

    @Override
    protected void validateConfiguration(Triplet<
            Calculator<Unit<Double>, Unit<Double>>, 
            Calculator<Tuple, Unit<UnivariateRealFunction>>, 
            Boolean> configuration) throws ConfigurationException {
    }

    /**
     * Initializes the calculator instance with the given configuration. The first
     * value of the configuration is the spectral binning calculator, the second is
     * a calculator representing the spectral resolution and the third one is true if the
     * calculation is per spectral resolution element and false if it is per
     * spectral pixel.
     * @param configuration the configuration to use for initialization
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(Triplet<Calculator<Unit<Double>, Unit<Double>>,
            Calculator<Tuple, Unit<UnivariateRealFunction>>,
            Boolean> configuration) throws InitializationException {
        spectralBinningCalculator = configuration.getValue0();
        spectralResolutionCalculator = configuration.getValue1();
        isPerSpectralResolutionElement = configuration.getValue2();
        try {
            spectralResolutionFunction = spectralResolutionCalculator.calculate(null).getValue0();
        } catch (CalculationException ex) {
            logger.error("Failed while calculating spectral resolution function", ex);
            throw new InitializationException(ex.getMessage(), ex);
        }
    }

    /**
     * Returns the {@latex.inline $\\Delta\\lambda_{(\\lambda)}$} for the given wavelength.
     * @param input
     * @return
     * @throws CalculationException 
     */
    @Override
    protected Unit<Double> performCalculation(Unit<Double> input) throws CalculationException {
        double result;
        double pixels = isPerSpectralResolutionElement
                ? spectralBinningCalculator.calculate(input).getValue0() : 1;
        double spectralResolution = 0;
        try {
            spectralResolution = spectralResolutionFunction.value(input.getValue0());
        } catch (FunctionEvaluationException ex) {
            logger.error("Error when calculating the spectral resolution", ex);
            throw new CalculationException(ex.getMessage());
        }
        result = (input.getValue0() / spectralResolution) * pixels;
        return new Unit<Double>(result);
    }

    /**
     * <p>For every calculation the calculator adds in the simulation results
     * the following outputs of the calculator:</p>
     * <ul>
     *   <li>The delta lambda in intermediate unimportant results with code
     *       DELTA_LAMBDA</li>
     * </ul>
     * @param input
     * @param output 
     */
    @Override
    protected void performForEveryCalculation(Unit<Double> input, Unit<Double> output) {
        ResultsHolder.getResults().addResult("DELTA_LAMBDA", input.getValue0(),
                output.getValue0(), Units.ANGSTROM, Units.ANGSTROM, Level.INTERMEDIATE_UNIMPORTANT);
    }

    /**
     * <p>For every calculator instantiation the following parts of its configuration
     * are added in the simulation results:</p>
     * <ul>
     *   <li>The type of spectral quantum in debug results with code SPECTRAL_QUANTUM</li>
     * </ul>
     * @param configuration The configuration of the calculator
     */
    @Override
    protected void performForEveryRetrieval(Triplet<Calculator<Unit<Double>, Unit<Double>>,
            Calculator<Tuple, Unit<UnivariateRealFunction>>, Boolean> configuration) {
        String spectralQuantum = configuration.getValue2()
                ? "Spectral resolution element"
                : "Spectral pixel";
        ResultsHolder.getResults().addResult(new StringResult("SPECTRAL_QUANTUM"
                , spectralQuantum), Level.DEBUG);
    }
}
