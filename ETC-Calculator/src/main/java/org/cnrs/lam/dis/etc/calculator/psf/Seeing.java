/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.psf;

import java.text.MessageFormat;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TreeMap;
import lombok.EqualsAndHashCode;
import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.analysis.BivariateRealFunction;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.apache.log4j.Logger;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.Cacheable;
import org.cnrs.lam.cesam.util.calculator.CachingPolicy;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.calculator.util.functions.SymmetricBivariateGaussian;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.javatuples.Triplet;
import org.javatuples.Unit;

/**
 * <p>The {@code Seeing} class is a calculator for computing the PSF
 * of the atmosphere following a circularly symmetric bivariate gaussian distribution 
 * defined by the seeing FWHM. The equation used for the calculation of 
 * the FWHM is:</p>
 * {@latex.inline $
 * FWHM_{(\\lambda)} = seeing_{(\\lambda)} = 
 * seeing_{(Zenith\\ in\\ V-band)}*AirMass^{0.6}*\\left(\\frac{\\lambda}{5000}\\right)^{-0.2}
 * $}
 * <p>where</p>
 * <ul>
 *   <li>{@latex.inline $FWHM_{(\\lambda)}$}: is the FWHM of the PSF expressed in
 *       {@latex.inline $arcsec$}</li>
 *   <li>{@latex.inline $\\lambda$}: is the wavelength expressed in angstrom 
 *       ({@latex.inline \\AA})</li>
 *   <li>{@latex.inline $seeing_{(Zenith\\ in\\ V-band)}$}: is the seeing at zenith,
 *       for V-band, expressed in {@latex.inline $arcsec$}</li>
 *   <li>{@latex.inline $AirMass$}: is the optical path length through earths 
 *       atmosphere, relative to that at the zenith (AirMass at zenith is 1)</li>
 * </ul>
 * 
 * <p>The configuration of the calculator is a {@link org.javatuples.Triplet},
 * with contains the following elements:</p>
 * <ol>
 *   <li>{@link java.lang.Double}: The seeing in zenith for V-band</li>
 *   <li>{@link java.lang.String}: The unit of the seeing</li>
 *   <li>{@link java.lang.Double}: The air mass</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the wavelength for which the calculation
 * is done (expressed in {@latex.inline \\AA}).</p>
 * 
 * <p>The output of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link BivariateRealFunction} representing the PSF (both axes are expressed in
 * {@latex.inline $arcsec$}).</p>
 */
@Cacheable(CachingPolicy.INSTANCE)
@EqualsAndHashCode(callSuper=false)
public class Seeing extends AbstractCalculator<Triplet<Double, String, Double>, Unit<Double>, Unit<BivariateRealFunction>> {

    private static final Logger logger = Logger.getLogger(DiffractionLimited.class);
    private static final ResourceBundle validationErrorsBundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");
    private double seeing;
    private double airMass;
    
    /**
     * Validates the given configuration. It requires that the first value of the
     * tuple (the seeing) is a positive number, that the second value (the unit
     * of the seeing) is {@latex.inline$arcsec$} and that the third value (the
     * air mass) is bigger or equal than 1.
     * 
     * @param configuration The configuration to validate
     * @throws ConfigurationException if the configuration is invalid
     */
    @Override
    protected void validateConfiguration(Triplet<Double, String, Double> configuration) throws ConfigurationException {
        // Check that the seeing is a possitive number
        if (configuration.getValue0() <= 0) {
            String message = validationErrorsBundle.getString("SEEING_NOT_POSITIVE");
            message = MessageFormat.format(message, configuration.getValue0());
            throw new ConfigurationException(message);
        }
        // Check if the seeing is in arcsec
        if (!Units.isArcsec(configuration.getValue1())) {
            String message = validationErrorsBundle.getString("SEEING_WRONG_UNIT");
            message = MessageFormat.format(message, Units.ARCSEC, configuration.getValue0());
            throw new ConfigurationException(message);
        }
        // Check that the airmass is bigger or equal to 1
        if (configuration.getValue2() < 1) {
            String message = validationErrorsBundle.getString("AIR_MASS_LESS_THAN_ONE");
            message = MessageFormat.format(message, configuration.getValue0());
            throw new ConfigurationException(message);
        }
    }

    /**
     * Initializes the calculator instance with the given configuration. The first
     * value of the configuration is the seeing at zenith for V-band, the second
     * is is unit and the third one is the air mass.
     * @param configuration The configuration of the calculator
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(Triplet<Double, String, Double> configuration) throws InitializationException {
        seeing = configuration.getValue0();
        airMass = configuration.getValue2();
    }

    /**
     * Returns a {@link org.javatuples.Unit} containing a bivariate function, of
     * which both axes are expressed in {@latex.inline $arcsec$}, which represents
     * the PSF for the given wavelength.
     * @param input The wavelength expressed in {@latex.inline \\AA}
     * @return A function representing the PSF
     * @throws CalculationException if there is any error during the calculation
     */
    @Override
    protected Unit<BivariateRealFunction> performCalculation(Unit<Double> input) throws CalculationException {
        double lambda = input.getValue0();
        double fwhm = seeing * Math.pow((lambda) / 5000., -0.2) * Math.pow(airMass, 0.6);
        double sigma = fwhm / 2.3548;
        SymmetricBivariateGaussian gaussian = new SymmetricBivariateGaussian(sigma);
        return new Unit<BivariateRealFunction>(gaussian);
    }

    /**
     * <p>For every calculation the calculator adds in the simulation results
     * the following outputs of the calculator:</p>
     * <ul>
     *   <li>The FWHM of the atmospheres PSF in intermediate unimportant results
     *       with code ATMOSPHERE_PSF_FWHM</li>
     *   <li> The projection of the PSF on the plane y=0 in the intermediate
     *       unimportant results with code ATMOSPHERE_PSF_PROJECTION</li>
     * </ul>
     * @param input
     * @param output 
     */
    @Override
    protected void performForEveryCalculation(Unit<Double> input, Unit<BivariateRealFunction> output) {
        double lambda = input.getValue0();
        // We know that we have a symmetric bivariate gaussian so the casting is safe
        SymmetricBivariateGaussian gaussian = (SymmetricBivariateGaussian) output.getValue0();
        double sigma = gaussian.getStandardDeviation();
        
        // We add the FWHM of the PSF in the results
        double fwhm = sigma * 2.3548;
        ResultsHolder.getResults().addResult("ATMOSPHERE_PSF_FWHM", lambda, fwhm, 
                Units.ANGSTROM, Units.ARCSEC, CalculationResults.Level.INTERMEDIATE_UNIMPORTANT);
        
        // We add the projection on the y = 0 plane in the results. We calculate
        // the valuse in the range of 5 sigma, as all the values outside of this
        // are too close to zero.
        UnivariateRealFunction projetion = gaussian.projectionFunction();
        Map<Double, Double> projectionData = new TreeMap<Double, Double>();
        // We will create 100 points to represent the plot
        double step = sigma / 20;
        for (double x = -5 * sigma; x <= 5 * sigma; x += step) {
            double value = 0;
            try {
                value = projetion.value(x);
            } catch (FunctionEvaluationException ex) {
                logger.warn("Failed to calculate the value of the PSF projection", ex);
            }
            projectionData.put(x, value);
        }
        CalculationResults.DoubleDatasetResult projectionResult = 
                new CalculationResults.DoubleDatasetResult("ATMOSPHERE_PSF_PROJECTION", projectionData, Units.ARCSEC, null);
        ResultsHolder.getResults().addResult("ATMOSPHERE_PSF_PROJECTION", lambda, projectionResult,
                Units.ANGSTROM, CalculationResults.Level.INTERMEDIATE_UNIMPORTANT);
    }

    /**
     * <p>For every calculator instantiation the following parts of its configuration
     * are added in the simulation results:</p>
     * <ul>
     *   <li>The value of the seeing in zenith for V-Band in debug results with
     * code SEEING</li>
     *   <li>The value of the Air Mass in debug results with code AIR_MASS</li>
     * </ul>
     * @param configuration The configuration of the calculator
     */
    @Override
    protected void performForEveryRetrieval(Triplet<Double, String, Double> configuration) {
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("SEEING",
                configuration.getValue0(), configuration.getValue1()), CalculationResults.Level.DEBUG);
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("AIR_MASS",
                configuration.getValue2(), null), CalculationResults.Level.DEBUG);
    }
    
}
