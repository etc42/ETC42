/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.util.functions;

import lombok.EqualsAndHashCode;
import org.apache.commons.math.FunctionEvaluationException;
import org.javatuples.Pair;

/**
 * <p>The {@link DoubleGaussian} class represents a scaled double Gaussian with
 * peak value for X=0. The double Gaussian is calculated as:</p>
 * {@latex.inline $
 * f_{(x)}=scale*(w*f_{\\sigma_1(x)}+(1-w)*f_{\\sigma_2(x)})
 * $}
 * <p>where</p>
 * <ul>
 *   <li>{@latex.inline $scale$}: is the scale factor of the double Gaussian</li>
 *   <li>{@latex.inline $w$}: is the weight of the double Gaussian</li>
 *   <li>{@latex.inline $f_{\\sigma_1(x)}$}: is a Gaussian with standard deviation
 *       {@latex.inline $\\sigma_1$}</li>
 *   <li>{@latex.inline $f_{\\sigma_2(x)}$}: is a Gaussian with standard deviation
 *       {@latex.inline $\\sigma_2$}</li>
 * </ul>
 */
@EqualsAndHashCode(callSuper=false)
public class DoubleGaussian implements IntegrableUnivariateFunction {
    
    private final Gaussian firstGaussian;
    private final Gaussian secondGaussian;
    private final double weight;
    private final double scale;
    
    /**
     * Constructs a new {@link DoubleGaussian} which consists of two Gaussians
     * with the given standard deviations and weight of the first the given one
     * and weight of the second one minus the given one. Note that the scale
     * factor used is one.
     * @param firstStandardDeviation  The standard deviation of the first Gaussian
     * @param secondStandardDeviation The standard deviation of the second Gaussian
     * @param weight The weight with which the first Gaussian is multiplied. The
     * second is multiplied with 1-weight.
     * @throws IllegalArgumentException if the weight is out of the range [0,1]
     */
    public DoubleGaussian(double firstStandardDeviation,
            double secondStandardDeviation, double weight) {
        this(firstStandardDeviation, secondStandardDeviation, weight, 1.);
    }
    
    /**
     * Constructs a new {@link DoubleGaussian} which consists of two scaled Gaussians
     * with the given standard deviations and weight of the first the given one
     * and weight of the second one minus the given one.
     * @param firstStandardDeviation  The standard deviation of the first Gaussian
     * @param secondStandardDeviation The standard deviation of the second Gaussian
     * @param weight The weight with which the first Gaussian is multiplied. The
     * second is multiplied with 1-weight.
     * @param scale The scale factor of the double gaussian
     * @throws IllegalArgumentException if the weight is out of the range [0,1]
     */
    public DoubleGaussian(double firstStandardDeviation,
            double secondStandardDeviation, double weight, double scale) {
        if (weight < 0 || weight > 1) {
            throw new IllegalArgumentException("Double Gaussian weight must be in range [0,1] but was " + weight);
        }
        this.weight = weight;
        this.scale = scale;
        firstGaussian = new Gaussian(firstStandardDeviation, scale);
        secondGaussian = new Gaussian(secondStandardDeviation, scale);
    }
    
    /**
     * Returns a {@link Pair} containing the standard deviations of the two Gaussians.
     * The first value is the standard deviation of the Gaussian multiplied with
     * the weight and the second of the one multiplied by 1-weight.
     * @return
     */
    public Pair<Double, Double> getStandardDeviations() {
        double firstSigma = firstGaussian.getStandardDeviation();
        double secondSigma = secondGaussian.getStandardDeviation();
        return new Pair<Double, Double>(firstSigma, secondSigma);
    }
    
    /**
     * Returns the weight with which the first Gaussian is multiplied. The weight
     * of the second one is 1-weight.
     * @return 
     */
    public double getWeight() {
        return weight;
    }
    
    /**
     * Returns the scale factor with which the double Gaussian is multiplied with.
     * @return 
     */
    public double getScale() {
        return scale;
    }

    /**
     * Returns the value of the Double Gaussian for the distance x from the
     * location of the peak.
     * @param x The distance from the peak
     * @return The value of the Gaussian
     * @throws FunctionEvaluationException 
     */
    @Override
    public double value(double x) throws FunctionEvaluationException {
        double firstValue = firstGaussian.value(x);
        double secondValue = secondGaussian.value(x);
        // Note that the gaussians are already scaled, so we do not multiply again with the scale
        double value = weight * firstValue + (1 - weight) * secondValue;
        return value;
    }

    /**
     * Calculates the integral of the double Gaussian.
     * @param x1 The low bound of the interval
     * @param x2 The high bound of the interval
     * @return the integral in the interval [x1,x2]
     * @throws FunctionEvaluationException if the integral evaluation fails
     * @throws IllegalArgumentException if x1 > x2
     */
    @Override
    public double integral(double x1, double x2) throws FunctionEvaluationException {
        // First check the x1 and x2. Note that if they are equal the integral is 0.
        if (x1 > x2) {
            throw new IllegalArgumentException("Illegal integration range [" + x1 + "," + x2 + "]");
        }
        if (x1 == x2) {
            return 0;
        }
        // Note that the gaussians are already scaled, so we do not multiply again with the scale
        double integral = weight * firstGaussian.integral(x1, x2) + (1 - weight) * secondGaussian.integral(x1, x2);
        return integral;
    }
    
}
