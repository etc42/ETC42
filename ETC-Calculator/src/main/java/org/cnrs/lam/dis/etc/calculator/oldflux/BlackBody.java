/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.oldflux;

import lombok.EqualsAndHashCode;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Constants;
import org.cnrs.lam.dis.etc.calculator.util.FluxUtil;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.DoubleValueResult;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.StringResult;
import org.cnrs.lam.dis.etc.datamodel.FilterBand;
import org.javatuples.Quintet;
import org.javatuples.Unit;

/**
 * <p>The {@code BlackBody} class is a calculator for computing the flux of the
 * source, which is assumed to be a redshifted black body. To calculate the flux it first
 * calculates the black body radiation per unit projected area, by using the
 * equation:</p>
 * {@latex.inline $
 * I_{(\\lambda)} = \\displaystyle\\frac{2hc^2}{\\lambda^5}
 * \\displaystyle\\frac{1}{e^{\\frac{hc}{\\lambda kT_z}} - 1}
 * $}
 * <p>where</p>
 * <ul>
 *   <li>{@latex.inline $I_{(\\lambda)}$}: is the black body radiation</li>
 *   <li>{@latex.inline $h$}: is the Planck constant expressed in {@latex.inline $erg∗s$}</li>
 *   <li>{@latex.inline $c$}: is the speed of light expressed in
 *       {@latex.inline \\AA$/s$}</li>
 *   <li>{@latex.inline $\\lambda$}: is the wavelength expressed in {@latex.inline \\AA}</li>
 *   <li>{@latex.inline $T_z$}: is the black body redshifted temperature expressed in {@latex.inline $K$}
 *       and is calculated as {@latex.inline $T_z = \\frac{T}{1+z}$}, where {@latex.inline $T$}
 *       is the temperature of the body at redshift 0 and {@latex.inline $z$} is the redshift.</li>
 * </ul>
 * 
 * <p>Then this radiation is normalized by using the flux calculated from the AB
 * magnitude:</p>
 * {@latex.inline $
 * F = 10^{-\\frac{m_{AB}+48.6}{2.5}}*\\displaystyle\\frac{c}{\\lambda_{x}^2}
 * $}
 * <p>where</p>
 * <ul>
 *   <li>{@latex.inline $F$}: is the flux on which the radiation will be normalized,
 *       expressed in {@latex.inline $erg/s/cm^2/$\\AA}</li>
 *   <li>{@latex.inline $m_{AB}$}: is the AB magnitude</li>
 *   <li>{@latex.inline $c$}: is the speed of light expressed in
 *       {@latex.inline \\AA$/s$}</li>
 *   <li>{@latex.inline $\\lambda_{x}$}: is the central wavelength of the filter
 *       for which the AB magnitude is given, expressed in {@latex.inline \\AA}</li>
 * </ul>
 * 
 * <p>Note that the units of the blackbody radiation are not important, as the
 * radiation is being normalized.</p>
 * 
 * <p>The configuration of the calculator is a {@link org.javatuples.Quintet},
 * with contains the following elements:</p>
 * <ol>
 *   <li>{@link java.lang.Double}: The AB magnitude</li>
 *   <li>{@link org.cnrs.lam.dis.etc.datamodel.FilterBand}: The filter for which
 *       theAB magnitude is given</li>
 *   <li>{@link java.lang.Double}: The temperature of the black body</li>
 *   <li>{@link java.lang.String}: The unit of the temperature</li>
 *   <li>{@link java.lang.Double}: The redshift</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the wavelength for which the calculation
 * is done (expressed in {@latex.inline \\AA}).</p>
 * 
 * <p>The output of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the signal flux (expressed in
 * {@latex.inline $erg/s/cm^2/$\\AA}).</p>
 */
@EqualsAndHashCode(callSuper = false, exclude = {"factor"})
public class BlackBody extends AbstractCalculator<
        Quintet<Double, Double, Double, String, Double>, Unit<Double>, Unit<Double>> {
    
    private double magnitude;
    private double magnitudeWavelength;
    private double temperature;
    private double redshift;
    
    private double factor;

    /**
     * Validates the given configuration. It requires that the second
     * value (the filter band) is not null, that the third one (the blackbody
     * temperature) is a positive number, that the fourth
     * value (the unit of the blackbody temperature) is in Kelvins and that the
     * fifth value (the redshift) is not negative.
     * @param configuration The configuration to validate
     * @throws ConfigurationException If the configuration is invalid
     */
    @Override
    protected void validateConfiguration(
            Quintet<Double, Double, Double, String, Double> configuration) throws ConfigurationException {
        if (configuration.getValue1() == null) {
            throw new ConfigurationException("Filter band of AB magnitude cannot be null");
        }
        if (configuration.getValue2() <= 0) {
            throw new ConfigurationException("Blackbody temperature must be a possitive number " +
                    "but was " + configuration.getValue2());
        }
        if (!Units.isKelvins(configuration.getValue3())) {
            throw new ConfigurationException("Blackbody temperature must be in " +
                    Units.KELVINS + " but was in " + configuration.getValue3());
        }
        if (configuration.getValue4() < 0) {
            throw new ConfigurationException("Redshift can not be a negative number " +
                    "but was " + configuration.getValue4());
        }
    }
    
    /**
     * Initializes the calculator instance with the given configuration. The first
     * value of the configuration is the AB magnitude, the second is the filter
     * band, the third is the blackbody temperature, the fourth is 
     * the unit of the blackbody temperature and the fifth is the redshift.
     * @param configuration The configuration to use for initialization
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(Quintet<Double, Double, Double, String, Double> configuration) throws InitializationException {
        magnitude = configuration.getValue0();
        magnitudeWavelength = configuration.getValue1();
        temperature = configuration.getValue2();
        redshift = configuration.getValue4();
        
        // Calculate the factor for converting the black body radiation to flux
        double abFlux = FluxUtil.convertMagnitudeToFlux(magnitude, magnitudeWavelength);
        double radiation = calculateBlackBodyRadiation(magnitudeWavelength);
        factor = abFlux / radiation;
    }
    
    /**
     * Returns a {@link org.javatuples.Unit} containing the signal flux,
     * expressed in {@latex.inline $erg/s/cm^2/$\\AA}.
     * @param input The wavelength expressed in {@latex.inline \\AA}
     * @return The signal flux expressed in {@latex.inline $erg/s/cm^2/$\\AA}
     * @throws CalculationException if there is any error during the calculation
     */
    @Override
    protected Unit<Double> performCalculation(Unit<Double> input) throws CalculationException {
        double radiation = calculateBlackBodyRadiation(input.getValue0());
        double flux = radiation * factor;
        return new Unit<Double>(flux);
    }
    
    /**
     * Calculates the black body radiation per unit projected area for the given
     * wavelength.
     * @param lambda The wavelength for which the radiation is calculated
     * @return the radiation
     */
    private double calculateBlackBodyRadiation(double lambda) {
        double a = 2 * Constants.PLANK * Constants.SPEED_OF_LIGHT * Constants.SPEED_OF_LIGHT; // 2*h*c^2
        double b = Math.pow(lambda,5); // lambda^5
        double c = Constants.PLANK * Constants.SPEED_OF_LIGHT; // h*c
        double d = lambda * Constants.BOLTZMANN * (temperature / (1 + redshift)); // lambda*k* (T/(1+z))
        double e = Math.exp(c/d); // e^(h*c/(lambda*k*T/(1+z)))
        double radiation = (a/b)/(e-1);
        return radiation;
    }
    
    /**
     * Overridden to add in the results the signal flux This is done here, so
     * we have this results even if the result is retrieved from the cache.
     * @param input The wavelength expressed in {@latex.inline \\AA}
     * @param output The signal flux expressed in {@latex.inline $erg/s/cm^2/$\\AA}
     */
    @Override
    protected void performForEveryCalculation(Unit<Double> input, Unit<Double> output) {
        ResultsHolder.getResults().addResult("SIGNAL_FLUX", input.getValue0(), output.getValue0(),
                Units.ANGSTROM, Units.getErgPerCm2PerSecPerAngstrom(), Level.INTERMEDIATE_UNIMPORTANT);
    }
    
    /**
     * Overridden to add in the results the AB magnitude, the filter of the AB
     * magnitude and the blackbody temperature.
     * @param configuration The configuration of the calculator
     */
    @Override
    protected void performForEveryRetrieval(Quintet<Double, Double, Double, String, Double> configuration) {
        ResultsHolder.getResults().addResult(new DoubleValueResult("SOURCE_AB_MAGNITUDE"
                , configuration.getValue0(), null), Level.DEBUG);
        ResultsHolder.getResults().addResult(new StringResult("SOURCE_AB_MAGNITUDE_FILTER"
                , configuration.getValue1().toString()), Level.DEBUG);
        ResultsHolder.getResults().addResult(new DoubleValueResult("BLACKBODY_TEMPERATURE",
                configuration.getValue2(), configuration.getValue3()), Level.DEBUG);
        ResultsHolder.getResults().addResult(new DoubleValueResult("REDSHIFT"
        , configuration.getValue4(), null), Level.DEBUG);
    }



}
