/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.backgroundflux;

import java.util.ResourceBundle;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.galacticflux.GalacticFluxFactory;
import org.cnrs.lam.dis.etc.calculator.skyflux.SkyFluxFactory;
import org.cnrs.lam.dis.etc.calculator.zodiacalflux.ZodiacalFluxFactory;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.StringResult;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.javatuples.Triplet;
import org.javatuples.Tuple;
import org.javatuples.Unit;

/**
 */
public class BackgroundFluxFactory implements Factory<Unit<Session>, Tuple, Unit<UnivariateRealFunction>> {

    private static final ResourceBundle bundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");

    @Override
    public Calculator<Tuple, Unit<UnivariateRealFunction>> getCalculator(Unit<Session> configuration) throws InitializationException, ConfigurationException {
        Calculator<Tuple, Unit<UnivariateRealFunction>> skyFlux = new SkyFluxFactory().getCalculator(configuration);
        switch (configuration.getValue0().getSite().getLocationType()) {
            case GROUND:
                ResultsHolder.getResults().addResult(
                        new StringResult("BACKGROUND_FLUX_METHOD", "Ground"), Level.DEBUG);
                Unit<Calculator<Tuple, Unit<UnivariateRealFunction>>> groundConfiguration =
                        new Unit<Calculator<Tuple, Unit<UnivariateRealFunction>>>(skyFlux);
                return EtcCalculatorManager.getManager(Ground.class).getCalculator(groundConfiguration);
            case SPACE:
                ResultsHolder.getResults().addResult(
                        new StringResult("BACKGROUND_FLUX_METHOD", "Space"), Level.DEBUG);
                Calculator<Tuple, Unit<UnivariateRealFunction>> zodiacalFlux = new ZodiacalFluxFactory().getCalculator(configuration);
                Calculator<Tuple, Unit<UnivariateRealFunction>> galacticFlux = new GalacticFluxFactory().getCalculator(configuration);
                Triplet<Calculator<Tuple, Unit<UnivariateRealFunction>>
                        , Calculator<Tuple, Unit<UnivariateRealFunction>>
                        , Calculator<Tuple, Unit<UnivariateRealFunction>>>
                        spaceConfiguration = new Triplet<
                            Calculator<Tuple, Unit<UnivariateRealFunction>>
                            , Calculator<Tuple, Unit<UnivariateRealFunction>>
                            , Calculator<Tuple, Unit<UnivariateRealFunction>>
                            >(skyFlux, zodiacalFlux, galacticFlux);
                return EtcCalculatorManager.getManager(Space.class).getCalculator(spaceConfiguration);
        }
        String message = bundle.getString("UNKNOWN_BACKGROUND_FLUX_METHOD");
        throw new ConfigurationException(message);
    }
    
}
