/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.util.datasets;

import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.SortedMap;
import org.apache.commons.math.FunctionEvaluationException;
import org.cnrs.lam.dis.etc.calculator.util.CautionMessage;
import org.cnrs.lam.dis.etc.calculator.util.functions.BoundedUnivariateFunction;
import org.cnrs.lam.dis.etc.calculator.util.functions.IntegrableUnivariateFunction;
import org.cnrs.lam.dis.etc.calculator.wavelengthrange.CommonRangeFactory;
import org.cnrs.lam.dis.etc.calculator.wavelengthrange.WavelengthRangeFactory;
import org.javatuples.Pair;

/**
 * The {@link TemplateFunctionDataset} is a {@link Dataset} the nodes of which
 * represent a template. It implements the {@link BoundedUnivariateRealFunction}
 * and {@link IntegrableUnivariateRealFunction} interfaces.
 */
public class TemplateFunctionDataset extends Dataset implements
        BoundedUnivariateFunction, IntegrableUnivariateFunction {
    
    private static final ResourceBundle bundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");
    
    private double minRange;
    private double maxRange;
    
    /**
     * Constructs a new instance of {@link TemplateFunctionDataset} with the given
     * data representing the template values. The keys of the map represent
     * the values of the X axis and the values of the map the template.
     * @param data The nodes of the function
     */
    public TemplateFunctionDataset(Map<Double, Double> data) {
        super(data);
        // We recalculate the min and max range because we can calculate values
        // for a range a little bit bigger than the minimum and maximum keys
        Pair<Double, Double> superBounds = super.getBounds();
        double superMin = superBounds.getValue0();
        double superMax = superBounds.getValue1();
        double higherNodeKey = getHigherDataNode(superMin).getKey();
        minRange = superMin - (higherNodeKey - superMin) / 2;
        double lowerNodeKey = getLowerDataNode(superMax).getKey();
        maxRange = superMax + (superMax - lowerNodeKey) / 2;
    }
    
    /**
     * Returns the value of the function for the given X value. If the requested
     * value is out of range a {@link FunctionEvaluationException} is thrown.
     * @param x The X value for which the function is calculated
     * @return The value of the function for the given X value
     * @throws FunctionEvaluationException if the given X value is out of range
     */
    @Override
    public double value(double x) throws FunctionEvaluationException {
        // First make a check that we are inside the bounds
        Pair<Double, Double> bounds = getBounds();
        if (x < bounds.getValue0() || x > bounds.getValue1()) {
            throw new FunctionEvaluationException(x);
        }
        
        // We get the floor and ceiling entries from the data.
        Map.Entry<Double, Double> floorDataNode = getFloorDataNode(x);
        Map.Entry<Double, Double> ceilingDataNode = getCeilingDataNode(x);
        // If both nodes are null something is wrong
        if (floorDataNode == null && ceilingDataNode == null) {
            throw new FunctionEvaluationException(x);
        }
        
        // Check if we are outside of the edge nodes
        if (floorDataNode == null) {
            double higherKey = getHigherDataNode(ceilingDataNode.getKey()).getKey();
            return ceilingDataNode.getValue() / (higherKey - ceilingDataNode.getKey());
        }
        if (ceilingDataNode == null) {
            double lowerKey = getLowerDataNode(floorDataNode.getKey()).getKey();
            return floorDataNode.getValue() / (floorDataNode.getKey() - lowerKey);
        }
        
        // Here we know we are between two nodes
        double floorX = floorDataNode.getKey();
        double floorValue = floorDataNode.getValue();
        double ceilingX = ceilingDataNode.getKey();
        double ceilingValue = ceilingDataNode.getValue();
        
        // We check to which template node this X belongs. Note that if the X
        // is exactly on a node the floor and ceiling will be the same.
        double nodeX;
        double nodeValue;
        if (x - floorX < ceilingX - x) {
            nodeX = floorX;
            nodeValue = floorValue;
        } else {
            nodeX = ceilingX;
            nodeValue = ceilingValue;
        }
        // Now we need the keys of the previous and next nodes to calcualte the
        // area in which the node value is distributed
        Entry<Double, Double> lowerDataNode = getLowerDataNode(nodeX);
        Entry<Double, Double> higherDataNode = getHigherDataNode(nodeX);
        // If any of the nodes does not exist we just take the double of the other
        // node distance
        double value;
        if (lowerDataNode == null) {
            value = nodeValue / (higherDataNode.getKey() - nodeX);
        } else if (higherDataNode == null) {
            value = nodeValue / (nodeX - lowerDataNode.getKey());
        } else {
            value = nodeValue * 2 / (higherDataNode.getKey() - lowerDataNode.getKey());
        }
        
        return value;
    }

    /**
     * Returns the integral of the represented function between the values X1 and X2.
     * @param x1 The low bound of the integration
     * @param x2 The high bound of the integration
     * @return The integral of the function in the range [x1,x2]
     * @throws FunctionEvaluationException if the range is out of bounds or x1 > x2
     * @throws IllegalArgumentException if the X1 is greater than X2
     */
    @Override
    public double integral(double x1, double x2) throws FunctionEvaluationException {
        // First check the x1 and x2. Note that if they are equal the integral is 0.
        if (x1 > x2) {
            throw new IllegalArgumentException("Illegal integration range [" + x1 + "," + x2 + "]");
        }
        if (x1 == x2) {
            return 0;
        }
        /*
        // Check that the integration range is inside the bounds of the function
        Pair<Double, Double> bounds = getBounds();
        if (x2 > bounds.getValue1() || x1 < bounds.getValue0()) {
        	if (CommonRangeFactory.getCanDisplayRangeError())
        		CautionMessage.getInstance().addMessage("INTEGRAL_OUT_OF_BOUNDS", bundle.getString("INTEGRAL_OUT_OF_BOUNDS"));
            //throw new FunctionEvaluationException(x2);
        }
        */
        
        double integral = 0;
        // We get all the nodes which are between the x1 and x2.
        SortedMap<Double, Double> nodesBetweenX1X2 = getData().subMap(x1, x2);
        
        // For each node between X1 and X2 we add its contribution to the integral
        for (Entry<Double, Double> entry : nodesBetweenX1X2.entrySet()) {
            Double key = entry.getKey();
            integral += integralInNode(key, x1, x2);
        }
        Set<Double> extraCheckedNodes = new HashSet<Double>();
        // We get the lower, higher, floor and ceiling nodes and we add their integral also. We
        // take care to not add them twice if they already have been added.
        Entry<Double, Double> floorDataNode = getFloorDataNode(x1);
        if (floorDataNode != null) {
            double floorKey = floorDataNode.getKey();
            if (!nodesBetweenX1X2.containsKey(floorKey) && !extraCheckedNodes.contains(floorKey)) {
                integral += integralInNode(floorKey, x1, x2);
            }
            extraCheckedNodes.add(floorKey);
        }
        Entry<Double, Double> ceilingDataNode = getCeilingDataNode(x2);
        if (ceilingDataNode != null) {
            double ceilingKey = ceilingDataNode.getKey();
            if (!nodesBetweenX1X2.containsKey(ceilingKey) && !extraCheckedNodes.contains(ceilingKey)) {
                integral += integralInNode(ceilingKey, x1, x2);
            }
            extraCheckedNodes.add(ceilingKey);
        }
        Entry<Double, Double> lowerDataNode = getLowerDataNode(x1);
        if (lowerDataNode != null) {
            double lowerKey = lowerDataNode.getKey();
            if (!nodesBetweenX1X2.containsKey(lowerKey) && !extraCheckedNodes.contains(lowerKey)) {
                integral += integralInNode(lowerKey, x1, x2);
            }
            extraCheckedNodes.add(lowerKey);
        }
        Entry<Double, Double> higherDataNode = getHigherDataNode(x2);
        if (higherDataNode != null) {
            double higherKey = higherDataNode.getKey();
            if (!nodesBetweenX1X2.containsKey(higherKey) && !extraCheckedNodes.contains(higherKey)) {
                integral += integralInNode(higherKey, x1, x2);
            }
            extraCheckedNodes.add(higherKey);
        }
        return integral;
    }
    
    /**
     * Returns the part of the integral of the node specified by the given key,
     * for the range x1, x2.
     * @param key
     * @param x1
     * @param x2
     * @return 
     */
    private double integralInNode(double key, double x1, double x2) {
        // Check that the given key is representing a node
        Double nodeValue = getData().get(key);
        if (nodeValue == null) {
            throw new IllegalArgumentException("The given key was not representing a node");
        }
        // We get the neighbour nodes
        Entry<Double, Double> lowerDataNode = getLowerDataNode(key);
        Entry<Double, Double> higherDataNode = getHigherDataNode(key);
        // We calcualte the lower and higher middle points
        double lowerMiddleSize = (lowerDataNode != null)
                ? (key - lowerDataNode.getKey()) / 2
                : (higherDataNode.getKey() - key) / 2;
        double lowerMiddle = key - lowerMiddleSize;
        double higherMiddleSize = (higherDataNode != null)
                ? (higherDataNode.getKey() - key) / 2
                : (key - lowerDataNode.getKey()) / 2;
        double higherMiddle = key + higherMiddleSize;
        // Now we calculate the part of the node value which contributes to the integral
        if (x1 >= higherMiddle || x2 <= lowerMiddle) {
            return 0;
        } else if (x1 <= lowerMiddle && x2 >= higherMiddle) {
            return nodeValue;
        } else {
            return nodeValue * (Math.min(x2, higherMiddle) - Math.max(x1, lowerMiddle)) / (higherMiddle - lowerMiddle);
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public Pair<Double, Double> getBounds() {
        return new Pair<Double, Double>(minRange, maxRange);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Pair<Double, Double> getNonZeroBounds() {
        return super.getNonZeroBounds();
    }
    
}