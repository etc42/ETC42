/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.util;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.MaxIterationsExceededException;
import org.apache.commons.math.analysis.BivariateRealFunction;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.apache.commons.math.analysis.integration.RombergIntegrator;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.Calculator;
import org.cnrs.lam.dis.etc.configuration.ConfigFactory;
import org.javatuples.Triplet;
import org.javatuples.Unit;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class IntegrationUtil {

    private static class ConstantYFunction implements UnivariateRealFunction {
        private final BivariateRealFunction function;
        private final double y;

        public ConstantYFunction(BivariateRealFunction function, double y) {
            this.function = function;
            this.y = y;
        }

        @Override
        public double value(double x) throws FunctionEvaluationException {
            return function.value(x, y);
        }
    }

    private static class IntermediateIntegral implements UnivariateRealFunction {

        private final BivariateRealFunction function;
        private final double a1;
        private final double b1;

        public IntermediateIntegral(BivariateRealFunction function, double a1, double b1) {
            this.function = function;
            this.a1 = a1;
            this.b1 = b1;
        }

        @Override
        public double value(double y) throws FunctionEvaluationException {
            double result = 0;
            try {
                result = univariateIntegral(new ConstantYFunction(function, y), a1, b1);
            } catch (Exception ex) {
                throw new FunctionEvaluationException(ex, y);
            }
            return result;
        }

    }

    /**
     * b2 b1 <br>
     * ∫  ∫ bI ( x, y )dxdy<br>
     * a2 a1
     */
    public static double bivariateIntegral(BivariateRealFunction function, double a1, double b1, double a2, double b2) throws MaxIterationsExceededException, FunctionEvaluationException {
        RombergIntegrator integrator = new RombergIntegrator();
        return integrator.integrate(new IntermediateIntegral(function, a1, b1), a2, b2);
    }
    
    /**b<br>
     * ∫ f(x)dx<br>
     * a<br>
     */
    public static double univariateIntegral(UnivariateRealFunction function, double a, double b) throws MaxIterationsExceededException, FunctionEvaluationException {
        RombergIntegrator integrator = new RombergIntegrator();
        return integrator.integrate(function, a, b);
    }

    /**
     * Implements the two dimensional integral of the given function:<br>
     * b2 b1 <br>
     * ∫  ∫ bI ( x, y )dxdy<br>
     * a2 a1<br>
     * Note that the given function must be a circularly symmetric function. The
     * method used is a Monte Carlo based integration which uses as reference point
     * the integral of the function in a circle area. This is computed by using
     * one dimension integration of the function and the fact that it is circularly
     * symmetric.
     */
    public static double monteCarloBivariateIntegralOfCircularlySymmetric(UnivariateRealFunction function, double a1, double b1, double a2, double b2) throws MaxIterationsExceededException, FunctionEvaluationException {
        int STEP_NO_OF_POINTS = 1000; // The number of points to add in each iteration
        int MAX_ITER_NO = 10000; // The maximum number of iterations
        double STEP_ACCURACY = 0.01; // The accuracy of the simulation
        int ACCURATE_STEPS_TO_STOP = 100; // The number of accurate steps we need to stop
        
        // First we calculate the radius or a circle in X-Y plane with center the (0,0)
        // which contains all the limits of the integral and the area of this circle
        double maxRadius = 0;
        maxRadius = Math.max(maxRadius, Math.sqrt(a1*a1 + a2*a2));
        maxRadius = Math.max(maxRadius, Math.sqrt(b1*b1 + a2*a2));
        maxRadius = Math.max(maxRadius, Math.sqrt(b1*b1 + b2*b2));
        maxRadius = Math.max(maxRadius, Math.sqrt(a1*a1 + b2*b2));
        double maxRadiusSqr = maxRadius * maxRadius;
        double baseArea = Math.PI * maxRadiusSqr;
        double stepAreaFactor = baseArea / STEP_NO_OF_POINTS;
        double totalAreaFactor = stepAreaFactor / ACCURATE_STEPS_TO_STOP;
        
        // We calculate the integral of our function in this circle. This is easy and fast,
        // because we have a circularly symmetric function. We will use this volume for
        // checking the simulation results.
        PolarIntegralFunction polarIntegrationFunction = new PolarIntegralFunction(function);
        double totalVolume = 2 * Math.PI * univariateIntegral(polarIntegrationFunction, 0, maxRadius);
        
        // The list where we put the points that have a nice fit
        List<Triplet<Double, Double, Double>> totalPointList = new ArrayList<Triplet<Double, Double, Double>>();
        
        // We start the iteration
        int iterNo = 0; // This tells us in which iteration we are
        int accurateStepsFound = 0; // This is how many steps were accurate till now
        while (accurateStepsFound < ACCURATE_STEPS_TO_STOP && iterNo < MAX_ITER_NO) {
            iterNo++;
            //We create the points for the step
            int pointsToCreate = STEP_NO_OF_POINTS;
            double stepValueOfPoints = 0;
            List<Triplet<Double, Double, Double>> stepPointList = new ArrayList<Triplet<Double, Double, Double>>();
            while (pointsToCreate > 0) {
                double x = Math.random();
                x = (2 * x - 1) * maxRadius;
                double y = Math.random();
                y = (2 * y - 1) * maxRadius;
                // We keep a point only if it is inside the circle
                double r = Math.sqrt(x * x + y * y);
                if (r <= maxRadius) {
                    double value = function.value(r);
                    stepPointList.add(new Triplet<Double, Double, Double>(x, y, value));
                    stepValueOfPoints += value;
                    pointsToCreate--;
                }
            }
            // We check the step accuracy and if it is accurate enough we add
            // the step points to the total
            double estimatedStepVolume = stepValueOfPoints * stepAreaFactor;
            double error = Math.abs((totalVolume - estimatedStepVolume) / totalVolume);
            if (error <= STEP_ACCURACY) {
                // The accuracy of the step is good enough. We add it in the total.
                accurateStepsFound++;
                totalPointList.addAll(stepPointList);
            }
        }
        
        // If we reached here and we don't have the correct number of accurate
        // steps means we exceeded the maximum number of iterations
        if (accurateStepsFound != ACCURATE_STEPS_TO_STOP) {
            throw new MaxIterationsExceededException(ACCURATE_STEPS_TO_STOP);
        }
        
        // We calculate now the estimated integral by taking into consideration
        // only the points into our limit
        double volume = 0;
        for (Triplet<Double, Double, Double> triplet : totalPointList) {
            double x = triplet.getValue0();
            double y = triplet.getValue1();
            if (x >= a1 && x <= b1 && y >= a2 && y <= b2) {
                volume = volume + triplet.getValue2() * totalAreaFactor;
            }
        }
        return volume;
    }
    
    public static double templateIntegral(Calculator<Unit<Double>, Unit<Double>> template, double a, double b) throws CalculationException {
        double spectrumResolution = ConfigFactory.getConfig().getSpectrumResolution();
        double step = spectrumResolution / 2;
        double integral = 0;
        for (double lambda = a; lambda <= b; lambda += step) {
            double value = template.calculate(new Unit<Double>(lambda)).getValue0();
            integral += value * step;
        }
        return integral;
    }

}
