/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.oldskyarea;

import lombok.EqualsAndHashCode;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.DoubleValueResult;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.javatuples.Pair;
import org.javatuples.Unit;

/**
 */
@EqualsAndHashCode(callSuper=false)
public class Fiber extends AbstractCalculator<Pair<Double, String>, Unit<Double>, Unit<Double>> {
    
    private double fiberDiameter;

    @Override
    protected void validateConfiguration(Pair<Double, String> configuration) throws ConfigurationException {
        // Check that the fiber diameter is a possitive number
        if (configuration.getValue0() <= 0) {
            throw new ConfigurationException("Fiber diameter must be a possitive "
                    + "number but was " + configuration.getValue0());
        }
        // Check if the fiber diameter is in arcsec
        if (!Units.isArcsec(configuration.getValue1())) {
            throw new ConfigurationException("Fiber diameter must be in " + Units.ARCSEC
                    + " but was in " + configuration.getValue1());
        }
    }

    @Override
    protected void initialize(Pair<Double, String> configuration) throws InitializationException {
        fiberDiameter = configuration.getValue0();
    }

    @Override
    protected Unit<Double> performCalculation(Unit<Double> input) throws CalculationException {
        double result = Math.PI * fiberDiameter * fiberDiameter / 4;
        return new Unit<Double>(result);
    }

    @Override
    protected void performForEveryCalculation(Unit<Double> input, Unit<Double> output) {
        ResultsHolder.getResults().addResult("SKY_AREA", input.getValue0(),
                output.getValue0(), Units.ANGSTROM, Units.getArcsec2(), Level.INTERMEDIATE_UNIMPORTANT);
    }

    @Override
    protected void performForEveryRetrieval(Pair<Double, String> configuration) {
        ResultsHolder.getResults().addResult(new DoubleValueResult("FIBER_DIAMETER",
                configuration.getValue0(), configuration.getValue1()), Level.DEBUG);
    }
    
}
