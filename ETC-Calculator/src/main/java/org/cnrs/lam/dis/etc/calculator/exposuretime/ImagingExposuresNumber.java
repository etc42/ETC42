/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.exposuretime;

import java.text.MessageFormat;
import java.util.ResourceBundle;
import lombok.EqualsAndHashCode;
import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.apache.log4j.Logger;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.Cacheable;
import org.cnrs.lam.cesam.util.calculator.CachingPolicy;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.Calculator;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Quadratic;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.calculator.util.integration.IntegrationTool;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.cnrs.lam.dis.etc.datamodel.ObsParam;
import org.javatuples.Ennead;
import org.javatuples.Pair;
import org.javatuples.Tuple;
import org.javatuples.Unit;

/**
 * <p>The class {@link ImagingExposuresNumber} is a calculator for computing
 * the exposure time for the case of imager simulation.</p>
 * 
 * <p>The configuration of the calculator is a {@link Ennead},
 * which contains the following elements:</p>
 * <ol>
 *   <li>{@link Calculator}: A calculator for computing the wavelength range</li>
 *   <li>{@link Pair}: A tuple containing the following:</li>
 *   <ol>
 *     <li>{@link Calculator}: A calculator for computing the simulated signal</li>
 *     <li>{@link Pair}: The extra signal and its unit</li>
 *   </ol>
 *   <li>{@link Pair}: A tuple containing the following:</li>
 *   <ul>
 *     <li>{@link Calculator}: A calculator for computing the simulated background</li>
 *     <li>{@link Pair}: The extra background and its unit</li>
 *   </ul>
 *   <li>{@link Calculator}: A calculator for computing the number of pixels</li>
 *   <li>{@link Pair}: The dark current and its unit</li>
 *   <li>{@link Pair}: The redout noise and its unit</li>
 *   <li>{@link Integer}: The number of exposures</li>
 *   <li>{@link Pair}: The fixed SNR wavelength and its unit</li>
 *   <li>{@link ObsParam.FixedSnrType}: The type of the fixed SNR (total or for
 *       central pixel)</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Tuple}, as this
 * calculator does not accept any input. It should always be null.</p>
 * 
 * <p>The output of the calculator is an {@link Unit} which contains the 
 * calculated exposure time, expressed in seconds.</p>
 */
@Cacheable(CachingPolicy.ALL)
@EqualsAndHashCode(callSuper=false, of={"wavelengthRange","simulatedSignal","extraSignal",
        "simulatedBackground","extraBackground","numberOfPixels","dark","readout",
        "noExpo","fixedSnr","fixedSnrType"})
public class ImagingExposuresNumber extends AbstractCalculator<
        Ennead<
            Calculator<Tuple, Pair<Double, Double>>,
            Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>,
                 Pair<Double, String>>,
            Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>,
                 Pair<Double, String>>,
            Calculator<Unit<Double>, Unit<Double>>,
            Pair<Double, String>, Pair<Double, String>,
            Integer, Double, ObsParam.FixedSnrType
        >, Tuple, Unit<Double>> {
    
    
    private static final Logger logger = Logger.getLogger(ImagingExposuresNumber.class);
    private static final ResourceBundle validationErrorsBundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");
    
    private Calculator<Tuple, Pair<Double, Double>> wavelengthRange;
    private Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>> simulatedSignal;
    private double extraSignal;
    private Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>> simulatedBackground;
    private double extraBackground;
    private Calculator<Unit<Double>, Unit<Double>> numberOfPixels;
    private double dark;
    private double readout;
    private double noExpo;
    private double fixedSnr;
    private ObsParam.FixedSnrType fixedSnrType;

    private double minLambda;
    private double maxLambda;
    
    /**
     * Validates the given configuration. It requires that the extra signal is
     * not negative and expressed in electrons, that the extra background
     * is not negative and expressed in electrons, that the dark current is 
     * non negative and expressed in e/pix/sec, that the readout noise is non
     * negative and expressed in e/pix,that the number of the exposures is positive
     * and that the fixed SNR is positive.
     * @param configuration The configuration to validate
     * @throws ConfigurationException if the configuration is invalid
     */
    @Override
    protected void validateConfiguration(Ennead<
            Calculator<Tuple, Pair<Double, Double>>,
            Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>,
                 Pair<Double, String>>,
            Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>,
                 Pair<Double, String>>,
            Calculator<Unit<Double>, Unit<Double>>,
            Pair<Double, String>, Pair<Double, String>,
            Integer, Double, ObsParam.FixedSnrType>
            configuration) throws ConfigurationException {
        // Check that the extra signal is a non negative number
        if (configuration.getValue1().getValue1().getValue0() < 0) {
            String message = validationErrorsBundle.getString("EXTRA_SIGNAL_NEGATIVE");
            message = MessageFormat.format(message, configuration.getValue1().getValue1().getValue0());
            throw new ConfigurationException(message);
        }
        // Check if the extra signal is in electrons per sec
        if (!Units.isElectronsPerSec(configuration.getValue1().getValue1().getValue1())) {
            String message = validationErrorsBundle.getString("EXTRA_SIGNAL_WRONG_UNIT");
            message = MessageFormat.format(message, Units.getElectronsPerSec(), configuration.getValue1().getValue1().getValue1());
            throw new ConfigurationException(message);
        }
        // Check that the extra background is a non negative number
        if (configuration.getValue2().getValue1().getValue0() < 0) {
            String message = validationErrorsBundle.getString("EXTRA_BACKGROUND_NEGATIVE");
            message = MessageFormat.format(message, configuration.getValue2().getValue1().getValue0());
            throw new ConfigurationException(message);
        }
        // Check if the extra background is in electrons per sec
        if (!Units.isElectronsPerSec(configuration.getValue2().getValue1().getValue1())) {
            String message = validationErrorsBundle.getString("EXTRA_BACKGROUND_WRONG_UNIT");
            message = MessageFormat.format(message, Units.getElectronsPerSec(), configuration.getValue2().getValue1().getValue1());
            throw new ConfigurationException(message);
        }
        // Check that the dark current is a non negative number
        if (configuration.getValue4().getValue0() < 0) {
            String message = validationErrorsBundle.getString("DARK_CURRENT_NEGATIVE");
            message = MessageFormat.format(message, configuration.getValue4().getValue0());
            throw new ConfigurationException(message);
        }
        // Check if the dark current is in e/pix/sec
        if (!Units.isElectronsPerPixelPerSec(configuration.getValue4().getValue1())) {
            String message = validationErrorsBundle.getString("DARK_CURRENT_WRONG_UNIT");
            message = MessageFormat.format(message, Units.getElectronsPerPixelPerSec(), configuration.getValue4().getValue1());
            throw new ConfigurationException(message);
        }
        // Check that the readout noise is a non negative number
        if (configuration.getValue5().getValue0() < 0) {
            String message = validationErrorsBundle.getString("READOUT_NOISE_NEGATIVE");
            message = MessageFormat.format(message, configuration.getValue5().getValue0());
            throw new ConfigurationException(message);
        }
        // Check if the readout noise is in e/pix
        if (!Units.isElectronsPerPixel(configuration.getValue5().getValue1())) {
            String message = validationErrorsBundle.getString("READOUT_NOISE_WRONG_UNIT");
            message = MessageFormat.format(message, Units.getElectronsPerPixel(), configuration.getValue5().getValue1());
            throw new ConfigurationException(message);
        }
        // Check that the number of exposures is positive
        if (configuration.getValue6() <= 0) {
            String message = validationErrorsBundle.getString("NUMBER_OF_EXPOSURES_NOT_POSITIVE");
            message = MessageFormat.format(message, configuration.getValue6());
            throw new ConfigurationException(message);
        }
        // Check that the fixed SNR is a positive number
        if (configuration.getValue7() <= 0) {
            String message = validationErrorsBundle.getString("FIXED_SNR_NOT_POSITIVE");
            message = MessageFormat.format(message, configuration.getValue7());
            throw new ConfigurationException(message);
        }
    }

    /**
     * Initializes the calculator instance with the given configuration.
     * @throws InitializationException 
     */
    @Override
    protected void initialize(Ennead<
            Calculator<Tuple, Pair<Double, Double>>,
            Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>,
                 Pair<Double, String>>,
            Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>,
                 Pair<Double, String>>,
            Calculator<Unit<Double>, Unit<Double>>,
            Pair<Double, String>, Pair<Double, String>,
            Integer, Double, ObsParam.FixedSnrType>
            configuration) throws InitializationException {
        wavelengthRange = configuration.getValue0();
        simulatedSignal = configuration.getValue1().getValue0();
        extraSignal = configuration.getValue1().getValue1().getValue0();
        simulatedBackground = configuration.getValue2().getValue0();
        extraBackground = configuration.getValue2().getValue1().getValue0();
        numberOfPixels = configuration.getValue3();
        dark = configuration.getValue4().getValue0();
        readout = configuration.getValue5().getValue0();
        noExpo = configuration.getValue6();
        fixedSnr = configuration.getValue7();
        fixedSnrType = configuration.getValue8();
        
        // Calculate the minimum and maximum of the wavelength range
        Pair<Double, Double> minMax = null;
        try {
            minMax = wavelengthRange.calculate(null);
        } catch (CalculationException e) {
            logger.error("Failed to calculate the wavelength range", e);
            throw new InitializationException(e.getMessage(), e);
        }
        minLambda = minMax.getValue0();
        maxLambda = minMax.getValue1();
    }

    /**
     * Calculates the maximum number of pixels for all the given wavelength range.
     * @param numberOfPixels
     * @param minLambda
     * @param maxLambda
     * @return 
     */
    private static double calculateNumberOfPixels(
            Calculator<Unit<Double>, Unit<Double>> numberOfPixels,
            double minLambda, double maxLambda) throws CalculationException {
        int minLambdaInt = (int) Math.floor(minLambda);
        int maxLambdaInt = (int) Math.ceil(maxLambda);
        double result = 0;
        for (int lambda = minLambdaInt; lambda <= maxLambdaInt; lambda++) {
            Unit<Double> input = new Unit<Double>((double) lambda);
            double noOfPixels = numberOfPixels.calculate(input).getValue0();
            if (noOfPixels > result) {
                result = noOfPixels;
            }
        }
        return result;
    }

    /**
     * Returns a {@link Unit} containing the exposure time.
     * @param input
     * @return
     * @throws CalculationException  if there is any error during the calculation
     */
    @Override
    protected Unit<Double> performCalculation(Tuple input) throws CalculationException {
        double numberOfPixelsValue = calculateNumberOfPixels(numberOfPixels, minLambda, maxLambda);
        
        // Calculate the total signal
        Pair<UnivariateRealFunction, UnivariateRealFunction> simulatedSignalFunctionsPair
                = simulatedSignal.calculate(new Pair<Double, Double>(minLambda, maxLambda));
        double simS = 0;
        try {
            simS = (fixedSnrType == ObsParam.FixedSnrType.CENTRAL_PIXEL)
                    ? IntegrationTool.univariateIntegral(simulatedSignalFunctionsPair.getValue0(), minLambda, maxLambda)
                    : IntegrationTool.univariateIntegral(simulatedSignalFunctionsPair.getValue1(), minLambda, maxLambda);
        } catch (FunctionEvaluationException ex) {
            logger.error(ex.getMessage(), ex);
            throw new CalculationException(ex.getMessage());
        }
        double extraS = (fixedSnrType == ObsParam.FixedSnrType.TOTAL)
                ? extraSignal : extraSignal / numberOfPixelsValue;
        double s = simS + extraS;
        
        // Calculate the total background
        Pair<UnivariateRealFunction, UnivariateRealFunction> simulatedBgFunctionsPair
                = simulatedBackground.calculate(new Pair<Double, Double>(minLambda, maxLambda));
        double simBg = 0;
        try {
            simBg = (fixedSnrType == ObsParam.FixedSnrType.CENTRAL_PIXEL)
                    ? IntegrationTool.univariateIntegral(simulatedBgFunctionsPair.getValue0(), minLambda, maxLambda)
                    : IntegrationTool.univariateIntegral(simulatedBgFunctionsPair.getValue1(), minLambda, maxLambda);
        } catch (FunctionEvaluationException ex) {
            logger.error(ex.getMessage(), ex);
            throw new CalculationException(ex.getMessage());
        }
        double extraBg = (fixedSnrType == ObsParam.FixedSnrType.TOTAL)
                ? extraBackground : extraBackground / numberOfPixelsValue;
        if (fixedSnrType == ObsParam.FixedSnrType.CENTRAL_PIXEL) {
            extraBg = extraBg / numberOfPixelsValue;
        }
        double bg = simBg + extraBg;
        
        double nPix = (fixedSnrType == ObsParam.FixedSnrType.CENTRAL_PIXEL)
                ? 1 : numberOfPixelsValue;
        
        // Now we solve the quadratic equation
        double a = s * s;
        double b = -1 * fixedSnr * fixedSnr * (s + bg + nPix * dark);
        double c = -1 * fixedSnr * fixedSnr * nPix * noExpo * readout * readout;
        double exposureTime = 0;
        for (Double time : Quadratic.solve(a, b, c)) {
            if (exposureTime < time) {
                exposureTime = time;
            }
        }
        return new Unit<Double>(exposureTime);
    }

    /**
     * <p>For every calculation the calculator adds in the simulation results
     * the following outputs of the calculator:</p>
     * <ul>
     *   <li>The calculated exposure time in final results with code CALCULATED_EXPOSURE_TIME</li>
     * </ul>
     * @param input
     * @param output 
     */
    @Override
    protected void performForEveryCalculation(Tuple input, Unit<Double> output) {
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("CALCULATED_EXPOSURE_TIME",
                output.getValue0(), Units.SEC), CalculationResults.Level.FINAL);
    }

    /**
     * <p>For every calculator instantiation the following parts of its configuration
     * are added in the simulation results:</p>
     * <ul>
     *   <li>The extra signal in debug results with code EXTRA_SIGNAL</li>
     *   <li>The extra background in debug results with code EXTRA_BACKGROUND</li>
     *   <li>The dark current in debug results with code DARK_CURRENT</li>
     *   <li>The readout in debug results with code READOUT_NOISE</li>
     *   <li>The number of exposures in debug results with code NUMBER_OF_EXPOSURES</li>
     *   <li>The fixed SNR in debug results with code FIXED_SNR</li>
     *   <li>The fixed SNR type in debug results with code FIXED_SNR_TYPE</li>
     * </ul>
     * @param configuration The configuration of the calculator
     */
    @Override
    protected void performForEveryRetrieval(Ennead<
            Calculator<Tuple, Pair<Double, Double>>,
            Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>,
                 Pair<Double, String>>,
            Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>,
                 Pair<Double, String>>, 
            Calculator<Unit<Double>, Unit<Double>>, Pair<Double, String>, 
            Pair<Double, String>, Integer, Double, ObsParam.FixedSnrType> configuration) {
        Pair<Double, String> extraSignalPair = configuration.getValue1().getValue1();
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("EXTRA_SIGNAL", 
                extraSignalPair.getValue0(), extraSignalPair.getValue1()), CalculationResults.Level.DEBUG);
        Pair<Double, String> extraBackgroundPair = configuration.getValue2().getValue1();
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("EXTRA_BACKGROUND", 
                extraBackgroundPair.getValue0(), extraBackgroundPair.getValue1()), CalculationResults.Level.DEBUG);
        Pair<Double, String> darkPair = configuration.getValue4();
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("DARK_CURRENT", 
                darkPair.getValue0(), darkPair.getValue1()), CalculationResults.Level.DEBUG);
        Pair<Double, String> readoutPair = configuration.getValue5();
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("READOUT_NOISE", 
                readoutPair.getValue0(), readoutPair.getValue1()), CalculationResults.Level.DEBUG);
        ResultsHolder.getResults().addResult(new CalculationResults.LongValueResult("NUMBER_OF_EXPOSURES", 
                configuration.getValue6(), null), CalculationResults.Level.DEBUG);
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("FIXED_SNR", 
                configuration.getValue7(), null), CalculationResults.Level.DEBUG);
        ResultsHolder.getResults().addResult(new CalculationResults.StringResult("FIXED_SNR_TYPE",
                configuration.getValue8().toString()), CalculationResults.Level.DEBUG);
    }
    
}
