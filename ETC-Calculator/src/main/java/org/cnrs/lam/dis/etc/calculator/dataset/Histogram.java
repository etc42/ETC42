/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.dataset;

import lombok.EqualsAndHashCode;
import org.apache.log4j.Logger;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.Cacheable;
import org.cnrs.lam.cesam.util.calculator.CachingPolicy;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.Calculator;
import org.cnrs.lam.cesam.util.calculator.CalculatorManager;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.DatasetProviderHolder;
import org.cnrs.lam.dis.etc.configuration.ConfigFactory;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.javatuples.Pair;
import org.javatuples.Quintet;
import org.javatuples.Unit;

/**
 * <p>the {@code Histogram} class is a calculator for computing values of histograms
 * represented by a dataset. It uses the {@link org.cnrs.lam.dis.etc.calculator.dataset.HistogramConvolution}
 * method for making the calculation of the neighbor values (based on a resolution) of the key
 * and then it does linear interpolation between them. The kernel length for the
 * convolution is retrieved by a given calculator.</p>
 * 
 * <p>The configuration of the calculator is a {@link org.javatuples.Quintet}, which
 * contains the following elements:</p>
 * <ol>
 *   <li>{@link org.cnrs.lam.dis.etc.datamodel.Dataset.Type}: The type of the
 *       dataset this calculator will represent</li>
 *   <li>{@link org.cnrs.lam.dis.etc.datamodel.DatasetInfo}: The information (name
 *       and namespace) of the dataset this calculator will represent</li>
 *   <li>{@link org.cnrs.lam.dis.etc.datamodel.Dataset.DataType}: The type of
 *       the data of the dataset, showing if we have a template or emission lines</li>
 *   <li>{@link org.cnrs.lam.cesam.util.calculator.Calculator}: A calculator for
 *       calculating the size of the kernel for each key</li>
 *   <li>{@link java.lang.String}: The option of a multi-dataset or null for single
 *       dataset</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the key for which the value will be
 * calculated.</p>
 * 
 * <p>The output of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the calculated value.</p>
 *
 * @author Nikolaos Apostolakos
 */

// Caching explenation
// -------------------
// This calculator is set to cache the instances because the configuration validation
// is retrieving the dataset (which is time consuming).
@Cacheable(CachingPolicy.INSTANCE)
@EqualsAndHashCode(callSuper=false, exclude={"convolution"})
public class Histogram extends AbstractCalculator<
        Quintet<Dataset.Type, DatasetInfo, Dataset.DataType, Calculator<Unit<Double>, Unit<Double>>, String>
        , Unit<Double>, Unit<Double>> {
    
    private static final Logger logger = Logger.getLogger(Histogram.class);
    
    private Dataset.Type datasetType;
    private DatasetInfo datasetInfo;
    private String option;
    private Dataset.DataType histogramType;
    private Calculator<Unit<Double>, Unit<Double>> kernelFunction;
    private double resolution;
    
    private HistogramConvolution convolution;

    /**
     * Validates the given configuration. It requires that the given dataset is
     * available via the dataset provider and that the data type is supported.
     * @param configuration The configuration to validate
     * @throws ConfigurationException If configuration is invalid
     */
    @Override
    protected void validateConfiguration(Quintet<
            Dataset.Type, DatasetInfo, Dataset.DataType, Calculator<Unit<Double>, Unit<Double>>, String> configuration
            ) throws ConfigurationException {
        Dataset dataset = DatasetProviderHolder.getDatasetProvider().getDataset(
                configuration.getValue0(), configuration.getValue1(), configuration.getValue4());
        if (dataset == null) {
            String optionString = (configuration.getValue4() == null || configuration.getValue4().equals(""))
                    ? "" : " and option " + configuration.getValue4();
            throw new ConfigurationException("The dataset of type" + configuration.getValue0()
                    + " and name " + configuration.getValue1() + optionString + " is not available");
        }
        switch (configuration.getValue2()) {
            case EMISSION_LINES:
            case TEMPLATE:
                break;
            default:
                throw new ConfigurationException("HistogramConvolution cannot be "
                        + "used for data with type " + configuration.getValue2());
        }
    }

    /**
     * Initializes the calculator instance with the given configuration. The first
     * value of the configuration is the type of the dataset, the second is the
     * name of it, the third is the data type and the fourth is the calculator
     * for retrieving the kernel size.
     * @param configuration The configuration to use for initialization
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(Quintet<
            Dataset.Type, DatasetInfo, Dataset.DataType, Calculator<Unit<Double>, Unit<Double>>, String> configuration
            ) throws InitializationException {
        datasetType = configuration.getValue0();
        datasetInfo = configuration.getValue1();
        option = configuration.getValue4();
        histogramType = configuration.getValue2();
        resolution = ConfigFactory.getConfig().getSpectrumResolution();
        Quintet<Dataset.Type, DatasetInfo, Double, Dataset.DataType, String> convConfiguration = 
                new Quintet<Dataset.Type, DatasetInfo, Double, Dataset.DataType, String>
                (datasetType, datasetInfo, resolution, histogramType, option);
        try {
            convolution = CalculatorManager.getManager(HistogramConvolution.class).getCalculator(convConfiguration);
        } catch (ConfigurationException e) {
            throw new InitializationException(e.getMessage(), e);
        }
        kernelFunction = configuration.getValue3();
    }

    /**
     * Returns a {@link org.javatuples.Unit} containing the value of the function
     * for the given input.
     * @param input The key to interpolate for
     * @return The value of the function for the given key
     * @throws CalculationException if there is any error during the calculation
     */
    @Override
    protected Unit<Double> performCalculation(Unit<Double> input) throws CalculationException {
        int kernel = (int) Math.ceil(kernelFunction.calculate(input).getValue0() / resolution);
        double key = input.getValue0() / resolution;
        int lowKey = (int) Math.floor(key);
        double lowValue = Double.parseDouble(convolution.calculate(new Pair(lowKey, kernel)).getValue0().toString());
        int hightKey = (int) Math.ceil(key);
        if (hightKey == lowKey) {
            return new Unit(lowValue);
        }
        double highValue = Double.parseDouble(convolution.calculate(new Pair(hightKey, kernel)).getValue0().toString());
        double result = highValue * (key - lowKey) + lowValue * (hightKey - key);
        return new Unit(result);
    }
    
}
