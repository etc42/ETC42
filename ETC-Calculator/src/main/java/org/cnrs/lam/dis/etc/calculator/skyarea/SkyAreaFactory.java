/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.skyarea;

import java.util.ResourceBundle;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.numberofpixels.NumberOfPixelsFactory;
import org.cnrs.lam.dis.etc.calculator.spatialbinning.SpatialBinningFactory;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.cnrs.lam.dis.etc.datamodel.Instrument;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.javatuples.Pair;
import org.javatuples.Quartet;
import org.javatuples.Tuple;
import org.javatuples.Unit;

/**
 */
public class SkyAreaFactory implements Factory<Unit<Session>, Tuple,
        Pair<UnivariateRealFunction, UnivariateRealFunction>> {

    private static final ResourceBundle bundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");

    @Override
    public Calculator<Tuple, Pair<UnivariateRealFunction, UnivariateRealFunction>>
            getCalculator(Unit<Session> configuration) throws InitializationException, ConfigurationException {
        Session session = configuration.getValue0();
        Instrument instrument = session.getInstrument();
        
        Double pixelScale = instrument.getPixelScale();
        String pixelScaleUnit = instrument.getPixelScaleUnit();
        Pair<Double, String> pixelScalePair = new Pair<Double, String>(pixelScale, pixelScaleUnit);
        switch (instrument.getInstrumentType()) {
            case IMAGING:
                ResultsHolder.getResults().addResult(new CalculationResults.StringResult(
                        "SKY_AREA_METHOD", "Imaging"), CalculationResults.Level.DEBUG);
                Calculator<Unit<Double>, Unit<Double>> numberOfPixelsCalculator
                        = new NumberOfPixelsFactory().getCalculator(configuration);
                Pair<Pair<Double, String>, Calculator<Unit<Double>, Unit<Double>>> imagingConfiguration =
                        new Pair<Pair<Double, String>, Calculator<Unit<Double>, Unit<Double>>>(pixelScalePair, numberOfPixelsCalculator);
                return EtcCalculatorManager.getManager(Imaging.class).getCalculator(imagingConfiguration);
            case SPECTROGRAPH:
                Calculator<Unit<Double>, Unit<Double>> spatialBinningCalculator
                        = new SpatialBinningFactory().getCalculator(configuration);
                switch (instrument.getSpectrographType()) {
                    case SLIT:
                        ResultsHolder.getResults().addResult(new CalculationResults.StringResult(
                                "SKY_AREA_METHOD", "Slit spectrograph"), CalculationResults.Level.DEBUG);
                        double slitWidth = instrument.getSlitWidth();
                        String slitWidthUnit = instrument.getSlitWidthUnit();
                        Pair<Double, String> slitWidthPair = new Pair<Double, String>(slitWidth, slitWidthUnit);
                        double slitLength = instrument.getSlitLength();
                        String slitLengthUnit = instrument.getSlitLengthUnit();
                        Pair<Double, String> slitLengthPair = new Pair<Double, String>(slitLength, slitLengthUnit);
                        Quartet<Pair<Double, String>, Pair<Double, String>, Pair<Double, String>, Calculator<Unit<Double>, Unit<Double>>> slitConfiguration =
                                new Quartet<Pair<Double, String>, Pair<Double, String>, Pair<Double, String>, Calculator<Unit<Double>, Unit<Double>>>
                                (pixelScalePair, slitWidthPair, slitLengthPair, spatialBinningCalculator);
                        return EtcCalculatorManager.getManager(Slit.class).getCalculator(slitConfiguration);
                    case FIBER:
                        ResultsHolder.getResults().addResult(new CalculationResults.StringResult(
                                "SKY_AREA_METHOD", "Fiber spectrograph"), CalculationResults.Level.DEBUG);
                        double fiberDiameter = instrument.getFiberDiameter();
                        String fiberDiameterUnit = instrument.getFiberDiameterUnit();
                        Pair<Double, String> fiberDiameterPair = new Pair<Double, String>(fiberDiameter, fiberDiameterUnit);
                        Pair<Pair<Double, String>, Pair<Double, String>> fiberConfiguration =
                                new Pair<Pair<Double, String>, Pair<Double, String>>(pixelScalePair, fiberDiameterPair);
                        return EtcCalculatorManager.getManager(Fiber.class).getCalculator(fiberConfiguration);
                }
        }
        String message = bundle.getString("UNKNOWN_SKY_AREA_METHOD");
        throw new ConfigurationException(message);
    }
    
}
