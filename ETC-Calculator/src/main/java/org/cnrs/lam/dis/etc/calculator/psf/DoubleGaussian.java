/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.psf;

import java.text.MessageFormat;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TreeMap;
import lombok.EqualsAndHashCode;
import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.analysis.BivariateRealFunction;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.apache.log4j.Logger;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.Cacheable;
import org.cnrs.lam.cesam.util.calculator.CachingPolicy;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.DatasetProviderHolder;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.calculator.util.datasets.LinearFunctionDataset;
import org.cnrs.lam.dis.etc.calculator.util.functions.SymmetricBivariateDoubleGaussian;
import org.cnrs.lam.dis.etc.calculator.util.functions.SymmetricBivariateGaussian;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.javatuples.Pair;
import org.javatuples.Triplet;
import org.javatuples.Unit;

/**
 * <p>The {@code InstrumentPsfFwhmProfile} class is a calculator for computing the PSF
 * of the instrument following a circularly symmetric double Gaussian distribution.
 * The double Gaussian is the addition of two Gaussian distributions, defined by
 * their FWHM (given as two templates over the wavelength). The multiplier of the
 * double Gaussian is a factor between 0 and 1 with which the first Gaussian is
 * multiplied. The second Gaussian is multiplied with 1-multiplier, so the total
 * double Gaussian is always a probability distribution.</p>
 * 
 * <p>The configuration of the calculator is a {@link org.javatuples.Triplet},
 * witch contains the following elements:</p>
 * <ol>
 *   <li>{@link org.cnrs.lam.dis.etc.datamodel.DatasetInfo}: the information of the
 *       dataset describing the FWHM of the first Gaussian</li>
 *   <li>{@link org.cnrs.lam.dis.etc.datamodel.DatasetInfo}: the information of the
 *       dataset describing the FWHM of the second Gaussian</li>
 *   <li>{@link Double}: the multiplier of the first Gaussian</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the wavelength for which the calculation
 * is done (expressed in {@latex.inline \\AA}).</p>
 * 
 * <p>The output of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link BivariateRealFunction} representing the PSF (both axes are expressed in
 * {@latex.inline $arcsec$}).</p>
 */

// Caching explenation
// -------------------
// This calculator is set to cache the instances because the configuration validation
// and the instantiation are retrieving the datasets, which is time consuming.
@Cacheable(CachingPolicy.INSTANCE)
@EqualsAndHashCode(callSuper=false, exclude={"fwhm1Function","fwhm2Function"})
public class DoubleGaussian extends AbstractCalculator<
        Triplet<DatasetInfo, DatasetInfo, Double>, Unit<Double>, Unit<BivariateRealFunction>> {

    private static final Logger logger = Logger.getLogger(DoubleGaussian.class);
    private static final ResourceBundle validationErrorsBundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");
    
    private DatasetInfo fwhm1DatasetInfo;
    private LinearFunctionDataset fwhm1Function;
    private DatasetInfo fwhm2DatasetInfo;
    private LinearFunctionDataset fwhm2Function;
    private double multiplier;

    /**
     * Validates the given configuration. It requires that the instrument has a
     * FWHM profiles for both Gaussians, that the data are available, that the 
     * unit of X axes are {@latex.inline \\AA}, that the unit of Y axes are
     * {@latex.inline $arcsec$} and that the multiplier is between 0 and 1.
     * @param configuration the configuration to validate
     * @throws ConfigurationException if the configuration is invalid
     */
    @Override
    protected void validateConfiguration(Triplet<DatasetInfo, DatasetInfo, Double> configuration) throws ConfigurationException {
        // Check the first Gaussian FWHM profile
        DatasetInfo info = configuration.getValue0();
        if (info == null) {
            String message = validationErrorsBundle.getString("PSF_DOUBLE_GAUSSIAN_FWHM_1_PROFILE_MISSING");
            throw new ConfigurationException(message);
        }
        Dataset dataset = DatasetProviderHolder.getDatasetProvider().getDataset(Dataset.Type.PSF_DOUBLE_GAUSSIAN_FWHM_1, info);
        if (dataset == null) {
            String message = validationErrorsBundle.getString("PSF_DOUBLE_GAUSSIAN_FWHM_1_PROFILE_NOT_AVAILABLE");
            message = MessageFormat.format(message, info);
            throw new ConfigurationException(message);
        }
        if (!Units.isAngstrom(dataset.getXUnit())) {
            String message = validationErrorsBundle.getString("PSF_DOUBLE_GAUSSIAN_FWHM_1_PROFILE_WRONG_X_UNIT");
            message = MessageFormat.format(message, Units.ANGSTROM, dataset.getXUnit());
            throw new ConfigurationException(message);
        }
        if (!Units.isArcsec(dataset.getYUnit())) {
            String message = validationErrorsBundle.getString("PSF_DOUBLE_GAUSSIAN_FWHM_1_PROFILE_WRONG_Y_UNIT");
            message = MessageFormat.format(message, Units.ARCSEC, dataset.getYUnit());
            throw new ConfigurationException(message);
        }
        // Check the second Gaussian FWHM profile
        info = configuration.getValue1();
        if (info == null) {
            String message = validationErrorsBundle.getString("PSF_DOUBLE_GAUSSIAN_FWHM_2_PROFILE_MISSING");
            throw new ConfigurationException(message);
        }
        dataset = DatasetProviderHolder.getDatasetProvider().getDataset(Dataset.Type.PSF_DOUBLE_GAUSSIAN_FWHM_2, info);
        if (dataset == null) {
            String message = validationErrorsBundle.getString("PSF_DOUBLE_GAUSSIAN_FWHM_2_PROFILE_NOT_AVAILABLE");
            message = MessageFormat.format(message, info);
            throw new ConfigurationException(message);
        }
        if (!Units.isAngstrom(dataset.getXUnit())) {
            String message = validationErrorsBundle.getString("PSF_DOUBLE_GAUSSIAN_FWHM_2_PROFILE_WRONG_X_UNIT");
            message = MessageFormat.format(message, Units.ANGSTROM, dataset.getXUnit());
            throw new ConfigurationException(message);
        }
        if (!Units.isArcsec(dataset.getYUnit())) {
            String message = validationErrorsBundle.getString("PSF_DOUBLE_GAUSSIAN_FWHM_2_PROFILE_WRONG_Y_UNIT");
            message = MessageFormat.format(message, Units.ARCSEC, dataset.getYUnit());
            throw new ConfigurationException(message);
        }
        // Check that the multiplier is between 0 and 1
        if (configuration.getValue2() < 0 || configuration.getValue2() > 1) {
            String message = validationErrorsBundle.getString("PSF_DOUBLE_GAUSSIAN_MULTIPLIER_OUT_OF_RANGE");
            message = MessageFormat.format(message, (configuration.getValue2() * 100));
            throw new ConfigurationException(message);
        }
    }

    /**
     * Initializes the calculator instance with the given configuration.
     * @param configuration the configuration to use for initialization
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(Triplet<DatasetInfo, DatasetInfo, Double> configuration) throws InitializationException {
        fwhm1DatasetInfo = configuration.getValue0();
        Dataset dataset = DatasetProviderHolder.getDatasetProvider().getDataset(Dataset.Type.PSF_DOUBLE_GAUSSIAN_FWHM_1, fwhm1DatasetInfo);
        fwhm1Function = new LinearFunctionDataset(dataset.getData());
        fwhm2DatasetInfo = configuration.getValue1();
        dataset = DatasetProviderHolder.getDatasetProvider().getDataset(Dataset.Type.PSF_DOUBLE_GAUSSIAN_FWHM_2, fwhm2DatasetInfo);
        fwhm2Function = new LinearFunctionDataset(dataset.getData());
        multiplier=  configuration.getValue2();
    }

    /**
     * Returns a {@link org.javatuples.Unit} containing a bivariate function, of
     * which both axes are expressed in {@latex.inline $arcsec$}, which represents
     * the PSF for the given wavelength.
     * @param input The wavelength expressed in {@latex.inline \\AA}
     * @return A function representing the PSF
     * @throws CalculationException if there is any error during the calculation
     */
    @Override
    protected Unit<BivariateRealFunction> performCalculation(Unit<Double> input) throws CalculationException {
        double lambda = input.getValue0();
        double fwhm1 = 0;
        double fwhm2 = 0;
        try {
            fwhm1 = this.fwhm1Function.value(lambda);
            fwhm2 = this.fwhm2Function.value(lambda);
        } catch (FunctionEvaluationException ex) {
            logger.warn("Failed to calculate the FWHMs of the double Gaussian PSF from the profiles", ex);
        }
        double sigma1 = fwhm1 / 2.3548;
        double sigma2 = fwhm2 / 2.3548;
        SymmetricBivariateDoubleGaussian doubleGaussian = new SymmetricBivariateDoubleGaussian(sigma1, sigma2, multiplier);
        return new Unit<BivariateRealFunction>(doubleGaussian);
    }

    /**
     * <p>For every calculation the calculator adds in the simulation results
     * the following outputs of the calculator:</p>
     * <ul>
     *   <li>The FWHM of the first Gaussian in intermediate unimportant results
     *       with code PSF_DOUBLE_GAUSSIAN_FWHM_1</li>
     *   <li>The FWHM of the second Gaussian in intermediate unimportant results
     *       with code PSF_DOUBLE_GAUSSIAN_FWHM_2</li>
     *   <li> The projection of the PSF on the plane y=0 in the intermediate
     *       unimportant results with code INSTRUMENT_PSF_PROJECTION</li>
     * </ul>
     * @param input
     * @param output 
     */
    @Override
    protected void performForEveryCalculation(Unit<Double> input, Unit<BivariateRealFunction> output) {
        double lambda = input.getValue0();
        //We know that we have a symmetric bivariate double gaussian so the casting is safe
        SymmetricBivariateDoubleGaussian doubleGaussian = (SymmetricBivariateDoubleGaussian) output.getValue0();
        
        // We add the two FWHM values in the results
        Pair<Double, Double> standardDeviations = doubleGaussian.getStandardDeviations();
        double fwhm1 = standardDeviations.getValue0() * 2.3548;
        ResultsHolder.getResults().addResult("PSF_DOUBLE_GAUSSIAN_FWHM_1", lambda, fwhm1, 
                Units.ANGSTROM, Units.ARCSEC, CalculationResults.Level.INTERMEDIATE_UNIMPORTANT);
        double fwhm2 = standardDeviations.getValue1() * 2.3548;
        ResultsHolder.getResults().addResult("PSF_DOUBLE_GAUSSIAN_FWHM_2", lambda, fwhm2, 
                Units.ANGSTROM, Units.ARCSEC, CalculationResults.Level.INTERMEDIATE_UNIMPORTANT);
        
        // We add the projection on the y = 0 plane in the results. We calculate
        // the values in the range of 5 maxSigma, as all the values outside of this
        // are too close to zero.
        double maxSigma = Math.max(standardDeviations.getValue0(), standardDeviations.getValue1());
        double minSigma = Math.min(standardDeviations.getValue0(), standardDeviations.getValue1());
        UnivariateRealFunction projetion = doubleGaussian.projectionFunction();
        Map<Double, Double> projectionData = new TreeMap<Double, Double>();
        // We will try to create at least 100 points inside the smaller Gaussian
        // but we will not exceed the 1000 points in total
        double step =  Math.max(maxSigma / 200., minSigma / 20.);
        for (double x = -5 * maxSigma; x <= 5 * maxSigma; x += step) {
            double value = 0;
            try {
                value = projetion.value(x);
            } catch (FunctionEvaluationException ex) {
                logger.warn("Failed to calculate the value of the PSF projection", ex);
            }
            projectionData.put(x, value);
        }
        CalculationResults.DoubleDatasetResult projectionResult = 
                new CalculationResults.DoubleDatasetResult("INSTRUMENT_PSF_PROJECTION", projectionData, Units.ARCSEC, null);
        ResultsHolder.getResults().addResult("INSTRUMENT_PSF_PROJECTION", lambda, projectionResult,
                Units.ANGSTROM, CalculationResults.Level.INTERMEDIATE_UNIMPORTANT);
    }

    /**
     * <p>For every calculator instantiation the following parts of its configuration
     * are added in the simulation results:</p>
     * <ul>
     *   <li>The name of the first Gaussian FWHM profile with code PSF_DOUBLE_GAUSSIAN_FWHM_1_PROFILE</li>
     *   <li>The name of the second Gaussian FWHM profile with code PSF_DOUBLE_GAUSSIAN_FWHM_2_PROFILE</li>
     *   <li>The multiplier of the first Gaussian with code PSF_DOUBLE_GAUSSIAN_MULTIPLIER</li>
     * </ul>
     * @param configuration 
     */
    @Override
    protected void performForEveryRetrieval(Triplet<DatasetInfo, DatasetInfo, Double> configuration) {
        CalculationResults.StringResult fwhm1Result = new CalculationResults.StringResult(
                "PSF_DOUBLE_GAUSSIAN_FWHM_1_PROFILE", configuration.getValue0().toString());
        ResultsHolder.getResults().addResult(fwhm1Result, CalculationResults.Level.DEBUG);
        CalculationResults.StringResult fwhm2Result = new CalculationResults.StringResult(
                "PSF_DOUBLE_GAUSSIAN_FWHM_2_PROFILE", configuration.getValue1().toString());
        ResultsHolder.getResults().addResult(fwhm2Result, CalculationResults.Level.DEBUG);
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("PSF_DOUBLE_GAUSSIAN_MULTIPLIER",
                configuration.getValue2(), null), CalculationResults.Level.DEBUG);
    }

}
