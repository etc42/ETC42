/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.util.integration;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.MaxIterationsExceededException;
import org.apache.commons.math.analysis.BivariateRealFunction;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.apache.commons.math.analysis.integration.RombergIntegrator;
import org.apache.log4j.Logger;
import org.cnrs.lam.dis.etc.calculator.util.functions.CircularlySymmetricBivariateFunction;
import org.cnrs.lam.dis.etc.calculator.util.functions.IntegrableBivariateFunction;
import org.cnrs.lam.dis.etc.calculator.util.functions.IntegrableUnivariateFunction;
import org.javatuples.Quartet;
import org.javatuples.Triplet;

/**
 */
public class IntegrationTool {
    
    private final static Logger logger = Logger.getLogger(IntegrationTool.class);
    
    /**
     * Calculates the integral {@latex.inline $\\int_{x_1}^{x_2}f_{(x)}\\,\\mathrm{d}x$}.
     * Note that this method takes advantage of functions which implement the
     * {@link IntegrableUnivariateFunction} to speed up the integration.
     * @param function The function of which the integral is calculated
     * @param x1 The lower limit of the integral range
     * @param x2 The upper limit of the integral range
     * @return The value of the integral
     * @throws FunctionEvaluationException if the integration fails
     * @throws IllegalArgumentException if x1 > x2
     */
    public static double univariateIntegral(UnivariateRealFunction function, double x1, double x2)
            throws FunctionEvaluationException {
        // First check the x1 and x2. Note that if they are equal the integral is 0.
        if (x1 > x2) {
            throw new IllegalArgumentException("Illegal integration range [" + x1 + "," + x2 + "]");
        }
        if (x1 == x2) {
            return 0;
        }
        // Check if the function is integrable or not. If it is it has implemented
        // a smarter way for calculating the integral, so use it. If it is not
        // use the current default integration method.
        if (function instanceof IntegrableUnivariateFunction) {
            IntegrableUnivariateFunction integrable = (IntegrableUnivariateFunction) function;
            return integrable.integral(x1, x2);
        } else {
            return univariateDefaultIntegral(function, x1, x2);
        }
    }
    
    /**
     * Calculates the integral {@latex.inline $\\int_{x_1}^{x_2}f_{(x)}\\,\\mathrm{d}x$}
     * by assuming nothing about the given function and using the Romberg algorithm.
     * @param function The function of which the integral is calculated
     * @param x1 The lower limit of the integral range
     * @param x2 The upper limit of the integral range
     * @return The value of the integral
     * @throws FunctionEvaluationException if the integration fails
     * @throws IllegalArgumentException if x1 > x2
     */
    private static double univariateDefaultIntegral(UnivariateRealFunction function, double x1, double x2)
            throws FunctionEvaluationException {
        // First check the x1 and x2. Note that if they are equal the integral is 0.
        if (x1 > x2) {
            throw new IllegalArgumentException("Illegal integration range [" + x1 + "," + x2 + "]");
        }
        if (x1 == x2) {
            return 0;
        }
        // use the Romberg algorithm to calculate the integral
        RombergIntegrator integrator = new RombergIntegrator();
        double integral = 0;
        try {
            integral = integrator.integrate(function, x1, x2);
        } catch (MaxIterationsExceededException ex) {
            logger.warn("Maximum number of iterations reached when trying to calculate "
                    + "univariate integral using the Romberg algorithm", ex);
            throw new FunctionEvaluationException(ex, new double[] {x1, x2});
        }
        return integral;
    }
    
    /**
     * <p>Calculates the integral of the given circularly symmetric function in the
     * circle with radius r. This integral is:</p>
     * {@latex.inline $
     * \\int\\limits_0^{2\\pi}\\int\\limits_0^Rf_{(r)}rdrd\\theta=
     * 2\\pi*\\int\\limits_0^Rf_{(r)}rdr
     * $}
     * @param function The function of which the integral is calculated
     * @param r The radius of the circle of the integration
     * @return The value of the integral
     * @throws FunctionEvaluationException if the integration fails
     * @throws IllegalArgumentException if the radius is negative
     */
    public static double bivariateIntegral(final CircularlySymmetricBivariateFunction function, final double r)
            throws FunctionEvaluationException {
        if (r < 0) {
            throw new IllegalArgumentException("Integration radius cannot be negative but was " + r);
        }
        if (r == 0) {
            return 0;
        }
        UnivariateRealFunction polarFunction = function.polarFunction();
        double polarFunctionIntegral = univariateIntegral(polarFunction, 0, r);
        return 2 * Math.PI * polarFunctionIntegral;
    }
    
    /**
     * <p>Calculates the two dimensional integral of the given bivariate function:</p>
     * {@latex.inline $
     * \\int\\limits_{y_1}^{y_2}\\int\\limits_{x_1}^{x_2}f_{(x,y)}dxdy
     * $}
     * @param function The function for which the integral is calculated
     * @param x1 The low bound of X axis
     * @param x2 The high bound of X axis
     * @param y1 The low bound of Y axis
     * @param y2 The high bound of Y axis
     * @return The value of the integral
     * @throws FunctionEvaluationException if the integral calculation fails
     */
    public static double bivariateIntegral(
            BivariateRealFunction function, double x1, double x2, double y1, double y2)
            throws FunctionEvaluationException {
        // First check the limits.
        if (x1 > x2) {
            throw new IllegalArgumentException("Illegal integration range on X axis [" + x1 + "," + x2 + "]");
        }
        if (y1 > y2) {
            throw new IllegalArgumentException("Illegal integration range on Y axis [" + y1 + "," + y2 + "]");
        }
        if (x1 == x2 || y1 == y2) {
            return 0;
        }
        // Check if the function is integrable or not. If it is it has implemented
        // a smarter way for calculating the integral, so use it.
        if (function instanceof IntegrableBivariateFunction) {
            IntegrableBivariateFunction integrable = (IntegrableBivariateFunction) function;
            return integrable.integral(x1, x2, y1, y2);
        }
        // Check if the function is circularly symmetric. If it is we use a faster method
        if (function instanceof CircularlySymmetricBivariateFunction) {
            CircularlySymmetricBivariateFunction symmetric = (CircularlySymmetricBivariateFunction) function;
            return circularlySymmetricBivariateIntegral(symmetric, x1, x2, y1, y2);
        }
        // If we are here use the default (and slowest) way of integration.
        return bivariateDefaultIntegral(function, x1, x2, y1, y2);
    }
    
    private static double bivariateDefaultIntegral(
            BivariateRealFunction function, double x1, double x2, double y1, double y2)
            throws FunctionEvaluationException {
        RombergIntegrator integrator = new RombergIntegrator();
        double integral = 0;
        try {
            integral = integrator.integrate(new IntermediateIntegral(function, x1, x2), y1, y2);
        } catch (MaxIterationsExceededException ex) {
            logger.warn("Maximum number of iterations reached when trying to calculate "
                    + "bivariate integral using the Romberg algorithm", ex);
            throw new FunctionEvaluationException(ex, new double[] {x1, x2, y1, y2});
        }
        return integral;
    }

    private static class ConstantYFunction implements UnivariateRealFunction {
        private final BivariateRealFunction function;
        private final double y;

        public ConstantYFunction(BivariateRealFunction function, double y) {
            this.function = function;
            this.y = y;
        }

        @Override
        public double value(double x) throws FunctionEvaluationException {
            return function.value(x, y);
        }
    }

    private static class IntermediateIntegral implements UnivariateRealFunction {

        private final BivariateRealFunction function;
        private final double x1;
        private final double x2;

        public IntermediateIntegral(BivariateRealFunction function, double x1, double x2) {
            this.function = function;
            this.x1 = x1;
            this.x2 = x2;
        }

        @Override
        public double value(double y) throws FunctionEvaluationException {
            double result = 0;
            try {
                result = univariateIntegral(new ConstantYFunction(function, y), x1, x2);
            } catch (Exception ex) {
                throw new FunctionEvaluationException(ex, y);
            }
            return result;
        }

    }

    /**
     * <p>Calculates the two dimensional integral of the given circularly symmetric
     * function:</p>
     * {@latex.inline $
     * \\int\\limits_{y_1}^{y_2}\\int\\limits_{x_1}^{x_2}f_{(x,y)}dxdy
     * $}
     * The method used is a Monte Carlo based integration which uses as reference point
     * the integral of the function in an annulus area. This is computed by using the fact
     * that it is circularly symmetric.
     */
    public static double monteCarloCircularSymmetricBivariateIntegral(
            CircularlySymmetricBivariateFunction function, double x1, double x2, double y1, double y2)
            throws MaxIterationsExceededException, FunctionEvaluationException {
        // First we calculate the annulus area which contains all the limits of the integral
        double maxRadius = 0;
        maxRadius = Math.max(maxRadius, Math.sqrt(x1*x1 + y1*y1));
        maxRadius = Math.max(maxRadius, Math.sqrt(x2*x2 + y1*y1));
        maxRadius = Math.max(maxRadius, Math.sqrt(x2*x2 + y2*y2));
        maxRadius = Math.max(maxRadius, Math.sqrt(x1*x1 + y2*y2));
        double minRadius = 0;
        // If we have apoint in each quarter then the min Radius has to stay zero
        // as we need a disk to cover the integration range. Otherwise calcualte
        // the radius of the inner circle of the annulus
        if (x1 * x2 > 0 || y1 * y2 > 0) {
            minRadius = Math.min(minRadius, Math.sqrt(x1 * x1 + y1 * y1));
            minRadius = Math.min(minRadius, Math.sqrt(x2 * x2 + y1 * y1));
            minRadius = Math.min(minRadius, Math.sqrt(x2 * x2 + y2 * y2));
            minRadius = Math.min(minRadius, Math.sqrt(x1 * x1 + y2 * y2));
        }
        // We create a monte carlo kernel for this annulus
        MonteCarloKernel kernel = monteCarloCircularSymmetricBivariateKernel(function, minRadius, maxRadius);
        // We calculate the volume
        double volume = monteCarloIntegral(kernel, x1, x2, y1, y2);
        return volume;
    }
    
    public static final class MonteCarloKernel {
        
        private final double totalAreaFactor;
        private final List<Triplet<Double, Double, Double>> totalPointList;

        private MonteCarloKernel(double totalAreaFactor, List<Triplet<Double, Double, Double>> totalPointList) {
            this.totalAreaFactor = totalAreaFactor;
            this.totalPointList = totalPointList;
        }

        private double getTotalAreaFactor() {
            return totalAreaFactor;
        }

        private List<Triplet<Double, Double, Double>> getTotalPointList() {
            return totalPointList;
        }
    }
    
    
    
    /**
     * Calculates a kernel for the Monte Carlo based integration which uses as reference point
     * the integral of the function in an annulus area. This is computed by using the fact
     * that it is circularly symmetric. This method can be used by classes which implement
     * multiple integrations of the same function to speed up the process.
     */
    public static MonteCarloKernel monteCarloCircularSymmetricBivariateKernel(
            CircularlySymmetricBivariateFunction function, double minRadius, double maxRadius)
            throws MaxIterationsExceededException, FunctionEvaluationException {
        final int STEP_NO_OF_POINTS = 1000; // The number of points to add in each iteration
        final int MAX_ITER_NO = 10000; // The maximum number of iterations
        final double STEP_ACCURACY = 0.01; // The accuracy of the simulation
        final int ACCURATE_STEPS_TO_STOP = 100; // The number of accurate steps we need to stop
        
        double maxArea = Math.PI * maxRadius * maxRadius;
        double minArea = Math.PI * minRadius * minRadius;
        double baseArea = maxArea - minArea;
        double stepAreaFactor = baseArea / STEP_NO_OF_POINTS;
        double totalAreaFactor = stepAreaFactor / ACCURATE_STEPS_TO_STOP;
        
        // We calculate the integral of our function in this annulus. This is easy and fast,
        // because we have a circularly symmetric function. We will use this volume for
        // checking the simulation results.
        double totalVolume = bivariateIntegral(function, maxRadius) - bivariateIntegral(function, minRadius);
        
        // The list where we put the points that have a nice fit
        List<Triplet<Double, Double, Double>> totalPointList = new ArrayList<Triplet<Double, Double, Double>>();
        
        // We start the iteration
        int iterNo = 0; // This tells us in which iteration we are
        int accurateStepsFound = 0; // This is how many steps were accurate till now
        while (accurateStepsFound < ACCURATE_STEPS_TO_STOP && iterNo < MAX_ITER_NO) {
            iterNo++;
            //We create the points for the step
            int pointsToCreate = STEP_NO_OF_POINTS;
            double stepValueOfPoints = 0;
            List<Triplet<Double, Double, Double>> stepPointList = new LinkedList<Triplet<Double, Double, Double>>();
            while (pointsToCreate > 0) {
                double x = Math.random();
                x = (2 * x - 1) * maxRadius;
                double y = Math.random();
                y = (2 * y - 1) * maxRadius;
                // We keep a point only if it is inside the annulus
                double r = Math.sqrt(x * x + y * y);
                if (r <= maxRadius && r >= minRadius) {
                    double value = function.value(r);
                    stepPointList.add(new Triplet<Double, Double, Double>(x, y, value));
                    stepValueOfPoints += value;
                    pointsToCreate--;
                }
            }
            // We check the step accuracy and if it is accurate enough we add
            // the step points to the total
            double estimatedStepVolume = stepValueOfPoints * stepAreaFactor;
            double error = Math.abs((totalVolume - estimatedStepVolume) / totalVolume);
            if (error <= STEP_ACCURACY) {
                // The accuracy of the step is good enough. We add it in the total.
                accurateStepsFound++;
                totalPointList.addAll(stepPointList);
            }
        }
        
        // If we reached here and we don't have the correct number of accurate
        // steps means we exceeded the maximum number of iterations
        if (accurateStepsFound != ACCURATE_STEPS_TO_STOP) {
            throw new MaxIterationsExceededException(ACCURATE_STEPS_TO_STOP);
        }
        
        return new MonteCarloKernel(totalAreaFactor, totalPointList);
    }
    
    /**
     * <p>Calculates the two dimensional integral of the function which was used
     * to create the Monte Carlo kernel:</p>
     * {@latex.inline $
     * \\int\\limits_{y_1}^{y_2}\\int\\limits_{x_1}^{x_2}f_{(x,y)}dxdy
     * $}
     */
    public static double monteCarloIntegral(
            MonteCarloKernel kernel, double x1, double x2, double y1, double y2)
            throws MaxIterationsExceededException, FunctionEvaluationException {
        // We calculate the estimated integral by taking into consideration
        // only the points into our limit
        double volume = 0;
        for (Triplet<Double, Double, Double> triplet : kernel.getTotalPointList()) {
            double x = triplet.getValue0();
            double y = triplet.getValue1();
            if (x >= x1 && x <= x2 && y >= y1 && y <= y2) {
                volume = volume + triplet.getValue2() * kernel.getTotalAreaFactor();
            }
        }
        return volume;
    }
    
    private static double circularlySymmetricBivariateIntegral(
            CircularlySymmetricBivariateFunction function, double x1, double x2, double y1, double y2)
            throws FunctionEvaluationException {
        double integral = 0;
        // check if we have any values in the quarter x>0,y>0
        if (x2 > 0 && y2 > 0) {
            double newX1 = Math.max(0., x1);
            double newY1 = Math.max(0., y1);
            integral += circularlySymmetricBivariateFirstQuarterIntegral(function, newX1, x2, newY1, y2);
        }
        // check if we have any values in the quarter x>0,y<0
        if (x2 > 0 && y1 < 0) {
            double newX1 = Math.max(0., x1);
            double newY2 = Math.min(0., y2);
            integral += circularlySymmetricBivariateFirstQuarterIntegral(function, newX1, x2, -newY2, -y1);
        }
        // check if we have any values in the quarter x<0,y>0
        if (x1 < 0 && y2 > 0) {
            double newX2 = Math.min(0., x2);
            double newY1 = Math.max(0., y1);
            integral += circularlySymmetricBivariateFirstQuarterIntegral(function, -newX2, -x1, newY1, y2);
        }
        // check if we have any values in the quarter x<0,y<0
        if (x1 < 0 && y1 < 0) {
            double newX2 = Math.min(0., x2);
            double newY2 = Math.min(0., y2);
            integral += circularlySymmetricBivariateFirstQuarterIntegral(function, -newX2, -x1, -newY2, -y1);
        }
        return integral;
    }
    
    private static double circularlySymmetricBivariateFirstQuarterIntegral(
            CircularlySymmetricBivariateFunction function, double x1, double x2, double y1, double y2)
            throws FunctionEvaluationException {
        final int NO_OF_STEPS = 100;
        if (x2 <= 0 || y2 <= 0) {
            return 0;
        }
        double newX1 = Math.max(0., x1);
        double newY1 = Math.max(0., y1);
        double rMin = Math.sqrt(newX1 * newX1 + newY1 * newY1);
        double rMax = Math.sqrt(x2 * x2 + y2 * y2);
        double step = (rMax - rMin) / NO_OF_STEPS;
        double totalIntegral = 0;
        for (double r = rMin; r < rMax; r += step) {
            double rLow = r;
            double rHigh = Math.min(r + step, rMax);
            double referenceIntegral = bivariateIntegral(function, rHigh) - bivariateIntegral(function, rLow);
            Quartet<Double, Double, Double, Double> lowCrossPoints = findCrossPoints(rLow, x1, x2, y1, y2);
            double xLowA = lowCrossPoints.getValue0();
            double yLowA = lowCrossPoints.getValue1();
            double xLowB = lowCrossPoints.getValue2();
            double yLowB = lowCrossPoints.getValue3();
            double lowAngleA = (xLowA == 0) ? 0 : Math.atan(yLowA / xLowA);
            double lowAngleB = (xLowB == 0) ? Math.PI / 2. : Math.atan(yLowB / xLowB);
            double lowAngle = lowAngleB - lowAngleA;
            Quartet<Double, Double, Double, Double> highCrossPoints = findCrossPoints(rHigh, x1, x2, y1, y2);
            double xHighA = highCrossPoints.getValue0();
            double yHighA = highCrossPoints.getValue1();
            double xHighB = highCrossPoints.getValue2();
            double yHighB = highCrossPoints.getValue3();
            double highAngleA = (xHighA == 0) ? 0 : Math.atan(yHighA / xHighA);
            double highAngleB = (xHighB == 0) ? Math.PI / 2. : Math.atan(yHighB / xHighB);
            double highAngle = highAngleB - highAngleA;
            double factor = (highAngle + lowAngle) / (4 * Math.PI);
            totalIntegral += factor * referenceIntegral;
        }
        return totalIntegral;
    }
    
    private static Quartet<Double, Double, Double, Double> findCrossPoints(double r, double x1, double x2, double y1, double y2) {
        if (x1 < 0 || y1 < 0) {
            throw new IllegalArgumentException();
        }
        double r1_1 = Math.sqrt(x1 * x1 + y1 * y1);
        double r1_2 = Math.sqrt(x1 * x1 + y2 * y2);
        double r2_1 = Math.sqrt(x2 * x2 + y1 * y1);
        double r2_2 = Math.sqrt(x2 * x2 + y2 * y2);
        if (r < r1_1 || r > r2_2) {
            return null;
        }
        double xA = 0, yA = 0, xB = 0, yB = 0;
        // Check if we have a cross with the (x1,y1)(x2,y1) line
        if (r >= r1_1 && r <= r2_1) {
            yA = y1;
            xA = Math.sqrt(r * r - yA * yA);
        }
        // Check if we have a cross with the (x2,y1)(x2,y2) line
        if (r >= r2_1 && r <= r2_2) {
            xA = x2;
            yA = Math.sqrt(r * r - xA * xA);
        }
        // Check if we have a cross with the (x1,y1)(x1,y2) line
        if (r >= r1_1 && r <= r1_2) {
            xB = x1;
            yB = Math.sqrt(r * r - xB * xB);
        }
        // Check if we have a cross with the (x1,y2)(x2,y2) line
        if (r >= r1_2 && r <= r2_2) {
            yB = y2;
            xB = Math.sqrt(r * r - yB * yB);
        }
        return new Quartet<Double, Double, Double, Double>(xA, yA, xB, yB);
    }
    
    /**
     * Calculates an estimation of the integral {@latex.inline $\\int_{x_1}^{x_2}f_{(x)}\\,\\mathrm{d}x$}.
     * The estimation is based on assuming that the value of the function is constant for
     * the given X step.
     * @param function The function of which the integral is calculated
     * @param x1 The lower limit of the integral range
     * @param x2 The upper limit of the integral range
     * @param constantStep The X step in which the function is assumed constant
     * @return The value of the integral
     * @throws FunctionEvaluationException if the integration fails
     * @throws IllegalArgumentException if x1 > x2 or if the constant step is bigger
     * than 1% of the integration range
     */
    public static double constantStepIntegral(UnivariateRealFunction function, double x1,
            double x2, double constantStep) throws FunctionEvaluationException {
        // First check the x1 and x2. Note that if they are equal the integral is 0.
        if (x1 > x2) {
            throw new IllegalArgumentException("Illegal integration range [" + x1 + "," + x2 + "]");
        }
        if (x1 == x2) {
            return 0;
        }
        if (constantStep / (x2 - x1) > 0.01) {
            throw new IllegalArgumentException("The constant step cannot be bigger than 1% of "
                    + "the integration range");
        }
        double integral = 0;
        for (double x = x1; x <= x2; x += constantStep) {
            double value = function.value(x);
            integral += value * constantStep;
        }
        return integral;
    }
    
}
