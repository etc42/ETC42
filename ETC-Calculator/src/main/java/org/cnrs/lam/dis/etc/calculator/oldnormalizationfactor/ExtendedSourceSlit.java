/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.oldnormalizationfactor;

import lombok.EqualsAndHashCode;
import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.MaxIterationsExceededException;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.Cacheable;
import org.cnrs.lam.cesam.util.calculator.CachingPolicy;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.Calculator;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.CalculatorUnivariateRealFunction;
import org.cnrs.lam.dis.etc.calculator.util.IntegrationUtil;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.DoubleValueResult;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.javatuples.Quintet;
import org.javatuples.Unit;

/**
 * <p>The {@code ExtendedSourceSlit} class is a calculator for computing the
 * normalization factor. The equation used is:</p>
 * {@latex.inline $
 * C_{(\\lambda)} = \\displaystyle\\int\\limits_{-r_{obj}}^{r_{obj}}
 * \\int\\limits_{-w/2}^{w/2} I_{(x,y)}dxdy
 * $}
 * <p>where</p>
 * <ul>
 *   <li>{@latex.inline $C_{(\\lambda)}$}: is the normalization factor</li>
 *   <li>{@latex.inline $r_{obj}$}: is the effective radius of the source expressed in
 *       {@latex.inline $arcsec$}</li>
 *   <li>{@latex.inline $w$}: is the width of the slit expressed in 
 *       {@latex.inline $arcsec$}</li>
 *   <li>{@latex.inline $I_{(r)}$}: is the surface brightness profile</li>
 * </ul>
 * 
 * <p>The configuration of the calculator is a {@link org.javatuples.Quintet},
 * with contains the following elements:</p>
 * <ol>
 *   <li>{@link java.lang.Double}: The effective radius of the source</li>
 *   <li>{@link java.lang.String}: The unit of the effective radius</li>
 *   <li>{@link java.lang.Double}: The slit width</li>
 *   <li>{@link java.lang.String}: The unit of the slit width</li>
 *   <li>{@link org.cnrs.lam.cesam.util.calculator.Calculator}: a calculator
 *       for calculating the surface brightness profile</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the wavelength for which the calculation
 * is done (expressed in {@latex.inline \\AA}).</p>
 * 
 * <p>The output of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the normalization factor.</p>
 */
// Caching explenation
// -------------------
// This calculator is set to cache the instances because the first calculation is done
// by a time consuming integration (the value is not recalculated in later calls)
@Cacheable(CachingPolicy.INSTANCE)
@EqualsAndHashCode(callSuper = false, exclude={"integral"})
public class ExtendedSourceSlit extends AbstractCalculator<Quintet<Double, String, Double, String, Calculator<Unit<Double>, Unit<Double>>>, Unit<Double>, Unit<Double>> {
    
    private double sourceRadius;
    private double slitWidth;
    private Calculator<Unit<Double>, Unit<Double>> surfaceBrightnessProfileCalculator;
    private Double integral; // we buffer here the result of the integration, as it needs to be done only once
    
    /**
     * Validates the given configuration. It requires that the first value of the
     * tuple(the source radius) is a positive number, that the second value 
     * (the unit of the source radius) is {@latex.inline $arcsec$}, that the
     * third value (the slit width) is a positive number and that the fourth
     * value (the unit of the slit width) is {@latex.inline $arcsec$}.
     * @param configuration The configuration to validate
     * @throws ConfigurationException If the configuration is invalid
     */
    @Override
    protected void validateConfiguration(Quintet<Double, String, Double, String, Calculator<Unit<Double>, Unit<Double>>> configuration) throws ConfigurationException {
        // Check that the source radius is a possitive number
        if (configuration.getValue0() <= 0) {
            throw new ConfigurationException("Source radius must be a possitive "
                    + "number but was " + configuration.getValue0());
        }
        // Check if the source radius is in arcsec
        if (!Units.isArcsec(configuration.getValue1())) {
            throw new ConfigurationException("Source radius must be in " + Units.ARCSEC
                    + " but was in " + configuration.getValue1());
        }
        // Check that the slit width is a possitive number
        if (configuration.getValue2() <= 0) {
            throw new ConfigurationException("Slit width must be a possitive "
                    + "number but was " + configuration.getValue2());
        }
        // Check if the slit width is in arcsec
        if (!Units.isArcsec(configuration.getValue3())) {
            throw new ConfigurationException("Slit width must be in " + Units.ARCSEC
                    + " but was in " + configuration.getValue3());
        }
    }

    /**
     * Initializes the calculator instance with the given configuration. The first
     * value of the configuration is the source radius, the second 
     * is the unit of the source radius, the third is the slit width, the fourth
     * is the unit of the slit width and the fifth is a calculator for the
     * surface brightness profile.
     * @param configuration the configuration to use for initialization
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(Quintet<Double, String, Double, String, Calculator<Unit<Double>, Unit<Double>>> configuration) throws InitializationException {
        sourceRadius = configuration.getValue0();
        slitWidth = configuration.getValue2();
        surfaceBrightnessProfileCalculator = configuration.getValue4();
    }

    /**
     * Returns a {@link org.javatuples.Unit} containing the normalization factor.
     * @param input The wavelength expressed in {@latex.inline \\AA}
     * @return The normalization factor
     * @throws CalculationException if there is any error during the calculation
     */
    @Override
    protected Unit<Double> performCalculation(Unit<Double> input) throws CalculationException {
        if (integral == null) {
            CalculatorUnivariateRealFunction curFunction = new CalculatorUnivariateRealFunction(surfaceBrightnessProfileCalculator);
            try {
                integral = IntegrationUtil.monteCarloBivariateIntegralOfCircularlySymmetric(curFunction, -1 * sourceRadius, sourceRadius, slitWidth / -2, slitWidth / 2);
            } catch (MaxIterationsExceededException e) {
                throw new CalculationException("Calculation of normalization factor failed because "
                        + "the maximum iterations number was exceeded during integral calculation.");
            } catch (FunctionEvaluationException e) {
                throw new CalculationException("Calculation of normalization factor failed because "
                        + "there was a function evaluation exception.");
            }
        }
        return new Unit<Double>(integral);
    }

    /**
     * Overridden to add in the results the normalization factor.
     * @param input The wavelength expressed in {@latex.inline \\AA}
     * @param output The normalization factor
     */
    @Override
    protected void performForEveryCalculation(Unit<Double> input, Unit<Double> output) {
        ResultsHolder.getResults().addResult("NORMALIZATION_FACTOR", input.getValue0()
                , output.getValue0(), Units.ANGSTROM, null, Level.INTERMEDIATE_IMPORTANT);
    }

    /**
     * Overridden to add in the results the source radius and the slit width.
     * @param configuration The configuration of the calculator
     */
    @Override
    protected void performForEveryRetrieval(Quintet<Double, String, Double, String, Calculator<Unit<Double>, Unit<Double>>> configuration) {
        ResultsHolder.getResults().addResult(new DoubleValueResult("SOURCE_RADIUS",
                configuration.getValue0(), configuration.getValue1()), Level.DEBUG);
        ResultsHolder.getResults().addResult(new DoubleValueResult("SLIT_WIDTH",
                configuration.getValue2(), configuration.getValue3()), Level.DEBUG);
    }
}
