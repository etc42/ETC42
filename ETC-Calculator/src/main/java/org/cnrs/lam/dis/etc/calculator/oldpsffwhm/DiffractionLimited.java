/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.oldpsffwhm;

import lombok.EqualsAndHashCode;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.DoubleValueResult;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.javatuples.Pair;
import org.javatuples.Unit;

/**
 * <p>The {@code DiffractionLimited} class is a calculator for computing the FWHM
 * of the PSF. The equation used for the calculation is:</p>
 * {@latex.inline $
 * FWHM_{(\\lambda)}=1.22*\\displaystyle\\frac{\\lambda*10^{-8}}{D_1}*206265
 * $}
 * <p>where</p>
 * <ul>
 *   <li>{@latex.inline $FWHM_{(\\lambda)}$}: is the FWHM of the PSF expressed in
 *       {@latex.inline $arcsec$}</li>
 *   <li>{@latex.inline $\\lambda$}: is the wavelength expressed in angstrom 
 *       ({@latex.inline \\AA})</li>
 *   <li>{@latex.inline $D_1$}: is the diameter of the primary mirror expressed
 *       in centimeters ({@latex.inline$cm$})</li>
 * </ul>
 * 
 * <p>The configuration of the calculator is a {@link org.javatuples.Pair},
 * with contains the following elements:</p>
 * <ol>
 *   <li>{@link java.lang.Double}: The diameter of the primary mirror</li>
 *   <li>{@link java.lang.String}: The unit of the diameter of the primary mirror</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the wavelength for which the calculation
 * is done (expressed in {@latex.inline \\AA}).</p>
 * 
 * <p>The output of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the FWHM of the PSF (expressed in
 * {@latex.inline $arcsec$}).</p>
 */
@EqualsAndHashCode(callSuper=false)
public class DiffractionLimited extends AbstractCalculator<Pair<Double, String>, Unit<Double>, Unit<Double>> {
    
    private double diameter;

    /**
     * Validates the given configuration. It requires that the first value of the
     * tuple (the diameter of the telescope) is a positive number and that the second
     * value (the unit of the diameter) is {@latex.inline $cm$}.
     * 
     * @param configuration The configuration to validate
     * @throws ConfigurationException if the configuration is invalid
     */
    @Override
    protected void validateConfiguration(Pair<Double, String> configuration) throws ConfigurationException {
        // Check that the diameter is a possitive number
        if (configuration.getValue0() <= 0) {
            throw new ConfigurationException("Mirror diameter must be a possitive "
                    + "number but was " + configuration.getValue0());
        }
        // Check if the mirror diameter is in cm
        if (!Units.isCm(configuration.getValue1())) {
            throw new ConfigurationException("Mirror diameter must be in " + Units.CM
                    + " but was in " + configuration.getValue1());
        }
    }

    /**
     * Initializes the calculator instance with the given configuration. The first
     * value of the configuration is the diameter of the telescope and the second is
     * the unit of the diameter.
     * @param configuration The configuration of the calculator
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(Pair<Double, String> configuration) throws InitializationException {
        diameter = configuration.getValue0();
    }

    /**
     * Returns a {@link org.javatuples.Unit} containing the FWHM of the PSF
     * expressed in {@latex.inline $arcsec$}.
     * @param input The wavelength expressed in {@latex.inline \\AA}
     * @return The FWHM of the PSF expressed in {@latex.inline $arcsec$}
     * @throws CalculationException if there is any error during the calculation
     */
    @Override
    protected Unit<Double> performCalculation(Unit<Double> input) throws CalculationException {
        double result = 1.22 * (input.getValue0() * 1.e-8 / this.diameter) * 206265;
        return new Unit<Double>(result);
    }

    /**
     * Overridden to add in the results the FWHM of the PSF.
     * @param input The wavelength expressed in {@latex.inline \\AA}
     * @param output The FWHM of the PSF expressed in {@latex.inline $arcsec$}
     */
    @Override
    protected void performForEveryCalculation(Unit<Double> input, Unit<Double> output) {
        ResultsHolder.getResults().addResult("PSF_FWHM", input.getValue0(), output.getValue0(), 
                Units.ANGSTROM, Units.ARCSEC, Level.INTERMEDIATE_UNIMPORTANT);
    }

    /**
     * Overridden to add in the results the primary mirror diameter.
     * @param configuration The configuration of the calculator
     */
    @Override
    protected void performForEveryRetrieval(Pair<Double, String> configuration) {
        ResultsHolder.getResults().addResult(new DoubleValueResult("PRIMARY_DIAMETER",
                configuration.getValue0(), configuration.getValue1()), Level.DEBUG);
    }
    
}
