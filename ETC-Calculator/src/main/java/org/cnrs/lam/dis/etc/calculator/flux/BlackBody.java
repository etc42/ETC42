/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.flux;

import java.text.MessageFormat;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TreeMap;
import lombok.EqualsAndHashCode;
import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.apache.log4j.Logger;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Constants;
import org.cnrs.lam.dis.etc.calculator.util.FluxUtil;
import org.cnrs.lam.dis.etc.calculator.util.FunctionToDatasetResultConverter;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.DoubleDatasetResult;
import org.javatuples.Pair;
import org.javatuples.Quartet;
import org.javatuples.Tuple;
import org.javatuples.Unit;

/**
 * <p>The {@link BlackBody} class is a calculator for computing functions of the flux
 * , which is assumed to be a redshifted black body. To calculate the flux it first
 * calculates the black body radiation per unit projected area, by using the
 * equation:</p>
 * {@latex.inline $
 * I_{(\\lambda)} = \\displaystyle\\frac{2hc^2}{\\lambda^5}
 * \\displaystyle\\frac{1}{e^{\\frac{hc}{\\lambda kT_z}} - 1}
 * $}
 * <p>where</p>
 * <ul>
 *   <li>{@latex.inline $I_{(\\lambda)}$}: is the black body radiation</li>
 *   <li>{@latex.inline $h$}: is the Planck constant expressed in {@latex.inline $erg∗s$}</li>
 *   <li>{@latex.inline $c$}: is the speed of light expressed in
 *       {@latex.inline \\AA$/s$}</li>
 *   <li>{@latex.inline $\\lambda$}: is the wavelength expressed in {@latex.inline \\AA}</li>
 *   <li>{@latex.inline $T_z$}: is the black body redshifted temperature expressed in {@latex.inline $K$}
 *       and is calculated as {@latex.inline $T_z = \\frac{T}{1+z}$}, where {@latex.inline $T$}
 *       is the temperature of the body at redshift 0 and {@latex.inline $z$} is the redshift.</li>
 * </ul>
 * 
 * <p>Then this radiation is normalized by using the flux calculated from the AB
 * magnitude:</p>
 * {@latex.inline $
 * F = 10^{-\\frac{m_{AB}+48.6}{2.5}}*\\displaystyle\\frac{c}{\\lambda_{x}^2}
 * $}
 * <p>where</p>
 * <ul>
 *   <li>{@latex.inline $F$}: is the flux on which the radiation will be normalized,
 *       expressed in {@latex.inline $erg/s/cm^2/$\\AA}</li>
 *   <li>{@latex.inline $m_{AB}$}: is the AB magnitude</li>
 *   <li>{@latex.inline $c$}: is the speed of light expressed in
 *       {@latex.inline \\AA$/s$}</li>
 *   <li>{@latex.inline $\\lambda_{x}$}: is the central wavelength of the filter
 *       for which the AB magnitude is given, expressed in {@latex.inline \\AA}</li>
 * </ul>
 * 
 * <p>Note that the units of the blackbody radiation are not important, as the
 * radiation is being normalized.</p>
 * 
 * <p>The configuration of the calculator is a {@link Quartet},
 * witch contains the following elements:</p>
 * <ol>
 *   <li>{@link Double}: The AB magnitude</li>
 *   <li>{@link Pair}: The reference wavelength and its unit</li>
 *   <li>{@link Pair}: The black body temperature and its unit</li>
 *   <li>{@link Double}: The redshift</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Tuple}, as this
 * calculator does not accept any input. It should always be null.</p>
 * 
 * <p>The output of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link UnivariateRealFunction} representing the flux of the source.</p>
 */
// Caching explenation
// -------------------
// This calculator is set to cache the instances because during instantiation
// it creates the flux function.
@Cacheable(CachingPolicy.INSTANCE)
@EqualsAndHashCode(callSuper=false, exclude={"fluxFunction"})
public class BlackBody extends AbstractCalculator<
        Quartet<Double, Pair<Double, String>, Pair<Double, String>, Double>,
        Tuple, Unit<UnivariateRealFunction>> {
    
    private static final Logger logger = Logger.getLogger(BlackBody.class);
    private static final ResourceBundle validationErrorsBundle =
            ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");
    private double magnitude;
    private double magnitudeWavelength;
    private double temperature;
    private double redshift;
    private BlackBodyFunction fluxFunction;

    /**
     * Validates the configuration. It requires that the reference wavelength
     * is positive and expressed in Angstrom, that the black body temperature is
     * positive and expressed in Kelvin and that the redshift is non negative.
     * @param configuration
     * @throws ConfigurationException 
     */
    @Override
    protected void validateConfiguration(Quartet<Double, Pair<Double, String>, 
            Pair<Double, String>, Double> configuration) throws ConfigurationException {
        // Check that the magnitude wavelength is a positive number
        if (configuration.getValue1().getValue0() <= 0) {
            String message = validationErrorsBundle.getString("MAGNITUDE_WAVELENGTH_NOT_POSITIVE");
            message = MessageFormat.format(message, configuration.getValue1().getValue0());
            throw new ConfigurationException(message);
        }
        // Check if the magnitude wavelength is in angstrom
        if (!Units.isAngstrom(configuration.getValue1().getValue1())) {
            String message = validationErrorsBundle.getString("MAGNITUDE_WAVELENGTH_RADIUS_WRONG_UNIT");
            message = MessageFormat.format(message, Units.ANGSTROM, configuration.getValue1().getValue1());
            throw new ConfigurationException(message);
        }
        // Check that the temperature is a positive number
        if (configuration.getValue2().getValue0() <= 0) {
            String message = validationErrorsBundle.getString("BLACK_BODY_TEMPERATURE_NOT_POSITIVE");
            message = MessageFormat.format(message, configuration.getValue2().getValue0());
            throw new ConfigurationException(message);
        }
        // Check if the temperature is in kelvin
        if (!Units.isKelvins(configuration.getValue2().getValue1())) {
            String message = validationErrorsBundle.getString("BLACK_BODY_TEMPERATURE_WRONG_UNIT");
            message = MessageFormat.format(message, Units.KELVINS, configuration.getValue2().getValue1());
            throw new ConfigurationException(message);
        }
        // Check that the redshift is a non negative number
        if (configuration.getValue3() < 0) {
            String message = validationErrorsBundle.getString("REDSHIFT_NEGATIVE");
            message = MessageFormat.format(message, configuration.getValue3());
            throw new ConfigurationException(message);
        }
    }

    /**
     * Initializes the calculator instance with the given configuration.
     * @param configuration the configuration to use for initialization
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(Quartet<Double, Pair<Double, String>,
            Pair<Double, String>, Double> configuration) throws InitializationException {
        magnitude = configuration.getValue0();
        magnitudeWavelength = configuration.getValue1().getValue0();
        temperature = configuration.getValue2().getValue0();
        redshift = configuration.getValue3();
        
        // Calculate the factor for converting the black body radiation to flux
        double abFlux = FluxUtil.convertMagnitudeToFlux(magnitude, magnitudeWavelength);
        double radiation = 0;
        try {
            radiation = new BlackBodyFunction(temperature, 1.).value(magnitudeWavelength);
        } catch (FunctionEvaluationException ex) {
            logger.warn("Failed to calculate the black body flux for reference wavelength", ex);
            throw new InitializationException("Failed to calculate the black body flux for reference wavelength", ex);
        }
        double factor = abFlux / radiation;
        fluxFunction = new BlackBodyFunction(temperature, factor);
    }

    /**
     * Returns a {@link UnivariateRealFunction} which can be used to calculate
     * the source flux.
     * @param input The calculator has no input, so it can be always null
     * @return A {@link UnivariateRealFunction} which can be used to calculate
     * the source flux
     * @throws CalculationException if there is any error during the calculation
     */
    @Override
    protected Unit<UnivariateRealFunction> performCalculation(Tuple input) throws CalculationException {
        return new Unit<UnivariateRealFunction>(fluxFunction);
    }

    /**
     * <p>For every calculation the calculator adds in the simulation results
     * the following outputs of the calculator:</p>
     * <ul>
     *   <li>The source flux in intermediate unimportant results with code
     *       SIGNAL_FLUX</li>
     * </ul>
     * @param input
     * @param output 
     */
    @Override
    protected void performForEveryCalculation(Tuple input, Unit<UnivariateRealFunction> output) {
        // If the result already exists we do not add it to save time
        if (ResultsHolder.getResults().getResultByName("SIGNAL_FLUX") != null) {
            return;
        }
        UnivariateRealFunction function = output.getValue0();
        DoubleDatasetResult result = FunctionToDatasetResultConverter.convert(function,
                "SIGNAL_FLUX", Units.ANGSTROM, Units.getErgPerCm2PerSecPerAngstrom());
        ResultsHolder.getResults().addResult(result, CalculationResults.Level.INTERMEDIATE_UNIMPORTANT);
    }

    /**
     * <p>For every calculator instantiation the following parts of its configuration
     * are added in the simulation results:</p>
     * <ul>
     *   <li>The AB magnitude of the source in debug results with code
     *       SOURCE_AB_MAGNITUDE</li>
     *   <li>The reference wavelength of the AB magnitude in debug results with
     *       code SOURCE_AB_MAGNITUDE_FILTER</li>
     *   <li>The black body temperature in debug results with code BLACKBODY_TEMPERATURE</li>
     *   <li>The redshift in debug results with code REDSHIFT</li>
     * </ul>
     * @param configuration 
     */
    @Override
    protected void performForEveryRetrieval(Quartet<Double, Pair<Double, String>, Pair<Double, String>, Double> configuration) {
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("SOURCE_AB_MAGNITUDE"
                , configuration.getValue0(), null), CalculationResults.Level.DEBUG);
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("SOURCE_AB_MAGNITUDE_FILTER"
                , configuration.getValue1().getValue0(), configuration.getValue1().getValue1()), CalculationResults.Level.DEBUG);
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("BLACKBODY_TEMPERATURE"
                , configuration.getValue2().getValue0(), configuration.getValue2().getValue1()), CalculationResults.Level.DEBUG);
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("REDSHIFT"
                , configuration.getValue3(), null), CalculationResults.Level.DEBUG);
    }
    
    /**
     * Represents a scaled black body function.
     */
    @EqualsAndHashCode(callSuper=false)
    private class BlackBodyFunction implements UnivariateRealFunction {
        
        private double temperature;
        private double factor;

        public BlackBodyFunction(double temperature, double factor) {
            this.temperature = temperature;
            this.factor = factor;
        }

        @Override
        public double value(double x) throws FunctionEvaluationException {
            double a = 2 * Constants.PLANK * Constants.SPEED_OF_LIGHT * Constants.SPEED_OF_LIGHT; // 2*h*c^2
            double b = Math.pow(x, 5); // lambda^5
            double c = Constants.PLANK * Constants.SPEED_OF_LIGHT; // h*c
            double d = x * Constants.BOLTZMANN * (temperature / (1 + redshift)); // lambda*k* (T/(1+z))
            double e = Math.exp(c / d); // e^(h*c/(lambda*k*T/(1+z)))
            double radiation = (a / b) / (e - 1);
            return factor * radiation;
        }
        
    }
    
}
