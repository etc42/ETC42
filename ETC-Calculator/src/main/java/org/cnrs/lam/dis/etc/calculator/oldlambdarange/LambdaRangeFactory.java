/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.oldlambdarange;

import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.StringResult;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.cnrs.lam.dis.etc.datamodel.Instrument;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.javatuples.Pair;
import org.javatuples.Quartet;
import org.javatuples.Tuple;
import org.javatuples.Unit;

/**
 */
public class LambdaRangeFactory implements Factory<Unit<Session>, Tuple, Pair<Double, Double>> {

    @Override
    public Calculator<Tuple, Pair<Double, Double>> getCalculator(Unit<Session> configuration) throws InitializationException, ConfigurationException {
        Session session = configuration.getValue0();
        Instrument instrument = session.getInstrument();
        switch (instrument.getInstrumentType()) {
            case IMAGING:
                ResultsHolder.getResults().addResult(
                        new StringResult("WAVELENGTH_RANGE_METHOD", "Imaging"), Level.DEBUG);
                DatasetInfo filterTransmission = instrument.getFilterTransmission();
                DatasetInfo totalEfficiency = instrument.getTransmission();
                Pair<DatasetInfo, DatasetInfo> imagingConfiguration
                        = new Pair<DatasetInfo, DatasetInfo>(filterTransmission, totalEfficiency);
                return EtcCalculatorManager.getManager(Imaging.class).getCalculator(imagingConfiguration);
            case SPECTROGRAPH:
                ResultsHolder.getResults().addResult(
                        new StringResult("WAVELENGTH_RANGE_METHOD", "Spectroscopy"), Level.DEBUG);
                Double rangeMin = instrument.getRangeMin();
                String rangeMinUnit = instrument.getRangeMinUnit();
                Double rangeMax = instrument.getRangeMax();
                String rangeMaxUnit = instrument.getRangeMaxUnit();
                Quartet<Double, String, Double, String> spectroscopyConfiguration
                        = new Quartet<Double, String, Double, String>(rangeMin, rangeMinUnit, rangeMax, rangeMaxUnit);
                return EtcCalculatorManager.getManager(Spectroscopy.class).getCalculator(spectroscopyConfiguration);
        }
        throw new UnsupportedOperationException("Wavelength range calculation is "
                + "not supported for the given configuration");
    }
    
    public Calculator<Tuple, Pair<Double, Double>> getRangeOfTransmissionCalculator(Unit<Session> configuration) throws InitializationException, ConfigurationException {
        Session session = configuration.getValue0();
        Instrument instrument = session.getInstrument();
        DatasetInfo totalEfficiency = instrument.getTransmission();
        Unit<DatasetInfo> conf = new Unit<DatasetInfo>(totalEfficiency);
        return EtcCalculatorManager.getManager(TotalEfficiency.class).getCalculator(conf);
    }
    
}
