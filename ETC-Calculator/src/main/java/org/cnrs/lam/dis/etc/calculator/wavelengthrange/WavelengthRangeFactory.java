/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.wavelengthrange;

import java.util.ResourceBundle;

import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.filterresponse.FilterResponseFactory;
import org.cnrs.lam.dis.etc.calculator.systemefficiency.SystemEfficiencyFactory;
import org.cnrs.lam.dis.etc.calculator.util.functions.BoundedUnivariateFunction;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.cnrs.lam.dis.etc.datamodel.Instrument;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.javatuples.Pair;
import org.javatuples.Quartet;
import org.javatuples.Tuple;
import org.javatuples.Unit;

/**
 * The class {@code WavelengthRangeFactory} is a factory for creating
 * calculators for performing the wavelength range calculation.
 */
public class WavelengthRangeFactory implements Factory<Unit<Session>, Tuple, Pair<Double, Double>> {

	// The bundle with all the error translations
	private static final ResourceBundle validationErrorsBundle = ResourceBundle
			.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");

	/**
	 * Provides a calculator for performing wavelength range calculations. The
	 * type of the calculator returned is added in the debug results with the
	 * code WAVELENGTH_RANGE_METHOD.
	 * 
	 * @param configuration
	 *            The session for which the simulation is executed
	 * @return A calculator for performing wavelength range calculations
	 * @throws InitializationException
	 *             If the initialization of the calculator fails
	 * @throws ConfigurationException
	 *             If the configuration session is invalid
	 */
	@Override
	public Calculator<Tuple, Pair<Double, Double>> getCalculator(Unit<Session> configuration)
			throws InitializationException, ConfigurationException {
		Session session = configuration.getValue0();
		Instrument instrument = session.getInstrument();

		switch (instrument.getInstrumentType()) {
		case IMAGING:
			ResultsHolder.getResults().addResult(
					new CalculationResults.StringResult("WAVELENGTH_RANGE_METHOD", "Imaging"),
					CalculationResults.Level.DEBUG);
			Calculator<Tuple, Unit<BoundedUnivariateFunction>> filterCalculator = new FilterResponseFactory()
					.getCalculator(configuration);

			Calculator<Tuple, Unit<BoundedUnivariateFunction>> systemCalculator = new SystemEfficiencyFactory()
					.getCalculator(configuration);

			return EtcCalculatorManager.getManager(Imaging.class).getCalculator(
					new Pair<Calculator<Tuple, Unit<BoundedUnivariateFunction>>, Calculator<Tuple, Unit<BoundedUnivariateFunction>>>(
							filterCalculator, systemCalculator));

		case SPECTROGRAPH:
			ResultsHolder.getResults().addResult(
					new CalculationResults.StringResult("WAVELENGTH_RANGE_METHOD", "Spectroscopy"),
					CalculationResults.Level.DEBUG);
			String rangeMinUnit = instrument.getRangeMinUnit();
			Double rangeMin = instrument.getRangeMin();
			String rangeMaxUnit = instrument.getRangeMaxUnit();
			Double rangeMax = instrument.getRangeMax();

			Quartet<Double, String, Double, String> spectroscopyConfiguration = new Quartet<Double, String, Double, String>(
					rangeMin, rangeMinUnit, rangeMax, rangeMaxUnit);

			Spectroscopy calculator = EtcCalculatorManager.getManager(Spectroscopy.class)
					.getCalculator(spectroscopyConfiguration);

			return calculator;
		}
		String message = validationErrorsBundle.getString("UNKNOWN_WAVELENGTH_RANGE_METHOD");
		throw new ConfigurationException(message);
	}

}
