/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.oldsignal;

import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.oldatmospherictransmission.AtmosphericTransmissionFactory;
import org.cnrs.lam.dis.etc.calculator.dataset.DatasetFactory;
import org.cnrs.lam.dis.etc.calculator.oldfilterresponse.FilterResponseFactory;
import org.cnrs.lam.dis.etc.calculator.oldflux.FluxFactory;
import org.cnrs.lam.dis.etc.calculator.oldnormalizationfactor.NormalizationFactorFactory;
import org.cnrs.lam.dis.etc.calculator.oldsystemefficiency.SystemEfficiencyFactory;
import org.cnrs.lam.dis.etc.calculator.oldtelescopearea.TelescopeAreaFactory;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.StringResult;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.cnrs.lam.dis.etc.datamodel.ObsParam;
import org.cnrs.lam.dis.etc.datamodel.ObsParam.ExtraSignalType;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.javatuples.Pair;
import org.javatuples.Quartet;
import org.javatuples.Sextet;
import org.javatuples.Unit;

/**
 */
public class SignalFactory implements Factory<Unit<Session>, Unit<Double>, Unit<Double>> {

    @Override
    public Calculator<Unit<Double>, Unit<Double>> getCalculator(Unit<Session> configuration) throws InitializationException, ConfigurationException {
        Session session = configuration.getValue0();
        ObsParam obsParam = session.getObsParam();
        ExtraSignalType extraSignalType = obsParam.getExtraSignalType();
        
        // If we are going to use the calculated signal calculator we create it
        Calculator<Unit<Double>, Unit<Double>> calculatedCalculator = null;
        if (extraSignalType == ExtraSignalType.ONLY_CALCULATED_SIGNAL 
                || extraSignalType == ExtraSignalType.CALCULATED_AND_EXTRA_SIGNAL) {
            Calculator<Unit<Double>, Unit<Double>> flux = new FluxFactory().getCalculator(configuration);
            Calculator<Unit<Double>, Unit<Double>> normalizationFactor = new NormalizationFactorFactory().getCalculator(configuration);
            Calculator<Unit<Double>, Unit<Double>> atmosphericTransmission = new AtmosphericTransmissionFactory().getCalculator(configuration);
            Calculator<Unit<Double>, Unit<Double>> systemEfficiency = new SystemEfficiencyFactory().getCalculator(configuration);
            Calculator<Unit<Double>, Unit<Double>> filterResponse = new FilterResponseFactory().getCalculator(configuration);
            Calculator<Unit<Double>, Unit<Double>> telescopeArea = new TelescopeAreaFactory().getCalculator(configuration);

            Sextet<Calculator<Unit<Double>, Unit<Double>>, Calculator<Unit<Double>, Unit<Double>>, 
                    Calculator<Unit<Double>, Unit<Double>>, Calculator<Unit<Double>, Unit<Double>>, 
                    Calculator<Unit<Double>, Unit<Double>>, Calculator<Unit<Double>, Unit<Double>>> calculatorConfiguration 
                = new Sextet(flux, normalizationFactor, atmosphericTransmission,
                    systemEfficiency, filterResponse, telescopeArea);
            
            calculatedCalculator = EtcCalculatorManager.getManager(Signal.class).getCalculator(calculatorConfiguration);
            // If we have only the calculated signal we return it
            if (extraSignalType == ExtraSignalType.ONLY_CALCULATED_SIGNAL) {
                ResultsHolder.getResults().addResult(
                    new StringResult("SIGNAL_METHOD", "Calculated signal only"), Level.DEBUG);
                return calculatedCalculator;
            }
        }
        
        // If we are going to use the extra signal calculator we create it
        Calculator<Unit<Double>, Unit<Double>> extraCalculator = null;
        if (extraSignalType == ExtraSignalType.ONLY_EXTRA_SIGNAL 
                || extraSignalType == ExtraSignalType.CALCULATED_AND_EXTRA_SIGNAL) {
            DatasetInfo templateInfo = obsParam.getExtraSignalDataset();
            Quartet<Session, Dataset.Type, DatasetInfo, String> datasetConfiguration 
                    = new Quartet<Session, Dataset.Type, DatasetInfo, String>
                    (session, Dataset.Type.EXTRA_SIGNAL, templateInfo, null);
            Calculator<Unit<Double>, Unit<Double>> template = new DatasetFactory().getCalculator(datasetConfiguration);
            
            Pair<DatasetInfo, Calculator<Unit<Double>, Unit<Double>>> calculatorConfiguration =
                    new Pair<DatasetInfo, Calculator<Unit<Double>, Unit<Double>>>(templateInfo, template);
            
            extraCalculator = EtcCalculatorManager.getManager(ExtraSignal.class).getCalculator(calculatorConfiguration);
            // If we have only the extra signal we return it
            if (extraSignalType == ExtraSignalType.ONLY_EXTRA_SIGNAL) {
                ResultsHolder.getResults().addResult(
                    new StringResult("SIGNAL_METHOD", "Extra signal only"), Level.DEBUG);
                return extraCalculator;
            }
        }
        
        // If we are here means that we have calculated and extra signal
        ResultsHolder.getResults().addResult(
                new StringResult("SIGNAL_METHOD", "Calculated and extra signal"), Level.DEBUG);
        Pair<Calculator<Unit<Double>, Unit<Double>>, Calculator<Unit<Double>, Unit<Double>>> calculatorConfiguration = 
                new Pair<Calculator<Unit<Double>, Unit<Double>>, Calculator<Unit<Double>, Unit<Double>>>(calculatedCalculator, extraCalculator);
        return EtcCalculatorManager.getManager(SumOfCalculatedAndExtra.class).getCalculator(calculatorConfiguration);
    }
    
}
