/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.oldspectralresolution;

import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.dataset.DatasetFactory;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.StringResult;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.cnrs.lam.dis.etc.datamodel.Instrument;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.javatuples.Pair;
import org.javatuples.Quartet;
import org.javatuples.Unit;

/**
 */
public class SpectralResolutionFactory implements Factory<Unit<Session>, Unit<Double>, Unit<Double>> {

    @Override
    public Calculator<Unit<Double>, Unit<Double>> getCalculator(Unit<Session> configuration) throws InitializationException, ConfigurationException {
        Session session = configuration.getValue0();
        Instrument instrument = session.getInstrument();
        switch (instrument.getSpectralResolutionType()) {
            case SPECTRAL_RESOLUTION_FUNCTION:
                ResultsHolder.getResults().addResult(
                        new StringResult("SPECTRAL_RESOLUTION_METHOD", "Spectral Resolution Function"), Level.DEBUG);
                DatasetInfo spectralResolutionInfo = instrument.getSpectralResolution();
                Quartet<Session, Dataset.Type, DatasetInfo, String> spectralResolutionConfiguration =
                        new Quartet<Session, Dataset.Type, DatasetInfo, String>
                                (session, Dataset.Type.SPECTRAL_RESOLUTION, spectralResolutionInfo, null);
                Calculator<Unit<Double>, Unit<Double>> spectralResolution =
                        new DatasetFactory().getCalculator(spectralResolutionConfiguration);
                Pair<DatasetInfo, Calculator<Unit<Double>, Unit<Double>>> functionCalculatorConfiguration =
                        new Pair<DatasetInfo, Calculator<Unit<Double>, Unit<Double>>>
                                (spectralResolutionInfo, spectralResolution);
                return EtcCalculatorManager.getManager(SpectralResolutionProfile.class).getCalculator(functionCalculatorConfiguration);
            case CONSTANT_DELTA_LAMBDA:
                ResultsHolder.getResults().addResult(
                        new StringResult("SPECTRAL_RESOLUTION_METHOD", "Fixed Delta Lambda per pixel"), Level.DEBUG);
                Pair<Double, String> deltaLambdaConfiguration = new Pair<Double, String>
                        (instrument.getDeltaLambdaPerPixel(), instrument.getDeltaLambdaPerPixelUnit());
                return EtcCalculatorManager.getManager(FixedDeltaLambdaPerPixel.class).getCalculator(deltaLambdaConfiguration);
        }
        throw new UnsupportedOperationException("Spectral resolution calculation is "
                + "not supported for the given configuration");
    }
    
}
