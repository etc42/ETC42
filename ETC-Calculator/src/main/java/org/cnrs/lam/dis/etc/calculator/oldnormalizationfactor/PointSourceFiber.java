/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.oldnormalizationfactor;

import lombok.EqualsAndHashCode;
import org.apache.commons.math.MathException;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.Cacheable;
import org.cnrs.lam.cesam.util.calculator.CachingPolicy;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.Calculator;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.DoubleValueResult;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.javatuples.Triplet;
import org.javatuples.Unit;

/**
 * <p>The {@code PointSourceFiber} class is a calculator for computing the
 * normalization factor. The factor is calculated by integrating the Gaussian
 * distribution in the range {@latex.inline $[-\\frac{\\phi_{fiber}}{2},\\frac{\\phi_{fiber}}{2}]$}
 * with standard deviation {@latex.inline $\\sigma_{PSF}=\\frac{FWHM_{(\\lambda)}}{2.37}$}.
 * The equation used is:</p>
 * {@latex.inline $
 * C_{(\\lambda)}=ERF\\left[\\frac{\\phi_{fiber}/2}{\\sqrt{2}*\\sigma_{PSF}}\\right]
 * $}
 * <p>where</p>
 * <ul>
 *   <li>{@latex.inline $C_{(\\lambda)}$}: is the normalization factor</li>
 *   <li>{@latex.inline $\\phi_{fiber}$}: is the diameter of the fiber expressed in
 *       {@latex.inline $arcsec$}</li>
 *   <li>{@latex.inline $ERF$}: is the error function</li>
 * </ul>
 * 
 * <p>The configuration of the calculator is a {@link org.javatuples.Triplet},
 * with contains the following elements:</p>
 * <ol>
 *   <li>{@link java.lang.Double}: The fiber diameter</li>
 *   <li>{@link java.lang.String}: The unit of the fiber diameter</li>
 *   <li>{@link org.cnrs.lam.cesam.util.calculator.Calculator}: a calculator
 *       for calculating the FWHM of the PSF</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the wavelength for which the calculation
 * is done (expressed in {@latex.inline \\AA}).</p>
 * 
 * <p>The output of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the normalization factor.</p>
 */

// Caching explenation
// -------------------
// This calculator is set to cache the instances because tthe calculation is done
// by a time consuming integration
@Cacheable(CachingPolicy.ALL)
@EqualsAndHashCode(callSuper=false)
public class PointSourceFiber extends AbstractCalculator<Triplet<Double, String, Calculator<Unit<Double>, Unit<Double>>>, Unit<Double>, Unit<Double>> {
    
    private double fiberDiameter;
    private Calculator<Unit<Double>, Unit<Double>> fwhmCalculator;

    /**
     * Validates the given configuration. It requires that the first value of the
     * tuple(the fiber diameter) is a positive number and that the second value 
     * (the unit of the fiber diameter) is {@latex.inline $arcsec$}.
     * @param configuration The configuration to validate
     * @throws ConfigurationException If the configuration is invalid
     */
    @Override
    protected void validateConfiguration(Triplet<Double, String, Calculator<Unit<Double>, Unit<Double>>> configuration) throws ConfigurationException {
        // Check that the fiber diameter is a possitive number
        if (configuration.getValue0() <= 0) {
            throw new ConfigurationException("Fiber diameter must be a possitive "
                    + "number but was " + configuration.getValue0());
        }
        // Check if the fiber diameter is in arcsec
        if (!Units.isArcsec(configuration.getValue1())) {
            throw new ConfigurationException("Fiber diameter must be in " + Units.ARCSEC
                    + " but was in " + configuration.getValue1());
        }
    }

    /**
     * Initializes the calculator instance with the given configuration. The first
     * value of the configuration is the  fiber diameter, the second 
     * is the unit of the fiber diameter and the third one is a calculator for the
     * FWHM of the PSF.
     * @param configuration the configuration to use for initialization
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(Triplet<Double, String, Calculator<Unit<Double>, Unit<Double>>> configuration) throws InitializationException {
        fiberDiameter = configuration.getValue0();
        fwhmCalculator = configuration.getValue2();
    }
    
    /**
     * Returns a {@link org.javatuples.Unit} containing the normalization factor.
     * @param input The wavelength expressed in {@latex.inline \\AA}
     * @return The normalization factor
     * @throws CalculationException if there is any error during the calculation
     */
    @Override
    protected Unit<Double> performCalculation(Unit<Double> input) throws CalculationException {
        double fwhm = fwhmCalculator.calculate(input).getValue0();
        double var = fiberDiameter * 2.37 / (2 * Math.sqrt(2) * fwhm);
        double result = 0;
        try {
            result = org.apache.commons.math.special.Erf.erf(var);
        } catch (MathException ex) {
            throw new CalculationException("Calculation of normalization factor failed because " +
                    "the calculation of the ERF failed to converge.");
        }
        return new Unit<Double>(result);
    }

    /**
     * Overridden to add in the results the normalization factor.
     * @param input The wavelength expressed in {@latex.inline \\AA}
     * @param output The normalization factor
     */
    @Override
    protected void performForEveryCalculation(Unit<Double> input, Unit<Double> output) {
        ResultsHolder.getResults().addResult("NORMALIZATION_FACTOR", input.getValue0()
                , output.getValue0(), Units.ANGSTROM, null, Level.INTERMEDIATE_IMPORTANT);
    }

    /**
     * Overridden to add in the results the fiber diameter.
     * @param configuration The configuration of the calculator
     */
    @Override
    protected void performForEveryRetrieval(Triplet<Double, String, Calculator<Unit<Double>, Unit<Double>>> configuration) {
        ResultsHolder.getResults().addResult(new DoubleValueResult("FIBER_DIAMETER",
                configuration.getValue0(), configuration.getValue1()), Level.DEBUG);
    }
    
}
