/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.psf;

import java.util.ResourceBundle;
import org.apache.commons.math.analysis.BivariateRealFunction;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.datamodel.*;
import org.javatuples.Pair;
import org.javatuples.Quintet;
import org.javatuples.Triplet;
import org.javatuples.Unit;

/**
 * <p>The {@link PsfFactory} provides calculators for calculating bivariate functions
 * representing the PSF, with (x,y) expressed in {@latex.inline $arcsec$}, for given
 * wavelength values, expressed in {@latex.inline $\\AA$}. The configuration of the
 * factory is a {@link org.cnrs.lam.dis.etc.datamodel.Session} object containing
 * all the configuration from the user.</p>
 */
public class PsfFactory implements Factory<Unit<Session>, Unit<Double>, Unit<BivariateRealFunction>> {
    
    private static final ResourceBundle bundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");

    @Override
    public Calculator<Unit<Double>, Unit<BivariateRealFunction>> getCalculator(Unit<Session> configuration) throws InitializationException, ConfigurationException {
        Session session = configuration.getValue0();
        Instrument instrument = session.getInstrument();
        Instrument.PsfType psfType = instrument.getPsfType();
        Site site = session.getSite();
        Site.LocationType locationType = site.getLocationType();
        boolean seeingLimited = site.getSeeingLimited();
        
        // Calculate if we are diffraction limited
        boolean diffractionLimited = true;
        if (locationType == Site.LocationType.GROUND && seeingLimited) {
            diffractionLimited = false;
        }
        
        // Create the some configurations that we use more than one time
        Double seeing = site.getSeeing();
        String seeingUnit = site.getSeeingUnit();
        Double airMass = site.getAirMass();
        Triplet<Double, String, Double> seeingConfiguration = new Triplet(seeing, seeingUnit, airMass);
        Double diameter = instrument.getDiameter();
        String diameterUnit = instrument.getDiameterUnit();
        Pair<Double, String> diffractionLimitedConfiguration = new Pair(diameter, diameterUnit);
        DatasetInfo profileInfo = instrument.getFwhm();
        Unit<DatasetInfo> profileConfiguration = new Unit<DatasetInfo>(profileInfo);
        DatasetInfo doubleGaussianFwhm1 = instrument.getPsfDoubleGaussianFwhm1();
        DatasetInfo doubleGaussianFwhm2 = instrument.getPsfDoubleGaussianFwhm2();
        Double doubleGaussianMultiplier = instrument.getPsfDoubleGaussianMultiplier();
        Triplet<DatasetInfo, DatasetInfo, Double> doubleGaussianConfiguration =
                new Triplet<DatasetInfo, DatasetInfo, Double>(doubleGaussianFwhm1, doubleGaussianFwhm2, doubleGaussianMultiplier);
        
        // Check if we have diffraction limited simulation
        if (psfType == Instrument.PsfType.AUTO && diffractionLimited) {
            ResultsHolder.getResults().addResult( new CalculationResults.StringResult(
                    "PSF_METHOD", "Diffraction Limited"), CalculationResults.Level.DEBUG);
            return EtcCalculatorManager.getManager(DiffractionLimited.class).getCalculator(diffractionLimitedConfiguration);
        }
        // Check if we have FWHM PSF profile simulation
        if (psfType == Instrument.PsfType.PROFILE && diffractionLimited) {
            ResultsHolder.getResults().addResult( new CalculationResults.StringResult(
                    "PSF_METHOD", "Instrument PSF FWHM profile"), CalculationResults.Level.DEBUG);
            return EtcCalculatorManager.getManager(InstrumentPsfFwhmProfile.class).getCalculator(profileConfiguration);
        }
        // Check if we have seeing simulation
        if (psfType == Instrument.PsfType.AUTO && !diffractionLimited) {
            ResultsHolder.getResults().addResult( new CalculationResults.StringResult(
                    "PSF_METHOD", "Seeing"), CalculationResults.Level.DEBUG);
            return EtcCalculatorManager.getManager(Seeing.class).getCalculator(seeingConfiguration);
        }
        // Check if we have PSF profile and seeing together
        if (psfType == Instrument.PsfType.PROFILE && !diffractionLimited) {
            ResultsHolder.getResults().addResult( new CalculationResults.StringResult(
                    "PSF_METHOD", "Instrument PSF FWHM profile + Seeing"), CalculationResults.Level.DEBUG);
            InstrumentPsfFwhmProfile profileCalculator = EtcCalculatorManager.getManager(InstrumentPsfFwhmProfile.class).getCalculator(profileConfiguration);
            Seeing seeingCalculator = EtcCalculatorManager.getManager(Seeing.class).getCalculator(seeingConfiguration);
            Pair<Calculator<Unit<Double>, Unit<BivariateRealFunction>>, 
                 Calculator<Unit<Double>, Unit<BivariateRealFunction>>> totalConfiguration =
                    new Pair<Calculator<Unit<Double>, Unit<BivariateRealFunction>>, 
                             Calculator<Unit<Double>, Unit<BivariateRealFunction>>>(profileCalculator, seeingCalculator);
            return EtcCalculatorManager.getManager(InstrumentPlusSeeing.class).getCalculator(totalConfiguration);
        }
        // Check if we have adaptive optics
        if (psfType == Instrument.PsfType.AO && !diffractionLimited) {
            ResultsHolder.getResults().addResult( new CalculationResults.StringResult(
                    "PSF_METHOD", "Adaptive Optics"), CalculationResults.Level.DEBUG);
            DiffractionLimited diffLimCalculator = EtcCalculatorManager.getManager(DiffractionLimited.class).getCalculator(diffractionLimitedConfiguration);
            Seeing seeingCalculator = EtcCalculatorManager.getManager(Seeing.class).getCalculator(seeingConfiguration);
            Double strehlRatio = instrument.getStrehlRatio();
            Double refLambda = instrument.getRefWavelength();
            String refLambdaUnit = instrument.getRefWavelengthUnit();
            Quintet<Calculator<Unit<Double>, Unit<BivariateRealFunction>>, 
                    Calculator<Unit<Double>, Unit<BivariateRealFunction>>,
                    Double, Double, String> aoConfiguration = 
                new Quintet<Calculator<Unit<Double>, Unit<BivariateRealFunction>>,
                            Calculator<Unit<Double>, Unit<BivariateRealFunction>>,
                            Double, Double, String>(diffLimCalculator, seeingCalculator, strehlRatio, refLambda, refLambdaUnit);
            return EtcCalculatorManager.getManager(AdaptiveOptics.class).getCalculator(aoConfiguration);
        }
        //Check if we have double gaussian PSF
        if (psfType == Instrument.PsfType.DOUBLE_GAUSSIAN && diffractionLimited) {
            ResultsHolder.getResults().addResult( new CalculationResults.StringResult(
                    "PSF_METHOD", "Double Gaussian PSF"), CalculationResults.Level.DEBUG);
            return EtcCalculatorManager.getManager(DoubleGaussian.class).getCalculator(doubleGaussianConfiguration);
        }
        // Check if we have double Gaussian and seeing together
        if (psfType == Instrument.PsfType.DOUBLE_GAUSSIAN && !diffractionLimited) {
            ResultsHolder.getResults().addResult( new CalculationResults.StringResult(
                    "PSF_METHOD", "Double Gaussian + Seeing"), CalculationResults.Level.DEBUG);
            DoubleGaussian doubleGaussianCalculator = EtcCalculatorManager.getManager(DoubleGaussian.class).getCalculator(doubleGaussianConfiguration);
            Seeing seeingCalculator = EtcCalculatorManager.getManager(Seeing.class).getCalculator(seeingConfiguration);
            Pair<Calculator<Unit<Double>, Unit<BivariateRealFunction>>, 
                 Calculator<Unit<Double>, Unit<BivariateRealFunction>>> totalConfiguration =
                    new Pair<Calculator<Unit<Double>, Unit<BivariateRealFunction>>, 
                             Calculator<Unit<Double>, Unit<BivariateRealFunction>>>(doubleGaussianCalculator, seeingCalculator);
            return EtcCalculatorManager.getManager(InstrumentPlusSeeing.class).getCalculator(totalConfiguration);
        }
        String message = bundle.getString("UNKNOWN_PSF_METHOD");
        throw new ConfigurationException(message);
    }
    
}
