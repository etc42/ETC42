/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.oldspectralbinning;

import lombok.EqualsAndHashCode;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.DoubleValueResult;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.javatuples.Quartet;
import org.javatuples.Unit;

/**
 * <p>The {@code Slit} class is a calculator for computing the spectral binning,
 * in the case the instrument is a long slit spectrograph.
 * The equation used for the calculation is:</p>
 * {@latex.inline $
 * SpectralBinning=\\frac{w}{p}
 * $}
 * <p>where</p>
 * <ul>
 *   <li>{@latex.inline $SpectralBinning$}: is spectral binning in 
 *       {@latex.inline $pixels$}</li>
 *   <li>{@latex.inline $w$}: is the width of the slit expressed in 
 *       {@latex.inline $arcsec$}</li>
 *   <li>{@latex.inline $p$}: is the pixel scale of the instrument expressed in
 *       {@latex.inline $arcsec/pixel$}</li>
 * </ul>
 * 
 * <p>The configuration of the calculator is a {@link org.javatuples.Quartet},
 * with contains the following elements:</p>
 * <ol>
 *   <li>{@link java.lang.Double}: The pixel scale of the instrument</li>
 *   <li>{@link java.lang.String}: The unit of the pixel scale of the instrument</li>
 *   <li>{@link java.lang.Double}: The width of the slit</li>
 *   <li>{@link java.lang.String}: The unit of the width of the slit</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the wavelength for which the calculation
 * is done (expressed in {@latex.inline \\AA}). Note that the calculation is not
 * related with the wavelength and its value is ignored.</p>
 * 
 * <p>The output of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the Spectral Binning (expressed in
 * {@latex.inline $pixel$}).</p>
 */
@EqualsAndHashCode(callSuper = false)
public class Slit extends AbstractCalculator<Quartet<Double, String, Double, String>, Unit<Double>, Unit<Double>> {

    private double pixelScale;
    private double slitWidth;

    /**
     * Validates the given configuration. It requires that the first value of the
     * tuple (the pixel scale of the instrument) is a positive number, that the second
     * value (the unit of the pixel scale) is {@latex.inline $arcsec/pixel$}, that the
     * third (the width of the slit) is a positive number and that the fourth value 
     * (the unit of the slit width) is {@latex.inline $arcsec$}.
     * @param configuration The configuration to validate
     * @throws ConfigurationException If the configuration is invalid
     */
    @Override
    protected void validateConfiguration(Quartet<Double, String, Double, String> configuration) throws ConfigurationException {
        // Check that the pixel scale is a possitive number
        if (configuration.getValue0() <= 0) {
            throw new ConfigurationException("Pixel scale must be a possitive "
                    + "number but was " + configuration.getValue0());
        }
        // Check if the pixel scale is in arcsec/pixel
        if (!Units.getArcsecPerPixel().equals(configuration.getValue1())) {
            throw new ConfigurationException("Pixel scale must be in " + Units.ARCSEC + "/"
                    + Units.PIXEL + " but was in " + configuration.getValue1());
        }
        // Check that the slit width is a possitive number
        if (configuration.getValue2() <= 0) {
            throw new ConfigurationException("Slit width must be a possitive "
                    + "number but was " + configuration.getValue2());
        }
        // Check if the slit width is in arcsec
        if (!Units.isArcsec(configuration.getValue3())) {
            throw new ConfigurationException("Slit width must be in " + Units.ARCSEC
                    + " but was in " + configuration.getValue3());
        }
    }

    /**
     * Initializes the calculator instance with the given configuration. The first
     * value of the configuration is the pixel scale of the instrument, the second is
     * the unit of the pixel scale, the third is the width of the slit and the fourth 
     * is the unit of the slit width.
     * @param configuration the configuration to use for initialization
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(Quartet<Double, String, Double, String> configuration) throws InitializationException {
        pixelScale = configuration.getValue0();
        slitWidth = configuration.getValue2();
    }

    /**
     * Returns a {@link org.javatuples.Unit} containing the Spectral Binning
     * expressed in {@latex.inline $arcsec/pixel$}.
     * @param input The wavelength expressed in {@latex.inline \\AA}
     * @return The spectral binning expressed in {@latex.inline $arcsec$}
     * @throws CalculationException  if there is any error during the calculation
     */
    @Override
    protected Unit<Double> performCalculation(Unit<Double> input) throws CalculationException {
        double result = slitWidth / pixelScale;
        return new Unit<Double>(result);
    }

    /**
     * Overridden to add in the results the spectral binning.
     * @param input The wavelength expressed in {@latex.inline \\AA}
     * @param output The spectral binning expressed in {@latex.inline $arcsec$}
     */
    @Override
    protected void performForEveryCalculation(Unit<Double> input, Unit<Double> output) {
        ResultsHolder.getResults().addResult(new DoubleValueResult("SPECTRAL_BINNING",
                output.getValue0(), Units.PIXEL), Level.INTERMEDIATE_IMPORTANT);
    }

    /**
     * Overridden to add in the results the pixel scale and the slit width.
     * @param configuration The configuration of the calculator
     */
    @Override
    protected void performForEveryRetrieval(Quartet<Double, String, Double, String> configuration) {
        ResultsHolder.getResults().addResult(new DoubleValueResult("PIXEL_SCALE",
                configuration.getValue0(), configuration.getValue1()), Level.DEBUG);
        ResultsHolder.getResults().addResult(new DoubleValueResult("SLIT_WIDTH",
                configuration.getValue2(), configuration.getValue3()), Level.DEBUG);
    }
}
