/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.oldlambdarange;

import java.util.Iterator;
import java.util.Map;
import lombok.EqualsAndHashCode;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.Cacheable;
import org.cnrs.lam.cesam.util.calculator.CachingPolicy;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.DatasetProviderHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.Dataset.Type;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.javatuples.Pair;
import org.javatuples.Tuple;
import org.javatuples.Unit;

/**
 */

// Caching explenation
// -------------------
// This calculator is set to cache the instances because the initialization
// is retrieving the datasets and iterates through them, which is time consuming.
@Cacheable(CachingPolicy.INSTANCE)
@EqualsAndHashCode(callSuper=false, exclude={"minLambda", "maxLambda"})
public class TotalEfficiency extends AbstractCalculator<Unit<DatasetInfo>, Tuple, Pair<Double, Double>> {
    
    private DatasetInfo totalEfficiencyInfo;
    private double minLambda;
    private double maxLambda;

    @Override
    protected void validateConfiguration(Unit<DatasetInfo> configuration) throws ConfigurationException {
        // Check the total transmission
        DatasetInfo efficiencyInfo = configuration.getValue0();
        if (efficiencyInfo == null) {
            throw new ConfigurationException("Instrument does not have total transmission");
        }
        Dataset dataset = DatasetProviderHolder.getDatasetProvider().getDataset(Dataset.Type.TRANSMISSION, efficiencyInfo);
        if (dataset == null) {
            throw new ConfigurationException("Data for total transmission "
                    + efficiencyInfo + " is not available");
        }
        if (!Units.isAngstrom(dataset.getXUnit())) {
            throw new ConfigurationException("Unit of X axis of total transmission must be "
                    + Units.ANGSTROM + " but was " + dataset.getXUnit());
        }
    }

    @Override
    protected void initialize(Unit<DatasetInfo> configuration) throws InitializationException {
        totalEfficiencyInfo = configuration.getValue0();
        Pair<Double, Double> minMax = calculateMinAndMax(DatasetProviderHolder.getDatasetProvider().getDataset(Type.TRANSMISSION, totalEfficiencyInfo));
        minLambda = minMax.getValue0();
        maxLambda = minMax.getValue1();
    }

    @Override
    protected Pair<Double, Double> performCalculation(Tuple input) throws CalculationException {
        return new Pair<Double, Double>(minLambda, maxLambda);
    }

    private Pair<Double, Double> calculateMinAndMax(Dataset dataset) {
        Map<Double, Double> data = dataset.getData();
        int lowLimit = 0;
        int highLimit = 0;
        boolean lowLimitFound = false;
        int i = -1;
        for (Map.Entry<Double, Double> entry : data.entrySet()) {
            i++;
            Double value = entry.getValue();
            // if we have a zero value and we still don't have the low limit continue
            if (!lowLimitFound && value == 0)
                continue;
            // The first time we don't have a zero we set the low limit
            if (!lowLimitFound && value != 0) {
                lowLimitFound = true;
                lowLimit = i;
                highLimit = i;
            }
            // If we have a non zero value we set the high limit
            if (value != 0)
                highLimit = i;
        }
        i = -1;
        Iterator<Double> lambdaIterator = data.keySet().iterator();
        double min = 0;
        double max = 0;
        while (lambdaIterator.hasNext()) {
            i++;
            double lambda = lambdaIterator.next();
            if (i == lowLimit)
                min = lambda;
            if (i == highLimit)
                max = lambda;
        }
        return new Pair<Double, Double>(min, max);
    }
    
}
