/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.spectralresolution;

import java.text.MessageFormat;
import java.util.Map;
import java.util.ResourceBundle;
import lombok.EqualsAndHashCode;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.Cacheable;
import org.cnrs.lam.cesam.util.calculator.CachingPolicy;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.DatasetProviderHolder;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.FunctionToDatasetResultConverter;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.calculator.util.datasets.LinearFunctionDataset;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.javatuples.Tuple;
import org.javatuples.Unit;

/**
 *<p>The {@link SpectralResolutionProfile} class is a calculator for computing the 
 * spectral resolution per pixel from a dataset. The configuration of the calculator
 * is a {@link org.javatuples.Unit} which contains the following elements:</p>
 * <ol>
 *   <li>{@link DatasetInfo}: the information of the spectral resolution dataset</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Tuple}, as this
 * calculator does not accept any input. It should always be null.</p>
 * 
 * <p>The output of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link UnivariateRealFunction} representing the spectral resolution.</p>
 */
// Caching explenation
// -------------------
// This calculator is set to cache the instances because the configuration validation
// and the instantiation are retrieving the dataset, which is time consuming.
@Cacheable(CachingPolicy.INSTANCE)
@EqualsAndHashCode(callSuper=false, of={"spectralResolutionDatasetInfo"})
public class SpectralResolutionProfile extends AbstractCalculator<Unit<DatasetInfo>, Tuple, Unit<UnivariateRealFunction>> {
    
    private static final ResourceBundle validationErrorsBundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");
    private DatasetInfo spectralResolutionDatasetInfo;
    private LinearFunctionDataset spectralResolutionFunction;

    /**
     * Validates the given configuration. It requires that the instrument has a
     * spectral resolution, that the data for the spectral resolution is 
     * available and that the unit of X and Y axes is {@latex.inline \\AA}.
     * @param configuration the configuration to validate
     * @throws ConfigurationException if the configuration is invalid
     */
    @Override
    protected void validateConfiguration(Unit<DatasetInfo> configuration) throws ConfigurationException {
        DatasetInfo info = configuration.getValue0();
        if (info == null) {
            String message = validationErrorsBundle.getString("SPECTRAL_RESOLUTION_MISSING");
            throw new ConfigurationException(message);
        }
        Dataset dataset = DatasetProviderHolder.getDatasetProvider().getDataset(Dataset.Type.SPECTRAL_RESOLUTION, info);
        if (dataset == null) {
            String message = validationErrorsBundle.getString("SPECTRAL_RESOLUTION_NOT_AVAILABLE");
            message = MessageFormat.format(message, info);
            throw new ConfigurationException(message);
        }
        if (!Units.isAngstrom(dataset.getXUnit())) {
            String message = validationErrorsBundle.getString("SPECTRAL_RESOLUTION_WRONG_X_UNIT");
            message = MessageFormat.format(message, Units.ANGSTROM, dataset.getXUnit());
            throw new ConfigurationException(message);
        }
        if (!Units.isAngstromPerPixel(dataset.getYUnit())) {
            String message = validationErrorsBundle.getString("SPECTRAL_RESOLUTION_WRONG_Y_UNIT");
            message = MessageFormat.format(message, Units.ANGSTROM, dataset.getXUnit());
            throw new ConfigurationException(message);
        }
    }

    /**
     * Initializes the calculator instance with the given configuration.
     * @param configuration the configuration to use for initialization
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(Unit<DatasetInfo> configuration) throws InitializationException {
        spectralResolutionDatasetInfo = configuration.getValue0();
        Dataset dataset = DatasetProviderHolder.getDatasetProvider().getDataset(Dataset.Type.SPECTRAL_RESOLUTION, spectralResolutionDatasetInfo);
        spectralResolutionFunction = new LinearFunctionDataset(dataset.getData());
    }

    /**
     * Returns a {@link UnivariateRealFunction} which can be used to calculate
     * the spectral resolution.
     * @param input The calculator has no input, so it can be always null
     * @return A {@link UnivariateRealFunction} which can be used to calculate
     * the spectral resolution
     * @throws CalculationException if there is any error during the calculation
     */
    @Override
    protected Unit<UnivariateRealFunction> performCalculation(Tuple input) throws CalculationException {
        return new Unit<UnivariateRealFunction>(spectralResolutionFunction);
    }

    /**
     * <p>For every calculation the calculator adds in the simulation results
     * the following outputs of the calculator:</p>
     * <ul>
     *   <li>The spectral resolution in intermediate unimportant results with code
     *       SPECTRAL_RESOLUTION</li>
     * </ul>
     * @param input
     * @param output 
     */
    @Override
    protected void performForEveryCalculation(Tuple input, Unit<UnivariateRealFunction> output) {
        // If the result already exists we do not add it to save time
        if (ResultsHolder.getResults().getResultByName("SPECTRAL_RESOLUTION") != null) {
            return;
        }
        UnivariateRealFunction function = output.getValue0();
        CalculationResults.DoubleDatasetResult result = FunctionToDatasetResultConverter.convert(function, 
                "SPECTRAL_RESOLUTION", Units.ANGSTROM, Units.ANGSTROM);
        ResultsHolder.getResults().addResult(result, CalculationResults.Level.INTERMEDIATE_UNIMPORTANT);
    }

    /**
     * <p>For every calculator instantiation the following parts of its configuration
     * are added in the simulation results:</p>
     * <ul>
     *   <li>The name of the spectral resolution profile with code SPECTRAL_RESOLUTION_FUNCTION</li>
     * </ul>
     * @param configuration 
     */
    @Override
    protected void performForEveryRetrieval(Unit<DatasetInfo> configuration) {
        CalculationResults.StringResult result = new CalculationResults.StringResult(
                "SPECTRAL_RESOLUTION_FUNCTION", configuration.getValue0().toString());
        ResultsHolder.getResults().addResult(result, CalculationResults.Level.DEBUG);
    }
    
}
