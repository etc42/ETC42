/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.covolvedsurfacebrightnessprofile;

import java.util.Map;
import java.util.TreeMap;
import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.cnrs.lam.dis.etc.calculator.util.datasets.LinearFunctionDataset;
import org.cnrs.lam.dis.etc.calculator.util.functions.CircularlySymmetricBivariateFunction;
import org.cnrs.lam.dis.etc.calculator.util.functions.IntegrableBivariateFunction;
import org.javatuples.Pair;

/**
 */
public class BivariateFunctionDataset implements IntegrableBivariateFunction, CircularlySymmetricBivariateFunction {
    
    private double[][] data;
    private double minX;
    private double minY;
    private double step;
    private UnivariateRealFunction projection;
    private UnivariateRealFunction polarProjection;

    public BivariateFunctionDataset(double[][] data, double minX, double minY, double step) {
        this.data = data;
        this.minX = minX;
        this.minY = minY;
        this.step = step;
        // Create the projection function
        Map<Double, Double> projectionData = new TreeMap<Double, Double>();
        Map<Double, Double> polarProjectionData = new TreeMap<Double, Double>();
        for (int i = 0; i <data.length; i++) {
            double x = minX + i * step;
            projectionData.put(x, data[i][data[0].length/2]);
            polarProjectionData.put(x, x * data[i][data[0].length/2]);
        }
        projection = new LinearFunctionDataset(projectionData);
        polarProjection = new LinearFunctionDataset(polarProjectionData);
    }

    @Override
    public double integral(double x1, double x2, double y1, double y2) throws FunctionEvaluationException {
        if (x1 > x2 || y1 > y2) {
            throw new IllegalArgumentException("Illegal integration range [" + x1 + "," + x2 + "][" + y1 + "," + y2 + "]");
        }
        x1 = Math.max(x1, minX);
        x2 = Math.min(x2, minX + data.length * step);
        y1 = Math.max(y1, minY);
        y2 = Math.min(y2, minY + data[0].length * step);
        if (x1 == x2 || y1 == y2) {
            return 0;
        }
        
        Pair<Integer, Integer> x1Borders = findBorderingIndexes(x1, minX, step);
        Pair<Integer, Integer> x2Borders = findBorderingIndexes(x2, minX, step);
        Pair<Integer, Integer> y1Borders = findBorderingIndexes(y1, minX, step);
        Pair<Integer, Integer> y2Borders = findBorderingIndexes(y2, minX, step);
        double integral = 0;
        for (int i = x1Borders.getValue1(); i <= x2Borders.getValue0(); i ++) {
            for (int j = y1Borders.getValue1(); j <= y2Borders.getValue0(); j ++) {
                double xL = Math.max(x1, minX + (i - 1./2.) * step);
                double xH = Math.min(x2, minX + (i + 1./2.) * step);
                double yL = Math.max(y1, minY + (j - 1./2.) * step);
                double yH = Math.min(y2, minY + (j + 1./2.) * step);
                integral += data[i][j] * (xH - xL) * (yH - yL);
            }
        }
        return integral;
    }

    @Override
    public double value(double x, double y) throws FunctionEvaluationException {
        if (x < minX || y < minY) {
            return 0;
        }
        Pair<Integer, Integer> xBorders = findBorderingIndexes(x, minX, step);
        Pair<Integer, Integer> yBorders = findBorderingIndexes(y, minY, step);
        if (xBorders.getValue1() >= data.length || yBorders.getValue1() >= data[0].length) {
            return 0;
        }
        double xL = minX + xBorders.getValue0() * step;
        double xH = minX + xBorders.getValue1() * step;
        double yL = minX + yBorders.getValue0() * step;
        double yH = minX + yBorders.getValue1() * step;
        // We calculate the multiplier for each corner, according how close is the points
        double multiplierLL = (xH - x) * (yH - y);
        double multiplierLH = (xH - x) * (y - yL);
        double multiplierHL = (x - xL) * (yH - y);
        double multiplierHH = (x - xL) * (y - yL);
        // We need a factor with which if we multiply the sum of the multipliers we get one
        double factor = 1 / (multiplierLL + multiplierLH + multiplierHL + multiplierHH);
        // We calcualte the value
        double value = multiplierLL * data[xBorders.getValue0()][yBorders.getValue0()];
        value += multiplierLH * data[xBorders.getValue0()][yBorders.getValue1()];
        value += multiplierHL * data[xBorders.getValue1()][yBorders.getValue0()];
        value += multiplierHH * data[xBorders.getValue1()][yBorders.getValue1()];
        value = factor * value;
        return value;
    }
    
    private Pair<Integer, Integer> findBorderingIndexes(double key, double min, double step) {
        if (key < min) {
            throw new IllegalArgumentException("The key cannot be smaller than the minimum");
        }
        int i = 0;
        while (min + i * step < key) {
            i++;
        }
        return new Pair<Integer, Integer>(i, i+1);
    }

    @Override
    public double value(double r) throws FunctionEvaluationException {
        return value(r, 0);
    }

    @Override
    public UnivariateRealFunction projectionFunction() {
        return projection;
    }

    @Override
    public UnivariateRealFunction polarFunction() {
        return polarProjection;
    }
    
}
