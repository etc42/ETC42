/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.oldbackgroundnoise;

import lombok.EqualsAndHashCode;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.Cacheable;
import org.cnrs.lam.cesam.util.calculator.CachingPolicy;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.CalculationListener;
import org.cnrs.lam.cesam.util.calculator.Calculator;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.DatasetProviderHolder;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Constants;
import org.cnrs.lam.dis.etc.calculator.util.IntegrationUtil;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.DoubleValueResult;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.javatuples.Octet;
import org.javatuples.Pair;
import org.javatuples.Tuple;
import org.javatuples.Unit;

/**
 */

// Caching explenation
// -------------------
// This calculator is set to cache the instances because the initialization
// includes the calculation of the background integral
@Cacheable(CachingPolicy.INSTANCE)
@EqualsAndHashCode(callSuper=false, exclude={"integral"})
public class Slitless extends AbstractCalculator<Octet<Calculator<Unit<Double>, Unit<Double>>,
                                                         Calculator<Unit<Double>, Unit<Double>>,
                                                         Calculator<Unit<Double>, Unit<Double>>,
                                                         Pair<Double, String>,
                                                         Calculator<Unit<Double>, Unit<Double>>,
                                                         Calculator<Unit<Double>, Unit<Double>>,
                                                         Calculator<Unit<Double>, Unit<Double>>,
                                                         Calculator<Tuple, Pair<Double, Double>>
                                                 >, Unit<Double>, Unit<Double>> {
    
    private Calculator<Unit<Double>, Unit<Double>> flux;
    private Calculator<Unit<Double>, Unit<Double>> systemEfficiency;
    private Calculator<Unit<Double>, Unit<Double>> filterResponse;
    private Double pixelScale;
    private Calculator<Unit<Double>, Unit<Double>> spectralResolutionCalculator;
    private Calculator<Unit<Double>, Unit<Double>> spatialBinningCalculator;
    private Calculator<Unit<Double>, Unit<Double>> telescopeArea;
    private Calculator<Tuple, Pair<Double, Double>> integrationRange;
    
    private double integral;

    @Override
    protected void validateConfiguration(Octet<Calculator<Unit<Double>, Unit<Double>>, 
                                                Calculator<Unit<Double>, Unit<Double>>,
                                                Calculator<Unit<Double>, Unit<Double>>,
                                                Pair<Double, String>,
                                                Calculator<Unit<Double>, Unit<Double>>,
                                                Calculator<Unit<Double>, Unit<Double>>,
                                                Calculator<Unit<Double>, Unit<Double>>,
                                                Calculator<Tuple, Pair<Double, Double>>> configuration) throws ConfigurationException {
        // Check that the pixel scale is a possitive number
        if (configuration.getValue3().getValue0() <= 0) {
            throw new ConfigurationException("Pixel scale must be a possitive "
                    + "number but was " + configuration.getValue3().getValue0());
        }
        // Check if the pixel scale is in arcsec/pixel
        if (!Units.getArcsecPerPixel().equals(configuration.getValue3().getValue1())) {
            throw new ConfigurationException("Pixel scale must be in " + Units.ARCSEC + "/"
                    + Units.PIXEL + " but was in " + configuration.getValue3().getValue1());
        }
    }

    @Override
    protected void initialize(Octet<Calculator<Unit<Double>, Unit<Double>>, 
                                    Calculator<Unit<Double>, Unit<Double>>,
                                    Calculator<Unit<Double>, Unit<Double>>,
                                    Pair<Double, String>,
                                    Calculator<Unit<Double>, Unit<Double>>,
                                    Calculator<Unit<Double>, Unit<Double>>,
                                    Calculator<Unit<Double>, Unit<Double>>,
                                    Calculator<Tuple, Pair<Double, Double>>> configuration) throws InitializationException {
        flux = configuration.getValue0();
        systemEfficiency = configuration.getValue1();
        filterResponse = configuration.getValue2();
        pixelScale = configuration.getValue3().getValue0();
        spectralResolutionCalculator = configuration.getValue4();
        spatialBinningCalculator = configuration.getValue5();
        telescopeArea = configuration.getValue6();
        integrationRange = configuration.getValue7();
        
        calculateIntegral(flux, systemEfficiency, filterResponse, integrationRange);
    }

    private void calculateIntegral(final Calculator<Unit<Double>, Unit<Double>> flux,
            final Calculator<Unit<Double>, Unit<Double>> systemEfficiency,
            final Calculator<Unit<Double>, Unit<Double>> filterResponse,
            Calculator<Tuple, Pair<Double, Double>> integrationRange) throws InitializationException {
        Pair<Double, Double> range = null;
        try {
            range = integrationRange.calculate(null);
        } catch (CalculationException e) {
            throw new InitializationException("Failed to calculate the integration range for "
                    + "background flux calculation", e);
        }
        double lambdaMin = range.getValue0();
        double lambdaMax = range.getValue1();
        Calculator<Unit<Double>, Unit<Double>> function = new Calculator<Unit<Double>, Unit<Double>>() {
            @Override
            public Unit<Double> calculate(Unit<Double> input) throws CalculationException {
                    double fluxValue = flux.calculate(input).getValue0();
                    double efficiency = systemEfficiency.calculate(input).getValue0();
                    double filterValue = filterResponse.calculate(input).getValue0();
                    double result = fluxValue * efficiency * filterValue;
                    return new Unit<Double>(result);
            }
            @Override
            public void setCalculationListener(CalculationListener calculationListener) {
                throw new UnsupportedOperationException("Not supported yet.");
            }
            @Override
            public CalculationListener getCalculationListener() {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        };
        try {
            integral = IntegrationUtil.templateIntegral(function, lambdaMin, lambdaMax);
        } catch (CalculationException ex) {
            throw new InitializationException(ex.getMessage(), ex);
        }
    }

    @Override
    protected Unit<Double> performCalculation(Unit<Double> input) throws CalculationException {
        double lambda = input.getValue0();
        double spectralResolutionPerPixel = spectralResolutionCalculator.calculate(input).getValue0();
        double angstromWidthInArcsec = pixelScale * spectralResolutionPerPixel / lambda;
        double spatialBinningInPixel = Math.ceil(spatialBinningCalculator.calculate(input).getValue0());
        double spatialBinningInArcsec = pixelScale * spatialBinningInPixel;
        double skyArea = angstromWidthInArcsec * spatialBinningInArcsec;
        double telescopeAreaValue = telescopeArea.calculate(input).getValue0();
        double electronsPerSecPerAngstrom = integral * skyArea * telescopeAreaValue
                * lambda / (Constants.SPEED_OF_LIGHT * Constants.PLANK);
        return new Unit<Double>(electronsPerSecPerAngstrom);
        
    }

    @Override
    protected void performForEveryCalculation(Unit<Double> input, Unit<Double> output) {
        ResultsHolder.getResults().addResult("BACKGROUND_NOISE", input.getValue0(),
                output.getValue0(), Units.ANGSTROM, Units.getElectronsPerSecPerAngstrom(), Level.INTERMEDIATE_UNIMPORTANT);
    }

    @Override
    protected void performForEveryRetrieval(Octet<Calculator<Unit<Double>, Unit<Double>>, 
                                                Calculator<Unit<Double>, Unit<Double>>,
                                                Calculator<Unit<Double>, Unit<Double>>,
                                                Pair<Double, String>,
                                                Calculator<Unit<Double>, Unit<Double>>,
                                                Calculator<Unit<Double>, Unit<Double>>,
                                                Calculator<Unit<Double>, Unit<Double>>,
                                                Calculator<Tuple, Pair<Double, Double>>> configuration) {
        ResultsHolder.getResults().addResult(new DoubleValueResult("PIXEL_SCALE",
                configuration.getValue3().getValue0(), configuration.getValue3().getValue1()), Level.DEBUG);
    }
    
}
