/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.oldflux;

import lombok.EqualsAndHashCode;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.Cacheable;
import org.cnrs.lam.cesam.util.calculator.CachingPolicy;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.FluxUtil;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.DoubleValueResult;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.StringResult;
import org.cnrs.lam.dis.etc.datamodel.FilterBand;
import org.javatuples.Pair;
import org.javatuples.Unit;

/**
 * <p>The {@code Continuum} class is a calculator for computing the flux of the
 * source. It assumes a constant flux value for all wavelengths, which is
 * calculated as:</p>
 * {@latex.inline $
 * F_{(\\lambda)} = 10^{-\\frac{m_{AB}+48.6}{2.5}}*\\displaystyle\\frac{c}{\\lambda_{x}^2}
 * $}
 * <p>where</p>
 * <ul>
 *   <li>{@latex.inline $F_{(\\lambda)}$}: is the calculated source flux, expressed
 *       in {@latex.inline $erg/s/cm^2/$\\AA}</li>
 *   <li>{@latex.inline $m_{AB}$}: is the AB magnitude</li>
 *   <li>{@latex.inline $c$}: is the speed of light expressed in
 *       {@latex.inline \\AA$/s$}</li>
 *   <li>{@latex.inline $\\lambda_{x}$}: is the central wavelength of the filter
 *       for which the AB magnitude is given, expressed in {@latex.inline \\AA}</li>
 * </ul>
 * 
 * <p>The configuration of the calculator is a {@link org.javatuples.Pair},
 * with contains the following elements:</p>
 * <ol>
 *   <li>{@link java.lang.Double}: The AB magnitude</li>
 *   <li>{@link org.cnrs.lam.dis.etc.datamodel.FilterBand}: The filter for which
 *       theAB magnitude is given</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the wavelength for which the calculation
 * is done (expressed in {@latex.inline \\AA}).</p>
 * 
 * <p>The output of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the signal flux (expressed in
 * {@latex.inline $erg/s/cm^2/$\\AA}).</p>
 */

// Caching explenation
// -------------------
// This calculator is set to cache the instances because during the initialization
// it calculates the flux value. Performance tests showed that cashing the instances
// was faster than this calculation (around 2 times), even for a great number of
// calculators.
@Cacheable(CachingPolicy.INSTANCE)
@EqualsAndHashCode(callSuper = false, exclude={"flux"})
public class Continuum extends AbstractCalculator<Pair<Double, Double>, Unit<Double>, Unit<Double>> {

    
    private double magnitude;
    private double magnitudeWavelength;
    private double flux;

    /**
     * Validates the given configuration. It requires that the second
     * value (the filter band) is not null.
     * @param configuration The configuration to validate
     * @throws ConfigurationException If the configuration is invalid
     */
    @Override
    protected void validateConfiguration(Pair<Double, Double> configuration) throws ConfigurationException {
        if (configuration.getValue1() == null) {
            throw new ConfigurationException("Filter band of source magnitude cannot be null");
        }
    }
    
    /**
     * Initializes the calculator instance with the given configuration. The first
     * value of the configuration is the AB magnitude and the second is
     * the filter band.
     * @param configuration The configuration to use for initialization
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(Pair<Double, Double> configuration) throws InitializationException {
        magnitude = configuration.getValue0();
        magnitudeWavelength = configuration.getValue1();
        flux = FluxUtil.convertMagnitudeToFlux(magnitude, magnitudeWavelength);
    }
    
    /**
     * Returns a {@link org.javatuples.Unit} containing the signal flux,
     * expressed in {@latex.inline $erg/s/cm^2/$\\AA}.
     * @param input The wavelength expressed in {@latex.inline \\AA}
     * @return The signal flux expressed in {@latex.inline $erg/s/cm^2/$\\AA}
     * @throws CalculationException if there is any error during the calculation
     */
    @Override
    protected Unit<Double> performCalculation(Unit<Double> input) throws CalculationException {
        return new Unit<Double>(flux);
    }
    
    /**
     * Overridden to add in the results the signal flux This is done here, so
     * we have this results even if the result is retrieved from the cache.
     * @param input The wavelength expressed in {@latex.inline \\AA}
     * @param output The signal flux expressed in {@latex.inline $erg/s/cm^2/$\\AA}
     */
    @Override
    protected void performForEveryCalculation(Unit<Double> input, Unit<Double> output) {
        ResultsHolder.getResults().addResult("SIGNAL_FLUX", input.getValue0(), output.getValue0(),
                Units.ANGSTROM, Units.getErgPerCm2PerSecPerAngstrom(), Level.INTERMEDIATE_UNIMPORTANT);
    }
    
    /**
     * Overridden to add in the results the AB magnitude and the filter.
     * This is done here, so we have these results even if the calculator is retrieved
     * from the cache.
     * @param configuration The configuration of the calculator
     */
    @Override
    protected void performForEveryRetrieval(Pair<Double, Double> configuration) {
        ResultsHolder.getResults().addResult(new DoubleValueResult("SOURCE_AB_MAGNITUDE"
                , configuration.getValue0(), null), Level.DEBUG);
        ResultsHolder.getResults().addResult(new StringResult("SOURCE_AB_MAGNITUDE_FILTER"
                , configuration.getValue1().toString()), Level.DEBUG);
    }
    
    
}
