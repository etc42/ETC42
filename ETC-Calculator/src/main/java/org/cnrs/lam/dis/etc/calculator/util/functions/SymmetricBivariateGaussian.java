/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.util.functions;

import lombok.EqualsAndHashCode;
import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.analysis.UnivariateRealFunction;

/**
 * <p>The {@link SymmetricBivariateGaussian} class represents a scaled bivariate normal
 * distribution with peak value at the point (0,0), where the variables X and Y
 * are not correlated and both have the same standard deviation {@latex.inline $\\sigma$}.
 * Its values are given by the equation:</p>
 * {@latex.inline $
 * f{(r)}=\\displaystyle\\frac{scale}{2\\pi\\sigma^2}
 * e^{-\\frac{1}{2} \\left( \\frac{r}{\\sigma}} \\right) ^2
 * $}
 */
@EqualsAndHashCode(callSuper=false, of={"standardDeviation", "scale"})
public class SymmetricBivariateGaussian implements CircularlySymmetricBivariateFunction {
    
    private final double standardDeviation; //sigma
    private final double scale; // The scale factor
    private final double variance; // sigma^2
    private final double multiplier; // scale/(2*pi*sigma^2)
    
    /**
     * Creates a new {@link SymmetricBivariateGaussian} which has a peak at X = 0, the given
     * standard deviation {@latex.inline $\\sigma$} and scale factor = 1.
     * @param standardDeviation The standard deviation {@latex.inline $\\sigma$}
     */
    public SymmetricBivariateGaussian(double standardDeviation) {
        this(standardDeviation, 1.);
    }

    /**
     * Creates a new {@link SymmetricBivariateGaussian} which has a peak at X = 0 and the given
     * standard deviation {@latex.inline $\\sigma$} and scale factor.
     * @param standardDeviation The standard deviation {@latex.inline $\\sigma$}
     * @param scale The scale factor to multiply the Gaussian with
     */
    public SymmetricBivariateGaussian(double standardDeviation, double scale) {
        this.standardDeviation = standardDeviation;
        this.scale = scale;
        this.variance = standardDeviation * standardDeviation;
        this.multiplier = scale / (2 * Math.PI * variance);
    }
    
    /**
     * Returns the standard deviation {@latex.inline $\\sigma$} of the gaussian
     * both axes.
     * @return The standard deviation
     */
    public double getStandardDeviation() {
        return standardDeviation;
    }
    
    /**
     * Returns the scale factor with which the Gaussian is multiplied with.
     * @return 
     */
    public double getScale() {
        return scale;
    }

    /**
     * Returns the value of the Bivariate Gaussian for the distance r from the
     * location of the peak.
     * @param r The distance from the peak
     * @return The value of the Gaussian
     * @throws FunctionEvaluationException 
     */
    @Override
    public double value(double r) throws FunctionEvaluationException {
        double rSquare = r * r;
        return valueFromSquare(rSquare);
    }
    
    /**
     * This method calculates the value of the bivariate gaussian from the square
     * of the distance to the center. Its purpose is to avoid the calculation of
     * a square root on the value(x,y) method.
     * @param rSquare The square o the distance to the center
     * @return The value of the bivariate gaussian
     * @throws FunctionEvaluationException 
     */
    private double valueFromSquare(double rSquare) {
        double value = multiplier * Math.exp(-0.5 * rSquare / variance);
        return value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UnivariateRealFunction projectionFunction() {
        // The projection of the symmetric bivariate gaussian is a univariate gaussian
        // with the same sigma but scaled with the factor 1/(sigma*sqrt(2*pi))
        double factor = scale / (standardDeviation * Math.sqrt(2 * Math.PI));
        return new Gaussian(standardDeviation, factor);
    }

    /**
     * Returns the value of the Bivariate Gaussian for the point (x,y) from the
     * location of the peak.
     * @param x The distance from the peak on the X axis
     * @param y The distance from the peak on the Y axis
     * @return The value of the Gaussian
     * @throws FunctionEvaluationException 
     */
    @Override
    public double value(double x, double y) throws FunctionEvaluationException {
        double rSquare = x * x + y * y;
        return valueFromSquare(rSquare);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UnivariateRealFunction polarFunction() {
        // The projection of the symmetric bivariate gaussian is a univariate gaussian
        // with the same sigma but scaled with the factor 1/(sigma*sqrt(2*pi))
        double factor = scale / (standardDeviation * Math.sqrt(2 * Math.PI));
        return new PolarGaussian(standardDeviation, factor);
    }
    
}
