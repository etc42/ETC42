/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.oldspectralbinning;

import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.oldspatialbinning.SpatialBinningFactory;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.StringResult;
import org.cnrs.lam.dis.etc.datamodel.Instrument;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.javatuples.Quartet;
import org.javatuples.Unit;

/**
 * <p>The {@code SpectralBinningFactory} provides calculators for calculating the
 * number of pixels of the detector affected from the same wavelength, in the
 * spectral direction. The retrieved calculators get as input the wavelength for
 * which the calculation is done (expressed in {@latex.inline \\AA}) and returns
 * the spectral binning expressed in {@latex.inline $pixels$}. The configuration
 * of the factory is a {@link org.cnrs.lam.dis.etc.datamodel.Session} object 
 * containing all the configuration from the user. Note that the spectral binning
 * calculation makes sense only for spectroscopy.</p>
 */
public class SpectralBinningFactory implements Factory<Unit<Session>, Unit<Double>, Unit<Double>> {

    /**
     * Returns a calculator for computing the spectral binning, based on the given
     * configuration.
     * @param configuration The session object containing all the user configuration
     * @return The calculator instance
     * @throws InitializationException if there is any problem with initializing
     * the calculator
     * @throws ConfigurationException if the configuration is invalid
     */
    @Override
    public Calculator<Unit<Double>, Unit<Double>> getCalculator(Unit<Session> configuration)
            throws InitializationException, ConfigurationException {
        Session session = configuration.getValue0();
        // The spectral binning makes sense only for spectroscopy
        if (session.getInstrument().getInstrumentType() == Instrument.InstrumentType.SPECTROGRAPH) {
            double pixelScale = session.getInstrument().getPixelScale();
            String pixelScaleUnit = session.getInstrument().getPixelScaleUnit();
            // If we have a long slit spectrograph we return a Slit calculator
            if (session.getInstrument().getSpectrographType() == Instrument.SpectrographType.SLIT) {
                ResultsHolder.getResults().addResult(
                        new StringResult("SPECTRAL_BINNING_METHOD", "Slit"), Level.DEBUG);
                double slitWidth = session.getInstrument().getSlitWidth();
                String slitWidthUnit = session.getInstrument().getSlitWidthUnit();
                Quartet<Double, String, Double, String> calculatorConfiguration = new Quartet(
                        pixelScale, pixelScaleUnit, slitWidth, slitWidthUnit);
                return EtcCalculatorManager.getManager(Slit.class).getCalculator(calculatorConfiguration);
            }
            // If we have a fiber spectrograph we return a Fiber calculator
            if (session.getInstrument().getSpectrographType() == Instrument.SpectrographType.FIBER) {
                ResultsHolder.getResults().addResult(
                        new StringResult("SPECTRAL_BINNING_METHOD", "Fiber"), Level.DEBUG);
                double fiberDiameter = session.getInstrument().getFiberDiameter();
                String fiberDiameterUnit = session.getInstrument().getFiberDiameterUnit();
                Quartet<Double, String, Double, String> calculatorConfiguration = new Quartet(
                        pixelScale, pixelScaleUnit, fiberDiameter, fiberDiameterUnit);
                return EtcCalculatorManager.getManager(Fiber.class).getCalculator(calculatorConfiguration);
            }
            // If we have a slitless spectrograph we return a Slitless calculator
            if (session.getInstrument().getSpectrographType() == Instrument.SpectrographType.SLITLESS) {
                ResultsHolder.getResults().addResult(
                        new StringResult("SPECTRAL_BINNING_METHOD", "Slitless"), Level.DEBUG);
                Calculator<Unit<Double>, Unit<Double>> spatialBinningCalculator
                        = new SpatialBinningFactory().getCalculator(configuration);
                Unit<Calculator<Unit<Double>, Unit<Double>>> calculatorConfiguration 
                        = new Unit(spatialBinningCalculator);
                return EtcCalculatorManager.getManager(Slitless.class).getCalculator(calculatorConfiguration);
            }
        }
        // If we are here means that we cannot create a calculator for this configuration
        throw new UnsupportedOperationException("Spectral binning calculation is "
                + "not supported for the given configuration");
    }
}
