/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.oldflux;

import lombok.EqualsAndHashCode;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.Cacheable;
import org.cnrs.lam.cesam.util.calculator.CachingPolicy;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.Calculator;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.DatasetProviderHolder;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.FluxUtil;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.DoubleValueResult;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.StringResult;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.cnrs.lam.dis.etc.datamodel.FilterBand;
import org.javatuples.Quintet;
import org.javatuples.Unit;

/**
 */

// Caching explenation
// -------------------
// This calculator is set to cache the instances because the configuration validation
// is retrieving the dataset, which is time consuming.
@Cacheable(CachingPolicy.INSTANCE)
@EqualsAndHashCode(callSuper=false, exclude={"normalizationFactor"})
public class RestFrameTemplate extends AbstractCalculator<Quintet<Double, Double, Double, DatasetInfo, Calculator<Unit<Double>, Unit<Double>>>, Unit<Double>, Unit<Double>> {
    
    private double magnitude;
    private double redshift;
    private double magnitudeWavelength;
    private Calculator<Unit<Double>, Unit<Double>> spectrumCalculator;
    private double normalizationFactor;

    @Override
    protected void validateConfiguration(Quintet<Double, Double, Double, DatasetInfo, Calculator<Unit<Double>, Unit<Double>>> configuration) throws ConfigurationException {
        if (configuration.getValue1() < 0) {
            throw new ConfigurationException("Redshift can not be a negative number " +
                    "but was " + configuration.getValue1());
        }
        if (configuration.getValue2() == null) {
            throw new ConfigurationException("Filter band of source magnitude cannot be null");
        }
        DatasetInfo info = configuration.getValue3();
        if (info == null) {
            throw new ConfigurationException("Source does not have restframe template");
        }
        Dataset dataset = DatasetProviderHolder.getDatasetProvider().getDataset(Dataset.Type.SPECTRAL_DIST_TEMPLATE, info);
        if (dataset == null) {
            throw new ConfigurationException("Data for source restframe template "
                    + configuration.getValue1() + " is not available");
        }
        if (!Units.isAngstrom(dataset.getXUnit())) {
            throw new ConfigurationException("Unit of X axis of source restframe template must be "
                    + Units.ANGSTROM + " but was " + dataset.getXUnit());
        }
    }

    @Override
    protected void initialize(Quintet<Double, Double, Double, DatasetInfo, Calculator<Unit<Double>, Unit<Double>>> configuration) throws InitializationException {
        magnitude = configuration.getValue0();
        redshift = configuration.getValue1();
        magnitudeWavelength = configuration.getValue2();
        spectrumCalculator = configuration.getValue4();
        // Calculate the normalization factor
        double abFlux = FluxUtil.convertMagnitudeToFlux(magnitude, magnitudeWavelength);
        double redshiftedLambda = magnitudeWavelength / (1 + redshift);
        double templateFlux = 0;
        try {
            templateFlux = Double.parseDouble(spectrumCalculator.calculate(new Unit(redshiftedLambda)).getValue0().toString());
        } catch (CalculationException e) {
            throw new InitializationException(e.getMessage(), e);
        }
        normalizationFactor = abFlux / templateFlux;
    }

    @Override
    protected Unit<Double> performCalculation(Unit<Double> input) throws CalculationException {
        double redshiftedLambda = input.getValue0() / (1 + redshift);
        double templateFlux = Double.parseDouble(spectrumCalculator.calculate(new Unit(redshiftedLambda)).getValue0().toString());
        double result = templateFlux * normalizationFactor;
        return new Unit<Double>(result);
    }
    
    /**
     * Overridden to add in the results the signal flux This is done here, so
     * we have this results even if the result is retrieved from the cache.
     * @param input The wavelength expressed in {@latex.inline \\AA}
     * @param output The signal flux expressed in {@latex.inline $erg/s/cm^2/$\\AA}
     */
    @Override
    protected void performForEveryCalculation(Unit<Double> input, Unit<Double> output) {
        ResultsHolder.getResults().addResult("SIGNAL_FLUX", input.getValue0(), output.getValue0(),
                Units.ANGSTROM, Units.getErgPerCm2PerSecPerAngstrom(), Level.INTERMEDIATE_UNIMPORTANT);
    }

    @Override
    protected void performForEveryRetrieval(Quintet<Double, Double, Double, DatasetInfo, Calculator<Unit<Double>, Unit<Double>>> configuration) {
        ResultsHolder.getResults().addResult(new DoubleValueResult("SOURCE_AB_MAGNITUDE"
                , configuration.getValue0(), null), Level.DEBUG);
        ResultsHolder.getResults().addResult(new DoubleValueResult("REDSHIFT"
                , configuration.getValue1(), null), Level.DEBUG);
        ResultsHolder.getResults().addResult(new StringResult("SOURCE_AB_MAGNITUDE_FILTER"
                , configuration.getValue2().toString()), Level.DEBUG);
        ResultsHolder.getResults().addResult(new StringResult("SOURCE_RESTFRAME_TEMPLATE"
                , configuration.getValue3().toString()), Level.DEBUG);
    }
    
}
