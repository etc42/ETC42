/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.oldlambdarange;

import lombok.EqualsAndHashCode;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.DoubleValueResult;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.javatuples.Pair;
import org.javatuples.Quartet;
import org.javatuples.Tuple;

/**
 */
@EqualsAndHashCode(callSuper=false)
public class Spectroscopy extends AbstractCalculator<Quartet<Double, String, Double, String>, Tuple, Pair<Double, Double>> {
    
    private double minLambda;
    private double maxLambda;

    @Override
    protected void validateConfiguration(Quartet<Double, String, Double, String> configuration) throws ConfigurationException {
        if (configuration.getValue0() < 0) {
            throw new ConfigurationException("Range minimum must be positive "
                    + "but was " + configuration.getValue0());
        }
        if (!Units.isAngstrom(configuration.getValue1())) {
             throw new ConfigurationException("Range minimum must be in " + Units.ANGSTROM
                    + " but was in " + configuration.getValue1());
        }
        if (configuration.getValue2() < 0) {
            throw new ConfigurationException("Range maximum must be positive "
                    + "but was " + configuration.getValue2());
        }
        if (!Units.isAngstrom(configuration.getValue3())) {
             throw new ConfigurationException("Range maximum must be in " + Units.ANGSTROM
                    + " but was in " + configuration.getValue3());
        }
        if (configuration.getValue0() > configuration.getValue2()) {
            throw new ConfigurationException("Range minimum must be smaller than "
                    + "range maximum");
        }
    }

    @Override
    protected void initialize(Quartet<Double, String, Double, String> configuration) throws InitializationException {
        minLambda = configuration.getValue0();
        maxLambda = configuration.getValue2();
    }

    @Override
    protected Pair<Double, Double> performCalculation(Tuple input) throws CalculationException {
        return new Pair<Double, Double>(minLambda, maxLambda);
    }
    
    @Override
    protected void performForEveryCalculation(Tuple input, Pair<Double, Double> output) {
        ResultsHolder.getResults().addResult(new DoubleValueResult("WAVELENGTH_MIN", 
                output.getValue0(), Units.ANGSTROM), Level.INTERMEDIATE_UNIMPORTANT);
        ResultsHolder.getResults().addResult(new DoubleValueResult("WAVELENGTH_MAX", 
                output.getValue1(), Units.ANGSTROM), Level.INTERMEDIATE_UNIMPORTANT);
    }

    @Override
    protected void performForEveryRetrieval(Quartet<Double, String, Double, String> configuration) {
        ResultsHolder.getResults().addResult(new DoubleValueResult("RANGE_MIN", 
                configuration.getValue0(), configuration.getValue1()), Level.DEBUG);
        ResultsHolder.getResults().addResult(new DoubleValueResult("RANGE_MAX", 
                configuration.getValue2(), configuration.getValue3()), Level.DEBUG);
    }
}
