/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.flux;

import java.text.MessageFormat;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TreeMap;
import lombok.EqualsAndHashCode;
import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.apache.log4j.Logger;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.FunctionToDatasetResultConverter;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.calculator.util.functions.Gaussian;
import org.cnrs.lam.dis.etc.calculator.util.functions.IntegrableUnivariateFunction;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.javatuples.Pair;
import org.javatuples.Quartet;
import org.javatuples.Tuple;
import org.javatuples.Unit;

/**
 * <p>The {@link EmissionLine} class is a calculator for computing functions
 * representing the flux of an emission line. The emission line is calculated as:</p>
 * {@latex.inline $
 * F_{(\\lambda)}=F_tG_{(\\lambda+\\lambda_0;\\sigma)}
 * $}
 * <p>where</p>
 * <ul>
 *   <li>{@latex.inline $F_t$}: is the total flux of the emission line</li>
 *   <li>{@latex.inline $G$}: is the Gaussian function</li>
 *   <li>{@latex.inline $\\lambda$}: is the wavelength expressed 
 *       in {@latex.inline \\AA}</li>
 *   <li>{@latex.inline $\\lambda_0=\\lambda_{em}(1+z)$}</li>
 *   <li>{@latex.inline $\\lambda_{em}$}: is the wavelength of the emission line
 *       at rest frame</li>
 *   <li>{@latex.inline $z$}: is the redshift</li>
 *   <li>{@latex.inline $\sigma=FWHM_{em}(1+z)/2.3548$}</li>
 *   <li>{@latex.inline $FWHM_{em}$}: is the FWHM of the emission line at rest frame</li>
 * </ul>
 * 
 * <p>The configuration of the calculator is a {@link Quartet},
 * witch contains the following elements:</p>
 * <ol>
 *   <li>{@link Pair}: The total flux of the emission line and its unit</li>
 *   <li>{@link Pair}: The wavelength of the emission line at rest frame and its unit</li>
 *   <li>{@link Pair}: The FWHM of the emission line at rest frame and its unit</li>
 *   <li>{@link Double}: The redshift</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Tuple}, as this
 * calculator does not accept any input. It should always be null.</p>
 * 
 * <p>The output of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link UnivariateRealFunction} representing the flux of the source.</p>
 */
// Caching explenation
// -------------------
// This calculator is set to cache the instances because during instantiation
// it creates the flux function.
@Cacheable(CachingPolicy.INSTANCE)
@EqualsAndHashCode(callSuper=false, exclude={"fluxFunction"})
public class EmissionLine extends AbstractCalculator<
        Quartet<Pair<Double, String>, Pair<Double, String>,
        Pair<Double, String>, Double>, Tuple, Unit<UnivariateRealFunction>> {
    
    private static final Logger logger = Logger.getLogger(FlatPhotonFlux.class);
    private static final ResourceBundle validationErrorsBundle =
            ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");
    private double fluxEm;
    private double lambdaEm;
    private double fwhmEm;
    private double redshift;
    private EmissionLineFunction fluxFunction;

    /**
     * It validates the configuration. It requires that the total flux is a positive
     * number, that its unit is erg/cm2/s, that the wavelength is a positive number,
     * that its unit is Angstrom, that the FWHM is a positive number, that its unit
     * is Angstrom and that the redshift is non negative.
     * @param configuration
     * @throws ConfigurationException 
     */
    @Override
    protected void validateConfiguration(Quartet<
            Pair<Double, String>, Pair<Double, String>,
            Pair<Double, String>, Double> configuration) throws ConfigurationException {
        // Check that the total flux is a positive number
        if (configuration.getValue0().getValue0() <= 0) {
            String message = validationErrorsBundle.getString("EMISSION_LINE_TOTAL_FLUX_NOT_POSITIVE");
            message = MessageFormat.format(message, configuration.getValue0().getValue0());
            throw new ConfigurationException(message);
        }
        // Check if the total flux is in erg/cm2/s
        if (!Units.isErgPerCm2PerSec(configuration.getValue0().getValue1())) {
            String message = validationErrorsBundle.getString("EMISSION_LINE_TOTAL_FLUX_WRONG_UNIT");
            message = MessageFormat.format(message, Units.getErgPerCm2PerSec(), configuration.getValue0().getValue1());
            throw new ConfigurationException(message);
        }
        // Check that the wavelength is a positive number
        if (configuration.getValue1().getValue0() <= 0) {
            String message = validationErrorsBundle.getString("EMISSION_LINE_WAVELENGTH_NOT_POSITIVE");
            message = MessageFormat.format(message, configuration.getValue1().getValue0());
            throw new ConfigurationException(message);
        }
        // Check if the wavelength is in Angstrom
        if (!Units.isAngstrom(configuration.getValue1().getValue1())) {
            String message = validationErrorsBundle.getString("EMISSION_LINE_WAVELENGTH_WRONG_UNIT");
            message = MessageFormat.format(message, Units.ANGSTROM, configuration.getValue1().getValue1());
            throw new ConfigurationException(message);
        }
        // Check that the FWHM is a positive number
        if (configuration.getValue2().getValue0() <= 0) {
            String message = validationErrorsBundle.getString("EMISSION_LINE_FWHM_NOT_POSITIVE");
            message = MessageFormat.format(message, configuration.getValue2().getValue0());
            throw new ConfigurationException(message);
        }
        // Check if the FWHM is in Angstrom
        if (!Units.isAngstrom(configuration.getValue2().getValue1())) {
            String message = validationErrorsBundle.getString("EMISSION_LINE_FWHM_WRONG_UNIT");
            message = MessageFormat.format(message, Units.ANGSTROM, configuration.getValue2().getValue1());
            throw new ConfigurationException(message);
        }
        // Check that the redshift is a non negative number
        if (configuration.getValue3() < 0) {
            String message = validationErrorsBundle.getString("REDSHIFT_NEGATIVE");
            message = MessageFormat.format(message, configuration.getValue3());
            throw new ConfigurationException(message);
        }
    }

    /**
     * Initializes the calculator instance with the given configuration.
     * @param configuration the configuration to use for initialization
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(Quartet<Pair<Double, String>, Pair<Double, String>, Pair<Double, String>, Double> configuration) throws InitializationException {
        fluxEm = configuration.getValue0().getValue0();
        lambdaEm = configuration.getValue1().getValue0();
        fwhmEm = configuration.getValue2().getValue0();
        redshift = configuration.getValue3();
        // We calculate the characteristics of the gaussian
        double redshiftedLambda = lambdaEm * (1 + redshift);
        double redshiftedFwhm = fwhmEm * (1 + redshift);
        fluxFunction = new EmissionLineFunction(fluxEm, redshiftedFwhm, redshiftedLambda);
    }

    /**
     * Returns a {@link UnivariateRealFunction} which can be used to calculate
     * the source flux.
     * @param input The calculator has no input, so it can be always null
     * @return A {@link UnivariateRealFunction} which can be used to calculate
     * the source flux
     * @throws CalculationException if there is any error during the calculation
     */
    @Override
    protected Unit<UnivariateRealFunction> performCalculation(Tuple input) throws CalculationException {
        return new Unit<UnivariateRealFunction>(fluxFunction);
    }

    /**
     * <p>For every calculation the calculator adds in the simulation results
     * the following outputs of the calculator:</p>
     * <ul>
     *   <li>The source flux in intermediate unimportant results with code
     *       SIGNAL_FLUX</li>
     * </ul>
     * @param input
     * @param output 
     */
    @Override
    protected void performForEveryCalculation(Tuple input, Unit<UnivariateRealFunction> output) {
        // If the result already exists we do not add it to save time
        if (ResultsHolder.getResults().getResultByName("SIGNAL_FLUX") != null) {
            return;
        }
        UnivariateRealFunction function = output.getValue0();
        CalculationResults.DoubleDatasetResult result = FunctionToDatasetResultConverter.convert(function,
                "SIGNAL_FLUX", Units.ANGSTROM, Units.getErgPerCm2PerSecPerAngstrom());
        ResultsHolder.getResults().addResult(result, CalculationResults.Level.INTERMEDIATE_UNIMPORTANT);
    }

    /**
     * <p>For every calculator instantiation the following parts of its configuration
     * are added in the simulation results:</p>
     * <ul>
     *   <li>The total flux of the emission line in debug results with code
     *       EMISSION_LINE_FLUX</li>
     *   <li>The wavelength of the emission line in debug results with code
     *       EMISSION_LINE_FLUX</li>
     *   <li>The FWHM of the emission line in debug results with code
     *       EMISSION_LINE_FWHM</li>
     *   <li>The redshift of the emission line in debug results with code
     *       REDSHIFT</li>
     * </ul>
     * @param configuration 
     */
    @Override
    protected void performForEveryRetrieval(Quartet<Pair<Double, String>, Pair<Double, String>, Pair<Double, String>, Double> configuration) {
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("EMISSION_LINE_FLUX"
                , configuration.getValue0().getValue0(), configuration.getValue0().getValue1()), CalculationResults.Level.DEBUG);
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("EMISSION_LINE_WAVELENGTH"
                , configuration.getValue1().getValue0(), configuration.getValue1().getValue1()), CalculationResults.Level.DEBUG);
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("EMISSION_LINE_FWHM"
                , configuration.getValue2().getValue0(), configuration.getValue2().getValue1()), CalculationResults.Level.DEBUG);
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("REDSHIFT"
        , configuration.getValue3(), null), CalculationResults.Level.DEBUG);
    }
    
    /**
     * Represents an emission line function.
     */
    @EqualsAndHashCode(callSuper=false)
    private static class EmissionLineFunction implements IntegrableUnivariateFunction {
        
        private Gaussian gaussian;
        private double wavelength;

        public EmissionLineFunction(double totalFlux, double fwhm, double wavelength) {
            double sigma = fwhm / 2.3548;
            this.wavelength = wavelength;
            gaussian = new Gaussian(sigma, totalFlux);
        }

        @Override
        public double value(double x) throws FunctionEvaluationException {
            return gaussian.value(wavelength - x);
        }

        @Override
        public double integral(double x1, double x2) throws FunctionEvaluationException {
            return gaussian.integral(wavelength - x2, wavelength - x1);
        }
        
    }
    
}
