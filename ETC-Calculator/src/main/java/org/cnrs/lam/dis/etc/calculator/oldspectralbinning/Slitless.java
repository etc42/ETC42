/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.oldspectralbinning;

import lombok.EqualsAndHashCode;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.Calculator;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.DoubleValueResult;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.javatuples.Unit;

/**
 * <p>The {@code Slitless} class is a calculator for computing the spectral binning,
 * in the case the instrument is a slitless spectrograph. In this case the spectral
 * binning is calculated as the same with the spatial binning.</p>
 * 
 * <p>The configuration of the calculator is a {@link org.javatuples.Unit},
 * with contains the following elements:</p>
 * <ol>
 *   <li>{@link org.cnrs.lam.cesam.util.calculator.Calculator}: a calculator for
 *       calculating the spatial binning</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the wavelength for which the calculation
 * is done (expressed in {@latex.inline \\AA}). Note that the calculation is not
 * related with the wavelength and its value is ignored.</p>
 * 
 * <p>The output of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the Spectral Binning (expressed in
 * {@latex.inline $pixel$}).</p>
 */
@EqualsAndHashCode(callSuper = false)
public class Slitless extends AbstractCalculator<Unit<Calculator<Unit<Double>, Unit<Double>>>, Unit<Double>, Unit<Double>> {
    
    private Calculator<Unit<Double>, Unit<Double>> spatialBinningCalculator;

    @Override
    protected void validateConfiguration(Unit<Calculator<Unit<Double>, Unit<Double>>> configuration) throws ConfigurationException {
    }

    @Override
    protected void initialize(Unit<Calculator<Unit<Double>, Unit<Double>>> configuration) throws InitializationException {
        spatialBinningCalculator = configuration.getValue0();
    }

    @Override
    protected Unit<Double> performCalculation(Unit<Double> input) throws CalculationException {
        return spatialBinningCalculator.calculate(input);
    }

    /**
     * Overridden to add in the results the spectral binning.
     * @param input The wavelength expressed in {@latex.inline \\AA}
     * @param output The spectral binning expressed in {@latex.inline $arcsec$}
     */
    @Override
    protected void performForEveryCalculation(Unit<Double> input, Unit<Double> output) {
        ResultsHolder.getResults().addResult(new DoubleValueResult("SPECTRAL_BINNING",
                output.getValue0(), Units.PIXEL), Level.INTERMEDIATE_IMPORTANT);
    }
    
}
