/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.backgroundflux;

import java.util.ArrayList;
import java.util.List;
import lombok.EqualsAndHashCode;
import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.apache.log4j.Logger;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.Cacheable;
import org.cnrs.lam.cesam.util.calculator.CachingPolicy;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.Calculator;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.FunctionToDatasetResultConverter;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.calculator.util.functions.BoundedUnivariateFunction;
import org.cnrs.lam.dis.etc.calculator.util.functions.IntegrableUnivariateFunction;
import org.cnrs.lam.dis.etc.calculator.util.integration.IntegrationTool;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.javatuples.Pair;
import org.javatuples.Triplet;
import org.javatuples.Tuple;
import org.javatuples.Unit;

/**
 * <p>The {@link Space} class is a calculator for computing the total background flux
 * for space telescopes. For this case the flux is equal with the summary of the
 * sky, zodiacal and galactic fluxes. The  configuration of the calculator
 * is a {@link Triplet} which contains the following elements:</p>
 * <ol>
 *   <li>{@link Calculator}: a calculator for performing sky flux computations</li>
 *   <li>{@link Calculator}: a calculator for performing zodiacal flux computations</li>
 *   <li>{@link Calculator}: a calculator for performing galactic flux computations</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Tuple}, as this
 * calculator does not accept any input. It should always be null.</p>
 * 
 * <p>The output of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link UnivariateRealFunction} representing the total background flux.</p>
 */
@Cacheable(CachingPolicy.INSTANCE)
@EqualsAndHashCode(callSuper=false)
public class Space extends AbstractCalculator<
        Triplet<Calculator<Tuple, Unit<UnivariateRealFunction>>,
                Calculator<Tuple, Unit<UnivariateRealFunction>>,
                Calculator<Tuple, Unit<UnivariateRealFunction>>>,
        Tuple, Unit<UnivariateRealFunction>> {
    
    private static final Logger logger = Logger.getLogger(Space.class);
    private Calculator<Tuple, Unit<UnivariateRealFunction>> skyFlux;
    private Calculator<Tuple, Unit<UnivariateRealFunction>> zodiacalFlux;
    private Calculator<Tuple, Unit<UnivariateRealFunction>> galacticFlux;
    
    /**
     * Does nothing.
     * @param configuration
     * @throws ConfigurationException 
     */
    @Override
    protected void validateConfiguration(
            Triplet<Calculator<Tuple, Unit<UnivariateRealFunction>>,
                    Calculator<Tuple, Unit<UnivariateRealFunction>>, 
                    Calculator<Tuple, Unit<UnivariateRealFunction>>>
                    configuration) throws ConfigurationException {
    }

    /**
     * Initializes the calculator instance with the given configuration.
     * @param configuration the configuration to use for initialization
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(
            Triplet<Calculator<Tuple, Unit<UnivariateRealFunction>>, 
                    Calculator<Tuple, Unit<UnivariateRealFunction>>, 
                    Calculator<Tuple, Unit<UnivariateRealFunction>>> 
                    configuration) throws InitializationException {
        skyFlux = configuration.getValue0();
        zodiacalFlux = configuration.getValue1();
        galacticFlux = configuration.getValue2();
    }

    /**
     * Returns a {@link UnivariateRealFunction} which can be used to calculate
     * the total background flux.
     * @param input The calculator has no input, so it can be always null
     * @return A {@link UnivariateRealFunction} which can be used to calculate
     * the total background flux
     * @throws CalculationException if there is any error during the calculation
     */
    @Override
    protected Unit<UnivariateRealFunction> performCalculation(Tuple input) throws CalculationException {
        UnivariateRealFunction skyFunction = skyFlux.calculate(input).getValue0();
        UnivariateRealFunction zodiacalFunction = zodiacalFlux.calculate(input).getValue0();
        UnivariateRealFunction galacticFunction = galacticFlux.calculate(input).getValue0();
        return new Unit<UnivariateRealFunction>(new SumFunction(skyFunction, zodiacalFunction, galacticFunction));
    }
    
    private static class SumFunction implements UnivariateRealFunction,
            BoundedUnivariateFunction, IntegrableUnivariateFunction {
        
        private List<UnivariateRealFunction> functionList = new ArrayList<UnivariateRealFunction>();
        private double min = Double.NEGATIVE_INFINITY;
        private double max = Double.POSITIVE_INFINITY;
        private double minZero = Double.NEGATIVE_INFINITY;
        private double maxZero = Double.POSITIVE_INFINITY;

        public SumFunction(UnivariateRealFunction ... functions) {
            for (UnivariateRealFunction function : functions) {
                functionList.add(function);
                if (function instanceof BoundedUnivariateFunction) {
                    Pair<Double, Double> bounds = ((BoundedUnivariateFunction) function).getBounds();
                    min = Math.max(min, bounds.getValue0());
                    max = Math.min(max, bounds.getValue1());
                }
            }
        }

        @Override
        public double value(double x) throws FunctionEvaluationException {
            double value = 0;
            for (UnivariateRealFunction function : functionList) {
                value += function.value(x);
            }
            return value;
        }

        @Override
        public Pair<Double, Double> getBounds() {
            return new Pair<Double, Double>(min, max);
        }

        @Override
        public Pair<Double, Double> getNonZeroBounds() {
            return new Pair<Double, Double>(minZero, maxZero);
        }

        @Override
        public double integral(double x1, double x2) throws FunctionEvaluationException {
            double integral = 0;
            for (UnivariateRealFunction function : functionList) {
                integral += IntegrationTool.univariateIntegral(function, x1, x2);
            }
            return integral;
        }
    }

    /**
     * <p>For every calculation the calculator adds in the simulation results
     * the following outputs of the calculator:</p>
     * <ul>
     *   <li>The total background flux in intermediate unimportant results with code
     *       BACKGROUND_FLUX</li>
     * </ul>
     * @param input
     * @param output 
     */
    @Override
    protected void performForEveryCalculation(Tuple input, Unit<UnivariateRealFunction> output) {
        // If the result already exists we do not add it to save time
        if (ResultsHolder.getResults().getResultByName("BACKGROUND_FLUX") != null) {
            return;
        }
        UnivariateRealFunction function = output.getValue0();
        CalculationResults.DoubleDatasetResult result = FunctionToDatasetResultConverter.convert(function,
                "BACKGROUND_FLUX", Units.ANGSTROM, Units.getErgPerCm2PerSecPerAngstromPerArcsec2());
        ResultsHolder.getResults().addResult(result, CalculationResults.Level.INTERMEDIATE_UNIMPORTANT);
    }
    
}
