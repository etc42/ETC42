/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.oldsurfacebrightnessprofile;

import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.cnrs.lam.dis.etc.datamodel.Source;
import org.javatuples.Pair;
import org.javatuples.Unit;

/**
 * <p>The {@code SurfaceBrightnessProfileFactory} provides calculators for
 * calculating the surface brightness profile value for given distance from the
 * center of the source. The configuration of the factory is a
 * {@link org.cnrs.lam.dis.etc.datamodel.Session} object
 * containing all the configuration from the user.</p>
 */
public class SurfaceBrightnessProfileFactory implements Factory<Unit<Session>, Unit<Double>, Unit<Double>> {

    @Override
    public Calculator<Unit<Double>, Unit<Double>> getCalculator(Unit<Session> configuration) throws InitializationException, ConfigurationException {
        Session session = configuration.getValue0();
        Source source = session.getSource();
        if (source.getSpatialDistributionType() == Source.SpatialDistributionType.EXTENDED_SOURCE) {
            Pair<Double, String> calculatorConfiguration = new Pair(source.getExtendedSourceRadius(), source.getExtendedSourceRadiusUnit());
            switch (source.getExtendedSourceProfile()) {
                case DE_VAUCOULEURS:
                    return EtcCalculatorManager.getManager(DeVacouleurs.class).getCalculator(calculatorConfiguration);
                case EXPONENTIAL:
                    return EtcCalculatorManager.getManager(Exponential.class).getCalculator(calculatorConfiguration);
                case UNIFORM:
                    return EtcCalculatorManager.getManager(Uniform.class).getCalculator(calculatorConfiguration);
            }
        }
        throw new UnsupportedOperationException("Surface brightness profile calculation is "
                + "not supported for the given configuration");
    }
    
}
