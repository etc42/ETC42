/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.psfsize;

import java.text.MessageFormat;
import java.util.ResourceBundle;
import lombok.EqualsAndHashCode;
import org.apache.commons.math.FunctionEvaluationException;
import org.apache.log4j.Logger;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.Cacheable;
import org.cnrs.lam.cesam.util.calculator.CachingPolicy;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.DatasetProviderHolder;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.calculator.util.datasets.LinearFunctionDataset;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.javatuples.Unit;

/**
 * <p>The {@link SizeFunction} class is a calculator for computing the size
 * of the PSF by a dataset representing the PSF size function.</p>
 * 
 * <p>The configuration of the calculator is a {@link org.javatuples.Unit},
 * with contains the following elements:</p>
 * <ol>
 *   <li>{@link org.cnrs.lam.dis.etc.datamodel.DatasetInfo}: the information of the
 *       PSF size function dataset</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the wavelength for which the calculation
 * is done (expressed in {@latex.inline \\AA}).</p>
 * 
 * <p>The output of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link Double} representing the size of the PSF expressed in
 * {@latex.inline $arcsec$}.</p>
 */

// Caching explenation
// -------------------
// This calculator is set to cache the instances because the configuration validation
// and the instantiation are retrieving the dataset, which is time consuming.
@Cacheable(CachingPolicy.INSTANCE)
@EqualsAndHashCode(callSuper=false, exclude={"psfSizeFunction"})
public class SizeFunction extends AbstractCalculator<Unit<DatasetInfo>, Unit<Double>, Unit<Double>>{
    
    private static final Logger logger = Logger.getLogger(SizeFunction.class);
    private static final ResourceBundle validationErrorsBundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");
    
    // This is a variable where the PSF size function dataset info is stored.
    // This is used from lombok library for the equals and the hashCode methods.
    // We do not want to use the function we create, as this would be way too time
    // consuming.
    private DatasetInfo psfSizeDatasetInfo;
    // The dataset which represents the PSF size function
    private LinearFunctionDataset psfSizeFunction;

    /**
     * Validates the given configuration. It requires that the instrument has a
     * PSF size function, that the data for the function is available, that the 
     * unit of X axis is {@latex.inline \\AA} and the unit of Y axis is {@latex.inline $arcsec$}.
     * @param configuration the configuration to validate
     * @throws ConfigurationException if the configuration is invalid
     */
    @Override
    protected void validateConfiguration(Unit<DatasetInfo> configuration) throws ConfigurationException {
        DatasetInfo info = configuration.getValue0();
        if (info == null) {
            String message = validationErrorsBundle.getString("PSF_SIZE_FUNCTION_MISSING");
            throw new ConfigurationException(message);
        }
        Dataset dataset = DatasetProviderHolder.getDatasetProvider().getDataset(Dataset.Type.PSF_SIZE_FUNCTION, info);
        if (dataset == null) {
            String message = validationErrorsBundle.getString("PSF_SIZE_FUNCTION_NOT_AVAILABLE");
            message = MessageFormat.format(message, info);
            throw new ConfigurationException(message);
        }
        if (!Units.isAngstrom(dataset.getXUnit())) {
            String message = validationErrorsBundle.getString("PSF_SIZE_FUNCTION_WRONG_X_UNIT");
            message = MessageFormat.format(message, Units.ANGSTROM, dataset.getXUnit());
            throw new ConfigurationException(message);
        }
        if (!Units.isArcsec(dataset.getYUnit())) {
            String message = validationErrorsBundle.getString("PSF_SIZE_FUNCTION_WRONG_Y_UNIT");
            message = MessageFormat.format(message, Units.ARCSEC, dataset.getXUnit());
            throw new ConfigurationException(message);
        }
    }

    @Override
    protected void initialize(Unit<DatasetInfo> configuration) throws InitializationException {
        psfSizeDatasetInfo = configuration.getValue0();
        Dataset dataset = DatasetProviderHolder.getDatasetProvider().getDataset(Dataset.Type.PSF_SIZE_FUNCTION, psfSizeDatasetInfo);
        psfSizeFunction = new LinearFunctionDataset(dataset.getData());
    }

    @Override
    protected Unit<Double> performCalculation(Unit<Double> input) throws CalculationException {
        double lambda = input.getValue0();
        double psfSize = Double.NaN;
        try {
            psfSize = psfSizeFunction.value(lambda);
        } catch (FunctionEvaluationException ex) {
            logger.warn("Failed to calculate the FWHM of the PSF from the profile", ex);
        }
        return new Unit<Double>(psfSize);
    }

    /**
     * <p>For every calculation the calculator adds in the simulation results
     * the following outputs of the calculator:</p>
     * <ul>
     *   <li>The size of the PSF in intermediate unimportant results
     *       with code PSF_SIZE</li>
     * </ul>
     * @param input
     * @param output 
     */
    @Override
    protected void performForEveryCalculation(Unit<Double> input, Unit<Double> output) {
        ResultsHolder.getResults().addResult("PSF_SIZE", input.getValue0(), output.getValue0(), 
                Units.ANGSTROM, Units.ARCSEC, CalculationResults.Level.INTERMEDIATE_UNIMPORTANT);
    }

    /**
     * <p>For every calculator instantiation the following parts of its configuration
     * are added in the simulation results:</p>
     * <ul>
     *   <li>The name of the instruments PSF size function with code INSTRUMENT_PSF_SIZE_FUNCTION</li>
     * </ul>
     * @param configuration 
     */
    @Override
    protected void performForEveryRetrieval(Unit<DatasetInfo> configuration) {
        CalculationResults.StringResult result = new CalculationResults.StringResult(
                "INSTRUMENT_PSF_SIZE_FUNCTION", configuration.getValue0().toString());
        ResultsHolder.getResults().addResult(result, CalculationResults.Level.DEBUG);
    }

}
