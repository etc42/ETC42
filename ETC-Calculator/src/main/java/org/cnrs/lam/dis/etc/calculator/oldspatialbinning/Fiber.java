/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.oldspatialbinning;

import lombok.EqualsAndHashCode;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.DoubleValueResult;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.javatuples.Quartet;
import org.javatuples.Unit;

/**
 * <p>The {@code PointSource} class is a calculator for computing the spatial
 * binning, in the case of a fiber spectrograph (for point source and extended
 * source). The equation used for the calculation is:</p>
 * {@latex.inline $
 * SpatialBinning_{(\\lambda)} = \\frac{\\phi_{fiber}}{p}
 * $}
 * <p>where</p>
 * <ul>
 *   <li>{@latex.inline $SpatialBinning$}: is the spatial binning in 
 *       {@latex.inline $pixels$}</li>
 *   <li>{@latex.inline $\\lambda$}: is the wavelength expressed in angstrom 
 *       ({@latex.inline \\AA})</li>
 *   <li>{@latex.inline $p$}: is the pixel scale of the instrument expressed in
 *       {@latex.inline $arcsec/pixel$}</li>
 *   <li>{@latex.inline $\\phi_{fiber}$}: is the diameter of the fiber expressed in
 *       {@latex.inline $arcsec$}</li>
 * </ul>
 * 
 * <p>The configuration of the calculator is a {@link org.javatuples.Quartet},
 * with contains the following elements:</p>
 * <ol>
 *   <li>{@link java.lang.Double}: The pixel scale of the instrument</li>
 *   <li>{@link java.lang.String}: The unit of the pixel scale of the instrument</li>
 *   <li>{@link java.lang.Double}: The diameter of the fiber</li>
 *   <li>{@link java.lang.String}: The unit of the diameter of the fiber</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the wavelength expressed in {@latex.inline \\AA}.</p>
 * 
 * <p>The output of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the spatial binning, expressed in
 * {@latex.inline $pixels$}.</p>
 */
@EqualsAndHashCode(callSuper=false)
public class Fiber extends AbstractCalculator<Quartet<Double, String, Double, String>, Unit<Double>, Unit<Double>> {
    
    private double pixelScale;
    private double fiberDiameter;

    /**
     * Validates the given configuration. It requires that the first value of the
     * tuple (the pixel scale of the instrument) is a positive number, that the second
     * value (the unit of the pixel scale) is {@latex.inline $arcsec/pixel$}, that
     * the third value (the fiber diameter) is positive and that the fourth value
     * (the fiber diameter) is {@latex.inline $arcsec$}.
     * @param configuration The configuration to validate
     * @throws ConfigurationException If the configuration is invalid
     */
    @Override
    protected void validateConfiguration(Quartet<Double, String, Double, String> configuration) throws ConfigurationException {
        // Check that the pixel scale is a possitive number
        if (configuration.getValue0() <= 0) {
            throw new ConfigurationException("Pixel scale must be a possitive "
                    + "number but was " + configuration.getValue0());
        }
        // Check if the pixel scale is in arcsec/pixel
        if (!Units.getArcsecPerPixel().equals(configuration.getValue1())) {
            throw new ConfigurationException("Pixel scale must be in " + Units.ARCSEC + "/"
                    + Units.PIXEL + " but was in " + configuration.getValue1());
        }
        // Check that the fiber diameter is a possitive number
        if (configuration.getValue2() <= 0) {
            throw new ConfigurationException("Fiber diameter must be a possitive "
                    + "number but was " + configuration.getValue2());
        }
        // Check if the fiber diameter is in arcsec
        if (!Units.isArcsec(configuration.getValue3())) {
            throw new ConfigurationException("Fiber diameter must be in " + Units.ARCSEC
                    + " but was in " + configuration.getValue3());
        }
    }

    /**
     * Initializes the calculator instance with the given configuration. The first
     * value of the configuration is the pixel scale of the instrument, the second is
     * the unit of the pixel scale, the third is the fiber diameter and the fourth
     * is the unit of the fiber diameter.
     * @param configuration the configuration to use for initialization
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(Quartet<Double, String, Double, String> configuration) throws InitializationException {
        pixelScale = configuration.getValue0();
        fiberDiameter = configuration.getValue2();
    }

    /**
     * Returns a {@link org.javatuples.Unit} containing the Spatial Binning
     * expressed in {@latex.inline $pixels$}.
     * @param input The wavelength expressed in {@latex.inline \\AA}
     * @return The spatial binning expressed in {@latex.inline $pixels$}
     * @throws CalculationException if there is any error during the calculation
     */
    @Override
    protected Unit<Double> performCalculation(Unit<Double> input) throws CalculationException {
        double result = fiberDiameter / pixelScale;
        return new Unit<Double>(result);
    }

    /**
     * Overridden to add in the results the spatial binning.
     * @param input The wavelength expressed in {@latex.inline \\AA}
     * @param output The spatial binning expressed in {@latex.inline $pixels$}
     */
    @Override
    protected void performForEveryCalculation(Unit<Double> input, Unit<Double> output) {
        ResultsHolder.getResults().addResult("SPATIAL_BINNING", input.getValue0(),
                output.getValue0(), Units.ANGSTROM, Units.PIXEL, Level.INTERMEDIATE_IMPORTANT);
    }

    /**
     * Overridden to add in the results the pixel scale and the fiber diameter.
     * @param configuration The configuration of the calculator
     */
    @Override
    protected void performForEveryRetrieval(Quartet<Double, String, Double, String> configuration) {
        ResultsHolder.getResults().addResult(new DoubleValueResult("PIXEL_SCALE",
                configuration.getValue0(), configuration.getValue1()), Level.DEBUG);
        ResultsHolder.getResults().addResult(new DoubleValueResult("FIBER_DIAMETER",
                configuration.getValue2(), configuration.getValue3()), Level.DEBUG);
    }
    
}
