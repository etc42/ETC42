/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.psf;

import java.text.MessageFormat;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TreeMap;
import lombok.EqualsAndHashCode;
import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.analysis.BivariateRealFunction;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.apache.log4j.Logger;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.Cacheable;
import org.cnrs.lam.cesam.util.calculator.CachingPolicy;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.calculator.util.functions.SymmetricBivariateGaussian;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.javatuples.Pair;
import org.javatuples.Unit;

/**
 * <p>The {@link DiffractionLimited} class is a calculator for computing the PSF
 * of the instrument following a circularly symmetric bivariate gaussian distribution 
 * defined by the diffraction limited FWHM. The equation used for the calculation of 
 * the FWHM is:</p>
 * {@latex.inline $
 * FWHM_{(\\lambda)}=1.22*\\displaystyle\\frac{\\lambda*10^{-8}}{D_1}*206265
 * $}
 * <p>where</p>
 * <ul>
 *   <li>{@latex.inline $FWHM_{(\\lambda)}$}: is the FWHM of the PSF expressed in
 *       {@latex.inline $arcsec$}</li>
 *   <li>{@latex.inline $\\lambda$}: is the wavelength expressed in angstrom 
 *       ({@latex.inline \\AA})</li>
 *   <li>{@latex.inline $D_1$}: is the diameter of the primary mirror expressed
 *       in centimeters ({@latex.inline $cm$})</li>
 * </ul>
 * 
 * <p>The configuration of the calculator is a {@link org.javatuples.Pair},
 * with contains the following elements:</p>
 * <ol>
 *   <li>{@link java.lang.Double}: The diameter of the primary mirror</li>
 *   <li>{@link java.lang.String}: The unit of the diameter of the primary mirror</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the wavelength for which the calculation
 * is done (expressed in {@latex.inline \\AA}).</p>
 * 
 * <p>The output of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link BivariateRealFunction} representing the PSF (both axes are expressed in
 * {@latex.inline $arcsec$}).</p>
 */
@Cacheable(CachingPolicy.INSTANCE)
@EqualsAndHashCode(callSuper=false)
public class DiffractionLimited extends AbstractCalculator<Pair<Double, String>, Unit<Double>, Unit<BivariateRealFunction>> {
    
    private static final Logger logger = Logger.getLogger(DiffractionLimited.class);
    // The bundle with all the error translations
    private static final ResourceBundle validationErrorsBundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");
    private double diameter;
    
    /**
     * Validates the given configuration. It requires that the first value of the
     * tuple (the diameter of the telescope) is a positive number and that the second
     * value (the unit of the diameter) is {@latex.inline $cm$}.
     * 
     * @param configuration The configuration to validate
     * @throws ConfigurationException if the configuration is invalid
     */
    @Override
    protected void validateConfiguration(Pair<Double, String> configuration) throws ConfigurationException {
        // Check that the diameter is a positive number
        if (configuration.getValue0() <= 0) {
            String message = validationErrorsBundle.getString("MIRROR_DIAMETER_NOT_POSITIVE");
            message = MessageFormat.format(message, configuration.getValue0());
            throw new ConfigurationException(message);
        }
        // Check if the mirror diameter is in cm
        if (!Units.isCm(configuration.getValue1())) {
            String message = validationErrorsBundle.getString("MIRROR_DIAMETER_WRONG_UNIT");
            message = MessageFormat.format(message, Units.CM, configuration.getValue0());
            throw new ConfigurationException(message);
        }
    }

    /**
     * Initializes the calculator instance with the given configuration. The first
     * value of the configuration is the diameter of the telescope and the second is
     * the unit of the diameter.
     * @param configuration The configuration of the calculator
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(Pair<Double, String> configuration) throws InitializationException {
        diameter = configuration.getValue0();
    }

    /**
     * Returns a {@link org.javatuples.Unit} containing a bivariate function, of
     * which both axes are expressed in {@latex.inline $arcsec$}, which represents
     * the PSF for the given wavelength.
     * @param input The wavelength expressed in {@latex.inline \\AA}
     * @return A function representing the PSF
     * @throws CalculationException if there is any error during the calculation
     */
    @Override
    protected Unit<BivariateRealFunction> performCalculation(Unit<Double> input) throws CalculationException {
        double lambda = input.getValue0();
        double fwhm = 1.22 * (lambda * 1.e-8 / this.diameter) * 206265;
        double sigma = fwhm / 2.3548;
        SymmetricBivariateGaussian gaussian = new SymmetricBivariateGaussian(sigma);
        return new Unit<BivariateRealFunction>(gaussian);
    }

    /**
     * <p>For every calculation the calculator adds in the simulation results
     * the following outputs of the calculator:</p>
     * <ul>
     *   <li>The FWHM of the instruments PSF in intermediate unimportant results
     *       with code INSTRUMENT_PSF_FWHM</li>
     *   <li>The projection of the PSF on the plane y=0 in the intermediate
     *       unimportant results with code INSTRUMENT_PSF_PROJECTION</li>
     * </ul>
     * @param input
     * @param output 
     */
    @Override
    protected void performForEveryCalculation(Unit<Double> input, Unit<BivariateRealFunction> output) {
        double lambda = input.getValue0();
        // We know that we have a symmetric bivariate gaussian so the casting is safe
        SymmetricBivariateGaussian gaussian = (SymmetricBivariateGaussian) output.getValue0();
        double sigma = gaussian.getStandardDeviation();
        
        // We add the FWHM of the PSF in the results
        double fwhm = sigma * 2.3548;
        ResultsHolder.getResults().addResult("INSTRUMENT_PSF_FWHM", lambda, fwhm, 
                Units.ANGSTROM, Units.ARCSEC, CalculationResults.Level.INTERMEDIATE_UNIMPORTANT);
        
        // We add the projection on the y = 0 plane in the results. We calculate
        // the valuse in the range of 5 sigma, as all the values outside of this
        // are too close to zero.
        UnivariateRealFunction projection = gaussian.projectionFunction();
        Map<Double, Double> projectionData = new TreeMap<Double, Double>();
        // We will create 100 points to represent the plot
        double step = sigma / 20;
        for (double x = -5 * sigma; x <= 5 * sigma; x += step) {
            double value = 0;
            try {
                value = projection.value(x);
            } catch (FunctionEvaluationException ex) {
                logger.warn("Failed to calculate the value of the PSF projection", ex);
            }
            projectionData.put(x, value);
        }
        CalculationResults.DoubleDatasetResult projectionResult = 
                new CalculationResults.DoubleDatasetResult("INSTRUMENT_PSF_PROJECTION", projectionData, Units.ARCSEC, null);
        ResultsHolder.getResults().addResult("INSTRUMENT_PSF_PROJECTION", lambda, projectionResult,
                Units.ANGSTROM, CalculationResults.Level.INTERMEDIATE_UNIMPORTANT);
    }

    /**
     * <p>For every calculator instantiation the following parts of its configuration
     * are added in the simulation results:</p>
     * <ul>
     *   <li>The diameter of the primary mirror in debug results with code PRIMARY_DIAMETER</li>
     * </ul>
     * @param configuration The configuration of the calculator
     */
    @Override
    protected void performForEveryRetrieval(Pair<Double, String> configuration) {
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("PRIMARY_DIAMETER",
                configuration.getValue0(), configuration.getValue1()), CalculationResults.Level.DEBUG);
    }
    
}
