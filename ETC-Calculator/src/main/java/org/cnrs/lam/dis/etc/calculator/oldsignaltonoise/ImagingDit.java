/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.oldsignaltonoise;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import lombok.EqualsAndHashCode;
import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.MaxIterationsExceededException;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.Calculator;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.CalculationError;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.IntegrationUtil;
import org.cnrs.lam.dis.etc.calculator.util.InterpolatableDataset;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.DoubleValueResult;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.javatuples.Octet;
import org.javatuples.Pair;
import org.javatuples.Quartet;
import org.javatuples.Tuple;
import org.javatuples.Unit;

/**
 */
@EqualsAndHashCode(callSuper=false, exclude={"minLambda", "maxLambda"})
public class ImagingDit extends AbstractCalculator<
                Octet<
                      Calculator<Tuple, Pair<Double, Double>>,
                      Calculator<Unit<Double>, Unit<Double>>,
                      Calculator<Unit<Double>, Unit<Double>>,
                      Calculator<Unit<Double>, Unit<Double>>,
                      Pair<Double, String>,
                      Pair<Double, String>,
                      Pair<Double, String>,
                      Pair<Double, String>
                >, Tuple, 
                Quartet<
                       Map<Double, Double>,
                       Map<Double, Double>,
                       Map<Double, Double>,
                       Map<Double, Double>
                >> {
    
    private Calculator<Tuple, Pair<Double, Double>> wavelengthRange;
    private Calculator<Unit<Double>, Unit<Double>> signal;
    private Calculator<Unit<Double>, Unit<Double>> numberOfPixels;
    private Calculator<Unit<Double>, Unit<Double>> backgroundNoise;
    private double dark;
    private double readout;
    private double dit;
    private double exposureTime;
    
    private double minLambda;
    private double maxLambda;

    @Override
    protected void validateConfiguration(Octet<
                      Calculator<Tuple, Pair<Double, Double>>,
                      Calculator<Unit<Double>, Unit<Double>>,
                      Calculator<Unit<Double>, Unit<Double>>,
                      Calculator<Unit<Double>, Unit<Double>>,
                      Pair<Double, String>,
                      Pair<Double, String>,
                      Pair<Double, String>,
                      Pair<Double, String>> configuration) throws ConfigurationException {
        // Check the dark current
        Pair<Double, String> darkPair = configuration.getValue4();
        if (darkPair.getValue0() < 0) {
            throw new ConfigurationException("Dark current must be non negative "
                    + "but was " + darkPair.getValue0());
        }
        if (!Units.isElectronsPerPixelPerSec(darkPair.getValue1())) {
             throw new ConfigurationException("Dark current must be in " + Units.getElectronsPerPixelPerSec()
                    + " but was in " + darkPair.getValue1());
        }
        // Check the readout noise
        Pair<Double, String> readoutPair = configuration.getValue5();
        if (readoutPair.getValue0() < 0) {
            throw new ConfigurationException("Readout noise must be non negative "
                    + "but was " + readoutPair.getValue0());
        }
        if (!Units.isElectronsPerPixel(readoutPair.getValue1())) {
             throw new ConfigurationException("Readout noise must be in " + Units.getElectronsPerPixel()
                    + " but was in " + readoutPair.getValue1());
        }
        // Check the dit
        Pair<Double, String> ditPair = configuration.getValue6();
        if (ditPair.getValue0() < 0) {
            throw new ConfigurationException("DIT must be non negative "
                    + "but was " + ditPair.getValue0());
        }
        if (!Units.isSec(ditPair.getValue1())) {
             throw new ConfigurationException("DIT must be in " + Units.SEC
                    + " but was in " + ditPair.getValue1());
        }
        // Check the exposure time
        Pair<Double, String> exposureTimePair = configuration.getValue7();
        if (exposureTimePair.getValue0() <= 0) {
            throw new ConfigurationException("Exposure time must be possitive "
                    + "but was " + exposureTimePair.getValue0());
        }
        if (!Units.isSec(exposureTimePair.getValue1())) {
             throw new ConfigurationException("Exposure time must be in " + Units.SEC
                    + " but was in " + exposureTimePair.getValue1());
        }
    }

    @Override
    protected void initialize(Octet<
                      Calculator<Tuple, Pair<Double, Double>>,
                      Calculator<Unit<Double>, Unit<Double>>,
                      Calculator<Unit<Double>, Unit<Double>>,
                      Calculator<Unit<Double>, Unit<Double>>,
                      Pair<Double, String>,
                      Pair<Double, String>,
                      Pair<Double, String>,
                      Pair<Double, String>> configuration) throws InitializationException {
        wavelengthRange = configuration.getValue0();
        signal = configuration.getValue1();
        numberOfPixels = configuration.getValue2();
        backgroundNoise = configuration.getValue3();
        dark = configuration.getValue4().getValue0();
        readout = configuration.getValue5().getValue0();
        dit = configuration.getValue6().getValue0();
        exposureTime = configuration.getValue7().getValue0();
        Pair<Double, Double> minMax = null;
        try {
            minMax = wavelengthRange.calculate(null);
        } catch (CalculationException e) {
            throw new InitializationException("Failed to calculate the wavelength range", e);
        }
        minLambda = minMax.getValue0();
        maxLambda = minMax.getValue1();
    }

    @Override
    protected  Quartet<
                       Map<Double, Double>,
                       Map<Double, Double>,
                       Map<Double, Double>,
                       Map<Double, Double>
                > performCalculation(Tuple input) throws CalculationException {
        double signalValue = calculateSignalIntegral();
        double backgroundNoiseValue = calculateBackgroundNoiseIntegral();
        double numberOfPixelsValue = calculateNumberOfPixels();
        double detectorPerPixel = dark * exposureTime + (exposureTime / dit) * readout * readout;
        
        double s = signalValue * exposureTime;
        double bg = backgroundNoiseValue * exposureTime;
        double det = numberOfPixelsValue * detectorPerPixel;
        double signalToNoise = s / Math.sqrt(s + bg + det);
        
        double centralWavelength = (maxLambda - minLambda) / 2;
        Map<Double, Double> signalToNoiseMap = new HashMap<Double, Double>();
        signalToNoiseMap.put(centralWavelength, signalToNoise);
        Map<Double, Double> totalSignalMap = new HashMap<Double, Double>();
        totalSignalMap.put(centralWavelength, s);
        Map<Double, Double> totalBackgroundNoiseMap = new HashMap<Double, Double>();
        totalBackgroundNoiseMap.put(centralWavelength, bg);
        Map<Double, Double> totalDetectorNoiseMap = new HashMap<Double, Double>();
        totalDetectorNoiseMap.put(centralWavelength, det);
        return new Quartet<Map<Double, Double>, Map<Double, Double>, Map<Double, Double>, Map<Double, Double>>
                (signalToNoiseMap, totalSignalMap, totalBackgroundNoiseMap, totalDetectorNoiseMap);
    }

    private double calculateSignalIntegral() throws CalculationException {
        return IntegrationUtil.templateIntegral(signal, minLambda, maxLambda);
    }

    private double calculateBackgroundNoiseIntegral() throws CalculationException {
        return IntegrationUtil.templateIntegral(backgroundNoise, minLambda, maxLambda);
    }

    private double calculateNumberOfPixels() throws CalculationException {
        int minLambdaInt = (int) Math.floor(minLambda);
        int maxLambdaInt = (int) Math.ceil(maxLambda);
        double result = 0;
        for (int lambda = minLambdaInt; lambda <= maxLambdaInt; lambda++) {
            Unit<Double> input = new Unit<Double>((double) lambda);
            double noOfPixels = numberOfPixels.calculate(input).getValue0();
            if (noOfPixels > result)
                result = noOfPixels;
        }
        return result;
    }

    @Override
    protected void performForEveryCalculation(Tuple input,
            Quartet<Map<Double, Double>, Map<Double, Double>, Map<Double, Double>, Map<Double, Double>> output) {
        // We know the map has only one entry with key the central wavelength
        Map<Double, Double> snrMap = output.getValue0();
        Entry<Double, Double> snrEntry = snrMap.entrySet().iterator().next();
        double centralWavelength = snrEntry.getKey();
        double snr = snrEntry.getValue();
        ResultsHolder.getResults().addResult(new DoubleValueResult("FILTER_CENTRAL_WAVELENGTH",
                centralWavelength, Units.ANGSTROM), Level.INTERMEDIATE_UNIMPORTANT);
        ResultsHolder.getResults().addResult(new DoubleValueResult("SIGNAL_TO_NOISE",
                snr, null), Level.FINAL);
        double totalSignal = output.getValue1().values().iterator().next();
        ResultsHolder.getResults().addResult(new DoubleValueResult("TOTAL_SIGNAL",
                totalSignal, Units.ELECTRONS), Level.FINAL);
        double totalBackgroundNoise = output.getValue2().values().iterator().next();
        ResultsHolder.getResults().addResult(new DoubleValueResult("TOTAL_BACKGROUND_NOISE",
                totalBackgroundNoise, Units.ELECTRONS), Level.FINAL);
        double totalDetectorNoise = output.getValue3().values().iterator().next();
        ResultsHolder.getResults().addResult(new DoubleValueResult("TOTAL_DELTECTOR_NOISE",
                totalDetectorNoise, Units.ELECTRONS), Level.FINAL);
    }

    @Override
    protected void performForEveryRetrieval(Octet<
                      Calculator<Tuple, Pair<Double, Double>>,
                      Calculator<Unit<Double>, Unit<Double>>,
                      Calculator<Unit<Double>, Unit<Double>>,
                      Calculator<Unit<Double>, Unit<Double>>,
                      Pair<Double, String>,
                      Pair<Double, String>,
                      Pair<Double, String>,
                      Pair<Double, String>> configuration) {
        Pair<Double, String> darkPair = configuration.getValue4();
        ResultsHolder.getResults().addResult(new DoubleValueResult("DARK_CURRENT", 
                darkPair.getValue0(), darkPair.getValue1()), Level.DEBUG);
        Pair<Double, String> readoutPair = configuration.getValue5();
        ResultsHolder.getResults().addResult(new DoubleValueResult("READOUT_NOISE", 
                readoutPair.getValue0(), readoutPair.getValue1()), Level.DEBUG);
        Pair<Double, String> ditPair = configuration.getValue6();
        ResultsHolder.getResults().addResult(new DoubleValueResult("DIT", 
                ditPair.getValue0(), ditPair.getValue1()), Level.DEBUG);
        Pair<Double, String> exposureTimePair = configuration.getValue7();
        ResultsHolder.getResults().addResult(new DoubleValueResult("EXPOSURE_TIME", 
                exposureTimePair.getValue0(), exposureTimePair.getValue1()), Level.DEBUG);
    }
    
}
