/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.util;

import java.util.Map;
import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.apache.commons.math.analysis.interpolation.SplineInterpolator;
import org.cnrs.lam.dis.etc.calculator.CalculationError;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class FluxUtil {

    public static double convertMagnitudeToFlux(double magnitude, double wavelength) {
        return Math.pow(10., -(magnitude + 48.6) / 2.5) * (Constants.SPEED_OF_LIGHT / (wavelength * wavelength));
    }

    public static double oneTimeInterpolation(Map<Double, Double> data, double missing) throws CalculationError {
        double[] x = new double[data.size()];
        double[] y = new double[data.size()];
        int i = 0;
        for (Map.Entry<Double, Double> entry : data.entrySet()) {
            x[i] = entry.getKey();
            y[i] = entry.getValue();
            i++;
        }
        return oneTimeInterpolation(x, y, missing);
    }

    public static double oneTimeInterpolation(double[] x, double[] y, double missing) throws CalculationError {
        SplineInterpolator splineInterpolator = new SplineInterpolator();
        UnivariateRealFunction function = splineInterpolator.interpolate(x, y);
        try {
            return function.value(missing);
        } catch (FunctionEvaluationException ex) {
            throw new CalculationError("The function evaluation failed");
        }
    }
}
