/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.systemefficiency;

import java.text.MessageFormat;
import java.util.Map;
import java.util.ResourceBundle;
import lombok.EqualsAndHashCode;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.DatasetProviderHolder;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.calculator.util.datasets.LinearFunctionDataset;
import org.cnrs.lam.dis.etc.calculator.util.functions.BoundedUnivariateFunction;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.javatuples.Tuple;
import org.javatuples.Unit;

/**
 *<p>The {@code TotalEfficiencyProfile} class is a calculator for computing the 
 * total system efficiency from a dataset. The configuration of the calculator
 * is a {@link org.javatuples.Unit} which contains the following elements:</p>
 * <ol>
 *   <li>{@link org.cnrs.lam.dis.etc.datamodel.DatasetInfo}: the information of the
 *       total system efficiency dataset</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Tuple}, as this
 * calculator does not accept any input. It should always be null.</p>
 * 
 * <p>The output of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link BoundedUnivariateRealFunction} representing the total system efficiency.</p>
 */

// Caching explenation
// -------------------
// This calculator is set to cache the instances because the configuration validation
// and the instantiation are retrieving the dataset, which is time consuming.
@Cacheable(CachingPolicy.INSTANCE)
@EqualsAndHashCode(callSuper=false, exclude={"efficiencyFunction"})
public class TotalEfficiencyProfile extends AbstractCalculator<Unit<DatasetInfo>, Tuple, Unit<BoundedUnivariateFunction>> {
    
    // The bundle with all the error translations
    private static final ResourceBundle validationErrorsBundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");
    // This is a variable where the system efficiency dataset info is stored.
    // This is used from lombok library for the equals and the hashCode methods.
    // We do not want to use the function we create, as this would be way too time
    // consuming.filterTransmissionDatasetInfo
    private DatasetInfo systemEfficiencyDatasetInfo;
    // The dataset which represents the system efficiency
    private LinearFunctionDataset efficiencyFunction;

    /**
     * Validates the given configuration. It requires that the instrument has a
     * system efficiency, that the data for the system efficiency is 
     * available and that the unit of X axis is {@latex.inline \\AA}.
     * @param configuration the configuration to validate
     * @throws ConfigurationException if the configuration is invalid
     */
    @Override
    protected void validateConfiguration(Unit<DatasetInfo> configuration) throws ConfigurationException {
        DatasetInfo info = configuration.getValue0();
        if (info == null) {
            String message = validationErrorsBundle.getString("TOTAL_EFFICIENCY_MISSING");
            throw new ConfigurationException(message);
        }
        Dataset dataset = DatasetProviderHolder.getDatasetProvider().getDataset(Dataset.Type.TRANSMISSION, info);
        if (dataset == null) {
            String message = validationErrorsBundle.getString("TOTAL_EFFICIENCY_NOT_AVAILABLE");
            message = MessageFormat.format(message, info);
            throw new ConfigurationException(message);
        }
        if (!Units.isAngstrom(dataset.getXUnit())) {
            String message = validationErrorsBundle.getString("TOTAL_EFFICIENCY_WRONG_X_UNIT");
            message = MessageFormat.format(message, Units.ANGSTROM, dataset.getXUnit());
            throw new ConfigurationException(message);
        }
    }

    /**
     * Initializes the calculator instance with the given configuration. The
     * configuration contains the total system transmission dataset info.
     * @param configuration the configuration to use for initialization
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(Unit<DatasetInfo> configuration) throws InitializationException {
        systemEfficiencyDatasetInfo = configuration.getValue0();
        Dataset dataset = DatasetProviderHolder.getDatasetProvider().getDataset(Dataset.Type.TRANSMISSION, systemEfficiencyDatasetInfo);
        efficiencyFunction = new LinearFunctionDataset(dataset.getData());
    }

    /**
     * Returns a {@link BoundedUnivariateRealFunction} which can be used to calculate
     * the total system transmission.
     * @param input The calculator has no input, so it can be always null
     * @return A {@link BoundedUnivariateRealFunction} which can be used to calculate
     * the total system transmission
     * @throws CalculationException if there is any error during the calculation
     */
    @Override
    protected Unit<BoundedUnivariateFunction> performCalculation(Tuple input) throws CalculationException {
        return new Unit<BoundedUnivariateFunction>(efficiencyFunction);
    }

    /**
     * <p>For every calculation the calculator adds in the simulation results
     * the following outputs of the calculator:</p>
     * <ul>
     *   <li>The total system transmission in intermediate important results with code
     *       SYSTEM_EFFICIENCY</li>
     * </ul>
     * @param input
     * @param output 
     */
    @Override
    protected void performForEveryCalculation(Tuple input, Unit<BoundedUnivariateFunction> output) {
        // If the result already exists we do not add it to save time
        if (ResultsHolder.getResults().getResultByName("SYSTEM_EFFICIENCY") != null) {
            return;
        }
        Map<Double, Double> data = efficiencyFunction.getData();
        CalculationResults.DoubleDatasetResult result = new CalculationResults.DoubleDatasetResult(
                "SYSTEM_EFFICIENCY", data, Units.ANGSTROM, null);
        ResultsHolder.getResults().addResult(result, CalculationResults.Level.INTERMEDIATE_IMPORTANT);
    }

    /**
     * <p>For every calculator instantiation the following parts of its configuration
     * are added in the simulation results:</p>
     * <ul>
     *   <li>The name of the total system transmission profile with code
     *       TOTAL_EFFICIENCY_PROFILE</li>
     * </ul>
     * @param configuration 
     */
    @Override
    protected void performForEveryRetrieval(Unit<DatasetInfo> configuration) {
        CalculationResults.StringResult result = new CalculationResults.StringResult(
                "TOTAL_EFFICIENCY_PROFILE", configuration.getValue0().toString());
        ResultsHolder.getResults().addResult(result, CalculationResults.Level.DEBUG);
    }
    
}
