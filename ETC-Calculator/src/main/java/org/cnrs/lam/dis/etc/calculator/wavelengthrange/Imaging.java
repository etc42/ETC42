/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.wavelengthrange;

import java.util.ArrayList;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import lombok.EqualsAndHashCode;
import org.apache.log4j.Logger;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.calculator.util.functions.BoundedUnivariateFunction;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.javatuples.Pair;
import org.javatuples.Tuple;
import org.javatuples.Unit;

/**
 *<p>The {@code Imaging} class is a calculator for computing the wavelength range
 * for an imager simulation. For this kind of simulation the wavelength range is
 * the overlapping of the non-zero ranges of the filter transmission and the total 
 * system efficiency. This is because outside this range there is no light reaching
 * the detector.</p>
 * 
 * <p>The configuration of the calculator is a {@link org.javatuples.Pair},
 * witch contains the following elements:</p>
 * <ol>
 *   <li>{@link Calculator}: A calculator for the filter transmission</li>
 *   <li>{@link Calculator}: A calculator for the total system efficiency</li>
 * </ol>
 * 
 * <p>The calculator does not need any input, so it accepts as an input
 * a {@link org.javatuples.Tuple} which will be ignored during the calculation
 * (it can be null).</p>
 * 
 * <p>The output of the calculator is a {@link org.javatuples.Pair} which contains
 * the following elements:</p>
 * <ol>
 *   <li>{@link java.lang.Double}: The minimum wavelength value (expressed in
 *       {@latex.inline \\AA})</li>
 *   <li>{@link java.lang.Double}: The maximum wavelength value (expressed in
 *       {@latex.inline \\AA})</li>
 * </ol>
 */
@Cacheable(CachingPolicy.INSTANCE)
@EqualsAndHashCode(callSuper=false, exclude={"minLambda", "maxLambda"})
public class Imaging extends AbstractCalculator<
        Pair<Calculator<Tuple, Unit<BoundedUnivariateFunction>>,
             Calculator<Tuple, Unit<BoundedUnivariateFunction>>>,
        Tuple, Pair<Double, Double>> {

    private static final Logger logger = Logger.getLogger(Imaging.class);
    private static final ResourceBundle validationErrorsBundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");
    private double minLambda;
    private double maxLambda;

    /**
     * It validates the configuration of the calculator. It checks that the filter
     * transmission and the total system efficiency have overlapping non-zero ranges.
     * @param configuration The configuration of the calculator
     * @throws ConfigurationException If the validation fails
     */
    @Override
    protected void validateConfiguration(
            Pair<Calculator<Tuple, Unit<BoundedUnivariateFunction>>, 
                 Calculator<Tuple, Unit<BoundedUnivariateFunction>>> configuration) 
            throws ConfigurationException {
        // Get the filter non-zero bounds
        Pair<Double, Double> filterBounds = null;
        try {
            filterBounds = configuration.getValue0().calculate(null).getValue0().getNonZeroBounds();
        } catch (CalculationException ex) {
            logger.info("Failed to calculate the filter transmission non-zero bounds", ex);
            throw new ConfigurationException(ex.getMessage());
        }
        // Get the system efficiency non-zero bounds
        Pair<Double, Double> systemBounds = null;
        try {
            systemBounds = configuration.getValue1().calculate(null).getValue0().getNonZeroBounds();
        } catch (CalculationException ex) {
            logger.info("Failed to calculate the total system efficiency non-zero bounds", ex);
            throw new ConfigurationException(ex.getMessage());
        }
        // Check that the two bounds are overlapping
        if (filterBounds.getValue0() >= systemBounds.getValue1() ||
                filterBounds.getValue1() <= systemBounds.getValue0()) {
            String message = validationErrorsBundle.getString("FILTER_TOTAL_EFFICIENCY_NOT_OVERLAPPING");
            throw new ConfigurationException(message);
        }
    }

    /**
     * Initializes the calculator.
     * @param configuration The configuration of the calculator
     * @throws InitializationException 
     */
    @Override
    protected void initialize(
            Pair<Calculator<Tuple, Unit<BoundedUnivariateFunction>>,
                 Calculator<Tuple, Unit<BoundedUnivariateFunction>>> configuration) 
            throws InitializationException {
        // Get the filter non-zero bounds
        Pair<Double, Double> filterBounds = null;
        try {
            filterBounds = configuration.getValue0().calculate(null).getValue0().getNonZeroBounds();
        } catch (CalculationException ex) {
            logger.info("Failed to calculate the filter transmission non-zero bounds", ex);
            throw new InitializationException(ex.getMessage());
        }
        // Get the system efficiency non-zero bounds
        Pair<Double, Double> systemBounds = null;
        try {
            systemBounds = configuration.getValue1().calculate(null).getValue0().getNonZeroBounds();
        } catch (CalculationException ex) {
            logger.info("Failed to calculate the total system efficiency non-zero bounds", ex);
            throw new InitializationException(ex.getMessage());
        }
        // Calculate the overlapping range
        minLambda = Math.max(filterBounds.getValue0(), systemBounds.getValue0());
        maxLambda = Math.min(filterBounds.getValue1(), systemBounds.getValue1());
    }

    /**
     * <p>Returns a {@link org.javatuples.Pair} which contains the following elements:</p>
     * <ol>
     *   <li>{@link java.lang.Double}: The minimum wavelength value (expressed in
     *       {@latex.inline \\AA})</li>
     *   <li>{@link java.lang.Double}: The maximum wavelength value (expressed in
     *       {@latex.inline \\AA})</li>
     * </ol>
     * <p>Both values are the ones given as input by the user.</p>
     */
    @Override
    protected Pair<Double, Double> performCalculation(Tuple input) throws CalculationException {
    	Pair<Double, Double> calculableRange = CommonRangeFactory.getCalculableRange(
			CommonRangeFactory.getSession(),
			new Pair<Double, Double>(minLambda, maxLambda)
		);
    	
        return new Pair<Double, Double>(calculableRange.getValue0(), calculableRange.getValue1());
    }
    
    /**
     * <p>For every calculation the calculator adds in the simulation results
     * the following outputs of the calculator:</p>
     * <ul>
     *   <li>The calculated minimum wavelength in intermediate unimportant results
     *       with code WAVELENGTH_MIN</li>
     *   <li>The calculated maximum wavelength in intermediate unimportant results
     *       with code WAVELENGTH_MAX</li>
     * </ul>
     * @param input
     * @param output 
     */
    @Override
    protected void performForEveryCalculation(Tuple input, Pair<Double, Double> output) {
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("WAVELENGTH_MIN", 
                output.getValue0(), Units.ANGSTROM), CalculationResults.Level.INTERMEDIATE_UNIMPORTANT);
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("WAVELENGTH_MAX", 
                output.getValue1(), Units.ANGSTROM), CalculationResults.Level.INTERMEDIATE_UNIMPORTANT);
    }
    
}
