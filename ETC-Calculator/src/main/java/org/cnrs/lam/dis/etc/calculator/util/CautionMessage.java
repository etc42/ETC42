/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.cnrs.lam.dis.etc.calculator.util;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author agross
 */
public class CautionMessage {
    
    private static CautionMessage instance = null;
    private Map<String, String> messages = new HashMap<String, String>();
    
    private CautionMessage(){};
    
    public static CautionMessage getInstance(){
        if(CautionMessage.instance == null){
            CautionMessage.instance = new CautionMessage();
        }
        
        return CautionMessage.instance;
    }
    
    public void addMessage(String key, String msg){
        this.messages.put(key, msg);
    }
    
    public void removeMessage(String key){
        this.messages.remove(key);
    }
    
    public Map<String, String> getMessages(){
        return this.messages;
    }
}
