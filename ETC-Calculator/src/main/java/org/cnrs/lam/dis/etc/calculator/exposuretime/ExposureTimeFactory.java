/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.exposuretime;

import java.util.ResourceBundle;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.deltalambda.DeltaLambdaFactory;
import org.cnrs.lam.dis.etc.calculator.extraBackgroundNoise.ExtraBackgroundNoiseFactory;
import org.cnrs.lam.dis.etc.calculator.extrasignal.ExtraSignalFactory;
import org.cnrs.lam.dis.etc.calculator.numberofpixels.NumberOfPixelsFactory;
import org.cnrs.lam.dis.etc.calculator.simulatedbackgroundnoise.SimulatedBackgroundNoiseFactory;
import org.cnrs.lam.dis.etc.calculator.simulatedsignal.SimulatedSignalFactory;
import org.cnrs.lam.dis.etc.calculator.util.functions.BoundedUnivariateFunction;
import org.cnrs.lam.dis.etc.calculator.wavelengthrange.WavelengthRangeFactory;
import org.cnrs.lam.dis.etc.configuration.ConfigFactory;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.cnrs.lam.dis.etc.datamodel.Instrument;
import org.cnrs.lam.dis.etc.datamodel.ObsParam;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.javatuples.Decade;
import org.javatuples.Ennead;
import org.javatuples.Pair;
import org.javatuples.Tuple;
import org.javatuples.Unit;

/**
 */
public class ExposureTimeFactory implements Factory<Unit<Session>, Tuple, Unit<Double>> {

    private static final ResourceBundle bundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");

    @Override
    public Calculator<Tuple, Unit<Double>> getCalculator(Unit<Session> configuration) throws InitializationException, ConfigurationException {
        Session session = configuration.getValue0();
        Instrument instrument = session.getInstrument();
        ObsParam obsParam = session.getObsParam();
        ObsParam.FixedSnrType fixedSnrType = obsParam.getFixedSnrType();
        if (fixedSnrType == ObsParam.FixedSnrType.CENTRAL_PIXEL && !ConfigFactory.getConfig().getCentralPixelFlag()) {
            throw new ConfigurationException(bundle.getString("CENTRAL_PIXEL_FIXED_SNR_DISABLED"));
        }
        
//        System.out.println("ExposureTimeFactory");
//        System.out.println(instrument.getInstrumentType());
//        System.out.println(obsParam.getTimeSampleType());
        
        Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>> simulatedSignal =
                new SimulatedSignalFactory().getCalculator(configuration);
//        System.out.println("END OF SimulatedSignalFactory");
        Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>> simulatedBackground =
                new SimulatedBackgroundNoiseFactory().getCalculator(configuration);
//        System.out.println("END OF SimulatedBackgroundNoiseFactory");
        Calculator<Unit<Double>, Unit<Double>> numberOfPixels =
                new NumberOfPixelsFactory().getCalculator(configuration);
//        System.out.println("END OF NumberOfPixelsFactory");
        Pair<Double, String> dark
                = new Pair(instrument.getDark(), instrument.getDarkUnit());
        Pair<Double, String> readout
                = new Pair(instrument.getReadout(), instrument.getReadoutUnit());
        // If we have fixed SNR then we make calculation for a single exposure.
        // Other calculations do not make sense.
        Integer nExpo = (obsParam.getFixedParameter() == ObsParam.FixedParameter.SNR)
                ? 1 : obsParam.getNoExpo(); 
        Pair<Double, String> dit 
                = new Pair(obsParam.getDit(), obsParam.getDitUnit());
        double fixedSnr = obsParam.getSnr();
        switch (instrument.getInstrumentType()) {
            case SPECTROGRAPH:
                Calculator<Unit<Double>, Unit<Double>> deltaLambda = 
                        new DeltaLambdaFactory().getCalculator(configuration);
                Pair<Double, String> fixedSnrLambda
                        = new Pair<Double, String>(obsParam.getSnrLambda(), obsParam.getSnrLambdaUnit());
                Calculator<Tuple, Unit<BoundedUnivariateFunction>> extraSignal =
                        new ExtraSignalFactory().getCalculator(configuration);
                Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>,
                        Calculator<Tuple, Unit<BoundedUnivariateFunction>>> signalPairSp =
                    new Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>,
                        Calculator<Tuple, Unit<BoundedUnivariateFunction>>>(simulatedSignal, extraSignal);
                Calculator<Tuple, Unit<BoundedUnivariateFunction>> extraBackground =
                        new ExtraBackgroundNoiseFactory().getCalculator(configuration);
                Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>,
                        Calculator<Tuple, Unit<BoundedUnivariateFunction>>> backgroundPairSp =
                    new Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>,
                        Calculator<Tuple, Unit<BoundedUnivariateFunction>>>(simulatedBackground, extraBackground);
                switch (obsParam.getTimeSampleType()) {
                    case DIT:
                        ResultsHolder.getResults().addResult(
                                new CalculationResults.StringResult("EXPOSURE_TIME_METHOD", "Spectroscopy / DIT"),
                                CalculationResults.Level.DEBUG);
                        Decade<Calculator<Unit<Double>, Unit<Double>>,
                                Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>,
                                     Calculator<Tuple, Unit<BoundedUnivariateFunction>>>,
                                Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>,
                                     Calculator<Tuple, Unit<BoundedUnivariateFunction>>>,
                                Calculator<Unit<Double>, Unit<Double>>,
                                Pair<Double, String>, Pair<Double, String>,
                                Pair<Double, String>, Double, Pair<Double, String>, ObsParam.FixedSnrType>
                                spectroscopyDitConfiguration =
                        new Decade<Calculator<Unit<Double>, Unit<Double>>,
                                Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>,
                                     Calculator<Tuple, Unit<BoundedUnivariateFunction>>>,
                                Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>,
                                     Calculator<Tuple, Unit<BoundedUnivariateFunction>>>,
                                Calculator<Unit<Double>, Unit<Double>>,
                                Pair<Double, String>, Pair<Double, String>,
                                Pair<Double, String>, Double, Pair<Double, String>, ObsParam.FixedSnrType>
                                (deltaLambda, signalPairSp, backgroundPairSp, numberOfPixels, dark, 
                                 readout, dit, fixedSnr, fixedSnrLambda, fixedSnrType);
                        return EtcCalculatorManager.getManager(SpectroscopyDit.class)
                                .getCalculator(spectroscopyDitConfiguration);
                    case NO_EXPO:
                        ResultsHolder.getResults().addResult(
                                new CalculationResults.StringResult("EXPOSURE_TIME_METHOD", "Spectroscopy / Exposures Number"),
                                CalculationResults.Level.DEBUG);
                        Decade<Calculator<Unit<Double>, Unit<Double>>,
                                Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>,
                                     Calculator<Tuple, Unit<BoundedUnivariateFunction>>>,
                                Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>,
                                     Calculator<Tuple, Unit<BoundedUnivariateFunction>>>,
                                Calculator<Unit<Double>, Unit<Double>>,
                                Pair<Double, String>, Pair<Double, String>,
                                Integer, Double, Pair<Double, String>, ObsParam.FixedSnrType>
                                spectroscopyNExpoConfiguration =
                        new Decade<Calculator<Unit<Double>, Unit<Double>>,
                                Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>,
                                     Calculator<Tuple, Unit<BoundedUnivariateFunction>>>,
                                Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>,
                                     Calculator<Tuple, Unit<BoundedUnivariateFunction>>>,
                                Calculator<Unit<Double>, Unit<Double>>,
                                Pair<Double, String>, Pair<Double, String>,
                                Integer, Double, Pair<Double, String>, ObsParam.FixedSnrType>
                                (deltaLambda, signalPairSp, backgroundPairSp, numberOfPixels, dark, 
                                 readout, nExpo, fixedSnr, fixedSnrLambda, fixedSnrType);
                        return EtcCalculatorManager.getManager(SpectroscopyExposuresNumber.class)
                                .getCalculator(spectroscopyNExpoConfiguration);
                }
            case IMAGING:
                Pair<Double, String> extraSignalPair = new Pair<Double, String>
                        (obsParam.getExtraSignal(), obsParam.getExtraSignalUnit());
                Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>,Pair<Double, String>> signalPairIm
                        = new Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>, Pair<Double, String>>
                        (simulatedSignal, extraSignalPair);
                Pair<Double, String> extraBackgroundPair = new Pair<Double, String>
                        (obsParam.getExtraBackgroundNoise(), obsParam.getExtraBackgroundNoiseUnit());
                Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>,Pair<Double, String>> backgroundPairIm
                        = new Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>, Pair<Double, String>>
                        (simulatedBackground, extraBackgroundPair);
                Calculator<Tuple, Pair<Double, Double>> wavelengthRange =
                        new WavelengthRangeFactory().getCalculator(configuration);
                switch (obsParam.getTimeSampleType()) {
                    case DIT:
                        ResultsHolder.getResults().addResult(
                                new CalculationResults.StringResult("EXPOSURE_TIME_METHOD", "Imaging / DIT"),
                                CalculationResults.Level.DEBUG);
                        Ennead<Calculator<Tuple, Pair<Double, Double>>,
                                Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>,
                                    Pair<Double, String>>, 
                                Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>,
                                    Pair<Double, String>>, 
                                Calculator<Unit<Double>, Unit<Double>>,
                                Pair<Double, String>, Pair<Double, String>,
                                Pair<Double, String>, Double, ObsParam.FixedSnrType>
                                imagingDitConfiguration =
                        new Ennead<Calculator<Tuple, Pair<Double, Double>>,
                                Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>,
                                    Pair<Double, String>>, 
                                Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>,
                                    Pair<Double, String>>, 
                                Calculator<Unit<Double>, Unit<Double>>,
                                Pair<Double, String>, Pair<Double, String>,
                                Pair<Double, String>, Double, ObsParam.FixedSnrType>
                                (wavelengthRange, signalPairIm, backgroundPairIm, numberOfPixels, dark, 
                                 readout, dit, fixedSnr, fixedSnrType);
                        return EtcCalculatorManager.getManager(ImagingDit.class)
                                .getCalculator(imagingDitConfiguration);
                    case NO_EXPO:
                        ResultsHolder.getResults().addResult(
                                new CalculationResults.StringResult("EXPOSURE_TIME_METHOD", "Imaging / Exposures Number"),
                                CalculationResults.Level.DEBUG);
                        Ennead<Calculator<Tuple, Pair<Double, Double>>,
                                Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>,
                                    Pair<Double, String>>, 
                                Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>,
                                    Pair<Double, String>>, 
                                Calculator<Unit<Double>, Unit<Double>>,
                                Pair<Double, String>, Pair<Double, String>,
                                Integer, Double, ObsParam.FixedSnrType>
                                imagingNExpoConfiguration =
                        new Ennead<Calculator<Tuple, Pair<Double, Double>>,
                                Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>,
                                    Pair<Double, String>>, 
                                Pair<Calculator<Pair<Double, Double>, Pair<UnivariateRealFunction, UnivariateRealFunction>>,
                                    Pair<Double, String>>, 
                                Calculator<Unit<Double>, Unit<Double>>,
                                Pair<Double, String>, Pair<Double, String>,
                                Integer, Double, ObsParam.FixedSnrType>
                                (wavelengthRange, signalPairIm, backgroundPairIm, numberOfPixels, dark, 
                                 readout, nExpo, fixedSnr, fixedSnrType);
                        return EtcCalculatorManager.getManager(ImagingExposuresNumber.class)
                                .getCalculator(imagingNExpoConfiguration);
                }
        }
        
        String message = bundle.getString("UNKNOWN_EXPOSURE_TIME_METHOD");
        throw new ConfigurationException(message);
    }
    
}
