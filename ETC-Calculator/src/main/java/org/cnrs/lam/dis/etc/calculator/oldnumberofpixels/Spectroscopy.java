/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.oldnumberofpixels;

import lombok.EqualsAndHashCode;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.Calculator;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.StringResult;
import org.javatuples.Triplet;
import org.javatuples.Unit;

/**
 * <p>The {@code Photometry} class is a calculator for calculating the number of
 * pixels of the detector which are affected by the light from the source. The
 * equation used for the calculation is:</p>
 * {@latex.inline $
 * N_{pix(\\lambda)} = 
 * \\left \\|
 * \\begin{array}{ll}
 * \\left\\lceil SpatialBinning_{(\\lambda)}\\right\\rceil
 * * \\left\\lceil SpectralBinning\\right\\rceil
 * & $, for per spectral resolution element calculation$ \\\\ \\\\
 * \\left\\lceil SpatialBinning_{(\\lambda)}\\right\\rceil
 * & $, for per spectral pixel calculation$
 * \\end{array}
 * \\right .
 * $}
 * <p>where</p>
 * <ul>
 *   <li>{@latex.inline $N_{pix(\\lambda)}$}: is the number of active pixels</li>
 *   <li>{@latex.inline $\\lambda$}: is the wavelength expressed in angstrom 
 *       ({@latex.inline \\AA})</li>
 *   <li>{@latex.inline $SpatialBinning$}: is the spatial binning in {@latex.inline $pixels$}
 *       (see package {@code org.cnrs.lam.dis.etc.calculator.spatialbinning})</li>
 *   <li>{@latex.inline $SpectralBinning$}: is the spectral binning in {@latex.inline $pixels$}
 *       (see package {@code org.cnrs.lam.dis.etc.calculator.spectralbinning})</li>
 *   <li>{@latex.inline $\\lceil\\ \\rceil$}: is the ceiling function (rounding up)</li>
 * </ul>
 * 
 * <p>The configuration of the calculator is a {@link org.javatuples.Triplet},
 * witch contains the following elements:</p>
 * <ol>
 *   <li>{@link org.cnrs.lam.cesam.util.calculator.Calculator}: a calculator for
 *       calculating the spatial binning</li>
 *   <li>{@link org.cnrs.lam.cesam.util.calculator.Calculator}: a calculator for
 *       calculating the spectral binning</li>
 *   <li>{@link java.lang.Boolean}: true if the calculation is per spectral resolution
 *       element, false if it is per spectral pixel</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the wavelength expressed in {@latex.inline \\AA}.</p>
 * 
 * <p>The output of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the number of active pixels, expressed in
 * {@latex.inline $pixels$}.</p>
 */
@EqualsAndHashCode(callSuper=false)
public class Spectroscopy extends AbstractCalculator<Triplet<Calculator<Unit<Double>, Unit<Double>>, Calculator<Unit<Double>, Unit<Double>>, Boolean>, Unit<Double>, Unit<Double>> {
    
    private Calculator<Unit<Double>, Unit<Double>> spatialBinningCalculator;
    private Calculator<Unit<Double>, Unit<Double>> spectralBinningCalculator;
    private boolean isPerSpectralResolutionElement;

    
    /**
     * Validates the given configuration. Currently it does nothing.
     * @param configuration the configuration to validate
     * @throws ConfigurationException if the configuration is invalid
     */
    @Override
    protected void validateConfiguration(Triplet<Calculator<Unit<Double>, Unit<Double>>, Calculator<Unit<Double>, Unit<Double>>, Boolean> configuration) throws ConfigurationException {
    }

    /**
     * Initializes the calculator instance with the given configuration. The first
     * value of the configuration is the spatial binning calculator, the second
     * is the spectral binning calculator and the third one is true if the
     * calculation is per spectral resolution element and false if it is per
     * spectral pixel..
     * @param configuration the configuration to use for initialization
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(Triplet<Calculator<Unit<Double>, Unit<Double>>, Calculator<Unit<Double>, Unit<Double>>, Boolean> configuration) throws InitializationException {
        spatialBinningCalculator = configuration.getValue0();
        spectralBinningCalculator = configuration.getValue1();
        isPerSpectralResolutionElement = configuration.getValue2();
    }

    /**
     * Returns a {@link org.javatuples.Unit} containing the number of active pixels,
     * expressed in {@latex.inline $pixels$}.
     * @param input The wavelength expressed in {@latex.inline \\AA}
     * @return The number of active pixels
     * @throws CalculationException if there is any error during the calculation
     */
    @Override
    protected Unit<Double> performCalculation(Unit<Double> input) throws CalculationException {
        double pixelsInSpatial = Math.ceil(spatialBinningCalculator.calculate(input).getValue0());
        double pixelsInSpectral = isPerSpectralResolutionElement
                ? Math.ceil(spectralBinningCalculator.calculate(input).getValue0())
                : 1;
        double result = pixelsInSpatial * pixelsInSpectral;
        return new Unit<Double>(result);
    }

    /**
     * Overridden to add in the results the number of pixels. This is done here, so
     * we have this results even if the result is retrieved from the cache.
     * @param input The wavelength expressed in {@latex.inline \\AA}
     * @param output The number of pixels expressed in {@latex.inline $pixels$}
     */
    @Override
    protected void performForEveryCalculation(Unit<Double> input, Unit<Double> output) {
        ResultsHolder.getResults().addResult("NUMBER_OF_PIXELS", input.getValue0()
                , output.getValue0(), Units.ANGSTROM, Units.PIXEL, Level.INTERMEDIATE_IMPORTANT);
    }

    /**
     * Overridden to add in the results the spectral quantum.
     * @param configuration The configuration of the calculator
     */
    @Override
    protected void performForEveryRetrieval(Triplet<Calculator<Unit<Double>, Unit<Double>>, Calculator<Unit<Double>, Unit<Double>>, Boolean> configuration) {
        String spectralQuantum = configuration.getValue2()
                ? "Spectral resolution element"
                : "Spectral pixel";
        ResultsHolder.getResults().addResult(new StringResult("SPECTRAL_QUANTUM"
                , spectralQuantum), Level.DEBUG);
    }
    
}
