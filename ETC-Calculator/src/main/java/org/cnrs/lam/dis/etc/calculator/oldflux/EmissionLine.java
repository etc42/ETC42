/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.oldflux;

import lombok.EqualsAndHashCode;
import org.apache.commons.math.MathException;
import org.apache.commons.math.special.Erf;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.Cacheable;
import org.cnrs.lam.cesam.util.calculator.CachingPolicy;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.Calculator;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.DoubleValueResult;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.javatuples.Septet;
import org.javatuples.Unit;

/**
 */
@Cacheable(CachingPolicy.ALL)
@EqualsAndHashCode(callSuper=false, exclude={"sigma"})
public class EmissionLine extends AbstractCalculator<
        Septet<Double, Double, String, Double, String, 
               Calculator<Unit<Double>, Unit<Double>>, Double
        >, Unit<Double>, Unit<Double>> {
    
    private double fluxEm;
    private double lambdaEm;
    private double fwhmEm;
    private Calculator<Unit<Double>, Unit<Double>> kernelCalculator;
    private double redshift;
    
    private double sigma;

    @Override
    protected void validateConfiguration(
            Septet<Double, Double, String, Double, String, 
                   Calculator<Unit<Double>, Unit<Double>>, Double
            > configuration) throws ConfigurationException {
        if (configuration.getValue0() <= 0) {
            throw new ConfigurationException("Emission line flux must be a positive number " +
                    "but was " + configuration.getValue0());
        }
        if (configuration.getValue1() <= 0) {
            throw new ConfigurationException("Emission line wavelength must be a positive number " +
                    "but was " + configuration.getValue1());
        }
        if (!Units.isAngstrom(configuration.getValue2())) {
            throw new ConfigurationException("Emission line wavelength must be in " +
                    Units.ANGSTROM + "but was in " + configuration.getValue2());
        }
        if (configuration.getValue3() <= 0) {
            throw new ConfigurationException("Emission line FWHM must be a positive number " +
                    "but was " + configuration.getValue3());
        }
        if (!Units.isAngstrom(configuration.getValue4())) {
            throw new ConfigurationException("Emission line FWHM must be in " +
                    Units.ANGSTROM + "but was in " + configuration.getValue4());
        }
        if (configuration.getValue6() < 0) {
            throw new ConfigurationException("Redshift can not be a negative number " +
                    "but was " + configuration.getValue4());
        }
    }

    @Override
    protected void initialize(
            Septet<Double, Double, String, Double, String, 
                   Calculator<Unit<Double>, Unit<Double>>, Double
            > configuration) throws InitializationException {
        redshift = configuration.getValue6();
        fluxEm = configuration.getValue0();
        lambdaEm = configuration.getValue1()*(1+redshift);
        fwhmEm = configuration.getValue3()*(1+redshift);
        kernelCalculator = configuration.getValue5();
        
        sigma = fwhmEm / 2.37;
    }

    @Override
    protected Unit<Double> performCalculation(Unit<Double> input) throws CalculationException {
        double lambda = input.getValue0();
        double sqrtTwoSigma = Math.sqrt(2) * sigma;
        double kernel = kernelCalculator.calculate(input).getValue0();
        double erfHigh;
        double erfLow;
        try {
            erfHigh = Erf.erf((lambda + kernel/2 - lambdaEm) / sqrtTwoSigma);
            erfLow = Erf.erf((lambda - kernel/2 - lambdaEm) / sqrtTwoSigma);
        } catch (MathException e) {
            throw new CalculationException("Failed to calculate the Flux of the emission line."
                    + " Reason:\n" + e.getMessage());
        }
        double result = fluxEm*(erfHigh-erfLow)/2/kernel;
        return new Unit<Double>(result);
    }

    @Override
    protected void performForEveryCalculation(Unit<Double> input, Unit<Double> output) {
        ResultsHolder.getResults().addResult("SIGNAL_FLUX", input.getValue0(), output.getValue0(),
                Units.ANGSTROM, Units.getErgPerCm2PerSecPerAngstrom(), Level.INTERMEDIATE_UNIMPORTANT);
    }

    @Override
    protected void performForEveryRetrieval(Septet<Double, Double, String, Double, String, Calculator<Unit<Double>, Unit<Double>>, Double> configuration) {
        ResultsHolder.getResults().addResult(new DoubleValueResult("EMISSION_LINE_FLUX"
                , configuration.getValue0(), null), Level.DEBUG);
        ResultsHolder.getResults().addResult(new DoubleValueResult("EMISSION_LINE_WAVELENGTH"
                , configuration.getValue1(), configuration.getValue2()), Level.DEBUG);
        ResultsHolder.getResults().addResult(new DoubleValueResult("EMISSION_LINE_FWHM"
                , configuration.getValue3(), configuration.getValue4()), Level.DEBUG);
        ResultsHolder.getResults().addResult(new DoubleValueResult("REDSHIFT"
        , configuration.getValue6(), null), Level.DEBUG);
    }
    
}
