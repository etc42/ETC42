/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.extrasignal;

import java.util.ResourceBundle;
import org.cnrs.lam.cesam.util.calculator.*;
import org.cnrs.lam.dis.etc.calculator.EtcCalculatorManager;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.functions.BoundedUnivariateFunction;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.cnrs.lam.dis.etc.datamodel.Instrument;
import org.cnrs.lam.dis.etc.datamodel.ObsParam;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.javatuples.Tuple;
import org.javatuples.Unit;

/**
 * The class {@code ExtraSignalFactory} is a factory for creating calculators
 * for performing the extra signal per Angstrom calculation.
 */
public class ExtraSignalFactory implements Factory<Unit<Session>, Tuple, Unit<BoundedUnivariateFunction>> {

    private static final ResourceBundle bundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");

    /**
     * Provides a calculator for performing extra signal calculations. The
     * type of the calculator returned is added in the debug results with the
     * code EXTRA_SIGNAL_METHOD.
     * @param configuration The session for which the simulation is executed
     * @return A calculator for performing extra signal calculations
     * @throws InitializationException If the initialization of the calculator fails
     * @throws ConfigurationException  If the configuration session is invalid
     */
    @Override
    public Calculator<Tuple, Unit<BoundedUnivariateFunction>> getCalculator(Unit<Session> configuration) throws InitializationException, ConfigurationException {
        Session session = configuration.getValue0();
        Instrument instrument = session.getInstrument();
        ObsParam obsParam = session.getObsParam();
        // This calculation should be used only for spectrogrph simulation
        if (instrument.getInstrumentType() == Instrument.InstrumentType.SPECTROGRAPH) {
            switch (obsParam.getExtraSignalType()) {
                case ONLY_CALCULATED_SIGNAL:
                    ResultsHolder.getResults().addResult(new CalculationResults.StringResult(
                            "EXTRA_SIGNAL_METHOD", "Extra signal ignored"), CalculationResults.Level.DEBUG);
                    return EtcCalculatorManager.getManager(NoExtraSignal.class).getCalculator(null);
                case CALCULATED_AND_EXTRA_SIGNAL:
                case ONLY_EXTRA_SIGNAL:
                    ResultsHolder.getResults().addResult(new CalculationResults.StringResult(
                            "EXTRA_SIGNAL_METHOD", "Extra signal template"), CalculationResults.Level.DEBUG);
                    DatasetInfo info = obsParam.getExtraSignalDataset();
                    Unit<DatasetInfo> calculatorConfiguration =
                            new Unit<DatasetInfo>(info);
                    return EtcCalculatorManager.getManager(ExtraSignalDataset.class).getCalculator(calculatorConfiguration);
            }
        }
        String message = bundle.getString("UNKNOWN_EXTRA_SIGNAL_METHOD");
        throw new ConfigurationException(message);
    }
    
}
