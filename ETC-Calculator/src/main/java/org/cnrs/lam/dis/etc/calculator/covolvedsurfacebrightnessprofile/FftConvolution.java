/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.covolvedsurfacebrightnessprofile;

import java.text.MessageFormat;
import java.util.Map;
import java.util.ResourceBundle;
import lombok.EqualsAndHashCode;
import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.analysis.BivariateRealFunction;
import org.apache.commons.math.complex.Complex;
import org.apache.commons.math.transform.FastFourierTransformer;
import org.apache.log4j.Logger;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.Cacheable;
import org.cnrs.lam.cesam.util.calculator.CachingPolicy;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.Calculator;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.calculator.util.datasets.Dataset;
import org.cnrs.lam.dis.etc.calculator.util.functions.CircularlySymmetricBivariateFunction;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.javatuples.Pair;
import org.javatuples.Quintet;
import org.javatuples.Tuple;
import org.javatuples.Unit;

/**
 * <p>The {@link FftConvolution} class is a calculator for computing functions
 * representing the surface brightness profile convolved with the PSF. It uses
 * the FFT Convolution method.</p>
 * 
 * <p>The configuration of the calculator is a {@link Quintet},
 * with contains the following elements:</p>
 * <ol>
 *   <li>{@link Pair}: The extended source radius and its unit</li>
 *   <li>{@link Calculator}: A calculator for computing the surface brightness profile functions</li>
 *   <li>{@link Calculator}: A calculator for computing the PSF functions</li>
 *   <li>{@link Calculator}: A calculator for computing the PSF size</li>
 *   <li>{@link Integer}: The sampling size</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link java.lang.Double} representing the wavelength for which the calculation
 * is done (expressed in {@latex.inline \\AA}).</p>
 * 
 * <p>The output of the calculator is a {@link org.javatuples.Unit} which contains
 * a {@link BivariateRealFunction} representing the convolved surface brightness
 * profile.</p>
 */
// Caching explenation
// -------------------
// This calculator is set to cache the instances and the results because the
// calculation of the results is done by a time consuming convolution
@Cacheable(CachingPolicy.ALL)
@EqualsAndHashCode(callSuper=false, of={"sourceRadius","surfaceBrightnessProfileCalculator",
    "psfCalculator","psfSizeCalculator","samplingSize"})
public class FftConvolution extends AbstractCalculator<
        Quintet<Pair<Double, String>, 
                Calculator<Tuple, Unit<BivariateRealFunction>>,
                Calculator<Unit<Double>, Unit<BivariateRealFunction>>,
                Calculator<Unit<Double>, Unit<Double>>, Integer>, 
        Unit<Double>, Unit<BivariateRealFunction>> {
    
    private static final Logger logger = Logger.getLogger(FftConvolution.class);
    private static final ResourceBundle validationErrorsBundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/calculator/ValidationErrors");
    private double sourceRadius;
    private Calculator<Tuple, Unit<BivariateRealFunction>> surfaceBrightnessProfileCalculator;
    BivariateRealFunction profileFunction;
    private Calculator<Unit<Double>, Unit<BivariateRealFunction>> psfCalculator;
    private Calculator<Unit<Double>, Unit<Double>> psfSizeCalculator;
    private int samplingSize;
    FastFourierTransformer fft = new FastFourierTransformer();

    /**
     * Validates the given configuration. It requires that the  source radius is 
     * a positive number and that its unit is {@latex.inline $arcsec$}.
     * @param configuration The configuration to validate
     * @throws ConfigurationException If the configuration is invalid
     */
    @Override
    protected void validateConfiguration(Quintet<
            Pair<Double, String>,
            Calculator<Tuple, Unit<BivariateRealFunction>>,
            Calculator<Unit<Double>, Unit<BivariateRealFunction>>,
            Calculator<Unit<Double>, Unit<Double>>,
            Integer> configuration) throws ConfigurationException {
        // Check that the source radius is a positive number
        if (configuration.getValue0().getValue0() <= 0) {
            String message = validationErrorsBundle.getString("EXTENDED_SOURCE_RADIUS_NOT_POSITIVE");
            message = MessageFormat.format(message, configuration.getValue0().getValue0());
            throw new ConfigurationException(message);
        }
        // Check if the source radius is in arcsec
        if (!Units.isArcsec(configuration.getValue0().getValue1())) {
            String message = validationErrorsBundle.getString("EXTENDED_SOURCE_RADIUS_WRONG_UNIT");
            message = MessageFormat.format(message, Units.ARCSEC, configuration.getValue0().getValue1());
            throw new ConfigurationException(message);
        }
        // Check if the sampling size is power of two
        if (!FastFourierTransformer.isPowerOf2(configuration.getValue4())) {
            String message = validationErrorsBundle.getString("PSF_CONVOLUTION_SAMPLING_SIZE_NOT_POWER_OF_TWO");
            message = MessageFormat.format(message, configuration.getValue4());
            throw new ConfigurationException(message);
        }
    }

    /**
     * Initializes the calculator instance with the given configuration.
     * @param configuration the configuration to use for initialization
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(Quintet<
            Pair<Double, String>,
            Calculator<Tuple, Unit<BivariateRealFunction>>,
            Calculator<Unit<Double>, Unit<BivariateRealFunction>>,
            Calculator<Unit<Double>, Unit<Double>>,
            Integer> configuration) throws InitializationException {
        sourceRadius = configuration.getValue0().getValue0();
        surfaceBrightnessProfileCalculator = configuration.getValue1();
        try {
            profileFunction = surfaceBrightnessProfileCalculator.calculate(null).getValue0();
        } catch (CalculationException ex) {
            logger.warn("Failed to calculate the surface brightness profile function", ex);
            throw new InitializationException(ex.getMessage(), ex);
        }
        if (!(profileFunction instanceof CircularlySymmetricBivariateFunction)) {
            throw new InitializationException("Only circularly symmetric surface brightness profiles "
                    + "are currently supported");
        }
        psfCalculator = configuration.getValue2();
        psfSizeCalculator = configuration.getValue3();
        samplingSize = configuration.getValue4();
    }

    @Override
    protected Unit<BivariateRealFunction> performCalculation(Unit<Double> input) throws CalculationException {
        BivariateRealFunction psf = psfCalculator.calculate(input).getValue0();
        if (!(psf instanceof CircularlySymmetricBivariateFunction)) {
            throw new CalculationException("Only circularly symmetric PSF functions "
                    + "are currently supported for convolution with the surface brightness profile");
        }
        Double psfSize = psfSizeCalculator.calculate(input).getValue0();
        double psfRadius = psfSize / 2;
        
        // We calculate the size of the box in which we make the convolution and
        // the step between the sampling points
        int n = samplingSize;
        double max = Math.max(psfRadius, sourceRadius);
        double min = -max;
        double step = (max - min) / n;
        
        // We construct the data representing the psf
        Complex[][] psfData;
        try {
            psfData = sampleFunction(psf, min, step, n);
        } catch (FunctionEvaluationException ex) {
            logger.warn("Failed to calculate the PSF value during convolution", ex);
            throw new CalculationException("Failed to calculate the PSF value during convolution");
        }
        psfData = shiftToCorner(psfData);
        
        // We construct the data representing the profile
        Complex[][] sbpData;
        try {
            sbpData = sampleFunction(profileFunction, min, step, n);
        } catch (FunctionEvaluationException ex) {
            logger.warn("Failed to calculate the surface brightness profile value for convolution", ex);
            throw new CalculationException("Failed to calculate the surface brightness profile value during convolution");
        }
        
        // We convert the psf and the surface brightness profile with FFT
        Complex[][] convertedPsf = (Complex[][]) fft.mdfft(psfData, true);
        Complex[][] convertedSbp = (Complex[][]) fft.mdfft(sbpData, true);
        
        // We multiply the converted psf and SBP to get the converted convolution
        Complex[][] convertedResult = new Complex[2 * n][2 * n];
        for (int i = 0; i < 2 * n; i++) {
            for (int j = 0; j < 2 * n; j++) {
                convertedResult[i][j] = convertedPsf[i][j].multiply(convertedSbp[i][j]).multiply(8 * max * max / n);
            }
        }
        
        // We convert back the convolution
        Complex[][] resultData = (Complex[][]) fft.mdfft(convertedResult, false);
        double[][] resultRealData = new double[2*n][2*n];
        for (int i = 0; i < 2 * n; i++) {
            for (int j = 0; j < 2 * n; j++) {
                resultRealData[i][j] = resultData[i][j].getReal();
            }
        }
        BivariateFunctionDataset convolvedFunction = new BivariateFunctionDataset(resultRealData, 2*min, 2*min, step);
        return new Unit<BivariateRealFunction>(convolvedFunction);
    }

    /**
     * Creates a 2D complex array of size 2*n X 2*n with the values of the given
     * function, which is centered in the center of the array.
     * @param function
     * @param min
     * @param step
     * @param n
     * @return
     * @throws FunctionEvaluationException 
     */
    private Complex[][] sampleFunction(BivariateRealFunction function,
            double min, double step, int n) throws FunctionEvaluationException {
        Complex[][] result = new Complex[2 * n][2 * n];
        for (int i = n / 2; i < 3 * n / 2; i++) {
            for (int j = n / 2; j < 3 * n / 2; j++) {
                double x = min + (i - n / 2) * step;
                double y = min + (j - n / 2) * step;
                result[i][j] = new Complex(function.value(x, y), 0);
            }
        }
        for (int i = 0; i < 2 * n; i++) {
            for (int j = 0; j < 2 * n; j++) {
                if (i < n / 2 || i >= 3 * n / 2 || j < n / 2 || j >= 3 * n / 2) {
                    result[i][j] = new Complex(0, 0);
                }
            }
        }
        return result;
    }

    /**
     * Shifts the center of the array to the corners.
     * @param data
     * @return 
     */
    private Complex[][] shiftToCorner(Complex[][] data) {
        int size = data.length;
        Complex[][] result = new Complex[size][size];
        for (int i = 0; i < size / 2; i++) {
            for (int j = 0; j < size / 2; j++) {
                result[i + size / 2][j + size / 2] = data[i][j];
            }
            for (int j = size / 2; j < size; j++) {
                result[i + size / 2][j - size / 2] = data[i][j];
            }
        }
        for (int i = size / 2; i < size; i++) {
            for (int j = 0; j < size / 2; j++) {
                result[i - size / 2][j + size / 2] = data[i][j];
            }
            for (int j = size / 2; j < size; j++) {
                result[i - size / 2][j - size / 2] = data[i][j];
            }
        }
        return result;
    }

    /**
     * <p>For every calculation the calculator adds in the simulation results
     * the following outputs of the calculator:</p>
     * <ul>
     *   <li>The projection of the convolved surface brightness profile on the plane y=0
     *       in the intermediate important results with code
     *       CONVOLVED_SURFACE_BRIGHTNESS_PROFILE_PROJECTION</li>
     * </ul>
     * @param input
     * @param output 
     */
    @Override
    protected void performForEveryCalculation(Unit<Double> input, Unit<BivariateRealFunction> output) {
        // We know we have a circularly symmetric function so the casting is safe
        CircularlySymmetricBivariateFunction function = (CircularlySymmetricBivariateFunction) output.getValue0();
        // We know that the projection is a Dataset, so the casting is safe
        Dataset projection = (Dataset) function.projectionFunction();
        Map<Double, Double> projectionData = projection.getData();
        
        double lambda = input.getValue0();
        CalculationResults.DoubleDatasetResult projectionResult = 
                new CalculationResults.DoubleDatasetResult("CONVOLVED_SURFACE_BRIGHTNESS_PROFILE_PROJECTION", projectionData, Units.ARCSEC, null);
        ResultsHolder.getResults().addResult("CONVOLVED_SURFACE_BRIGHTNESS_PROFILE_PROJECTION", lambda, projectionResult,
                Units.ANGSTROM, CalculationResults.Level.INTERMEDIATE_IMPORTANT);
    }

    /**
     * <p>For every calculator instantiation the following parts of its configuration
     * are added in the simulation results:</p>
     * <ul>
     *   <li>The extended source effective radius in debug results with code SOURCE_RADIUS</li>
     *   <li>The sampling size of the PSF convolution in debug results with code PSF_CONVOLUTION_SAMPLE_SIZE</li>
     * </ul>
     * @param configuration The configuration of the calculator
     */
    @Override
    protected void performForEveryRetrieval(Quintet<
            Pair<Double, String>,
            Calculator<Tuple, Unit<BivariateRealFunction>>,
            Calculator<Unit<Double>, Unit<BivariateRealFunction>>,
            Calculator<Unit<Double>, Unit<Double>>,
            Integer> configuration) {
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("SOURCE_RADIUS",
                configuration.getValue0().getValue0(), configuration.getValue0().getValue1()), CalculationResults.Level.DEBUG);
        ResultsHolder.getResults().addResult(new CalculationResults.DoubleValueResult("PSF_CONVOLUTION_SAMPLE_SIZE",
                configuration.getValue4(), null), CalculationResults.Level.DEBUG);
    }
}
