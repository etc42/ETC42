/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.calculator.simulatedbackgroundnoise;

import lombok.EqualsAndHashCode;
import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.apache.log4j.Logger;
import org.cnrs.lam.cesam.util.calculator.AbstractCalculator;
import org.cnrs.lam.cesam.util.calculator.Cacheable;
import org.cnrs.lam.cesam.util.calculator.CachingPolicy;
import org.cnrs.lam.cesam.util.calculator.CalculationException;
import org.cnrs.lam.cesam.util.calculator.Calculator;
import org.cnrs.lam.cesam.util.calculator.ConfigurationException;
import org.cnrs.lam.cesam.util.calculator.InitializationException;
import org.cnrs.lam.dis.etc.calculator.ResultsHolder;
import org.cnrs.lam.dis.etc.calculator.util.Constants;
import org.cnrs.lam.dis.etc.calculator.util.FunctionToDatasetResultConverter;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.calculator.util.functionmultiplication.FunctionBoundsTooSmallException;
import org.cnrs.lam.dis.etc.calculator.util.functionmultiplication.FunctionMultiplicationTool;
import org.cnrs.lam.dis.etc.calculator.util.functions.AveragingKernelConvolution;
import org.cnrs.lam.dis.etc.calculator.util.functions.BoundedUnivariateFunction;
import org.cnrs.lam.dis.etc.calculator.util.functions.CalculatorWrapperFunction;
import org.cnrs.lam.dis.etc.calculator.util.functions.PolynomialFunction;
import org.cnrs.lam.dis.etc.configuration.ConfigFactory;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.DoubleDatasetResult;
import org.javatuples.Pair;
import org.javatuples.Sextet;
import org.javatuples.Tuple;
import org.javatuples.Unit;

/**
 * <p>The {@link Flux} class is a calculator for computing the per Angstrom
 * number of electrons counted by the instrument from the simulated background.
 * In this case the background noise is calculated based on the flux of the background
 * using the equation:</p>
 * {@latex.inline $
 * BN_{(\\lambda)} = \\phi_{(\\lambda)} * \\varepsilon_{(\\lambda)} *
 * Filter_{(\\lambda)} * A_{tel} * A_{sky(\\lambda)} * \\frac{\\lambda}{h*c}
 * $}
 * <p>where</p>
 * <ul>
 *   <li>{@latex.inline $BN_{(\\lambda)}$}: is the calculated background expressed in
 *       {@latex.inline $e_-/s/$\\AA}</li>
 *   <li>{@latex.inline $\\lambda$}: is the wavelength</li>
 *   <li>{@latex.inline $\\phi_{(\\lambda)}$}: is the simulated background flux</li>
 *   <li>{@latex.inline $\\varepsilon_{(\\lambda)}$}: is the total system efficiency
 *       excluding the filter</li>
 *   <li>{@latex.inline $Filter_{(\\lambda)}$}: is the filter response</li>
 *   <li>{@latex.inline $A_{tel}$}: is the effective area of the primary mirror</li>
 *   <li>{@latex.inline $A_{sky(\\lambda)}$}: is the area of the sky contributing to the noise</li>
 *   <li>{@latex.inline $h$}: is the Planck constant expressed in {@latex.inline $erg*s$}</li>
 *   <li>{@latex.inline $c$}: is the speed of light expressed in {@latex.inline \AA$/sec$}</li>
 * </ul>
 * 
 * <p>The configuration of the calculator is a {@link Sextet},
 * witch contains the following elements:</p>
 * <ol>
 *   <li>{@link Calculator}: A calculator computing the size of the convolution
 *       kernel in the spectral direction</li>
 *   <li>{@link Calculator}: A calculator computing the simulated background flux</li>
 *   <li>{@link Calculator}: A calculator computing the total system efficiency</li>
 *   <li>{@link Calculator}: A calculator computing the filter response</li>
 *   <li>{@link Calculator}: A calculator computing the telescope area</li>
 *   <li>{@link Calculator}: A calculator computing the sky area</li>
 * </ol>
 * 
 * <p>The input of the calculator is a {@link org.javatuples.Pair} which contains
 * two {@link Double} values representing the wavelength range (expressed in
 * {@latex.inline \\AA}) for which the returned calculators perform the calculation.</p>
 * 
 * <p>The output of the calculator is a {@link Pair} which contains two 
 * {@link BoundedUnivariateFunction} for computing the background in the central 
 * pixel and the total spatial binning accordingly.</p>
 */
// Caching explenation
// -------------------
// This calculator is set to cache the instances and the results because the
// calculation of the results contains time consuming integrations
@Cacheable(CachingPolicy.ALL)
@EqualsAndHashCode(callSuper=false, of={"convolutionKernelSizeCalculator","backgroundFluxCalculator",
    "systemEfficiencyCalculator","filterResponseCalculator", "telescopeAreaCalculator","skyAreaCalculator"})
public class Flux extends AbstractCalculator<
        Sextet<Calculator<Unit<Double>, Unit<Double>>,
               Calculator<Tuple, Unit<UnivariateRealFunction>>,
               Calculator<Tuple, Unit<BoundedUnivariateFunction>>,
               Calculator<Tuple, Unit<BoundedUnivariateFunction>>,
               Calculator<Tuple, Unit<Double>>,
               Calculator<Tuple, Pair<UnivariateRealFunction, UnivariateRealFunction>>>,
        Pair<Double, Double>,
        Pair<UnivariateRealFunction, UnivariateRealFunction>> {
    
    private final static Logger logger = Logger.getLogger(Flux.class);
    
    // The calculators which are used by lombok for the equals method
    Calculator<Unit<Double>, Unit<Double>> convolutionKernelSizeCalculator;
    Calculator<Tuple, Unit<UnivariateRealFunction>> backgroundFluxCalculator;
    Calculator<Tuple, Unit<BoundedUnivariateFunction>> systemEfficiencyCalculator;
    Calculator<Tuple, Unit<BoundedUnivariateFunction>> filterResponseCalculator;
    Calculator<Tuple, Unit<Double>> telescopeAreaCalculator;
    Calculator<Tuple, Pair<UnivariateRealFunction, UnivariateRealFunction>> skyAreaCalculator;
    // The functions retrieved during initialization and used for the calculation
    AveragingKernelConvolution convolvedfluxFunction;
    BoundedUnivariateFunction systemEfficiencyFunction;
    BoundedUnivariateFunction filterResponseFunction;
    UnivariateRealFunction centralPixelSkyAreaFunction;
    UnivariateRealFunction totalSkyAreaFunction;
    PolynomialFunction polynomialFunction;

    /**
     * Does nothing as the calculator accepts as configuration only other calculators.
     * @param configuration
     * @throws ConfigurationException 
     */
    @Override
    protected void validateConfiguration(Sextet<
            Calculator<Unit<Double>, Unit<Double>>,
            Calculator<Tuple, Unit<UnivariateRealFunction>>,
            Calculator<Tuple, Unit<BoundedUnivariateFunction>>,
            Calculator<Tuple, Unit<BoundedUnivariateFunction>>,
            Calculator<Tuple, Unit<Double>>,
            Calculator<Tuple, Pair<UnivariateRealFunction, UnivariateRealFunction>>> configuration) throws ConfigurationException {
        // Do nothing here
    }

    /**
     * Initializes the calculator instance with the given configuration.
     * @param configuration the configuration to use for initialization
     * @throws InitializationException if the initialization fails
     */
    @Override
    protected void initialize(Sextet<
            Calculator<Unit<Double>, Unit<Double>>,
            Calculator<Tuple, Unit<UnivariateRealFunction>>,
            Calculator<Tuple, Unit<BoundedUnivariateFunction>>,
            Calculator<Tuple, Unit<BoundedUnivariateFunction>>,
            Calculator<Tuple, Unit<Double>>,
            Calculator<Tuple, Pair<UnivariateRealFunction, UnivariateRealFunction>>> configuration) throws InitializationException {
        // We get the convolution kernel calculator and we create a function to use for the convolution
        convolutionKernelSizeCalculator = configuration.getValue0();
        UnivariateRealFunction convolutionKernelSizeFunction = new CalculatorWrapperFunction(convolutionKernelSizeCalculator);
        // We retrieve the flux function
        backgroundFluxCalculator = configuration.getValue1();
        UnivariateRealFunction fluxFunction = null;
        try {
            fluxFunction = backgroundFluxCalculator.calculate(null).getValue0();
        } catch (CalculationException ex) {
            logger.error("Failed to calculate the flux flunction", ex);
            throw new InitializationException(ex.getMessage(), ex);
        }
        // We create a function which performs the convolution
        convolvedfluxFunction = new AveragingKernelConvolution(fluxFunction, convolutionKernelSizeFunction);
        // We retrieve the system efficiency function
        systemEfficiencyCalculator = configuration.getValue2();
        try {
            systemEfficiencyFunction = systemEfficiencyCalculator.calculate(null).getValue0();
        } catch (CalculationException ex) {
            logger.error("Failed to calculate the system efficiency flunction", ex);
            throw new InitializationException(ex.getMessage(), ex);
        }
        // We retrieve the filter response function
        filterResponseCalculator = configuration.getValue3();
        try {
            filterResponseFunction = filterResponseCalculator.calculate(null).getValue0();
        } catch (CalculationException ex) {
            logger.error("Failed to calculate the filter response flunction", ex);
            throw new InitializationException(ex.getMessage(), ex);
        }
        // We calculate the telescoe area
        telescopeAreaCalculator = configuration.getValue4();
        double telescopeArea = 0;
        try {
            telescopeArea = telescopeAreaCalculator.calculate(null).getValue0();
        } catch (CalculationException ex) {
            logger.error("Failed to calculate the telescope area", ex);
            throw new InitializationException(ex.getMessage(), ex);
        }
        // We create the polynomial (Atel/h*c)*lambda
        double factor = telescopeArea / (Constants.SPEED_OF_LIGHT * Constants.PLANK);
        polynomialFunction = new PolynomialFunction(0, factor);
        //We calculate the sky area calculators
        skyAreaCalculator = configuration.getValue5();
        Pair<UnivariateRealFunction, UnivariateRealFunction> skyAreaResult = null;
        try {
            skyAreaResult = skyAreaCalculator.calculate(null);
        } catch (CalculationException ex) {
            logger.error("Failed to calculate the sky area", ex);
            throw new InitializationException(ex.getMessage(), ex);
        }
        centralPixelSkyAreaFunction = skyAreaResult.getValue0();
        totalSkyAreaFunction = skyAreaResult.getValue1();
    }

    /**
     * Returns a {@link Pair} containing the functions to calculate the background
     * in the central pixel and the total spatial binning accordingly.
     * @param input The wavelength range expressed in Angstrom
     * @return A {@link Pair} containing the central pixel and total background functions
     * @throws CalculationException if there is any error during the calculation
     */
    @Override
    protected Pair<UnivariateRealFunction, UnivariateRealFunction>
            performCalculation(Pair<Double, Double> input) throws CalculationException {
        double min = input.getValue0();
        double max = input.getValue1();
        
        BoundedUnivariateFunction centralPixelFunction = null;
        BoundedUnivariateFunction totalFunction = null;
        try {
            if (ConfigFactory.getConfig().getCentralPixelFlag()) {
                centralPixelFunction = FunctionMultiplicationTool.multiply(min, max,
                        convolvedfluxFunction, systemEfficiencyFunction, filterResponseFunction,
                        polynomialFunction);
                // We multiply separately with the sky area and we don't use
                // the optimized way. This is because the sky area has jumps
                // (when the pixel number increases and the optimization methods
                // are not working correctly
                centralPixelFunction = FunctionMultiplicationTool.multiply(centralPixelFunction, centralPixelSkyAreaFunction);
            }
            totalFunction = FunctionMultiplicationTool.multiply(min, max,
                    convolvedfluxFunction, systemEfficiencyFunction, filterResponseFunction,
                    polynomialFunction);
            // We multiply separately with the sky area and we don't use
            // the optimized way. This is because the sky area has jumps
            // (when the pixel number increases and the optimization methods
            // are not working correctly
            totalFunction = FunctionMultiplicationTool.multiplyUnoptimized(totalFunction, totalSkyAreaFunction);
        } catch (FunctionBoundsTooSmallException ex) {
            String functionName = null;
            switch (ex.getFunctionIndex()) {
                case 0:
                    functionName = "convolved background flux";
                    break;
                case 1:
                    functionName = "system efficiency";
                    break;
                case 2:
                    functionName = "filter response";
                    break;
                case 3:
                    functionName = "sky area";
                    break;
            }
            String message = (functionName == null) ? ex.getMessage()
                    : "The " + functionName + " cannot be calculated in the range ["
                      + min + "," + max + "]";
            logger.error(message, ex);
            throw new CalculationException(message);
        } catch (FunctionEvaluationException ex) {
            logger.error(ex.getMessage(), ex);
            throw new CalculationException(ex.getMessage());
        }
        
        return new Pair<UnivariateRealFunction, UnivariateRealFunction>(centralPixelFunction, totalFunction);
    }

    /**
     * <p>For every calculation the calculator adds in the simulation results
     * the following outputs of the calculator:</p>
     * <ul>
     *   <li>The central pixel simulated background noise in intermediate important results
     *       with code CENTRAL_PIXEL_SIMULATED_BACKGROUND_NOISE</li>
     *   <li>The total simulated background noise in intermediate important results
     *       with code SIMULATED_BACKGROUND_NOISE</li>
     * </ul>
     * @param input
     * @param output 
     */
    @Override
    protected void performForEveryCalculation(Pair<Double, Double> input, 
                Pair<UnivariateRealFunction, UnivariateRealFunction> output) {
        double min = input.getValue0();
        double max = input.getValue1();
        if (ConfigFactory.getConfig().getCentralPixelFlag()) {
            DoubleDatasetResult cpResult = FunctionToDatasetResultConverter.convert(min, max, output.getValue0(),
                    "CENTRAL_PIXEL_SIMULATED_BACKGROUND_NOISE", Units.ANGSTROM, Units.getElectronsPerSecPerAngstrom());
            ResultsHolder.getResults().addResult(cpResult, CalculationResults.Level.INTERMEDIATE_IMPORTANT);
        }
        DoubleDatasetResult totalResult = FunctionToDatasetResultConverter.convert(min, max, output.getValue1(), 
                "SIMULATED_BACKGROUND_NOISE", Units.ANGSTROM, Units.getElectronsPerSecPerAngstrom());
        ResultsHolder.getResults().addResult(totalResult, CalculationResults.Level.INTERMEDIATE_IMPORTANT);
    }
    
}
