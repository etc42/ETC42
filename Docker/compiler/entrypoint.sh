#!/bin/bash

set -e

svn co  --username=$SVN_USER --password=$SVN_PASS https://svn.lam.fr/repos/etc/trunk etc42
svn co  --username=$SVN_USER --password=$SVN_PASS https://svn.lam.fr/repos/volibrary/Java/Vo\ drag\ and\ drop/trunk volibrary
svn co  --username=$SVN_USER --password=$SVN_PASS https://svn.lam.fr/repos/javatools/CeSAMUtilsJ/trunk CeSAMUtilsJ
svn co  --username=$SVN_USER --password=$SVN_PASS https://svn.lam.fr/repos/javatools/CeSAMSamp/trunk CeSAMSamp

echo "CeSAMSamp/ExternalLibraries"
cd CeSAMSamp/ExternalLibraries/
chmod +x installInRepository
./installInRepository
cd ../..

echo "etc42/ExternalLibraries/"
cd etc42/ExternalLibraries/
chmod +x installInRepository
./installInRepository
cd ../..

echo "============ > CeSAMSamp"
cd CeSAMSamp/
mvn install -DskipTests
cd ..

echo "============ > CeSAMUtilsJ"
cd CeSAMUtilsJ/
mvn install -DskipTests
cd ..

echo "============ > Volibrary"
cd volibrary/
mvn install -DskipTests
cd ..

echo "============ > Etc42"
cd etc42/

echo "====== > Install "
mvn install -DskipTests 
echo "====== > Clean "
mvn clean -DskipTests
echo "====== > Package "
mvn package -DskipTests
cd ..

mkdir target -p
cp etc42/ETC-Jar/target/*.jar target/ -rf
cp etc42/ETC-Documentation/target/*.pdf target/ -rf
cp etc42/ETC-WebStart/target/jnlp target/ -rf

rm CeSAMSamp -Rf
rm CeSAMUtilsJ -Rf
rm volibrary -Rf
rm etc42 -Rf
#cp /*.jar /etc42/