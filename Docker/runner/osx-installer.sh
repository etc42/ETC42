#/bin/bash
set -e

if ! type "brew" > /dev/null; then
	echo "Installing Brew ..."
	ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi

echo "Booting boot2docker"
eval "$(docker-machine env default)"

chmod +x create-images.sh
./create-images.sh

if [ ! -z "$1" ]; then
	echo "Custom path existing"
	if [ -d "$1" ]; then
		ETC_HOME_DIR=$1
	else
		echo "$1 isn't a valid directory"
		exit
	fi
fi

PID=$(ps -A | grep -m1 XQuartz | grep -v "grep -m1 XQuartz" | awk '{print $1}')
if [ ! -z "$PID" ]; then
	echo "Killing XQuartz on running ..."
	sudo kill -9 $PID
fi

PID=$(ps -A | grep -m1 socat | grep -v "grep -m1 socat" | awk '{print $1}')
if [ ! -z "$PID" ]; then
	echo "Killing Socat on running ..."
	sudo kill -9 $PID
fi

	
echo "Installing Cask for Brew ..."
brew install caskroom/cask/brew-cask

echo "Installing Socat ..."
brew install socat
 
echo "Installing XQuartz ..."
brew cask install xquartz

echo "Launching XQuartz"
open -a XQuartz

echo "Launching Socat"
socat TCP-LISTEN:6000,reuseaddr,fork UNIX-CLIENT:\"$DISPLAY\" &

LOCAL_IP=$(ifconfig vboxnet0 | grep 'inet' | cut -d" " -f2)

echo "===================================================================="
echo "In X11 preferences in XQuartz, in the security tab, check both boxes"
echo "===================================================================="

echo "Launching ETC42 image (Docker)"

if [ ! -z "$ETC_HOME_DIR" ]; then
	echo "ETC42 data will be saved into $ETC_HOME_DIR"
	docker run -e DISPLAY=$LOCAL_IP:0 -v $ETC_HOME_DIR:/ETC42/.etc/ -v /:/MY_HOST lam_runner_etc42:latest
else
	echo "Data won't be persisted"
	docker run -e DISPLAY=$LOCAL_IP:0 -v /:/MY_HOST lam_runner_etc42:latest
fi