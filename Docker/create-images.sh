#/bin/bash

docker build --file="runner/Dockerfile" --tag="lam_runner_etc42:latest" runner/.
docker build --file="compiler/Dockerfile" --tag="lam_compiler_etc42:latest" compiler/.