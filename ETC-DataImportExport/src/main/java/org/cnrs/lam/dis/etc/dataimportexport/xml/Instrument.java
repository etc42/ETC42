/*
 * XML Type:  Instrument
 * Namespace: 
 * Java type: org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument
 *
 * Automatically generated - do not modify.
 */
package org.cnrs.lam.dis.etc.dataimportexport.xml;


/**
 * An XML Instrument(@).
 *
 * This is a complex type.
 */
public interface Instrument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Instrument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sCF658226FCE8387FD3C82F8A9EBA922F").resolveHandle("instrument1b30type");
    
    /**
     * Gets the "info" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo getInfo();
    
    /**
     * Sets the "info" element
     */
    void setInfo(org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo info);
    
    /**
     * Appends and returns a new empty "info" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo addNewInfo();
    
    /**
     * Gets the "dark" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getDark();
    
    /**
     * Sets the "dark" element
     */
    void setDark(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit dark);
    
    /**
     * Appends and returns a new empty "dark" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewDark();
    
    /**
     * Gets the "diameter" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getDiameter();
    
    /**
     * Sets the "diameter" element
     */
    void setDiameter(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit diameter);
    
    /**
     * Appends and returns a new empty "diameter" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewDiameter();
    
    /**
     * Gets the "fiberDiameter" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getFiberDiameter();
    
    /**
     * Sets the "fiberDiameter" element
     */
    void setFiberDiameter(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit fiberDiameter);
    
    /**
     * Appends and returns a new empty "fiberDiameter" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewFiberDiameter();
    
    /**
     * Gets the "filterTransmission" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.Graph getFilterTransmission();
    
    /**
     * Sets the "filterTransmission" element
     */
    void setFilterTransmission(org.cnrs.lam.dis.etc.dataimportexport.xml.Graph filterTransmission);
    
    /**
     * Appends and returns a new empty "filterTransmission" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.Graph addNewFilterTransmission();
    
    /**
     * Gets the "fwhm" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.Graph getFwhm();
    
    /**
     * Sets the "fwhm" element
     */
    void setFwhm(org.cnrs.lam.dis.etc.dataimportexport.xml.Graph fwhm);
    
    /**
     * Appends and returns a new empty "fwhm" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.Graph addNewFwhm();
    
    /**
     * Gets the "instrumentType" element
     */
    java.lang.String getInstrumentType();
    
    /**
     * Gets (as xml) the "instrumentType" element
     */
    org.apache.xmlbeans.XmlString xgetInstrumentType();
    
    /**
     * Sets the "instrumentType" element
     */
    void setInstrumentType(java.lang.String instrumentType);
    
    /**
     * Sets (as xml) the "instrumentType" element
     */
    void xsetInstrumentType(org.apache.xmlbeans.XmlString instrumentType);
    
    /**
     * Gets the "nSlit" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.IntUnit getNSlit();
    
    /**
     * Sets the "nSlit" element
     */
    void setNSlit(org.cnrs.lam.dis.etc.dataimportexport.xml.IntUnit nSlit);
    
    /**
     * Appends and returns a new empty "nSlit" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.IntUnit addNewNSlit();
    
    /**
     * Gets the "obstruction" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getObstruction();
    
    /**
     * Sets the "obstruction" element
     */
    void setObstruction(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit obstruction);
    
    /**
     * Appends and returns a new empty "obstruction" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewObstruction();
    
    /**
     * Gets the "pixelScale" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getPixelScale();
    
    /**
     * Sets the "pixelScale" element
     */
    void setPixelScale(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit pixelScale);
    
    /**
     * Appends and returns a new empty "pixelScale" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewPixelScale();
    
    /**
     * Gets the "psfType" element
     */
    java.lang.String getPsfType();
    
    /**
     * Gets (as xml) the "psfType" element
     */
    org.apache.xmlbeans.XmlString xgetPsfType();
    
    /**
     * Sets the "psfType" element
     */
    void setPsfType(java.lang.String psfType);
    
    /**
     * Sets (as xml) the "psfType" element
     */
    void xsetPsfType(org.apache.xmlbeans.XmlString psfType);
    
    /**
     * Gets the "rangeMax" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getRangeMax();
    
    /**
     * Sets the "rangeMax" element
     */
    void setRangeMax(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit rangeMax);
    
    /**
     * Appends and returns a new empty "rangeMax" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewRangeMax();
    
    /**
     * Gets the "rangeMin" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getRangeMin();
    
    /**
     * Sets the "rangeMin" element
     */
    void setRangeMin(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit rangeMin);
    
    /**
     * Appends and returns a new empty "rangeMin" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewRangeMin();
    
    /**
     * Gets the "readout" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getReadout();
    
    /**
     * Sets the "readout" element
     */
    void setReadout(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit readout);
    
    /**
     * Appends and returns a new empty "readout" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewReadout();
    
    /**
     * Gets the "slitLength" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getSlitLength();
    
    /**
     * Sets the "slitLength" element
     */
    void setSlitLength(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit slitLength);
    
    /**
     * Appends and returns a new empty "slitLength" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewSlitLength();
    
    /**
     * Gets the "slitWidth" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getSlitWidth();
    
    /**
     * Sets the "slitWidth" element
     */
    void setSlitWidth(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit slitWidth);
    
    /**
     * Appends and returns a new empty "slitWidth" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewSlitWidth();
    
    /**
     * Gets the "spectralResolution" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.Graph getSpectralResolution();
    
    /**
     * Sets the "spectralResolution" element
     */
    void setSpectralResolution(org.cnrs.lam.dis.etc.dataimportexport.xml.Graph spectralResolution);
    
    /**
     * Appends and returns a new empty "spectralResolution" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.Graph addNewSpectralResolution();
    
    /**
     * Gets the "spectrographType" element
     */
    java.lang.String getSpectrographType();
    
    /**
     * Gets (as xml) the "spectrographType" element
     */
    org.apache.xmlbeans.XmlString xgetSpectrographType();
    
    /**
     * Sets the "spectrographType" element
     */
    void setSpectrographType(java.lang.String spectrographType);
    
    /**
     * Sets (as xml) the "spectrographType" element
     */
    void xsetSpectrographType(org.apache.xmlbeans.XmlString spectrographType);
    
    /**
     * Gets the "transmission" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.Graph getTransmission();
    
    /**
     * Sets the "transmission" element
     */
    void setTransmission(org.cnrs.lam.dis.etc.dataimportexport.xml.Graph transmission);
    
    /**
     * Appends and returns a new empty "transmission" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.Graph addNewTransmission();
    
    /**
     * Gets the "strehlRatio" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getStrehlRatio();
    
    /**
     * Sets the "strehlRatio" element
     */
    void setStrehlRatio(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit strehlRatio);
    
    /**
     * Appends and returns a new empty "strehlRatio" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewStrehlRatio();
    
    /**
     * Gets the "refWavelength" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getRefWavelength();
    
    /**
     * Sets the "refWavelength" element
     */
    void setRefWavelength(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit refWavelength);
    
    /**
     * Appends and returns a new empty "refWavelength" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewRefWavelength();
    
    /**
     * Gets the "deltaLambdaPerPixel" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getDeltaLambdaPerPixel();
    
    /**
     * Sets the "deltaLambdaPerPixel" element
     */
    void setDeltaLambdaPerPixel(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit deltaLambdaPerPixel);
    
    /**
     * Appends and returns a new empty "deltaLambdaPerPixel" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewDeltaLambdaPerPixel();
    
    /**
     * Gets the "spectralResolutionType" element
     */
    java.lang.String getSpectralResolutionType();
    
    /**
     * Gets (as xml) the "spectralResolutionType" element
     */
    org.apache.xmlbeans.XmlString xgetSpectralResolutionType();
    
    /**
     * Sets the "spectralResolutionType" element
     */
    void setSpectralResolutionType(java.lang.String spectralResolutionType);
    
    /**
     * Sets (as xml) the "spectralResolutionType" element
     */
    void xsetSpectralResolutionType(org.apache.xmlbeans.XmlString spectralResolutionType);
    
    /**
     * Gets the "psfDoubleGaussianMultiplier" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getPsfDoubleGaussianMultiplier();
    
    /**
     * Sets the "psfDoubleGaussianMultiplier" element
     */
    void setPsfDoubleGaussianMultiplier(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit psfDoubleGaussianMultiplier);
    
    /**
     * Appends and returns a new empty "psfDoubleGaussianMultiplier" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewPsfDoubleGaussianMultiplier();
    
    /**
     * Gets the "psfDoubleGaussianFwhm1" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.Graph getPsfDoubleGaussianFwhm1();
    
    /**
     * Sets the "psfDoubleGaussianFwhm1" element
     */
    void setPsfDoubleGaussianFwhm1(org.cnrs.lam.dis.etc.dataimportexport.xml.Graph psfDoubleGaussianFwhm1);
    
    /**
     * Appends and returns a new empty "psfDoubleGaussianFwhm1" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.Graph addNewPsfDoubleGaussianFwhm1();
    
    /**
     * Gets the "psfDoubleGaussianFwhm2" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.Graph getPsfDoubleGaussianFwhm2();
    
    /**
     * Sets the "psfDoubleGaussianFwhm2" element
     */
    void setPsfDoubleGaussianFwhm2(org.cnrs.lam.dis.etc.dataimportexport.xml.Graph psfDoubleGaussianFwhm2);
    
    /**
     * Appends and returns a new empty "psfDoubleGaussianFwhm2" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.Graph addNewPsfDoubleGaussianFwhm2();
    
    /**
     * Gets the "psfSizeType" element
     */
    java.lang.String getPsfSizeType();
    
    /**
     * Gets (as xml) the "psfSizeType" element
     */
    org.apache.xmlbeans.XmlString xgetPsfSizeType();
    
    /**
     * Sets the "psfSizeType" element
     */
    void setPsfSizeType(java.lang.String psfSizeType);
    
    /**
     * Sets (as xml) the "psfSizeType" element
     */
    void xsetPsfSizeType(org.apache.xmlbeans.XmlString psfSizeType);
    
    /**
     * Gets the "fixedPsfSize" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getFixedPsfSize();
    
    /**
     * Sets the "fixedPsfSize" element
     */
    void setFixedPsfSize(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit fixedPsfSize);
    
    /**
     * Appends and returns a new empty "fixedPsfSize" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewFixedPsfSize();
    
    /**
     * Gets the "psfSizeFunction" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.Graph getPsfSizeFunction();
    
    /**
     * Sets the "psfSizeFunction" element
     */
    void setPsfSizeFunction(org.cnrs.lam.dis.etc.dataimportexport.xml.Graph psfSizeFunction);
    
    /**
     * Appends and returns a new empty "psfSizeFunction" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.Graph addNewPsfSizeFunction();
    
    /**
     * Gets the "psfSizePercentage" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getPsfSizePercentage();
    
    /**
     * Sets the "psfSizePercentage" element
     */
    void setPsfSizePercentage(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit psfSizePercentage);
    
    /**
     * Appends and returns a new empty "psfSizePercentage" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewPsfSizePercentage();
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument newInstance() {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
