/*
 * An XML document type.
 * Localname: observingParameters
 * Namespace: 
 * Java type: org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParametersDocument
 *
 * Automatically generated - do not modify.
 */
package org.cnrs.lam.dis.etc.dataimportexport.xml.impl;
/**
 * A document containing one observingParameters(@) element.
 *
 * This is a complex type.
 */
public class ObservingParametersDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParametersDocument
{
    private static final long serialVersionUID = 1L;
    
    public ObservingParametersDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName OBSERVINGPARAMETERS$0 = 
        new javax.xml.namespace.QName("", "observingParameters");
    
    
    /**
     * Gets the "observingParameters" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters getObservingParameters()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters)get_store().find_element_user(OBSERVINGPARAMETERS$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "observingParameters" element
     */
    public void setObservingParameters(org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters observingParameters)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters)get_store().find_element_user(OBSERVINGPARAMETERS$0, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters)get_store().add_element_user(OBSERVINGPARAMETERS$0);
            }
            target.set(observingParameters);
        }
    }
    
    /**
     * Appends and returns a new empty "observingParameters" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters addNewObservingParameters()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters)get_store().add_element_user(OBSERVINGPARAMETERS$0);
            return target;
        }
    }
}
