/*
 * XML Type:  Graph
 * Namespace: 
 * Java type: org.cnrs.lam.dis.etc.dataimportexport.xml.Graph
 *
 * Automatically generated - do not modify.
 */
package org.cnrs.lam.dis.etc.dataimportexport.xml.impl;
/**
 * An XML Graph(@).
 *
 * This is a complex type.
 */
public class GraphImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements org.cnrs.lam.dis.etc.dataimportexport.xml.Graph
{
    private static final long serialVersionUID = 1L;
    
    public GraphImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName SAMPLE$0 = 
        new javax.xml.namespace.QName("", "sample");
    private static final javax.xml.namespace.QName NAME$2 = 
        new javax.xml.namespace.QName("", "name");
    private static final javax.xml.namespace.QName UNITX$4 = 
        new javax.xml.namespace.QName("", "unitX");
    private static final javax.xml.namespace.QName UNITY$6 = 
        new javax.xml.namespace.QName("", "unitY");
    private static final javax.xml.namespace.QName OPTION$8 = 
        new javax.xml.namespace.QName("", "option");
    private static final javax.xml.namespace.QName DATATYPE$10 = 
        new javax.xml.namespace.QName("", "dataType");
    
    
    /**
     * Gets array of all "sample" elements
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.Sample[] getSampleArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(SAMPLE$0, targetList);
            org.cnrs.lam.dis.etc.dataimportexport.xml.Sample[] result = new org.cnrs.lam.dis.etc.dataimportexport.xml.Sample[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "sample" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.Sample getSampleArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Sample target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Sample)get_store().find_element_user(SAMPLE$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "sample" element
     */
    public int sizeOfSampleArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(SAMPLE$0);
        }
    }
    
    /**
     * Sets array of all "sample" element
     */
    public void setSampleArray(org.cnrs.lam.dis.etc.dataimportexport.xml.Sample[] sampleArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(sampleArray, SAMPLE$0);
        }
    }
    
    /**
     * Sets ith "sample" element
     */
    public void setSampleArray(int i, org.cnrs.lam.dis.etc.dataimportexport.xml.Sample sample)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Sample target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Sample)get_store().find_element_user(SAMPLE$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(sample);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "sample" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.Sample insertNewSample(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Sample target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Sample)get_store().insert_element_user(SAMPLE$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "sample" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.Sample addNewSample()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Sample target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Sample)get_store().add_element_user(SAMPLE$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "sample" element
     */
    public void removeSample(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(SAMPLE$0, i);
        }
    }
    
    /**
     * Gets the "name" attribute
     */
    public java.lang.String getName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(NAME$2);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "name" attribute
     */
    public org.apache.xmlbeans.XmlString xgetName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_attribute_user(NAME$2);
            return target;
        }
    }
    
    /**
     * Sets the "name" attribute
     */
    public void setName(java.lang.String name)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(NAME$2);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(NAME$2);
            }
            target.setStringValue(name);
        }
    }
    
    /**
     * Sets (as xml) the "name" attribute
     */
    public void xsetName(org.apache.xmlbeans.XmlString name)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_attribute_user(NAME$2);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_attribute_user(NAME$2);
            }
            target.set(name);
        }
    }
    
    /**
     * Gets the "unitX" attribute
     */
    public java.lang.String getUnitX()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(UNITX$4);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "unitX" attribute
     */
    public org.apache.xmlbeans.XmlString xgetUnitX()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_attribute_user(UNITX$4);
            return target;
        }
    }
    
    /**
     * Sets the "unitX" attribute
     */
    public void setUnitX(java.lang.String unitX)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(UNITX$4);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(UNITX$4);
            }
            target.setStringValue(unitX);
        }
    }
    
    /**
     * Sets (as xml) the "unitX" attribute
     */
    public void xsetUnitX(org.apache.xmlbeans.XmlString unitX)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_attribute_user(UNITX$4);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_attribute_user(UNITX$4);
            }
            target.set(unitX);
        }
    }
    
    /**
     * Gets the "unitY" attribute
     */
    public java.lang.String getUnitY()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(UNITY$6);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "unitY" attribute
     */
    public org.apache.xmlbeans.XmlString xgetUnitY()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_attribute_user(UNITY$6);
            return target;
        }
    }
    
    /**
     * Sets the "unitY" attribute
     */
    public void setUnitY(java.lang.String unitY)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(UNITY$6);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(UNITY$6);
            }
            target.setStringValue(unitY);
        }
    }
    
    /**
     * Sets (as xml) the "unitY" attribute
     */
    public void xsetUnitY(org.apache.xmlbeans.XmlString unitY)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_attribute_user(UNITY$6);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_attribute_user(UNITY$6);
            }
            target.set(unitY);
        }
    }
    
    /**
     * Gets the "option" attribute
     */
    public java.lang.String getOption()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(OPTION$8);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "option" attribute
     */
    public org.apache.xmlbeans.XmlString xgetOption()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_attribute_user(OPTION$8);
            return target;
        }
    }
    
    /**
     * True if has "option" attribute
     */
    public boolean isSetOption()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().find_attribute_user(OPTION$8) != null;
        }
    }
    
    /**
     * Sets the "option" attribute
     */
    public void setOption(java.lang.String option)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(OPTION$8);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(OPTION$8);
            }
            target.setStringValue(option);
        }
    }
    
    /**
     * Sets (as xml) the "option" attribute
     */
    public void xsetOption(org.apache.xmlbeans.XmlString option)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_attribute_user(OPTION$8);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_attribute_user(OPTION$8);
            }
            target.set(option);
        }
    }
    
    /**
     * Unsets the "option" attribute
     */
    public void unsetOption()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_attribute(OPTION$8);
        }
    }
    
    /**
     * Gets the "dataType" attribute
     */
    public java.lang.String getDataType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(DATATYPE$10);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "dataType" attribute
     */
    public org.apache.xmlbeans.XmlString xgetDataType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_attribute_user(DATATYPE$10);
            return target;
        }
    }
    
    /**
     * True if has "dataType" attribute
     */
    public boolean isSetDataType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().find_attribute_user(DATATYPE$10) != null;
        }
    }
    
    /**
     * Sets the "dataType" attribute
     */
    public void setDataType(java.lang.String dataType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(DATATYPE$10);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(DATATYPE$10);
            }
            target.setStringValue(dataType);
        }
    }
    
    /**
     * Sets (as xml) the "dataType" attribute
     */
    public void xsetDataType(org.apache.xmlbeans.XmlString dataType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_attribute_user(DATATYPE$10);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_attribute_user(DATATYPE$10);
            }
            target.set(dataType);
        }
    }
    
    /**
     * Unsets the "dataType" attribute
     */
    public void unsetDataType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_attribute(DATATYPE$10);
        }
    }
}
