/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.dataimportexport;

import org.cnrs.lam.dis.etc.dataimportexport.xml.Graph;
import org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters;
import org.cnrs.lam.dis.etc.datamodel.ComponentInfo;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class ObsParamWithDatasetsImpl implements ObsParamWithDatasets {

    private ObservingParameters obsParamXml;
    private ComponentInfo componentInfo;

    public ObsParamWithDatasetsImpl(ObservingParameters obsParamXml, ComponentInfo componentInfo) {
        this.obsParamXml = obsParamXml;
        this.componentInfo = componentInfo;
    }

    @Override
    public ComponentInfo getInfo() {
        return componentInfo;
    }

    @Override
    public TimeSampleType getTimeSampleType() {
        return TimeSampleType.valueOf(obsParamXml.getTimeSampleType());
    }

    @Override
    public void setTimeSampleType(TimeSampleType type) {
        throw new UnsupportedOperationException("Read only version of ObsParam");
    }

    @Override
    public double getDit() {
        return obsParamXml.getDit().getValue();
    }

    @Override
    public void setDit(double dit) {
        throw new UnsupportedOperationException("Read only version of ObsParam");
    }

    @Override
    public String getDitUnit() {
        return obsParamXml.getDit().getUnit();
    }

    @Override
    public int getNoExpo() {
        return obsParamXml.getNoExpo().getValue().intValue();
    }

    @Override
    public void setNoExpo(int noExpo) {
        throw new UnsupportedOperationException("Read only version of ObsParam");
    }

    @Override
    public FixedParameter getFixedParameter() {
        return FixedParameter.valueOf(obsParamXml.getFixedParameter());
    }

    @Override
    public void setFixedParameter(FixedParameter parameter) {
        throw new UnsupportedOperationException("Read only version of ObsParam");
    }

    @Override
    public double getExposureTime() {
        return obsParamXml.getExposureTime().getValue();
    }

    @Override
    public void setExposureTime(double time) {
        throw new UnsupportedOperationException("Read only version of ObsParam");
    }

    @Override
    public String getExposureTimeUnit() {
        return obsParamXml.getExposureTime().getUnit();
    }

    @Override
    public double getSnr() {
        return obsParamXml.getSnr().getValue();
    }

    @Override
    public void setSnr(double snr) {
        throw new UnsupportedOperationException("Read only version of ObsParam");
    }

    @Override
    public String getSnrUnit() {
        return obsParamXml.getSnr().getUnit();
    }

    @Override
    public double getSnrLambda() {
        return obsParamXml.getSnrLambda().getValue();
    }

    @Override
    public void setSnrLambda(double lambda) {
        throw new UnsupportedOperationException("Read only version of ObsParam");
    }

    @Override
    public String getSnrLambdaUnit() {
        return obsParamXml.getSnrLambda().getUnit();
    }
    
    @Override
    public SpectralQuantumType getSpectralQuantumType() {
        return SpectralQuantumType.valueOf(obsParamXml.getSpectralQuantumType());
    }
    
    @Override
    public void setSpectralQuantumType(SpectralQuantumType spectralQuantumType) {
        throw new UnsupportedOperationException("Read only version of ObsParam");
    }

    @Override
    public ExtraBackgroundNoiseType getExtraBackgroundNoiseType() {
        return ExtraBackgroundNoiseType.valueOf(obsParamXml.getExtraBackgroundNoiseType());
    }

    @Override
    public void setExtraBackgroundNoiseType(ExtraBackgroundNoiseType type) {
        throw new UnsupportedOperationException("Read only version of ObsParam");
    }

    @Override
    public ExtraSignalType getExtraSignalType() {
        return ExtraSignalType.valueOf(obsParamXml.getExtraSignalType());
    }

    @Override
    public void setExtraSignalType(ExtraSignalType type) {
        throw new UnsupportedOperationException("Read only version of ObsParam");
    }

    @Override
    public DatasetInfo getExtraBackgroundNoiseDataset() {
        if (obsParamXml.getExtraBackgroundNoiseDataset() == null)
            return null;
        return new DatasetInfo(obsParamXml.getExtraBackgroundNoiseDataset().getName(), componentInfo.getName(), null);
    }

    @Override
    public void setExtraBackgroundNoiseDataset(DatasetInfo extraBackgroundNoise) {
        throw new UnsupportedOperationException("Read only version of ObsParam");
    }

    @Override
    public DatasetInfo getExtraSignalDataset() {
        if (obsParamXml.getExtraSignalDataset() == null)
            return null;
        return new DatasetInfo(obsParamXml.getExtraSignalDataset().getName(), componentInfo.getName(), null);
    }

    @Override
    public void setExtraSignalDataset(DatasetInfo extraSignal) {
        throw new UnsupportedOperationException("Read only version of ObsParam");
    }

    @Override
    public Dataset getExtraBackgroundNoiseDatasetDataset() {
        Graph graph = obsParamXml.getExtraBackgroundNoiseDataset();
        return Helper.convertGraphToDataset(graph, componentInfo.getName(), Dataset.Type.EXTRA_BACKGROUND_NOISE);
    }

    @Override
    public Dataset getExtraSignalDatasetDataset() {
        Graph graph = obsParamXml.getExtraSignalDataset();
        return Helper.convertGraphToDataset(graph, componentInfo.getName(), Dataset.Type.EXTRA_SIGNAL);
    }

    @Override
    public double getExtraBackgroundNoise() {
        return obsParamXml.getExtraBackgroundNoise().getValue();
    }

    @Override
    public void setExtraBackgroundNoise(double extraBackgroundNoise) {
        throw new UnsupportedOperationException("Read only version of ObsParam");
    }

    @Override
    public String getExtraBackgroundNoiseUnit() {
        return obsParamXml.getExtraBackgroundNoise().getUnit();
    }

    @Override
    public double getExtraSignal() {
        return obsParamXml.getExtraSignal().getValue();
    }

    @Override
    public void setExtraSignal(double extraSignal) {
        throw new UnsupportedOperationException("Read only version of ObsParam");
    }

    @Override
    public String getExtraSignalUnit() {
        return obsParamXml.getExtraSignal().getUnit();
    }

    @Override
    public FixedSnrType getFixedSnrType() {
        return FixedSnrType.valueOf(obsParamXml.getFixedSnrType());
    }

    @Override
    public void setFixedSnrType(FixedSnrType type) {
        throw new UnsupportedOperationException("Read only version of ObsParam");
    }

}
