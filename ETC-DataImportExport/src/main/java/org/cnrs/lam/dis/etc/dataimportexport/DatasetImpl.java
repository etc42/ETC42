/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.dataimportexport;

import java.util.Map;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class DatasetImpl implements Dataset {
    
    private DatasetInfo info;
    private Type type;
    private Map<Double, Double> data;
    private String xUnit;
    private String yUnit;
    private String option;
    private DataType dataType;

    public DatasetImpl(DatasetInfo info, Type type, Map<Double, Double> data, String xUnit, String yUnit, DataType dataType) {
        this(info, type, data, xUnit, yUnit, null, dataType);
    }
    
    public DatasetImpl(DatasetInfo info, Type type, Map<Double, Double> data, String xUnit, String yUnit, String option, DataType dataType) {
        this.info = info;
        this.type = type;
        this.data = data;
        this.xUnit = xUnit;
        this.yUnit = yUnit;
        this.option = option;
        this.dataType = dataType;
    }

    @Override
    public DatasetInfo getInfo() {
        return info;
    }

    void setInfo(DatasetInfo info) {
        this.info = info;
    }

    @Override
    public Type getType() {
        return type;
    }

    @Override
    public Map<Double, Double> getData() {
        return data;
    }

    @Override
    public String getXUnit() {
        return xUnit;
    }

    @Override
    public String getYUnit() {
        return yUnit;
    }

    @Override
    public String getOption() {
        return option;
    }

    @Override
    public DataType getDataType() {
        return dataType;
    }

}
