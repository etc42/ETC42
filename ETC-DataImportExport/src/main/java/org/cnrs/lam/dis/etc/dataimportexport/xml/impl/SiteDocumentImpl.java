/*
 * An XML document type.
 * Localname: site
 * Namespace: 
 * Java type: org.cnrs.lam.dis.etc.dataimportexport.xml.SiteDocument
 *
 * Automatically generated - do not modify.
 */
package org.cnrs.lam.dis.etc.dataimportexport.xml.impl;
/**
 * A document containing one site(@) element.
 *
 * This is a complex type.
 */
public class SiteDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements org.cnrs.lam.dis.etc.dataimportexport.xml.SiteDocument
{
    private static final long serialVersionUID = 1L;
    
    public SiteDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName SITE$0 = 
        new javax.xml.namespace.QName("", "site");
    
    
    /**
     * Gets the "site" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.Site getSite()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Site target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Site)get_store().find_element_user(SITE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "site" element
     */
    public void setSite(org.cnrs.lam.dis.etc.dataimportexport.xml.Site site)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Site target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Site)get_store().find_element_user(SITE$0, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Site)get_store().add_element_user(SITE$0);
            }
            target.set(site);
        }
    }
    
    /**
     * Appends and returns a new empty "site" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.Site addNewSite()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Site target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Site)get_store().add_element_user(SITE$0);
            return target;
        }
    }
}
