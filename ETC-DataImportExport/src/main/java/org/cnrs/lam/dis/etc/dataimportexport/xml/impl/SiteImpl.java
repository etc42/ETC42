/*
 * XML Type:  Site
 * Namespace: 
 * Java type: org.cnrs.lam.dis.etc.dataimportexport.xml.Site
 *
 * Automatically generated - do not modify.
 */
package org.cnrs.lam.dis.etc.dataimportexport.xml.impl;
/**
 * An XML Site(@).
 *
 * This is a complex type.
 */
public class SiteImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements org.cnrs.lam.dis.etc.dataimportexport.xml.Site
{
    private static final long serialVersionUID = 1L;
    
    public SiteImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName INFO$0 = 
        new javax.xml.namespace.QName("", "info");
    private static final javax.xml.namespace.QName AIRMASS$2 = 
        new javax.xml.namespace.QName("", "airMass");
    private static final javax.xml.namespace.QName GALACTICCONTRIBUTING$4 = 
        new javax.xml.namespace.QName("", "galacticContributing");
    private static final javax.xml.namespace.QName GALACTICPROFILE$6 = 
        new javax.xml.namespace.QName("", "galacticProfile");
    private static final javax.xml.namespace.QName LOCATIONTYPE$8 = 
        new javax.xml.namespace.QName("", "locationType");
    private static final javax.xml.namespace.QName SEEING$10 = 
        new javax.xml.namespace.QName("", "seeing");
    private static final javax.xml.namespace.QName SKYABSORPTION$12 = 
        new javax.xml.namespace.QName("", "skyAbsorption");
    private static final javax.xml.namespace.QName SKYEMISSIONSELECTEDOPTION$14 = 
        new javax.xml.namespace.QName("", "skyEmissionSelectedOption");
    private static final javax.xml.namespace.QName SKYEMISSION$16 = 
        new javax.xml.namespace.QName("", "skyEmission");
    private static final javax.xml.namespace.QName SKYEXTINCTION$18 = 
        new javax.xml.namespace.QName("", "skyExtinction");
    private static final javax.xml.namespace.QName ZODIACALCONTRIBUTING$20 = 
        new javax.xml.namespace.QName("", "zodiacalContributing");
    private static final javax.xml.namespace.QName SEEINGLIMITED$22 = 
        new javax.xml.namespace.QName("", "seeingLimited");
    private static final javax.xml.namespace.QName ZODIACALPROFILE$24 = 
        new javax.xml.namespace.QName("", "zodiacalProfile");
    private static final javax.xml.namespace.QName ATMOSPHERICTRANSMISSION$26 = 
        new javax.xml.namespace.QName("", "atmosphericTransmission");
    private static final javax.xml.namespace.QName ATMOSPHERICTRANSMISSIONTYPE$28 = 
        new javax.xml.namespace.QName("", "atmosphericTransmissionType");
    
    
    /**
     * Gets the "info" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo getInfo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo)get_store().find_element_user(INFO$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "info" element
     */
    public void setInfo(org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo info)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo)get_store().find_element_user(INFO$0, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo)get_store().add_element_user(INFO$0);
            }
            target.set(info);
        }
    }
    
    /**
     * Appends and returns a new empty "info" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo addNewInfo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo)get_store().add_element_user(INFO$0);
            return target;
        }
    }
    
    /**
     * Gets the "airMass" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getAirMass()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(AIRMASS$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "airMass" element
     */
    public void setAirMass(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit airMass)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(AIRMASS$2, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(AIRMASS$2);
            }
            target.set(airMass);
        }
    }
    
    /**
     * Appends and returns a new empty "airMass" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewAirMass()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(AIRMASS$2);
            return target;
        }
    }
    
    /**
     * Gets the "galacticContributing" element
     */
    public boolean getGalacticContributing()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(GALACTICCONTRIBUTING$4, 0);
            if (target == null)
            {
                return false;
            }
            return target.getBooleanValue();
        }
    }
    
    /**
     * Gets (as xml) the "galacticContributing" element
     */
    public org.apache.xmlbeans.XmlBoolean xgetGalacticContributing()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(GALACTICCONTRIBUTING$4, 0);
            return target;
        }
    }
    
    /**
     * Sets the "galacticContributing" element
     */
    public void setGalacticContributing(boolean galacticContributing)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(GALACTICCONTRIBUTING$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(GALACTICCONTRIBUTING$4);
            }
            target.setBooleanValue(galacticContributing);
        }
    }
    
    /**
     * Sets (as xml) the "galacticContributing" element
     */
    public void xsetGalacticContributing(org.apache.xmlbeans.XmlBoolean galacticContributing)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(GALACTICCONTRIBUTING$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlBoolean)get_store().add_element_user(GALACTICCONTRIBUTING$4);
            }
            target.set(galacticContributing);
        }
    }
    
    /**
     * Gets the "galacticProfile" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.Graph getGalacticProfile()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().find_element_user(GALACTICPROFILE$6, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "galacticProfile" element
     */
    public void setGalacticProfile(org.cnrs.lam.dis.etc.dataimportexport.xml.Graph galacticProfile)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().find_element_user(GALACTICPROFILE$6, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().add_element_user(GALACTICPROFILE$6);
            }
            target.set(galacticProfile);
        }
    }
    
    /**
     * Appends and returns a new empty "galacticProfile" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.Graph addNewGalacticProfile()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().add_element_user(GALACTICPROFILE$6);
            return target;
        }
    }
    
    /**
     * Gets the "locationType" element
     */
    public java.lang.String getLocationType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(LOCATIONTYPE$8, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "locationType" element
     */
    public org.apache.xmlbeans.XmlString xgetLocationType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(LOCATIONTYPE$8, 0);
            return target;
        }
    }
    
    /**
     * Sets the "locationType" element
     */
    public void setLocationType(java.lang.String locationType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(LOCATIONTYPE$8, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(LOCATIONTYPE$8);
            }
            target.setStringValue(locationType);
        }
    }
    
    /**
     * Sets (as xml) the "locationType" element
     */
    public void xsetLocationType(org.apache.xmlbeans.XmlString locationType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(LOCATIONTYPE$8, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(LOCATIONTYPE$8);
            }
            target.set(locationType);
        }
    }
    
    /**
     * Gets the "seeing" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getSeeing()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(SEEING$10, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "seeing" element
     */
    public void setSeeing(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit seeing)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(SEEING$10, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(SEEING$10);
            }
            target.set(seeing);
        }
    }
    
    /**
     * Appends and returns a new empty "seeing" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewSeeing()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(SEEING$10);
            return target;
        }
    }
    
    /**
     * Gets the "skyAbsorption" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.Graph getSkyAbsorption()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().find_element_user(SKYABSORPTION$12, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "skyAbsorption" element
     */
    public void setSkyAbsorption(org.cnrs.lam.dis.etc.dataimportexport.xml.Graph skyAbsorption)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().find_element_user(SKYABSORPTION$12, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().add_element_user(SKYABSORPTION$12);
            }
            target.set(skyAbsorption);
        }
    }
    
    /**
     * Appends and returns a new empty "skyAbsorption" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.Graph addNewSkyAbsorption()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().add_element_user(SKYABSORPTION$12);
            return target;
        }
    }
    
    /**
     * Gets the "skyEmissionSelectedOption" element
     */
    public java.lang.String getSkyEmissionSelectedOption()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SKYEMISSIONSELECTEDOPTION$14, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "skyEmissionSelectedOption" element
     */
    public org.apache.xmlbeans.XmlString xgetSkyEmissionSelectedOption()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(SKYEMISSIONSELECTEDOPTION$14, 0);
            return target;
        }
    }
    
    /**
     * Sets the "skyEmissionSelectedOption" element
     */
    public void setSkyEmissionSelectedOption(java.lang.String skyEmissionSelectedOption)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SKYEMISSIONSELECTEDOPTION$14, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SKYEMISSIONSELECTEDOPTION$14);
            }
            target.setStringValue(skyEmissionSelectedOption);
        }
    }
    
    /**
     * Sets (as xml) the "skyEmissionSelectedOption" element
     */
    public void xsetSkyEmissionSelectedOption(org.apache.xmlbeans.XmlString skyEmissionSelectedOption)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(SKYEMISSIONSELECTEDOPTION$14, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(SKYEMISSIONSELECTEDOPTION$14);
            }
            target.set(skyEmissionSelectedOption);
        }
    }
    
    /**
     * Gets array of all "skyEmission" elements
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.Graph[] getSkyEmissionArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(SKYEMISSION$16, targetList);
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph[] result = new org.cnrs.lam.dis.etc.dataimportexport.xml.Graph[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "skyEmission" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.Graph getSkyEmissionArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().find_element_user(SKYEMISSION$16, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "skyEmission" element
     */
    public int sizeOfSkyEmissionArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(SKYEMISSION$16);
        }
    }
    
    /**
     * Sets array of all "skyEmission" element
     */
    public void setSkyEmissionArray(org.cnrs.lam.dis.etc.dataimportexport.xml.Graph[] skyEmissionArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(skyEmissionArray, SKYEMISSION$16);
        }
    }
    
    /**
     * Sets ith "skyEmission" element
     */
    public void setSkyEmissionArray(int i, org.cnrs.lam.dis.etc.dataimportexport.xml.Graph skyEmission)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().find_element_user(SKYEMISSION$16, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(skyEmission);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "skyEmission" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.Graph insertNewSkyEmission(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().insert_element_user(SKYEMISSION$16, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "skyEmission" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.Graph addNewSkyEmission()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().add_element_user(SKYEMISSION$16);
            return target;
        }
    }
    
    /**
     * Removes the ith "skyEmission" element
     */
    public void removeSkyEmission(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(SKYEMISSION$16, i);
        }
    }
    
    /**
     * Gets the "skyExtinction" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.Graph getSkyExtinction()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().find_element_user(SKYEXTINCTION$18, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "skyExtinction" element
     */
    public void setSkyExtinction(org.cnrs.lam.dis.etc.dataimportexport.xml.Graph skyExtinction)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().find_element_user(SKYEXTINCTION$18, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().add_element_user(SKYEXTINCTION$18);
            }
            target.set(skyExtinction);
        }
    }
    
    /**
     * Appends and returns a new empty "skyExtinction" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.Graph addNewSkyExtinction()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().add_element_user(SKYEXTINCTION$18);
            return target;
        }
    }
    
    /**
     * Gets the "zodiacalContributing" element
     */
    public boolean getZodiacalContributing()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ZODIACALCONTRIBUTING$20, 0);
            if (target == null)
            {
                return false;
            }
            return target.getBooleanValue();
        }
    }
    
    /**
     * Gets (as xml) the "zodiacalContributing" element
     */
    public org.apache.xmlbeans.XmlBoolean xgetZodiacalContributing()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(ZODIACALCONTRIBUTING$20, 0);
            return target;
        }
    }
    
    /**
     * Sets the "zodiacalContributing" element
     */
    public void setZodiacalContributing(boolean zodiacalContributing)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ZODIACALCONTRIBUTING$20, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ZODIACALCONTRIBUTING$20);
            }
            target.setBooleanValue(zodiacalContributing);
        }
    }
    
    /**
     * Sets (as xml) the "zodiacalContributing" element
     */
    public void xsetZodiacalContributing(org.apache.xmlbeans.XmlBoolean zodiacalContributing)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(ZODIACALCONTRIBUTING$20, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlBoolean)get_store().add_element_user(ZODIACALCONTRIBUTING$20);
            }
            target.set(zodiacalContributing);
        }
    }
    
    /**
     * Gets the "seeingLimited" element
     */
    public boolean getSeeingLimited()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SEEINGLIMITED$22, 0);
            if (target == null)
            {
                return false;
            }
            return target.getBooleanValue();
        }
    }
    
    /**
     * Gets (as xml) the "seeingLimited" element
     */
    public org.apache.xmlbeans.XmlBoolean xgetSeeingLimited()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(SEEINGLIMITED$22, 0);
            return target;
        }
    }
    
    /**
     * Sets the "seeingLimited" element
     */
    public void setSeeingLimited(boolean seeingLimited)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SEEINGLIMITED$22, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SEEINGLIMITED$22);
            }
            target.setBooleanValue(seeingLimited);
        }
    }
    
    /**
     * Sets (as xml) the "seeingLimited" element
     */
    public void xsetSeeingLimited(org.apache.xmlbeans.XmlBoolean seeingLimited)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(SEEINGLIMITED$22, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlBoolean)get_store().add_element_user(SEEINGLIMITED$22);
            }
            target.set(seeingLimited);
        }
    }
    
    /**
     * Gets the "zodiacalProfile" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.Graph getZodiacalProfile()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().find_element_user(ZODIACALPROFILE$24, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "zodiacalProfile" element
     */
    public void setZodiacalProfile(org.cnrs.lam.dis.etc.dataimportexport.xml.Graph zodiacalProfile)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().find_element_user(ZODIACALPROFILE$24, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().add_element_user(ZODIACALPROFILE$24);
            }
            target.set(zodiacalProfile);
        }
    }
    
    /**
     * Appends and returns a new empty "zodiacalProfile" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.Graph addNewZodiacalProfile()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().add_element_user(ZODIACALPROFILE$24);
            return target;
        }
    }
    
    /**
     * Gets the "atmosphericTransmission" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.Graph getAtmosphericTransmission()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().find_element_user(ATMOSPHERICTRANSMISSION$26, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "atmosphericTransmission" element
     */
    public void setAtmosphericTransmission(org.cnrs.lam.dis.etc.dataimportexport.xml.Graph atmosphericTransmission)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().find_element_user(ATMOSPHERICTRANSMISSION$26, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().add_element_user(ATMOSPHERICTRANSMISSION$26);
            }
            target.set(atmosphericTransmission);
        }
    }
    
    /**
     * Appends and returns a new empty "atmosphericTransmission" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.Graph addNewAtmosphericTransmission()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().add_element_user(ATMOSPHERICTRANSMISSION$26);
            return target;
        }
    }
    
    /**
     * Gets the "atmosphericTransmissionType" element
     */
    public java.lang.String getAtmosphericTransmissionType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ATMOSPHERICTRANSMISSIONTYPE$28, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "atmosphericTransmissionType" element
     */
    public org.apache.xmlbeans.XmlString xgetAtmosphericTransmissionType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(ATMOSPHERICTRANSMISSIONTYPE$28, 0);
            return target;
        }
    }
    
    /**
     * Sets the "atmosphericTransmissionType" element
     */
    public void setAtmosphericTransmissionType(java.lang.String atmosphericTransmissionType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ATMOSPHERICTRANSMISSIONTYPE$28, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ATMOSPHERICTRANSMISSIONTYPE$28);
            }
            target.setStringValue(atmosphericTransmissionType);
        }
    }
    
    /**
     * Sets (as xml) the "atmosphericTransmissionType" element
     */
    public void xsetAtmosphericTransmissionType(org.apache.xmlbeans.XmlString atmosphericTransmissionType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(ATMOSPHERICTRANSMISSIONTYPE$28, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(ATMOSPHERICTRANSMISSIONTYPE$28);
            }
            target.set(atmosphericTransmissionType);
        }
    }
}
