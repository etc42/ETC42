/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.dataimportexport;

import org.cnrs.lam.dis.etc.dataimportexport.xml.Graph;
import org.cnrs.lam.dis.etc.dataimportexport.xml.Source;
import org.cnrs.lam.dis.etc.datamodel.ComponentInfo;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;

/**
 *
 * @author Nikolaos Apostolakos
 */
class SourceWithDatasetsImpl implements SourceWithDatasets {

    private Source sourceXml;
    private ComponentInfo componentInfo;

    public SourceWithDatasetsImpl(Source sourceXml, ComponentInfo componentInfo) {
        this.sourceXml = sourceXml;
        this.componentInfo = componentInfo;
    }

    @Override
    public Dataset getSpectralDistributionTemplateDataset() {
        Graph graph = sourceXml.getSpectralDistributionTemplate();
        return Helper.convertGraphToDataset(graph, componentInfo.getName(), Dataset.Type.SPECTRAL_DIST_TEMPLATE);
    }

    @Override
    public ComponentInfo getInfo() {
        return componentInfo;
    }

    @Override
    public double getRedshift() {
        return sourceXml.getRedshift().getValue();
    }
    
    @Override
    public void setRedshift(double redshift) {
        throw new UnsupportedOperationException("Read only version of Source");
    }
    
    @Override
    public double getTemperature(){
        return sourceXml.getTemperature().getValue();
    }
    @Override
    public void setTemperature(double temperature){
        throw new UnsupportedOperationException("Read only version of Source");
    }
    
    @Override
    public String getTemperatureUnit(){
        return sourceXml.getTemperature().getUnit();
    }

    @Override
    public double getMagnitude() {
        return sourceXml.getMagnitude().getValue();
    }

    @Override
    public void setMagnitude(double magnitude) {
        throw new UnsupportedOperationException("Read only version of Source");
    }

    @Override
    public SpectralDistributionType getSpectralDistributionType() {
        return SpectralDistributionType.valueOf(sourceXml.getSpectralDistributionType());
    }

    @Override
    public void setSpectralDistributionType(SpectralDistributionType type) {
        throw new UnsupportedOperationException("Read only version of Source");
    }

    @Override
    public DatasetInfo getSpectralDistributionTemplate() {
        if (sourceXml.getSpectralDistributionTemplate() == null)
            return null;
        return new DatasetInfo(sourceXml.getSpectralDistributionTemplate().getName(), componentInfo.getName(), null);
    }

    @Override
    public void setSpectralDistributionTemplate(DatasetInfo template) {
        throw new UnsupportedOperationException("Read only version of Source");
    }

    @Override
    public double getEmissionLineWavelength() {
        return sourceXml.getEmissionLineWavelength().getValue();
    }

    @Override
    public void setEmissionLineWavelength(double wavelength) {
        throw new UnsupportedOperationException("Read only version of Source");
    }

    @Override
    public String getEmissionLineWavelengthUnit() {
        return sourceXml.getEmissionLineWavelength().getUnit();
    }
    
    @Override
    public double getEmissionLineFwhm() {
        return sourceXml.getEmissionLineFwhm().getValue();
    }

    @Override
    public void setEmissionLineFwhm(double fwhm) {
        throw new UnsupportedOperationException("Read only version of Source");
    }

    @Override
    public String getEmissionLineFwhmUnit() {
        return sourceXml.getEmissionLineFwhm().getUnit();
    }

    @Override
    public SpatialDistributionType getSpatialDistributionType() {
        return SpatialDistributionType.valueOf(sourceXml.getSpatialDistributionType());
    }

    @Override
    public void setSpatialDistributionType(SpatialDistributionType type) {
        throw new UnsupportedOperationException("Read only version of Source");
    }

    @Override
    public double getExtendedSourceRadius() {
        return sourceXml.getExtendedSourceRadius().getValue();
    }

    @Override
    public void setExtendedSourceRadius(double radious) {
        throw new UnsupportedOperationException("Read only version of Source");
    }

    @Override
    public String getExtendedSourceRadiusUnit() {
        return sourceXml.getExtendedSourceRadius().getUnit();
    }

    @Override
    public SurfaceBrightnessProfile getExtendedSourceProfile() {
        return SurfaceBrightnessProfile.valueOf(sourceXml.getExtendedSourceProfile());
    }

    @Override
    public void setExtendedSourceProfile(SurfaceBrightnessProfile profile) {
        throw new UnsupportedOperationException("Read only version of Source");
    }

    @Override
    public double getEmissionLineFlux() {
        return sourceXml.getEmissionLineFlux().getValue();
    }

    @Override
    public void setEmissionLineFlux(double flux) {
        throw new UnsupportedOperationException("Read only version of Source");
    }

    @Override
    public String getEmissionLineFluxUnit() {
        return sourceXml.getEmissionLineFlux().getUnit();
    }

    @Override
    public double getMagnitudeWavelength() {
        return sourceXml.getMagnitudeWavelength().getValue();
    }

    @Override
    public void setMagnitudeWavelength(double lambda) {
        throw new UnsupportedOperationException("Read only version of Source");
    }

    @Override
    public String getMagnitudeWavelengthUnit() {
        return sourceXml.getMagnitudeWavelength().getUnit();
    }

    @Override
    public ExtendedMagnitudeType getExtendedMagnitudeType() {
        return ExtendedMagnitudeType.valueOf(sourceXml.getExtendedMagnitudeType());
    }

    @Override
    public void setExtendedMagnitudeType(ExtendedMagnitudeType type) {
        throw new UnsupportedOperationException("Read only version of Source");
    }

}
