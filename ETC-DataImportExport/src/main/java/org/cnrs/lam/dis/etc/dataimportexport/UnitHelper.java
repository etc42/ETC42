/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.dataimportexport;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Nikolaos Apostolakos
 */
final class UnitHelper {

    private static Map<String, String> dicoUnit;

    static {
        dicoUnit = new HashMap<String, String>();
        // Liste des graphies possibles de micron
        dicoUnit.put("micron", "micron");
        dicoUnit.put("microm", "micron");
        dicoUnit.put("micro", "micron");
        dicoUnit.put("um", "micron");
        dicoUnit.put("umetre", "micron");
        dicoUnit.put("micrometre", "micron");

        // Liste des graphies possibles de nanometre
        dicoUnit.put("nano", "nanometre");
        dicoUnit.put("nanom", "nanometre");
        dicoUnit.put("nmetre", "nanometre");
        dicoUnit.put("nanometre", "nanometre");
        dicoUnit.put("nm", "nanometre");

        // Liste des graphies possibles de angstrom
        dicoUnit.put("angstrom", "angstrom");
        dicoUnit.put("Angstrom", "angstrom");
        dicoUnit.put("ang", "angstrom");
        dicoUnit.put("Ang", "angstrom");
        dicoUnit.put("A*", "angstrom");
        dicoUnit.put("A", "angstrom");
        dicoUnit.put("1.0E-10 L", "angstrom");
        dicoUnit.put("Angstroms", "angstrom");
        dicoUnit.put("angstroms", "angstrom");

        // Liste des graphies possibles de metre
        dicoUnit.put("m", "metre");
        dicoUnit.put("metre", "metre");

        // Liste des graphies possibles de Hertz
        dicoUnit.put("Hz", "Hz");
        dicoUnit.put("Hertz", "Hz");
        dicoUnit.put("1.0 T-1", "Hz");


        // => Valeurs de Y **********************

        // Liste des graphies possibles de Janski
        dicoUnit.put("Jy", "Jy");
        dicoUnit.put("jy", "Jy");
        dicoUnit.put("Janski", "Jy");

        // Liste des graphies possibles de milliJanski
        dicoUnit.put("mJy", "mJy");
        dicoUnit.put("mjy", "mJy");
        dicoUnit.put("mJanski", "mJy");

        // Liste des graphies possibles de erg_s_cm_Hz
        dicoUnit.put("erg/cm2/s/Hz", "erg/s/cm2/Hz");
        dicoUnit.put("erg/s/cm2/Hz", "erg/s/cm2/Hz");
        dicoUnit.put("erg/cm**2/s/Hz", "erg/s/cm2/Hz");
        dicoUnit.put("erg/s/cm**2/Hz", "erg/s/cm2/Hz");
        dicoUnit.put("erg/cm^2/s/Hz", "erg/s/cm2/Hz");
        dicoUnit.put("erg/s/cm^2/Hz", "erg/s/cm2/Hz");
        dicoUnit.put("0.0010 MT-2", "erg/s/cm2/Hz");

        // Liste des graphies possibles de erg_s_cm_um
        dicoUnit.put("erg/cm2/s/um", "erg/s/cm2/um");
        dicoUnit.put("erg/s/cm2/um", "erg/s/cm2/um");
        dicoUnit.put("erg/cm**2/s/um", "erg/s/cm2/um");
        dicoUnit.put("erg/s/cm**2/um", "erg/s/cm2/um");
        dicoUnit.put("erg/cm^2/s/um", "erg/s/cm2/um");
        dicoUnit.put("erg/s/cm^2/um", "erg/s/cm2/um");
        dicoUnit.put("1000.0 ML-1T-3", "erg/s/cm2/um");

        // Liste des graphies possibles de erg_s_cm_ang
        dicoUnit.put("erg/s/cm2/ang", "erg/s/cm2/ang");
        dicoUnit.put("erg/s/cm2/A", "erg/s/cm2/ang");
        dicoUnit.put("erg/cm2/s/ang", "erg/s/cm2/ang");
        dicoUnit.put("erg/cm2/s/A", "erg/s/cm2/ang");
        dicoUnit.put("1.0E7 ML-1T-3", "erg/s/cm2/ang");
        dicoUnit.put("erg/s/cm**2/ang", "erg/s/cm2/ang");
        dicoUnit.put("erg/s/cm**2/A", "erg/s/cm2/ang");
        dicoUnit.put("erg/cm**2/s/ang", "erg/s/cm2/ang");
        dicoUnit.put("erg/cm**2/s/A", "erg/s/cm2/ang");
        dicoUnit.put("erg/s/cm^2/ang", "erg/s/cm2/ang");
        dicoUnit.put("erg/s/cm^2/A", "erg/s/cm2/ang");
        dicoUnit.put("erg/cm^2/s/ang", "erg/s/cm2/ang");
        dicoUnit.put("erg/cm^2/s/A", "erg/s/cm2/ang");
        dicoUnit.put("erg/s/cm**2/Angstrom", "erg/s/cm2/ang");
        dicoUnit.put("erg/cm**2/s/Angstrom", "erg/s/cm2/ang");

        // Liste des graphies possibles de W/cm²/um
        dicoUnit.put("W/cm2/um", "W/cm2/um");
        dicoUnit.put("1.0E10 ML-1T-3", "W/cm2/um");
        dicoUnit.put("W/cm**2/um", "W/cm2/um");
        dicoUnit.put("W/cm^2/um", "W/cm2/um");

        // Liste des graphies possibles de W/cm²/A
        dicoUnit.put("W/cm2/A", "W/cm2/A");
        dicoUnit.put("1.0E14 ML-1T-3", "W/cm2/A");
        dicoUnit.put("W/cm**2/A", "W/cm2/A");
        dicoUnit.put("W/cm^2/A", "W/cm2/A");

        // Liste des graphies possibles de W/cm²/Hz
        dicoUnit.put("W/cm2/Hz", "W/cm2/Hz");
        dicoUnit.put("10000.0 MT-2", "W/cm2/Hz");
        dicoUnit.put("W/cm**2/Hz", "W/cm2/Hz");
        dicoUnit.put("W/cm^2/Hz", "W/cm2/Hz");

        // Liste des graphies possibles de W/m²/um
        dicoUnit.put("W/m2/um", "W/m2/um");
        dicoUnit.put("1000000.0 ML-1T-3", "W/m2/um");
        dicoUnit.put("W/m**2/um", "W/m2/um");
        dicoUnit.put("W/m^2/um", "W/m2/um");

        // Liste des graphies possibles de W/m²/A
        dicoUnit.put("W/m2/A", "W/m2/A");
        dicoUnit.put("1.0E10 ML-1T-3", "W/m2/A");
        dicoUnit.put("W/m**2/A", "W/m2/A");
        dicoUnit.put("W/m^2/A", "W/m2/A");

        // Liste des graphies possibles de W/m²/Hz
        dicoUnit.put("W/m2/um", "W/m2/Hz");
        dicoUnit.put("1000000.0 ML-1T-3", "W/m2/Hz");
        dicoUnit.put("W/m**2/um", "W/m2/Hz");
        dicoUnit.put("W/m^2/um", "W/m2/Hz");

        // Liste des graphies possibles de photons/cm²/s/A
        dicoUnit.put("photons/cm2/s/A", "photons/cm2/s/A");
        dicoUnit.put("photons/s/cm2/A", "photons/cm2/s/A");
        dicoUnit.put("1.0E14 L-3T-1", "photons/s/cm2/A");
        dicoUnit.put("photons/cm**2/s/A", "photons/cm2/s/A");
        dicoUnit.put("photons/s/cm**2/A", "photons/cm2/s/A");
        dicoUnit.put("photons/cm^2/s/A", "photons/cm2/s/A");
        dicoUnit.put("photons/s/cm^2/A", "photons/cm2/s/A");
    }

    static double omniParseX(double in, String unit) throws Exception {

        try {
            double out = in;


            if ((dicoUnit.get(unit)).equals("micron")) {
                out = micron2Ang(in);
            } else if ((dicoUnit.get(unit)).equals("nanometre")) {
                out = nano2Ang(in);
            } else if ((dicoUnit.get(unit)).equals("angstrom")) {
                out = in;
            } else if ((dicoUnit.get(unit)).equals("Hz")) {
                out = hz2Ang(in);
            } else if ((dicoUnit.get(unit)).equals("metre")) {
                out = m2Ang(in);
            } else {
                throw new Exception("The unit of X (wavelenght) is unknown");
            }
            return out;
        } catch (Exception ex) {
            throw new Exception("The unit of X (wavelenght) is unknown");
        }

    }

    static double omniParseY(double inY, double inX, String unit) throws Exception {

        try {
            double out = inY;

            if ((dicoUnit.get(unit)).equals("Jy")) {
                out = jy_TO_erg_s_cm_ang(inY, inX);
            } else if ((dicoUnit.get(unit)).equals("mJy")) {
                out = mjy_TO_erg_s_cm_ang(inY, inX);
            } else if ((dicoUnit.get(unit)).equals("W/cm2/um")) {
                out = wcm2um2erg_s_cm_ang(inY);
            } else if ((dicoUnit.get(unit)).equals("W/cm2/A")) {
                out = wcm2A2erg_s_cm_ang(inY);
            } else if ((dicoUnit.get(unit)).equals("W/cm2/Hz")) {
                out = wcm2Hz2erg_s_cm_ang(inY);
            } else if ((dicoUnit.get(unit)).equals("W/m2/um")) {
                out = wm2um2erg_s_cm_ang(inY);
            } else if ((dicoUnit.get(unit)).equals("W/m2/A")) {
                out = wm2A2erg_s_cm_ang(inY);
            } else if ((dicoUnit.get(unit)).equals("W/m2/Hz")) {
                out = wm2Hz2erg_s_cm_ang(inY);
            } else if ((dicoUnit.get(unit)).equals("erg/s/cm2/um")) {
                out = erg_s_cm_um2erg_s_cm_ang(inY);
            } else if ((dicoUnit.get(unit)).equals("erg/s/cm2/Hz")) {
                out = erg_s_cm_Hz2erg_s_cm_ang(inY);
            } else if ((dicoUnit.get(unit)).equals("erg/s/cm2/ang")) {
                out = inY;
            } else if ((dicoUnit.get(unit)).equals("photons/cm2/s/A")) {
                out = photons_s_cm_A2erg_s_cm_ang(inY);
            } else {
                throw new Exception("The unit of Y (flux) is unknown");
            }
            return out;
        } catch (Exception ex) {
            throw new Exception("The unit of Y (flux) is unknown");
        }

    }

    private static double nano2Ang(double nano) {

        double Ang = 10 * nano;
        return Ang;
    }

    private static double micron2Ang(double micron) {

        double Ang = 10000 * micron;
        return Ang;
    }

    private static double m2Ang(double m) {

        double Ang = 1E10 * m;
        return Ang;
    }

    private static double hz2Ang(double hz) {

        double Ang = 2.99792458E18 * (1 / hz);
        return Ang;
    }

// Méthodes de conversion de Y
    /**
     * convert from mJy to erg/cm²/s/A
     * * @param iny
     * @param inx
     * @return
     */
    private static double mjy_TO_erg_s_cm_ang(double iny, double inx) {

        return jy_TO_erg_s_cm_ang(1000 * iny, inx);

    }

    /**
     * convert from Jy to erg/cm²/s/A
     * @param iny
     * @param inx
     * @return
     */
    private static double jy_TO_erg_s_cm_ang(double iny, double inx) {

        // iny est la valeur de y en Jy
        // inx est la valeur de x en A
        // SpeedOfLight est exprimée en m/s => on la veut en A
        double out = ((2.99792458e8 * 1E10) * 1E-23 * iny) / (Math.pow(inx, 2.0));

        return out;

    }

    /**
     * convert from W/m²/um to erg/cm²/s/A
     * @param wm
     * @return
     */
    private static double wm2um2erg_s_cm_ang(double wm) {

        return 0.1 * wm;
    }

    /**
     * convert from W/m²/A to erg/cm²/s/A
     * @param wm
     * @return
     */
    private static double wm2A2erg_s_cm_ang(double wm) {

        return 1000.0 * wm;
    }

    /**
     * convert from W/m²/hz to erg/cm²/s/A
     * @param wm
     * @return
     */
    private static double wm2Hz2erg_s_cm_ang(double wm) {

        return 2.99792458E21 * wm;
    }

    /**
     * convert from W/cm²/um to erg/cm²/s/A
     * @param wm
     * @return
     */
    private static double wcm2um2erg_s_cm_ang(double wm) {

        return 1000.0 * wm;
    }

    /**
     * convert from W/cm²/A to erg/cm²/s/A
     * @param wm
     * @return
     */
    private static double wcm2A2erg_s_cm_ang(double wm) {

        return 1.0E7 * wm;
    }

    /**
     * convert from W/cm²/Hz to erg/cm²/s/A
     * @param wm
     * @return
     */
    private static double wcm2Hz2erg_s_cm_ang(double wm) {

        return 2.99792458E25 * wm;
    }

    /**
     * convert from erg/cm²/s/Hz to erg/cm²/s/A
     * @param in
     * @return
     */
    private static double erg_s_cm_Hz2erg_s_cm_ang(double in) {

        return 2.9979245799999995E18 * in;
    }

    /**
     * convert from erg/cm²/s/um to erg/cm²/s/A
     * @param in
     * @return
     */
    private static double erg_s_cm_um2erg_s_cm_ang(double in) {

        return 1.0E-4 * in;
    }

    /**
     * convert from photons/cm²/s/A to erg/cm²/s/A
     * @param in
     * @return
     */
    private static double photons_s_cm_A2erg_s_cm_ang(double in) {

        return 1.986445212595144E-8 * in;
    }
}
