/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.dataimportexport;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.astrogrid.samp.Message;
import org.astrogrid.samp.Metadata;
import org.astrogrid.samp.Subscriptions;
import org.astrogrid.samp.client.HubConnection;
import org.astrogrid.samp.client.SampException;
import org.astrogrid.samp.xmlrpc.StandardClientProfile;
import org.cnrs.lam.dis.etc.configuration.ConfigFactory;
import org.cnrs.lam.dis.samp.connector.SAMPConnector;

/**
 *
 * @author Nikolaos Apostolakos
 */
final class SampControllerImpl implements SampController {
    
    private static final String VO_TABLE_MTYPE = "table.load.votable";
    
    private static Logger logger = Logger.getLogger(SampControllerImpl.class);

    private SampListener sampListener;
    private SAMPConnector connector;

    @Override
    public void connect() throws SampException {
        if (connector != null)
            disconnect();
        connector = new SAMPConnector(true, getmetadata(), getSubscription());
        connector.addSampEventListener(new SampEvenListenerImpl(sampListener));
    }

    private Metadata getmetadata() {
        Metadata metadataETC = new Metadata();
        metadataETC.setName("ETC-42");
        metadataETC.setDescriptionText("Exposure Time Calculator");
        return metadataETC;
    }

    private Subscriptions getSubscription() {
        Subscriptions subETC = new Subscriptions();
        subETC.addMType("samp.app.ping");
        subETC.addMType("spectrum.load.ssa-generic");
        return subETC;
    }

    @Override
    public void disconnect() throws SampException {
        connector.unregister();
        connector = null;
    }

    @Override
    public void setSampListener(SampListener listener) {
        this.sampListener = listener;
    }

    @Override
    public boolean hubExists() {
        StandardClientProfile clientProfile = StandardClientProfile.getInstance();
        try {
            HubConnection hubConnection = clientProfile.register();
            if (hubConnection == null)
                return false;
            hubConnection.unregister();
            return true;
        } catch (SampException ex) {
            return false;
        }
    }

    @Override
    public void createHub() {
        org.astrogrid.samp.JSamp.main(new String[] {"hub"});
    }

    @Override
    public List<String> getVoTableClients() {
        List<String> clientNameList = new ArrayList<String>();
        if (connector != null) {
            try {
                Map subscribedClientMap = connector.getSubscribedClients(VO_TABLE_MTYPE);
                for (Object key : subscribedClientMap.keySet()) {
                    clientNameList.add(connector.getMetadata(key.toString()).getName());
                }
            } catch (SampException ex) {
                logger.error("Failed to get the VO table SAMP clients. Reason:" + ex.getMessage(), ex);
            }
        }
        return clientNameList;
    }

    @Override
    public void sendVoTable(String clientName, String description, Map<? extends Number, ? extends Number> data, String xUnit, String yUnit) throws SampException {
        String clientId = findClientId(clientName);
        if (clientId == null) {
            logger.warn("Failed to send VO table to application " + clientName + " because it wasn't possible to retrieve its ID");
            throw new SampException("Failed to send VO table to application " + clientName + " because it wasn't possible to retrieve its ID");
        }
        Message messageToSend = new Message(VO_TABLE_MTYPE);
        File fileToSend = null;
        try {
            fileToSend = makeVoTableTempFile(description, data, xUnit, yUnit);
        } catch (IOException ex) {
            logger.error("Failed to make the VOTable temporary file. Reason: " + ex.getMessage(), ex);
            throw new SampException(ex);
        }
        String urlString = null;
        try {
            urlString = fileToSend.toURI().toURL().toString();
        } catch (MalformedURLException ex) {
            logger.error("Malformed file URL. " + ex.getMessage(), ex);
            throw new SampException("Failed to create the temporary file. Malformed URL.");
        }
        messageToSend.addParam("url", urlString);
        connector.notify(clientId, messageToSend);
    }

    private String findClientId(String clientName) throws SampException {
        String clientId = null;
        if (connector != null) {
            Map subscribedClientMap = connector.getSubscribedClients(VO_TABLE_MTYPE);
            for (Object key : subscribedClientMap.keySet()) {
                if (connector.getMetadata(key.toString()).getName().equals(clientName)) {
                    clientId = key.toString();
                }
            }
        }
        return clientId;
    }
    
    private File makeVoTableTempFile(String description, Map<? extends Number, ? extends Number> data, String xUnit, String yUnit) throws IOException {
        File tempFile = null;
        String fileName = "ETC_" + Calendar.getInstance().getTimeInMillis() + ".vot";
        File tempDir = new File(ConfigFactory.getConfig().getTempDir());
        tempDir.mkdirs();
        tempFile = new File(tempDir, fileName);
        tempFile.createNewFile();
        tempFile.deleteOnExit();
        
        // TODO: The following lines have been copied from the old ETC and must be reviewed
        // *********************************************************************
        PrintWriter fOut;
        fOut = new PrintWriter(tempFile, "UTF-8");
        StringBuilder headerFicVO = new StringBuilder();
        StringBuilder bodyFicVO = new StringBuilder();
        StringBuilder footerFicVO = new StringBuilder();
        
        String lineSeparator = ConfigFactory.getConfig().getLineSeparator();

        headerFicVO.append("<VOTABLE version=\"1.1\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" ");
        headerFicVO.append("xmlns=\"http://www.ivoa.net/xml/VOTable/v1.1\" ");
        headerFicVO.append("xsi:schemaLocation=\"http://www.ivoa.net/xml/VOTable/v1.1 http://www.ivoa.net/xml/VOTable/v1.1\">");
        headerFicVO.append(lineSeparator);

        headerFicVO.append("<DESCRIPTION>");
        headerFicVO.append("Export from the Exposure Time Calculator - made by CeSAM");
        headerFicVO.append(lineSeparator);
        headerFicVO.append("http://www.oamp.fr/infoglueDeliverLive/www/LAM");
        headerFicVO.append(lineSeparator);
        headerFicVO.append("</DESCRIPTION>");
        
        fOut.println(headerFicVO.toString());

        bodyFicVO.append("<RESOURCE>");
        bodyFicVO.append(lineSeparator);

        bodyFicVO.append("<TABLE>");
        bodyFicVO.append(lineSeparator);
        bodyFicVO.append("<DESCRIPTION>");
        bodyFicVO.append(description);
        bodyFicVO.append("</DESCRIPTION>");
        bodyFicVO.append(lineSeparator);

        bodyFicVO.append("<FIELD ID=\"X_Axis\" datatype=\"double\" unit=\"");
        bodyFicVO.append((xUnit == null ? "" : xUnit));
        bodyFicVO.append("\"/>");
        bodyFicVO.append(lineSeparator);
        bodyFicVO.append("<FIELD ID=\"Y_Axis\" datatype=\"double\" unit=\"");
        bodyFicVO.append((yUnit == null ? "" : yUnit));
        bodyFicVO.append("\"/>");
        bodyFicVO.append(lineSeparator);

        bodyFicVO.append("<DATA>");
        bodyFicVO.append(lineSeparator);
        bodyFicVO.append("<TABLEDATA>");
        bodyFicVO.append(lineSeparator);
        
        fOut.println(bodyFicVO.toString());


        // Pour chaque couple de valeur
        String startTr = "<TR>" + lineSeparator;
        String endTr = "</TR>" + lineSeparator;
        String startTd = "<TD>";
        String endTd = "</TD>" + lineSeparator;
        for (Map.Entry<? extends Number, ? extends Number> entry : data.entrySet()) {
            bodyFicVO = new StringBuilder();
            bodyFicVO.append(startTr);
            bodyFicVO.append(startTd);
            bodyFicVO.append(entry.getKey());
            bodyFicVO.append(endTd);
            bodyFicVO.append(startTd);
            bodyFicVO.append(entry.getValue());
            bodyFicVO.append(endTd);
            bodyFicVO.append(endTr);
            fOut.println(bodyFicVO.toString());
        }

        bodyFicVO.append("</TABLEDATA>");
        bodyFicVO.append(lineSeparator);
        bodyFicVO.append("</DATA>");
        bodyFicVO.append(lineSeparator);

        bodyFicVO.append("</TABLE>");
        bodyFicVO.append(lineSeparator);
        bodyFicVO.append("</RESOURCE>");
        bodyFicVO.append(lineSeparator);
        
        fOut.println(bodyFicVO.toString());

        footerFicVO.append("</VOTABLE>");
        footerFicVO.append(lineSeparator);
        
        fOut.println(footerFicVO);
        
        fOut.close();
        // *********************************************************************
        
        logger.info("Created temporary VOTable file " + tempFile);
        return tempFile;
    }
}
