/*
 * XML Type:  Instrument
 * Namespace: 
 * Java type: org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument
 *
 * Automatically generated - do not modify.
 */
package org.cnrs.lam.dis.etc.dataimportexport.xml.impl;
/**
 * An XML Instrument(@).
 *
 * This is a complex type.
 */
public class InstrumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument
{
    private static final long serialVersionUID = 1L;
    
    public InstrumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName INFO$0 = 
        new javax.xml.namespace.QName("", "info");
    private static final javax.xml.namespace.QName DARK$2 = 
        new javax.xml.namespace.QName("", "dark");
    private static final javax.xml.namespace.QName DIAMETER$4 = 
        new javax.xml.namespace.QName("", "diameter");
    private static final javax.xml.namespace.QName FIBERDIAMETER$6 = 
        new javax.xml.namespace.QName("", "fiberDiameter");
    private static final javax.xml.namespace.QName FILTERTRANSMISSION$8 = 
        new javax.xml.namespace.QName("", "filterTransmission");
    private static final javax.xml.namespace.QName FWHM$10 = 
        new javax.xml.namespace.QName("", "fwhm");
    private static final javax.xml.namespace.QName INSTRUMENTTYPE$12 = 
        new javax.xml.namespace.QName("", "instrumentType");
    private static final javax.xml.namespace.QName NSLIT$14 = 
        new javax.xml.namespace.QName("", "nSlit");
    private static final javax.xml.namespace.QName OBSTRUCTION$16 = 
        new javax.xml.namespace.QName("", "obstruction");
    private static final javax.xml.namespace.QName PIXELSCALE$18 = 
        new javax.xml.namespace.QName("", "pixelScale");
    private static final javax.xml.namespace.QName PSFTYPE$20 = 
        new javax.xml.namespace.QName("", "psfType");
    private static final javax.xml.namespace.QName RANGEMAX$22 = 
        new javax.xml.namespace.QName("", "rangeMax");
    private static final javax.xml.namespace.QName RANGEMIN$24 = 
        new javax.xml.namespace.QName("", "rangeMin");
    private static final javax.xml.namespace.QName READOUT$26 = 
        new javax.xml.namespace.QName("", "readout");
    private static final javax.xml.namespace.QName SLITLENGTH$28 = 
        new javax.xml.namespace.QName("", "slitLength");
    private static final javax.xml.namespace.QName SLITWIDTH$30 = 
        new javax.xml.namespace.QName("", "slitWidth");
    private static final javax.xml.namespace.QName SPECTRALRESOLUTION$32 = 
        new javax.xml.namespace.QName("", "spectralResolution");
    private static final javax.xml.namespace.QName SPECTROGRAPHTYPE$34 = 
        new javax.xml.namespace.QName("", "spectrographType");
    private static final javax.xml.namespace.QName TRANSMISSION$36 = 
        new javax.xml.namespace.QName("", "transmission");
    private static final javax.xml.namespace.QName STREHLRATIO$38 = 
        new javax.xml.namespace.QName("", "strehlRatio");
    private static final javax.xml.namespace.QName REFWAVELENGTH$40 = 
        new javax.xml.namespace.QName("", "refWavelength");
    private static final javax.xml.namespace.QName DELTALAMBDAPERPIXEL$42 = 
        new javax.xml.namespace.QName("", "deltaLambdaPerPixel");
    private static final javax.xml.namespace.QName SPECTRALRESOLUTIONTYPE$44 = 
        new javax.xml.namespace.QName("", "spectralResolutionType");
    private static final javax.xml.namespace.QName PSFDOUBLEGAUSSIANMULTIPLIER$46 = 
        new javax.xml.namespace.QName("", "psfDoubleGaussianMultiplier");
    private static final javax.xml.namespace.QName PSFDOUBLEGAUSSIANFWHM1$48 = 
        new javax.xml.namespace.QName("", "psfDoubleGaussianFwhm1");
    private static final javax.xml.namespace.QName PSFDOUBLEGAUSSIANFWHM2$50 = 
        new javax.xml.namespace.QName("", "psfDoubleGaussianFwhm2");
    private static final javax.xml.namespace.QName PSFSIZETYPE$52 = 
        new javax.xml.namespace.QName("", "psfSizeType");
    private static final javax.xml.namespace.QName FIXEDPSFSIZE$54 = 
        new javax.xml.namespace.QName("", "fixedPsfSize");
    private static final javax.xml.namespace.QName PSFSIZEFUNCTION$56 = 
        new javax.xml.namespace.QName("", "psfSizeFunction");
    private static final javax.xml.namespace.QName PSFSIZEPERCENTAGE$58 = 
        new javax.xml.namespace.QName("", "psfSizePercentage");
    
    
    /**
     * Gets the "info" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo getInfo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo)get_store().find_element_user(INFO$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "info" element
     */
    public void setInfo(org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo info)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo)get_store().find_element_user(INFO$0, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo)get_store().add_element_user(INFO$0);
            }
            target.set(info);
        }
    }
    
    /**
     * Appends and returns a new empty "info" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo addNewInfo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo)get_store().add_element_user(INFO$0);
            return target;
        }
    }
    
    /**
     * Gets the "dark" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getDark()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(DARK$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "dark" element
     */
    public void setDark(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit dark)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(DARK$2, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(DARK$2);
            }
            target.set(dark);
        }
    }
    
    /**
     * Appends and returns a new empty "dark" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewDark()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(DARK$2);
            return target;
        }
    }
    
    /**
     * Gets the "diameter" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getDiameter()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(DIAMETER$4, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "diameter" element
     */
    public void setDiameter(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit diameter)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(DIAMETER$4, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(DIAMETER$4);
            }
            target.set(diameter);
        }
    }
    
    /**
     * Appends and returns a new empty "diameter" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewDiameter()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(DIAMETER$4);
            return target;
        }
    }
    
    /**
     * Gets the "fiberDiameter" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getFiberDiameter()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(FIBERDIAMETER$6, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "fiberDiameter" element
     */
    public void setFiberDiameter(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit fiberDiameter)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(FIBERDIAMETER$6, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(FIBERDIAMETER$6);
            }
            target.set(fiberDiameter);
        }
    }
    
    /**
     * Appends and returns a new empty "fiberDiameter" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewFiberDiameter()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(FIBERDIAMETER$6);
            return target;
        }
    }
    
    /**
     * Gets the "filterTransmission" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.Graph getFilterTransmission()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().find_element_user(FILTERTRANSMISSION$8, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "filterTransmission" element
     */
    public void setFilterTransmission(org.cnrs.lam.dis.etc.dataimportexport.xml.Graph filterTransmission)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().find_element_user(FILTERTRANSMISSION$8, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().add_element_user(FILTERTRANSMISSION$8);
            }
            target.set(filterTransmission);
        }
    }
    
    /**
     * Appends and returns a new empty "filterTransmission" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.Graph addNewFilterTransmission()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().add_element_user(FILTERTRANSMISSION$8);
            return target;
        }
    }
    
    /**
     * Gets the "fwhm" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.Graph getFwhm()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().find_element_user(FWHM$10, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "fwhm" element
     */
    public void setFwhm(org.cnrs.lam.dis.etc.dataimportexport.xml.Graph fwhm)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().find_element_user(FWHM$10, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().add_element_user(FWHM$10);
            }
            target.set(fwhm);
        }
    }
    
    /**
     * Appends and returns a new empty "fwhm" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.Graph addNewFwhm()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().add_element_user(FWHM$10);
            return target;
        }
    }
    
    /**
     * Gets the "instrumentType" element
     */
    public java.lang.String getInstrumentType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(INSTRUMENTTYPE$12, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "instrumentType" element
     */
    public org.apache.xmlbeans.XmlString xgetInstrumentType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(INSTRUMENTTYPE$12, 0);
            return target;
        }
    }
    
    /**
     * Sets the "instrumentType" element
     */
    public void setInstrumentType(java.lang.String instrumentType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(INSTRUMENTTYPE$12, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(INSTRUMENTTYPE$12);
            }
            target.setStringValue(instrumentType);
        }
    }
    
    /**
     * Sets (as xml) the "instrumentType" element
     */
    public void xsetInstrumentType(org.apache.xmlbeans.XmlString instrumentType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(INSTRUMENTTYPE$12, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(INSTRUMENTTYPE$12);
            }
            target.set(instrumentType);
        }
    }
    
    /**
     * Gets the "nSlit" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.IntUnit getNSlit()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.IntUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.IntUnit)get_store().find_element_user(NSLIT$14, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "nSlit" element
     */
    public void setNSlit(org.cnrs.lam.dis.etc.dataimportexport.xml.IntUnit nSlit)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.IntUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.IntUnit)get_store().find_element_user(NSLIT$14, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.IntUnit)get_store().add_element_user(NSLIT$14);
            }
            target.set(nSlit);
        }
    }
    
    /**
     * Appends and returns a new empty "nSlit" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.IntUnit addNewNSlit()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.IntUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.IntUnit)get_store().add_element_user(NSLIT$14);
            return target;
        }
    }
    
    /**
     * Gets the "obstruction" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getObstruction()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(OBSTRUCTION$16, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "obstruction" element
     */
    public void setObstruction(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit obstruction)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(OBSTRUCTION$16, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(OBSTRUCTION$16);
            }
            target.set(obstruction);
        }
    }
    
    /**
     * Appends and returns a new empty "obstruction" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewObstruction()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(OBSTRUCTION$16);
            return target;
        }
    }
    
    /**
     * Gets the "pixelScale" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getPixelScale()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(PIXELSCALE$18, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "pixelScale" element
     */
    public void setPixelScale(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit pixelScale)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(PIXELSCALE$18, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(PIXELSCALE$18);
            }
            target.set(pixelScale);
        }
    }
    
    /**
     * Appends and returns a new empty "pixelScale" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewPixelScale()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(PIXELSCALE$18);
            return target;
        }
    }
    
    /**
     * Gets the "psfType" element
     */
    public java.lang.String getPsfType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PSFTYPE$20, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "psfType" element
     */
    public org.apache.xmlbeans.XmlString xgetPsfType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(PSFTYPE$20, 0);
            return target;
        }
    }
    
    /**
     * Sets the "psfType" element
     */
    public void setPsfType(java.lang.String psfType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PSFTYPE$20, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PSFTYPE$20);
            }
            target.setStringValue(psfType);
        }
    }
    
    /**
     * Sets (as xml) the "psfType" element
     */
    public void xsetPsfType(org.apache.xmlbeans.XmlString psfType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(PSFTYPE$20, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(PSFTYPE$20);
            }
            target.set(psfType);
        }
    }
    
    /**
     * Gets the "rangeMax" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getRangeMax()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(RANGEMAX$22, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "rangeMax" element
     */
    public void setRangeMax(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit rangeMax)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(RANGEMAX$22, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(RANGEMAX$22);
            }
            target.set(rangeMax);
        }
    }
    
    /**
     * Appends and returns a new empty "rangeMax" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewRangeMax()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(RANGEMAX$22);
            return target;
        }
    }
    
    /**
     * Gets the "rangeMin" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getRangeMin()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(RANGEMIN$24, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "rangeMin" element
     */
    public void setRangeMin(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit rangeMin)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(RANGEMIN$24, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(RANGEMIN$24);
            }
            target.set(rangeMin);
        }
    }
    
    /**
     * Appends and returns a new empty "rangeMin" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewRangeMin()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(RANGEMIN$24);
            return target;
        }
    }
    
    /**
     * Gets the "readout" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getReadout()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(READOUT$26, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "readout" element
     */
    public void setReadout(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit readout)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(READOUT$26, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(READOUT$26);
            }
            target.set(readout);
        }
    }
    
    /**
     * Appends and returns a new empty "readout" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewReadout()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(READOUT$26);
            return target;
        }
    }
    
    /**
     * Gets the "slitLength" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getSlitLength()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(SLITLENGTH$28, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "slitLength" element
     */
    public void setSlitLength(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit slitLength)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(SLITLENGTH$28, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(SLITLENGTH$28);
            }
            target.set(slitLength);
        }
    }
    
    /**
     * Appends and returns a new empty "slitLength" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewSlitLength()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(SLITLENGTH$28);
            return target;
        }
    }
    
    /**
     * Gets the "slitWidth" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getSlitWidth()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(SLITWIDTH$30, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "slitWidth" element
     */
    public void setSlitWidth(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit slitWidth)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(SLITWIDTH$30, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(SLITWIDTH$30);
            }
            target.set(slitWidth);
        }
    }
    
    /**
     * Appends and returns a new empty "slitWidth" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewSlitWidth()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(SLITWIDTH$30);
            return target;
        }
    }
    
    /**
     * Gets the "spectralResolution" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.Graph getSpectralResolution()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().find_element_user(SPECTRALRESOLUTION$32, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "spectralResolution" element
     */
    public void setSpectralResolution(org.cnrs.lam.dis.etc.dataimportexport.xml.Graph spectralResolution)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().find_element_user(SPECTRALRESOLUTION$32, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().add_element_user(SPECTRALRESOLUTION$32);
            }
            target.set(spectralResolution);
        }
    }
    
    /**
     * Appends and returns a new empty "spectralResolution" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.Graph addNewSpectralResolution()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().add_element_user(SPECTRALRESOLUTION$32);
            return target;
        }
    }
    
    /**
     * Gets the "spectrographType" element
     */
    public java.lang.String getSpectrographType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SPECTROGRAPHTYPE$34, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "spectrographType" element
     */
    public org.apache.xmlbeans.XmlString xgetSpectrographType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(SPECTROGRAPHTYPE$34, 0);
            return target;
        }
    }
    
    /**
     * Sets the "spectrographType" element
     */
    public void setSpectrographType(java.lang.String spectrographType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SPECTROGRAPHTYPE$34, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SPECTROGRAPHTYPE$34);
            }
            target.setStringValue(spectrographType);
        }
    }
    
    /**
     * Sets (as xml) the "spectrographType" element
     */
    public void xsetSpectrographType(org.apache.xmlbeans.XmlString spectrographType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(SPECTROGRAPHTYPE$34, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(SPECTROGRAPHTYPE$34);
            }
            target.set(spectrographType);
        }
    }
    
    /**
     * Gets the "transmission" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.Graph getTransmission()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().find_element_user(TRANSMISSION$36, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "transmission" element
     */
    public void setTransmission(org.cnrs.lam.dis.etc.dataimportexport.xml.Graph transmission)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().find_element_user(TRANSMISSION$36, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().add_element_user(TRANSMISSION$36);
            }
            target.set(transmission);
        }
    }
    
    /**
     * Appends and returns a new empty "transmission" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.Graph addNewTransmission()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().add_element_user(TRANSMISSION$36);
            return target;
        }
    }
    
    /**
     * Gets the "strehlRatio" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getStrehlRatio()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(STREHLRATIO$38, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "strehlRatio" element
     */
    public void setStrehlRatio(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit strehlRatio)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(STREHLRATIO$38, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(STREHLRATIO$38);
            }
            target.set(strehlRatio);
        }
    }
    
    /**
     * Appends and returns a new empty "strehlRatio" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewStrehlRatio()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(STREHLRATIO$38);
            return target;
        }
    }
    
    /**
     * Gets the "refWavelength" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getRefWavelength()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(REFWAVELENGTH$40, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "refWavelength" element
     */
    public void setRefWavelength(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit refWavelength)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(REFWAVELENGTH$40, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(REFWAVELENGTH$40);
            }
            target.set(refWavelength);
        }
    }
    
    /**
     * Appends and returns a new empty "refWavelength" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewRefWavelength()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(REFWAVELENGTH$40);
            return target;
        }
    }
    
    /**
     * Gets the "deltaLambdaPerPixel" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getDeltaLambdaPerPixel()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(DELTALAMBDAPERPIXEL$42, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "deltaLambdaPerPixel" element
     */
    public void setDeltaLambdaPerPixel(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit deltaLambdaPerPixel)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(DELTALAMBDAPERPIXEL$42, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(DELTALAMBDAPERPIXEL$42);
            }
            target.set(deltaLambdaPerPixel);
        }
    }
    
    /**
     * Appends and returns a new empty "deltaLambdaPerPixel" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewDeltaLambdaPerPixel()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(DELTALAMBDAPERPIXEL$42);
            return target;
        }
    }
    
    /**
     * Gets the "spectralResolutionType" element
     */
    public java.lang.String getSpectralResolutionType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SPECTRALRESOLUTIONTYPE$44, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "spectralResolutionType" element
     */
    public org.apache.xmlbeans.XmlString xgetSpectralResolutionType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(SPECTRALRESOLUTIONTYPE$44, 0);
            return target;
        }
    }
    
    /**
     * Sets the "spectralResolutionType" element
     */
    public void setSpectralResolutionType(java.lang.String spectralResolutionType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SPECTRALRESOLUTIONTYPE$44, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SPECTRALRESOLUTIONTYPE$44);
            }
            target.setStringValue(spectralResolutionType);
        }
    }
    
    /**
     * Sets (as xml) the "spectralResolutionType" element
     */
    public void xsetSpectralResolutionType(org.apache.xmlbeans.XmlString spectralResolutionType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(SPECTRALRESOLUTIONTYPE$44, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(SPECTRALRESOLUTIONTYPE$44);
            }
            target.set(spectralResolutionType);
        }
    }
    
    /**
     * Gets the "psfDoubleGaussianMultiplier" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getPsfDoubleGaussianMultiplier()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(PSFDOUBLEGAUSSIANMULTIPLIER$46, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "psfDoubleGaussianMultiplier" element
     */
    public void setPsfDoubleGaussianMultiplier(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit psfDoubleGaussianMultiplier)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(PSFDOUBLEGAUSSIANMULTIPLIER$46, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(PSFDOUBLEGAUSSIANMULTIPLIER$46);
            }
            target.set(psfDoubleGaussianMultiplier);
        }
    }
    
    /**
     * Appends and returns a new empty "psfDoubleGaussianMultiplier" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewPsfDoubleGaussianMultiplier()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(PSFDOUBLEGAUSSIANMULTIPLIER$46);
            return target;
        }
    }
    
    /**
     * Gets the "psfDoubleGaussianFwhm1" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.Graph getPsfDoubleGaussianFwhm1()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().find_element_user(PSFDOUBLEGAUSSIANFWHM1$48, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "psfDoubleGaussianFwhm1" element
     */
    public void setPsfDoubleGaussianFwhm1(org.cnrs.lam.dis.etc.dataimportexport.xml.Graph psfDoubleGaussianFwhm1)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().find_element_user(PSFDOUBLEGAUSSIANFWHM1$48, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().add_element_user(PSFDOUBLEGAUSSIANFWHM1$48);
            }
            target.set(psfDoubleGaussianFwhm1);
        }
    }
    
    /**
     * Appends and returns a new empty "psfDoubleGaussianFwhm1" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.Graph addNewPsfDoubleGaussianFwhm1()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().add_element_user(PSFDOUBLEGAUSSIANFWHM1$48);
            return target;
        }
    }
    
    /**
     * Gets the "psfDoubleGaussianFwhm2" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.Graph getPsfDoubleGaussianFwhm2()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().find_element_user(PSFDOUBLEGAUSSIANFWHM2$50, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "psfDoubleGaussianFwhm2" element
     */
    public void setPsfDoubleGaussianFwhm2(org.cnrs.lam.dis.etc.dataimportexport.xml.Graph psfDoubleGaussianFwhm2)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().find_element_user(PSFDOUBLEGAUSSIANFWHM2$50, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().add_element_user(PSFDOUBLEGAUSSIANFWHM2$50);
            }
            target.set(psfDoubleGaussianFwhm2);
        }
    }
    
    /**
     * Appends and returns a new empty "psfDoubleGaussianFwhm2" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.Graph addNewPsfDoubleGaussianFwhm2()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().add_element_user(PSFDOUBLEGAUSSIANFWHM2$50);
            return target;
        }
    }
    
    /**
     * Gets the "psfSizeType" element
     */
    public java.lang.String getPsfSizeType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PSFSIZETYPE$52, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "psfSizeType" element
     */
    public org.apache.xmlbeans.XmlString xgetPsfSizeType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(PSFSIZETYPE$52, 0);
            return target;
        }
    }
    
    /**
     * Sets the "psfSizeType" element
     */
    public void setPsfSizeType(java.lang.String psfSizeType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PSFSIZETYPE$52, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PSFSIZETYPE$52);
            }
            target.setStringValue(psfSizeType);
        }
    }
    
    /**
     * Sets (as xml) the "psfSizeType" element
     */
    public void xsetPsfSizeType(org.apache.xmlbeans.XmlString psfSizeType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(PSFSIZETYPE$52, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(PSFSIZETYPE$52);
            }
            target.set(psfSizeType);
        }
    }
    
    /**
     * Gets the "fixedPsfSize" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getFixedPsfSize()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(FIXEDPSFSIZE$54, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "fixedPsfSize" element
     */
    public void setFixedPsfSize(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit fixedPsfSize)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(FIXEDPSFSIZE$54, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(FIXEDPSFSIZE$54);
            }
            target.set(fixedPsfSize);
        }
    }
    
    /**
     * Appends and returns a new empty "fixedPsfSize" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewFixedPsfSize()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(FIXEDPSFSIZE$54);
            return target;
        }
    }
    
    /**
     * Gets the "psfSizeFunction" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.Graph getPsfSizeFunction()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().find_element_user(PSFSIZEFUNCTION$56, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "psfSizeFunction" element
     */
    public void setPsfSizeFunction(org.cnrs.lam.dis.etc.dataimportexport.xml.Graph psfSizeFunction)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().find_element_user(PSFSIZEFUNCTION$56, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().add_element_user(PSFSIZEFUNCTION$56);
            }
            target.set(psfSizeFunction);
        }
    }
    
    /**
     * Appends and returns a new empty "psfSizeFunction" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.Graph addNewPsfSizeFunction()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().add_element_user(PSFSIZEFUNCTION$56);
            return target;
        }
    }
    
    /**
     * Gets the "psfSizePercentage" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getPsfSizePercentage()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(PSFSIZEPERCENTAGE$58, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "psfSizePercentage" element
     */
    public void setPsfSizePercentage(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit psfSizePercentage)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(PSFSIZEPERCENTAGE$58, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(PSFSIZEPERCENTAGE$58);
            }
            target.set(psfSizePercentage);
        }
    }
    
    /**
     * Appends and returns a new empty "psfSizePercentage" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewPsfSizePercentage()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(PSFSIZEPERCENTAGE$58);
            return target;
        }
    }
}
