/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.dataimportexport;

import java.io.File;
import java.io.IOException;
import org.cnrs.lam.dis.etc.datamodel.ComponentInfo;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;

/**
 *
 * @author Nikolaos Apostolakos
 */
public interface DataImporter {

    /**
     * Imports the data from the given file and creates a Dataset with them.
     * The implementations should be able to handle importing from text
     * files containing just data and from files containing components. In the
     * second case the namespace of the file from where the importing is done
     * is ignored.
     * @param file The file to import from
     * @param type The type of the dataset
     * @param info The namespace, name and description of the dataset
     * @param xUnit The unit of the X axis
     * @param yUnit The unit of the Y axis
     * @param dataType The type of the data of the dataset
     * @return A Dataset object containing the data of the file
     * @throws IOException If there is any problem while reading the file
     * @throws WrongFormatException If the data in the file have wrong format
     */
    public Dataset importDatasetFromFile(File file, Dataset.Type type, DatasetInfo info,
            String xUnit, String yUnit, Dataset.DataType dataType) throws IOException, WrongFormatException;

    public Dataset importDatasetFromComponentFile(File file, Dataset.Type type, DatasetInfo info)
            throws IOException, WrongFormatException;

    /**
     * Imports an instrument from a file and returns it as an object. The
     * datasets of the file are NOT imported and they need to be imported
     * with separate calls of the importDatasetFromFile method.
     * @param file The file to import from
     * @param info The name and description of the instrument
     * @return The loaded instrument
     * @throws IOException If there is any problem while reading the file
     * @throws WrongFormatException If the data in the file have wrong format
     */
    public InstrumentWithDatasets importInstrumentFromDataFile(File file, ComponentInfo info)
            throws IOException, WrongFormatException;

    /**
     * Imports a site from a file and returns it as an object. The
     * datasets of the file are NOT imported and they need to be imported
     * with separate calls of the importDatasetFromFile method.
     * @param file The file to import from
     * @param info The name and description of the site
     * @return The loaded site
     * @throws IOException If there is any problem while reading the file
     * @throws WrongFormatException If the data in the file have wrong format
     */
    public SiteWithDatasets importSiteFromFile(File file, ComponentInfo info)
            throws IOException, WrongFormatException;

    /**
     * Imports a source from a file and returns it as an object. The
     * datasets of the file are NOT imported and they need to be imported
     * with separate calls of the importDatasetFromFile method.
     * @param file The file to import from
     * @param info The name and description of the source
     * @return The loaded source
     * @throws IOException If there is any problem while reading the file
     * @throws WrongFormatException If the data in the file have wrong format
     */
    public SourceWithDatasets importSourceFromFile(File file, ComponentInfo info)
            throws IOException, WrongFormatException;

    /**
     * Imports an obs param from a file and returns it as an object. The
     * datasets of the file are NOT imported and they need to be imported
     * with separate calls of the importDatasetFromFile method.
     * @param file The file to import from
     * @param info The name and description of the obs param
     * @return The loaded obs param
     * @throws IOException If there is any problem while reading the file
     * @throws WrongFormatException If the data in the file have wrong format
     */
    public ObsParamWithDatasets importObsParamFromFile(File file, ComponentInfo info)
            throws IOException, WrongFormatException;

	public void checkVotable(File file);
}
