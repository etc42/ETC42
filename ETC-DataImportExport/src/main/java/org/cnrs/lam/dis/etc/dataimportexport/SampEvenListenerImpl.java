/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.dataimportexport;

import cds.savot.model.SavotField;
import cds.savot.model.SavotResource;
import cds.savot.model.SavotVOTable;
import cds.savot.model.TDSet;
import cds.savot.model.TRSet;
import cds.savot.pull.SavotPullEngine;
import cds.savot.pull.SavotPullParser;
import java.util.Map;
import java.util.TreeMap;
import org.apache.log4j.Logger;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.Dataset.Type;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.cnrs.lam.dis.samp.event.SAMPCallEvent;
import org.cnrs.lam.dis.samp.event.SAMPNotificationEvent;
import org.cnrs.lam.dis.samp.event.SAMPResponseEvent;
import org.cnrs.lam.dis.samp.listener.SAMPEventListener;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class SampEvenListenerImpl implements SAMPEventListener {

    private static Logger logger = Logger.getLogger(SampEvenListenerImpl.class);
    private SampListener sampListener;

    public SampEvenListenerImpl(SampListener sampListener) {
        this.sampListener = sampListener;
    }

    @Override
    public void actionPerformed(SAMPNotificationEvent sAMPNotificationEvent) {
        if (!sampListener.confirmImport()) {
            return;
        }
        DatasetInfo info = sampListener.getNewDatasetInfo();

        // TODO: This code is copied from the old system and should be reviewed
        // ********************************************************************************
        
        String ficVO = sAMPNotificationEvent.getMessage().getParams().get("url").toString();
        System.out.println(ficVO.substring(16));
        SavotPullParser sb = new SavotPullParser(ficVO.substring(16),SavotPullEngine.FULL);
        SavotVOTable sv = sb.getVOTable();
        int indexCurrentResource = 0;
        SavotResource currentResource = (SavotResource)(sv.getResources().getItemAt(indexCurrentResource));
        int indexTable = getIndexTable(currentResource);
        TRSet tr = currentResource.getTRSet(indexTable);
        double[] x = new double[tr.getItemCount()] ;
        double[] y = new double[tr.getItemCount()] ;
        // idem est verifie deux Filed declaree
        if (currentResource.getFieldSet(indexTable).getItemCount() != 2) {
            sampListener.importFailed();
            return;
        }
        // identifier indice de X et indice de Y et les unités;
	       int indiceX;
        int indiceY;
        if (((SavotField) currentResource.getFieldSet(indexTable).getItemAt(0)).getUtype().equals("Data.FluxAxis.Value")
                && ((SavotField) currentResource.getFieldSet(indexTable).getItemAt(1)).getUtype().equals("Data.SpectralAxis.Value")) {
            indiceX = 1;
            indiceY = 0;
        } else if (((SavotField) currentResource.getFieldSet(indexTable).getItemAt(1)).getUtype().equals("Data.FluxAxis.Value")
                && ((SavotField) currentResource.getFieldSet(indexTable).getItemAt(0)).getUtype().equals("Data.SpectralAxis.Value")) {
            indiceX = 0;
            indiceY = 1;
        } else {
            sampListener.importFailed();
            return;
        }
        // Recupérer les unités depuis les champs Field
        String unitX = ((SavotField) currentResource.getFieldSet(indexTable).getItemAt(indiceX)).getUnit();
        String unitY = ((SavotField)currentResource.getFieldSet(indexTable).getItemAt(indiceY)).getUnit();

        System.out.println(unitX);
        System.out.println(unitY);

        TDSet theTDs;
        // Chaque ligne est chargée une a une
        for (int i = 0; i < tr.getItemCount(); i++) {
            // get all the data of the row
            theTDs = tr.getTDSet(i);
            // verifie que seulement 2 donnes par ligne, sinon stop
            if (theTDs.getItemCount() != 2) {
                sampListener.importFailed();
                return;
            }
            try {
                //conversion des X en angstrom
                x[i] = UnitHelper.omniParseX(Double.valueOf(theTDs.getContent(1)).doubleValue(), unitX);

                //conversion des Y en erg/s/cm²/Ang
                y[i] = UnitHelper.omniParseY(Double.valueOf(theTDs.getContent(0)).doubleValue(), x[i], unitY);
            } catch (Exception ex) {
                logger.error("Failed to convert to the correct unit data from SAMP", ex);
                sampListener.importFailed();
                return;
            }
        }
        String qunitX = "angstrom";
        String qunitY = "erg/s/cm2/Ang";
        // ********************************************************************************
        Map<Double, Double> data = new TreeMap<Double, Double>();
        for (int i = 0; i < x.length; i++) {
            data.put(x[i], y[i]);
        }
        /** ANTHONY K GROSS **/
        Dataset dataset = new DatasetImpl(info, Type.SPECTRAL_DIST_TEMPLATE, data, qunitX, qunitY, Dataset.DataType.FUNCTION);
        sampListener.datasetImported(dataset);
    }

    // TODO: Copied from old code and need reviewing
    private int getIndexTable(SavotResource currentResource) {
        int index = 1;
        // TODO A FINIR lundi ;)
        // D'abord cas normal ou il n'y a qu'une table dans la VOTAble
        if (currentResource.getTableCount() == 1) {
            index = 0;
        } // ensuite les autres qui doivent pas arriver et qui arrivent
        else {
            TRSet trtest;
            // Pour chaque Table on regarde celui a qui des donnees dedans
            // et on choisi la derniere table avec des donnees ..
            for (int i = 0; i < currentResource.getTableCount(); i++) {
                try {
                    trtest = currentResource.getTRSet(i);
                    if (trtest.getItemCount() > 0) {
                        index = i;
                    }
                } catch (Exception ex) {
                }
            }
        }
        return index;
    }

    @Override
    public void actionPerformed(SAMPCallEvent sAMPCallEvent) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void actionPerformed(SAMPResponseEvent sAMPResponseEvent) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
