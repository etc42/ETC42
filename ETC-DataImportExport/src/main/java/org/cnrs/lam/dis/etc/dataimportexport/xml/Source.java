/*
 * XML Type:  Source
 * Namespace: 
 * Java type: org.cnrs.lam.dis.etc.dataimportexport.xml.Source
 *
 * Automatically generated - do not modify.
 */
package org.cnrs.lam.dis.etc.dataimportexport.xml;


/**
 * An XML Source(@).
 *
 * This is a complex type.
 */
public interface Source extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Source.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sCF658226FCE8387FD3C82F8A9EBA922F").resolveHandle("source2be4type");
    
    /**
     * Gets the "info" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo getInfo();
    
    /**
     * Sets the "info" element
     */
    void setInfo(org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo info);
    
    /**
     * Appends and returns a new empty "info" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo addNewInfo();
    
    /**
     * Gets the "magnitudeWavelength" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getMagnitudeWavelength();
    
    /**
     * Sets the "magnitudeWavelength" element
     */
    void setMagnitudeWavelength(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit magnitudeWavelength);
    
    /**
     * Appends and returns a new empty "magnitudeWavelength" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewMagnitudeWavelength();
    
    /**
     * Gets the "emissionLineWavelength" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getEmissionLineWavelength();
    
    /**
     * Sets the "emissionLineWavelength" element
     */
    void setEmissionLineWavelength(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit emissionLineWavelength);
    
    /**
     * Appends and returns a new empty "emissionLineWavelength" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewEmissionLineWavelength();
    
    /**
     * Gets the "emissionLineFwhm" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getEmissionLineFwhm();
    
    /**
     * Sets the "emissionLineFwhm" element
     */
    void setEmissionLineFwhm(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit emissionLineFwhm);
    
    /**
     * Appends and returns a new empty "emissionLineFwhm" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewEmissionLineFwhm();
    
    /**
     * Gets the "extendedSourceProfile" element
     */
    java.lang.String getExtendedSourceProfile();
    
    /**
     * Gets (as xml) the "extendedSourceProfile" element
     */
    org.apache.xmlbeans.XmlString xgetExtendedSourceProfile();
    
    /**
     * Sets the "extendedSourceProfile" element
     */
    void setExtendedSourceProfile(java.lang.String extendedSourceProfile);
    
    /**
     * Sets (as xml) the "extendedSourceProfile" element
     */
    void xsetExtendedSourceProfile(org.apache.xmlbeans.XmlString extendedSourceProfile);
    
    /**
     * Gets the "extendedSourceRadius" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getExtendedSourceRadius();
    
    /**
     * Sets the "extendedSourceRadius" element
     */
    void setExtendedSourceRadius(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit extendedSourceRadius);
    
    /**
     * Appends and returns a new empty "extendedSourceRadius" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewExtendedSourceRadius();
    
    /**
     * Gets the "magnitude" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getMagnitude();
    
    /**
     * Sets the "magnitude" element
     */
    void setMagnitude(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit magnitude);
    
    /**
     * Appends and returns a new empty "magnitude" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewMagnitude();
    
    /**
     * Gets the "redshift" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getRedshift();
    
    /**
     * Sets the "redshift" element
     */
    void setRedshift(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit redshift);
    
    /**
     * Appends and returns a new empty "redshift" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewRedshift();
    
    /**
     * Gets the "temperature" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getTemperature();
    
    /**
     * Sets the "temperature" element
     */
    void setTemperature(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit temperature);
    
    /**
     * Appends and returns a new empty "temperature" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewTemperature();
    
    /**
     * Gets the "spatialDistributionType" element
     */
    java.lang.String getSpatialDistributionType();
    
    /**
     * Gets (as xml) the "spatialDistributionType" element
     */
    org.apache.xmlbeans.XmlString xgetSpatialDistributionType();
    
    /**
     * Sets the "spatialDistributionType" element
     */
    void setSpatialDistributionType(java.lang.String spatialDistributionType);
    
    /**
     * Sets (as xml) the "spatialDistributionType" element
     */
    void xsetSpatialDistributionType(org.apache.xmlbeans.XmlString spatialDistributionType);
    
    /**
     * Gets the "spectralDistributionType" element
     */
    java.lang.String getSpectralDistributionType();
    
    /**
     * Gets (as xml) the "spectralDistributionType" element
     */
    org.apache.xmlbeans.XmlString xgetSpectralDistributionType();
    
    /**
     * Sets the "spectralDistributionType" element
     */
    void setSpectralDistributionType(java.lang.String spectralDistributionType);
    
    /**
     * Sets (as xml) the "spectralDistributionType" element
     */
    void xsetSpectralDistributionType(org.apache.xmlbeans.XmlString spectralDistributionType);
    
    /**
     * Gets the "spectralDistributionTemplate" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.Graph getSpectralDistributionTemplate();
    
    /**
     * True if has "spectralDistributionTemplate" element
     */
    boolean isSetSpectralDistributionTemplate();
    
    /**
     * Sets the "spectralDistributionTemplate" element
     */
    void setSpectralDistributionTemplate(org.cnrs.lam.dis.etc.dataimportexport.xml.Graph spectralDistributionTemplate);
    
    /**
     * Appends and returns a new empty "spectralDistributionTemplate" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.Graph addNewSpectralDistributionTemplate();
    
    /**
     * Unsets the "spectralDistributionTemplate" element
     */
    void unsetSpectralDistributionTemplate();
    
    /**
     * Gets the "emissionLineFlux" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getEmissionLineFlux();
    
    /**
     * Sets the "emissionLineFlux" element
     */
    void setEmissionLineFlux(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit emissionLineFlux);
    
    /**
     * Appends and returns a new empty "emissionLineFlux" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewEmissionLineFlux();
    
    /**
     * Gets the "extendedMagnitudeType" element
     */
    java.lang.String getExtendedMagnitudeType();
    
    /**
     * Gets (as xml) the "extendedMagnitudeType" element
     */
    org.apache.xmlbeans.XmlString xgetExtendedMagnitudeType();
    
    /**
     * Sets the "extendedMagnitudeType" element
     */
    void setExtendedMagnitudeType(java.lang.String extendedMagnitudeType);
    
    /**
     * Sets (as xml) the "extendedMagnitudeType" element
     */
    void xsetExtendedMagnitudeType(org.apache.xmlbeans.XmlString extendedMagnitudeType);
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Source newInstance() {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Source) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Source newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Source) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Source parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Source) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Source parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Source) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Source parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Source) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Source parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Source) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Source parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Source) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Source parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Source) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Source parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Source) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Source parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Source) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Source parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Source) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Source parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Source) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Source parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Source) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Source parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Source) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Source parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Source) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Source parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Source) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Source parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Source) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Source parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Source) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
