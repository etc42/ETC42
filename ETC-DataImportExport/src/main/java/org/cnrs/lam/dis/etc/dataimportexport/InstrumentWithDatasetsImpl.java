/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.dataimportexport;

import org.cnrs.lam.dis.etc.dataimportexport.xml.Graph;
import org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument;
import org.cnrs.lam.dis.etc.datamodel.ComponentInfo;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class InstrumentWithDatasetsImpl implements InstrumentWithDatasets {

    private Instrument instrumentXml;
    private ComponentInfo componentInfo;

    public InstrumentWithDatasetsImpl(Instrument instrumentXml, ComponentInfo componentInfo) {
        this.instrumentXml = instrumentXml;
        this.componentInfo = componentInfo;
    }

    @Override
    public Dataset getFilterTransmissionDataset() {
        Graph graph = instrumentXml.getFilterTransmission();
        return Helper.convertGraphToDataset(graph, componentInfo.getName(), Dataset.Type.FILTER_TRANSMISSION);
    }

    @Override
    public Dataset getFwhmDataset() {
        Graph graph = instrumentXml.getFwhm();
        return Helper.convertGraphToDataset(graph, componentInfo.getName(), Dataset.Type.FWHM);
    }

    @Override
    public Dataset getSpectralResolutionDataset() {
        Graph graph = instrumentXml.getSpectralResolution();
        return Helper.convertGraphToDataset(graph, componentInfo.getName(), Dataset.Type.SPECTRAL_RESOLUTION);
    }

    @Override
    public Dataset getTransmissionDataset() {
        Graph graph = instrumentXml.getTransmission();
        return Helper.convertGraphToDataset(graph, componentInfo.getName(), Dataset.Type.TRANSMISSION);
    }
    
    @Override
    public PsfType getPsfType() {
        return PsfType.valueOf(instrumentXml.getPsfType());
    }

    @Override
    public void setPsfType(PsfType type) {
        throw new UnsupportedOperationException("Read only version of Instrument");
    }

    @Override
    public InstrumentType getInstrumentType() {
        return InstrumentType.valueOf(instrumentXml.getInstrumentType());
    }

    @Override
    public void setInstrumentType(InstrumentType type) {
        throw new UnsupportedOperationException("Read only version of Instrument");
    }

    @Override
    public ComponentInfo getInfo() {
        return componentInfo;
    }

    @Override
    public SpectrographType getSpectrographType() {
        return SpectrographType.valueOf(instrumentXml.getSpectrographType());
    }

    @Override
    public void setSpectrographType(SpectrographType type) {
        throw new UnsupportedOperationException("Read only version of Instrument");
    }

    @Override
    public double getDiameter() {
        return instrumentXml.getDiameter().getValue();
    }

    @Override
    public void setDiameter(double diameter) {
        throw new UnsupportedOperationException("Read only version of Instrument");
    }

    @Override
    public String getDiameterUnit() {
        return instrumentXml.getDiameter().getUnit();
    }

    @Override
    public double getObstruction() {
        return instrumentXml.getObstruction().getValue();
    }

    @Override
    public void setObstruction(double obstruction) {
        throw new UnsupportedOperationException("Read only version of Instrument");
    }

    @Override
    public double getReadout() {
        return instrumentXml.getReadout().getValue();
    }

    @Override
    public void setReadout(double readout) {
        throw new UnsupportedOperationException("Read only version of Instrument");
    }

    @Override
    public String getReadoutUnit() {
        return instrumentXml.getReadout().getUnit();
    }

    @Override
    public double getDark() {
        return instrumentXml.getDark().getValue();
    }

    @Override
    public void setDark(double dark) {
        throw new UnsupportedOperationException("Read only version of Instrument");
    }

    @Override
    public String getDarkUnit() {
        return instrumentXml.getDark().getUnit();
    }

    @Override
    public double getPixelScale() {
        return instrumentXml.getPixelScale().getValue();
    }

    @Override
    public void setPixelScale(double pixelScale) {
        throw new UnsupportedOperationException("Read only version of Instrument");
    }

    @Override
    public String getPixelScaleUnit() {
        return instrumentXml.getPixelScale().getUnit();
    }

    @Override
    public DatasetInfo getFwhm() {
        if (instrumentXml.getFwhm() == null)
            return null;
        return new DatasetInfo(instrumentXml.getFwhm().getName(), componentInfo.getName(), null);
    }

    @Override
    public void setFwhm(DatasetInfo fwhm) {
        throw new UnsupportedOperationException("Read only version of Instrument");
    }

    @Override
    public DatasetInfo getTransmission() {
        if (instrumentXml.getTransmission() == null)
            return null;
        return new DatasetInfo(instrumentXml.getTransmission().getName(), componentInfo.getName(), null);
    }

    @Override
    public void setTransmission(DatasetInfo transmission) {
        throw new UnsupportedOperationException("Read only version of Instrument");
    }

    @Override
    public double getRangeMin() {
        return instrumentXml.getRangeMin().getValue();
    }

    @Override
    public void setRangeMin(double rangeMin) {
        throw new UnsupportedOperationException("Read only version of Instrument");
    }

    @Override
    public String getRangeMinUnit() {
        return instrumentXml.getRangeMin().getUnit();
    }

    @Override
    public double getRangeMax() {
        return instrumentXml.getRangeMax().getValue();
    }

    @Override
    public void setRangeMax(double rangeMax) {
        throw new UnsupportedOperationException("Read only version of Instrument");
    }

    @Override
    public String getRangeMaxUnit() {
        return instrumentXml.getRangeMax().getUnit();
    }

    @Override
    public double getSlitLength() {
        return instrumentXml.getSlitLength().getValue();
    }

    @Override
    public void setSlitLength(double slitLength) {
        throw new UnsupportedOperationException("Read only version of Instrument");
    }

    @Override
    public String getSlitLengthUnit() {
        return instrumentXml.getSlitLength().getUnit();
    }

    @Override
    public double getSlitWidth() {
        return instrumentXml.getSlitWidth().getValue();
    }

    @Override
    public void setSlitWidth(double slitWidth) {
        throw new UnsupportedOperationException("Read only version of Instrument");
    }

    @Override
    public String getSlitWidthUnit() {
        return instrumentXml.getSlitWidth().getUnit();
    }

    @Override
    public int getNSlit() {
        return instrumentXml.getNSlit().getValue().intValue();
    }

    @Override
    public void setNSlit(int nSlit) {
        throw new UnsupportedOperationException("Read only version of Instrument");
    }

    @Override
    public double getFiberDiameter() {
        return instrumentXml.getFiberDiameter().getValue();
    }

    @Override
    public void setFiberDiameter(double fiberDiameter) {
        throw new UnsupportedOperationException("Read only version of Instrument");
    }

    @Override
    public String getFiberDiameterUnit() {
        return instrumentXml.getFiberDiameter().getUnit();
    }

    @Override
    public DatasetInfo getSpectralResolution() {
        if (instrumentXml.getSpectralResolution() == null)
            return null;
        return new DatasetInfo(instrumentXml.getSpectralResolution().getName(), componentInfo.getName(), null);
    }

    @Override
    public void setSpectralResolution(DatasetInfo spectralResolution) {
        throw new UnsupportedOperationException("Read only version of Instrument");
    }

    @Override
    public DatasetInfo getFilterTransmission() {
        if (instrumentXml.getFilterTransmission() == null)
            return null;
        return new DatasetInfo(instrumentXml.getFilterTransmission().getName(), componentInfo.getName(), null);
    }

    @Override
    public void setFilterTransmission(DatasetInfo filterTransmission) {
        throw new UnsupportedOperationException("Read only version of Instrument");
    }
    
    @Override
    public double getStrehlRatio() {
        return instrumentXml.getStrehlRatio().getValue();
    }

    @Override
    public void setStrehlRatio(double strehlRatio) {
        throw new UnsupportedOperationException("Read only version of Instrument");
    }
    
    @Override
    public double getRefWavelength() {
        return instrumentXml.getRefWavelength().getValue();
    }

    @Override
    public void setRefWavelength(double refWavelength) {
        throw new UnsupportedOperationException("Read only version of Instrument");
    }

    @Override
    public String getRefWavelengthUnit() {
        return instrumentXml.getRefWavelength().getUnit();
    }

    @Override
    public double getDeltaLambdaPerPixel() {
        return instrumentXml.getDeltaLambdaPerPixel().getValue();
    }

    @Override
    public void setDeltaLambdaPerPixel(double deltaLambda) {
        throw new UnsupportedOperationException("Read only version of Instrument");
    }

    @Override
    public String getDeltaLambdaPerPixelUnit() {
        return instrumentXml.getDeltaLambdaPerPixel().getUnit();
    }

    @Override
    public SpectralResolutionType getSpectralResolutionType() {
        return SpectralResolutionType.valueOf(instrumentXml.getSpectralResolutionType());
    }

    @Override
    public void setSpectralResolutionType(SpectralResolutionType type) {
        throw new UnsupportedOperationException("Read only version of Instrument");
    }
    
    @Override
    public double getPsfDoubleGaussianMultiplier() {
        return instrumentXml.getPsfDoubleGaussianMultiplier().getValue();
    }

    @Override
    public void setPsfDoubleGaussianMultiplier(double multiplier) {
        throw new UnsupportedOperationException("Read only version of Instrument");
    }

    @Override
    public Dataset getPsfDoubleGaussianFwhm1Dataset() {
        Graph graph = instrumentXml.getPsfDoubleGaussianFwhm1();
        return Helper.convertGraphToDataset(graph, componentInfo.getName(), Dataset.Type.PSF_DOUBLE_GAUSSIAN_FWHM_1);
    }

    @Override
    public DatasetInfo getPsfDoubleGaussianFwhm1() {
        if (instrumentXml.getPsfDoubleGaussianFwhm1() == null)
            return null;
        return new DatasetInfo(instrumentXml.getPsfDoubleGaussianFwhm1().getName(), componentInfo.getName(), null);
    }

    @Override
    public void setPsfDoubleGaussianFwhm1(DatasetInfo fwhm) {
        throw new UnsupportedOperationException("Read only version of Instrument");
    }

    @Override
    public Dataset getPsfDoubleGaussianFwhm2Dataset() {
        Graph graph = instrumentXml.getPsfDoubleGaussianFwhm2();
        return Helper.convertGraphToDataset(graph, componentInfo.getName(), Dataset.Type.PSF_DOUBLE_GAUSSIAN_FWHM_2);
    }

    @Override
    public DatasetInfo getPsfDoubleGaussianFwhm2() {
        if (instrumentXml.getPsfDoubleGaussianFwhm2() == null)
            return null;
        return new DatasetInfo(instrumentXml.getPsfDoubleGaussianFwhm2().getName(), componentInfo.getName(), null);
    }

    @Override
    public void setPsfDoubleGaussianFwhm2(DatasetInfo fwhm) {
        throw new UnsupportedOperationException("Read only version of Instrument");
    }
    
    @Override
    public PsfSizeType getPsfSizeType() {
        return PsfSizeType.valueOf(instrumentXml.getPsfSizeType());
    }

    @Override
    public void setPsfSizeType(PsfSizeType type) {
        throw new UnsupportedOperationException("Read only version of Instrument");
    }

    @Override
    public double getFixedPsfSize() {
        return instrumentXml.getFixedPsfSize().getValue();
    }

    @Override
    public void setFixedPsfSize(double size) {
        throw new UnsupportedOperationException("Read only version of Instrument");
    }

    @Override
    public String getFixedPsfSizeUnit() {
        return instrumentXml.getFixedPsfSize().getUnit();
    }

    @Override
    public Dataset getPsfSizeFunctionDataset() {
        Graph graph = instrumentXml.getPsfSizeFunction();
        return Helper.convertGraphToDataset(graph, componentInfo.getName(), Dataset.Type.PSF_SIZE_FUNCTION);
    }

    @Override
    public DatasetInfo getPsfSizeFunction() {
        if (instrumentXml.getPsfSizeFunction() == null)
            return null;
        return new DatasetInfo(instrumentXml.getPsfSizeFunction().getName(), componentInfo.getName(), null);
    }

    @Override
    public void setPsfSizeFunction(DatasetInfo info) {
        throw new UnsupportedOperationException("Read only version of Instrument");
    }

    @Override
    public double getPsfSizePercentage() {
        return instrumentXml.getPsfSizePercentage().getValue();
    }

    @Override
    public void setPsfSizePercentage(double percentage) {
        throw new UnsupportedOperationException("Read only version of Instrument");
    }
}
