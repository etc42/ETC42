/*
 * An XML document type.
 * Localname: source
 * Namespace: 
 * Java type: org.cnrs.lam.dis.etc.dataimportexport.xml.SourceDocument
 *
 * Automatically generated - do not modify.
 */
package org.cnrs.lam.dis.etc.dataimportexport.xml.impl;
/**
 * A document containing one source(@) element.
 *
 * This is a complex type.
 */
public class SourceDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements org.cnrs.lam.dis.etc.dataimportexport.xml.SourceDocument
{
    private static final long serialVersionUID = 1L;
    
    public SourceDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName SOURCE$0 = 
        new javax.xml.namespace.QName("", "source");
    
    
    /**
     * Gets the "source" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.Source getSource()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Source target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Source)get_store().find_element_user(SOURCE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "source" element
     */
    public void setSource(org.cnrs.lam.dis.etc.dataimportexport.xml.Source source)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Source target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Source)get_store().find_element_user(SOURCE$0, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Source)get_store().add_element_user(SOURCE$0);
            }
            target.set(source);
        }
    }
    
    /**
     * Appends and returns a new empty "source" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.Source addNewSource()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Source target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Source)get_store().add_element_user(SOURCE$0);
            return target;
        }
    }
}
