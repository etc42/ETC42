/*
 * An XML document type.
 * Localname: instrument
 * Namespace: 
 * Java type: org.cnrs.lam.dis.etc.dataimportexport.xml.InstrumentDocument
 *
 * Automatically generated - do not modify.
 */
package org.cnrs.lam.dis.etc.dataimportexport.xml.impl;
/**
 * A document containing one instrument(@) element.
 *
 * This is a complex type.
 */
public class InstrumentDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements org.cnrs.lam.dis.etc.dataimportexport.xml.InstrumentDocument
{
    private static final long serialVersionUID = 1L;
    
    public InstrumentDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName INSTRUMENT$0 = 
        new javax.xml.namespace.QName("", "instrument");
    
    
    /**
     * Gets the "instrument" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument getInstrument()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument)get_store().find_element_user(INSTRUMENT$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "instrument" element
     */
    public void setInstrument(org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument instrument)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument)get_store().find_element_user(INSTRUMENT$0, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument)get_store().add_element_user(INSTRUMENT$0);
            }
            target.set(instrument);
        }
    }
    
    /**
     * Appends and returns a new empty "instrument" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument addNewInstrument()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument)get_store().add_element_user(INSTRUMENT$0);
            return target;
        }
    }
}
