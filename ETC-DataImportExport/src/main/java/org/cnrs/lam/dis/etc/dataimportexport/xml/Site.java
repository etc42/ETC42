/*
 * XML Type:  Site
 * Namespace: 
 * Java type: org.cnrs.lam.dis.etc.dataimportexport.xml.Site
 *
 * Automatically generated - do not modify.
 */
package org.cnrs.lam.dis.etc.dataimportexport.xml;


/**
 * An XML Site(@).
 *
 * This is a complex type.
 */
public interface Site extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Site.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sCF658226FCE8387FD3C82F8A9EBA922F").resolveHandle("site0150type");
    
    /**
     * Gets the "info" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo getInfo();
    
    /**
     * Sets the "info" element
     */
    void setInfo(org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo info);
    
    /**
     * Appends and returns a new empty "info" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo addNewInfo();
    
    /**
     * Gets the "airMass" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getAirMass();
    
    /**
     * Sets the "airMass" element
     */
    void setAirMass(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit airMass);
    
    /**
     * Appends and returns a new empty "airMass" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewAirMass();
    
    /**
     * Gets the "galacticContributing" element
     */
    boolean getGalacticContributing();
    
    /**
     * Gets (as xml) the "galacticContributing" element
     */
    org.apache.xmlbeans.XmlBoolean xgetGalacticContributing();
    
    /**
     * Sets the "galacticContributing" element
     */
    void setGalacticContributing(boolean galacticContributing);
    
    /**
     * Sets (as xml) the "galacticContributing" element
     */
    void xsetGalacticContributing(org.apache.xmlbeans.XmlBoolean galacticContributing);
    
    /**
     * Gets the "galacticProfile" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.Graph getGalacticProfile();
    
    /**
     * Sets the "galacticProfile" element
     */
    void setGalacticProfile(org.cnrs.lam.dis.etc.dataimportexport.xml.Graph galacticProfile);
    
    /**
     * Appends and returns a new empty "galacticProfile" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.Graph addNewGalacticProfile();
    
    /**
     * Gets the "locationType" element
     */
    java.lang.String getLocationType();
    
    /**
     * Gets (as xml) the "locationType" element
     */
    org.apache.xmlbeans.XmlString xgetLocationType();
    
    /**
     * Sets the "locationType" element
     */
    void setLocationType(java.lang.String locationType);
    
    /**
     * Sets (as xml) the "locationType" element
     */
    void xsetLocationType(org.apache.xmlbeans.XmlString locationType);
    
    /**
     * Gets the "seeing" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getSeeing();
    
    /**
     * Sets the "seeing" element
     */
    void setSeeing(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit seeing);
    
    /**
     * Appends and returns a new empty "seeing" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewSeeing();
    
    /**
     * Gets the "skyAbsorption" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.Graph getSkyAbsorption();
    
    /**
     * Sets the "skyAbsorption" element
     */
    void setSkyAbsorption(org.cnrs.lam.dis.etc.dataimportexport.xml.Graph skyAbsorption);
    
    /**
     * Appends and returns a new empty "skyAbsorption" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.Graph addNewSkyAbsorption();
    
    /**
     * Gets the "skyEmissionSelectedOption" element
     */
    java.lang.String getSkyEmissionSelectedOption();
    
    /**
     * Gets (as xml) the "skyEmissionSelectedOption" element
     */
    org.apache.xmlbeans.XmlString xgetSkyEmissionSelectedOption();
    
    /**
     * Sets the "skyEmissionSelectedOption" element
     */
    void setSkyEmissionSelectedOption(java.lang.String skyEmissionSelectedOption);
    
    /**
     * Sets (as xml) the "skyEmissionSelectedOption" element
     */
    void xsetSkyEmissionSelectedOption(org.apache.xmlbeans.XmlString skyEmissionSelectedOption);
    
    /**
     * Gets array of all "skyEmission" elements
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.Graph[] getSkyEmissionArray();
    
    /**
     * Gets ith "skyEmission" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.Graph getSkyEmissionArray(int i);
    
    /**
     * Returns number of "skyEmission" element
     */
    int sizeOfSkyEmissionArray();
    
    /**
     * Sets array of all "skyEmission" element
     */
    void setSkyEmissionArray(org.cnrs.lam.dis.etc.dataimportexport.xml.Graph[] skyEmissionArray);
    
    /**
     * Sets ith "skyEmission" element
     */
    void setSkyEmissionArray(int i, org.cnrs.lam.dis.etc.dataimportexport.xml.Graph skyEmission);
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "skyEmission" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.Graph insertNewSkyEmission(int i);
    
    /**
     * Appends and returns a new empty value (as xml) as the last "skyEmission" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.Graph addNewSkyEmission();
    
    /**
     * Removes the ith "skyEmission" element
     */
    void removeSkyEmission(int i);
    
    /**
     * Gets the "skyExtinction" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.Graph getSkyExtinction();
    
    /**
     * Sets the "skyExtinction" element
     */
    void setSkyExtinction(org.cnrs.lam.dis.etc.dataimportexport.xml.Graph skyExtinction);
    
    /**
     * Appends and returns a new empty "skyExtinction" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.Graph addNewSkyExtinction();
    
    /**
     * Gets the "zodiacalContributing" element
     */
    boolean getZodiacalContributing();
    
    /**
     * Gets (as xml) the "zodiacalContributing" element
     */
    org.apache.xmlbeans.XmlBoolean xgetZodiacalContributing();
    
    /**
     * Sets the "zodiacalContributing" element
     */
    void setZodiacalContributing(boolean zodiacalContributing);
    
    /**
     * Sets (as xml) the "zodiacalContributing" element
     */
    void xsetZodiacalContributing(org.apache.xmlbeans.XmlBoolean zodiacalContributing);
    
    /**
     * Gets the "seeingLimited" element
     */
    boolean getSeeingLimited();
    
    /**
     * Gets (as xml) the "seeingLimited" element
     */
    org.apache.xmlbeans.XmlBoolean xgetSeeingLimited();
    
    /**
     * Sets the "seeingLimited" element
     */
    void setSeeingLimited(boolean seeingLimited);
    
    /**
     * Sets (as xml) the "seeingLimited" element
     */
    void xsetSeeingLimited(org.apache.xmlbeans.XmlBoolean seeingLimited);
    
    /**
     * Gets the "zodiacalProfile" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.Graph getZodiacalProfile();
    
    /**
     * Sets the "zodiacalProfile" element
     */
    void setZodiacalProfile(org.cnrs.lam.dis.etc.dataimportexport.xml.Graph zodiacalProfile);
    
    /**
     * Appends and returns a new empty "zodiacalProfile" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.Graph addNewZodiacalProfile();
    
    /**
     * Gets the "atmosphericTransmission" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.Graph getAtmosphericTransmission();
    
    /**
     * Sets the "atmosphericTransmission" element
     */
    void setAtmosphericTransmission(org.cnrs.lam.dis.etc.dataimportexport.xml.Graph atmosphericTransmission);
    
    /**
     * Appends and returns a new empty "atmosphericTransmission" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.Graph addNewAtmosphericTransmission();
    
    /**
     * Gets the "atmosphericTransmissionType" element
     */
    java.lang.String getAtmosphericTransmissionType();
    
    /**
     * Gets (as xml) the "atmosphericTransmissionType" element
     */
    org.apache.xmlbeans.XmlString xgetAtmosphericTransmissionType();
    
    /**
     * Sets the "atmosphericTransmissionType" element
     */
    void setAtmosphericTransmissionType(java.lang.String atmosphericTransmissionType);
    
    /**
     * Sets (as xml) the "atmosphericTransmissionType" element
     */
    void xsetAtmosphericTransmissionType(org.apache.xmlbeans.XmlString atmosphericTransmissionType);
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Site newInstance() {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Site) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Site newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Site) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Site parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Site) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Site parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Site) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Site parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Site) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Site parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Site) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Site parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Site) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Site parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Site) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Site parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Site) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Site parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Site) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Site parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Site) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Site parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Site) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Site parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Site) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Site parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Site) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Site parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Site) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Site parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Site) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Site parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Site) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Site parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Site) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
