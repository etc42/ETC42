/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.dataimportexport;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

import org.apache.xmlbeans.XmlException;
import org.cnrs.lam.dis.etc.dataimportexport.xml.InstrumentDocument;
import org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParametersDocument;
import org.cnrs.lam.dis.etc.dataimportexport.xml.SiteDocument;
import org.cnrs.lam.dis.etc.dataimportexport.xml.SourceDocument;
import org.cnrs.lam.dis.etc.datamodel.ComponentInfo;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.Dataset.Type;

import cds.savot.model.SavotField;
import cds.savot.model.SavotResource;
import cds.savot.model.SavotVOTable;
import cds.savot.model.TDSet;
import cds.savot.model.TRSet;
import cds.savot.pull.SavotPullEngine;
import cds.savot.pull.SavotPullParser;

import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class DataImporterImpl implements DataImporter {

    @Override
    public Dataset importDatasetFromComponentFile(File file, Type type, DatasetInfo info) throws IOException, WrongFormatException {
        Dataset dataset = null;
        switch (type) {
            case FWHM:
            case FILTER_TRANSMISSION:
            case SPECTRAL_RESOLUTION:
            case TRANSMISSION:
            case PSF_DOUBLE_GAUSSIAN_FWHM_1:
            case PSF_DOUBLE_GAUSSIAN_FWHM_2:
            case PSF_SIZE_FUNCTION:
                dataset = getDatasetFromInstrument(file, type, info);
                break;
            case GALACTIC:
            case ZODIACAL:
            case SKY_ABSORPTION:
            case SKY_EMISSION:
            case SKY_EXTINCTION:
            case ATMOSPHERIC_TRANSMISSION:
                dataset = getDatasetFromSite(file, type, info);
                break;
            case SPECTRAL_DIST_TEMPLATE:
                dataset = getDatasetFromSource(file, type, info);
                break;
            case EXTRA_BACKGROUND_NOISE:
            case EXTRA_SIGNAL:
                dataset = getDatasetFromObsParam(file, type, info);
                break;
        }
        if (dataset != null && dataset instanceof DatasetImpl)
            ((DatasetImpl) dataset).setInfo(info);
        return dataset;
    }

    private Dataset getDatasetFromInstrument(File file, Type type, DatasetInfo info) throws IOException, WrongFormatException {
        InstrumentWithDatasets instrument = importInstrumentFromDataFile(file, new ComponentInfo(info.getNamespace(), null));
        switch (type) {
            case FWHM:
                return instrument.getFwhmDataset();
            case FILTER_TRANSMISSION:
                return instrument.getFilterTransmissionDataset();
            case SPECTRAL_RESOLUTION:
                return instrument.getSpectralResolutionDataset();
            case TRANSMISSION:
                return instrument.getTransmissionDataset();
            case PSF_DOUBLE_GAUSSIAN_FWHM_1:
                return instrument.getPsfDoubleGaussianFwhm1Dataset();
            case PSF_DOUBLE_GAUSSIAN_FWHM_2:
                return instrument.getPsfDoubleGaussianFwhm2Dataset();
            case PSF_SIZE_FUNCTION:
                return instrument.getPsfSizeFunctionDataset();
        }
        return null;
    }

    private Dataset getDatasetFromSite(File file, Type type, DatasetInfo info) throws IOException, WrongFormatException {
        SiteWithDatasets site = importSiteFromFile(file, new ComponentInfo(info.getNamespace(), null));
        switch (type) {
            case GALACTIC:
                return site.getGalacticProfileDataset();
            case ZODIACAL:
                return site.getZodiacalProfileDataset();
            case SKY_ABSORPTION:
                return site.getSkyAbsorptionDataset();
            case SKY_EXTINCTION:
                return site.getSkyExtinctionDataset();
            case ATMOSPHERIC_TRANSMISSION:
                return site.getAtmosphericTransmissionDataset();
        }
        return null;
    }

    private Dataset getDatasetFromObsParam(File file, Type type, DatasetInfo info) throws IOException, WrongFormatException {
        ObsParamWithDatasets obsParam = importObsParamFromFile(file, new ComponentInfo(info.getNamespace(), null));
        switch (type) {
            case EXTRA_BACKGROUND_NOISE:
                return obsParam.getExtraBackgroundNoiseDatasetDataset();
            case EXTRA_SIGNAL:
                return obsParam.getExtraSignalDatasetDataset();
        }
        return null;
    }

    private Dataset getDatasetFromSource(File file, Type type, DatasetInfo info) throws IOException, WrongFormatException {
        SourceWithDatasets source = importSourceFromFile(file, new ComponentInfo(info.getNamespace(), null));
        switch (type) {
            case SPECTRAL_DIST_TEMPLATE:
                return source.getSpectralDistributionTemplateDataset();
        }
        return null;
    }

    @Override
    public Dataset importDatasetFromFile(File file, Type type, DatasetInfo info, String xUnit, String yUnit, Dataset.DataType dataType) throws IOException, WrongFormatException {
        BufferedReader in = new BufferedReader(new FileReader(file));
        Map<Double, Double> template = new TreeMap<Double, Double>();
        String line;
        int lineNumber = 0;
        try {
            while ((line = in.readLine()) != null) {
                lineNumber++;
                if (line.indexOf('#') != -1)
                    line = line.substring(0, line.indexOf('#'));
                if (line.trim().length() == 0)
                    continue;
                String[] data = line.trim().split("\\s+");
                if (data.length != 2)
                    throw new NumberFormatException();
                template.put(Double.valueOf(data[0]), Double.valueOf(data[1]));
            }
        } catch (NumberFormatException ex) {
        	try{
        		SavotPullParser sb = new SavotPullParser(file.getAbsolutePath(),SavotPullEngine.FULL);
                SavotVOTable sv = sb.getVOTable();
                int indexCurrentResource = 0;
                SavotResource currentResource = (SavotResource)(sv.getResources().getItemAt(indexCurrentResource));
                int indexTable = getIndexTable(currentResource);
                TRSet tr = currentResource.getTRSet(indexTable);
                double[] x = new double[tr.getItemCount()] ;
                double[] y = new double[tr.getItemCount()] ;
                // idem est verifie deux Filed declaree
                if (currentResource.getFieldSet(indexTable).getItemCount() != 2) {
                	throw new NumberFormatException();
                }
                // identifier indice de X et indice de Y et les unités;
                int indiceX;
                int indiceY;
                if (((SavotField) currentResource.getFieldSet(indexTable).getItemAt(0)).getUtype().equals("Data.FluxAxis.Value")
                        && ((SavotField) currentResource.getFieldSet(indexTable).getItemAt(1)).getUtype().equals("Data.SpectralAxis.Value")) {
                    indiceX = 1;
                    indiceY = 0;
                } else if (((SavotField) currentResource.getFieldSet(indexTable).getItemAt(1)).getUtype().equals("Data.FluxAxis.Value")
                        && ((SavotField) currentResource.getFieldSet(indexTable).getItemAt(0)).getUtype().equals("Data.SpectralAxis.Value")) {
                    indiceX = 0;
                    indiceY = 1;
                } else {
                	throw new NumberFormatException();
                }
                // Recupérer les unités depuis les champs Field
                String unitX = ((SavotField) currentResource.getFieldSet(indexTable).getItemAt(indiceX)).getUnit();
                String unitY = ((SavotField) currentResource.getFieldSet(indexTable).getItemAt(indiceY)).getUnit();

                TDSet theTDs;
                // Chaque ligne est chargée une a une
                for (int i = 0; i < tr.getItemCount(); i++) {
                    // get all the data of the row
                    theTDs = tr.getTDSet(i);
                    // verifie que seulement 2 donnes par ligne, sinon stop
                    if (theTDs.getItemCount() != 2) {
                    	throw new NumberFormatException();
                    }
                    try {
                        //conversion des X en angstrom
                        x[i] = UnitHelper.omniParseX(Double.valueOf(theTDs.getContent(1)).doubleValue(), unitX);

                        //conversion des Y en erg/s/cm²/Ang
                        y[i] = UnitHelper.omniParseY(Double.valueOf(theTDs.getContent(0)).doubleValue(), x[i], unitY);
                    } catch (Exception exxx) {
                    	throw new NumberFormatException();
                    }
                }
                String qunitX = "angstrom";
                String qunitY = "erg/s/cm2/Ang";
                for (int i = 0; i < x.length; i++) {
                	template.put(x[i], y[i]);
                }
        	}
        	catch (NumberFormatException exx) {
        		throw new WrongFormatException("The file " + file + " didn't contain valid data (line " + lineNumber + ")");
        	}
        	catch (Exception e) {
        		throw new WrongFormatException("The file " + file + " didn't contain valid data (line " + lineNumber + ")");
			}
        }
        in.close();
        return new DatasetImpl(info, type, template, xUnit, yUnit, dataType);
    }

    public void checkVotable(File file) throws NumberFormatException{
    	SavotPullParser sb = new SavotPullParser(file.getAbsolutePath(),SavotPullEngine.FULL);
        SavotVOTable sv = sb.getVOTable();
        int indexCurrentResource = 0;
        SavotResource currentResource = (SavotResource)(sv.getResources().getItemAt(indexCurrentResource));
        if (currentResource == null) {
        	throw new NumberFormatException();
        }
    }
    
    @Override
    public InstrumentWithDatasets importInstrumentFromDataFile(File file, ComponentInfo info) throws IOException, WrongFormatException {
        InstrumentDocument instrumentDoc = null;
        try {
            try {
				instrumentDoc = InstrumentDocument.Factory.parse(file);
			} catch (XmlException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
                throw new WrongFormatException(e);
			}
        } catch (IOException ioEx) {
            throw ioEx;
        }
        return new InstrumentWithDatasetsImpl(instrumentDoc.getInstrument(), info);
    }

    // TODO: Copied from old code and need reviewing
    private int getIndexTable(SavotResource currentResource) {
        int index = 1;
        // TODO A FINIR lundi ;)
        // D'abord cas normal ou il n'y a qu'une table dans la VOTAble
        if (currentResource.getTableCount() == 1) {
            index = 0;
        } // ensuite les autres qui doivent pas arriver et qui arrivent
        else {
            TRSet trtest;
            // Pour chaque Table on regarde celui a qui des donnees dedans
            // et on choisi la derniere table avec des donnees ..
            for (int i = 0; i < currentResource.getTableCount(); i++) {
                try {
                    trtest = currentResource.getTRSet(i);
                    if (trtest.getItemCount() > 0) {
                        index = i;
                    }
                } catch (Exception ex) {
                }
            }
        }
        return index;
    }
    
    @Override
    public SiteWithDatasets importSiteFromFile(File file, ComponentInfo info) throws IOException, WrongFormatException {
        SiteDocument siteDoc = null;
        try {
            siteDoc = SiteDocument.Factory.parse(file);
        } catch (IOException ioEx) {
            throw ioEx;
        } catch (Throwable ex) {
            throw new WrongFormatException(ex);
        }
        return new SiteWithDatasetsImpl(siteDoc.getSite(), info);
    }

    @Override
    public SourceWithDatasets importSourceFromFile(File file, ComponentInfo info) throws IOException, WrongFormatException {
        SourceDocument sourceDoc = null;
        try {
            sourceDoc = SourceDocument.Factory.parse(file);
        } catch (IOException ioEx) {
            throw ioEx;
        } catch (Throwable ex) {
            throw new WrongFormatException(ex);
        }
        return new SourceWithDatasetsImpl(sourceDoc.getSource(), info);
    }

    @Override
    public ObsParamWithDatasets importObsParamFromFile(File file, ComponentInfo info) throws IOException, WrongFormatException {
        ObservingParametersDocument obsParamDoc = null;
        try {
            obsParamDoc = ObservingParametersDocument.Factory.parse(file);
        } catch (IOException ioEx) {
            throw ioEx;
        } catch (Throwable ex) {
            throw new WrongFormatException(ex);
        }
        return new ObsParamWithDatasetsImpl(obsParamDoc.getObservingParameters(), info);
    }

}
