/*
 * An XML document type.
 * Localname: site
 * Namespace: 
 * Java type: org.cnrs.lam.dis.etc.dataimportexport.xml.SiteDocument
 *
 * Automatically generated - do not modify.
 */
package org.cnrs.lam.dis.etc.dataimportexport.xml;


/**
 * A document containing one site(@) element.
 *
 * This is a complex type.
 */
public interface SiteDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(SiteDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sCF658226FCE8387FD3C82F8A9EBA922F").resolveHandle("site0340doctype");
    
    /**
     * Gets the "site" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.Site getSite();
    
    /**
     * Sets the "site" element
     */
    void setSite(org.cnrs.lam.dis.etc.dataimportexport.xml.Site site);
    
    /**
     * Appends and returns a new empty "site" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.Site addNewSite();
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.SiteDocument newInstance() {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.SiteDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.SiteDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.SiteDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.SiteDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.SiteDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.SiteDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.SiteDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.SiteDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.SiteDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.SiteDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.SiteDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.SiteDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.SiteDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.SiteDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.SiteDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.SiteDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.SiteDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.SiteDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.SiteDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.SiteDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.SiteDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.SiteDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.SiteDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.SiteDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.SiteDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.SiteDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.SiteDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.SiteDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.SiteDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.SiteDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.SiteDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.SiteDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.SiteDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.SiteDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.SiteDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
