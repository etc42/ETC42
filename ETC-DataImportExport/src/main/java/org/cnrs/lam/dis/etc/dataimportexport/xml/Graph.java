/*
 * XML Type:  Graph
 * Namespace: 
 * Java type: org.cnrs.lam.dis.etc.dataimportexport.xml.Graph
 *
 * Automatically generated - do not modify.
 */
package org.cnrs.lam.dis.etc.dataimportexport.xml;


/**
 * An XML Graph(@).
 *
 * This is a complex type.
 */
public interface Graph extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Graph.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sCF658226FCE8387FD3C82F8A9EBA922F").resolveHandle("graphe065type");
    
    /**
     * Gets array of all "sample" elements
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.Sample[] getSampleArray();
    
    /**
     * Gets ith "sample" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.Sample getSampleArray(int i);
    
    /**
     * Returns number of "sample" element
     */
    int sizeOfSampleArray();
    
    /**
     * Sets array of all "sample" element
     */
    void setSampleArray(org.cnrs.lam.dis.etc.dataimportexport.xml.Sample[] sampleArray);
    
    /**
     * Sets ith "sample" element
     */
    void setSampleArray(int i, org.cnrs.lam.dis.etc.dataimportexport.xml.Sample sample);
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "sample" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.Sample insertNewSample(int i);
    
    /**
     * Appends and returns a new empty value (as xml) as the last "sample" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.Sample addNewSample();
    
    /**
     * Removes the ith "sample" element
     */
    void removeSample(int i);
    
    /**
     * Gets the "name" attribute
     */
    java.lang.String getName();
    
    /**
     * Gets (as xml) the "name" attribute
     */
    org.apache.xmlbeans.XmlString xgetName();
    
    /**
     * Sets the "name" attribute
     */
    void setName(java.lang.String name);
    
    /**
     * Sets (as xml) the "name" attribute
     */
    void xsetName(org.apache.xmlbeans.XmlString name);
    
    /**
     * Gets the "unitX" attribute
     */
    java.lang.String getUnitX();
    
    /**
     * Gets (as xml) the "unitX" attribute
     */
    org.apache.xmlbeans.XmlString xgetUnitX();
    
    /**
     * Sets the "unitX" attribute
     */
    void setUnitX(java.lang.String unitX);
    
    /**
     * Sets (as xml) the "unitX" attribute
     */
    void xsetUnitX(org.apache.xmlbeans.XmlString unitX);
    
    /**
     * Gets the "unitY" attribute
     */
    java.lang.String getUnitY();
    
    /**
     * Gets (as xml) the "unitY" attribute
     */
    org.apache.xmlbeans.XmlString xgetUnitY();
    
    /**
     * Sets the "unitY" attribute
     */
    void setUnitY(java.lang.String unitY);
    
    /**
     * Sets (as xml) the "unitY" attribute
     */
    void xsetUnitY(org.apache.xmlbeans.XmlString unitY);
    
    /**
     * Gets the "option" attribute
     */
    java.lang.String getOption();
    
    /**
     * Gets (as xml) the "option" attribute
     */
    org.apache.xmlbeans.XmlString xgetOption();
    
    /**
     * True if has "option" attribute
     */
    boolean isSetOption();
    
    /**
     * Sets the "option" attribute
     */
    void setOption(java.lang.String option);
    
    /**
     * Sets (as xml) the "option" attribute
     */
    void xsetOption(org.apache.xmlbeans.XmlString option);
    
    /**
     * Unsets the "option" attribute
     */
    void unsetOption();
    
    /**
     * Gets the "dataType" attribute
     */
    java.lang.String getDataType();
    
    /**
     * Gets (as xml) the "dataType" attribute
     */
    org.apache.xmlbeans.XmlString xgetDataType();
    
    /**
     * True if has "dataType" attribute
     */
    boolean isSetDataType();
    
    /**
     * Sets the "dataType" attribute
     */
    void setDataType(java.lang.String dataType);
    
    /**
     * Sets (as xml) the "dataType" attribute
     */
    void xsetDataType(org.apache.xmlbeans.XmlString dataType);
    
    /**
     * Unsets the "dataType" attribute
     */
    void unsetDataType();
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Graph newInstance() {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Graph newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Graph parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Graph parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Graph parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Graph parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Graph parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Graph parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Graph parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Graph parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Graph parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Graph parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Graph parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Graph parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Graph parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Graph parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Graph parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.Graph parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
