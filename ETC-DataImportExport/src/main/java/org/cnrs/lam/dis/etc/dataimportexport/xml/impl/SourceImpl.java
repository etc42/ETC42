/*
 * XML Type:  Source
 * Namespace: 
 * Java type: org.cnrs.lam.dis.etc.dataimportexport.xml.Source
 *
 * Automatically generated - do not modify.
 */
package org.cnrs.lam.dis.etc.dataimportexport.xml.impl;
/**
 * An XML Source(@).
 *
 * This is a complex type.
 */
public class SourceImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements org.cnrs.lam.dis.etc.dataimportexport.xml.Source
{
    private static final long serialVersionUID = 1L;
    
    public SourceImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName INFO$0 = 
        new javax.xml.namespace.QName("", "info");
    private static final javax.xml.namespace.QName MAGNITUDEWAVELENGTH$2 = 
        new javax.xml.namespace.QName("", "magnitudeWavelength");
    private static final javax.xml.namespace.QName EMISSIONLINEWAVELENGTH$4 = 
        new javax.xml.namespace.QName("", "emissionLineWavelength");
    private static final javax.xml.namespace.QName EMISSIONLINEFWHM$6 = 
        new javax.xml.namespace.QName("", "emissionLineFwhm");
    private static final javax.xml.namespace.QName EXTENDEDSOURCEPROFILE$8 = 
        new javax.xml.namespace.QName("", "extendedSourceProfile");
    private static final javax.xml.namespace.QName EXTENDEDSOURCERADIUS$10 = 
        new javax.xml.namespace.QName("", "extendedSourceRadius");
    private static final javax.xml.namespace.QName MAGNITUDE$12 = 
        new javax.xml.namespace.QName("", "magnitude");
    private static final javax.xml.namespace.QName REDSHIFT$14 = 
        new javax.xml.namespace.QName("", "redshift");
    private static final javax.xml.namespace.QName TEMPERATURE$16 = 
        new javax.xml.namespace.QName("", "temperature");
    private static final javax.xml.namespace.QName SPATIALDISTRIBUTIONTYPE$18 = 
        new javax.xml.namespace.QName("", "spatialDistributionType");
    private static final javax.xml.namespace.QName SPECTRALDISTRIBUTIONTYPE$20 = 
        new javax.xml.namespace.QName("", "spectralDistributionType");
    private static final javax.xml.namespace.QName SPECTRALDISTRIBUTIONTEMPLATE$22 = 
        new javax.xml.namespace.QName("", "spectralDistributionTemplate");
    private static final javax.xml.namespace.QName EMISSIONLINEFLUX$24 = 
        new javax.xml.namespace.QName("", "emissionLineFlux");
    private static final javax.xml.namespace.QName EXTENDEDMAGNITUDETYPE$26 = 
        new javax.xml.namespace.QName("", "extendedMagnitudeType");
    
    
    /**
     * Gets the "info" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo getInfo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo)get_store().find_element_user(INFO$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "info" element
     */
    public void setInfo(org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo info)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo)get_store().find_element_user(INFO$0, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo)get_store().add_element_user(INFO$0);
            }
            target.set(info);
        }
    }
    
    /**
     * Appends and returns a new empty "info" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo addNewInfo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo)get_store().add_element_user(INFO$0);
            return target;
        }
    }
    
    /**
     * Gets the "magnitudeWavelength" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getMagnitudeWavelength()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(MAGNITUDEWAVELENGTH$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "magnitudeWavelength" element
     */
    public void setMagnitudeWavelength(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit magnitudeWavelength)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(MAGNITUDEWAVELENGTH$2, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(MAGNITUDEWAVELENGTH$2);
            }
            target.set(magnitudeWavelength);
        }
    }
    
    /**
     * Appends and returns a new empty "magnitudeWavelength" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewMagnitudeWavelength()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(MAGNITUDEWAVELENGTH$2);
            return target;
        }
    }
    
    /**
     * Gets the "emissionLineWavelength" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getEmissionLineWavelength()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(EMISSIONLINEWAVELENGTH$4, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "emissionLineWavelength" element
     */
    public void setEmissionLineWavelength(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit emissionLineWavelength)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(EMISSIONLINEWAVELENGTH$4, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(EMISSIONLINEWAVELENGTH$4);
            }
            target.set(emissionLineWavelength);
        }
    }
    
    /**
     * Appends and returns a new empty "emissionLineWavelength" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewEmissionLineWavelength()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(EMISSIONLINEWAVELENGTH$4);
            return target;
        }
    }
    
    /**
     * Gets the "emissionLineFwhm" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getEmissionLineFwhm()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(EMISSIONLINEFWHM$6, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "emissionLineFwhm" element
     */
    public void setEmissionLineFwhm(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit emissionLineFwhm)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(EMISSIONLINEFWHM$6, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(EMISSIONLINEFWHM$6);
            }
            target.set(emissionLineFwhm);
        }
    }
    
    /**
     * Appends and returns a new empty "emissionLineFwhm" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewEmissionLineFwhm()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(EMISSIONLINEFWHM$6);
            return target;
        }
    }
    
    /**
     * Gets the "extendedSourceProfile" element
     */
    public java.lang.String getExtendedSourceProfile()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(EXTENDEDSOURCEPROFILE$8, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "extendedSourceProfile" element
     */
    public org.apache.xmlbeans.XmlString xgetExtendedSourceProfile()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(EXTENDEDSOURCEPROFILE$8, 0);
            return target;
        }
    }
    
    /**
     * Sets the "extendedSourceProfile" element
     */
    public void setExtendedSourceProfile(java.lang.String extendedSourceProfile)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(EXTENDEDSOURCEPROFILE$8, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(EXTENDEDSOURCEPROFILE$8);
            }
            target.setStringValue(extendedSourceProfile);
        }
    }
    
    /**
     * Sets (as xml) the "extendedSourceProfile" element
     */
    public void xsetExtendedSourceProfile(org.apache.xmlbeans.XmlString extendedSourceProfile)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(EXTENDEDSOURCEPROFILE$8, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(EXTENDEDSOURCEPROFILE$8);
            }
            target.set(extendedSourceProfile);
        }
    }
    
    /**
     * Gets the "extendedSourceRadius" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getExtendedSourceRadius()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(EXTENDEDSOURCERADIUS$10, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "extendedSourceRadius" element
     */
    public void setExtendedSourceRadius(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit extendedSourceRadius)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(EXTENDEDSOURCERADIUS$10, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(EXTENDEDSOURCERADIUS$10);
            }
            target.set(extendedSourceRadius);
        }
    }
    
    /**
     * Appends and returns a new empty "extendedSourceRadius" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewExtendedSourceRadius()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(EXTENDEDSOURCERADIUS$10);
            return target;
        }
    }
    
    /**
     * Gets the "magnitude" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getMagnitude()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(MAGNITUDE$12, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "magnitude" element
     */
    public void setMagnitude(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit magnitude)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(MAGNITUDE$12, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(MAGNITUDE$12);
            }
            target.set(magnitude);
        }
    }
    
    /**
     * Appends and returns a new empty "magnitude" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewMagnitude()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(MAGNITUDE$12);
            return target;
        }
    }
    
    /**
     * Gets the "redshift" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getRedshift()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(REDSHIFT$14, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "redshift" element
     */
    public void setRedshift(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit redshift)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(REDSHIFT$14, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(REDSHIFT$14);
            }
            target.set(redshift);
        }
    }
    
    /**
     * Appends and returns a new empty "redshift" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewRedshift()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(REDSHIFT$14);
            return target;
        }
    }
    
    /**
     * Gets the "temperature" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getTemperature()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(TEMPERATURE$16, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "temperature" element
     */
    public void setTemperature(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit temperature)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(TEMPERATURE$16, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(TEMPERATURE$16);
            }
            target.set(temperature);
        }
    }
    
    /**
     * Appends and returns a new empty "temperature" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewTemperature()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(TEMPERATURE$16);
            return target;
        }
    }
    
    /**
     * Gets the "spatialDistributionType" element
     */
    public java.lang.String getSpatialDistributionType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SPATIALDISTRIBUTIONTYPE$18, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "spatialDistributionType" element
     */
    public org.apache.xmlbeans.XmlString xgetSpatialDistributionType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(SPATIALDISTRIBUTIONTYPE$18, 0);
            return target;
        }
    }
    
    /**
     * Sets the "spatialDistributionType" element
     */
    public void setSpatialDistributionType(java.lang.String spatialDistributionType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SPATIALDISTRIBUTIONTYPE$18, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SPATIALDISTRIBUTIONTYPE$18);
            }
            target.setStringValue(spatialDistributionType);
        }
    }
    
    /**
     * Sets (as xml) the "spatialDistributionType" element
     */
    public void xsetSpatialDistributionType(org.apache.xmlbeans.XmlString spatialDistributionType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(SPATIALDISTRIBUTIONTYPE$18, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(SPATIALDISTRIBUTIONTYPE$18);
            }
            target.set(spatialDistributionType);
        }
    }
    
    /**
     * Gets the "spectralDistributionType" element
     */
    public java.lang.String getSpectralDistributionType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SPECTRALDISTRIBUTIONTYPE$20, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "spectralDistributionType" element
     */
    public org.apache.xmlbeans.XmlString xgetSpectralDistributionType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(SPECTRALDISTRIBUTIONTYPE$20, 0);
            return target;
        }
    }
    
    /**
     * Sets the "spectralDistributionType" element
     */
    public void setSpectralDistributionType(java.lang.String spectralDistributionType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SPECTRALDISTRIBUTIONTYPE$20, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SPECTRALDISTRIBUTIONTYPE$20);
            }
            target.setStringValue(spectralDistributionType);
        }
    }
    
    /**
     * Sets (as xml) the "spectralDistributionType" element
     */
    public void xsetSpectralDistributionType(org.apache.xmlbeans.XmlString spectralDistributionType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(SPECTRALDISTRIBUTIONTYPE$20, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(SPECTRALDISTRIBUTIONTYPE$20);
            }
            target.set(spectralDistributionType);
        }
    }
    
    /**
     * Gets the "spectralDistributionTemplate" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.Graph getSpectralDistributionTemplate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().find_element_user(SPECTRALDISTRIBUTIONTEMPLATE$22, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "spectralDistributionTemplate" element
     */
    public boolean isSetSpectralDistributionTemplate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(SPECTRALDISTRIBUTIONTEMPLATE$22) != 0;
        }
    }
    
    /**
     * Sets the "spectralDistributionTemplate" element
     */
    public void setSpectralDistributionTemplate(org.cnrs.lam.dis.etc.dataimportexport.xml.Graph spectralDistributionTemplate)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().find_element_user(SPECTRALDISTRIBUTIONTEMPLATE$22, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().add_element_user(SPECTRALDISTRIBUTIONTEMPLATE$22);
            }
            target.set(spectralDistributionTemplate);
        }
    }
    
    /**
     * Appends and returns a new empty "spectralDistributionTemplate" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.Graph addNewSpectralDistributionTemplate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().add_element_user(SPECTRALDISTRIBUTIONTEMPLATE$22);
            return target;
        }
    }
    
    /**
     * Unsets the "spectralDistributionTemplate" element
     */
    public void unsetSpectralDistributionTemplate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(SPECTRALDISTRIBUTIONTEMPLATE$22, 0);
        }
    }
    
    /**
     * Gets the "emissionLineFlux" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getEmissionLineFlux()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(EMISSIONLINEFLUX$24, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "emissionLineFlux" element
     */
    public void setEmissionLineFlux(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit emissionLineFlux)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(EMISSIONLINEFLUX$24, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(EMISSIONLINEFLUX$24);
            }
            target.set(emissionLineFlux);
        }
    }
    
    /**
     * Appends and returns a new empty "emissionLineFlux" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewEmissionLineFlux()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(EMISSIONLINEFLUX$24);
            return target;
        }
    }
    
    /**
     * Gets the "extendedMagnitudeType" element
     */
    public java.lang.String getExtendedMagnitudeType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(EXTENDEDMAGNITUDETYPE$26, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "extendedMagnitudeType" element
     */
    public org.apache.xmlbeans.XmlString xgetExtendedMagnitudeType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(EXTENDEDMAGNITUDETYPE$26, 0);
            return target;
        }
    }
    
    /**
     * Sets the "extendedMagnitudeType" element
     */
    public void setExtendedMagnitudeType(java.lang.String extendedMagnitudeType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(EXTENDEDMAGNITUDETYPE$26, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(EXTENDEDMAGNITUDETYPE$26);
            }
            target.setStringValue(extendedMagnitudeType);
        }
    }
    
    /**
     * Sets (as xml) the "extendedMagnitudeType" element
     */
    public void xsetExtendedMagnitudeType(org.apache.xmlbeans.XmlString extendedMagnitudeType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(EXTENDEDMAGNITUDETYPE$26, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(EXTENDEDMAGNITUDETYPE$26);
            }
            target.set(extendedMagnitudeType);
        }
    }
}
