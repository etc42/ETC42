/*
 * XML Type:  ObservingParameters
 * Namespace: 
 * Java type: org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters
 *
 * Automatically generated - do not modify.
 */
package org.cnrs.lam.dis.etc.dataimportexport.xml;


/**
 * An XML ObservingParameters(@).
 *
 * This is a complex type.
 */
public interface ObservingParameters extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(ObservingParameters.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sCF658226FCE8387FD3C82F8A9EBA922F").resolveHandle("observingparameters247atype");
    
    /**
     * Gets the "info" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo getInfo();
    
    /**
     * Sets the "info" element
     */
    void setInfo(org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo info);
    
    /**
     * Appends and returns a new empty "info" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo addNewInfo();
    
    /**
     * Gets the "dit" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getDit();
    
    /**
     * Sets the "dit" element
     */
    void setDit(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit dit);
    
    /**
     * Appends and returns a new empty "dit" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewDit();
    
    /**
     * Gets the "exposureTime" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getExposureTime();
    
    /**
     * Sets the "exposureTime" element
     */
    void setExposureTime(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit exposureTime);
    
    /**
     * Appends and returns a new empty "exposureTime" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewExposureTime();
    
    /**
     * Gets the "fixedParameter" element
     */
    java.lang.String getFixedParameter();
    
    /**
     * Gets (as xml) the "fixedParameter" element
     */
    org.apache.xmlbeans.XmlString xgetFixedParameter();
    
    /**
     * Sets the "fixedParameter" element
     */
    void setFixedParameter(java.lang.String fixedParameter);
    
    /**
     * Sets (as xml) the "fixedParameter" element
     */
    void xsetFixedParameter(org.apache.xmlbeans.XmlString fixedParameter);
    
    /**
     * Gets the "noExpo" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.IntUnit getNoExpo();
    
    /**
     * Sets the "noExpo" element
     */
    void setNoExpo(org.cnrs.lam.dis.etc.dataimportexport.xml.IntUnit noExpo);
    
    /**
     * Appends and returns a new empty "noExpo" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.IntUnit addNewNoExpo();
    
    /**
     * Gets the "snr" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getSnr();
    
    /**
     * Sets the "snr" element
     */
    void setSnr(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit snr);
    
    /**
     * Appends and returns a new empty "snr" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewSnr();
    
    /**
     * Gets the "snrLambda" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getSnrLambda();
    
    /**
     * Sets the "snrLambda" element
     */
    void setSnrLambda(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit snrLambda);
    
    /**
     * Appends and returns a new empty "snrLambda" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewSnrLambda();
    
    /**
     * Gets the "timeSampleType" element
     */
    java.lang.String getTimeSampleType();
    
    /**
     * Gets (as xml) the "timeSampleType" element
     */
    org.apache.xmlbeans.XmlString xgetTimeSampleType();
    
    /**
     * Sets the "timeSampleType" element
     */
    void setTimeSampleType(java.lang.String timeSampleType);
    
    /**
     * Sets (as xml) the "timeSampleType" element
     */
    void xsetTimeSampleType(org.apache.xmlbeans.XmlString timeSampleType);
    
    /**
     * Gets the "spectralQuantumType" element
     */
    java.lang.String getSpectralQuantumType();
    
    /**
     * Gets (as xml) the "spectralQuantumType" element
     */
    org.apache.xmlbeans.XmlString xgetSpectralQuantumType();
    
    /**
     * Sets the "spectralQuantumType" element
     */
    void setSpectralQuantumType(java.lang.String spectralQuantumType);
    
    /**
     * Sets (as xml) the "spectralQuantumType" element
     */
    void xsetSpectralQuantumType(org.apache.xmlbeans.XmlString spectralQuantumType);
    
    /**
     * Gets the "extraBackgroundNoiseType" element
     */
    java.lang.String getExtraBackgroundNoiseType();
    
    /**
     * Gets (as xml) the "extraBackgroundNoiseType" element
     */
    org.apache.xmlbeans.XmlString xgetExtraBackgroundNoiseType();
    
    /**
     * Sets the "extraBackgroundNoiseType" element
     */
    void setExtraBackgroundNoiseType(java.lang.String extraBackgroundNoiseType);
    
    /**
     * Sets (as xml) the "extraBackgroundNoiseType" element
     */
    void xsetExtraBackgroundNoiseType(org.apache.xmlbeans.XmlString extraBackgroundNoiseType);
    
    /**
     * Gets the "extraSignalType" element
     */
    java.lang.String getExtraSignalType();
    
    /**
     * Gets (as xml) the "extraSignalType" element
     */
    org.apache.xmlbeans.XmlString xgetExtraSignalType();
    
    /**
     * Sets the "extraSignalType" element
     */
    void setExtraSignalType(java.lang.String extraSignalType);
    
    /**
     * Sets (as xml) the "extraSignalType" element
     */
    void xsetExtraSignalType(org.apache.xmlbeans.XmlString extraSignalType);
    
    /**
     * Gets the "extraBackgroundNoiseDataset" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.Graph getExtraBackgroundNoiseDataset();
    
    /**
     * Sets the "extraBackgroundNoiseDataset" element
     */
    void setExtraBackgroundNoiseDataset(org.cnrs.lam.dis.etc.dataimportexport.xml.Graph extraBackgroundNoiseDataset);
    
    /**
     * Appends and returns a new empty "extraBackgroundNoiseDataset" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.Graph addNewExtraBackgroundNoiseDataset();
    
    /**
     * Gets the "extraSignalDataset" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.Graph getExtraSignalDataset();
    
    /**
     * Sets the "extraSignalDataset" element
     */
    void setExtraSignalDataset(org.cnrs.lam.dis.etc.dataimportexport.xml.Graph extraSignalDataset);
    
    /**
     * Appends and returns a new empty "extraSignalDataset" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.Graph addNewExtraSignalDataset();
    
    /**
     * Gets the "extraBackgroundNoise" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getExtraBackgroundNoise();
    
    /**
     * Sets the "extraBackgroundNoise" element
     */
    void setExtraBackgroundNoise(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit extraBackgroundNoise);
    
    /**
     * Appends and returns a new empty "extraBackgroundNoise" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewExtraBackgroundNoise();
    
    /**
     * Gets the "extraSignal" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getExtraSignal();
    
    /**
     * Sets the "extraSignal" element
     */
    void setExtraSignal(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit extraSignal);
    
    /**
     * Appends and returns a new empty "extraSignal" element
     */
    org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewExtraSignal();
    
    /**
     * Gets the "fixedSnrType" element
     */
    java.lang.String getFixedSnrType();
    
    /**
     * Gets (as xml) the "fixedSnrType" element
     */
    org.apache.xmlbeans.XmlString xgetFixedSnrType();
    
    /**
     * Sets the "fixedSnrType" element
     */
    void setFixedSnrType(java.lang.String fixedSnrType);
    
    /**
     * Sets (as xml) the "fixedSnrType" element
     */
    void xsetFixedSnrType(org.apache.xmlbeans.XmlString fixedSnrType);
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters newInstance() {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
