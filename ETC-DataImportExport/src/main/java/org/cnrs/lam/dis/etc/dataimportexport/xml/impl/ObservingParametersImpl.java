/*
 * XML Type:  ObservingParameters
 * Namespace: 
 * Java type: org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters
 *
 * Automatically generated - do not modify.
 */
package org.cnrs.lam.dis.etc.dataimportexport.xml.impl;
/**
 * An XML ObservingParameters(@).
 *
 * This is a complex type.
 */
public class ObservingParametersImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters
{
    private static final long serialVersionUID = 1L;
    
    public ObservingParametersImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName INFO$0 = 
        new javax.xml.namespace.QName("", "info");
    private static final javax.xml.namespace.QName DIT$2 = 
        new javax.xml.namespace.QName("", "dit");
    private static final javax.xml.namespace.QName EXPOSURETIME$4 = 
        new javax.xml.namespace.QName("", "exposureTime");
    private static final javax.xml.namespace.QName FIXEDPARAMETER$6 = 
        new javax.xml.namespace.QName("", "fixedParameter");
    private static final javax.xml.namespace.QName NOEXPO$8 = 
        new javax.xml.namespace.QName("", "noExpo");
    private static final javax.xml.namespace.QName SNR$10 = 
        new javax.xml.namespace.QName("", "snr");
    private static final javax.xml.namespace.QName SNRLAMBDA$12 = 
        new javax.xml.namespace.QName("", "snrLambda");
    private static final javax.xml.namespace.QName TIMESAMPLETYPE$14 = 
        new javax.xml.namespace.QName("", "timeSampleType");
    private static final javax.xml.namespace.QName SPECTRALQUANTUMTYPE$16 = 
        new javax.xml.namespace.QName("", "spectralQuantumType");
    private static final javax.xml.namespace.QName EXTRABACKGROUNDNOISETYPE$18 = 
        new javax.xml.namespace.QName("", "extraBackgroundNoiseType");
    private static final javax.xml.namespace.QName EXTRASIGNALTYPE$20 = 
        new javax.xml.namespace.QName("", "extraSignalType");
    private static final javax.xml.namespace.QName EXTRABACKGROUNDNOISEDATASET$22 = 
        new javax.xml.namespace.QName("", "extraBackgroundNoiseDataset");
    private static final javax.xml.namespace.QName EXTRASIGNALDATASET$24 = 
        new javax.xml.namespace.QName("", "extraSignalDataset");
    private static final javax.xml.namespace.QName EXTRABACKGROUNDNOISE$26 = 
        new javax.xml.namespace.QName("", "extraBackgroundNoise");
    private static final javax.xml.namespace.QName EXTRASIGNAL$28 = 
        new javax.xml.namespace.QName("", "extraSignal");
    private static final javax.xml.namespace.QName FIXEDSNRTYPE$30 = 
        new javax.xml.namespace.QName("", "fixedSnrType");
    
    
    /**
     * Gets the "info" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo getInfo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo)get_store().find_element_user(INFO$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "info" element
     */
    public void setInfo(org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo info)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo)get_store().find_element_user(INFO$0, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo)get_store().add_element_user(INFO$0);
            }
            target.set(info);
        }
    }
    
    /**
     * Appends and returns a new empty "info" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo addNewInfo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo)get_store().add_element_user(INFO$0);
            return target;
        }
    }
    
    /**
     * Gets the "dit" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getDit()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(DIT$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "dit" element
     */
    public void setDit(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit dit)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(DIT$2, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(DIT$2);
            }
            target.set(dit);
        }
    }
    
    /**
     * Appends and returns a new empty "dit" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewDit()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(DIT$2);
            return target;
        }
    }
    
    /**
     * Gets the "exposureTime" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getExposureTime()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(EXPOSURETIME$4, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "exposureTime" element
     */
    public void setExposureTime(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit exposureTime)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(EXPOSURETIME$4, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(EXPOSURETIME$4);
            }
            target.set(exposureTime);
        }
    }
    
    /**
     * Appends and returns a new empty "exposureTime" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewExposureTime()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(EXPOSURETIME$4);
            return target;
        }
    }
    
    /**
     * Gets the "fixedParameter" element
     */
    public java.lang.String getFixedParameter()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FIXEDPARAMETER$6, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "fixedParameter" element
     */
    public org.apache.xmlbeans.XmlString xgetFixedParameter()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(FIXEDPARAMETER$6, 0);
            return target;
        }
    }
    
    /**
     * Sets the "fixedParameter" element
     */
    public void setFixedParameter(java.lang.String fixedParameter)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FIXEDPARAMETER$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(FIXEDPARAMETER$6);
            }
            target.setStringValue(fixedParameter);
        }
    }
    
    /**
     * Sets (as xml) the "fixedParameter" element
     */
    public void xsetFixedParameter(org.apache.xmlbeans.XmlString fixedParameter)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(FIXEDPARAMETER$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(FIXEDPARAMETER$6);
            }
            target.set(fixedParameter);
        }
    }
    
    /**
     * Gets the "noExpo" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.IntUnit getNoExpo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.IntUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.IntUnit)get_store().find_element_user(NOEXPO$8, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "noExpo" element
     */
    public void setNoExpo(org.cnrs.lam.dis.etc.dataimportexport.xml.IntUnit noExpo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.IntUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.IntUnit)get_store().find_element_user(NOEXPO$8, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.IntUnit)get_store().add_element_user(NOEXPO$8);
            }
            target.set(noExpo);
        }
    }
    
    /**
     * Appends and returns a new empty "noExpo" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.IntUnit addNewNoExpo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.IntUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.IntUnit)get_store().add_element_user(NOEXPO$8);
            return target;
        }
    }
    
    /**
     * Gets the "snr" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getSnr()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(SNR$10, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "snr" element
     */
    public void setSnr(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit snr)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(SNR$10, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(SNR$10);
            }
            target.set(snr);
        }
    }
    
    /**
     * Appends and returns a new empty "snr" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewSnr()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(SNR$10);
            return target;
        }
    }
    
    /**
     * Gets the "snrLambda" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getSnrLambda()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(SNRLAMBDA$12, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "snrLambda" element
     */
    public void setSnrLambda(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit snrLambda)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(SNRLAMBDA$12, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(SNRLAMBDA$12);
            }
            target.set(snrLambda);
        }
    }
    
    /**
     * Appends and returns a new empty "snrLambda" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewSnrLambda()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(SNRLAMBDA$12);
            return target;
        }
    }
    
    /**
     * Gets the "timeSampleType" element
     */
    public java.lang.String getTimeSampleType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TIMESAMPLETYPE$14, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "timeSampleType" element
     */
    public org.apache.xmlbeans.XmlString xgetTimeSampleType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(TIMESAMPLETYPE$14, 0);
            return target;
        }
    }
    
    /**
     * Sets the "timeSampleType" element
     */
    public void setTimeSampleType(java.lang.String timeSampleType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TIMESAMPLETYPE$14, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(TIMESAMPLETYPE$14);
            }
            target.setStringValue(timeSampleType);
        }
    }
    
    /**
     * Sets (as xml) the "timeSampleType" element
     */
    public void xsetTimeSampleType(org.apache.xmlbeans.XmlString timeSampleType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(TIMESAMPLETYPE$14, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(TIMESAMPLETYPE$14);
            }
            target.set(timeSampleType);
        }
    }
    
    /**
     * Gets the "spectralQuantumType" element
     */
    public java.lang.String getSpectralQuantumType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SPECTRALQUANTUMTYPE$16, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "spectralQuantumType" element
     */
    public org.apache.xmlbeans.XmlString xgetSpectralQuantumType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(SPECTRALQUANTUMTYPE$16, 0);
            return target;
        }
    }
    
    /**
     * Sets the "spectralQuantumType" element
     */
    public void setSpectralQuantumType(java.lang.String spectralQuantumType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SPECTRALQUANTUMTYPE$16, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SPECTRALQUANTUMTYPE$16);
            }
            target.setStringValue(spectralQuantumType);
        }
    }
    
    /**
     * Sets (as xml) the "spectralQuantumType" element
     */
    public void xsetSpectralQuantumType(org.apache.xmlbeans.XmlString spectralQuantumType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(SPECTRALQUANTUMTYPE$16, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(SPECTRALQUANTUMTYPE$16);
            }
            target.set(spectralQuantumType);
        }
    }
    
    /**
     * Gets the "extraBackgroundNoiseType" element
     */
    public java.lang.String getExtraBackgroundNoiseType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(EXTRABACKGROUNDNOISETYPE$18, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "extraBackgroundNoiseType" element
     */
    public org.apache.xmlbeans.XmlString xgetExtraBackgroundNoiseType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(EXTRABACKGROUNDNOISETYPE$18, 0);
            return target;
        }
    }
    
    /**
     * Sets the "extraBackgroundNoiseType" element
     */
    public void setExtraBackgroundNoiseType(java.lang.String extraBackgroundNoiseType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(EXTRABACKGROUNDNOISETYPE$18, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(EXTRABACKGROUNDNOISETYPE$18);
            }
            target.setStringValue(extraBackgroundNoiseType);
        }
    }
    
    /**
     * Sets (as xml) the "extraBackgroundNoiseType" element
     */
    public void xsetExtraBackgroundNoiseType(org.apache.xmlbeans.XmlString extraBackgroundNoiseType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(EXTRABACKGROUNDNOISETYPE$18, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(EXTRABACKGROUNDNOISETYPE$18);
            }
            target.set(extraBackgroundNoiseType);
        }
    }
    
    /**
     * Gets the "extraSignalType" element
     */
    public java.lang.String getExtraSignalType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(EXTRASIGNALTYPE$20, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "extraSignalType" element
     */
    public org.apache.xmlbeans.XmlString xgetExtraSignalType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(EXTRASIGNALTYPE$20, 0);
            return target;
        }
    }
    
    /**
     * Sets the "extraSignalType" element
     */
    public void setExtraSignalType(java.lang.String extraSignalType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(EXTRASIGNALTYPE$20, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(EXTRASIGNALTYPE$20);
            }
            target.setStringValue(extraSignalType);
        }
    }
    
    /**
     * Sets (as xml) the "extraSignalType" element
     */
    public void xsetExtraSignalType(org.apache.xmlbeans.XmlString extraSignalType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(EXTRASIGNALTYPE$20, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(EXTRASIGNALTYPE$20);
            }
            target.set(extraSignalType);
        }
    }
    
    /**
     * Gets the "extraBackgroundNoiseDataset" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.Graph getExtraBackgroundNoiseDataset()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().find_element_user(EXTRABACKGROUNDNOISEDATASET$22, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "extraBackgroundNoiseDataset" element
     */
    public void setExtraBackgroundNoiseDataset(org.cnrs.lam.dis.etc.dataimportexport.xml.Graph extraBackgroundNoiseDataset)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().find_element_user(EXTRABACKGROUNDNOISEDATASET$22, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().add_element_user(EXTRABACKGROUNDNOISEDATASET$22);
            }
            target.set(extraBackgroundNoiseDataset);
        }
    }
    
    /**
     * Appends and returns a new empty "extraBackgroundNoiseDataset" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.Graph addNewExtraBackgroundNoiseDataset()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().add_element_user(EXTRABACKGROUNDNOISEDATASET$22);
            return target;
        }
    }
    
    /**
     * Gets the "extraSignalDataset" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.Graph getExtraSignalDataset()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().find_element_user(EXTRASIGNALDATASET$24, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "extraSignalDataset" element
     */
    public void setExtraSignalDataset(org.cnrs.lam.dis.etc.dataimportexport.xml.Graph extraSignalDataset)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().find_element_user(EXTRASIGNALDATASET$24, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().add_element_user(EXTRASIGNALDATASET$24);
            }
            target.set(extraSignalDataset);
        }
    }
    
    /**
     * Appends and returns a new empty "extraSignalDataset" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.Graph addNewExtraSignalDataset()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.Graph target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.Graph)get_store().add_element_user(EXTRASIGNALDATASET$24);
            return target;
        }
    }
    
    /**
     * Gets the "extraBackgroundNoise" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getExtraBackgroundNoise()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(EXTRABACKGROUNDNOISE$26, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "extraBackgroundNoise" element
     */
    public void setExtraBackgroundNoise(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit extraBackgroundNoise)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(EXTRABACKGROUNDNOISE$26, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(EXTRABACKGROUNDNOISE$26);
            }
            target.set(extraBackgroundNoise);
        }
    }
    
    /**
     * Appends and returns a new empty "extraBackgroundNoise" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewExtraBackgroundNoise()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(EXTRABACKGROUNDNOISE$26);
            return target;
        }
    }
    
    /**
     * Gets the "extraSignal" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit getExtraSignal()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(EXTRASIGNAL$28, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "extraSignal" element
     */
    public void setExtraSignal(org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit extraSignal)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().find_element_user(EXTRASIGNAL$28, 0);
            if (target == null)
            {
                target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(EXTRASIGNAL$28);
            }
            target.set(extraSignal);
        }
    }
    
    /**
     * Appends and returns a new empty "extraSignal" element
     */
    public org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit addNewExtraSignal()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit target = null;
            target = (org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit)get_store().add_element_user(EXTRASIGNAL$28);
            return target;
        }
    }
    
    /**
     * Gets the "fixedSnrType" element
     */
    public java.lang.String getFixedSnrType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FIXEDSNRTYPE$30, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "fixedSnrType" element
     */
    public org.apache.xmlbeans.XmlString xgetFixedSnrType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(FIXEDSNRTYPE$30, 0);
            return target;
        }
    }
    
    /**
     * Sets the "fixedSnrType" element
     */
    public void setFixedSnrType(java.lang.String fixedSnrType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FIXEDSNRTYPE$30, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(FIXEDSNRTYPE$30);
            }
            target.setStringValue(fixedSnrType);
        }
    }
    
    /**
     * Sets (as xml) the "fixedSnrType" element
     */
    public void xsetFixedSnrType(org.apache.xmlbeans.XmlString fixedSnrType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(FIXEDSNRTYPE$30, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(FIXEDSNRTYPE$30);
            }
            target.set(fixedSnrType);
        }
    }
}
