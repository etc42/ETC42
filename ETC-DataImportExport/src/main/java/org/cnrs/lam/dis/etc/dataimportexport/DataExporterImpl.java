/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.dataimportexport;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import org.cnrs.lam.dis.etc.configuration.ConfigFactory;
import org.cnrs.lam.dis.etc.dataimportexport.xml.ComponentInfo;
import org.cnrs.lam.dis.etc.dataimportexport.xml.DoubleUnit;
import org.cnrs.lam.dis.etc.dataimportexport.xml.Graph;
import org.cnrs.lam.dis.etc.dataimportexport.xml.InstrumentDocument;
import org.cnrs.lam.dis.etc.dataimportexport.xml.IntUnit;
import org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParameters;
import org.cnrs.lam.dis.etc.dataimportexport.xml.ObservingParametersDocument;
import org.cnrs.lam.dis.etc.dataimportexport.xml.Sample;
import org.cnrs.lam.dis.etc.dataimportexport.xml.SiteDocument;
import org.cnrs.lam.dis.etc.dataimportexport.xml.SourceDocument;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.Dataset.Type;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.cnrs.lam.dis.etc.datamodel.DatasetProvider;
import org.cnrs.lam.dis.etc.datamodel.Instrument;
import org.cnrs.lam.dis.etc.datamodel.ObsParam;
import org.cnrs.lam.dis.etc.datamodel.Site;
import org.cnrs.lam.dis.etc.datamodel.Source;

/**
 *
 * @author Nikolaos Apostolakos
 */
class DataExporterImpl implements DataExporter {

    @Override
    public void exportInstrumentInFile(File file, Instrument instrument, DatasetProvider provider) throws IOException {
        InstrumentDocument instrumentDoc = InstrumentDocument.Factory.newInstance();
        org.cnrs.lam.dis.etc.dataimportexport.xml.Instrument instrumentXml = instrumentDoc.addNewInstrument();
        ComponentInfo info = instrumentXml.addNewInfo();
        info.setName(instrument.getInfo().getName());
        info.setDescription(instrument.getInfo().getDescription());
        DoubleUnit dark = instrumentXml.addNewDark();
        dark.setValue(instrument.getDark());
        dark.setUnit(instrument.getDarkUnit());
        DoubleUnit diameter = instrumentXml.addNewDiameter();
        diameter.setValue(instrument.getDiameter());
        diameter.setUnit(instrument.getDiameterUnit());
        DoubleUnit fiberDiameter = instrumentXml.addNewFiberDiameter();
        fiberDiameter.setValue(instrument.getFiberDiameter());
        fiberDiameter.setUnit(instrument.getFiberDiameterUnit());
        if (instrument.getFilterTransmission() != null) {
            Graph filterTransmission = instrumentXml.addNewFilterTransmission();
            populateGraph(filterTransmission, provider.getDataset(Dataset.Type.FILTER_TRANSMISSION, instrument.getFilterTransmission()));
        }
        if (instrument.getFwhm() != null) {
            Graph fwhm = instrumentXml.addNewFwhm();
            populateGraph(fwhm, provider.getDataset(Dataset.Type.FWHM, instrument.getFwhm()));
        }
        instrumentXml.setInstrumentType(instrument.getInstrumentType().name());
        IntUnit nSlit = instrumentXml.addNewNSlit();
        nSlit.setValue(BigInteger.valueOf(instrument.getNSlit()));
        DoubleUnit obstruction = instrumentXml.addNewObstruction();
        obstruction.setValue(instrument.getObstruction());
        DoubleUnit pixelScale = instrumentXml.addNewPixelScale();
        pixelScale.setValue(instrument.getPixelScale());
        pixelScale.setUnit(instrument.getPixelScaleUnit());
        instrumentXml.setPsfType(instrument.getPsfType().name());
        DoubleUnit rangeMax = instrumentXml.addNewRangeMax();
        rangeMax.setValue(instrument.getRangeMax());
        rangeMax.setUnit(instrument.getRangeMaxUnit());
        DoubleUnit rangeMin = instrumentXml.addNewRangeMin();
        rangeMin.setValue(instrument.getRangeMin());
        rangeMin.setUnit(instrument.getRangeMinUnit());
        DoubleUnit readout = instrumentXml.addNewReadout();
        readout.setValue(instrument.getReadout());
        readout.setUnit(instrument.getReadoutUnit());
        DoubleUnit slitLength = instrumentXml.addNewSlitLength();
        slitLength.setValue(instrument.getSlitLength());
        slitLength.setUnit(instrument.getSlitLengthUnit());
        DoubleUnit slitWidth = instrumentXml.addNewSlitWidth();
        slitWidth.setValue(instrument.getSlitWidth());
        slitWidth.setUnit(instrument.getSlitWidthUnit());
        if (instrument.getSpectralResolution() != null) {
            Graph spectralResolution = instrumentXml.addNewSpectralResolution();
            populateGraph(spectralResolution, provider.getDataset(Dataset.Type.SPECTRAL_RESOLUTION, instrument.getSpectralResolution()));
        }
        instrumentXml.setSpectrographType(instrument.getSpectrographType().name());
        if (instrument.getTransmission() != null) {
            Graph transmission = instrumentXml.addNewTransmission();
            populateGraph(transmission, provider.getDataset(Dataset.Type.TRANSMISSION, instrument.getTransmission()));
        }
        DoubleUnit strehlRatio = instrumentXml.addNewStrehlRatio();
        strehlRatio.setValue(instrument.getStrehlRatio());
        DoubleUnit refWavelength = instrumentXml.addNewRefWavelength();
        refWavelength.setValue(instrument.getRefWavelength());
        refWavelength.setUnit(instrument.getRefWavelengthUnit());
        DoubleUnit deltaLambdaPerPixel = instrumentXml.addNewDeltaLambdaPerPixel();
        deltaLambdaPerPixel.setValue(instrument.getDeltaLambdaPerPixel());
        deltaLambdaPerPixel.setUnit(instrument.getDeltaLambdaPerPixelUnit());
        instrumentXml.setSpectralResolutionType(instrument.getSpectralResolutionType().name());
        DoubleUnit psfDoubleGaussianMultiplier = instrumentXml.addNewPsfDoubleGaussianMultiplier();
        psfDoubleGaussianMultiplier.setValue(instrument.getPsfDoubleGaussianMultiplier());
        if (instrument.getPsfDoubleGaussianFwhm1() != null) {
            Graph psfDoubleGaussianFwhm1 = instrumentXml.addNewPsfDoubleGaussianFwhm1();
            populateGraph(psfDoubleGaussianFwhm1, provider.getDataset(Dataset.Type.PSF_DOUBLE_GAUSSIAN_FWHM_1, instrument.getPsfDoubleGaussianFwhm1()));
        }
        if (instrument.getPsfDoubleGaussianFwhm2() != null) {
            Graph psfDoubleGaussianFwhm2 = instrumentXml.addNewPsfDoubleGaussianFwhm2();
            populateGraph(psfDoubleGaussianFwhm2, provider.getDataset(Dataset.Type.PSF_DOUBLE_GAUSSIAN_FWHM_2, instrument.getPsfDoubleGaussianFwhm2()));
        }
        instrumentXml.setPsfSizeType(instrument.getPsfSizeType().name());
        DoubleUnit fixedPsfSize = instrumentXml.addNewFixedPsfSize();
        fixedPsfSize.setValue(instrument.getFixedPsfSize());
        fixedPsfSize.setUnit(instrument.getFixedPsfSizeUnit());
        if (instrument.getPsfSizeFunction()!= null) {
            Graph psfSizeFunction = instrumentXml.addNewPsfSizeFunction();
            populateGraph(psfSizeFunction, provider.getDataset(Dataset.Type.PSF_SIZE_FUNCTION, instrument.getPsfSizeFunction()));
        }
        DoubleUnit psfSizePercentage = instrumentXml.addNewPsfSizePercentage();
        psfSizePercentage.setValue(instrument.getPsfSizePercentage());
        
        instrumentDoc.save(file);
    }

    @Override
    public void exportSiteInFile(File file, Site site, DatasetProvider provider) throws IOException {
        SiteDocument siteDoc = SiteDocument.Factory.newInstance();
        org.cnrs.lam.dis.etc.dataimportexport.xml.Site siteXml = siteDoc.addNewSite();
        ComponentInfo info = siteXml.addNewInfo();
        info.setName(site.getInfo().getName());
        info.setDescription(site.getInfo().getDescription());
        DoubleUnit airMass = siteXml.addNewAirMass();
        airMass.setValue(site.getAirMass());
        siteXml.setGalacticContributing(site.isGalacticContributing());
        siteXml.setSeeingLimited(site.getSeeingLimited());
        if (site.getGalacticProfile() != null) {
            Graph galacticProfile = siteXml.addNewGalacticProfile();
            populateGraph(galacticProfile, provider.getDataset(Dataset.Type.GALACTIC, site.getGalacticProfile()));
        }
        siteXml.setLocationType(site.getLocationType().name());
        DoubleUnit seeing = siteXml.addNewSeeing();
        seeing.setValue(site.getSeeing());
        seeing.setUnit(site.getSeeingUnit());
        if (site.getSkyAbsorption() != null) {
            Graph skyAbsorption = siteXml.addNewSkyAbsorption();
            populateGraph(skyAbsorption, provider.getDataset(Dataset.Type.SKY_ABSORPTION, site.getSkyAbsorption()));
        }
        siteXml.setSkyEmissionSelectedOption(site.getSkyEmissionSelectedOption());
        if (site.getSkyEmission() != null) {
            List<String> optionList = provider.getDatasetOptionList(Dataset.Type.SKY_EMISSION, site.getSkyEmission());
            for (String option : optionList) {
                Graph skyEmission = siteXml.addNewSkyEmission();
                skyEmission.setOption(option);
                populateGraph(skyEmission, provider.getDataset(Dataset.Type.SKY_EMISSION, site.getSkyEmission(), option));
            }
        }
        if (site.getSkyExtinction() != null) {
            Graph skyExtinction = siteXml.addNewSkyExtinction();
            populateGraph(skyExtinction, provider.getDataset(Dataset.Type.SKY_EXTINCTION, site.getSkyExtinction()));
        }
        siteXml.setZodiacalContributing(site.isZodiacalContributing());
        if (site.getZodiacalProfile() != null) {
            Graph zodiacalProfile = siteXml.addNewZodiacalProfile();
            populateGraph(zodiacalProfile, provider.getDataset(Dataset.Type.ZODIACAL, site.getZodiacalProfile()));
        }
        if (site.getAtmosphericTransmission()!= null) {
            Graph atmosphericTransmission = siteXml.addNewAtmosphericTransmission();
            populateGraph(atmosphericTransmission, provider.getDataset(Dataset.Type.ATMOSPHERIC_TRANSMISSION, site.getAtmosphericTransmission()));
        }
        siteXml.setAtmosphericTransmissionType(site.getAtmosphericTransmissionType().name());
        siteDoc.save(file);
    }

    @Override
    public void exportSourceInFile(File file, Source source, DatasetProvider provider) throws IOException {
        SourceDocument sourceDoc = SourceDocument.Factory.newInstance();
        org.cnrs.lam.dis.etc.dataimportexport.xml.Source sourceXml = sourceDoc.addNewSource();
        ComponentInfo info = sourceXml.addNewInfo();
        info.setName(source.getInfo().getName());
        info.setDescription(source.getInfo().getDescription());
        DoubleUnit magnitudeWavelength = sourceXml.addNewMagnitudeWavelength();
        magnitudeWavelength.setValue(source.getMagnitudeWavelength());
        magnitudeWavelength.setUnit(source.getMagnitudeWavelengthUnit());
        DoubleUnit emissionLineWavelength = sourceXml.addNewEmissionLineWavelength();
        emissionLineWavelength.setValue(source.getEmissionLineWavelength());
        emissionLineWavelength.setUnit(source.getEmissionLineWavelengthUnit());
        DoubleUnit emissionLineFwhm = sourceXml.addNewEmissionLineFwhm();
        emissionLineFwhm.setValue(source.getEmissionLineFwhm());
        emissionLineFwhm.setUnit(source.getEmissionLineFwhmUnit());
        sourceXml.setExtendedSourceProfile(source.getExtendedSourceProfile().name());
        DoubleUnit extendedSourceRadius = sourceXml.addNewExtendedSourceRadius();
        extendedSourceRadius.setValue(source.getExtendedSourceRadius());
        extendedSourceRadius.setUnit(source.getExtendedSourceRadiusUnit());
        DoubleUnit magnitude = sourceXml.addNewMagnitude();
        magnitude.setValue(source.getMagnitude());
        DoubleUnit redshift = sourceXml.addNewRedshift();
        redshift.setValue(source.getRedshift());
        DoubleUnit temperature = sourceXml.addNewTemperature();
        temperature.setValue(source.getTemperature());
        temperature.setUnit(source.getTemperatureUnit());
        sourceXml.setSpatialDistributionType(source.getSpatialDistributionType().name());
        sourceXml.setSpectralDistributionType(source.getSpectralDistributionType().name());
        if (source.getSpectralDistributionTemplate() != null) {
            Graph spectralDistributionTemplate = sourceXml.addNewSpectralDistributionTemplate();
            populateGraph(spectralDistributionTemplate, provider.getDataset(Dataset.Type.SPECTRAL_DIST_TEMPLATE, source.getSpectralDistributionTemplate()));
        }
        DoubleUnit emissionLineFlux = sourceXml.addNewEmissionLineFlux();
        emissionLineFlux.setValue(source.getEmissionLineFlux());
        emissionLineFlux.setUnit(source.getEmissionLineFluxUnit());
        sourceXml.setExtendedMagnitudeType(source.getExtendedMagnitudeType().name());
        sourceDoc.save(file);
    }

    @Override
    public void exportObsParamInFile(File file, ObsParam obsParam, DatasetProvider provider) throws IOException {
        ObservingParametersDocument obsPramDoc = ObservingParametersDocument.Factory.newInstance();
        ObservingParameters obsParamXml = obsPramDoc.addNewObservingParameters();
        ComponentInfo info = obsParamXml.addNewInfo();
        info.setName(obsParam.getInfo().getName());
        info.setDescription(obsParam.getInfo().getDescription());
        DoubleUnit dit = obsParamXml.addNewDit();
        dit.setValue(obsParam.getDit());
        dit.setUnit(obsParam.getDitUnit());
        DoubleUnit exposureTime = obsParamXml.addNewExposureTime();
        exposureTime.setValue(obsParam.getExposureTime());
        exposureTime.setUnit(obsParam.getExposureTimeUnit());
        obsParamXml.setFixedParameter(obsParam.getFixedParameter().name());
        IntUnit noExpo = obsParamXml.addNewNoExpo();
        noExpo.setValue(BigInteger.valueOf(obsParam.getNoExpo()));
        DoubleUnit snr = obsParamXml.addNewSnr();
        snr.setValue(obsParam.getSnr());
        snr.setUnit(obsParam.getSnrUnit());
        DoubleUnit snrLambda = obsParamXml.addNewSnrLambda();
        snrLambda.setValue(obsParam.getSnrLambda());
        snrLambda.setUnit(obsParam.getSnrLambdaUnit());
        obsParamXml.setTimeSampleType(obsParam.getTimeSampleType().name());
        obsParamXml.setSpectralQuantumType(obsParam.getSpectralQuantumType().name());
        obsParamXml.setExtraBackgroundNoiseType(obsParam.getExtraBackgroundNoiseType().name());
        obsParamXml.setExtraSignalType(obsParam.getExtraSignalType().name());
        if (obsParam.getExtraBackgroundNoiseDataset() != null) {
            Graph extraBackgroundNoiseDataset = obsParamXml.addNewExtraBackgroundNoiseDataset();
            populateGraph(extraBackgroundNoiseDataset, provider.getDataset(Dataset.Type.EXTRA_BACKGROUND_NOISE, obsParam.getExtraBackgroundNoiseDataset()));
        }
        if (obsParam.getExtraSignalDataset() != null) {
            Graph extraSignalDataset = obsParamXml.addNewExtraSignalDataset();
            populateGraph(extraSignalDataset, provider.getDataset(Dataset.Type.EXTRA_SIGNAL, obsParam.getExtraSignalDataset()));
        }
        DoubleUnit extraBackgroundNoise = obsParamXml.addNewExtraBackgroundNoise();
        extraBackgroundNoise.setValue(obsParam.getExtraBackgroundNoise());
        extraBackgroundNoise.setUnit(obsParam.getExtraBackgroundNoiseUnit());
        DoubleUnit extraSignal = obsParamXml.addNewExtraSignal();
        extraSignal.setValue(obsParam.getExtraSignal());
        extraSignal.setUnit(obsParam.getExtraSignalUnit());
        obsParamXml.setFixedSnrType(obsParam.getFixedSnrType().name());
        obsPramDoc.save(file);
    }

    private void populateGraph(Graph graph, Dataset dataset) {
        graph.setName(dataset.getInfo().getName());
        graph.setUnitX(dataset.getXUnit());
        graph.setUnitY(dataset.getYUnit());
        for (Map.Entry<Double, Double> entry : dataset.getData().entrySet()) {
            Sample sample = graph.addNewSample();
            sample.setX(entry.getKey());
            sample.setY(entry.getValue());
        }
        graph.setDataType(dataset.getDataType().name());
    }

    @Override
    public void exportDatasetToFile(File file, Type type, DatasetInfo info, DatasetProvider provider) throws IOException {
        Dataset dataset = provider.getDataset(type, info);
        FileWriter writer = new FileWriter(file);
        for (Map.Entry<Double, Double> entry : dataset.getData().entrySet()) {
            writer.write(Double.toString(entry.getKey()));
            writer.write("\t");
            writer.write(Double.toString(entry.getValue()));
            writer.write(ConfigFactory.getConfig().getLineSeparator());
        }
        writer.close();
    }
}
