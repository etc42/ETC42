/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.dataimportexport;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.cnrs.lam.dis.etc.dataimportexport.xml.Graph;
import org.cnrs.lam.dis.etc.dataimportexport.xml.Site;
import org.cnrs.lam.dis.etc.datamodel.ComponentInfo;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class SiteWithDatasetsImpl implements SiteWithDatasets {

    private Site siteXml;
    private ComponentInfo componentInfo;

    public SiteWithDatasetsImpl(Site siteXml, ComponentInfo componentInfo) {
        this.siteXml = siteXml;
        this.componentInfo = componentInfo;
    }

    @Override
    public Dataset getGalacticProfileDataset() {
        Graph graph = siteXml.getGalacticProfile();
        return Helper.convertGraphToDataset(graph, componentInfo.getName(), Dataset.Type.GALACTIC);
    }

    @Override
    public Dataset getZodiacalProfileDataset() {
        Graph graph = siteXml.getZodiacalProfile();
        return Helper.convertGraphToDataset(graph, componentInfo.getName(), Dataset.Type.ZODIACAL);
    }

    @Override
    public Dataset getSkyAbsorptionDataset() {
        Graph graph = siteXml.getSkyAbsorption();
        return Helper.convertGraphToDataset(graph, componentInfo.getName(), Dataset.Type.SKY_ABSORPTION);
    }

    @Override
    public Map<String, Dataset> getSkyEmissionDatasetMap() {
        Map<String, Dataset> result = new HashMap<String, Dataset>();
        for (Graph graph : siteXml.getSkyEmissionArray()) {
            String option = graph.getOption();
            Dataset dataset = Helper.convertGraphToDataset(graph, componentInfo.getName(), Dataset.Type.SKY_EMISSION, option);
            result.put(option, dataset);
        }
        return result;
    }

    @Override
    public Dataset getSkyExtinctionDataset() {
        Graph graph = siteXml.getSkyExtinction();
        return Helper.convertGraphToDataset(graph, componentInfo.getName(), Dataset.Type.SKY_EXTINCTION);
    }

    @Override
    public ComponentInfo getInfo() {
        return componentInfo;
    }

    @Override
    public LocationType getLocationType() {
        return LocationType.valueOf(siteXml.getLocationType());
    }

    @Override
    public void setLocationType(LocationType type) {
        throw new UnsupportedOperationException("Read only version of Site");
    }

    @Override
    public double getAirMass() {
        return siteXml.getAirMass().getValue();
    }

    @Override
    public void setAirMass(double airMass) {
        throw new UnsupportedOperationException("Read only version of Site");
    }

    @Override
    public double getSeeing() {
        return siteXml.getSeeing().getValue();
    }

    @Override
    public void setSeeing(double seeing) {
        throw new UnsupportedOperationException("Read only version of Site");
    }

    @Override
    public String getSeeingUnit() {
        return siteXml.getSeeing().getUnit();
    }

    @Override
    public DatasetInfo getSkyEmission() {
        if (siteXml.getSkyEmissionArray() == null || siteXml.getSkyEmissionArray().length == 0)
            return null;
        String name = siteXml.getSkyEmissionArray(0).getName();
        String namespace = componentInfo.getName();
        List<String> optionList = new ArrayList<String>();
        for (Graph graph : siteXml.getSkyEmissionArray()) {
            optionList.add(graph.getOption());
        }
        return new DatasetInfo(name, namespace, null, optionList);
    }

    @Override
    public void setSkyEmission(DatasetInfo skyEmission) {
        throw new UnsupportedOperationException("Read only version of Site");
    }

    @Override
    public String getSkyEmissionSelectedOption() {
        return siteXml.getSkyEmissionSelectedOption();
    }

    @Override
    public void setSkyEmissionSelectedOption(String option) {
        throw new UnsupportedOperationException("Read only version of Site");
    }

    @Override
    public DatasetInfo getSkyAbsorption() {
        if (siteXml.getSkyAbsorption() == null)
            return null;
        return new DatasetInfo(siteXml.getSkyAbsorption().getName(), componentInfo.getName(), null);
    }

    @Override
    public void setSkyAbsorption(DatasetInfo skyAbsorption) {
        throw new UnsupportedOperationException("Read only version of Site");
    }

    @Override
    public DatasetInfo getSkyExtinction() {
        if (siteXml.getSkyExtinction() == null)
            return null;
        return new DatasetInfo(siteXml.getSkyExtinction().getName(), componentInfo.getName(), null);
    }

    @Override
    public void setSkyExtinction(DatasetInfo skyExtinction) {
        throw new UnsupportedOperationException("Read only version of Site");
    }

    @Override
    public boolean isZodiacalContributing() {
        return siteXml.getZodiacalContributing();
    }

    @Override
    public void setZodiacalContributing(boolean contributes) {
        throw new UnsupportedOperationException("Read only version of Site");
    }

    @Override
    public boolean isGalacticContributing() {
        return siteXml.getGalacticContributing();
    }

    @Override
    public void setGalacticContributing(boolean contributes) {
        throw new UnsupportedOperationException("Read only version of Site");
    }

    @Override
    public DatasetInfo getGalacticProfile() {
        if (siteXml.getGalacticProfile() == null)
            return null;
        return new DatasetInfo(siteXml.getGalacticProfile().getName(), componentInfo.getName(), null);
    }

    @Override
    public void setGalacticProfile(DatasetInfo profile) {
        throw new UnsupportedOperationException("Read only version of Site");
    }

    @Override
    public DatasetInfo getZodiacalProfile() {
        if (siteXml.getZodiacalProfile() == null)
            return null;
        return new DatasetInfo(siteXml.getZodiacalProfile().getName(), componentInfo.getName(), null);
    }

    @Override
    public void setZodiacalProfile(DatasetInfo profile) {
        throw new UnsupportedOperationException("Read only version of Site");
    }

    @Override
    public boolean getSeeingLimited() {
        return siteXml.getSeeingLimited();
    }

    @Override
    public void setSeeingLimited(boolean contributes) {
        throw new UnsupportedOperationException("Read only version of Site");
    }

    @Override
    public DatasetInfo getAtmosphericTransmission() {
        if (siteXml.getAtmosphericTransmission() == null)
            return null;
        return new DatasetInfo(siteXml.getAtmosphericTransmission().getName(), componentInfo.getName(), null);
    }

    @Override
    public void setAtmosphericTransmission(DatasetInfo profile) {
        throw new UnsupportedOperationException("Read only version of Site");
    }

    @Override
    public Dataset getAtmosphericTransmissionDataset() {
        Graph graph = siteXml.getAtmosphericTransmission();
        return Helper.convertGraphToDataset(graph, componentInfo.getName(), Dataset.Type.ATMOSPHERIC_TRANSMISSION);
    }

    @Override
    public AtmosphericTransmissionType getAtmosphericTransmissionType() {
        return AtmosphericTransmissionType.valueOf(siteXml.getAtmosphericTransmissionType());
    }

    @Override
    public void setAtmosphericTransmissionType(AtmosphericTransmissionType type) {
        throw new UnsupportedOperationException("Read only version of Site");
    }

}
