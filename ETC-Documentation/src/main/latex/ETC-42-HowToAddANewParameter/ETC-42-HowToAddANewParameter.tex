\documentclass[a4paper,11pt]{article}
%LaTeX Model for Lab reports
%Tested and used in Mac OS X with MacTEX
% initial Felipe Brando Cavalcanti - LARA, UnB, Brasilia, Brazil

%Packages
%\usepackage[latin1]{inputenc}
%\usepackage[cyr]{aeguill}
%\usepackage[francais]{babel}
\usepackage[T1]{fontenc}
\usepackage{subfig}
\usepackage{url} %URLs
\usepackage{makeidx} %Index
\usepackage[pdftex]{graphicx} %Graphics
\usepackage{amsfonts} %Math fonts
%\usepackage{indentfirst} %Makes it indent the first paragraph of the section
\usepackage{listings} %Code support, properly highlighted
\usepackage{color}
\usepackage{verbatim}%Better verbatim
%\usepackage{fancyvrb} %Fancy verbatim
\usepackage{cite}%Better citation
%\usepackage{siunitx} %SI Units 
\usepackage{hyperref} %Makes links to your references, making your life quite a bit easier.
%\usepackage[pdftex]{colortbl} %Color Tables
\usepackage{array} %Better tables - improves the algorythms

\hypersetup{ %Sets up hyperref
    %bookmarks=true,         % show bookmarks bar?
    %unicode=false,          % non-Latin characters in Acrobat?s bookmarks
    %pdftoolbar=true,        % show Acrobat?s toolbar?
    %pdfmenubar=true,        % show Acrobat?s menu?
    %pdffitwindow=false,     % window fit to page when opened
    %pdfstartview={FitH},    % fits the width of the page to the window
    pdftitle={ETC-42 HOW TO add a new parameter to the data model},    % title
    pdfauthor={Nikolaos Apostolakos},     % author
    colorlinks=false,       % false: boxed links; true: colored links
    linkcolor=red,          % color of internal links
    citecolor=green,        % color of links to bibliography
    filecolor=magenta,      % color of file links
    urlcolor=cyan           % color of external links
}
\lstset{ %Sets up lisitings, so we get highlighted code
language=JAVA,                % choose the language of the code
basicstyle=\footnotesize,       % the size of the fonts that are used for the code
numbers=left,                   % where to put the line-numbers
numberstyle=\footnotesize,      % the size of the fonts that are used for the line-numbers
stepnumber=2,                   % the step between two line-numbers. If it's 1 each line will be numbered
numbersep=5pt,                  % how far the line-numbers are from the code
backgroundcolor=\color{white},  % choose the background color. You must add \usepackage{color}
showspaces=false,               % show spaces adding particular underscores
showstringspaces=false,         % underline spaces within strings
showtabs=false,                 % show tabs within strings adding particular underscores
frame=single,	                % adds a frame around the code
tabsize=2,	                % sets default tabsize to 2 spaces
captionpos=b,                   % sets the caption-position to bottom
breaklines=true,                % sets automatic line breaking
breakatwhitespace=false,        % sets if automatic breaks should only happen at whitespace
escapeinside={\%*}{*)}          % if you want to add a comment within your code
}


%\parindent15pt  \parskip0pt
\usepackage{parskip} %No paragraph indentation, single space between paragraphs
\setlength\voffset{-2.0cm}
\setlength\hoffset{-1.5cm}
\setlength\textwidth{16.0cm}
\setlength\textheight{24.5cm}
\setlength\baselineskip{2cm}
\renewcommand{\baselinestretch}{1.2}

\newcommand{\HRule}{\rule{\linewidth}{0.5mm}}
\newcommand{\cesam}{CeSAM} 
\newcommand{\etcdocurl}{{\it \url{http://projets.oamp.fr/projects/etc/wiki/Documentation}}}

\usepackage{graphicx}
\begin{document}
\begin{titlepage}
\begin{center}
 
% Upper part of the page
\includegraphics[width=0.25\textwidth]{./images/Logo_LAM_grand.jpg}\\[1cm]
\includegraphics[width=0.25\textwidth]{./images/logo_cesam_big.jpg}\\[1cm]
 
\textsc{\LARGE ETC-42 : Universal Exposure Time Calculator}\\[1.5cm]
  
% Title
\HRule \\[0.4cm]
{ \huge \bfseries HOW TO\\ add a new parameter to the data model}
\HRule
\vspace{0.75cm}
\large Laboratoire d'Astrophysique de Marseille\\ Centre donneeS Astrophysique de Marseille \\
\vspace{0.8cm}
% Author and supervisor
\begin{minipage}{0.4\textwidth}
\begin{flushleft} \large
\emph{Authors:}\\
N. Apostolakos
\end{flushleft}
\end{minipage}
\begin{minipage}{0.4\textwidth}
\begin{flushright} \large
%\emph{Orientador:} \\
%Geovany Ara�jo Borges
\end{flushright}
\end{minipage}
 
\vfill
 
% Bottom of the page
{\large November 2012}
\end{center}
\end{titlepage}

%\begin{minipage}{0.9\textwidth}
\tableofcontents
 \vfill
%\end{minipage}
\newpage

\pagestyle{plain}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction} 

This is a technical document of the ETC project and is written as a help for developers who want to apply code modifications. It describes in detail the necessary modifications for successfully adding a new parameter in any of the components (\textit{Instrument}, \textit{Site}, \textit{Source} or \textit{ObsParam}). The actions described in this document should be done when there is the need for storing some new parameter given by the user.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Data model changes}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Update the related interface}

The first step is to update the related interface in the \textbf{ETC-DataModel} project. These interfaces are in the package \verb!org.cnrs.lam.dis.etc.datamodel! and they are the classes \verb!Instrument.java!, \verb!Site.java!, \verb!Source.java! and \verb!ObsParam.java!.

If the new parameter is a \textbf{primitive data type} (int, long, float, etc) then the only thing need to be done is the declaration of public getter and setter methods. For example if the new parameter is a double with name slitLength, the following lines need to be added at the interface:
\lstset{language=Java}
\begin{lstlisting}
public double getSlitLength();
public void setSlitLength(double slitLength);
\end{lstlisting}

In the case the parameter is a \textbf{dataset} there is need again only for declaration of public getter and setter methods. For example if the new parameter is the total transmission, the following lines need to be added at the interface:
\begin{lstlisting}
public DatasetInfo getTransmission();
public void setTransmission(DatasetInfo transmission);
\end{lstlisting}

If the parameter should be represented with an enumeration, then the enumeration needs also to be defined in the interface. For example if the new parameter is the psfType and it can have one of the options auto, profile and adaptive optics, the following lines need to be added at the interface:
\lstset{language=Java}
\begin{lstlisting}
public enum PsfType {
    AUTO, PROFILE, AO
}

public PsfType getPsfType();
public void setPsfType(PsfType type);
\end{lstlisting}

Finally, if the parameter has a unit, a getter method for it needs also to be declared. For example, the slitLength of the first example is expressed in some unit, so the following line needs to be added:
\lstset{language=Java}
\begin{lstlisting}
public String getSlitLengthUnit();
\end{lstlisting}
Note that the returned type has to be \textbf{String} and the convention used by ETC for naming the getter is \textbf{get\textit{ParameterName}Unit}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Dataset parameter specific changes}

When the added parameter is a dataset the following extra steps that need to be also performed.
First, a new dataset type need to be added in the\verb!Type! enumeration of the \verb!Dataset.java! class (in the \verb!datamodel! package). For example, if the dataset \verb!transmission! was added in the \verb!Instrument! component, the following line needs to be added:
\lstset{language=Java}
\begin{lstlisting}
public enum Type {
    ...
    TRANSMISSION(ComponentType.INSTRUMENT, false),
\end{lstlisting}

Note that the first parameter is the component in which the dataset was added and that the second parameter is a flag showing if the dataset is a multi-dataset or not, where true means multi-dataset and false single dataset (most of the times this will be false).

Second, a humanly readable translation of the new type must be added in the \verb!messages.properties! file, in the directory \verb!src/main/resources/org/cnrs/lam/dis/etc/datamodel!. Note that the exact name of the enumeration defined above must be used as the translation code:
\lstset{language=Java}
\begin{lstlisting}
TRANSMISSION=Instrument transmission
\end{lstlisting}

Finally, a humanly readable translation for the axes labels (used when the dataset is plotted by using the GUI) need to be added in the \verb!messages.properties! file of the project ETC-Controller, in the directory \verb!src/main/resources/org/cnrs/lam/dis/etc/controller!. For the transmission dataset the following lines need to be added:
\lstset{language=Java}
\begin{lstlisting}
TRANSMISSION_PLOT_X_DESC=Wavelength
TRANSMISSION_PLOT_Y_DESC=Total instrument transmission
\end{lstlisting}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Database changes}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{CreateTables.sql script}

The \textbf{ETC-Persistence} project contains an SQL script for generating all the database schema from scratch (in the \verb!src\main\sql! directory). This script needs to be updated to add a new column for the new parameter in the related table (and a column for the unit, if necessary). The ETC convention for the column name is to use capital letters with underscores (\_) between the different words.

For the correct conversion of the data between the java and the database the following mapping of data types must be followed for the column type:

\begin{tabular}{|r|l|}
\hline
	\textbf{Java Type} & \textbf{Database Type}\\
\hline
	boolean & SMALLINT\\
\hline
	short & SMALLINT\\
\hline
	int & INTEGER\\
\hline
	long & BIGINT\\
\hline
	float & REAL\\
\hline
	double & DOUBLE\\
\hline
	String & VARCHAR(50)\\
\hline
	enumeration & VARCHAR(255) NOT NULL\\
\hline
	Dataset & BIGINT\\
\hline
\end{tabular}

For example, for adding the slitLength (double with unit), the psfType (enumeration) and the transmission (dataset) described earlier, the following lines need to be added at the creation of the INSTRUMENT table:
\lstset{language=SQL}
\begin{lstlisting}
SLIT_LENGTH DOUBLE, 
SLIT_LENGTH_UNIT VARCHAR(50),
PSF_TYPE VARCHAR(255) NOT NULL,
TRANSMISSION BIGINT,
\end{lstlisting}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Update the schema version}

Because the ETC database is installed locally in every machine where the system is running, there is the need for versioning the database schema. This is done with the use of the table \textbf{ETC\_INFO} which contains the field \textbf{DB\_VERSION}. This field must \textbf{always} be increased \textbf{BY 1} every time the schema is modified, as it is used by the system to apply patches to older databases.

The version number must be increased in two places. The first one is the \verb!CreateTables.sql! script (mentioned above). The line which initialize the values of the table \textbf{ETC\_INFO} needs to be modified with the new version. For example, if the previous version was 3, the line must be modified as:
\lstset{language=SQL}
\begin{lstlisting}
INSERT INTO ETC_INFO VALUES (4);
\end{lstlisting}

The second place where the database schema version needs to be changed is in the file \verb!DbUpdater.java! of the package \verb!org.cnrs.lam.dis.etc.controller! (project \textbf{ETC-Controller}). This class has the field \verb!DB_VERSION! which keeps the current database schema version. For example, if the previous version was 3, the class \verb!DbUpdater.java! should be modified as:
\lstset{language=Java}
\begin{lstlisting}
private static final int DB_VERSION = 4;
\end{lstlisting}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{DbUpdater patch}

The method of the ETC for updating the already installed databases is implemented by the class \verb!DbUpdater! mentioned above. The way this class works is by applying patches to the database for updating version by version, until the latest version. For implementing a new patch a new method needs to be added in the class. This method must be \textbf{static}, it must get one argument of type \verb!java.sql.Statement! and it must be named like \textbf{updateFromVersion\textless \textit{OldVersion}\textgreater To\textless \textit{NewVersion}\textgreater }. Here it must be noted that the old version and the new version must differ only by one. There is no case where a patch might increase the version number by 2 or more (a version change that does not require a patch doesn't make sense anyways).

The update method must implement all the table modifications, by using the statement provided. The method is going to be called from inside a transaction, so transaction control is not necessary, but the method should throw an \verb!SQLException! at the case of any failure. The given statement is handled by the caller and it should not be closed. If the patch need to use a prepared statement, it can use the database connection of the parameter statement like this:
\lstset{language=Java}
\begin{lstlisting}
statement.getConnection().prepareStatement(...);
\end{lstlisting}

In the case the new parameter is an enumeration (and the new column is not allowing null values), a proper default value must also be given. This value \textbf{must} be the name of the enumeration constant. The same should be done also for units, as the current version of ETC does not allow their modification.

For example, for updating the database for the slitLength, the transmission and the psfType as mentioned above, assuming the previous version was 3, the following method needs to be added in the \verb!DbUpdater! class:
\lstset{language=Java}
\begin{lstlisting}
private static void updateFromVersion3To4(Statement statement)
                                           throws SQLException {
    // Make the table modifications
    statement.executeUpdate("ALTER TABLE INSTRUMENT ADD SLIT_LENGTH DOUBLE");
    statement.executeUpdate("ALTER TABLE INSTRUMENT ADD " +
                            "SLIT_LENGTH_UNIT VARCHAR(50) DEFAULT 'arcsec'");
    statement.executeUpdate("ALTER TABLE INSTRUMENT ADD TRANSMISSION BIGINT");
    statement.executeUpdate("ALTER TABLE INSTRUMENT ADD " +
                            "PSF_TYPE VARCHAR(255) NOT NULL " +
                            "DEFAULT 'AUTO'");
}
\end{lstlisting}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{JPA changes}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Entity class}

For the correct communication between the Java world and the database word, the entity class of the related component must be updated. These classes are in the project \textbf{ETC-Persistence} and the package \verb!org.cnrs.lam.dis.etc.persistence.database.entities!. They are the classes ending with the keyword \textbf{Entity} (for example \verb!InstrumentEntity!). There are two necessary modifications in the entity files.

First, a new field need to be created to map to the new parameter. The type of the field must be according the modifications done to the component interface (first step of this document). The field must also have the following JPA annotations:

\begin{tabular}{|l|l|}
\hline
	\verb!@Column(name="<COLUMN_NAME>")! & For all the parameters except of datasets\\
\hline
	\verb!@Basic(optional=false)! & For columns which are not null (like enumerations)\\
\hline
	\verb!@Enumerated(EnumType.STRING)! & For enumerations\\
\hline
	\verb!@JoinColumn(name="<COLUMN_NAME>",!& For datasets\\
	\verb!      referencedColumnName="ID")! & \\
\hline
	\verb!@ManyToOne! & For datasets\\
\hline
\end{tabular}

For example, for the slitLength, the psfType and the filter transmission used earlier, the following fields should be added in the \verb!InstrumentEntity! class (note that the filter transmission is of type \verb!DatasetInfoEntity! and it refers to another entity class):
\lstset{language=Java}
\begin{lstlisting}
@Column(name = "SLIT_LENGTH")
private double slitLength;

@Column(name = "SLIT_LENGTH_UNIT")
private String slitLengthUnit;

@Basic(optional = false)
@Column(name = "PSF_TYPE")
@Enumerated(EnumType.STRING)
private PsfType psfType;

@JoinColumn(name = "TRANSMISSION", referencedColumnName = "ID")
@ManyToOne
private DatasetInfoEntity transmissionEntity;
\end{lstlisting}

Second, getter and setter methods need to be added for accessing the new parameters. The setter methods must take care to update the \verb!modified! flag when the parameters value is changing. For example:
\lstset{language=Java}
\begin{lstlisting}
@Override
public double getSlitLength() {
    return slitLength;
}

@Override
public void setSlitLength(double slitLength) {
    if (this.slitLength != slitLength)
        modified = true;
    this.slitLength = slitLength;
}

@Override
public String getSlitLengthUnit() {
    return slitLengthUnit;
}

public void setSlitLengthUnit(String slitLengthUnit) {
    if(this.slitLengthUnit != null && !this.slitLengthUnit.equals(slitLengthUnit))
       modified = true;
   this.slitLengthUnit = slitLengthUnit;
}

@Override
public PsfType getPsfType() {
    return psfType;
}
    
@Override
public void setPsfType(PsfType psfType) {
    if (this.psfType != psfType)
        modified = true;
    this.psfType = psfType;
}
\end{lstlisting}

In the case of a dataset parameter, the implementation of the getter and setter methods of the interface is a little bit more complicated, as there is a variable of \verb!DatasetInfoEntity! type and not directly a \verb!DatasetInfo!. Because of that, there is need for getter and setter methods for the entity variable and the interface methods should use them to retrieve and set the entity (updating also correctly the \verb!modified! flag). For example:
\lstset{language=Java}
\begin{lstlisting}
public DatasetInfoEntity getTransmissionEntity() {
    return transmissionEntity;
}

public void setTransmissionEntity(DatasetInfoEntity transmissionEntity) {
    this.transmissionEntity = transmissionEntity;
}

@Override
public DatasetInfo getTransmission() {
    if (getTransmissionEntity() == null) }
        return null;
    }
    return new DatasetInfo(getTransmissionEntity().getName(),
                           getTransmissionEntity().getNamespace(),
                           getTransmissionEntity().getDescription());
}

@Override
public void setTransmission(DatasetInfo transmission) {
    // Check if the new transmission is the same with the old one or not
    if ((this.transmissionEntity == null && transmission != null) ||
            (this.transmissionEntity != null && transmission == null) ||
            (this.transmissionEntity != null && transmission != null &&
            !(this.transmissionEntity.getName().equals(transmission.getName()) &&
            (this.transmissionEntity.getNamespace() == null
                 ? transmission.getNamespace() == null
                 : this.transmissionEntity.getNamespace()
                          .equals(transmission.getNamespace())
            ))))
        modified = true;
    if (transmission == null) {
        setTransmissionEntity(null);
        return;
    }
    DatasetInfoEntityJpaController controller
        = new DatasetInfoEntityJpaController();
    setTransmissionEntity(controller
        .findDatasetEntity(Type.TRANSMISSION, transmission));
}
\end{lstlisting}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Static metamodel class}

The ETC uses static metamodel classes to allow use of strongly typed criteria. These classes are in the package \verb!org.cnrs.lam.dis.etc.persistence.database.entities! of the project \textbf{ETC-Persistence} and they are the classes ending with the keyword \textbf{Entity\_} (for example \verb!InstrumentEntity_!). These classes are used to map the entity fields to specific types and a new mapping needs to be added for the new parameter. The mapping is done by adding a \verb!SingularAttribute! field in the class, which uses generics for doing the mapping.

For example, for the slitLength, the transmission and the psfType, the following fields need to be added in the \verb!InstrumentEntity_! class:
\lstset{language=Java}
\begin{lstlisting}
public static volatile SingularAttribute<InstrumentEntity,Double> slitLength;
public static volatile SingularAttribute<InstrumentEntity,String> slitLengthUnit;
public static volatile SingularAttribute<InstrumentEntity,PsfType> psfType;
public static volatile SingularAttribute<InstrumentEntity,DatasetInfoEntity> 
                                                              transmissionEntity;
\end{lstlisting}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Entity controller}

Each entity has a corresponding controller class, which provides operations related with the entity. These classes are in the package \verb!org.cnrs.lam.dis.etc.persistence.database.entities! of the project \textbf{ETC-Persistence} and they are the classes ending with the keyword \textbf{EntityJpaController} (for example \verb!InstrumentEntityJpaController!). This controller contains a method for creating new entity objects, named \verb!createNewUnpersisted<Component>Entity! which needs to be updated to set default values for the new parameters. If a new parameter is an enumeration (or any other not null field) or it has a unit this step is necessary. In any other case it can be omitted.

For continuing the example with the psfType and the slitLength, the following lines need to be added in the \verb!createNewUnpersistedInstrumentEntity! method of the \verb!InstrumentEntityJpaController! class:
\lstset{language=Java}
\begin{lstlisting}
instrument.setSlitLengthUnit("arcsec");
instrument.setPsfType(PsfType.AUTO);
\end{lstlisting}
Note that a default is given also for the units, as currently there is no GUI support for setting the units of the parameters.

In the case of a dataset parameter the method \verb!find<Components>UsingDataset! (where \verb!<Components>! is replaced with Instruments, Sites, etc accordingly) must also be modified, to add in the \verb!cb.or! method the new dataset as parameter. For the transmission example the following line should be added:
\lstset{language=Java}
\begin{lstlisting}
cb.equal(from.get(InstrumentEntity_.transmissionEntity), datasetInfoEntity)
\end{lstlisting}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{SessionManagerImpl class}

Finally, the \verb!SessionManagerImpl! class needs to be updated to copy over the new parameters when the SaveAs option is executed. This class is in the project \textbf{ETC-Persistence} and the package \verb!org.cnrs.lam.dis.etc.persistence.database!, and the related methods are the ones named \verb!save<Component>As!.

For our example the following lines need to be added in the method \verb!saveInstrumentAs!:
\lstset{language=Java}
\begin{lstlisting}
// Copy the slit width and its unit
newInstrument.setSlitWidth(instrument.getSlitWidth());
newInstrument.setSlitWidthUnit(instrument.getSlitWidthUnit());
// Copy the PSF type
newInstrument.setPsfType(instrument.getPsfType());
// Copy the transmission (change the namespace if necessary)
if (instrument.getTransmission() == null
        || instrument.getTransmission().getNamespace() == null) {
    newInstrument.setTransmission(instrument.getTransmission());
} else {
    DatasetInfo datasetInfo = instrument.getTransmission();
    if (!newInfo.getName().equals(datasetInfo.getNamespace())) {
        datasetInfo = copyDatasetInNamespace(Type.TRANSMISSION
            , instrument.getTransmission(), newInfo.getName());
    }
    newInstrument.setTransmission(datasetInfo);
}
\end{lstlisting}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{DatasetInfoEntityController class (dataset parameters only)}

In the case a dataset parameter is added the method \verb!find<Component>DatasetsInNamespace! (where \verb!<Component>! is replaced with Instrument, Site, etc) of the class \verb!DatasetInfoEntityController! needs to be modified, to search also for the new type of dataset.

For the transmission example, the following line should be added in the \verb!cb.or! parameters:
\lstset{language=Java}
\begin{lstlisting}
cb.equal(from.get(DatasetInfoEntity_.type), Type.TRANSMISSION)
\end{lstlisting}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{XML Beans updates}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{XSD file}

The ETC uses XML files for importing and exporting components and more precisely the XMLBeans framework. The XSD definitions of the XML are in the directory \verb!src\main\xsd! of the \textbf{ETC-DataImportExport} project. These files need to be updated for the new parameters.

For the slitLength, transmission and psfType example, the following lines need to be added in the \verb!Instrument.xsd! file:
\lstset{language=XML}
\begin{lstlisting}
<xs:element name="slitLength" type="DoubleUnit"/>
<xs:element name="psfType" type="xs:string"/>
<xs:element name="transmission" type="Graph"/>
\end{lstlisting}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Build auto generated classes}
The XMLBeans auto generated classes are generated automatically when the \textbf{ETC-DataImportExport} project is compiled. It is though recommended at this step to generate them, so if an IDE tool is used (like eclipse, netbeans, etc) the autocomplete will work correctly for the next steps. The generation of the classes can be done by trying to build the ETC. The build will fail, as there are more code changes need to be done, but the XMLBeans classes will be generated.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{ComponentWithDatasets interface}

Only for the case a new parameter is a dataset, a new method must be added in the interface \verb!<Component>WithDatasets! of the pachage \verb!org.cnrs.lam.dis.etc.dataimportexport!, to allow retrieval of the dataset. For the example of the transmission the following method should be added:
\lstset{language=Java}
\begin{lstlisting}
public Dataset getTransmissionDataset();
\end{lstlisting}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{ComponentWithDatasetsImpl class}

The \textbf{ETC-DataImportExport} project defines an ETC component implementation, which is used as a wrapper above the XMLBeans classes. These classes are in the package \verb!org.cnrs.lam.dis.etc.dataimportexport! and they are ending with the keyword \textbf{WithDatasetsImpl} (for example \verb!InstrumentWithDatasetsImpl!). As these classes are used only when importing a component, the getter method of the parameter must be implemented to delegate the call to the XMLBean and the setter method must be implemented to throw an \verb!UnsupportedOperationException!. Note that in the case of a dataset parameter an extra method needs to be implemented, to return the data (and not only the dataset info).

For example, for the slitLength, the transmission and the psfType, the following methods need to be added in the \verb!InstrumentWithDatasetsImpl! class:
\lstset{language=Java}
\begin{lstlisting}
@Override
public double getSlitLength() {
    return instrumentXml.getSlitLength().getValue();
}

@Override
public void setSlitLength(double slitLength) {
    throw new UnsupportedOperationException("Read only version of Instrument");
}

@Override
public String getSlitLengthUnit() {
    return instrumentXml.getSlitLength().getUnit();
}

@Override
public PsfType getPsfType() {
    return PsfType.valueOf(instrumentXml.getPsfType());
}

@Override
public void setPsfType(PsfType type) {
    throw new UnsupportedOperationException("Read only version of Instrument");
}

@Override
public Dataset getTransmissionDataset() {
    Graph graph = instrumentXml.getTransmission();
    return Helper.convertGraphToDataset(graph, componentInfo.getName()
                                        , Dataset.Type.TRANSMISSION);
}

@Override
public DatasetInfo getTransmission() {
    if (instrumentXml.getTransmission() == null) {
        return null;
    }
    return new DatasetInfo(instrumentXml.getTransmission().getName()
                           , componentInfo.getName(), null);
}

@Override
public void setTransmission(DatasetInfo transmission) {
    throw new UnsupportedOperationException("Read only version of Instrument");
}
\end{lstlisting}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{DataImporterImpl class (dataset parameters only)}

If the new parameter is a dataset, the \verb!DataImporterImpl! class needs to be modified. More precisely the method \verb!importDatasetFromComponentFile! need to be modified to have a new case in the switch statement, for example:
\lstset{language=Java}
\begin{lstlisting}
switch (type) {
   ...
   case TRANSMISSION:
       dataset = getDatasetFromInstrument(file, type, info);
       break;
   ...
}
\end{lstlisting}

and also the method \verb!getDatasetFrom<Component>! need to be modified to return the correct dataset, for example:
\lstset{language=Java}
\begin{lstlisting}
case TRANSMISSION:
    return instrument.getTransmissionDataset();
\end{lstlisting}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{DataExporterImpl class}

Finally, the class \verb!DataExporterImpl! of the package \verb!org.cnrs.lam.dis.etc.dataimportexport! need to be updated to export correctly the new parameter. This class contains methods with names \verb!export<Component>InFile! which are creating a new XMLBeans object and they set all the parameters.

For example, for the slitLength, the transmission and the psfType, the following lines need to be added in the \verb!exportInstrumentInFile! method of the \verb!DataExporterImpl! class:
\lstset{language=Java}
\begin{lstlisting}
// Set the slit length value and unit
DoubleUnit slitLength = instrumentXml.addNewSlitLength();
slitLength.setValue(instrument.getSlitLength());
slitLength.setUnit(instrument.getSlitLengthUnit());

// Set the psf type
instrumentXml.setPsfType(instrument.getPsfType().name());

// Set the transmission
if (instrument.getTransmission() != null) {
    Graph transmission = instrumentXml.addNewTransmission();
    populateGraph(transmission, provider.getDataset(Dataset.Type.TRANSMISSION
                                             , instrument.getTransmission()));
}
\end{lstlisting}
Note that for the enumerations the parameter passed is the result of the \verb!name! method. This is for the case the \verb!toString! method is overridden.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Epilogue}

After applying all the above changes the ETC will be handling correctly the new parameter. The next steps will be to modify the \textbf{ETC-Calculator} project for implementing the new logic of the SNR calculation related with the new parameter and to implement the GUI modifications necessary in the \textbf{ETC-UI} project for presenting the new parameter to the user and let him modify its value. These steps are out of the scope of this document and are covered in other dedicated documents.

Here is must be noted that after the modifications mentioned in this document the command line version of the ETC (when it runs with the parameter \verb!-Detc.uiMode=cl!) will be directly usable with the new parameter, as it uses reflection and it will automatically detect the new parameter. This mode can be used for developing the calculator modifications before implementing the GUI changes.



\end{document}