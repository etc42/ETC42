/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.plugins.skybackground.skybrightnesstable.calculation;

import java.util.Map;
import java.util.TreeMap;
import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.apache.commons.math.analysis.integration.RombergIntegrator;

/**
 */
public class AtmosphericEfficiency implements BoundedFunction, IntegrableFunction {
    
    private double lowerBound;
    private double upperBound;
    private BoundedFunction absorption;
    private BoundedFunction extinction;

    public AtmosphericEfficiency(BoundedFunction absorption, BoundedFunction extinction) throws CalculationException {
        this.absorption = absorption;
        this.extinction = extinction;
        lowerBound = Math.max(absorption.getLowerBound(), extinction.getLowerBound());
        upperBound = Math.min(absorption.getUpperBound(), extinction.getUpperBound());
        if (lowerBound > upperBound) {
            throw new CalculationException("Atmospheric absorption and extinction are not overlapping");
        }
    }

    @Override
    public double getLowerBound() {
        return lowerBound;
    }

    @Override
    public double getUpperBound() {
        return upperBound;
    }

    @Override
    public double value(double x) throws CalculationException {
        double value = absorption.value(x) * Math.pow(10, -0.4 * extinction.value(x));
        return (value <= 0) ? 0 : value;
    }

    @Override
    public double integral(double x1, double x2) throws CalculationException {
        RombergIntegrator integrator = new RombergIntegrator();
        UnivariateRealFunction urf = new UnivariateRealFunction() {
            @Override
            public double value(double x) throws FunctionEvaluationException {
                double value = 0;
                try {
                    value = AtmosphericEfficiency.this.value(x);
                } catch (CalculationException ex) {
                    throw new FunctionEvaluationException(ex, x);
                }
                return value;
            }
        };
        double result = 0;
        try {
            result = integrator.integrate(urf, x1, x2);
        } catch (Exception ex) {
            throw new CalculationException(ex);
        }
        return result;
    }
    
    public Map<Double, Double> mapToPlot() {
        Map<Double, Double> result = new TreeMap<Double, Double>();
        for (double x = getLowerBound(); x <= getUpperBound(); x+= 0.1) {
            try {
                result.put(x, value(x));
            } catch (CalculationException ex) {
                ex.printStackTrace();
            }
        }
        return result;
    }
    
}
