/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.plugins.skybackground.skybrightnesstable;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.Dataset.Type;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.cnrs.lam.dis.etc.datamodel.Site;
import org.cnrs.lam.dis.etc.plugins.PluginCommunicator;
import org.cnrs.lam.dis.etc.plugins.skybackground.skybrightnesstable.calculation.AtmosphericEfficiency;
import org.cnrs.lam.dis.etc.plugins.skybackground.skybrightnesstable.calculation.BlackBody;
import org.cnrs.lam.dis.etc.plugins.skybackground.skybrightnesstable.calculation.BoundedTemplate;
import org.cnrs.lam.dis.etc.plugins.skybackground.skybrightnesstable.calculation.CalculationException;
import org.cnrs.lam.dis.etc.plugins.skybackground.skybrightnesstable.calculation.ConstantFrequencyFlux;
import org.cnrs.lam.dis.etc.plugins.skybackground.skybrightnesstable.calculation.ContinuumFunction;
import org.cnrs.lam.dis.etc.plugins.skybackground.skybrightnesstable.calculation.DatasetFunction;
import org.cnrs.lam.dis.etc.plugins.skybackground.skybrightnesstable.calculation.EmissionLines;
import org.cnrs.lam.dis.etc.plugins.skybackground.skybrightnesstable.calculation.Multiplier;
import org.javatuples.Pair;

/**
 */
public class Calculator {
    
    private static final Logger logger = Logger.getLogger(Calculator.class);
    
    private List<DatasetInfo> filterList;
    private List<String> skyTypesList;
    private Map<DatasetInfo, Map<String, Double>> skyBrightnessTable;
    private PluginCommunicator communicator;
    private File emissionLinesFile;
    private int continuumResolution;
    private double continuumErrorAccuracy;
    private int continuumMaxIterations;
    private int continuumSmoothness;

    public Calculator(List<DatasetInfo> filterList, List<String> skyTypesList,
            Map<DatasetInfo, Map<String, Double>> skyBrightnessTable, PluginCommunicator communicator,
            File emissionLinesFile, int continuumResolution, double continuumErrorAccuracy,
            int continuumMaxIterations, int continuumSmoothness) {
        this.filterList = filterList;
        this.skyTypesList = skyTypesList;
        this.skyBrightnessTable = skyBrightnessTable;
        this.communicator = communicator;
        this.emissionLinesFile = emissionLinesFile;
        this.continuumResolution = continuumResolution;
        this.continuumErrorAccuracy = continuumErrorAccuracy;
        this.continuumMaxIterations = continuumMaxIterations;
        this.continuumSmoothness = continuumSmoothness;
    }
    
    public Map<String, BoundedTemplate> performCalculation(CalculationResults results) throws CalculationException {
        // The map where the generated templates will be stored
        Map<String, BoundedTemplate> generatedSkyTemplates = new HashMap<String, BoundedTemplate>();
        // We load the filters
        Map<DatasetInfo, DatasetFunction> filterMap = loadFilters(filterList, communicator, results);
        // We convert the magnitudes from the sky brightness table to total flux via the filters
        Map<DatasetInfo, Map<String, Double>> expectedTotalFluxMap = calculateExpectedTotalFlux(filterList, filterMap,
                skyTypesList, skyBrightnessTable, results);
        // We calculated the wavelength range that the filters cover
        Pair<Double, Double> lambdaRange = calculateLambdaRange(filterMap, results);
        // We load the emission lines
        EmissionLines emissionLines = loadEmissionLines(emissionLinesFile, results);
        // We calculate the total flux for each filter that is because of emission lines
        Map<DatasetInfo, Double> emissionLinesTotalFluxMap = calculateEmissionLinesTotalFlux(emissionLines, filterMap, results);
        // We find out which sky type will be considered as the no moon type, so
        // the continuum will be calculated from it
        String noMoonType = findNoMoonType(filterList, skyTypesList, skyBrightnessTable, results);
        // We calculate the continuum for the no moon sky type
        ContinuumFunction noMoonContinuum = calculateNoMoonContinuum(continuumResolution, continuumMaxIterations,
                continuumErrorAccuracy, continuumSmoothness, lambdaRange, filterList, filterMap, expectedTotalFluxMap, emissionLinesTotalFluxMap,
                noMoonType, results);
        // We create the sky template for the no moon type
        BoundedTemplate noMoonSkyTemplate = BoundedTemplate.convertFunctionToTemplate(noMoonContinuum).add(emissionLines);
        generatedSkyTemplates.put(noMoonType, noMoonSkyTemplate);
        if (results != null) {
            results.addResult(new CalculationResults.DoubleDatasetResult("Sky template for " + noMoonType, noMoonSkyTemplate.mapToPlot(),
                    "Angstrom", "erg/s/cm^2/Angstrom"), CalculationResults.Level.FINAL);
        }
        // We create the sun spectrum as we see it at ground (after the atmosphere)
        BoundedTemplate sunAfterAtmosphere = calculateSunSpectrum(lambdaRange, communicator, results);
        // We calculate the total flux of the sun per filter
        Map<DatasetInfo, Double> sunPerFilterTotalFlux = calculateSunPerFilterFlux(filterMap, sunAfterAtmosphere, results);
        // We go through all the sky types which contain some moon light
        Set<String> otherSkyTypes = new HashSet<String>(skyTypesList);
        otherSkyTypes.remove(noMoonType);
        for (String skyType : otherSkyTypes) {
            BoundedTemplate moonTempate = calculateMoonTemplate(skyType, noMoonType, expectedTotalFluxMap, sunPerFilterTotalFlux, sunAfterAtmosphere, results);
            Map<DatasetInfo, Double> moonPerFilterTotalFlux = calculateMoonPerFilterFlux(filterMap, moonTempate, results);
            ContinuumFunction moonContinuum = calculateMoonContinuum(continuumResolution, continuumMaxIterations, continuumErrorAccuracy, continuumSmoothness,
                    lambdaRange, filterList, filterMap, expectedTotalFluxMap, emissionLinesTotalFluxMap, moonPerFilterTotalFlux, skyType, results);
            // We create the sky template for the sky type
            BoundedTemplate skyTemplate = BoundedTemplate.convertFunctionToTemplate(noMoonContinuum).add(emissionLines).add(moonTempate)
                    .add(BoundedTemplate.convertFunctionToTemplate(moonContinuum));
            generatedSkyTemplates.put(skyType, skyTemplate);
            if (results != null) {
                results.addResult(new CalculationResults.DoubleDatasetResult("Sky template for " + skyType, skyTemplate.mapToPlot(),
                        "Angstrom", "erg/s/cm^2/Angstrom"), CalculationResults.Level.FINAL);
            }
        }
        return generatedSkyTemplates;
    }

    /**
     * Retrieves from the communicator the data for the given list of filters and
     * returns the Functions which can be used to retrieve their values. If the
     * results parameter is not null the filters are added in it.
     * @return
     * @throws CalculationException 
     */
    private Map<DatasetInfo, DatasetFunction> loadFilters(List<DatasetInfo> filterList,
            PluginCommunicator communicator, CalculationResults results) throws CalculationException {
        Map<DatasetInfo, DatasetFunction> filterMap = new HashMap<DatasetInfo, DatasetFunction>();
        for (DatasetInfo filterInfo : filterList) {
            Dataset filter = communicator.getDataset(Type.FILTER_TRANSMISSION, filterInfo, null);
            DatasetFunction function = new DatasetFunction(filter.getData());
            filterMap.put(filterInfo, function);
            if (results != null) {
                String resultName = "Filter " + filterInfo;
                results.addResult(new CalculationResults.DoubleDatasetResult(resultName, function.mapToPlot(),
                        "Angstrom", ""), CalculationResults.Level.INTERMEDIATE_IMPORTANT);
            }
        }
        return filterMap;
    }

    /**
     * Returns the minimum and maximum lambda values for all the given filters.
     * @param filterMap
     * @return 
     */
    private Pair<Double, Double> calculateLambdaRange(Map<DatasetInfo, DatasetFunction> filterMap,
            CalculationResults results) {
        Double min = null;
        Double max = null;
        for (DatasetFunction function : filterMap.values()) {
            min = (min == null || function.getLowerBound() < min) ? function.getLowerBound() : min;
            max = (max == null || function.getUpperBound() > max) ? function.getUpperBound() : max;
        }
        if (results != null) {
            results.addResult(new CalculationResults.StringResult("Lambda range", "[" + min + " , " + max + "]"),
                    CalculationResults.Level.INTERMEDIATE_IMPORTANT);
        }
        return new Pair<Double, Double>(min, max);
    }

    /**
     * Loads the emission lines from the file or sets them to no emission lines
     * if there is no file given
     * @param emissionLinesFile
     * @param results
     * @return 
     */
    private EmissionLines loadEmissionLines(File emissionLinesFile, CalculationResults results)
            throws CalculationException {
        EmissionLines emissionLines = null;
        if (!emissionLinesFile.getName().trim().isEmpty()) {
            try {
                emissionLines = EmissionLines.createFromFile(emissionLinesFile);
                if (results != null) {
                    results.addResult(new CalculationResults.DoubleDatasetResult("Emission lines",
                            emissionLines.mapToPlot(), "Angstrom", "erg/s/cm^2/Angstrom/arcsec^2"),
                            CalculationResults.Level.INTERMEDIATE_IMPORTANT);
                }
            } catch (IOException ex) {
                logger.error("There was an error with the emission lines file", ex);
                throw new CalculationException("There is an error with the emission lines file.\n" + ex.getMessage());
            }
        } else {
            emissionLines = new EmissionLines(new HashMap<Integer, Double>(), 0.1);
            results.addResult(new CalculationResults.StringResult("Enission lines", "No file given"),
                    CalculationResults.Level.INTERMEDIATE_IMPORTANT);
        }
        return emissionLines;
    }

    private String findNoMoonType(List<DatasetInfo> filterList, List<String> skyTypesList,
            Map<DatasetInfo, Map<String, Double>> skyBrightnessTable, CalculationResults results) {
        String noMoonType = null;
        double maxMagSumFound = Double.NEGATIVE_INFINITY;
        for (String type : skyTypesList) {
            double magSum = 0;
            for (DatasetInfo filter : filterList) {
                magSum += skyBrightnessTable.get(filter).get(type);
            }
            if (magSum > maxMagSumFound) {
                noMoonType = type;
                maxMagSumFound = magSum;
            }
        }
        if (results != null) {
            results.addResult(new CalculationResults.StringResult("No moon type", noMoonType),
                    CalculationResults.Level.INTERMEDIATE_IMPORTANT);
        }
        return noMoonType;
    }

    /**
     * Calculates the total flux of the emission lines for each filter.
     * @param emissionLines
     * @param filterMap
     * @param results
     * @return 
     */
    private Map<DatasetInfo, Double> calculateEmissionLinesTotalFlux(EmissionLines emissionLines,
            Map<DatasetInfo, DatasetFunction> filterMap, CalculationResults results) throws CalculationException {
        Map<DatasetInfo, Double> emissionLinesTotalFluxMap = new HashMap<DatasetInfo, Double>();
        for (DatasetInfo filter : filterMap.keySet()) {
            DatasetFunction filterFunction = filterMap.get(filter);
            BoundedTemplate emissionLinesThroughFilter = (BoundedTemplate) Multiplier.multiply(emissionLines, filterFunction);
            if (results != null) {
                results.addResult(new CalculationResults.DoubleDatasetResult("Emission lines through filter " + filter,
                        emissionLinesThroughFilter.mapToPlot(), "Angstrom", "erg/s/cm^2/Angstrom"),
                        CalculationResults.Level.INTERMEDIATE_IMPORTANT);
            }
            double totalEmissionLinesFlux = emissionLinesThroughFilter.integral(emissionLinesThroughFilter.getLowerBound(),
                    emissionLinesThroughFilter.getUpperBound());
            emissionLinesTotalFluxMap.put(filter, totalEmissionLinesFlux);
            if (results != null) {
                results.addResult(new CalculationResults.DoubleValueResult("Total emission lines flux for filter " + filter,
                        totalEmissionLinesFlux, "erg/s/cm^2"), CalculationResults.Level.INTERMEDIATE_IMPORTANT);
            }
        }
        return emissionLinesTotalFluxMap;
    }

    /**
     * Calculates the expected total flux for each filter and each sky type, based
     * on the given sky brightness table.
     * @param filterList
     * @param skyTypesList
     * @param skyBrightnessTable
     * @param results
     * @return 
     */
    private Map<DatasetInfo, Map<String, Double>> calculateExpectedTotalFlux(List<DatasetInfo> filterList,
            Map<DatasetInfo, DatasetFunction> filterMap, List<String> skyTypesList,
            Map<DatasetInfo, Map<String, Double>> skyBrightnessTable, CalculationResults results) throws CalculationException {
        Map<DatasetInfo, Map<String, Double>> expectedTotalFluxMap = new HashMap<DatasetInfo, Map<String, Double>>();
        for (DatasetInfo filter : filterList) {
            DatasetFunction filterFunction = filterMap.get(filter);
            Map<String, Double> expectedTotalFluxForSkyTypeMap = new HashMap<String, Double>();
            for (String skyType : skyTypesList) {
                double expectedAbMagnitude = skyBrightnessTable.get(filter).get(skyType);
                ConstantFrequencyFlux fluxFunction = ConstantFrequencyFlux.getForABmagnitude(expectedAbMagnitude);
                BoundedTemplate fluxFunctionAfterFilter = (BoundedTemplate) Multiplier.multiply(
                        BoundedTemplate.convertFunctionToTemplate(filterFunction), fluxFunction);
                double expectedTotalFlux = fluxFunctionAfterFilter.integral(
                        fluxFunctionAfterFilter.getLowerBound(), fluxFunctionAfterFilter.getUpperBound());
                expectedTotalFluxForSkyTypeMap.put(skyType, expectedTotalFlux);
                if (results != null) {
                    results.addResult(new CalculationResults.DoubleValueResult("Expected magnitude for " + filter + " and sky " + skyType,
                            expectedAbMagnitude, ""), CalculationResults.Level.INTERMEDIATE_IMPORTANT);
                    results.addResult(new CalculationResults.DoubleDatasetResult("Expected " + skyType + " flux for filter " + filter + " (before filter)", 
                            fluxFunction.mapToPlot(filterFunction.getLowerBound(), filterFunction.getUpperBound()), "Angstrom", "erg/s/cm^2/Angstrom"),
                            CalculationResults.Level.INTERMEDIATE_IMPORTANT);
                    results.addResult(new CalculationResults.DoubleDatasetResult("Expected " + skyType + " flux for filter " + filter + " (after filter)",
                            fluxFunctionAfterFilter.mapToPlot(), "Angstrom", "erg/s/cm^2/Angstrom"), CalculationResults.Level.INTERMEDIATE_IMPORTANT);
                    results.addResult(new CalculationResults.DoubleValueResult("Expected " + skyType + " total flux for filter " + filter, expectedTotalFlux,
                            "erg/s/cm^2"), CalculationResults.Level.INTERMEDIATE_IMPORTANT);
                }
            }
            expectedTotalFluxMap.put(filter, expectedTotalFluxForSkyTypeMap);
        }
        return expectedTotalFluxMap;
    }
    
    /**
     * Calculates the continuum for the no moon sky type. This continuum plus the
     * emission lines should give us the total expected flux.
     * @param resolution
     * @param maxIterations
     * @param requiredAccuracy
     * @param lambdaRange
     * @param filterList
     * @param filterMap
     * @param expectedTotalFluxMap
     * @param emissionLinesTotalFluxMap
     * @param noMoonType
     * @param results
     * @return
     * @throws CalculationException 
     */
    private ContinuumFunction calculateNoMoonContinuum(int resolution, int maxIterations, double requiredAccuracy, int smoothness, Pair<Double, Double> lambdaRange,
            List<DatasetInfo> filterList, Map<DatasetInfo, DatasetFunction> filterMap, Map<DatasetInfo, Map<String, Double>> expectedTotalFluxMap,
            Map<DatasetInfo, Double> emissionLinesTotalFluxMap, String noMoonType, CalculationResults results) throws CalculationException {
        Map<DatasetInfo, Double> expectedContinuumFluxMap = new HashMap<DatasetInfo, Double>();
        for (DatasetInfo filter: filterList) {
            double expectedTotalFlux = expectedTotalFluxMap.get(filter).get(noMoonType);
            double emissionLinesFlux = emissionLinesTotalFluxMap.get(filter);
            double expectedContinuumFlux = expectedTotalFlux - emissionLinesFlux;
            expectedContinuumFluxMap.put(filter, expectedContinuumFlux);
        }
        return calculateContinuum(resolution, maxIterations, requiredAccuracy, smoothness, lambdaRange, filterList, filterMap, expectedContinuumFluxMap, noMoonType, results);
    }
    
    private ContinuumFunction calculateMoonContinuum(int resolution, int maxIterations, double requiredAccuracy, int smoothness, Pair<Double, Double> lambdaRange,
            List<DatasetInfo> filterList, Map<DatasetInfo, DatasetFunction> filterMap, Map<DatasetInfo, Map<String, Double>> expectedTotalFluxMap,
            Map<DatasetInfo, Double> emissionLinesTotalFluxMap, Map<DatasetInfo, Double> moonTotalFluxMap, String skyType, CalculationResults results) throws CalculationException {
        Map<DatasetInfo, Double> expectedContinuumFluxMap = new HashMap<DatasetInfo, Double>();
        for (DatasetInfo filter: filterList) {
            double expectedTotalFlux = expectedTotalFluxMap.get(filter).get(skyType);
            double emissionLinesFlux = emissionLinesTotalFluxMap.get(filter);
            double moonFlux = moonTotalFluxMap.get(filter);
            double expectedContinuumFlux = expectedTotalFlux - emissionLinesFlux - moonFlux;
            expectedContinuumFluxMap.put(filter, expectedContinuumFlux);
        }
        return calculateContinuum(resolution, maxIterations, requiredAccuracy, smoothness, lambdaRange, filterList, filterMap, expectedContinuumFluxMap, skyType, results);
    }
    
    /**
     * Calculates the continuum for simulating the expected continuum flux given.
     * @param resolution
     * @param maxIterations
     * @param requiredAccuracy
     * @param lambdaRange
     * @param filterList
     * @param filterMap
     * @param expectedContinuumFlux
     * @param description
     * @param results
     * @return
     * @throws CalculationException 
     */
    private ContinuumFunction calculateContinuum(int resolution, int maxIterations, double requiredAccuracy, int smoothness, Pair<Double, Double> lambdaRange,
            List<DatasetInfo> filterList, Map<DatasetInfo, DatasetFunction> filterMap, Map<DatasetInfo, Double> expectedContinuumFluxMap,
            String description, CalculationResults results) throws CalculationException {
        int continuumMinLambda = (int) Math.floor(lambdaRange.getValue0() / resolution);
        continuumMinLambda = continuumMinLambda * resolution;
        int continuumMaxLambda = (int) Math.ceil(lambdaRange.getValue1() / resolution);
        continuumMaxLambda = continuumMaxLambda * resolution;
        // Initialize the continuum to zeroes
        Map<Integer, Double> continuumValues = new HashMap<Integer, Double>();
        for (int i = continuumMinLambda; i <= continuumMaxLambda; i += resolution) {
            if (lambdaInFilters(i, filterMap))
                continuumValues.put(i, 0.);
        }
        // Repeat the iterations until we have the correct accuracy
        boolean continuumCalculated = false;
        int iterationNo = 0;
        while (!continuumCalculated && iterationNo < maxIterations) {
            iterationNo++;
            ContinuumFunction continuumFunction = new ContinuumFunction(continuumValues);
            Map<DatasetInfo, Double> filterCorrectionsMap = new HashMap<DatasetInfo, Double>();
            Map<DatasetInfo, Double> filterMeanEfficiencyMap = calculateMeanEfficiencyForFilters(filterMap, results);
            for (DatasetInfo filter : filterList) {
                // Calculate the total flux of the continuum for this filter
                DatasetFunction filterFunction = filterMap.get(filter);
                BoundedTemplate filteredCountinuum = (BoundedTemplate) Multiplier.multiply(BoundedTemplate.convertFunctionToTemplate(filterFunction), continuumFunction);
                double totalContinuumFlux = filteredCountinuum.integral(filteredCountinuum.getLowerBound(), filteredCountinuum.getUpperBound());
                // now calculate the correction of the flux we need to do
                double expectedTotalFlux = expectedContinuumFluxMap.get(filter);
                double filterMeanEfficiency = filterMeanEfficiencyMap.get(filter);
                double totalCorrection = (expectedTotalFlux - totalContinuumFlux) / filterMeanEfficiency;
                filterCorrectionsMap.put(filter, totalCorrection);
            }
            // Calculate the average corrections, so we will not overestimate the
            // corrections at the places the filters overlap
            Map<Integer, Double> correctionsMap = new HashMap<Integer, Double>();
            for (DatasetInfo filter : filterList) {
                DatasetFunction filterFlunction = filterMap.get(filter);
                int minLambda = (int) Math.floor(filterFlunction.getLowerBound());
                int maxLambda = (int) Math.ceil(filterFlunction.getUpperBound());
                double perAngstromCorrection = filterCorrectionsMap.get(filter) / (maxLambda - minLambda);
                for (Integer i : continuumValues.keySet()) {
                    if (i < minLambda || i > maxLambda) {
                        continue;
                    }
                    double averagedCorrection = (correctionsMap.get(i) == null)
                            ? perAngstromCorrection
                            : (correctionsMap.get(i) + perAngstromCorrection) / 2;
                    correctionsMap.put(i, averagedCorrection);
                }
            }
            // Apply the corrections
            for (Map.Entry<Integer, Double> entry : correctionsMap.entrySet()) {
                Integer key = entry.getKey();
                Double correction = entry.getValue();
                double oldValue = (continuumValues.get(key) == null) ? 0 : continuumValues.get(key);
                double newValue = oldValue + correction;
                newValue = (newValue < 0) ? 0 : newValue;
                continuumValues.put(key, newValue);
            }
            // Smoothen a little bit the contiuum
            int kernelRadius = smoothness + 1;
            for (Integer key : continuumValues.keySet()) {
                double total = continuumValues.get(key) * kernelRadius;
                double devider = kernelRadius;
                for (int i = 1; i < kernelRadius; i++) {
                    Double value = continuumValues.get(key + i * resolution);
                    if (value != null) {
                        total += value * (kernelRadius - i);
                        devider += (kernelRadius - i);
                    }
                    value = continuumValues.get(key - i * resolution);
                    if (value != null) {
                        total += value * (kernelRadius - i);
                        devider += (kernelRadius - i);
                    }
                }
                continuumValues.put(key, total / devider);
            }
            // Now check if the continuum integral is good enough
            continuumCalculated = true;
            ContinuumFunction continuumFunctionAfterCorrection = new ContinuumFunction(continuumValues);
            for (DatasetInfo filter : filterList) {
                // Calculate the total flux of the continuum for this filter
                DatasetFunction filterFunction = filterMap.get(filter);
                BoundedTemplate filteredCountinuum = (BoundedTemplate) Multiplier.multiply(
                        BoundedTemplate.convertFunctionToTemplate(filterFunction), continuumFunctionAfterCorrection);
                double totalContinuumFlux = filteredCountinuum.integral(filteredCountinuum.getLowerBound(), filteredCountinuum.getUpperBound());
                // now check the error from the expected flux
                double expectedTotalFlux = expectedContinuumFluxMap.get(filter);
                double reachedAccuracy = Math.abs((expectedTotalFlux - totalContinuumFlux) / expectedTotalFlux);
                if (reachedAccuracy > requiredAccuracy) {
                    continuumCalculated = false;
                    break;
                }
            }
        }
        if (!continuumCalculated) {
            logger.error("Maximum iteration number reached when calculating continuum " + description);
            throw new CalculationException(description + " continuum calculation exceeded the maximun iterations number");
        }
        ContinuumFunction continuum = new ContinuumFunction(continuumValues);
        if (results != null) {
            results.addResult(new CalculationResults.LongValueResult("Continuum for " + description + " calculation iterations number", iterationNo, ""),
                    CalculationResults.Level.INTERMEDIATE_IMPORTANT);
            results.addResult(new CalculationResults.DoubleDatasetResult("Continuum for " + description, continuum.mapToPlot(),
                    "Angstrom", "erg/s/cm^2/Angstrom"), CalculationResults.Level.INTERMEDIATE_IMPORTANT);
        }
        return continuum;
    }

    private boolean lambdaInFilters(int i, Map<DatasetInfo, DatasetFunction> filterMap) {
        for (DatasetFunction filter : filterMap.values()) {
            if (i >= filter.getLowerBound() || i <= filter.getUpperBound())
                return true;
        }
        return false;
    }
    
    private Map<DatasetInfo, Double> calculateMeanEfficiencyForFilters(Map<DatasetInfo, DatasetFunction> filterMap, CalculationResults results) throws CalculationException {
        Map<DatasetInfo, Double> meanEfficiencyMap = new HashMap<DatasetInfo, Double>();
        for (Map.Entry<DatasetInfo, DatasetFunction> entry : filterMap.entrySet()) {
            DatasetInfo filter = entry.getKey();
            DatasetFunction filterFunction = entry.getValue();
            double integral = filterFunction.integral(filterFunction.getLowerBound(), filterFunction.getUpperBound());
            double range = filterFunction.getUpperBound() - filterFunction.getLowerBound();
            double meanEfficiency = integral / range;
            if (results != null) {
                results.addResult(new CalculationResults.DoubleValueResult("Filter " + filter + " mean efficiency", meanEfficiency, "Angstrom"),
                        CalculationResults.Level.INTERMEDIATE_IMPORTANT);
            }
            meanEfficiencyMap.put(filter, meanEfficiency);
        }
        return meanEfficiencyMap;
    }

    private Map<DatasetInfo, Double> calculateMeanLambdaForFilters(Map<DatasetInfo, DatasetFunction> filterMap, CalculationResults results) throws CalculationException {
        Map<DatasetInfo, Double> meanLambdaMap = new HashMap<DatasetInfo, Double>();
        for (Map.Entry<DatasetInfo, DatasetFunction> entry : filterMap.entrySet()) {
            DatasetInfo filter = entry.getKey();
            DatasetFunction filterFunction = entry.getValue();
            double totalWavelength = 0;
            double totalFactor = 0;
            for (double i = filterFunction.getLowerBound(); i < filterFunction.getUpperBound(); i++) { 
                double factor = filterFunction.value(i);
                totalWavelength += i * factor;
                totalFactor += factor;
            }
            double meanLambda = totalWavelength / totalFactor;
            meanLambdaMap.put(filter, meanLambda);
            if (results != null) {
                results.addResult(new CalculationResults.DoubleValueResult("Filter " + filter + " mean wavelength", meanLambda, "Angstrom"),
                        CalculationResults.Level.INTERMEDIATE_IMPORTANT);
            }
        }
        return meanLambdaMap;
    }

    private AtmosphericEfficiency createAtmosphericEfficiency(Pair<Double, Double> lambdaRange, PluginCommunicator communicator,
            CalculationResults results) throws CalculationException {
        Site site = communicator.getCurrentSession().getSite();
        Dataset absorption = communicator.getDataset(Type.SKY_ABSORPTION, site.getSkyAbsorption(), null);
        DatasetFunction absorptionFunction = new DatasetFunction(absorption.getData());
        Dataset extinction = communicator.getDataset(Type.SKY_EXTINCTION, site.getSkyExtinction(), null);
        DatasetFunction extinctionFunction = new DatasetFunction(extinction.getData());
        AtmosphericEfficiency efficiency = new AtmosphericEfficiency(absorptionFunction, extinctionFunction);
        if (efficiency.getLowerBound() > lambdaRange.getValue0() || efficiency.getUpperBound() < lambdaRange.getValue1()) {
            throw new CalculationException("Atmospheric efficiency range too small");
        }
        if (results != null) {
            results.addResult(new CalculationResults.DoubleDatasetResult("Atmospheric absorption", absorptionFunction.mapToPlot(), "Angstrom", ""),
                    CalculationResults.Level.INTERMEDIATE_IMPORTANT);
            results.addResult(new CalculationResults.DoubleDatasetResult("Atmospheric extinction", extinctionFunction.mapToPlot(), "Angstrom", ""),
                    CalculationResults.Level.INTERMEDIATE_IMPORTANT);
        }
        return efficiency;
    }

    private BoundedTemplate calculateSunSpectrum(Pair<Double, Double> lambdaRange, PluginCommunicator communicator,
            CalculationResults results) throws CalculationException {
        // We create the atmospheric efficiency
        AtmosphericEfficiency atmosphericEfficiency = createAtmosphericEfficiency(lambdaRange, communicator, results);
        // Create the black body to simulate the sun
        BlackBody sun = new BlackBody(5777);
        // Pass the sky through the atmosphere
        BoundedTemplate sunAfterAtmosphere = (BoundedTemplate) Multiplier.multiply(BoundedTemplate.convertFunctionToTemplate(atmosphericEfficiency), sun);
        if (results != null) {
            results.addResult(new CalculationResults.DoubleDatasetResult("Atmospheric efficiency", atmosphericEfficiency.mapToPlot(), "Angstrom", ""),
                    CalculationResults.Level.INTERMEDIATE_IMPORTANT);
            results.addResult(new CalculationResults.DoubleDatasetResult("Sun before atmosphere", sun.mapToPlot(sunAfterAtmosphere.getLowerBound(), sunAfterAtmosphere.getUpperBound()),
                    "Angstrom", "erg/s/cm^2/Angstrom"), CalculationResults.Level.INTERMEDIATE_IMPORTANT);
            results.addResult(new CalculationResults.DoubleDatasetResult("Sun after atmosphere", sunAfterAtmosphere.mapToPlot(), "Angstrom", "erg/s/cm^2/Angstrom"),
                    CalculationResults.Level.INTERMEDIATE_IMPORTANT);
        }
        return sunAfterAtmosphere;
    }

    private Map<DatasetInfo, Double> calculateSunPerFilterFlux(Map<DatasetInfo, DatasetFunction> filterMap, 
            BoundedTemplate sunAfterAtmosphere, CalculationResults results) throws CalculationException {
        Map<DatasetInfo, Double> sunTotalFluxMap = new HashMap<DatasetInfo, Double>();
        for (DatasetInfo filter : filterMap.keySet()) {
            DatasetFunction filterFunction = filterMap.get(filter);
            BoundedTemplate sunThroughFilter = (BoundedTemplate) Multiplier.multiply(sunAfterAtmosphere, filterFunction);
            if (results != null) {
                results.addResult(new CalculationResults.DoubleDatasetResult("Sun spectrum through filter " + filter,
                        sunThroughFilter.mapToPlot(), "Angstrom", "erg/s/cm^2/Angstrom"),
                        CalculationResults.Level.INTERMEDIATE_IMPORTANT);
            }
            double totalSunFlux = sunThroughFilter.integral(sunThroughFilter.getLowerBound(),
                    sunThroughFilter.getUpperBound());
            sunTotalFluxMap.put(filter, totalSunFlux);
            if (results != null) {
                results.addResult(new CalculationResults.DoubleValueResult("Total sun flux for filter " + filter,
                        totalSunFlux, "erg/s/cm^2"), CalculationResults.Level.INTERMEDIATE_IMPORTANT);
            }
        }
        return sunTotalFluxMap;
    }

    private BoundedTemplate calculateMoonTemplate(String skyType, String noMoonType, Map<DatasetInfo, Map<String, Double>> expectedTotalFluxMap, 
            Map<DatasetInfo, Double> sunPerFilterTotalFlux, BoundedTemplate sunAfterAtmosphere, CalculationResults results) {
        double factor = Double.POSITIVE_INFINITY;
        for (DatasetInfo filter : expectedTotalFluxMap.keySet()) {
            double expectedFlux = expectedTotalFluxMap.get(filter).get(skyType);
            double noMoonFlux = expectedTotalFluxMap.get(filter).get(noMoonType);
            double moonFlux = expectedFlux - noMoonFlux;
            double sunFlux = sunPerFilterTotalFlux.get(filter);
            double filterFactor = moonFlux / sunFlux;
            if (filterFactor < factor) {
                factor = filterFactor;
            }
        }
        BoundedTemplate moonTemplate = new BoundedTemplate(sunAfterAtmosphere, factor);
        if (results != null) {
            results.addResult(new CalculationResults.DoubleValueResult("Moon scale for sky type " + skyType, factor, ""),
                    CalculationResults.Level.INTERMEDIATE_IMPORTANT);
            results.addResult(new CalculationResults.DoubleDatasetResult("Moon template for " + skyType, moonTemplate.mapToPlot(),
                    "Angstrom", "erg/s/cm^2/Angstrom"), CalculationResults.Level.INTERMEDIATE_IMPORTANT);
        }
        return moonTemplate;
    }

    private Map<DatasetInfo, Double> calculateMoonPerFilterFlux(Map<DatasetInfo, DatasetFunction> filterMap, BoundedTemplate moonTempate,
            CalculationResults results) throws CalculationException {
        Map<DatasetInfo, Double> moonTotalFluxMap = new HashMap<DatasetInfo, Double>();
        for (DatasetInfo filter : filterMap.keySet()) {
            DatasetFunction filterFunction = filterMap.get(filter);
            BoundedTemplate moonThroughFilter = (BoundedTemplate) Multiplier.multiply(moonTempate, filterFunction);
            if (results != null) {
                results.addResult(new CalculationResults.DoubleDatasetResult("Moon spectrum through filter " + filter,
                        moonThroughFilter.mapToPlot(), "Angstrom", "erg/s/cm^2/Angstrom"),
                        CalculationResults.Level.INTERMEDIATE_IMPORTANT);
            }
            double totalMoonFlux = moonThroughFilter.integral(moonThroughFilter.getLowerBound(),
                    moonThroughFilter.getUpperBound());
            moonTotalFluxMap.put(filter, totalMoonFlux);
            if (results != null) {
                results.addResult(new CalculationResults.DoubleValueResult("Total moon flux for filter " + filter,
                        totalMoonFlux, "erg/s/cm^2"), CalculationResults.Level.INTERMEDIATE_IMPORTANT);
            }
        }
        return moonTotalFluxMap;
    }
    
}
