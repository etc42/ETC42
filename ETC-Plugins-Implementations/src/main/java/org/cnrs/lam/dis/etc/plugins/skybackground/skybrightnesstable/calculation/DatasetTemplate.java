/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.plugins.skybackground.skybrightnesstable.calculation;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import org.cnrs.lam.dis.etc.configuration.ConfigFactory;

/**
 */
public class DatasetTemplate extends BoundedTemplate {

    private DatasetTemplate(double lowerBound, double upperBound, Map<Integer, Double> data, double resolution) {
        super(lowerBound, upperBound, data, resolution);
    }
    
    public static DatasetTemplate createFromDataset(Map<Double, Double> data) {
        Map<Integer, Double> newData = new HashMap<Integer, Double>();
        // First we get the keys and the values of the input data in arrays, to
        // make the implementation of the logic easier
        Map<Double, Double> oldData = new TreeMap<Double, Double>(data);
        Double[] oldKeys = oldData.keySet().toArray(new Double[] {});
        Double[] oldValues = oldData.values().toArray(new Double[] {});
        double minKey = oldKeys[0] - (oldKeys[1] - oldKeys[0]) / 2;
        double maxKey = oldKeys[oldKeys.length - 1] + (oldKeys[oldKeys.length - 1] - oldKeys[oldKeys.length - 2]) / 2;
        double resolution = ConfigFactory.getConfig().getSpectrumResolution();
        minKey = Math.floor(minKey / resolution);
        maxKey = Math.ceil(maxKey / resolution);
        // Go through the old values and create the new ones
        double oldRangeMax = 0;
        for (int i = 0; i < oldKeys.length; i++) {
            // Calculate the range this value represents
            double rangeMin = (i == 0) ? minKey : Math.ceil((oldKeys[i - 1] + oldKeys[i]) / 2 / resolution);
            rangeMin = Math.floor(Math.min(rangeMin, oldKeys[i] / resolution));
            rangeMin = (oldRangeMax == rangeMin) ? rangeMin + 1 : rangeMin;
            double rangeMax = (i == oldKeys.length - 1) ? maxKey : Math.floor((oldKeys[i] + oldKeys[i + 1]) / 2 / resolution);
            rangeMax = Math.ceil(Math.max(rangeMax, oldKeys[i] / resolution));
            oldRangeMax = rangeMax;
            // Calculate the value the elements in the range should have
            double average = oldValues[i] / (rangeMax - rangeMin + 1);
            // Add the average to the new data
            for (int j = (int) rangeMin; j <= (int) rangeMax; j++) {
                Double oldValue = newData.get(j);
                double newValue = (oldValue == null) ? 0 : oldValue;
                newValue += average;
                newData.put(j, newValue);
            }
        }
        return new DatasetTemplate(minKey * resolution, maxKey * resolution, newData, resolution);
    }
    
}
