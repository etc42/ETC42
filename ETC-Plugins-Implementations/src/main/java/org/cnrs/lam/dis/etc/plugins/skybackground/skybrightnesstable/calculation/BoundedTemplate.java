/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.plugins.skybackground.skybrightnesstable.calculation;

import java.util.Map;
import java.util.TreeMap;
import org.cnrs.lam.dis.etc.configuration.ConfigFactory;

/**
 */
public class BoundedTemplate extends Template implements BoundedFunction {
    
    private double lowerBound;
    private double upperBound;

    public BoundedTemplate(double lowerBound, double upperBound, Map<Integer, Double> data, double resolution) {
        super(data, resolution);
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
    }

    public BoundedTemplate(double lowerBound, double upperBound, Template other) {
        super(other);
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
    }
    
    public BoundedTemplate(double lowerBound, double upperBound, Template other, double multiplier) {
        super(other, multiplier);
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
    }
    
    public BoundedTemplate(BoundedTemplate other, double multiplier) {
        super(other, multiplier);
        this.lowerBound = other.getLowerBound();
        this.upperBound = other.getUpperBound();
    }
    
    public static BoundedTemplate convertFunctionToTemplate(BoundedFunction function) throws CalculationException {
        Map<Integer, Double> functionData = new TreeMap<Integer, Double>();
        double resolution = ConfigFactory.getConfig().getSpectrumResolution();
        int minKey = (int) Math.ceil(function.getLowerBound() / resolution);
        int maxKey = (int) Math.floor(function.getUpperBound() / resolution);
        for (int x = minKey; x <= maxKey; x++) {
            functionData.put(x, function.value(x * resolution) * resolution);
        }
        return new BoundedTemplate(minKey * resolution, maxKey * resolution, functionData, resolution);
    }

//    private static Map<Double, Double> createMapFromFunction(Function function) throws CalculationException {
//        Map<Double, Double> functionData = new TreeMap<Double, Double>();
//        double functionRes = function.getResolution() / 2;
//        for (double x = function.getMinKey(); x <= function.getMaxKey(); x += functionRes) {
//            functionData.put(x, function.value(x) * functionRes);
//        }
//        return functionData;
//    }

    @Override
    public double getLowerBound() {
        return lowerBound;
    }

    @Override
    public double getUpperBound() {
        return upperBound;
    }

    @Override
    public double integral(double x1, double x2) throws CalculationException {
        if (x1 < lowerBound || x2 > upperBound) {
            throw new CalculationException("Integration limits [" + x1 + "," + x2 + "] is out of range ([" + lowerBound + "," + upperBound + "])");
        }
        return super.integral(x1, x2);
    }

    @Override
    public double value(double x) throws CalculationException {
        if (x < lowerBound || x > upperBound) {
            throw new CalculationException("Requested X " + x + " is out of range ([" + lowerBound + "," + upperBound + "])");
        }
        return super.value(x);
    }

    @Override
    BoundedTemplate multiply(Function other) throws CalculationException, UnsupportedMultiplicationType {
        Template unboundedMultiplication = (Template) super.multiply(other);
        double newLowerBound = this.lowerBound;
        double newUpperBound = this.upperBound;
        if (other instanceof BoundedFunction) {
            BoundedFunction otherLimited = (BoundedFunction) other;
            newLowerBound = (newLowerBound > otherLimited.getLowerBound())
                    ? newLowerBound : otherLimited.getLowerBound();
            newUpperBound = (newUpperBound < otherLimited.getUpperBound())
                    ? newUpperBound : otherLimited.getUpperBound();
        }
        return new BoundedTemplate(newLowerBound, newUpperBound, unboundedMultiplication);
    }

    @Override
    public BoundedTemplate add(Template other) throws CalculationException {
        Template unboundedAddition = super.add(other);
        double newLowerBound = this.lowerBound;
        double newUpperBound = this.upperBound;
        if (other instanceof BoundedFunction) {
            BoundedFunction otherLimited = (BoundedFunction) other;
            newLowerBound = (newLowerBound > otherLimited.getLowerBound())
                    ? newLowerBound : otherLimited.getLowerBound();
            newUpperBound = (newUpperBound < otherLimited.getUpperBound())
                    ? newUpperBound : otherLimited.getUpperBound();
        }
        return new BoundedTemplate(newLowerBound, newUpperBound, unboundedAddition);
    }
    
}
