/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.plugins.skybackground.skybrightnesstable.calculation;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 */
public class Template extends MultipliableFunction implements IntegrableFunction {
    
    private Map<Integer, Double> data;
    private double resolution;

    public Template(Map<Integer, Double> data, double resolution) {
        this.data = new HashMap<Integer, Double>(data);
        this.resolution = resolution;
    }
    
    public Template(Template other) {
        this.data = new HashMap<Integer, Double>(other.data);
        this.resolution = other.resolution;
    }
    
    public Template(Template other, double multiplier) {
        this.data = new HashMap<Integer, Double>();
        for (Map.Entry<Integer, Double> entry : other.data.entrySet()) {
            this.data.put(entry.getKey(), entry.getValue() * multiplier);
        }
        this.resolution = other.resolution;
    }

    @Override
    public double value(double x) throws CalculationException {
        double scaledX = x / resolution;
        int xLow = (int) Math.floor(scaledX);
        int xHi = (int) Math.ceil(scaledX);
        double value = 0;
        if (xLow == xHi) {
            value = getValueFromMap(xLow);
        } else {
            value = getValueFromMap(xLow) * (xHi - scaledX) / (xHi - xLow);
            value += getValueFromMap(xHi) * (scaledX - xLow) / (xHi - xLow);
        }
        return value / resolution;
    }
    
    private double getValueFromMap(Integer x) {
        Double value = data.get(x);
        return (value == null) ? 0 : value;
    }

    @Override
    public double integral(double x1, double x2) throws CalculationException {
        if (x1 > x2) {
            throw new CalculationException("X1 must be smaller than X2");
        }
        if (x1 == x2) {
            return 0;
        }
        double scaledX1 = x1 / resolution;
        double scaledX2 = x2 / resolution;
        int lowX = (int) Math.floor(scaledX1);
        int hiX = (int) Math.ceil(scaledX2);
        double result = 0;
        for (int i = lowX; i <= hiX; i++) {
            result += getValueFromMap(i);
        }
        result -= getValueFromMap(lowX) * (scaledX1 - lowX) * resolution;
        result -= getValueFromMap(hiX) * (hiX - scaledX2) * resolution;
        return result;
    }
    
    public Template add(Template other) throws CalculationException {
        if (this.resolution != other.resolution) {
            throw new CalculationException("Addition of templates with different resolution is not yet supported");
        }
        Map<Integer, Double> newData = new TreeMap<Integer, Double>();
        Set<Integer> newKeys = new HashSet<Integer>();
        newKeys.addAll(Collections.unmodifiableSet(this.data.keySet()));
        newKeys.addAll(Collections.unmodifiableSet(other.data.keySet()));
        for (Integer key : newKeys) {
            double thisValue = (this.data.get(key) == null) ? 0 : this.data.get(key);
            double otherValue = (other.data.get(key) == null) ? 0 : other.data.get(key);
            double newValue = thisValue + otherValue;
            newData.put(key, newValue);
        }
        return new Template(newData, this.resolution);
    }
    
    private Template multiplyWithTemplate(Template other) throws CalculationException {
        if (this.resolution != other.resolution) {
            throw new CalculationException("Multiplication of templates with different resolution is not yet supported");
        }
        Map<Integer, Double> newData = new TreeMap<Integer, Double>();
        Set<Integer> newKeys = new HashSet<Integer>();
        newKeys.addAll(Collections.unmodifiableSet(this.data.keySet()));
        newKeys.retainAll(Collections.unmodifiableSet(other.data.keySet()));
        for (Integer key : newKeys) {
            double newValue = this.data.get(key) * other.data.get(key);
            newData.put(key, newValue);
        }
        return new Template(newData, this.resolution);
    }

    private Template multiplyWithBoundedIntegrable(Function other) throws CalculationException {
        Map<Integer, Double> newData = new TreeMap<Integer, Double>();
        IntegrableFunction otherIntegrable = (IntegrableFunction) other;
        BoundedFunction otherBounded = (BoundedFunction) other;
        double lowerBound = otherBounded.getLowerBound();
        double upperBound = otherBounded.getUpperBound();
        for (Integer key : this.data.keySet()) {
            double x = key * resolution;
            // If we are out of the range of the other function we skip the key
            if (x - (resolution / 2) < lowerBound || x + (resolution / 2) > upperBound) {
                continue;
            }
            // we calculate the value of the other function as the average in the resolution range
            double otherValue = otherIntegrable.integral(x - (resolution / 2), x + (resolution / 2)) / resolution;
            double newValue = this.data.get(key) * otherValue;
            newData.put(key, newValue);
        }
        return new BoundedTemplate(lowerBound, upperBound, newData, this.resolution);
    }

    private Template multiplyWithIntegrable(IntegrableFunction other) throws CalculationException {
        Map<Integer, Double> newData = new TreeMap<Integer, Double>();
        for (Integer key : this.data.keySet()) {
            double x = key * resolution;
            // we calculate the value of the other function as the average in the resolution range
            double otherValue = other.integral(x - (resolution / 2), x + (resolution / 2)) / resolution;
            double newValue = this.data.get(key) * otherValue;
            newData.put(key, newValue);
        }
        return new Template(newData, this.resolution);
    }
    
    @Override
    Template multiply(Function other) throws UnsupportedMultiplicationType, CalculationException {
        if (other instanceof Template) {
            return multiplyWithTemplate((Template) other);
        }
        if (other instanceof IntegrableFunction) {
            if (other instanceof BoundedFunction) {
                return multiplyWithBoundedIntegrable((Function) other);
            } else {
                return multiplyWithIntegrable((IntegrableFunction) other);
            }
        }
        throw new UnsupportedMultiplicationType();
    }
    
    public Map<Double, Double> mapToPlot() {
        Map<Double, Double> result = new TreeMap<Double, Double>();
        boolean bounded = false;
        double min = Double.NEGATIVE_INFINITY;
        double max = Double.POSITIVE_INFINITY;
        if (this instanceof BoundedFunction) {
            bounded = true;
            min = ((BoundedFunction) this).getLowerBound();
            max = ((BoundedFunction) this).getUpperBound();
        }
        for (Integer i : data.keySet()) {
            double x = i * resolution;
            if (x < min || x > max)
                continue;
            try {
                result.put(x, value(x));
            } catch (CalculationException ex) {
                ex.printStackTrace();
            }
        }
        if (result.isEmpty()) {
            result.put(min, 0.);
            result.put(max, 0.);
        }
        return result;
    }
    
}
