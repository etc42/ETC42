/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.plugins.skybackground.skybrightnesstable.calculation;

import java.util.Map;
import java.util.TreeMap;
import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.apache.commons.math.analysis.integration.RombergIntegrator;

/**
 */
public class BlackBody implements IntegrableFunction {
    
    private double temperature;

    public BlackBody(double temperature) {
        this.temperature = temperature;
    }

    @Override
    public double value(double x) {
        double a = 2 * Constants.PLANK * Constants.SPEED_OF_LIGHT * Constants.SPEED_OF_LIGHT; // 2*h*c^2
        double b = Math.pow(x,5); // lambda^5
        double c = Constants.PLANK * Constants.SPEED_OF_LIGHT; // h*c
        double d = x * Constants.BOLTZMANN * temperature; // lambda*k* T
        double e = Math.exp(c/d); // e^(h*c/(lambda*k*T))
        double radiation = (a/b)/(e-1);
        return radiation;
    }

    @Override
    public double integral(double x1, double x2) throws CalculationException {
        RombergIntegrator integrator = new RombergIntegrator();
        UnivariateRealFunction urf = new UnivariateRealFunction() {
            @Override
            public double value(double x) throws FunctionEvaluationException {
                double value = BlackBody.this.value(x);
                return value;
            }
        };
        double result = 0;
        try {
            result = integrator.integrate(urf, x1, x2);
        } catch (Exception ex) {
            throw new CalculationException(ex);
        }
        return result;
    }
    
    public Map<Double, Double> mapToPlot(double low, double up) {
        Map<Double, Double> result = new TreeMap<Double, Double>();
        for (double x = low; x <= up; x+= 0.1) {
            result.put(x, value(x));
        }
        return result;
    }
    
}
