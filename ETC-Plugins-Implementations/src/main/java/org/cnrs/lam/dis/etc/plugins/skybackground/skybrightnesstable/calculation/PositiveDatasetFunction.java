/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.plugins.skybackground.skybrightnesstable.calculation;

import java.util.HashMap;
import java.util.Map;

/**
 */
public class PositiveDatasetFunction extends DatasetFunction {

    public PositiveDatasetFunction(Map<Double, Double> data) throws CalculationException {
        super(data);
    }

    @Override
    public double value(double x) throws CalculationException {
        double value = super.value(x);
        return (value >= 0) ? value : 0;
    }
    
    public static PositiveDatasetFunction getForIntegerKeys(Map<Integer, Double> data) throws CalculationException {
        Map<Double, Double> newData = new HashMap<Double, Double>();
        for (Map.Entry<Integer, Double> entry : data.entrySet()) {
            newData.put(Double.valueOf(entry.getKey()), entry.getValue());
        }
        return new PositiveDatasetFunction(newData);
    }
    
}
