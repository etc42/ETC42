/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.plugins.skybackground.skybrightnesstable.calculation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.math.special.Erf;
import org.cnrs.lam.dis.etc.configuration.ConfigFactory;
import org.javatuples.Triplet;

/**
 */
public class EmissionLines extends Template {
    
    private static final double SQRT_2_PI = Math.sqrt(2 * Math.PI) / 2.37;
    private static final double SQRT_2 = Math.sqrt(2) / 2.37;

    public EmissionLines(Map<Integer, Double> data, double resolution) {
        super(data, resolution);
    }
    
    public static EmissionLines createFromFile(File emissionLinesFile) throws IOException, CalculationException {
        if (emissionLinesFile == null || emissionLinesFile.getName().isEmpty())
            throw new IOException("File path cannot be empty");
        if (!emissionLinesFile.exists())
            throw new IOException("The file " + emissionLinesFile + " does not exist");
        if (!emissionLinesFile.isFile())
            throw new IOException("The file " + emissionLinesFile + " is not a file");
        List<Triplet<Double, Double, Double>> emissionLinesList = parseFile(emissionLinesFile);
        Map<Integer, Double> data = new HashMap<Integer, Double>();
        double resolution = ConfigFactory.getConfig().getSpectrumResolution();
        for (Triplet<Double, Double, Double> emissionLine : emissionLinesList) {
            addEmissionLineInData(emissionLine, data, resolution);
        }
        return new EmissionLines(data, resolution);
    }

    private static void addEmissionLineInData(Triplet<Double, Double, Double> emissionLine, Map<Integer, Double> data, double resolution) throws CalculationException {
        double lambda = emissionLine.getValue0();
        double fwhm = emissionLine.getValue1();
        double fluxPeak = emissionLine.getValue2();
        double fluxTotal = SQRT_2_PI * fwhm * fluxPeak;
        int lowLambda = (int) Math.floor((lambda - 2 * fwhm) / resolution);
        int hiLambda = (int) Math.floor((lambda + 2 * fwhm) / resolution);
        for (int i = lowLambda; i <= hiLambda; i++) {
            double erfHigh = 0;
            double erfLow = 0;
            try {
                double iLambda = i * resolution;
                double range = 0.5 * resolution;
                erfLow = Erf.erf((iLambda - range - lambda) / (SQRT_2 * fwhm));
                erfHigh = Erf.erf((iLambda + range - lambda) / (SQRT_2 * fwhm));
            } catch (Exception e) {
                throw new CalculationException(e);
            }
            double value = fluxTotal * (erfHigh - erfLow) / 2;
            Double oldValue = data.get(i);
            if (oldValue != null) {
                value += oldValue;
            }
            data.put(i, value);
        }
    }

    private static List<Triplet<Double, Double, Double>> parseFile(File emissionLinesFile) throws IOException {
        BufferedReader in = new BufferedReader(new FileReader(emissionLinesFile));
        List<Triplet<Double, Double, Double>> valuesFromFile = new ArrayList<Triplet<Double, Double, Double>>();
        String line;
        int lineNumber = 0;
        try {
            while ((line = in.readLine()) != null) {
                lineNumber++;
                if (line.indexOf('#') != -1)
                    line = line.substring(0, line.indexOf('#'));
                if (line.trim().length() == 0)
                    continue;
                String[] data = line.trim().split("\\s+");
                if (data.length != 3)
                    throw new NumberFormatException();
                valuesFromFile.add(new Triplet<Double, Double, Double>
                        (Double.valueOf(data[0]), Double.valueOf(data[1]), Double.valueOf(data[2])));
            }
        } catch (NumberFormatException ex) {
            throw new IOException("The file " + emissionLinesFile + " didn't contain valid data (line " + lineNumber + ")");
        }
        in.close();
        return valuesFromFile;
    }
    
}
