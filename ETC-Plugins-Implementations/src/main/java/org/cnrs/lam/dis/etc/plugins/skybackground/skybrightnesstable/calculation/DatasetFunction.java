/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.plugins.skybackground.skybrightnesstable.calculation;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import org.apache.commons.math.ArgumentOutsideDomainException;
import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.apache.commons.math.analysis.integration.RombergIntegrator;
import org.apache.commons.math.analysis.interpolation.SplineInterpolator;
import org.apache.commons.math.analysis.polynomials.PolynomialSplineFunction;

/**
 */
public class DatasetFunction implements BoundedFunction, IntegrableFunction {
    
    private Double minKey;
    private Double maxKey;
    private PolynomialSplineFunction function;

    public DatasetFunction(Map<Double, Double> data) throws CalculationException {
        Map<Double, Double> shortedData = new TreeMap<Double, Double>(data);
        double[] x = new double[shortedData.size()];
        double[] y = new double[shortedData.size()];
        int i = 0;
        for (Map.Entry<Double, Double> entry : shortedData.entrySet()) {
            x[i] = entry.getKey();
            y[i] = entry.getValue();
            i++;
        }
        minKey = x[0];
        maxKey = x[i - 1];
        // We use SpLine interpolation. This means the values are calculated with
        // third degree polynomials between the knots
        try {
            function = new SplineInterpolator().interpolate(x, y);
        } catch (Exception ex) {
            throw new CalculationException(ex);
        }
    }
    
    public static DatasetFunction getForIntegerKeys(Map<Integer, Double> data) throws CalculationException {
        Map<Double, Double> newData = new HashMap<Double, Double>();
        for (Map.Entry<Integer, Double> entry : data.entrySet()) {
            newData.put(Double.valueOf(entry.getKey()), entry.getValue());
        }
        return new DatasetFunction(newData);
    }

    @Override
    public double getLowerBound() {
        return minKey;
    }

    @Override
    public double getUpperBound() {
        return maxKey;
    }

    @Override
    public double value(double x) throws CalculationException {
        if (x < minKey || x > maxKey) {
            throw new CalculationException("Given X = " + x + " is out of the function bounds [" + minKey + "," + maxKey + "]");
        }
        Double y = null;
        try {
            y = function.value(x);
        } catch (ArgumentOutsideDomainException ex) {
            throw new CalculationException(ex);
        }
        return y;
    }

    @Override
    public double integral(double x1, double x2) throws CalculationException {
        RombergIntegrator integrator = new RombergIntegrator();
        UnivariateRealFunction urf = new UnivariateRealFunction() {
            @Override
            public double value(double x) throws FunctionEvaluationException {
                double value = 0;
                try {
                    value = DatasetFunction.this.value(x);
                } catch (CalculationException ex) {
                    throw new FunctionEvaluationException(ex, x);
                }
                return value;
            }
        };
        double result = 0;
        try {
            result = integrator.integrate(urf, x1, x2);
        } catch (Exception ex) {
            throw new CalculationException(ex);
        }
        return result;
    }
    
    public Map<Double, Double> mapToPlot() {
        Map<Double, Double> result = new TreeMap<Double, Double>();
        for (double x = getLowerBound(); x <= getUpperBound(); x+= 0.1) {
            try {
                result.put(x, value(x));
            } catch (CalculationException ex) {
                ex.printStackTrace();
            }
        }
        return result;
    }
    
}
