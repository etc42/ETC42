/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.plugins.skybackground.skybrightnesstable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.cnrs.lam.dis.etc.ui.swing.generic.TableColumnResizer;

/**
 */
public class SkyBrightnessTable extends JTable {
    
    private SkyBrightnessTableModel tableModel = new SkyBrightnessTableModel();
    
    public SkyBrightnessTable() {
        setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        setCellSelectionEnabled(false);
        setModel(tableModel);
    }
    
    public List<DatasetInfo> getFilterList() {
        return Collections.unmodifiableList(tableModel.filterList);
    }
    
    public List<String> getSkyTypeList() {
        return Collections.unmodifiableList(tableModel.skyTypeList);
    }
    
    public Double getSkyBrightness(DatasetInfo filter, String skyType) {
        if (!getFilterList().contains(filter) || !getSkyTypeList().contains(skyType)) {
            return null;
        }
        return tableModel.dataMap.get(filter).get(skyType);
    }
    
    public void addFilter(DatasetInfo filter) {
        tableModel.addFilter(filter);
    }
    
    public void removeFilter(DatasetInfo filter) {
        tableModel.removeFilter(filter);
    }
    
    public void addSkyType(String type) {
        tableModel.addSkyType(type);
    }
    
    public void removeSkyType(String type) {
        tableModel.removeSkyType(type);
    }
    
    private class SkyBrightnessTableModel extends AbstractTableModel {
        
        private List<String> skyTypeList = new ArrayList<String>();
        private List<DatasetInfo> filterList = new ArrayList<DatasetInfo>();
        private Map<DatasetInfo, Map<String, Double>> dataMap = new HashMap<DatasetInfo, Map<String, Double>>();

        void addSkyType(String type) {
            if (type == null || skyTypeList.contains(type)) {
                return;
            }
            skyTypeList.add(type);
            for (Map<String, Double> map : dataMap.values()) {
                map.put(type, 0.);
            }
            fireTableStructureChanged();
            TableColumnResizer.adjustColumnPreferredWidths(SkyBrightnessTable.this);
        }
        
        void removeSkyType(String type) {
            if (type == null || !skyTypeList.contains(type)) {
                return;
            }
            skyTypeList.remove(type);
            for (Map<String, Double> map : dataMap.values()) {
                map.remove(type);
            }
            fireTableStructureChanged();
            TableColumnResizer.adjustColumnPreferredWidths(SkyBrightnessTable.this);
        }
        
        void addFilter(DatasetInfo filter) {
            if (filter == null || filterList.contains(filter)) {
                return;
            }
            filterList.add(filter);
            Map<String, Double> map = new HashMap<String, Double>();
            for (String skyType : skyTypeList) {
                map.put(skyType, 0.);
            }
            dataMap.put(filter, map);
            fireTableDataChanged();
            TableColumnResizer.adjustColumnPreferredWidths(SkyBrightnessTable.this);
        }
        
        void removeFilter(DatasetInfo filter) {
            if (filter == null || !filterList.contains(filter)) {
                return;
            }
            filterList.remove(filter);
            dataMap.remove(filter);
            fireTableDataChanged();
            TableColumnResizer.adjustColumnPreferredWidths(SkyBrightnessTable.this);
        }
        
        @Override
        public int getRowCount() {
            return dataMap.size();
        }

        @Override
        public int getColumnCount() {
            return skyTypeList.size() + 1;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            DatasetInfo filter = filterList.get(rowIndex);
            if (filter == null) {
                return null;
            }
            if (columnIndex == 0) {
                return filter;
            }
            String skyType = skyTypeList.get(columnIndex - 1);
            if (skyType == null) {
                return null;
            }
            return dataMap.get(filter).get(skyType);
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            switch (columnIndex) {
                case 0:
                    return false;
                default:
                    return true;
            }
        }

        @Override
        public String getColumnName(int column) {
            switch (column) {
                case 0:
                    return "Filter";
                default:
                    return skyTypeList.get(column - 1);
            }
        }

        @Override
        public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
            if (columnIndex == 0) {
                return;
            }
            double value = 0;
            try {
                value = Double.parseDouble(aValue.toString());
            } catch (Exception ex) {
                return;
            }
            DatasetInfo filter = filterList.get(rowIndex);
            String type = skyTypeList.get(columnIndex - 1);
            dataMap.get(filter).put(type, value);
            SkyBrightnessTable.this.repaint();
            TableColumnResizer.adjustColumnPreferredWidths(SkyBrightnessTable.this);
        }
        
    }
    
}
