/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.plugins.skybackground.skybrightnesstable;

import org.cnrs.lam.dis.etc.plugins.skybackground.skybrightnesstable.calculation.ConstantFrequencyFlux;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class Test {
    
    public static void main(String[] args) throws Exception {
        ConstantFrequencyFlux flux = ConstantFrequencyFlux.getForABmagnitude(21.3);
        System.out.println(flux.value(5500));
//        Map<Double, Double> data = new TreeMap<Double, Double>();
//        data.put(1., 0.);
//        data.put(1.1, 1.);
//        data.put(1.2, 2.);
//        data.put(1.3, 3.);
//        data.put(1.4, 4.);
//        data.put(1.5, 5.);
//        data.put(1.6, 4.);
//        data.put(1.7, 3.);
//        data.put(1.8, 2.);
//        data.put(1.9, 1.);
//        data.put(2., 0.);
//        DatasetFunction filter = new DatasetFunction(data);
//        Map<Integer, Double> data2 = new TreeMap<Integer, Double>();
//        data2.put(9, 1.);
//        data2.put(10, 1.);
//        data2.put(11, 1.);
//        data2.put(12, 1.);
//        data2.put(13, 1.);
//        data2.put(14, 1.);
//        data2.put(15, 1.);
//        data2.put(16, 1.);
//        data2.put(17, 1.);
//        data2.put(18, 1.);
//        data2.put(19, 1.);
//        data2.put(20, 1.);
//        data2.put(21, 1.);
//        Template template = new Template(data2, 0.1);
//        Template mult = (Template) Multiplier.multiply(template, filter);
//        for (int i = 10; i <= 20; i ++) {
//            System.out.println(mult.value(i*0.1));
//        }
//        System.out.println("");
//        System.out.println(mult.integral(1, 2));
    }
    
}
