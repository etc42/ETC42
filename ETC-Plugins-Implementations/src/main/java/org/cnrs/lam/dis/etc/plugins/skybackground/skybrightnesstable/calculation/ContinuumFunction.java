/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.plugins.skybackground.skybrightnesstable.calculation;

import java.util.Map;
import java.util.TreeMap;

/**
 */
public class ContinuumFunction implements BoundedFunction, IntegrableFunction {
    
    private double minKey;
    private double maxKey;
    private int[] x;
    private double[] y;
    
    public ContinuumFunction(Map<Integer, Double> data) throws CalculationException {
        Map<Integer, Double> shortedData = new TreeMap<Integer, Double>(data);
        x = new int[shortedData.size()];
        y = new double[shortedData.size()];
        int i = 0;
        for (Map.Entry<Integer, Double> entry : shortedData.entrySet()) {
            x[i] = entry.getKey();
            y[i] = entry.getValue();
            i++;
        }
        minKey = x[0];
        maxKey = x[i -1 ];
    }

    @Override
    public double getLowerBound() {
        return minKey;
    }

    @Override
    public double getUpperBound() {
        return maxKey;
    }

    @Override
    public double value(double lambda) throws CalculationException {
        if (lambda < minKey || lambda > maxKey) {
            throw new CalculationException("Given X = " + x + " is out of the function bounds [" + minKey + "," + maxKey + "]");
        }
        int lowIndex = findLowIndex(0, x.length - 1, lambda);
        if (lambda == lowIndex)
            return y[lowIndex];
        int hiIndex = lowIndex + 1;
        double lowFactor = (x[hiIndex] - lambda) / (x[hiIndex] - x[lowIndex]);
        double value = y[lowIndex] * lowFactor + y[hiIndex] * (1 - lowFactor);
        return value;
    }

    @Override
    public double integral(double lambda1, double lambda2) throws CalculationException {
        int iLow = findLowIndex(0, x.length - 1, lambda1);
        int iHi = findHiIndex(0, x.length - 1, lambda2);
        if (iLow == iHi)
            return 0;
        double integral = 0;
        for (int i = iLow; i < iHi; i++) {
            integral += (y[i] + y[i + 1]) * (x[i + 1] - x[i]) / 2;
        }
        double lowIntegral = (y[iLow] + y[iLow + 1]) * (x[iLow + 1] - x[iLow]) / 2;
        lowIntegral *= (lambda1 - x[iLow]) / (x[iLow + 1] - x[iLow]);
        integral -= lowIntegral;
        double hiIntegral = (y[iHi] + y[iHi - 1]) * (x[iHi] - x[iHi - 1]) / 2;
        hiIntegral *= (x[iHi] - lambda2) / (x[iHi] - x[iHi - 1]);
        integral -= hiIntegral;
        return integral;
    }

    private int findLowIndex(int iLow, int iHi, double lambda) {
        if (iHi - iLow <= 1)
            return iLow;
        int iMiddle = iLow + (iHi - iLow) / 2;
        if (lambda < x[iMiddle])
            return findLowIndex(iLow, iMiddle, lambda);
        else
            return findLowIndex(iMiddle, iHi, lambda);
    }

    private int findHiIndex(int iLow, int iHi, double lambda) {
        if (iHi - iLow <= 1)
            return iHi;
        int iMiddle = iLow + (iHi - iLow) / 2;
        if (lambda < x[iMiddle])
            return findHiIndex(iLow, iMiddle, lambda);
        else
            return findHiIndex(iMiddle, iHi, lambda);
    }
    
    public Map<Double, Double> mapToPlot() {
        Map<Double, Double> result = new TreeMap<Double, Double>();
        for (double x = getLowerBound(); x <= getUpperBound(); x+= 0.1) {
            try {
                result.put(x, value(x));
            } catch (CalculationException ex) {
                ex.printStackTrace();
            }
        }
        return result;
    }
    
}
