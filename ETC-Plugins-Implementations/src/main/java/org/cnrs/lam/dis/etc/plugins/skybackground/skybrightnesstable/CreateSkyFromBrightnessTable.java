/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.plugins.skybackground.skybrightnesstable;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.swing.JOptionPane;
import org.apache.log4j.Logger;
import org.cnrs.lam.dis.etc.configuration.ConfigFactory;
import org.cnrs.lam.dis.etc.datamodel.*;
import org.cnrs.lam.dis.etc.plugins.*;
import org.cnrs.lam.dis.etc.plugins.skybackground.skybrightnesstable.calculation.BoundedTemplate;
import org.cnrs.lam.dis.etc.plugins.skybackground.skybrightnesstable.calculation.CalculationException;

/**
 *
 * @author Nikolaos Apostolakos
 */
@Plugin(menuPath={"Sky Flux","Generate from brightness table"},tooltip="Generate sky flux from brightness table")
public class CreateSkyFromBrightnessTable extends PluginFrame {
    
    private static final Logger logger = Logger.getLogger(CreateSkyFromBrightnessTable.class);

    /** Creates new form CreateSkyFromBrightnessTable */
    public CreateSkyFromBrightnessTable() {
        initComponents();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        nameTextField = new javax.swing.JTextField();
        descriptionTextField = new javax.swing.JTextField();
        globalyAvailableCheckBox = new javax.swing.JCheckBox();
        jPanel2 = new javax.swing.JPanel();
        addFilterButton = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        skyBrightnessTable = new org.cnrs.lam.dis.etc.plugins.skybackground.skybrightnesstable.SkyBrightnessTable();
        addTypeButton = new javax.swing.JButton();
        removeFilterButton = new javax.swing.JButton();
        removeTypeButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        createButton = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        emissionLinesFileTextField = new org.cnrs.lam.dis.etc.plugins.generic.FileTextField();
        jPanel4 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        continuumResolutionTextField = new org.cnrs.lam.dis.etc.ui.swing.generic.IntegerTextField();
        jLabel3 = new javax.swing.JLabel();
        continuumAccuracy = new org.cnrs.lam.dis.etc.ui.swing.generic.PercentageTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        continuumMaxIterationsTextField = new org.cnrs.lam.dis.etc.ui.swing.generic.IntegerTextField();
        jLabel8 = new javax.swing.JLabel();
        continuumSmothnessTextField = new org.cnrs.lam.dis.etc.ui.swing.generic.IntegerTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Info", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("DejaVu Sans", 0, 12))); // NOI18N

        jLabel5.setText("Name:");

        jLabel6.setText("Description:");

        nameTextField.setName("nameTextField"); // NOI18N

        descriptionTextField.setName("descriptionTextField"); // NOI18N

        globalyAvailableCheckBox.setText("Globaly available");
        globalyAvailableCheckBox.setName("globalyAvailableCheckBox"); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(globalyAvailableCheckBox)
                    .addComponent(nameTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 279, Short.MAX_VALUE)
                    .addComponent(descriptionTextField, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 279, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(nameTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(descriptionTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(globalyAvailableCheckBox)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Sky Brightness", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("DejaVu Sans", 0, 12))); // NOI18N

        addFilterButton.setText("Add filter");
        addFilterButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addFilterButtonActionPerformed(evt);
            }
        });

        jScrollPane1.setViewportView(skyBrightnessTable);

        addTypeButton.setText("Add sky type");
        addTypeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addTypeButtonActionPerformed(evt);
            }
        });

        removeFilterButton.setText("Remove filter");
        removeFilterButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeFilterButtonActionPerformed(evt);
            }
        });

        removeTypeButton.setText("Remove sky type");
        removeTypeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeTypeButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 360, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(addFilterButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(removeFilterButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(addTypeButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(removeTypeButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addFilterButton)
                    .addComponent(addTypeButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(removeFilterButton)
                    .addComponent(removeTypeButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 109, Short.MAX_VALUE)
                .addContainerGap())
        );

        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        createButton.setText("Create");
        createButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createButtonActionPerformed(evt);
            }
        });

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Sky Emission Lines", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("DejaVu Sans", 0, 12))); // NOI18N

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(emissionLinesFileTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 360, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(emissionLinesFileTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Continuum", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("DejaVu Sans", 0, 12))); // NOI18N

        jLabel1.setText("Knot distance:");

        jLabel2.setText("Å");

        continuumResolutionTextField.setText("200");
        continuumResolutionTextField.setNormalForeground(new java.awt.Color(0, 0, 0));

        jLabel3.setText("Error accuracy:");

        continuumAccuracy.setText("1");
        continuumAccuracy.setNormalForeground(new java.awt.Color(0, 0, 0));

        jLabel4.setText("%");

        jLabel7.setText("Max iterations:");

        continuumMaxIterationsTextField.setText("100");
        continuumMaxIterationsTextField.setNormalForeground(new java.awt.Color(0, 0, 0));

        jLabel8.setText("Smoothness:");

        continuumSmothnessTextField.setText("1");
        continuumSmothnessTextField.setNormalForeground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel8)
                    .addComponent(jLabel7)
                    .addComponent(jLabel3)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(continuumSmothnessTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 242, Short.MAX_VALUE)
                    .addComponent(continuumMaxIterationsTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 242, Short.MAX_VALUE)
                    .addComponent(continuumAccuracy, javax.swing.GroupLayout.DEFAULT_SIZE, 242, Short.MAX_VALUE)
                    .addComponent(continuumResolutionTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 242, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel4))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(continuumResolutionTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(continuumAccuracy, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(continuumMaxIterationsTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(continuumSmothnessTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(createButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cancelButton))
                    .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {cancelButton, createButton});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cancelButton)
                    .addComponent(createButton))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void addFilterButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addFilterButtonActionPerformed
        String instrumentName = getCommunicator().getCurrentSession().getInstrument().getInfo().getName();
        List<DatasetInfo> availableDatasetList = getCommunicator().getAvailableDatasetList(Dataset.Type.FILTER_TRANSMISSION, instrumentName);
        availableDatasetList.removeAll(skyBrightnessTable.getFilterList());
        DatasetInfo selectedFilter = (DatasetInfo) JOptionPane.showInputDialog(this, "Please select a filter to add", null, JOptionPane.PLAIN_MESSAGE, null, availableDatasetList.toArray(), null);
        if (selectedFilter == null) {
            return;
        }
        skyBrightnessTable.addFilter(selectedFilter);
    }//GEN-LAST:event_addFilterButtonActionPerformed

    private void addTypeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addTypeButtonActionPerformed
        String selectedType = JOptionPane.showInputDialog(this, "Please give the new sky type name");
        if (selectedType == null || selectedType.isEmpty() || skyBrightnessTable.getSkyTypeList().contains(selectedType)) {
            return;
        }
        skyBrightnessTable.addSkyType(selectedType);
    }//GEN-LAST:event_addTypeButtonActionPerformed

    private void removeFilterButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeFilterButtonActionPerformed
        DatasetInfo selectedFilter = (DatasetInfo) JOptionPane.showInputDialog(this, "Please select the filter to remove", null, JOptionPane.PLAIN_MESSAGE, null, skyBrightnessTable.getFilterList().toArray(), null);
        if (selectedFilter == null) {
            return;
        }
        skyBrightnessTable.removeFilter(selectedFilter);
    }//GEN-LAST:event_removeFilterButtonActionPerformed

    private void removeTypeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeTypeButtonActionPerformed
        String selectedType = (String) JOptionPane.showInputDialog(this, "Please select the sky type to remove", null, JOptionPane.PLAIN_MESSAGE, null, skyBrightnessTable.getSkyTypeList().toArray(), null);
        if (selectedType == null) {
            return;
        }
        skyBrightnessTable.removeSkyType(selectedType);
    }//GEN-LAST:event_removeTypeButtonActionPerformed

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        this.dispose();
    }//GEN-LAST:event_cancelButtonActionPerformed

    private void createButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createButtonActionPerformed
        if (!validateInput()) {
            return;
        }
        
        // Create the calculator and generate the sky templates
        List<DatasetInfo> filterList = skyBrightnessTable.getFilterList();
        List<String> skyTypesList = skyBrightnessTable.getSkyTypeList();
        Map<DatasetInfo, Map<String, Double>> skyBrightness = new HashMap<DatasetInfo, Map<String, Double>>();
        for (DatasetInfo filter : filterList) {
            Map<String, Double> skyMap = new HashMap<String, Double>();
            for (String skyType : skyTypesList) {
                skyMap.put(skyType, skyBrightnessTable.getSkyBrightness(filter, skyType));
            }
            skyBrightness.put(filter, skyMap);
        }
        PluginCommunicator communicator = getCommunicator();
        File emissionLinesFile = emissionLinesFileTextField.getSelectedFile();
        int continuumResolution = continuumResolutionTextField.getValue();
        double continuumErrorAccuracy = continuumAccuracy.getValue();
        int continuumMaxIterations = continuumMaxIterationsTextField.getValue();
        int continuumSmoothness = continuumSmothnessTextField.getValue();
        Calculator calculator = new Calculator(filterList, skyTypesList, skyBrightness, communicator,
                emissionLinesFile, continuumResolution, continuumErrorAccuracy, continuumMaxIterations, continuumSmoothness);
        CalculationResults results = new CalculationResults();
        Map<String, BoundedTemplate> skyTemplates = null;
        try {
            skyTemplates = calculator.performCalculation(results);
        } catch (CalculationException ex) {
            logger.error("Failed to perform sky background calculation.", ex);
            JOptionPane.showMessageDialog(this, "Failed to calculate the sky background. Reason:\n" + ex.getMessage() + "\nSee log file for more details", null, JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        // Create the new sky emisison dataset
        String name = nameTextField.getText().trim();
        String description = descriptionTextField.getText().trim();
        String xUnit = "\u00C5";
        String yUnit = "erg/cm^2/s/\u00C5/arcsec^2";
        boolean isGlobalyAvailable = globalyAvailableCheckBox.isSelected();
        String namespace = isGlobalyAvailable ? null : getCommunicator().getCurrentSession().getSite().getInfo().getName();
        DatasetInfo info = new DatasetInfo(name, namespace, description, skyBrightnessTable.getSkyTypeList());
        double resolution = ConfigFactory.getConfig().getSpectrumResolution();
        Map<String, Map<Double, Double>> data = new HashMap<String, Map<Double, Double>>();
        for (Map.Entry<String, BoundedTemplate> entry : skyTemplates.entrySet()) {
            String skyType = entry.getKey();
            BoundedTemplate tempate = entry.getValue();
            Map<Double, Double> skyTypeData = new TreeMap<Double, Double>();
            for (double lambda = tempate.getLowerBound(); lambda + resolution <= tempate.getUpperBound(); lambda += resolution) {
                try {
                    skyTypeData.put(lambda, tempate.integral(lambda, lambda + resolution));
                } catch (CalculationException ex) {
                    logger.error("Failed to create the sky template", ex);
                    JOptionPane.showMessageDialog(this, "Failed to calculate the sky background. Reason:\n" + ex.getMessage() + "\nSee log file for more details", null, JOptionPane.ERROR_MESSAGE);
                    return;
                }
            }
            data.put(skyType, skyTypeData);
        }
        try {
            getCommunicator().createMultiDataset(Dataset.Type.SKY_EMISSION, info, xUnit, yUnit, data);
        } catch (NameExistsException nameExistsException) {
            JOptionPane.showMessageDialog(this, "There is already a sky template with this name", null, JOptionPane.ERROR_MESSAGE);
            return;
        } catch (DatasetCreationException datasetCreationException) {
            datasetCreationException.printStackTrace();
            JOptionPane.showMessageDialog(this, "Failed to create sky template. Reason:\n" + datasetCreationException.getMessage(), null, JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        // Show the results for user reference and close the plugin window
        communicator.showResults(results);
        this.dispose();
    }//GEN-LAST:event_createButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addFilterButton;
    private javax.swing.JButton addTypeButton;
    private javax.swing.JButton cancelButton;
    private org.cnrs.lam.dis.etc.ui.swing.generic.PercentageTextField continuumAccuracy;
    private org.cnrs.lam.dis.etc.ui.swing.generic.IntegerTextField continuumMaxIterationsTextField;
    private org.cnrs.lam.dis.etc.ui.swing.generic.IntegerTextField continuumResolutionTextField;
    private org.cnrs.lam.dis.etc.ui.swing.generic.IntegerTextField continuumSmothnessTextField;
    private javax.swing.JButton createButton;
    private javax.swing.JTextField descriptionTextField;
    private org.cnrs.lam.dis.etc.plugins.generic.FileTextField emissionLinesFileTextField;
    private javax.swing.JCheckBox globalyAvailableCheckBox;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField nameTextField;
    private javax.swing.JButton removeFilterButton;
    private javax.swing.JButton removeTypeButton;
    private org.cnrs.lam.dis.etc.plugins.skybackground.skybrightnesstable.SkyBrightnessTable skyBrightnessTable;
    // End of variables declaration//GEN-END:variables

    @Override
    protected void initialize(PluginCommunicator communicator) {
        // Here we check that the loaded components contain the necessary information
        StringBuilder errors = new StringBuilder();
        Session session = communicator.getCurrentSession();
        if (communicator.getAvailableDatasetList(Dataset.Type.FILTER_TRANSMISSION, session.getInstrument().getInfo().getName()).isEmpty()) {
            errors.append("  There are no filters available for the selected Instrument\n");
        }
        if (session.getSite().getLocationType() == Site.LocationType.SPACE) {
            errors.append("  The selected Site is located on space but it should be located on ground\n");
        }
        if (session.getSite().getSkyAbsorption() == null) {
            errors.append("  The selected Site does not have sky absorption\n");
        }
        if (session.getSite().getSkyExtinction() == null) {
            errors.append("  The selected Site does not have sky extinction\n");
        }
        String errorsString = errors.toString();
        if (!errorsString.isEmpty()) {
            errorsString = "The following problems will not let the plugin run correctly.\n" + errorsString + "Please fix them before creating the sky background emission.";
            JOptionPane.showMessageDialog(this, errorsString, null, JOptionPane.WARNING_MESSAGE);
        }
    }

    private boolean validateInput() {
        // Check that the user gave a name
        String newName = nameTextField.getText().trim();
        if (newName.isEmpty()) {
            JOptionPane.showMessageDialog(this, "The name field cannot be empty", null, JOptionPane.WARNING_MESSAGE);
            return false;
        }
        // Check that there is no sky emission existing with this name
        String siteName = getCommunicator().getCurrentSession().getSite().getInfo().getName();
        // Note that with calling the getAvailableDatasetList method we get both the site specific and the generic datasets
        List<DatasetInfo> existingSkyEmissionList = getCommunicator().getAvailableDatasetList(Dataset.Type.SKY_EMISSION, siteName);
        siteName = globalyAvailableCheckBox.isSelected() ? null : siteName;
        if (existingSkyEmissionList.contains(new DatasetInfo(newName, siteName, null))) {
            JOptionPane.showMessageDialog(this, "There is already a sky emission with the given name", null, JOptionPane.WARNING_MESSAGE);
            return false;
        }
        // Check that there is nothing missing from the sky brightness table
        if (skyBrightnessTable.getFilterList().isEmpty()) {
            JOptionPane.showMessageDialog(this, "At least one filter must be set for the sky brightness table", null, JOptionPane.WARNING_MESSAGE);
            return false;
        }
        if (skyBrightnessTable.getSkyTypeList().isEmpty()) {
            JOptionPane.showMessageDialog(this, "At least one sky type must be set for the sky brightness table", null, JOptionPane.WARNING_MESSAGE);
            return false;
        }
        // Check that if the emission lines are given the file is valid
        if (!emissionLinesFileTextField.getSelectedFile().getName().trim().isEmpty()) {
            if (!emissionLinesFileTextField.getSelectedFile().exists()) {
                JOptionPane.showMessageDialog(this, "The sky emission lines file does not exist", null, JOptionPane.WARNING_MESSAGE);
                return false;
            }
            if (!emissionLinesFileTextField.getSelectedFile().isFile()) {
                JOptionPane.showMessageDialog(this, "The sky emission lines file is not a file", null, JOptionPane.WARNING_MESSAGE);
                return false;
            }
        }
        // Check that we have all the necessary information from the site
        Site site = getCommunicator().getCurrentSession().getSite();
        if (site.getLocationType() == Site.LocationType.SPACE) {
            JOptionPane.showMessageDialog(this, "The selected Site is located on space but it should be located on ground", null, JOptionPane.WARNING_MESSAGE);
            return false;
        }
        if (site.getSkyAbsorption() == null) {
            JOptionPane.showMessageDialog(this, "The selected Site does not have sky absorption", null, JOptionPane.WARNING_MESSAGE);
            return false;
        }
        if (site.getSkyExtinction() == null) {
            JOptionPane.showMessageDialog(this, "The selected Site does not have sky extinction", null, JOptionPane.WARNING_MESSAGE);
            return false;
        }
        if (continuumResolutionTextField.getValue() < 1) {
            JOptionPane.showMessageDialog(this, "The continuum resolution cannot be less than 1", null, JOptionPane.WARNING_MESSAGE);
            return false;
        }
        if (continuumSmothnessTextField.getValue() < 0) {
            JOptionPane.showMessageDialog(this, "The continuum smoothness cannot be a negative number", null, JOptionPane.WARNING_MESSAGE);
            return false;
        }
        return true;
    }
}
