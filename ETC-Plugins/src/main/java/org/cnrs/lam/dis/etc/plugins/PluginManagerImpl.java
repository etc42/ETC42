/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.plugins;

import java.awt.Component;
import java.io.*;
import java.lang.reflect.Modifier;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import org.apache.log4j.Logger;
import org.cnrs.lam.dis.etc.configuration.ConfigFactory;
import org.javatuples.Triplet;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;

/**
 */
class PluginManagerImpl implements PluginManager {
    
    private static Logger logger = Logger.getLogger(PluginManagerImpl.class.getName());
    private PluginCommunicator communicator;
    private Component locationComponent;
    private List<Triplet<Class, List<String>, String>> pluginList = null;
    
    private ClassLoader createPluginDirClassLoader() {
        List<URL> urls = new ArrayList<URL>();
        File pluginDir = new File(ConfigFactory.getConfig().getPluginDir());
        if (pluginDir.isDirectory()) {
            for (File file : pluginDir.listFiles()) {
                if (file.getName().matches(".*\\.jar")) {
                    try {
                        urls.add(file.toURI().toURL());
                    } catch (MalformedURLException e) {
                        logger.warn("Failed to load plugin file " + file, e);
                    }
                }
            }
        }
        // *********************************************************************
        // This is a fix for bug #109. The problem is that the org.collections
        // library used for scanning the jar files for plugins cannot handle the http
        // protocol. For this reason we copy the jar in a temporary file localy and
        // we add it in the list of the plugin jars.
        if (getClass().getClassLoader() instanceof URLClassLoader) {
            URLClassLoader urlClassLoader = (URLClassLoader) getClass().getClassLoader();
            for (URL url : urlClassLoader.getURLs()) {
                if ("http".equals(url.getProtocol()) && url.getFile().contains("ETC-Plugins-Implementations")) {
                    try {
                        File urlFile = new File(url.getFile());
                        File tempFile = new File(ConfigFactory.getConfig().getTempDir() + ConfigFactory.getConfig().getFileSeparator() + urlFile.getName() + System.nanoTime() + ".jar");
                        copyFromStream(url.openStream(), tempFile);
                        tempFile.deleteOnExit();
                        urls.add(tempFile.toURI().toURL());
                    } catch (IOException ex) {
                        logger.error("Failed to add the ETC-Plugin-Implementations jar file in the class loader", ex);
                    }
                }
            }
        }
        // *********************************************************************
        return new URLClassLoader(urls.toArray(new URL[] {}), getClass().getClassLoader());
    }
    
    private void createPluginList() {
        pluginList = new ArrayList<Triplet<Class, List<String>, String>>();
        ClassLoader pluginDirClassLoader = createPluginDirClassLoader();
        Set<String> pluginClassNames = getPluginClassNames(pluginDirClassLoader);
        for (String className : pluginClassNames) {
            Triplet<Class, List<String>, String>
                    triplet = createPluginTriplet(className, pluginDirClassLoader);
            if (triplet != null) {
                pluginList.add(createPluginTriplet(className, pluginDirClassLoader));
            }
        }
    }
    
    @Override
    public void setCommunicator(PluginCommunicator communicator) {
        this.communicator = communicator;
    }

    @Override
    public void setLocationComponent(Component component) {
        this.locationComponent = component;
    }

    @Override
    public List<Triplet<Class, List<String>, String>> getAvailablePlugins() {
        if (pluginList == null) {
            createPluginList();
        }
        return pluginList;
    }

    private Triplet<Class, List<String>, String> createPluginTriplet(String className, ClassLoader classLoader) {
        Class<?> pluginClass = null;
        try {
            pluginClass = classLoader.loadClass(className);
        } catch (ClassNotFoundException ex) {
            logger.error("Failed to configure plugin " + className, ex);
            return null;
        }
        if (Modifier.isAbstract(pluginClass.getModifiers())) {
            logger.info("Ignoring plugin class " + pluginClass.getName() + " because it is abstract");
            return null;
        }
        Plugin annotation = pluginClass.getAnnotation(Plugin.class);
        if (annotation == null) {
            logger.error("The plugin " + className + " is not annotated with the '@Plugin' annotation.");
            return null;
        }
        return new Triplet<Class, List<String>, String>
                ((Class<? extends PluginFrame>) pluginClass, Arrays.asList(annotation.menuPath()), annotation.tooltip());
    }

    private Set<String> getPluginClassNames(ClassLoader classLoader) {
        Reflections reflections = new Reflections(new ConfigurationBuilder()
                .filterInputsBy(new FilterBuilder().include(".*class").exclude("org.slf4j.*"))
                .setUrls(ClasspathHelper.forClassLoader(classLoader))
                .setScanners(new SubTypesScanner()));
        return reflections.getStore().getSubTypesOf(PluginFrame.class.getName());
    }

    @Override
    public void runPlugin(Class<? extends PluginFrame> plugin) throws PluginException {
        PluginFrame pluginInstance = null;
        try {
            pluginInstance = plugin.newInstance();
            pluginInstance.setCommunicator(communicator);
        } catch (Exception ex) {
            logger.error("Failed to instantiate plugin " + plugin.getName() + ". Reason : " + ex.getMessage(), ex);
            return;
        }
        pluginInstance.setLocationRelativeTo(locationComponent);
        pluginInstance.setVisible(true);
    }

    @Override
    public void addPluginsFromJar(File jarFile) throws IOException, PluginException {
        File distFile = copyJarInPluginDirectory(jarFile);
        // We create a class loader which will only read from the new jar file.
        // We do that for speed up the search of plugins. This class loader CANNOT
        // be used for creating the instances, as it does not know anything about
        // the plugin framework classes
        URLClassLoader jarClassLoader = new URLClassLoader(new URL[] {distFile.toURI().toURL()}, null);
        Set<String> pluginClassNames = getPluginClassNames(jarClassLoader);
        if (pluginClassNames.isEmpty()) {
            distFile.delete();
            throw new PluginException("The jar file " + jarFile + " does not contain any ETC-42 plugins");
        }
        // We create now a class loader which can be used for creating the instances
        jarClassLoader = new URLClassLoader(new URL[] {distFile.toURI().toURL()});
        List<Triplet<Class, List<String>, String>> tempList = new ArrayList<Triplet<Class, List<String>, String>>();
        for (String className : pluginClassNames) {
            Triplet<Class, List<String>, String>
                    triplet = createPluginTriplet(className, jarClassLoader);
            if (triplet != null) {
                tempList.add(createPluginTriplet(className, jarClassLoader));
            }
        }
        // We check now that we do not have already the same plugins installed
        StringBuilder alreadyInstalledError = new StringBuilder();
        for (Triplet<Class, List<String>, String> installed : pluginList) {
            Class installedClass = installed.getValue0();
            for (Triplet<Class, List<String>, String> toBeInstalled : tempList) {
                Class toBeInstalledClass = toBeInstalled.getValue0();
                if (installedClass.getName().equals(toBeInstalledClass.getName())) {
                    alreadyInstalledError.append(toBeInstalledClass.getName());
                    alreadyInstalledError.append(ConfigFactory.getConfig().getLineSeparator());
                }
            }
        }
        String alreadyInstalledErrorString = alreadyInstalledError.toString();
        if (!alreadyInstalledErrorString.isEmpty()) {
            distFile.delete();
            throw new PluginException("The following plugins are already installed"
                    + ConfigFactory.getConfig().getLineSeparator() + alreadyInstalledErrorString);
        }
        pluginList.addAll(tempList);
    }
  
    private File copyJarInPluginDirectory(File jarFile) throws IOException {
        if (!jarFile.isFile()) {
            throw new IOException("The " + jarFile + " is not a file");
        }
        String fileName = jarFile.getName();
        if (!fileName.endsWith(".jar")) {
            throw new IOException("The file " + jarFile + " is not a jar file");
        }
        fileName = fileName.substring(0, fileName.length() - 4);
        File destFile = new File(ConfigFactory.getConfig().getPluginDir(), fileName + ".jar");
        int i = 0;
        while (destFile.exists()) {
            i++;
            destFile = new File(ConfigFactory.getConfig().getPluginDir(), fileName + "_" + i + ".jar");
        }
        copyFile(jarFile, destFile);
        return destFile;
    }
    
    private void copyFile(File from, File to) throws IOException {
        if (!from.exists()) {
            throw new IOException("Failed to copy file " + from + " because it does not exist");
        }
        if (!from.isFile()) {
            throw new IOException("Failed to copy file " + from + " because it is not a file");
        }
        if (to.exists()) {
            throw new IOException("Failed to copy " + from + " to " + to + " because target exsists");
        }
        if (to.getParentFile() != null) {
            to.getParentFile().mkdirs();
        }
        to.createNewFile();
        FileChannel source = null;
        FileChannel destination = null;
        try {
            source = new FileInputStream(from).getChannel();
            destination = new FileOutputStream(to).getChannel();
            long count = 0;
            long size = source.size();
            while ((count += destination.transferFrom(source, 0, size - count)) < size);
        } finally {
            if (source != null) {
                source.close();
            }
            if (destination != null) {
                destination.close();
            }
        }
    }

    @Override
    public void reloadPlugins() {
        createPluginList();
    }

    private void copyFromStream(InputStream from, File to) throws IOException {
        if (to.exists()) {
            throw new IOException("Failed to copy " + from + " to " + to + " because target exsists");
        }
        if (to.getParentFile() != null) {
            to.getParentFile().mkdirs();
        }
        to.createNewFile();
        FileOutputStream out = new FileOutputStream(to);
        final int BUF_SIZE = 1 << 8;
        byte[] buffer = new byte[BUF_SIZE];
        int bytesRead = -1;
        while ((bytesRead = from.read(buffer)) > -1) {
            out.write(buffer, 0, bytesRead);
        }
        from.close();
        out.close();
    }
    
}
