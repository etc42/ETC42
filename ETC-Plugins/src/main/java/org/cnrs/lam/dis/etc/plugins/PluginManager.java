/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.plugins;

import java.awt.Component;
import java.io.File;
import java.io.IOException;
import java.util.List;
import org.javatuples.Triplet;

/**
 */
public interface PluginManager {
    
    /**
     * <p>Returns a list of {@link org.javatuples.Triplet} which contain the necessary
     * information for the available plugins. The triplets contain the following
     * things (in this order):</p>
     * <ol>
     * <li>A {@link java.lang.Class} representing the type of the plugin</li>
     * <li>A list containing the menu path and name (which is ordered)</li>
     * <li>A String containing the tooltip description</li>
     * </ol>
     * @return The available plugins
     */
    public List<Triplet<Class, List<String>, String>> getAvailablePlugins();
    
    /**
     * Executes the given plugin.
     * @param plugin 
     */
    public void runPlugin(Class<? extends PluginFrame> plugin) throws PluginException;
    
    public void setCommunicator(PluginCommunicator communicator);
    
    public void setLocationComponent(Component component);
    
    public void addPluginsFromJar(File jarFile) throws IOException, PluginException;
    
    public void reloadPlugins();
    
}
