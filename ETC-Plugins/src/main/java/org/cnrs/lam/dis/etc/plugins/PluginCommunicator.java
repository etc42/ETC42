/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.plugins;

import java.util.List;
import java.util.Map;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.Dataset.Type;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.cnrs.lam.dis.etc.datamodel.Session;

/**
 * The <code>PluginCommunicator</code> is responsible for the communication of a
 * plugin with the rest of the ETC. The idea is that the Controller will give an
 * object implementing this interface to each plugin.
 */
public interface PluginCommunicator {
    
    /**
     * A plugin can use this method to get access to the current instrument, site,
     * source and observing parameters. This can be done for two reasons. For
     * accessing the current values that might be used for a calculation or for
     * modifying some values, as result of the plugin functionality. Note that
     * all the changes done to the Session will be reflected to the rest of the
     * ETC.
     * @return The current session
     */
    public Session getCurrentSession();
    
    /**
     * This method can be used for accessing a Dataset object containing the data of the
     * dataset described by the given datasetInfo and option. If the option is
     * null, then the datasetInfo should represent a dataset that is not a
     * multi-dataset. If it is not null, it defines which option of the multi-dataset
     * will be returned.
     * @param type The type of the dataset to retrieve
     * @param datasetInfo The name and namespace of the dataset to retrieve
     * @param option The specific option of a multi-dataset to retrieve
     * @return The data of the dataset, or null if it is not available
     */
    public Dataset getDataset(Dataset.Type type, DatasetInfo datasetInfo, String option);
    
    /**
     * This method returns a list of <code>DatasetInfo</code> objects representing
     * all the available datasets for a specific type, for the given namespace.
     * Its purpose is to be used from plugins which are changing the selected
     * datasets, to check for available options. Because of this reason, the namespace
     * cannot be null or the empty string. In this case null is returned.
     * @param type The type of dataset to retrieve the list for
     * @return A list with the available datasets of the given type for the
     * current session
     */
    public List<DatasetInfo> getAvailableDatasetList(Type type, String namespace);
    
    /**
     * This method is used to create new single datasets in the ETC. If the
     * given namespace is null, then the dataset will be globally available. Otherwise
     * it will be available for the specific component. Note that if there is a 
     * problem with creating the Dataset 
     * an exception is thrown.
     * @param type The type of the dataset to create
     * @param info The name, namespace and description of the dataset
     * @param xUnit The unit of the X axis of the dataset
     * @param yUnit The unit of the Y axis of the dataset
     * @param data A map containing the data of the dataset
     * @throws NameExistsException If there is already a dataset with the given name
     * @throws DatasetCreationException If there is any problem while creating the dataset
     */
    public void createDataset(Dataset.Type type, DatasetInfo info,
            String xUnit, String yUnit, Map<Double, Double> data) 
            throws NameExistsException, DatasetCreationException;
    
    /**
     * This method is used to create new multiple datasets in the ETC. The keys of
     * the data map are the different options of the multi-dataset and the values
     * of it contain the data of each dataset. Note that the null and the empty
     * string are not acceptable keys. If
     * the given namespace is null, then the dataset will be globally available.
     * Otherwise it will be available only for the specified namespace.
     * @param type The type of the multi dataset to create
     * @param info The name, namespace and description of the dataset
     * @param xUnit The unit of the X axis of the dataset
     * @param yUnit The unit of the Y axis of the dataset
     * @param data A map with keys the options of the multi dataset and values the
     * data of the datasets
     */
    public void createMultiDataset(Dataset.Type type, DatasetInfo info, String xUnit,
            String yUnit, Map<String, Map<Double, Double>> data)
            throws NameExistsException, DatasetCreationException;
    
    /**
     * This method will update all the parts of the ETC (like the GUI) for modifications
     * done to the components by the plugin.
     */
    public void sessionModified();
    
    /**
     * This method can be used by plugins which want to run the ETC simulation and
     * further process the results. It is running all the ETC calculations and returns
     * all the results (final and intermediate), which can be further processed.
     * Note that this method is synchronous and will be blocked for the time the
     * calculations will be executed.
     * @return The results of the calculation
     */
    public CalculationResults runSimulation() throws CalculationFailed;
    
    /**
     * This method can be used by plugins which want to use the ETC infrastructure
     * for showing their results.
     * @param results 
     */
    public void showResults(CalculationResults results);
    
}
