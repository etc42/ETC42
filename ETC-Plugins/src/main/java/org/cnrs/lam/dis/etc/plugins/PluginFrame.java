/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.plugins;

import javax.swing.JFrame;

/**
 * <p>The {@code PlugingFrame} is the class all the plugins should inherit from.
 * Each plugin <b>must</b> implement the following things:</p>
 * <ul>
 * <li>It must have a default constructor (a constructor without parameters).
 * ETC will use this constructor for creating the plugin instances. Note that
 * during construction the {@code getCommunicator()} method (see bellow) will
 * always return null.</li>
 * <li>It must implement the {@code initialize(communicator)} method. This method
 * should do all the initialization which depends on the communicator (which is
 * not available during the execution of the constructor).</li>
 * <li>It must be annotated with the {@link org.cnrs.lam.dis.etc.plugins.Plugin}
 * annotation. Please refer to its javadoc for details about its parameters.</li>
 * </ul>
 * <p>The plugins can communicate with the rest of the ETC by using the method
 * {@code getCommunicator()}, which returns a {@link org.cnrs.lam.dis.etc.plugins.PluginCommunicator}.
 * For more details of using this object please look the javadoc of the communicator.
 * Note that if the {@code PluginFrame} hasn't been instantiated by the ETC plugin
 * framework (during testing for example), the communicator will not be available.</p>
 */
public abstract class PluginFrame extends JFrame {
    
    private PluginCommunicator communicator;
    
    final void setCommunicator(PluginCommunicator communicator) {
        this.communicator = communicator;
        initialize(communicator);
    }
    
    /**
     * Returns a communicator instance for communicating with the rest of the ETC.
     * Note that if the instance hasn't been instantiated by the ETC plugin
     * framework this method (if no other actions have been taken) will return null.
     * @return A communicator instance
     */
    final protected PluginCommunicator getCommunicator() {
        return communicator;
    }

    /**
     * This method should be overridden to perform any initialization which requires
     * the communicators existence. This method will be called automatically by the
     * plugin framework <b>after</b> the construction of the object is done (the
     * execution of the default constructor). Note that any call to the
     * {@code getCommunicator()} method during the execution of the constructor
     * will return null.
     * @param communicator The communicator to use for the initialization
     */
    protected abstract void initialize(PluginCommunicator communicator);
    
}
