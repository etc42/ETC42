/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.plugins;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>All ETC plugins must implement the {@code Plugin} interface. It provides the
 * ETC system with the necessary information about the plugin.</p>
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Plugin {
    
    /**
     * <p>The {@code menuPath} defines where the plugin will be accessible from the
     * GUI menu. All the plugins are under the menu {@code Plugins} and with the
     * name defined with the past string of this parameter. All the previous strings
     * are sub-menu names. For example, if a plugin is accessible under
     * "Plugins -&#62; My menu -&#62; My sub-menu -&#62; My plugin" the menuPath should
     * be:</p>
     * {@code menuPath = {"My menu", "My sub-menu", "My plugin"}
     * <p>Note that if there is a conflict with plugin names, ETC will add a numeric
     * postfix</p>
     * @return The menu path and name of the plugin
     */
    String[] menuPath();
    
    /**
     * <p>Defines the description which will be presented to the user when hovering
     * the mouse above the menu. It is optional but it is strongly recommended to
     * define it, as it will be useful for plugin separation during a name conflict.</p>
     * @return The description to be presented on mouse hovering
     */
    String tooltip() default "";
    
}
