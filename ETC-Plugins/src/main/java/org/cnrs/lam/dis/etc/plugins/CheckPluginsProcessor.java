/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.plugins;

import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.code.Symbol.ClassSymbol;
import com.sun.tools.javac.code.Type;
import com.sun.tools.javac.util.List;
import java.util.HashSet;
import java.util.Set;
import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Messager;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic.Kind;

/**
 * This is an annotation processor which checks that every {@code PluginFrame}
 * is annotated with the {@code @Plugin} annotation and it has a default constructor.
 * This check is done during compile time and the compilation will fail if the
 * checks fail.
 * 
 * NOTE!!!! Do not modify this class if you don't really know what you are doing...
 *
 * @author Nikolaos Apostolakos
 */
@SupportedAnnotationTypes("*")
@SupportedSourceVersion(SourceVersion.RELEASE_6)
public class CheckPluginsProcessor extends AbstractProcessor {

    /**
     * Overridden to check that every {@code PluginFrame} non abstract implementation
     * is annotated with the {@code @Plugin} annotation and it has a
     * public default constructor.
     * @param annotations
     * @param roundEnv
     * @return always false
     */
    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        Messager messager = processingEnv.getMessager();
        
        // First make a list with all the classes annotated as @Plugin
        // Every non abstract plugin must have this
        Set<Element> pluginElements = new HashSet<Element>();
        for (Element element : roundEnv.getElementsAnnotatedWith(Plugin.class)) {
            pluginElements.add(element);
        }
        
        // Go through all the elements to check for plugins
        for (Element element : roundEnv.getRootElements()) {
            // If the element is not a class we ignore it
            if (element.getKind() != ElementKind.CLASS)
                continue;
            // If the element is not a PluginFrame we ignore it
            if (!isPluginFrame(element))
                continue;
            // If the plugin is abstract we ignore it
            if (element.getModifiers().contains(Modifier.ABSTRACT))
                continue;
            
            //Ok... here we know we have an element wich is a non abstract PluginFrame!!!
            // Lets start checking that the user implemented everything correctly
            
            // check that the element is annotated with @Plugin
            if (!pluginElements.contains(element)) {
                messager.printMessage(Kind.ERROR, "The class " + element + " is a non abstract "
                        + "PluginFrame implementation, so it must be annotated with the "
                        + "@Plugin annotation", element);
            }
            // check that the element has a public default constructor
            if (!haveDefaultConstructor(element)) {
                messager.printMessage(Kind.ERROR, "The class " + element + " is a non abstract "
                        + "PluginFrame implementation, but it does not have a public default constructor", element);
            }
        }
        
        // Return false to let other processors to handle the annotations also
        return false;
    }

    /**
     * Searches the inheritance tree and returns if the given element is extending
     * the {@code PluginFrame} class or not.
     * @param element The element to check
     * @return true if the element is a {@code PluginFrame}, false otherwise
     */
    private boolean isPluginFrame(Element element) {
        // If the element is null or not a class kind it is not a plugin
        if (element == null || element.getKind() != ElementKind.CLASS)
            return false;
        // Check if this element is a plugin
        if (PluginFrame.class.getCanonicalName().equals(element.toString()))
                return true;
        // If it is not we check the superclass
        return isPluginFrame(((ClassSymbol) element).getSuperclass().asElement());
    }

    /**
     * Checks if the given element is a class which has a public default constructor (with
     * no arguments). During the check warnings are also generated if non default
     * constructors exist.
     * @param element The element to check
     * @return true if the element is a class with default constructor, false otherwise
     */
    private boolean haveDefaultConstructor(Element element) {
        // If the element is null or not a class kind it does not have constructors
        if (element == null || element.getKind() != ElementKind.CLASS)
            return false;
        // Go through all the members of the class and check for a default constructor
        boolean defaultConstructorFound = false;
        for (Symbol member : ((ClassSymbol) element).members().getElements()) {
            if (!member.isConstructor())
                continue;
            Symbol.MethodSymbol method = (Symbol.MethodSymbol) member;
            if (method.getParameters().isEmpty()) {
                if (method.getModifiers().contains(Modifier.PUBLIC))
                    defaultConstructorFound = true;
            } else { 
                processingEnv.getMessager().printMessage(Kind.MANDATORY_WARNING, "The PluginFrame " +
                        element + " has a non default constructor. ETC Plugin framework is not "
                        + "designed to use non default constructors", element);
            }
        }
        return defaultConstructorFound;
    }
    
}
