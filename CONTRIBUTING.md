We are always looking for contributors.

A contribution can be a feature request, any issue you can find, or even preset sharing.

- If you have any feature request, you can create an issue with the "enhancement" label.
- To notify us you found an issue in the software, you can create an issue with the "bug" or "critical" label.
- Have you made your own source, instrument, site or observing parameters ? Or even a more simple preset you would like to share, such as a sky emission template ? You are very welcome to share it with the community. Contact us at etc42@lam.fr so we can add it in the official repository ! Of course, your name will be added in the contributors list, and in the preset name itself.

Thank you for your incoming contributions !
