/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.datamodel;

import java.util.HashMap;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.*;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class CalculationResultsTest {

    public CalculationResultsTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test (expected=IllegalArgumentException.class)
    public void nameOfResultCannotBeNull() {
        new DoubleValueResult(null, 1.1, "m");
    }

    @Test (expected=IllegalArgumentException.class)
    public void nameOfResultCannotBeEmptyString() {
        new DoubleValueResult("", 1.1, "m");
    }

    @Test
    public void doubleValueResultInfoRetirevedOK() {
        DoubleValueResult result = new DoubleValueResult("Test", 1.2, "m");
        assertEquals("Test", result.getName());
        assertEquals(1.2, result.getValue(), Double.MIN_VALUE);
        assertEquals("m", result.getUnit());
    }

    @Test
    public void longValueResultInfoRetirevedOK() {
        LongValueResult result = new LongValueResult("Test", 1, "m");
        assertEquals("Test", result.getName());
        assertEquals(1, result.getValue());
        assertEquals("m", result.getUnit());
    }

    @Test
    public void doubleDatasetResultInfoRetrievedOK() {
        Map<Double,Double> map = new HashMap<Double, Double>();
        map.put(Double.valueOf(1),Double.valueOf(1));
        map.put(Double.valueOf(2),Double.valueOf(2));
        DoubleDatasetResult result = new DoubleDatasetResult("Test", map, "m", "s");
        assertEquals("Test", result.getName());
        assertNotNull(result.getValues());
        assertEquals(2, result.getValues().size());
        assertEquals(1, result.getValues().get(Double.valueOf(1)),Double.MIN_VALUE);
        assertEquals(2, result.getValues().get(Double.valueOf(2)),Double.MIN_VALUE);
        assertEquals("m", result.getXUnit());
        assertEquals("s", result.getYUnit());
    }

    @Test (expected=IllegalArgumentException.class)
    public void doubleDatasetValuesCannotBeNull() {
        new DoubleDatasetResult("Name", null, "m", "s");
    }

    @Test (expected=IllegalArgumentException.class)
    public void doubleDatasetValuesCannotBeEmpty() {
        new DoubleDatasetResult("Name", new HashMap<Double, Double>(), "m", "s");
    }

    @Test
    public void doubleDatasetValuesAreCopied() {
        Map<Double,Double> map = new HashMap<Double, Double>();
        map.put(Double.valueOf(1),Double.valueOf(1));
        DoubleDatasetResult result = new DoubleDatasetResult("Name", map, "m", "s");
        map.put(Double.valueOf(1), Double.valueOf("3"));
        assertEquals(1, result.getValues().get(Double.valueOf(1)), Double.MIN_VALUE);
    }

    @Test (expected=UnsupportedOperationException.class)
    public void doubleDatasetValuesReadOnly() {
        Map<Double,Double> map = new HashMap<Double, Double>();
        map.put(Double.valueOf(1),Double.valueOf(1));
        DoubleDatasetResult result = new DoubleDatasetResult("Name", map, "m", "s");
        result.getValues().put(Double.valueOf(1),Double.valueOf(2));
    }

    @Test
    public void longDatasetResultInfoRetrievedOK() {
        Map<Long,Long> map = new HashMap<Long, Long>();
        map.put(Long.valueOf(1),Long.valueOf(1));
        map.put(Long.valueOf(2),Long.valueOf(2));
        LongDatasetResult result = new LongDatasetResult("Test", map, "m", "s");
        assertEquals("Test", result.getName());
        assertNotNull(result.getValues());
        assertEquals(2, result.getValues().size());
        assertEquals(1, result.getValues().get(Long.valueOf(1)),Long.MIN_VALUE);
        assertEquals(2, result.getValues().get(Long.valueOf(2)),Long.MIN_VALUE);
        assertEquals("m", result.getXUnit());
        assertEquals("s", result.getYUnit());
    }

    @Test (expected=IllegalArgumentException.class)
    public void longDatasetValuesCannotBeNull() {
        new LongDatasetResult("Name", null, "m", "s");
    }

    @Test (expected=IllegalArgumentException.class)
    public void longDatasetValuesCannotBeEmpty() {
        new LongDatasetResult("Name", new HashMap<Long, Long>(), "m", "s");
    }

    @Test
    public void longDatasetValuesAreCopied() {
        Map<Long,Long> map = new HashMap<Long, Long>();
        map.put(Long.valueOf(1),Long.valueOf(1));
        LongDatasetResult result = new LongDatasetResult("Name", map, "m", "s");
        map.put(Long.valueOf(1), Long.valueOf("3"));
        assertEquals(1, result.getValues().get(Long.valueOf(1)), Long.MIN_VALUE);
    }

    @Test (expected=UnsupportedOperationException.class)
    public void longDatasetValuesReadOnly() {
        Map<Long,Long> map = new HashMap<Long, Long>();
        map.put(Long.valueOf(1),Long.valueOf(1));
        LongDatasetResult result = new LongDatasetResult("Name", map, "m", "s");
        result.getValues().put(Long.valueOf(1),Long.valueOf(2));
    }

    @Test
    public void longLongDoubleDatasetResultInfoRetrievedOK() {
        Map<Long,Double> map = new HashMap<Long, Double>();
        map.put(Long.valueOf(1),Double.valueOf(1));
        map.put(Long.valueOf(2),Double.valueOf(2));
        LongDoubleDatasetResult result = new LongDoubleDatasetResult("Test", map, "m", "s");
        assertEquals("Test", result.getName());
        assertNotNull(result.getValues());
        assertEquals(2, result.getValues().size());
        assertEquals(1, result.getValues().get(Long.valueOf(1)),Double.MIN_VALUE);
        assertEquals(2, result.getValues().get(Long.valueOf(2)),Double.MIN_VALUE);
        assertEquals("m", result.getXUnit());
        assertEquals("s", result.getYUnit());
    }

    @Test (expected=IllegalArgumentException.class)
    public void longLongDoubleDatasetValuesCannotBeNull() {
        new LongDoubleDatasetResult("Name", null, "m", "s");
    }

    @Test (expected=IllegalArgumentException.class)
    public void longLongDoubleDatasetValuesCannotBeEmpty() {
        new LongDoubleDatasetResult("Name", new HashMap<Long, Double>(), "m", "s");
    }

    @Test
    public void longLongDoubleDatasetValuesAreCopied() {
        Map<Long,Double> map = new HashMap<Long, Double>();
        map.put(Long.valueOf(1),Double.valueOf(1));
        LongDoubleDatasetResult result = new LongDoubleDatasetResult("Name", map, "m", "s");
        map.put(Long.valueOf(1), Double.valueOf("3"));
        assertEquals(1, result.getValues().get(Long.valueOf(1)), Double.MIN_VALUE);
    }

    @Test (expected=UnsupportedOperationException.class)
    public void longLongDoubleDatasetValuesReadOnly() {
        Map<Long,Double> map = new HashMap<Long, Double>();
        map.put(Long.valueOf(1),Double.valueOf(1));
        LongDoubleDatasetResult result = new LongDoubleDatasetResult("Name", map, "m", "s");
        result.getValues().put(Long.valueOf(1),Double.valueOf(2));
    }

    @Test (expected=IllegalArgumentException.class)
    public void addNullResultFails() {
        CalculationResults results = new CalculationResults();
        results.addResult(null, Level.FINAL);
    }

    @Test (expected=IllegalArgumentException.class)
    public void addNullLevelFails() {
        CalculationResults results = new CalculationResults();
        results.addResult(new DoubleValueResult("Test", 1, "m"), null);
    }

    @Test
    public void addExistingNameInSameLevel() {
        CalculationResults results = new CalculationResults();
        DoubleValueResult first = new DoubleValueResult("Test", 1, "m");
        DoubleValueResult second = new DoubleValueResult("Test", 2, "m");
        results.addResult(first, Level.FINAL);
        results.addResult(second, Level.FINAL);
        assertSame(second, results.getResults(Level.FINAL).get("Test"));
        assertNotSame(first, results.getResults(Level.FINAL).get("Test"));
    }

    @Test
    public void addExistingNameInDifferentLevel() {
        CalculationResults results = new CalculationResults();
        DoubleValueResult first = new DoubleValueResult("Test", 1, "m");
        DoubleValueResult second = new DoubleValueResult("Test", 2, "m");
        results.addResult(first, Level.FINAL);
        results.addResult(second, Level.DEBUG);
        assertSame(second, results.getResults(Level.DEBUG).get("Test"));
        assertNotSame(first, results.getResults(Level.DEBUG).get("Test"));
        assertNull(results.getResults(Level.FINAL).get("Test"));
    }

    @Test
    public void getResultByName() {
        CalculationResults results = new CalculationResults();
        results.addResult(new DoubleValueResult("Test", 1, "m"), Level.FINAL);
        Result result = results.getResultByName("Test");
        assertNotNull(result);
        assertEquals("Test", result.getName());
        results.addResult(new DoubleValueResult("Test2", 1, "m"), Level.DEBUG);
        result = results.getResultByName("Test2");
        assertNotNull(result);
        assertEquals("Test2", result.getName());
    }

    @Test (expected=IllegalArgumentException.class)
    public void getResultsWithNullLevelFails() {
        CalculationResults results = new CalculationResults();
        results.getResults(null);
    }

    @Test
    public void resultsAreSortedByLevel() {
        CalculationResults results = new CalculationResults();
        results.addResult(new DoubleValueResult("Test1", 1, "m"), Level.FINAL);
        results.addResult(new DoubleValueResult("Test2", 2, "m"), Level.FINAL);
        results.addResult(new DoubleValueResult("Test3", 3, "m"), Level.INTERMEDIATE_IMPORTANT);
        assertEquals(2, results.getResults(Level.FINAL).size());
        assertEquals(1, results.getResults(Level.INTERMEDIATE_IMPORTANT).size());
        assertEquals(0, results.getResults(Level.INTERMEDIATE_UNIMPORTANT).size());
        assertEquals(0, results.getResults(Level.DEBUG).size());
        assertTrue(results.getResults(Level.FINAL).containsKey("Test1"));
        assertTrue(results.getResults(Level.FINAL).containsKey("Test2"));
        assertTrue(results.getResults(Level.INTERMEDIATE_IMPORTANT).containsKey("Test3"));
    }

    @Test
    public void datasetValueAddResultCreatesResult() {
        CalculationResults results = new CalculationResults();
        results.addResult("Test", 1., 12., "xUnit", "yUnit", Level.FINAL);
        Result result = results.getResultByName("Test");
        assertNotNull(result);
        assertTrue(result instanceof DoubleDatasetResult);
        assertEquals(12., ((DoubleDatasetResult) result).getValues().get(1.), Double.MIN_VALUE);
    }

}