/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.datamodel;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.TreeMap;
import static org.mockito.Mockito.*;

/**
 * This class is a tool which creates Mockito mock classes for the different
 * types of datasets. It can be used instead of setting up each time a new mock
 * class.
 *
 * @author Nikolaos Apostolakos
 */
public class DatasetMockCreator {
    
    public static Dataset createFwhm() {
        return createMock(Dataset.Type.FWHM, "fwhm", "test_instrument", "\u00C5", "arcsec", "data/fwhm", Dataset.DataType.FUNCTION, null);
    }
    
    public static Dataset createSpectralDistribution() {
        return createMock(Dataset.Type.SPECTRAL_DIST_TEMPLATE, "spectral_distribution", "test_source", "\u00C5", null, "data/spectral_distribution", Dataset.DataType.FUNCTION, null);
    }
    
    public static Dataset createTotalEfficiency() {
        return createMock(Dataset.Type.TRANSMISSION, "total_efficiency", "test_instrument", "\u00C5", null, "data/total_efficiency", Dataset.DataType.FUNCTION, null);
    }
    
    public static Dataset createSkyAbsorption() {
        return createMock(Dataset.Type.SKY_ABSORPTION, "sky_absorption", "test_site", "\u00C5", null, "data/sky_absorption", Dataset.DataType.FUNCTION, null);
    }
    
    public static Dataset createSkyExtinction() {
        return createMock(Dataset.Type.SKY_EXTINCTION, "sky_extinction", "test_site", "\u00C5", null, "data/sky_extinction", Dataset.DataType.FUNCTION, null);
    }
    
    public static Dataset createFilterTransmission() {
        return createMock(Dataset.Type.FILTER_TRANSMISSION, "filter_transmission", "test_instrument", "\u00C5", null, "data/filter_transmission", Dataset.DataType.FUNCTION, null);
    }
    
    public static Dataset createSpectralResolution() {
        return createMock(Dataset.Type.SPECTRAL_RESOLUTION, "spectral_resolution", "test_instrument", "\u00C5", "\u00C5", "data/spectral_resolution", Dataset.DataType.FUNCTION, null);
    }
    
    public static Dataset createSkyEmission() {
        return createMock(Dataset.Type.SKY_EMISSION, "sky_emission", "test_site", "\u00C5", null, "data/sky_emission", Dataset.DataType.TEMPLATE, "Darkest");
    }
    
    public static Dataset createGalactic() {
        return createMock(Dataset.Type.GALACTIC, "galactic", "test_site", "\u00C5", null, "data/galactic", Dataset.DataType.FUNCTION, null);
    }
    
    public static Dataset createZodiacal() {
        return createMock(Dataset.Type.ZODIACAL, "zodiacal", "test_site", "\u00C5", null, "data/zodiacal", Dataset.DataType.FUNCTION, null);
    }
    
    public static Dataset createMock(Dataset.Type type, String name, String namespace, String xUnit, String yUnit
            , String dataResource, Dataset.DataType dataType, String option) {
        Dataset dataset = mock(Dataset.class);
        when(dataset.getType()).thenReturn(type);
        when(dataset.getInfo()).thenReturn(new DatasetInfo(name, namespace, null));
        when(dataset.getXUnit()).thenReturn(xUnit);
        when(dataset.getYUnit()).thenReturn(yUnit);
        when(dataset.getDataType()).thenReturn(dataType);
        when(dataset.getOption()).thenReturn(option);
        
        BufferedReader in = new BufferedReader(new InputStreamReader(Thread.currentThread().getContextClassLoader().getResourceAsStream(dataResource)));
        Map<Double, Double> data = new TreeMap<Double, Double>();
        String line;
        try {
            while ((line = in.readLine()) != null) {
                String[] lineData = line.split("\\s");
                data.put(Double.valueOf(lineData[0]), Double.valueOf(lineData[1]));
            }
        } catch (IOException ex) {
            throw new RuntimeException("Failed to get mock dataset data from " + dataResource);
        }
        when(dataset.getData()).thenReturn(data);
        
        return dataset;
    }
    
}
