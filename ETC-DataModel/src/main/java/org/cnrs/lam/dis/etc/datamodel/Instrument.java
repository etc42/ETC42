/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.datamodel;

/**
 *
 * @author Nikolaos Apostolakos
 */
public interface Instrument {

    public enum InstrumentType {
        SPECTROGRAPH, IMAGING
    }

    public enum SpectrographType {
        SLICER, SLIT, SLITLESS, FIBER
    }

    public enum PsfType {
        AUTO, PROFILE, DOUBLE_GAUSSIAN, AO
    }
    
    public enum SpectralResolutionType {
        CONSTANT_DELTA_LAMBDA, SPECTRAL_RESOLUTION_FUNCTION
    }
    
    public enum PsfSizeType {
        AUTO, FIXED, FUNCTION, PERCENTAGE
    }

    public PsfType getPsfType();

    public void setPsfType(PsfType type);

    public InstrumentType getInstrumentType();

    public void setInstrumentType(InstrumentType type);

    public ComponentInfo getInfo();

    public SpectrographType getSpectrographType();

    public void setSpectrographType(SpectrographType type);

    public double getDiameter();

    public void setDiameter(double diameter);

    public String getDiameterUnit();

    public double getObstruction();

    public void setObstruction(double obstruction);

    public double getReadout();

    public void setReadout(double readout);

    public String getReadoutUnit();

    public double getDark();

    public void setDark(double dark);

    public String getDarkUnit();

    public double getPixelScale();

    public void setPixelScale(double pixelScale);

    public String getPixelScaleUnit();

    public DatasetInfo getFwhm();

    public void setFwhm(DatasetInfo fwhm);

    public DatasetInfo getTransmission();

    public void setTransmission(DatasetInfo transmission);

    public double getRangeMin();

    public void setRangeMin(double rangeMin);

    public String getRangeMinUnit();

    public double getRangeMax();

    public void setRangeMax(double rangeMax);

    public String getRangeMaxUnit();

    public double getSlitLength();

    public void setSlitLength(double slitLength);

    public String getSlitLengthUnit();

    public double getSlitWidth();

    public void setSlitWidth(double slitWidth);

    public String getSlitWidthUnit();

    public int getNSlit();

    public void setNSlit(int nSlit);

    public double getFiberDiameter();

    public void setFiberDiameter(double fiberDiameter);

    public String getFiberDiameterUnit();

    public DatasetInfo getSpectralResolution();

    public void setSpectralResolution(DatasetInfo spectralResolution);

    public DatasetInfo getFilterTransmission();

    public void setFilterTransmission(DatasetInfo filterTransmission);
    
    public double getStrehlRatio();

    public void setStrehlRatio(double strehlRatio);
    
    public double getRefWavelength();

    public void setRefWavelength(double refWavelength);

    public String getRefWavelengthUnit();
    
    public double getDeltaLambdaPerPixel();
    
    public void setDeltaLambdaPerPixel(double deltaLambda);
    
    public String getDeltaLambdaPerPixelUnit();
    
    public SpectralResolutionType getSpectralResolutionType();
    
    public void setSpectralResolutionType(SpectralResolutionType type);
    
    public double getPsfDoubleGaussianMultiplier();
    
    public void setPsfDoubleGaussianMultiplier(double multiplier);
    
    public DatasetInfo getPsfDoubleGaussianFwhm1();
    
    public void setPsfDoubleGaussianFwhm1(DatasetInfo fwhm);
    
    public DatasetInfo getPsfDoubleGaussianFwhm2();
    
    public void setPsfDoubleGaussianFwhm2(DatasetInfo fwhm);
    
    public PsfSizeType getPsfSizeType();
    
    public void setPsfSizeType(PsfSizeType type);
    
    public double getFixedPsfSize();
    
    public void setFixedPsfSize(double size);
    
    public String getFixedPsfSizeUnit();
    
    public DatasetInfo getPsfSizeFunction();
    
    public void setPsfSizeFunction(DatasetInfo function);
    
    public double getPsfSizePercentage();
    
    public void setPsfSizePercentage(double percentage);
    
}
