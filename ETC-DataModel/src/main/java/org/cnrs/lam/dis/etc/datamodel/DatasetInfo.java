/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.datamodel;

import java.util.List;

/**
 * <p>The <code>DatasetInfo</code> class is designed for transferring limited
 * information of datasets between the different parts of the system. Its
 * purpose is to be used when only limited information of the dataset is needed
 * and especially when the retrieval of this information can happen without
 * fully loading the dataset. The information it provides is the datasets name,
 * namespace and description, as well as a list of the different available options
 * of the dataset, for the case of a multi-dataset. The
 * class implements the equals and hashCode methods, so it is safe to be used
 * for keys in maps and as items in combo boxes. Two DatasetInfo instances
 * are considered the same if they have the same name and namespace.</p>
 */
public final class DatasetInfo {

    private String name;
    private String namespace;
    private String description;
    private List<String> optionList;

    /**
     * Constructs a DatasetInfo with the given name and namespace. If the optionList
     * parameter is null, then it represents a single dataset, otherwise it represents
     * a multi-dataset.
     */
    public DatasetInfo(String name, String namespace, String description, List<String> optionList) {
        this.name = name;
        this.namespace = namespace;
        this.description = description;
        this.optionList = optionList;
    }
    
    /**
     * Constructs a DatasetInfo with the given name and namespace, which represents
     * a single dataset (not multi-dataset).
     */
    public DatasetInfo(String name, String namespace, String description) {
        this(name, namespace, description, null);
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

    public String getNamespace() {
        return namespace;
    }

    /**
     * This method returns a list with all the available options of the dataset
     * in the case of a multi-dataset, or null if the dataset is not a multi-dataset.
     * @return The available options for the dataset
     */
    public List<String> getOptionList() {
        return optionList;
    }

    @Override
    public String toString() {
        return (namespace == null ? "" : namespace + ".") + name;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DatasetInfo other = (DatasetInfo) obj;
        if ((this.name == null) ? (other.name != null) : !this.name.equals(other.name)) {
            return false;
        }
        if ((this.namespace == null) ? (other.namespace != null) : !this.namespace.equals(other.namespace)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + (this.name != null ? this.name.hashCode() : 0);
        hash = 59 * hash + (this.namespace != null ? this.namespace.hashCode() : 0);
        return hash;
    }
}
