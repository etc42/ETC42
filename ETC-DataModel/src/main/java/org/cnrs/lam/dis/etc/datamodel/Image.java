/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.datamodel;

import java.io.File;
import java.io.IOException;
import nom.tam.fits.BasicHDU;
import nom.tam.fits.Fits;
import nom.tam.fits.FitsException;
import nom.tam.fits.FitsFactory;
import nom.tam.util.BufferedFile;

/**
 * This class represents an image, stored in a per pixel value way.
 *
 * @author Nikolaos Apostolakos
 */
public class Image <T extends Number> {

    private T[][] data;
    private T maxValue;
    private long activePixelCount;

    /**
     * Constructs a new image representing the given data. Note that the data
     * are NOT copied, so any modifications of the original array will be
     * reflected on the Image object. This is done to save memory and computational
     * power, as astronomical images might be quite big.
     * @param data The data to create the image from, representing the pixel values
     */
    public Image(T[][] data) {
        this.data = data;
        activePixelCount = 0;
        for (int x = 0; x < data.length; x++) {
            for (int y = 0; y < data[0].length; y++) {
                if (maxValue == null || data[x][y].doubleValue() > maxValue.doubleValue())
                    maxValue = data[x][y];
                if (data[x][y].doubleValue() != 0)
                    activePixelCount++;
            }
        }
    }
    
    public long getActivePixelCount() {
        return activePixelCount;
    }

    public T getMaxValue() {
        return maxValue;
    }

    /**
     * Returns the width of the image.
     * @return The width of the image
     */
    public int getWidth() {
        return data.length;
    }

    /**
     * Returns the height of the image.
     * @return The height of the image
     */
    public int getHeight() {
        return data[0].length;
    }

    /**
     * Returns the value of a specific pixel of the image.
     * @param x The x dimention of the pixel
     * @param y The y dimention of the pixel
     * @return The value of the pixel
     */
    public T getData(int x, int y) {
        return data[x][y];
    }
    
    public void exportToFits(File fitsFile) throws IOException, FitsException {
        // We create the file if it doesn't exist. like this we get an exception early
        if (!fitsFile.exists()) {
            fitsFile.getParentFile().mkdirs();
            fitsFile.createNewFile();
        }
        // We copy the values of the image in a double array
        double[][] fitsData = new double[data[0].length][data.length];
        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data[0].length; j++) {
                fitsData[j][i] = data[i][j].doubleValue();
            }
        }
        
        Fits f = new Fits();
        BasicHDU hdu = FitsFactory.HDUFactory(fitsData);
        f.addHDU(hdu);
        BufferedFile bf = new BufferedFile(fitsFile, "rw");
        f.write(bf);
        bf.close();
    }
}
