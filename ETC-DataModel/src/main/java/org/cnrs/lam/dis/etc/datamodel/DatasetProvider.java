/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.datamodel;

import java.util.List;

/**
 * This class provides the means of retrieving the data of a specific dataset.
 * Its purpose is to be used from the different parts of the system (like
 * calculator, import/export engine, etc) by hiding the implementation of the
 * data storage. It is placed in the datamodel package as it can be considered
 * as a datamodel tool.
 */
public interface DatasetProvider {

    /**
     * This method should return a Dataset object containing the data of the
     * dataset described by the given datasetInfo. If there is no such dataset
     * available the method returns null. Note that this method can be used only
     * for datasets which are not options of a multi-dataset.
     * @param type The type of the dataset to retrieve
     * @param datasetInfo The name and namespace of the dataset to retrieve
     * @return The data of the dataset or null if it is not available
     */
    public Dataset getDataset(Dataset.Type type, DatasetInfo datasetInfo);
    
    /**
     * This method should return a Dataset object containing the data of the
     * dataset described by the given datasetInfo and option. If the option is
     * null, then the datasetInfo should represent a dataset that is not a
     * multi-dataset. If it is not null, it defines which option of the multi-dataset
     * will be returned.
     * @param type The type of the dataset to retrieve
     * @param datasetInfo The name and namespace of the dataset to retrieve
     * @param option The specific option of a multi-dataset to retrieve
     * @return The data of the dataset, or null if it is not available
     */
    public Dataset getDataset(Dataset.Type type, DatasetInfo datasetInfo, String option);
    
    /**
     * This method should return a list with the different options available for the
     * given dataset info. If the dataset info represents a single dataset, it returns null.
     * @param type The type of the dataset
     * @param datasetInfo The name and namespace of the dataset
     * @return A list with the dataset options for multi-dataset or null for single
     * datasets
     */
    public List<String> getDatasetOptionList(Dataset.Type type, DatasetInfo datasetInfo);
    
}
