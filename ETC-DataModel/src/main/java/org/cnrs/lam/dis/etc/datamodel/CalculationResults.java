/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.datamodel;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * <p>The {@code CalculationResults} class is used to move around calculation
 * results. It can keep different types of results, defined by the subclasses of
 * the {@code Result} class, which are devided in levels (enumeration
 * {@code Level}). The levels define the nature and the importance of the
 * result and can be used in the presentation layer for showing/hiding results.
 * A typical usage of this class is to create an instance and then use the
 * {@code addResult()} method to add results. Results can be constructed
 * by using their constructors. For example:</p>
 * <p><code>
 * CalculationResults results = new CalculationResults();<br/>
 * results.addResult(new CalculationResults.DoubleValueResult("Test", 14.2, "m"), CalculationResults.Level.FINAL);<br/>
 * print(results.getResultByName("Test"));<br/>
 * for (CalculationResults.Result result : results.getResults(CalculationResults.Level.FINAL))<br/>
 * &nbsp&nbsp print(result);<br/>
 * </code></p>
 * <p>When using the addResult, if there is already a result with the same
 * level and name it is going to be replaced. This can be the case either if
 * a method inserting the result is called more than once during the calculation
 * or if a recursive algorithm is used for the calculation of the result and
 * its value is replaced with a more accurate one.</p>
 * <p>A second way to add results is the variation of the method {@code addResult}
 * which is designed for dataset results for which their values are not
 * calculated at the same method call, so the {@code Result} constructor cannot
 * be used. For example:</p>
 * <p><code>
 * CalculationResults results = new CalculationResults();<br/>
 * results.addResult("Test", 1, 34.2, "s", "m", CalculationResults.Level.FINAL);<br/>
 * results.addResult("Test", 2, 45.6, "s", "m", CalculationResults.Level.FINAL);<br/>
 * results.addResult("Test", 2, 62.3, "s", "m", CalculationResults.Level.FINAL);<br/>
 * print(results.getResultByName("Test"));<br/>
 * </code></p>
 * <p>This method will create a dataset result with the given name and level if
 * there is not one existing already. If there is one, then it will set the
 * value for the given (x,y) pair for the existing dataset (or replace it if
 * it is already set).</p>
 * <p>For the possible result types look the documentation of the <code>Result</code>
 * class and its subclasses.</p>
 *
 * @author Nikolaos Apostolakos
 */
public final class CalculationResults {

    /** Defines the level of a calculation result.*/
    public enum Level {
        /** Results of type {@code FINAL} are considered the output of the
         * calculation and should always be presented to the user.*/
        FINAL,
        /** Results of type {@code INTERMEDIATE_IMPORTANT} are results of
         * intermediate calculations, for which the user will most probably be
         * interested of.*/
        INTERMEDIATE_IMPORTANT,
        /** Results of type {@code INTERMEDIATE_IMPORTANT} are results of
         * intermediate calculations, for which the user will most probably not
         * be so interested of.*/
        INTERMEDIATE_UNIMPORTANT,
        /** Results of type {@code DEBUG} are results of intermediate
         * calculations, for which the user will be interested only if he wants
         * to validate the final results.*/
        DEBUG
    }

    /** The collection where all the results are kept. For each level there is
     * one map containing the pairs name->result. These maps are the values of
     * another map, which uses the related level as a key.*/
    private final Map<Level,Map<String, Result>> resultsMap;

    /**
     * Creates a new instance of {@code CalculationResults}.
     */
    public CalculationResults() {
        resultsMap = new HashMap<Level, Map<String, Result>>();
        for (Level level : Level.values()) {
            resultsMap.put(level, new HashMap<String, Result>());
        }
    }

    /**
     * Returns a result by its name. The method searches for results of any
     * level with this name and if it finds it it returns it. Otherwise it
     * returns null.
     * @param name The name of the result
     * @return The result if it exists, null otherwise
     */
    public Result getResultByName(String name) {
        Result result = null;
        for (Level level : Level.values()) {
            result = resultsMap.get(level).get(name);
            if (result != null)
                break;
        }
        return result;
    }

    /**
     * Returns a map containing all the results for a specific level. The returned
     * map is for "read only" and any attempt to modify it will result to an
     * {@code UnsupportedOperationException}.
     * @param level The level for which the results will be retrieved
     * @return The results for the specified level
     */
    public Map<String, Result> getResults(Level level) {
        if (level == null) {
            throw new IllegalArgumentException("The level argument cannot be null");
        }
        return Collections.unmodifiableMap(resultsMap.get(level));
    }

    /**
     * Adds the given result in the calculation results, with the specified level.
     * There can never be two results with the same name in any level, so if a
     * result with the same name already exists the old one is replaced. If the
     * level of the new one is different the level of the new result is used.
     * @param result The result to add
     * @param level The level of the result
     */
    public void addResult(Result result, Level level) {
        if (result == null) {
            throw new IllegalArgumentException("The result argument cannot be null");
        }
        if (level == null) {
            throw new IllegalArgumentException("The level argument cannot be null");
        }
        removeResultByName(result.getName());
        resultsMap.get(level).put(result.getName(), result);
    }

    /**
     * Removes (if it exists) a result from any level.
     * @param name The result to remove
     */
    private void removeResultByName(String name) {
        for (Level level : Level.values()) {
            resultsMap.get(level).remove(name);
        }
    }

    /**
     * Adds the given pair (x,y) to the dataset result with name {@code name}.
     * If there is no such result a new one is created. If the dataset exists
     * and it already contains a value for the given x, the old one is replaced.
     * @param name The name of the result
     * @param x The x value
     * @param y The y value
     * @param xUnit The unit in which the x value is expressed
     * @param yUnit The unit in which the y value is expressed
     * @param level The level of the result
     */
    public void addResult(String name, Double x, Double y, String xUnit, String yUnit, Level level) {
        Result result = getResultByName(name);
        if (result == null) {
            Map<Double, Double> values = new TreeMap<Double, Double>();
            values.put(x, y);
            result = new DoubleDatasetResult(name, values, xUnit, yUnit);
            addResult(result, level);
        } else {
            if (result instanceof DoubleDatasetResult) {
                DoubleDatasetResult doubleDatasetResult = (DoubleDatasetResult) result;
                if (xUnit == null ? doubleDatasetResult.getXUnit() != null : !xUnit.equals(doubleDatasetResult.getXUnit())) {
                    throw new IllegalArgumentException("X unit should be " + doubleDatasetResult.getXUnit() +
                            " but was " + xUnit);
                }
                if (yUnit == null ? doubleDatasetResult.getYUnit() != null : !yUnit.equals(doubleDatasetResult.getYUnit())) {
                    throw new IllegalArgumentException("Y unit should be " + doubleDatasetResult.getYUnit() +
                            " but was " + yUnit);
                }
                doubleDatasetResult.values.put(x, y);
            } else {
                throw new IllegalArgumentException("Result with name " + name + " already " +
                        "exists and it is not a double dataset.");
            }
        }
    }

    public void addResult(String name, Double key, Image<? extends Number> image, String keyUnit, Level level) {
        Result result = getResultByName(name);
        if (result == null) {
            Map<Double, Image> imageMap = new TreeMap<Double, Image>();
            imageMap.put(key, image);
            result = new ImageSetResult(name, imageMap, keyUnit);
            addResult(result, level);
        } else {
            if (result instanceof ImageSetResult) {
                ImageSetResult imageSetResult = (ImageSetResult) result;
                if (keyUnit == null ? imageSetResult.getKeyUnit() != null : !keyUnit.equals(imageSetResult.getKeyUnit())) {
                    throw new IllegalArgumentException("Key unit should be " + imageSetResult.getKeyUnit() +
                            " but was " + keyUnit);
                }
                imageSetResult.imageMap.put(key, image);
            } else {
                throw new IllegalArgumentException("Result with name " + name + " already " +
                        "exists and it is not an image set.");
            }
        }
    }

    public void addResult(String name, Double key, DoubleDatasetResult dataset, String keyUnit, Level level) {
        Result result = getResultByName(name);
        if (result == null) {
            Map<Double, DoubleDatasetResult> datasetMap = new TreeMap<Double, DoubleDatasetResult>();
            datasetMap.put(key, dataset);
            result = new DoubleDatasetSetResult(name, datasetMap, keyUnit);
            addResult(result, level);
        } else {
            if (result instanceof DoubleDatasetSetResult) {
                DoubleDatasetSetResult datasetSetResult = (DoubleDatasetSetResult) result;
                if (keyUnit == null ? datasetSetResult.getKeyUnit() != null : !keyUnit.equals(datasetSetResult.getKeyUnit())) {
                    throw new IllegalArgumentException("Key unit should be " + datasetSetResult.getKeyUnit() +
                            " but was " + keyUnit);
                }
                datasetSetResult.datasetMap.put(key, dataset);
            } else {
                throw new IllegalArgumentException("Result with name " + name + " already " +
                        "exists and it is not an dataset set.");
            }
        }
    }

    /**
     * <p>This is an abrstact representation of a result. It just specifies the
     * name of the result (which works as the identifier of the result). The
     * subclasses of the {@code Result} class are responsible for containing
     * the real information of the result.</p>
     */
    public static abstract class Result {
        private final String name;

        /**
         * Constructs a {@code Result} with the given name. Don't be surprised
         * that the only constructor of an abstract method is private. As this
         * class is a nested class, the visibility is the one of the outer class.
         * This is done to enforce the implementation of all the Result classes
         * to be in the same place.
         * @param name The name of the result
         */
        private Result(String name) {
            if (name == null) {
                throw new IllegalArgumentException("The name argument cannot be null");
            } else if (name.equals("")) {
                throw new IllegalArgumentException("The name argument cannot be empty string");
            }
            this.name = name;
        }

        /**
         * Returns the name of the result.
         * @return The name of the result
         */
        public final String getName() {
            return name;
        }
    }

    public static final class ImageResult<T extends Number> extends Result {
        private final Image<T> image;

        public ImageResult(String name, Image<T> image) {
            super(name);
            this.image = image;
        }

        public Image<T> getImage() {
            return image;
        }
    }

    public static final class ImageSetResult<K extends Number, I extends Number> extends Result {
        private final Map<K, Image<I>> imageMap;
        private String keyUnit;

        public ImageSetResult(String name, Map<K, Image<I>> imageMap, String keyUnit) {
            super(name);
            this.imageMap = imageMap;
            this.keyUnit = keyUnit;
        }

        public Map<K, Image<I>> getImageMap() {
            return imageMap;
        }

        public String getKeyUnit() {
            return keyUnit;
        }
    }

    public static final class StringResult extends Result {
        private final String value;

        public StringResult(String name, String value) {
            super(name);
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    /**
     * The class {@code DoubleValueResult} represents a real number result.
     * It contains information about the arithmetic value of the result and the
     * unit in which it is expressed.<br/>
     * <b>NOTE:</b>As the accurate representation
     * of integers by using floating point arithmetic is not possible this class
     * should NOT be used for results representing integers. In this case the
     * {@code LongValueResult} class should be used.
     */
    public static final class DoubleValueResult extends Result {
        private final double value;
        private final String unit;

        /**
         * Creates an instance of {@code DoubleValueResult}, with the given name,
         * value and unit.
         * @param name
         * @param value
         * @param unit
         */
        public DoubleValueResult(String name, double value, String unit) {
            super(name);
            this.value = value;
            this.unit = unit;
        }

        /**
         * Returns the value of the result.
         * @return The value of the result
         */
        public double getValue() {
            return value;
        }

        /**
         * Returns the unit in which the result is expressed.
         * @return the unit of the result
         */
        public String getUnit() {
            return unit;
        }
    }

    /**
     * The class {@code LongValueResult} represents a whole number result.
     * It contains information about the arithmetic value of the result and the
     * unit in which it is expressed.
     */
    public static final class LongValueResult extends Result {
        private final long value;
        private final String unit;

        /**
         * Creates an instance of {@code LongValueResult}, with the given name,
         * value and unit.
         * @param name
         * @param value
         * @param unit
         */
        public LongValueResult(String name, long value, String unit) {
            super(name);
            this.value = value;
            this.unit = unit;
        }

        /**
         * Returns the value of the result.
         * @return The value of the result
         */
        public long getValue() {
            return value;
        }

        /**
         * Returns the unit in which the result is expressed.
         * @return the unit of the result
         */
        public String getUnit() {
            return unit;
        }
    }

    /**
     * The class {@code DoubleDatasetResult} represents a result consisting of a
     * set of value pairs (x,y). It contains information about the arithmetic values
     * of x and y and the the  units in which they are expressed. Note that each
     * x value can be only in one pair (the y values can be in more than one).
     * this result should be used to return results like spectrums, etc.<br>
     * <b>NOTE:</b>As the accurate representation
     * of integers by using floating point arithmetic is not possible this class
     * should NOT be used for (x,y) pairs representing integers. In this case the
     * {@code LongDatasetResult} and {@code LongDoubleDatasetResult}
     * classes should be used.
     */
    public static final class DoubleDatasetResult extends Result {
        private final Map<Double,Double> values;
        private final String xUnit;
        private final String yUnit;

        /**
         * Constructs a new {@code DoubleDatasetResult} object with the given
         * name, values and units. The passed values are copied, so future modifications
         * of the passed map are not reflected in the result object. The values must
         * contain at least one pair of data, otherwise an exception is thrown.
         * @param name The name of the result
         * @param values The (x,y) pairs in a map where x is the key and y the value
         * @param xUnit The unit in which the x values are expressed
         * @param yUnit The unit in which the y values are expressed
         */
        public DoubleDatasetResult(String name, Map<Double, Double> values, String xUnit, String yUnit) {
        	super(name);

            if (values == null) {
                throw new IllegalArgumentException(name + " : The values argument cannot be null");
            } else if (values.isEmpty()) {
                throw new IllegalArgumentException(name + " : The values argument cannot be empty");
            }
            this.values = new TreeMap<Double, Double>(values);
            this.xUnit = xUnit;
            this.yUnit = yUnit;
        }

        /**
         * Returns a map containing the (x,y) pairs, with x being the key and y
         * the value of the map. The returned map is ordered by the key values.
         * This map is for "read only" usage and any attempt to modify it will
         * result to an exception thrown.
         * @return A map with the (x,y) pairs
         */
        public Map<Double, Double> getValues() {
            return Collections.unmodifiableMap(values);
        }

        /**
         * Returns the units in which the x values are expressed.
         * @return The units in which the x values are expressed
         */
        public String getXUnit() {
            return xUnit;
        }

        /**
         * Returns the units in which the y values are expressed.
         * @return The units in which the y values are expressed
         */
        public String getYUnit() {
            return yUnit;
        }
    }

    public static final class DoubleDatasetSetResult<K extends Number> extends Result {
        private final Map<K, DoubleDatasetResult> datasetMap;
        private String keyUnit;

        public DoubleDatasetSetResult(String name, Map<K, DoubleDatasetResult> datasetMap, String keyUnit) {
            super(name);
            this.datasetMap = datasetMap;
            this.keyUnit = keyUnit;
        }

        public Map<K, DoubleDatasetResult> getDatasetMap() {
            return datasetMap;
        }

        public String getKeyUnit() {
            return keyUnit;
        }
    }

    /**
     * The class {@code LongDatasetResult} represents a result consisting of a
     * set of value pairs (x,y). It contains information about the arithmetic values
     * of x and y and the the  units in which they are expressed. Note that each
     * x value can be only in one pair (the y values can be in more than one).
     */
    public static final class LongDatasetResult extends Result {
        private final Map<Long,Long> values;
        private final String xUnit;
        private final String yUnit;

        /**
         * Constructs a new {@code LongDatasetResult} object with the given
         * name, values and units. The passed values are copied, so future modifications
         * of the passed map are not reflected in the result object. The values must
         * contain at least one pair of data, otherwise an exception is thrown.
         * @param name The name of the result
         * @param values The (x,y) pairs in a map where x is the key and y the value
         * @param xUnit The unit in which the x values are expressed
         * @param yUnit The unit in which the y values are expressed
         */
        public LongDatasetResult(String name, Map<Long, Long> values, String xUnit, String yUnit) {
            super(name);
            if (values == null) {
                throw new IllegalArgumentException(name + " : The values argument cannot be null");
            } else if (values.isEmpty()) {
                throw new IllegalArgumentException(name + " : The values argument cannot be empty");
            }
            this.values = new TreeMap<Long, Long>(values);
            this.xUnit = xUnit;
            this.yUnit = yUnit;
        }

        /**
         * Returns a map containing the (x,y) pairs, with x being the key and y
         * the value of the map. The returned map is ordered by the key values.
         * This map is for "read only" usage and any attempt to modify it will
         * result to an exception thrown.
         * @return A map with the (x,y) pairs
         */
        public Map<Long, Long> getValues() {
            return Collections.unmodifiableMap(values);
        }

        /**
         * Returns the units in which the x values are expressed.
         * @return The units in which the x values are expressed
         */
        public String getXUnit() {
            return xUnit;
        }

        /**
         * Returns the units in which the y values are expressed.
         * @return The units in which the y values are expressed
         */
        public String getYUnit() {
            return yUnit;
        }
    }

    /**
     * The class {@code LongDoubleDatasetResult} represents a result consisting of a
     * set of value pairs (x,y). It contains information about the arithmetic values
     * of x and y and the the  units in which they are expressed. Note that each
     * x value can be only in one pair (the y values can be in more than one).
     */
    public static final class LongDoubleDatasetResult extends Result {
        private final Map<Long,Double> values;
        private final String xUnit;
        private final String yUnit;

        /**
         * Constructs a new {@code LongDoubleDatasetResult} object with the given
         * name, values and units. The passed values are copied, so future modifications
         * of the passed map are not reflected in the result object. The values must
         * contain at least one pair of data, otherwise an exception is thrown.
         * @param name The name of the result
         * @param values The (x,y) pairs in a map where x is the key and y the value
         * @param xUnit The unit in which the x values are expressed
         * @param yUnit The unit in which the y values are expressed
         */
        public LongDoubleDatasetResult(String name, Map<Long, Double> values, String xUnit, String yUnit) {
            super(name);
            if (values == null) {
                throw new IllegalArgumentException(name + " : The values argument cannot be null");
            } else if (values.isEmpty()) {
                throw new IllegalArgumentException(name + " : The values argument cannot be empty");
            }
            this.values = new TreeMap<Long, Double>(values);
            this.xUnit = xUnit;
            this.yUnit = yUnit;
        }

        /**
         * Returns a map containing the (x,y) pairs, with x being the key and y
         * the value of the map. The returned map is ordered by the key values.
         * This map is for "read only" usage and any attempt to modify it will
         * result to an exception thrown.
         * @return A map with the (x,y) pairs
         */
        public Map<Long, Double> getValues() {
            return Collections.unmodifiableMap(values);
        }

        /**
         * Returns the units in which the x values are expressed.
         * @return The units in which the x values are expressed
         */
        public String getXUnit() {
            return xUnit;
        }

        /**
         * Returns the units in which the y values are expressed.
         * @return The units in which the y values are expressed
         */
        public String getYUnit() {
            return yUnit;
        }
    }
}
