/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.datamodel;

/**
 *
 * @author Nikolaos Apostolakos
 */
public interface ObsParam {

    public enum TimeSampleType {
        DIT, NO_EXPO
    }

    public enum FixedParameter {
        EXPOSURE_TIME, SNR
    }
    
    public enum FixedSnrType {
        TOTAL, CENTRAL_PIXEL
    }
    
    public enum SpectralQuantumType {
        SPECTRAL_PIXEL, SPECTRAL_RESOLUTION_ELEMENT
    }
    
    public enum ExtraBackgroundNoiseType {
        ONLY_CALCULATED_BACKGROUND_NOISE, ONLY_EXTRA_BACKGROUND_NOISE, CALCULATED_AND_EXTRA_BACKGROUND_NOISE
    }
    
    public enum ExtraSignalType {
        ONLY_CALCULATED_SIGNAL, ONLY_EXTRA_SIGNAL, CALCULATED_AND_EXTRA_SIGNAL
    }

    public ComponentInfo getInfo();

    public TimeSampleType getTimeSampleType();

    public void setTimeSampleType(TimeSampleType type);

    public double getDit();

    public void setDit(double dit);

    public String getDitUnit();

    public int getNoExpo();

    public void setNoExpo(int noExpo);

    public FixedParameter getFixedParameter();

    public void setFixedParameter(FixedParameter parameter);

    public double getExposureTime();

    public void setExposureTime(double time);

    public String getExposureTimeUnit();

    public double getSnr();

    public void setSnr(double snr);

    public String getSnrUnit();

    public double getSnrLambda();

    public void setSnrLambda(double lambda);

    public String getSnrLambdaUnit();
    
    public SpectralQuantumType getSpectralQuantumType();
    
    public void setSpectralQuantumType(SpectralQuantumType type);
    
    public ExtraBackgroundNoiseType getExtraBackgroundNoiseType();
    
    public void setExtraBackgroundNoiseType(ExtraBackgroundNoiseType type);
    
    public ExtraSignalType getExtraSignalType();
    
    public void setExtraSignalType(ExtraSignalType type);
    
    public DatasetInfo getExtraBackgroundNoiseDataset();
    
    public void setExtraBackgroundNoiseDataset(DatasetInfo extraBackgroundNoise);
    
    public double getExtraBackgroundNoise();
    
    public void setExtraBackgroundNoise(double extraBackgroundNoise);
    
    public String getExtraBackgroundNoiseUnit();
    
    public DatasetInfo getExtraSignalDataset();
    
    public void setExtraSignalDataset(DatasetInfo extraSignal);
    
    public double getExtraSignal();
    
    public void setExtraSignal(double extraSignal);
    
    public String getExtraSignalUnit();
    
    public FixedSnrType getFixedSnrType();
    
    public void setFixedSnrType(FixedSnrType type);
}
