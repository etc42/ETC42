/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.datamodel;

import java.util.Map;

/**
 *
 * @author Nikolaos Apostolakos
 */
public interface Dataset {
    
    public enum Type {
        TRANSMISSION(ComponentType.INSTRUMENT, false, new DataType[] {DataType.FUNCTION}),
        FWHM(ComponentType.INSTRUMENT, false, new DataType[] {DataType.FUNCTION}),
        // DO NOT ADD ANY OTHER DATA TYPES AT THE SPECTRAL RESOLUTION FOR ANY
        // REASON!!!! The spectral resolution dataset is used from the
        // Histogram implementation, so you will end up in a infinite loop
        SPECTRAL_RESOLUTION(ComponentType.INSTRUMENT, false, new DataType[] {DataType.FUNCTION}),
        SKY_EMISSION(ComponentType.SITE, true, new DataType[] {DataType.TEMPLATE}),
        SKY_ABSORPTION(ComponentType.SITE, false, new DataType[] {DataType.FUNCTION}),
        SKY_EXTINCTION(ComponentType.SITE, false, new DataType[] {DataType.FUNCTION}),
        SPECTRAL_DIST_TEMPLATE(ComponentType.SOURCE, false, new DataType[] {DataType.FUNCTION, DataType.TEMPLATE}),
        FILTER_TRANSMISSION(ComponentType.INSTRUMENT, false, new DataType[] {DataType.FUNCTION}),
        GALACTIC(ComponentType.SITE, false, new DataType[] {DataType.FUNCTION, DataType.TEMPLATE}),
        ZODIACAL(ComponentType.SITE, false, new DataType[] {DataType.FUNCTION, DataType.TEMPLATE}),
        EXTRA_BACKGROUND_NOISE(ComponentType.OBS_PARAM, false, new DataType[] {DataType.FUNCTION, DataType.TEMPLATE}),
        EXTRA_SIGNAL(ComponentType.OBS_PARAM, false, new DataType[] {DataType.FUNCTION, DataType.TEMPLATE}),
        PSF_DOUBLE_GAUSSIAN_FWHM_1(ComponentType.INSTRUMENT, false, new DataType[] {DataType.FUNCTION}),
        PSF_DOUBLE_GAUSSIAN_FWHM_2(ComponentType.INSTRUMENT, false, new DataType[] {DataType.FUNCTION}),
        PSF_SIZE_FUNCTION(ComponentType.INSTRUMENT, false, new DataType[] {DataType.FUNCTION}),
        ATMOSPHERIC_TRANSMISSION(ComponentType.SITE, false, new DataType[] {DataType.FUNCTION});
        
        
        private ComponentType componentType;
        private boolean multiDataset;
        private DataType[] possibleDataTypes;

        private Type(ComponentType componentType, boolean multiDataset, DataType[] possibleDataTypes) {
            this.componentType = componentType;
            this.multiDataset = multiDataset;
            this.possibleDataTypes = possibleDataTypes;
        }

        public ComponentType getComponentType() {
            return componentType;
        }

        public boolean isMultiDataset() {
            return multiDataset;
        }
        
        public DataType[] getPossibleDataTypes() {
            return possibleDataTypes;
        }

        @Override
        public String toString() {
            return java.util.ResourceBundle.getBundle("org.cnrs.lam.dis.etc.datamodel.messages").getString(name());
        }
    }
    
    public enum DataType {
        FUNCTION, TEMPLATE, EMISSION_LINES
    }

    public DatasetInfo getInfo();

    public Type getType();

    public Map<Double, Double> getData();

    public String getXUnit();

    public String getYUnit();
    
    /**
     * Returns the option of a multi-dataset that this dataset represents. If it
     * represents a single (non-multi) dataset, it returns null.
     */
    public String getOption();
    
    /**
     * Returns the data type of the dataset.
     */
    public DataType getDataType();
    
}
