/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;
import org.astrogrid.samp.client.SampException;
import org.cnrs.lam.dis.etc.dataimportexport.SampController;
import org.cnrs.lam.dis.etc.ui.Messenger;
import org.cnrs.lam.dis.etc.ui.SampListener;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class SampUiListener implements SampListener {

    private static Logger logger = Logger.getLogger(SampUiListener.class);
    private static ResourceBundle bundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/controller/messages");
    private SampController sampController;
    private Messenger messenger;

    public SampUiListener(SampController sampController, Messenger messenger) {
        this.sampController = sampController;
        this.messenger = messenger;
    }
    
    @Override
    public void connectToLauncher() {
    	try {
            sampController.connect();
        } catch (SampException ex) {
            logger.error("Failed to connect to Launcher SAMP hub", ex);
        }
    }

    @Override
    public void connect() {
        // If there is no hub, ask the user if he wants to create one. If not we return
        if (!sampController.hubExists()) {
            String message = bundle.getString("CREATE_SAMP_HUB_MESSAGE");
            List<String> list = new ArrayList<String>(2);
            list.add(bundle.getString("YES_OPTION"));
            list.add(bundle.getString("NO_OPTION"));
            String decision = messenger.decision(message, list);
            if (decision == null || decision.equals(list.get(1)))
                return;
            sampController.createHub();
        }
        try {
            sampController.connect();
        } catch (SampException ex) {
            logger.error("Failed to connect to SAMP hub", ex);
            messenger.error(bundle.getString("SAMP_CONNECTION_FAILED"));
        }
    }

    @Override
    public void disconnect() {
        try {
            sampController.disconnect();
        } catch (SampException ex) {
            logger.error("Failed to disconnect to SAMP hub", ex);
            messenger.error(bundle.getString("SAMP_DISCONNECTION_FAILED"));
        }
    }

    @Override
    public void sendAsVoTable(String description, Map<? extends Number, ? extends Number> data, String xUnit, String yUnit) {
        List<String> voTableClientList = sampController.getVoTableClients();
        if (voTableClientList.isEmpty()) {
            messenger.info(bundle.getString("SAMP_NO_AVAILABLE_VO_TABLE_CLIENTS"));
            return;
        }
        String client = messenger.requestInput(bundle.getString("SAMP_SELECT_VO_TABLE_CLIENT"), voTableClientList);
        if (client == null)
            return;
        try {
            sampController.sendVoTable(client, description, data, xUnit, yUnit);
        } catch (SampException ex) {
            logger.error("Failed to send VO table. Reason: " + ex.getMessage(), ex);
            messenger.error(bundle.getString("SAMP_SEND_VO_TABLE_FAILURE"));
        }
    }

}
