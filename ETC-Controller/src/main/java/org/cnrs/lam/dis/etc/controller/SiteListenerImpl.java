/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.controller;

import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;
import org.cnrs.lam.dis.etc.dataimportexport.DataExporter;
import org.cnrs.lam.dis.etc.dataimportexport.DataImporter;
import org.cnrs.lam.dis.etc.dataimportexport.SiteWithDatasets;
import org.cnrs.lam.dis.etc.dataimportexport.WrongFormatException;
import org.cnrs.lam.dis.etc.datamodel.ComponentInfo;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.cnrs.lam.dis.etc.datamodel.DatasetProvider;
import org.cnrs.lam.dis.etc.datamodel.Site;
import org.cnrs.lam.dis.etc.persistence.DatasetManager;
import org.cnrs.lam.dis.etc.persistence.InUseException;
import org.cnrs.lam.dis.etc.persistence.NameExistsException;
import org.cnrs.lam.dis.etc.persistence.NameNotFoundException;
import org.cnrs.lam.dis.etc.persistence.PersistenceDeviceException;
import org.cnrs.lam.dis.etc.persistence.PersistenceFactory;
import org.cnrs.lam.dis.etc.persistence.SessionManager;
import org.cnrs.lam.dis.etc.ui.Messenger;
import org.cnrs.lam.dis.etc.ui.SiteListener;
import org.cnrs.lam.dis.etc.ui.UIManager;

/**
 *
 * @author Nikolaos Apostolakos
 */
final class SiteListenerImpl implements SiteListener {

    /** The bundle for retrieving the i18n strings for the messages.*/
    private static ResourceBundle bundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/controller/messages");
    /** The logger of the class.*/
    private static Logger logger = Logger.getLogger(SiteListenerImpl.class.getName());

    private SessionManager sessionManager;
    private DatasetManager datasetManager;
    private Messenger messenger;
    private UIManager uiManager;
    private DataExporter dataExporter;
    private DataImporter dataImporter;
    private DatasetProvider datasetProvider;

    public SiteListenerImpl(SessionManager sessionManager, DatasetManager datasetManager, Messenger messenger,
            UIManager uiManager, DataExporter dataExporter, DataImporter dataImporter,
            DatasetProvider datasetProvider) {
        this.sessionManager = sessionManager;
        this.datasetManager = datasetManager;
        this.messenger = messenger;
        this.uiManager = uiManager;
        this.dataExporter = dataExporter;
        this.dataImporter = dataImporter;
        this.datasetProvider = datasetProvider;
    }

    @Override
    public void createNewSite(ComponentInfo newSiteInfo) {
        //First check if the current site has been modified and if it is
        // ask the user what he wants to do and do it
        if (isCurrentSiteModified()) {
            if (!handleModifiedCurrentSite()) {
                return;
            }
        }

        // Create the instance of the new site
        Site newSite = getNewSiteFromSessionManager(newSiteInfo);
        if (newSite == null) {
            return;
        }

        // Set the new site as the current one and notify the user interface
        CurrentSessionContainer.getCurrentSession().setSite(newSite);
        uiManager.sessionModified();
    }

    /**
     * Creates a new instance of an site by using the createNewSite()
     * method of the session manager. If the creation fails it shows the necessary
     * messages to the user and it returns null.
     * @param info The name and description of the new site
     * @return The new site instance
     */
    private Site getNewSiteFromSessionManager(ComponentInfo info) {
        Site newSite = null;
        try {
            newSite = sessionManager.createNewSite(info);
        } catch (NameExistsException ex) {
            logger.warn("Did not create new site because there is already " +
                    "an site with name " + info.getName(), ex);
            messenger.error(MessageFormat.format(bundle.getString("CREATE_SITE_NAME_EXISTS"), info.getName()));
        } catch (PersistenceDeviceException ex) {
            logger.warn("Failed to create new site with name " +
                    info.getName() + " because there of an error from the percistence device.", ex);
            messenger.error(MessageFormat.format(bundle.getString("CREATE_SITE_DEVICE_ERROR"), info.getName()));
        }
        return newSite;
    }

    /**
     * This method handles the case when an action will replace the current
     * site, but the current site is modified. It presents to the
     * user a dialog for selecting what he wants to do (save the changes, ignore
     * the changes or cancel the action). If the user selects to save the changes
     * then this method will persist the current site.
     * @return true if the action can continue, false otherwise
     */
    private boolean handleModifiedCurrentSite() {
        boolean canContinue = false;
        // Get the decision of the user
        String message = bundle.getString("CURRENT_SITE_MODIFIED");
        List<String> list = new ArrayList<String>(3);
        list.add(bundle.getString("SAVE_OPTION"));
        list.add(bundle.getString("IGNORE_OPTION"));
        list.add(bundle.getString("CANCEL_OPTION"));
        String decision = messenger.decision(message, list);

        if (decision == null || decision.equals(list.get(2))) {
            // Here we know the user wants to cancel so return false to stop the calling action
            canContinue = false;
        } else if (decision.equals(list.get(1))) {
            // Ignore the modified data. Let the calling action to continue without saving the data
            canContinue = true;
        } else if (decision.equals(list.get(0))) {
            // Save the current data and allow the calling action to continue if successful
            canContinue = persistCurrentSite();
        }
        return canContinue;
    }

    @Override
    public void loadSite(ComponentInfo siteInfo) {
        // First check if the requested site is the current site.
        // If it is do nothing.
        if (siteInfo.getName().equals(CurrentSessionContainer.getCurrentSession().
                getSite().getInfo().getName())) {
            return;
        }
        // Check if the current site has been modified and if it is
        // ask the user what he wants to do and do it
        if (isCurrentSiteModified()) {
            if (!handleModifiedCurrentSite()) {
                return;
            }
        }
        // Load the instance of the site
        Site loadedSite = loadSiteFromSessionManager(siteInfo);
        if (loadedSite == null) {
            return;
        }
        // Set the loaded site as the current one and notify the user interface
        CurrentSessionContainer.getCurrentSession().setSite(loadedSite);
        uiManager.sessionModified();
    }

    /**
     * Loads an instance of an site by using the loadSite()
     * method of the session manager. If the load fails it shows the necessary
     * messages to the user and it returns null.
     * @param info The name of the site to load
     * @return The loaded site instance
     */
    private Site loadSiteFromSessionManager(ComponentInfo info) {
        Site loadedSite = null;
        try {
            loadedSite = sessionManager.loadSite(info);
        } catch (NameNotFoundException ex) {
            logger.warn("Did not load the site because there is no " +
                    "site with name " + info.getName(), ex);
            messenger.error(MessageFormat.format(bundle.getString("LOAD_SITE_NOT_FOUND"), info.getName()));
        } catch (PersistenceDeviceException ex) {
            logger.warn("Failed to load site with name " + info.getName() +
                    " because of an error from the percistence device.", ex);
            messenger.error(MessageFormat.format(bundle.getString("LOAD_SITE_DEVICE_ERROR"), info.getName()));
        }
        return loadedSite;
    }

    @Override
    public void reloadCurrentSite() {
        // Check if the site exist in the database. If not give a message
        if (!sessionManager.isSiteInDatabase(CurrentSessionContainer.getCurrentSession().getSite())) {
            messenger.warn(bundle.getString("RELOAD_SITE_NOT_IN_DB_MESSAGE"));
            return;
        }
        // Check if the site has been modified. If it is not we don't need to do anything
        if (!isCurrentSiteModified()) {
            return;
        }
        // Ask the user if he really wants to do the reload and lose any changes
        if (!getUserConfirmationForReload()) {
            return;
        }
        // Reload the curernt site from the persistence device
        ComponentInfo currentInfo = CurrentSessionContainer.getCurrentSession().getSite().getInfo();
        Site loadedSite = loadSiteFromSessionManager(currentInfo);
        if (loadedSite == null) {
            return;
        }
        // Set the loaded site as the current one and notify the user interface
        CurrentSessionContainer.getCurrentSession().setSite(loadedSite);
        uiManager.sessionModified();
    }

    /**
     * Gets a confirmation from the user to reload the current site.
     * @return true if the user confirms, false otherwise
     */
    private boolean getUserConfirmationForReload() {
        boolean confirmed = false;
        String message = bundle.getString("CONFIRM_SITE_RELOAD");
        List<String> list = new ArrayList<String>(2);
        list.add(bundle.getString("YES_OPTION"));
        list.add(bundle.getString("NO_OPTION"));
        String decision = messenger.decision(message, list);

        if (decision == null || decision.equals(list.get(1))) {
            // Here we know the user wants to cancel so return false to stop the calling action
            confirmed = false;
        } else if (decision.equals(list.get(0))) {
            // Got the confirmation for reloading
            confirmed = true;
        }
        return confirmed;
    }

    /**
     * Handles a user request for saving the current site. If the current
     * site has not been modified (its representation in the persistence
     * device has the same information) nothing is done. Otherwise it persists
     * the site and it updates the representation shown to the user.
     */
    @Override
    public void saveCurrentSite() {
        // If the current site has not been modified we do not need to do anything
        if (!isCurrentSiteModified()) {
            return;
        }

        // Persist the current site. On success notify the user
        // interface to update the user representation for possible changes
        // (like dataset namespaces, etc)
        if (persistCurrentSite()) {
            uiManager.sessionModified();
        }
    }

    /**
     * Tries to persist all the modifications done to the current site.
     * If it succeed it returns true. If there is any problem with the persistence
     * device it shows an error to the user and it returns false.
     * @return true if the current site is persisted successfully, false otherwise
     */
    private boolean persistCurrentSite() {
        // Check if the current site is the "new" site. In this case saving is not allowed
        if (CurrentSessionContainer.getCurrentSession().getSite().getInfo().getName().equals("new")) {
            messenger.warn(bundle.getString("SAVE_NEW_SITE_MESSAGE"));
            return false;
        }
        boolean persisted = false;
        Site currentSite = CurrentSessionContainer.getCurrentSession().getSite();
        try {
            sessionManager.saveSite(currentSite);
            persisted = true;
        } catch (NameExistsException ex) {
            logger.warn("Failed to persist the current site because " +
                    "it is never persisted and there is already " +
                    "an site with name " + currentSite.getInfo().getName(), ex);
            messenger.error(MessageFormat.format(bundle.getString("CREATE_SITE_NAME_EXISTS"), currentSite.getInfo().getName()));
        } catch (PersistenceDeviceException ex) {
            logger.error("Failed to persist the current site (" +
                    currentSite.getInfo().getName() + ") because of a device error", ex);
            String message = MessageFormat.format(bundle.getString("SAVE_SITE_DEVICE_ERROR"),
                    currentSite.getInfo().getName());
            messenger.error(message);
            persisted = false;
        }
        return persisted;
    }

    /**
     * Checks if the current site is different from its representation in
     * the persistence device. If they are the same it returns true. If they
     * are different or there is no representation in the persistence device
     * or the persistence device is not accessible it returns true.
     * @return false if the current site is the same with its representation
     * in the persistence device, true otherwise
     */
    @Override
    public boolean isCurrentSiteModified() {
        boolean modified = true;
        Site currentSite = CurrentSessionContainer.getCurrentSession().getSite();
        try {
            modified = !sessionManager.isSitePersisted(currentSite);
        } catch (PersistenceDeviceException ex) {
            logger.error("There was a persistence device error while checking " +
                    "if site " + currentSite.getInfo().getName() + " is persisted. " +
                    "Non persistence of the site is assumed.", ex);
            modified = true;
        }
        return modified;
    }

    @Override
    public void saveCurrentSiteAs(ComponentInfo newSiteInfo) {
        // Try to save the current site with a different name
        Site currentSite = CurrentSessionContainer.getCurrentSession().getSite();
        Site newSite = null;
        try {
            newSite = sessionManager.saveSiteAs(currentSite, newSiteInfo);
        } catch (NameExistsException ex) {
            logger.warn("Did not copied site because there is already " +
                    "an site with name " + newSiteInfo.getName(), ex);
            messenger.error(MessageFormat.format(bundle.getString("CREATE_SITE_NAME_EXISTS"), newSiteInfo.getName()));
        } catch (PersistenceDeviceException ex) {
            logger.warn("Failed to copy site with name " +
                    newSiteInfo.getName() + " because of an error from the percistence device.", ex);
            messenger.error(MessageFormat.format(bundle.getString("CREATE_SITE_DEVICE_ERROR"), newSiteInfo.getName()));
        }
        // If everything was fine update the session and the user interface
        if (newSite != null) {
            CurrentSessionContainer.getCurrentSession().setSite(newSite);
            uiManager.sessionModified();
        }
    }

    @Override
    public void deleteSite(ComponentInfo siteInfo) {
        // Check if the site to delete is the current site. Then we do nothing
        if (CurrentSessionContainer.getCurrentSession().getSite().getInfo().getName().equals(siteInfo.getName())) {
            messenger.error(MessageFormat.format(bundle.getString("DELETE_SITE_CURRENT"), siteInfo.getName()));
            return;
        }
        // Get a confirmation from the user that he really wants to delete the site
        String message = MessageFormat.format(bundle.getString("DELETE_SITE_CONFIRM"),
                siteInfo.getName());
        List<String> options = new ArrayList<String>(2);
        options.add(bundle.getString("DELETE_OPTION"));
        options.add(bundle.getString("CANCEL_OPTION"));
        if(!options.get(0).equals(messenger.decision(message, options))) {
            return;
        }
        // Delete the site from the persistence device by using the SessionManager
        // provided by the persistence framework
        logger.info("User requested deletion of site " + siteInfo.getName());
        try {
            sessionManager.deleteSite(siteInfo);
            logger.info("Site was deleted successfully");
            message = MessageFormat.format(bundle.getString("DELETE_SITE_SUCCESS"), siteInfo.getName());
            messenger.info(message);
            uiManager.sessionModified();
        } catch (NameNotFoundException ex) {
            logger.warn("Failed to delete site " + siteInfo.getName() +
                    " because it does not exist in persistence device", ex);
            message = MessageFormat.format(bundle.getString("DELETE_SITE_NOT_FOUND"), siteInfo.getName());
            messenger.error(message);
        } catch (InUseException ex) {
            StringBuilder usersBuffer = new StringBuilder();
            for (String session : ex.getUsers()) {
                usersBuffer.append(session);
                usersBuffer.append(", ");
            }
            String users = usersBuffer.toString();
            users = users.substring(0, users.length() - 2);
            logger.warn("Failed to delete site " + siteInfo.getName() +
                    " because it is in use by " + users, ex);
            message = MessageFormat.format(bundle.getString("DELETE_SITE_IN_USE"), siteInfo.getName(), users);
            messenger.error(message);
        } catch (PersistenceDeviceException ex) {
            logger.warn("Failed to delete site " + siteInfo.getName() +
                    " because there was an error while accessing the persistence device", ex);
            message = MessageFormat.format(bundle.getString("DELETE_SITE_DEVICE_ERROR"), siteInfo.getName());
            messenger.error(message);
        }
    }

    @Override
    public void importFromFile(File file, ComponentInfo siteInfo) {
        // Check if the current site has been modified and if it is
        // ask the user what he wants to do and do it
        if (isCurrentSiteModified()) {
            if (!handleModifiedCurrentSite()) {
                return;
            }
        }
        Thread importThread = new ImportSiteThread(file, siteInfo);
        messenger.actionStarted(bundle.getString("IMPORT_SITE_MESSAGE"), importThread);
        importThread.start();
    }

    private class ImportSiteThread extends Thread {
        private File file;
        private ComponentInfo siteInfo;

        public ImportSiteThread(File file, ComponentInfo siteInfo) {
            super();
            this.file = file;
            this.siteInfo = siteInfo;
        }

        @Override
        public void run() {
            boolean ok = true;
            SiteWithDatasets site = null;
            try {
                site = dataImporter.importSiteFromFile(file, siteInfo);
            } catch (IOException ex) {
                logger.warn("Failed to import site because of IO problem", ex);
                String message = MessageFormat.format(bundle.getString("IMPORT_SITE_ERROR"), siteInfo.getName());
                messenger.error(message);
                ok = false;
            } catch (WrongFormatException ex) {
                logger.info("Failed to import site because of wrong format", ex);
                String message = MessageFormat.format(bundle.getString("IMPORT_SITE_WRONG_FORMAT"), file.getName());
                messenger.error(message);
                ok = false;
            }
            if (!ok) {
                messenger.actionStoped(this);
                return;
            }
            PersistenceFactory.beginTransaction();
            try {
                Dataset dataset = site.getGalacticProfileDataset();
                if (dataset != null) {
                    datasetManager.saveDataset(dataset);
                }
                dataset = site.getZodiacalProfileDataset();
                if (dataset != null) {
                    datasetManager.saveDataset(dataset);
                }
                dataset = site.getSkyAbsorptionDataset();
                if (dataset != null) {
                    datasetManager.saveDataset(dataset);
                }
                dataset = site.getSkyExtinctionDataset();
                if (dataset != null) {
                    datasetManager.saveDataset(dataset);
                }
                dataset = site.getAtmosphericTransmissionDataset();
                if (dataset != null) {
                    datasetManager.saveDataset(dataset);
                }
                Map<String, Dataset> skyEmissionDatasetMap = site.getSkyEmissionDatasetMap();
                if (!skyEmissionDatasetMap.isEmpty()) {
                    DatasetInfo info = null;
                    String xUnit = null;
                    String yUnit = null;
                    Dataset.DataType dataType = null;
                    Map<String, Map<Double, Double>> data = new HashMap<String, Map<Double, Double>>();
                    for (Map.Entry<String, Dataset> entry : skyEmissionDatasetMap.entrySet()) {
                        String option = entry.getKey();
                        Dataset datasetPart = entry.getValue();
                        info = datasetPart.getInfo();
                        xUnit = datasetPart.getXUnit();
                        yUnit = datasetPart.getYUnit();
                        data.put(option, datasetPart.getData());
                        dataType = datasetPart.getDataType();
                    }
                    datasetManager.saveMultiDataset(Dataset.Type.SKY_EMISSION, info, xUnit, yUnit, data, dataType);
                }
                sessionManager.saveSiteAs(site, siteInfo);
                PersistenceFactory.commitTransaction();
            } catch (NameExistsException ex) {
                PersistenceFactory.rollbackTransaction();
                logger.info("Failed to import site because of name already exists", ex);
                String message = MessageFormat.format(bundle.getString("CREATE_SITE_NAME_EXISTS"), siteInfo.getName());
                messenger.error(message);
                ok = false;
            } catch (PersistenceDeviceException ex) {
                PersistenceFactory.rollbackTransaction();
                logger.warn("Failed to import site because of an error on persistence device", ex);
                String message = MessageFormat.format(bundle.getString("IMPORT_SITE_ERROR"), siteInfo.getName());
                messenger.error(message);
                ok = false;
            }
            if (!ok) {
                messenger.actionStoped(this);
                return;
            }
            Site persistedSite = null;
            try {
                persistedSite = sessionManager.loadSite(siteInfo);
            } catch (NameNotFoundException ex) {
                logger.warn("Site just imported not found in persistence device", ex);
                String message = MessageFormat.format(bundle.getString("IMPORT_SITE_ERROR"), siteInfo.getName());
                messenger.error(message);
                ok = false;
            } catch (PersistenceDeviceException ex) {
                logger.warn("Failed to retrieve just imported site from persistence device", ex);
                String message = MessageFormat.format(bundle.getString("IMPORT_SITE_ERROR"), siteInfo.getName());
                messenger.error(message);
                ok = false;
            }
            messenger.actionStoped(this);
            if (!ok) {
                return;
            }
            CurrentSessionContainer.getCurrentSession().setSite(persistedSite);
            uiManager.sessionModified();
        }
    }

    @Override
    public void exportCurrentSiteInFile(File file) {
        Site currentSite = CurrentSessionContainer.getCurrentSession().getSite();
        try {
            dataExporter.exportSiteInFile(file, currentSite, datasetProvider);
        } catch (IOException ex) {
            logger.warn("Failed to export site", ex);
            String message = MessageFormat.format(bundle.getString("EXPORT_SITE_ERROR"), currentSite.getInfo().getName());
            messenger.error(message);
        }
    }

}
