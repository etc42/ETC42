/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLClassLoader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import javax.swing.JOptionPane;
import org.apache.log4j.Logger;
import org.cnrs.lam.dis.etc.configuration.ConfigFactory;
import org.cnrs.lam.dis.etc.ui.swing.SplashScreen;
import org.cnrs.lam.dis.etc.ui.swing.importer.FileImportWorker;
import org.cnrs.lam.dis.etc.ui.swing.importer.ImportHelper;

/**
 *
 * @author Nikolaos Apostolakos
 */
final class Installer {



	private static Logger logger = Logger.getLogger(Installer.class);
	private static boolean newDatabase = false;

	private Installer() {
	}

	public static boolean isNewDatabase() {
		return newDatabase;
	}

	public static void getPresetsList(List<String> filenames, File dir) {
		if (dir.isDirectory()) {
		    for (File entry : dir.listFiles()) {
		        if (entry.isFile())
		        	filenames.add("/installdata/" + entry.getPath().split("/installdata/")[1]);
		        else
		        	getPresetsList(filenames, entry);
		    }
		}
	}

	private static void getResourceFiles(List<String> filenames, String path, Class<?> resourceClass)  throws IOException {

		Enumeration<URL> en = resourceClass.getClassLoader().getResources(path);
		if (en.hasMoreElements()) {
			URL url = en.nextElement();
			File rootPresetPath = new File(url.getFile());
			getPresetsList(filenames, rootPresetPath);
			
			// We are in the jar and can't get to path
			if(filenames.size() == 0) {				
				JarURLConnection urlcon = (JarURLConnection) (url.openConnection());
				try (JarFile jar = urlcon.getJarFile();) {
					Enumeration<JarEntry> entries = jar.entries();
					while (entries.hasMoreElements()) {
						String entry = entries.nextElement().getName();
						if (entry.contains("installdata/presets/") && entry.endsWith(".xml")) {						
							filenames.add("/" + entry);
						}
					}
				}
			}
		}
	}

	public static FileImportWorker importPresets() throws IOException {
		List<String> presetsResources = new ArrayList<>();

		// presetsResources.add("/installdata/presets/sources/Sources-Halpha - CFRS-14.0600.xml");
		getResourceFiles(presetsResources, "installdata/presets/", DbUpdater.class);

		FileImportWorker worker = ImportHelper.importPresets(presetsResources, DbUpdater.class);
		return worker;
	}


	static void install() {
		// Create the directory if it is not already created
		if (missingEtcHomeDir()) {
			logger.info("ETC home directory is missing");
			createEtcHomeDir();
		}
		if (missingPluginDir()) {
			logger.info("Plugins directory is missing");
			createPluginDir();
		}
		if (missingDatabase()) {
			logger.info("Database is missing");
			SplashScreen parent = null;
			if (SplashScreen.getInstance().isVisible()) {
				parent = SplashScreen.getInstance();
			}
			int selection = JOptionPane.showConfirmDialog(parent,
					"I cannot find any previous persistence installation. Do you want me to install the default?\n" +
							"(If this is the first time you run ETC please select YES)\n" +
							"WARNING:\n" +
							"This operation will replace any data in the directory\n" +
							ConfigFactory.getConfig().getEtcHome(),
							"Database needed", JOptionPane.YES_NO_OPTION);
			if (selection == JOptionPane.YES_OPTION) {
				createDatabase();
			}
			else {
				logger.info("User decided to don't crate a database. Exiting...");
				System.exit(0);
			}
		}
	}

	private static boolean missingEtcHomeDir() {
		return !(new File(ConfigFactory.getConfig().getEtcHome()).isDirectory());
	}

	private static void createEtcHomeDir() {
		File etcHome = new File(ConfigFactory.getConfig().getEtcHome());
		logger.info("Creating directory " + etcHome);
		etcHome.mkdirs();
	}

	private static boolean missingPluginDir() {
		return !(new File(ConfigFactory.getConfig().getPluginDir()).isDirectory());
	}

	private static void createPluginDir() {
		File pluginDir = new File(ConfigFactory.getConfig().getPluginDir());
		logger.info("Creating directory " + pluginDir);
		pluginDir.mkdirs();
	}

	private static boolean missingDatabase() {
		try {
			Connection connection = DriverManager.getConnection("jdbc:derby:db");
			connection.close();
		} catch (SQLException ex) {
			// The SQLState XJ004 is the "Database '<databaseName>' not found" derby code
			if (ex.getSQLState().equals("XJ004"))
				return true;
		}
		return false;
	}

	private static void createDatabase() {
		logger.info("Creating default database");
		// First clear any database directory (if it exists)
		File dbDir = new File(ConfigFactory.getConfig().getEtcHome() + ConfigFactory.getConfig().getFileSeparator() + "db");
		if (dbDir.exists()) {
			logger.info("Deleting old database files");
			delete(dbDir);
		}
		// Unzip the database
		DbInstaller.installDb(dbDir.getAbsolutePath());
		newDatabase = true;
	}

	private static void delete(File file) {
		if (!file.exists())
			return;
		if (file.isFile()) {
			file.delete();
			return;
		}
		if (file.isDirectory()) {
			for (File innerFile : file.listFiles()) {
				delete(innerFile);
			}
			file.delete();
		}
	}

}
