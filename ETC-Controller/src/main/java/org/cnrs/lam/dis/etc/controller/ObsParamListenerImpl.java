/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.controller;

import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;
import org.cnrs.lam.dis.etc.dataimportexport.DataExporter;
import org.cnrs.lam.dis.etc.dataimportexport.DataImporter;
import org.cnrs.lam.dis.etc.dataimportexport.ObsParamWithDatasets;
import org.cnrs.lam.dis.etc.dataimportexport.WrongFormatException;
import org.cnrs.lam.dis.etc.datamodel.ComponentInfo;
import org.cnrs.lam.dis.etc.datamodel.DatasetProvider;
import org.cnrs.lam.dis.etc.datamodel.ObsParam;
import org.cnrs.lam.dis.etc.persistence.DatasetManager;
import org.cnrs.lam.dis.etc.persistence.InUseException;
import org.cnrs.lam.dis.etc.persistence.NameExistsException;
import org.cnrs.lam.dis.etc.persistence.NameNotFoundException;
import org.cnrs.lam.dis.etc.persistence.PersistenceDeviceException;
import org.cnrs.lam.dis.etc.persistence.SessionManager;
import org.cnrs.lam.dis.etc.ui.Messenger;
import org.cnrs.lam.dis.etc.ui.ObsParamListener;
import org.cnrs.lam.dis.etc.ui.UIManager;

/**
 *
 * @author Nikolaos Apostolakos
 */
final class ObsParamListenerImpl implements ObsParamListener {

    /** The bundle for retrieving the i18n strings for the messages.*/
    private static ResourceBundle bundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/controller/messages");
    /** The logger of the class.*/
    private static Logger logger = Logger.getLogger(ObsParamListenerImpl.class);

    private SessionManager sessionManager;
    private DatasetManager datasetManager;
    private Messenger messenger;
    private UIManager uiManager;
    private DataExporter dataExporter;
    private DataImporter dataImporter;
    private DatasetProvider datasetProvider;

    public ObsParamListenerImpl(SessionManager sessionManager, DatasetManager datasetManager, Messenger messenger,
            UIManager uiManager, DataExporter dataExporter, DataImporter dataImporter,
            DatasetProvider datasetProvider) {
        this.sessionManager = sessionManager;
        this.datasetManager = datasetManager;
        this.messenger = messenger;
        this.uiManager = uiManager;
        this.dataExporter = dataExporter;
        this.dataImporter = dataImporter;
        this.datasetProvider = datasetProvider;
    }

    public void createNewObsParam(ComponentInfo newObsParamInfo) {
        //First check if the current obsParam has been modified and if it is
        // ask the user what he wants to do and do it
        if (isCurrentObsParamModified()) {
            if (!handleModifiedCurrentObsParam()) {
                return;
            }
        }

        // Create the instance of the new obsParam
        ObsParam newObsParam = getNewObsParamFromSessionManager(newObsParamInfo);
        if (newObsParam == null) {
            return;
        }

        // Set the new obsParam as the current one and notify the user interface
        CurrentSessionContainer.getCurrentSession().setObsParam(newObsParam);
        uiManager.sessionModified();
    }

    /**
     * Creates a new instance of an obsParam by using the createNewObsParam()
     * method of the session manager. If the creation fails it shows the necessary
     * messages to the user and it returns null.
     * @param info The name and description of the new obsParam
     * @return The new obsParam instance
     */
    private ObsParam getNewObsParamFromSessionManager(ComponentInfo info) {
        ObsParam newObsParam = null;
        try {
            newObsParam = sessionManager.createNewObsParam(info);
        } catch (NameExistsException ex) {
            logger.warn("Did not create new obsParam because there is already " +
                    "an obsParam with name " + info.getName(), ex);
            messenger.error(MessageFormat.format(bundle.getString("CREATE_OBS_PARAM_NAME_EXISTS"), info.getName()));
        } catch (PersistenceDeviceException ex) {
            logger.warn("Failed to create new obsParam with name " +
                    info.getName() + " because there of an error from the percistence device.", ex);
            messenger.error(MessageFormat.format(bundle.getString("CREATE_OBS_PARAM_DEVICE_ERROR"), info.getName()));
        }
        return newObsParam;
    }

    /**
     * This method handles the case when an action will replace the current
     * obsParam, but the current obsParam is modified. It presents to the
     * user a dialog for selecting what he wants to do (save the changes, ignore
     * the changes or cancel the action). If the user selects to save the changes
     * then this method will persist the current obsParam.
     * @return true if the action can continue, false otherwise
     */
    private boolean handleModifiedCurrentObsParam() {
        boolean canContinue = false;
        // Get the decision of the user
        String message = bundle.getString("CURRENT_OBS_PARAM_MODIFIED");
        List<String> list = new ArrayList<String>(3);
        list.add(bundle.getString("SAVE_OPTION"));
        list.add(bundle.getString("IGNORE_OPTION"));
        list.add(bundle.getString("CANCEL_OPTION"));
        String decision = messenger.decision(message, list);

        if (decision == null || decision.equals(list.get(2))) {
            // Here we know the user wants to cancel so return false to stop the calling action
            canContinue = false;
        } else if (decision.equals(list.get(1))) {
            // Ignore the modified data. Let the calling action to continue without saving the data
            canContinue = true;
        } else if (decision.equals(list.get(0))) {
            // Save the current data and allow the calling action to continue if successful
            canContinue = persistCurrentObsParam();
        }
        return canContinue;
    }

    public void loadObsParam(ComponentInfo obsParamInfo) {
        // First check if the requested obsParam is the current obsParam.
        // If it is do nothing.
        if (obsParamInfo.getName().equals(CurrentSessionContainer.getCurrentSession().
                getObsParam().getInfo().getName())) {
            return;
        }
        // Check if the current obsParam has been modified and if it is
        // ask the user what he wants to do and do it
        if (isCurrentObsParamModified()) {
            if (!handleModifiedCurrentObsParam()) {
                return;
            }
        }
        // Load the instance of the obsParam
        ObsParam loadedObsParam = loadObsParamFromSessionManager(obsParamInfo);
        if (loadedObsParam == null) {
            return;
        }
        // Set the loaded obsParam as the current one and notify the user interface
        CurrentSessionContainer.getCurrentSession().setObsParam(loadedObsParam);
        uiManager.sessionModified();
    }

    /**
     * Loads an instance of an obsParam by using the loadObsParam()
     * method of the session manager. If the load fails it shows the necessary
     * messages to the user and it returns null.
     * @param info The name of the obsParam to load
     * @return The loaded obsParam instance
     */
    private ObsParam loadObsParamFromSessionManager(ComponentInfo info) {
        ObsParam loadedObsParam = null;
        try {
            loadedObsParam = sessionManager.loadObsParam(info);
        } catch (NameNotFoundException ex) {
            logger.warn("Did not load the obsParam because there is no " +
                    "obsParam with name " + info.getName(), ex);
            messenger.error(MessageFormat.format(bundle.getString("LOAD_OBS_PARAM_NOT_FOUND"), info.getName()));
        } catch (PersistenceDeviceException ex) {
            logger.warn("Failed to load obsParam with name " + info.getName() +
                    " because of an error from the percistence device.", ex);
            messenger.error(MessageFormat.format(bundle.getString("LOAD_OBS_PARAM_DEVICE_ERROR"), info.getName()));
        }
        return loadedObsParam;
    }

    public void reloadCurrentObsParam() {
        // Check if the obs param exist in the database. If not give a message
        if (!sessionManager.isObsParamInDatabase(CurrentSessionContainer.getCurrentSession().getObsParam())) {
            messenger.warn(bundle.getString("RELOAD_OBS_PARAM_NOT_IN_DB_MESSAGE"));
            return;
        }
        // Check if the obsParam has been modified. If it is not we don't need to do anything
        if (!isCurrentObsParamModified()) {
            return;
        }
        // Ask the user if he really wants to do the reload and lose any changes
        if (!getUserConfirmationForReload()) {
            return;
        }
        // Reload the curernt obsParam from the persistence device
        ComponentInfo currentInfo = CurrentSessionContainer.getCurrentSession().getObsParam().getInfo();
        ObsParam loadedObsParam = loadObsParamFromSessionManager(currentInfo);
        if (loadedObsParam == null) {
            return;
        }
        // Set the loaded obsParam as the current one and notify the user interface
        CurrentSessionContainer.getCurrentSession().setObsParam(loadedObsParam);
        uiManager.sessionModified();
    }

    /**
     * Gets a confirmation from the user to reload the current obsParam.
     * @return true if the user confirms, false otherwise
     */
    private boolean getUserConfirmationForReload() {
        boolean confirmed = false;
        String message = bundle.getString("CONFIRM_OBS_PARAM_RELOAD");
        List<String> list = new ArrayList<String>(2);
        list.add(bundle.getString("YES_OPTION"));
        list.add(bundle.getString("NO_OPTION"));
        String decision = messenger.decision(message, list);

        if (decision == null || decision.equals(list.get(1))) {
            // Here we know the user wants to cancel so return false to stop the calling action
            confirmed = false;
        } else if (decision.equals(list.get(0))) {
            // Got the confirmation for reloading
            confirmed = true;
        }
        return confirmed;
    }

    /**
     * Handles a user request for saving the current obsParam. If the current
     * obsParam has not been modified (its representation in the persistence
     * device has the same information) nothing is done. Otherwise it persists
     * the obsParam and it updates the representation shown to the user.
     */
    public void saveCurrentObsParam() {
        // If the current obsParam has not been modified we do not need to do anything
        if (!isCurrentObsParamModified()) {
            return;
        }

        // Persist the current obsParam. On success notify the user
        // interface to update the user representation for possible changes
        // (like dataset namespaces, etc)
        if (persistCurrentObsParam()) {
            uiManager.sessionModified();
        }
    }

    /**
     * Tries to persist all the modifications done to the current obsParam.
     * If it succeed it returns true. If there is any problem with the persistence
     * device it shows an error to the user and it returns false.
     * @return true if the current obsParam is persisted successfully, false otherwise
     */
    private boolean persistCurrentObsParam() {
        // Check if the current obsParam is the "new" obsParam. In this case saving is not allowed
        if (CurrentSessionContainer.getCurrentSession().getObsParam().getInfo().getName().equals("new")) {
            messenger.warn(bundle.getString("SAVE_NEW_OBS_PARAM_MESSAGE"));
            return false;
        }
        boolean persisted = false;
        ObsParam currentObsParam = CurrentSessionContainer.getCurrentSession().getObsParam();
        try {
            sessionManager.saveObsParam(currentObsParam);
            persisted = true;
        } catch (NameExistsException ex) {
            logger.warn("Failed to persist the current obs param because " +
                    "it is never persisted and there is already " +
                    "an obs param with name " + currentObsParam.getInfo().getName(), ex);
            messenger.error(MessageFormat.format(bundle.getString("CREATE_OBS_PARAM_NAME_EXISTS"), currentObsParam.getInfo().getName()));
        } catch (PersistenceDeviceException ex) {
            logger.error("Failed to persist the current obsParam (" +
                    currentObsParam.getInfo().getName() + ") because of a device error", ex);
            String message = MessageFormat.format(bundle.getString("SAVE_OBS_PARAM_DEVICE_ERROR"),
                    currentObsParam.getInfo().getName());
            messenger.error(message);
            persisted = false;
        }
        return persisted;
    }

    /**
     * Checks if the current obsParam is different from its representation in
     * the persistence device. If they are the same it returns true. If they
     * are different or there is no representation in the persistence device
     * or the persistence device is not accessible it returns true.
     * @return false if the current obsParam is the same with its representation
     * in the persistence device, true otherwise
     */
    public boolean isCurrentObsParamModified() {
        boolean modified = true;
        ObsParam currentObsParam = CurrentSessionContainer.getCurrentSession().getObsParam();
        try {
            modified = !sessionManager.isObsParamPersisted(currentObsParam);
        } catch (PersistenceDeviceException ex) {
            logger.error("There was a persistence device error while checking " +
                    "if obsParam " + currentObsParam.getInfo().getName() + " is persisted. " +
                    "Non persistence of the obsParam is assumed.", ex);
            modified = true;
        }
        return modified;
    }

    public void saveCurrentObsParamAs(ComponentInfo newObsParamInfo) {
        // Try to save the current obsParam with a different name
        ObsParam currentObsParam = CurrentSessionContainer.getCurrentSession().getObsParam();
        ObsParam newObsParam = null;
        try {
            newObsParam = sessionManager.saveObsParamAs(currentObsParam, newObsParamInfo);
        } catch (NameExistsException ex) {
            logger.warn("Did not copied obsParam because there is already " +
                    "an obsParam with name " + newObsParamInfo.getName(), ex);
            messenger.error(MessageFormat.format(bundle.getString("CREATE_OBS_PARAM_NAME_EXISTS"), newObsParamInfo.getName()));
        } catch (PersistenceDeviceException ex) {
            logger.warn("Failed to copy obsParam with name " +
                    newObsParamInfo.getName() + " because of an error from the percistence device.", ex);
            messenger.error(MessageFormat.format(bundle.getString("CREATE_OBS_PARAM_DEVICE_ERROR"), newObsParamInfo.getName()));
        }
        // If everything was fine update the session and the user interface
        if (newObsParam != null) {
            CurrentSessionContainer.getCurrentSession().setObsParam(newObsParam);
            uiManager.sessionModified();
        }
    }

    public void deleteObsParam(ComponentInfo obsParamInfo) {
        // Check if the obsParam to delete is the current obsParam. Then we do nothing
        if (CurrentSessionContainer.getCurrentSession().getObsParam().getInfo().getName().equals(obsParamInfo.getName())) {
            messenger.error(MessageFormat.format(bundle.getString("DELETE_OBS_PARAM_CURRENT"), obsParamInfo.getName()));
            return;
        }
        // Get a confirmation from the user that he really wants to delete the obsParam
        String message = MessageFormat.format(bundle.getString("DELETE_OBS_PARAM_CONFIRM"),
                obsParamInfo.getName());
        List<String> options = new ArrayList<String>(2);
        options.add(bundle.getString("DELETE_OPTION"));
        options.add(bundle.getString("CANCEL_OPTION"));
        if(!options.get(0).equals(messenger.decision(message, options))) {
            return;
        }
        // Delete the obsParam from the persistence device by using the SessionManager
        // provided by the persistence framework
        logger.info("User requested deletion of obsParam " + obsParamInfo.getName());
        try {
            sessionManager.deleteObsParam(obsParamInfo);
            logger.info("ObsParam was deleted successfully");
            message = MessageFormat.format(bundle.getString("DELETE_OBS_PARAM_SUCCESS"), obsParamInfo.getName());
            messenger.info(message);
            uiManager.sessionModified();
        } catch (NameNotFoundException ex) {
            logger.warn("Failed to delete obsParam " + obsParamInfo.getName() +
                    " because it does not exist in persistence device", ex);
            message = MessageFormat.format(bundle.getString("DELETE_OBS_PARAM_NOT_FOUND"), obsParamInfo.getName());
            messenger.error(message);
        } catch (InUseException ex) {
            StringBuffer usersBuffer = new StringBuffer();
            for (String session : ex.getUsers()) {
                usersBuffer.append(session);
                usersBuffer.append(", ");
            }
            String users = usersBuffer.toString();
            users = users.substring(0, users.length() - 2);
            logger.warn("Failed to delete obsParam " + obsParamInfo.getName() +
                    " because it is in use by " + users, ex);
            message = MessageFormat.format(bundle.getString("DELETE_OBS_PARAM_IN_USE"), obsParamInfo.getName(), users);
            messenger.error(message);
        } catch (PersistenceDeviceException ex) {
            logger.warn("Failed to delete obsParam " + obsParamInfo.getName() +
                    " because there was an error while accessing the persistence device", ex);
            message = MessageFormat.format(bundle.getString("DELETE_OBS_PARAM_DEVICE_ERROR"), obsParamInfo.getName());
            messenger.error(message);
        }
    }

    public void importFromFile(File file, ComponentInfo obsParamInfo) {
        // Check if the current obsParam has been modified and if it is
        // ask the user what he wants to do and do it
        if (isCurrentObsParamModified()) {
            if (!handleModifiedCurrentObsParam()) {
                return;
            }
        }
        Thread importThread = new ImportObsParamThread(file, obsParamInfo);
        messenger.actionStarted(bundle.getString("IMPORT_OBS_PARAM_MESSAGE"), importThread);
        importThread.start();
    }

    private class ImportObsParamThread extends Thread {
        private File file;
        private ComponentInfo obsParamInfo;

        public ImportObsParamThread(File file, ComponentInfo obsParamInfo) {
            this.file = file;
            this.obsParamInfo = obsParamInfo;
        }

        @Override
        public void run() {
            boolean ok = true;
            ObsParamWithDatasets obsParam = null;
            try {
                obsParam = dataImporter.importObsParamFromFile(file, obsParamInfo);
            } catch (IOException ex) {
                logger.warn("Failed to import obsParam because of IO problem", ex);
                String message = MessageFormat.format(bundle.getString("IMPORT_OBS_PARAM_ERROR"), obsParamInfo.getName());
                messenger.error(message);
                ok = false;
            } catch (WrongFormatException ex) {
                logger.info("Failed to import obsParam because of wrong format", ex);
                String message = MessageFormat.format(bundle.getString("IMPORT_OBS_PARAM_WRONG_FORMAT"), file.getName());
                messenger.error(message);
                ok = false;
            }
            if (!ok) {
                messenger.actionStoped(this);
                return;
            }
            try {
                sessionManager.saveObsParamAs(obsParam, obsParamInfo);
            } catch (NameExistsException ex) {
                logger.info("Failed to import obsParam because of name already exists", ex);
                String message = MessageFormat.format(bundle.getString("CREATE_OBS_PARAM_NAME_EXISTS"), obsParamInfo.getName());
                messenger.error(message);
                ok = false;
            } catch (PersistenceDeviceException ex) {
                logger.warn("Failed to import obsParam because of an error on persistence device", ex);
                String message = MessageFormat.format(bundle.getString("IMPORT_OBS_PARAM_ERROR"), obsParamInfo.getName());
                messenger.error(message);
                ok = false;
            }
            if (!ok) {
                messenger.actionStoped(this);
                return;
            }
            ObsParam persistedObsParam = null;
            try {
                persistedObsParam = sessionManager.loadObsParam(obsParamInfo);
            } catch (NameNotFoundException ex) {
                logger.warn("ObsParam just imported not found in persistence device", ex);
                String message = MessageFormat.format(bundle.getString("IMPORT_OBS_PARAM_ERROR"), obsParamInfo.getName());
                messenger.error(message);
                ok = false;
            } catch (PersistenceDeviceException ex) {
                logger.warn("Failed to retrieve just imported obsParam from persistence device", ex);
                String message = MessageFormat.format(bundle.getString("IMPORT_OBS_PARAM_ERROR"), obsParamInfo.getName());
                messenger.error(message);
                ok = false;
            }
            messenger.actionStoped(this);
            if (!ok) {
                return;
            }
            CurrentSessionContainer.getCurrentSession().setObsParam(persistedObsParam);
            uiManager.sessionModified();
        }
    }

    public void exportCurrentObsParamInFile(File file) {
        ObsParam currentObsParam = CurrentSessionContainer.getCurrentSession().getObsParam();
        try {
            dataExporter.exportObsParamInFile(file, currentObsParam, datasetProvider);
        } catch (IOException ex) {
            logger.warn("Failed to export obsParam", ex);
            String message = MessageFormat.format(bundle.getString("EXPORT_OBS_PARAM_ERROR"), currentObsParam.getInfo().getName());
            messenger.error(message);
        }
    }

}
