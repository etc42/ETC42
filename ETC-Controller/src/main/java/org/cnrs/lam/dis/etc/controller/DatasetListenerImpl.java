/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.controller;

import cds.savot.model.SavotVOTable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TreeMap;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.apache.log4j.Logger;
import org.cnrs.lam.cesam.vo.dnd.VoTable1DHelper;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.calculator.util.datasets.LinearFunctionDataset;
import org.cnrs.lam.dis.etc.calculator.util.datasets.TemplateFunctionDataset;
import org.cnrs.lam.dis.etc.dataimportexport.DataExporter;
import org.cnrs.lam.dis.etc.dataimportexport.DataImporter;
import org.cnrs.lam.dis.etc.dataimportexport.WrongFormatException;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.Dataset.Type;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.cnrs.lam.dis.etc.datamodel.DatasetProvider;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.cnrs.lam.dis.etc.persistence.PersistenceDeviceException;
import org.cnrs.lam.dis.etc.persistence.DatasetManager;
import org.cnrs.lam.dis.etc.persistence.InUseException;
import org.cnrs.lam.dis.etc.persistence.NameExistsException;
import org.cnrs.lam.dis.etc.persistence.NameNotFoundException;
import org.cnrs.lam.dis.etc.ui.DatasetListener;
import org.cnrs.lam.dis.etc.ui.Messenger;
import org.cnrs.lam.dis.etc.ui.UIManager;

/**
 *
 * @author Nikolaos Apostolakos
 */
final class DatasetListenerImpl implements DatasetListener {

    /** The bundle for retrieving the i18n strings for the messages.*/
    private static ResourceBundle bundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/controller/messages");
    /** The logger of the class.*/
    private static Logger logger = Logger.getLogger(DatasetListenerImpl.class.getName());

    private DatasetManager datasetManager;
    private Messenger messenger;
    private UIManager uiManager;
    private DataImporter dataImporter;
    private DataExporter dataExporter;
    private DatasetProvider datasetProvider;

    public DatasetListenerImpl(DatasetManager datasetManager, Messenger messenger,
            UIManager uiManager, DataImporter dataImporter, DataExporter dataExporter,
            DatasetProvider datasetProvider) {
        this.datasetManager = datasetManager;
        this.messenger = messenger;
        this.uiManager = uiManager;
        this.dataImporter = dataImporter;
        this.dataExporter = dataExporter;
        this.datasetProvider = datasetProvider;
    }

    /**
     * This method is called when the listener gets notified that the user
     * wants to preview a dataset. To load the dataset from the persistence
     * device, the DatasetManager provided by the persistence framework is used.
     * If there is no such dataset or if there is any problem with retrieving
     * the dataset from the device a message is shown to the user. Otherwise
     * a plot is shown to the user.
     *
     * @param type The type of the dataset to preview
     * @param datasetInfo The namespace and name of the dataset
     * @param option The option to preview if we have a multi-dataset or null if
     * we have single dataset
     */
    @Override
    public void previewDataset(Type type, DatasetInfo datasetInfo, String option) {
        // First retrieve the dataset from the persistence device
        Dataset dataset = null;
        try {
            dataset = datasetManager.loadDataset(type, datasetInfo, option);
        } catch (NameNotFoundException ex) {
            logger.error("There is no dataset found with name " +
                    getDatasetFullName(datasetInfo), ex);
            messenger.error(bundle.getString("DATASET_NOT_FOUND"));
            return;
        } catch (PersistenceDeviceException ex) {
            logger.error("There was an error while retrieving the dataset " +
                    getDatasetFullName(datasetInfo), ex);
            messenger.error(bundle.getString("RETRIEVE_DATASET_ERROR"));
            return;
        } catch (NullPointerException ex) {
            logger.error("No data selected", ex);
            messenger.error(bundle.getString("NO_DATA_SELECTED_ERROR"));
            return;
        }
        
        // Create the necessary information for the VOTable
        String name = dataset.getInfo().getName();
        String title = (option == null) ? name : name + " - " + option;
        String xDescription = getXPlotDesc(type);
        String yDescription = getYPlotDesc(type);
        String xUnit = dataset.getXUnit();
        String yUnit = dataset.getYUnit();
        
        // Create the values taking in consideration if the data are function or template
        Map<Double, Double> datasetData = dataset.getData();
        UnivariateRealFunction function = null;
        switch (dataset.getDataType()) {
            case FUNCTION:
                function = new LinearFunctionDataset(datasetData);
                break;
            case TEMPLATE:
                function = new TemplateFunctionDataset(datasetData);
                /* @author: Anthony K GROSS */
                //System.out.println(datasetData);
                
                if(yUnit.contains("Ang")){
                	yUnit = yUnit.replace("Ang", xUnit);
                }
                else{
                	yUnit = yUnit + "/" + xUnit;
                }
                //System.out.println(yUnit);
                break;
        }
        Map<Double, Double> functionData = new TreeMap<Double, Double>();
        for (Double key : datasetData.keySet()) {
            try {
                functionData.put(key, function.value(key));
            } catch (Exception ex) {
                logger.error("Failed to plot dataset. Reason: " + ex.getMessage(), ex);
                messenger.error("Failed to plot dataset. Reason: " + ex.getMessage());
                return;
            }
        }
        //System.out.println(functionData);
        // Create the VOTable and plot it
        SavotVOTable voTable = VoTable1DHelper.createVoTable(title, xDescription, xUnit, yDescription, yUnit, functionData);
        uiManager.showChart(title, voTable, null);
    }

    private String getXPlotDesc(Type type) {
        return bundle.getString(type.name() + "_PLOT_X_DESC");
    }

    private String getYPlotDesc(Type type) {
        return bundle.getString(type.name() + "_PLOT_Y_DESC");
    }

    @Override
    public void importFromFile(File file, Type type, DatasetInfo info, Dataset.DataType dataType) {
        logger.info("User requested to import dataset from file " + file +
                " of type " + type + " and name " + getDatasetFullName(info));

        // Try to import the dataset from the file to the memory.
        // If the import fails we do not do anything else
        Dataset dataset = importDatasetInstanceFromFile(file, type, info, dataType);
        if (dataset == null) {
            return;
        }

        // Try to save the new dataset in the persistence device.
        // If this fails we do not do anything else
        if (!persistImportedDatasetInstance(dataset)) {
            return;
        }

        // If the dataset is not imported as a general use dataset, update the
        // current session
        if (info.getNamespace() != null && !info.getNamespace().isEmpty()) {
            updateCurrentSessionForDataset(type, info);
        }
    }

    private void updateCurrentSessionForDataset(Type type, DatasetInfo info) {
        // First update the session instance
        Session currentSession = CurrentSessionContainer.getCurrentSession();
        switch (type) {
            case FILTER_TRANSMISSION:
                currentSession.getInstrument().setFilterTransmission(info);
                break;
            case FWHM:
                currentSession.getInstrument().setFwhm(info);
                break;
            case SPECTRAL_RESOLUTION:
                currentSession.getInstrument().setSpectralResolution(info);
                break;
            case TRANSMISSION:
                currentSession.getInstrument().setTransmission(info);
                break;
            case ZODIACAL:
                currentSession.getSite().setZodiacalProfile(info);
                break;
            case GALACTIC:
                currentSession.getSite().setGalacticProfile(info);
                break;
            case SKY_ABSORPTION:
                currentSession.getSite().setSkyAbsorption(info);
                break;
            case SKY_EMISSION:
                currentSession.getSite().setSkyEmission(info);
                break;
            case SKY_EXTINCTION:
                currentSession.getSite().setSkyExtinction(info);
                break;
            case SPECTRAL_DIST_TEMPLATE:
                currentSession.getSource().setSpectralDistributionTemplate(info);
                break;
        }

        // Finally notify the UIManager that the session has been modified to
        // update the users view
        uiManager.sessionModified();
    }

    private boolean persistImportedDatasetInstance(Dataset dataset) {
        try {
            datasetManager.saveDataset(dataset);
            return true;
        } catch (NameExistsException ex) {
            logger.warn("Importing dataset failed because there is already a dataset with this name", ex);
            messenger.error(MessageFormat.format(bundle.getString("IMPORT_NAME_EXISTS"), getDatasetFullName(dataset.getInfo())));
        } catch (PersistenceDeviceException ex) {
            logger.error("Importing dataset failed because of an error during accessing the persistence device", ex);
            messenger.error(bundle.getString("IMPORT_DEVICE_ERROR"));
        }
        return false;
    }

    /**
     * This method imports a dataset from the given file into the memory as a
     * Dataset object. If there is any problem with reading the file an
     * error is shown to the user and the method returns null.
     * @param file The file containing the data
     * @param type The type of the dataset
     * @param info The namespace, name and description of the new dataset
     * @param dataType The type of the data of the dataset
     * @return The imported Dataset or null if there was a problem during reading the file
     */
    private Dataset importDatasetInstanceFromFile(File file, Type type, DatasetInfo info, Dataset.DataType dataType) {
        Dataset dataset = null;
        
        try {
            // First try to get the dataset assuming it is an exported component
            try {
                dataset = dataImporter.importDatasetFromComponentFile(file, type, info);
            } catch (WrongFormatException wrongFormatException) {
                // In this case we want to ignore the wrong format exception because
                // the file is a data file
            }
            
            // First try to get the dataset assuming it is an exported component
            try {
            	dataImporter.checkVotable(file);
            	dataset = dataImporter.importDatasetFromFile(file, type, info, null, null, dataType);
            } catch (NumberFormatException e) {
                // In this case we want to ignore the wrong format exception because
                //System.out.println(e.getMessage());
            }
            
            // If dataset is still null means we have a data file
            if (dataset == null) {
                String xUnit = messenger.requestInput(bundle.getString("REQUEST_X_UNIT"), Units.getAvailableUnitList());
                if (xUnit == null)
                    return null;
                if ("".equals(xUnit))
                    xUnit = null;
                boolean yUnitOk = false;
                String yUnit = null;
                while (!yUnitOk) {
                    yUnit = messenger.requestInput(bundle.getString("REQUEST_Y_UNIT"), Units.getAvailableUnitList());
                    if (yUnit == null) {
                        return null;
                    }
                    if ("".equals(yUnit)) {
                        yUnit = null;
                    }
                    if ("unitless".equals(yUnit)) {
                        yUnit = null;
                    }
                    yUnitOk = true;
                    // We do a check of the units for the template case
                    if (dataType == Dataset.DataType.TEMPLATE && yUnit != null && yUnit.contains("/" + xUnit)) {
                        yUnitOk = false;
                        messenger.warn(MessageFormat.format(bundle.getString("TEMPLATE_Y_UNIT_ERROR"), xUnit, yUnit));
                    }
                }
                dataset = dataImporter.importDatasetFromFile(file, type, info, xUnit, yUnit, dataType);
            }
        } catch (FileNotFoundException ex) {
            logger.error("Importing dataset failed because file " + file + " does not exist", ex);
            messenger.error(MessageFormat.format(bundle.getString("IMPORT_FILE_NOT_FOUND"), file));
        } catch (IOException ex) {
            logger.error("Importing dataset failed because there was a I/O error while reading the file.", ex);
            messenger.error(MessageFormat.format(bundle.getString("IMPORT_FILE_ERROR"), file));
        } catch (WrongFormatException ex) {
            logger.warn("Importing dataset failed because the file follows wrong format", ex);
            messenger.error(MessageFormat.format(bundle.getString("IMPORT_FILE_FORMAT_ERROR"), ex.getMessage()));
        }
        return dataset;
    }

    @Override
    public void deleteDataset(Type type, DatasetInfo datasetInfo) {
        // First get a confirmation from the user that he really wants to delete the dataset
        String message = MessageFormat.format(bundle.getString("DELETE_DATASET_CONFIRM"),
                getDatasetFullName(datasetInfo));
        List<String> options = new ArrayList<String>(2);
        options.add(bundle.getString("DELETE_OPTION"));
        options.add(bundle.getString("CANCEL_OPTION"));
        if(!options.get(0).equals(messenger.decision(message, options))) {
            return;
        }

        // Delete the dataset from the persistence device by using the DatasetManager
        // provided by the persistence framework
        logger.info("User requested deletion of dataset " +
                getDatasetFullName(datasetInfo));
        try {
            datasetManager.deleteDataset(type, datasetInfo);
            logger.info("Dataset was deleted successfully");
            message = MessageFormat.format(bundle.getString("DELETE_DATASET_SUCCESS"),
                getDatasetFullName(datasetInfo));
            messenger.info(message);
        } catch (NameNotFoundException ex) {
            logger.warn("Failed to delete dataset " + getDatasetFullName(datasetInfo) +
                    " because it does not exist in persistence device", ex);
            message = MessageFormat.format(bundle.getString("DELETE_DATASET_NOT_FOUND"),
                getDatasetFullName(datasetInfo));
            messenger.error(message);
        } catch (InUseException ex) {
            StringBuilder usersBuffer = new StringBuilder();
            for (String component : ex.getUsers()) {
                usersBuffer.append("\n");
                usersBuffer.append(component);
            }
            String users = usersBuffer.toString();
            users = users.substring(0, users.length());
            logger.warn("Failed to delete dataset " + getDatasetFullName(datasetInfo) +
                    " because it is in use by " + users, ex);
            message = MessageFormat.format(bundle.getString("DELETE_DATASET_IN_USE"),
                getDatasetFullName(datasetInfo), users);
            messenger.error(message);
        } catch (PersistenceDeviceException ex) {
            logger.warn("Failed to delete dataset " + getDatasetFullName(datasetInfo) +
                    " because there was an error while accessing the persistence device", ex);
            message = MessageFormat.format(bundle.getString("DELETE_DATASET_DEVICE_ERROR"),
                getDatasetFullName(datasetInfo));
            messenger.error(message);
        }
        uiManager.sessionModified();
    }

    private String getDatasetFullName(DatasetInfo datasetInfo) {
        String result = null;
        if (datasetInfo.getNamespace() != null && datasetInfo.getNamespace().length() != 0) {
            result = datasetInfo.getNamespace() + ".";
        } else {
            result = "";
        }
        result += datasetInfo.getName();
        return result;
    }

    @Override
    public void exportToFile(File file, Type type, DatasetInfo info) {
        try {
            dataExporter.exportDatasetToFile(file, type, info, datasetProvider);
        } catch (IOException ex) {
            logger.warn("Failed to export dataset", ex);
            String message = MessageFormat.format(bundle.getString("EXPORT_DATASET_ERROR"), info);
            messenger.error(message);
        }
    }

    @Override
    public void copyToNamespace(Type type, DatasetInfo datasetInfo, DatasetInfo newInfo) {
        logger.info("User requested to copy dataset " + datasetInfo + " to " + newInfo);
        try {
            datasetManager.copyToNamespace(type, datasetInfo, newInfo);
        } catch (NameNotFoundException ex) {
            logger.warn("Failed to copy dataset " + datasetInfo +
                    " because it does not exist in persistence device", ex);
            messenger.error(MessageFormat.format(bundle.getString("COPY_DATASET_NOT_FOUND"), datasetInfo));
        } catch (NameExistsException ex) {
            logger.warn("Failed to copy dataset " + datasetInfo + " to " + newInfo +
                    " because " + newInfo + " already exists", ex);
            messenger.error(MessageFormat.format(bundle.getString("COPY_DATASET_EXISTS"),
                datasetInfo, newInfo));
        } catch (PersistenceDeviceException ex) {
            logger.warn("Failed to copy dataset " + datasetInfo + " to " + newInfo +
                    " because of a persistence device error", ex);
            messenger.error(MessageFormat.format(bundle.getString("COPY_DATASET_DEVICE_ERROR"),
                datasetInfo, newInfo));
        }
    }

}
