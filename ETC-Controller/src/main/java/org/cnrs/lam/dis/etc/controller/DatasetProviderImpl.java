/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.controller;

import java.util.List;
import org.apache.log4j.Logger;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.Dataset.Type;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.cnrs.lam.dis.etc.datamodel.DatasetProvider;
import org.cnrs.lam.dis.etc.persistence.PersistenceDeviceException;
import org.cnrs.lam.dis.etc.persistence.DatasetManager;
import org.cnrs.lam.dis.etc.persistence.NameNotFoundException;

/**
 *
 * @author Nikolaos Apostolakos
 */
final class DatasetProviderImpl implements DatasetProvider {
    
    /** The logger of the class.*/
    private static Logger logger = Logger.getLogger(DatasetProviderImpl.class.getName());

    private DatasetManager datasetManager;

    DatasetProviderImpl(DatasetManager datasetManager) {
        this.datasetManager = datasetManager;
    }

    /**
     * Returns a Dataset object containing the data of the dataset described by
     * the given datasetInfo. The data are retrieved by using a DatasetManager
     * provided by the persistence framework. If there is no such dataset
     * in the persistence device the method returns null.
     * @param type The type of the dataset
     * @param datasetInfo The name and namespace of the dataset to retrieve
     * @return The data of the dataset or null if it is not available
     */
    @Override
    public Dataset getDataset(Type type, DatasetInfo datasetInfo) {
        return getDataset(type, datasetInfo, null);
    }

    /**
     * Returns a Dataset object containing the data of the specific option of the
     * multi-dataset described by
     * the given datasetInfo. The data are retrieved by using a DatasetManager
     * provided by the persistence framework. If there is no such dataset
     * in the persistence device the method returns null.
     * @param type The type of the dataset
     * @param datasetInfo The name and namespace of the dataset to retrieve
     * @param The option of the multi-dataset to retrieve
     * @return The data of the dataset or null if it is not available
     */
    @Override
    public Dataset getDataset(Type type, DatasetInfo datasetInfo, String option) {
        if (datasetInfo == null)
            return null;
        try {
            return datasetManager.loadDataset(type, datasetInfo, option);
        } catch (NameNotFoundException ex) {
            logger.error("The dataset " + datasetInfo.getName() +
                    " does not exist in the namespace " + datasetInfo.getNamespace(), ex);
            return null;
        } catch (PersistenceDeviceException ex) {
            logger.error("There was an error while retrieving the " +
                    "dataset " + datasetInfo.getNamespace() + "." +
                    datasetInfo.getName(), ex);
            return null;
        }
    }

    @Override
    public List<String> getDatasetOptionList(Type type, DatasetInfo datasetInfo) {
        if (datasetInfo == null)
            return null;
        try {
            return datasetManager.getDatasetOptionList(type, datasetInfo);
        } catch (NameNotFoundException ex) {
            logger.error("The dataset " + datasetInfo.getName() +
                    " does not exist in the namespace " + datasetInfo.getNamespace(), ex);
            return null;
        } catch (PersistenceDeviceException ex) {
            logger.error("There was an error while retrieving the " +
                    "datasets " + datasetInfo.getNamespace() + " option list." +
                    datasetInfo.getName(), ex);
            return null;
        }
    }

}
