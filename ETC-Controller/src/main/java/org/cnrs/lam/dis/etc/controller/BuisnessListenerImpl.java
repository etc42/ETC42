/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.controller;

import java.io.File;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.SwingWorker;
import org.apache.log4j.Logger;
import org.cnrs.lam.dis.etc.calculator.Calculator;
import org.cnrs.lam.dis.etc.calculator.ValidationError;
import org.cnrs.lam.dis.etc.calculator.Validator;
import org.cnrs.lam.dis.etc.calculator.util.CautionMessage;
import org.cnrs.lam.dis.etc.calculator.wavelengthrange.CommonRangeFactory;
import org.cnrs.lam.dis.etc.calculator.wavelengthrange.Imaging;
import org.cnrs.lam.dis.etc.calculator.wavelengthrange.WavelengthRangeFactory;
import org.cnrs.lam.dis.etc.configuration.ConfigFactory;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.cnrs.lam.dis.etc.datamodel.DatasetProvider;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.cnrs.lam.dis.etc.plugins.PluginException;
import org.cnrs.lam.dis.etc.plugins.PluginManager;
import org.cnrs.lam.dis.etc.ui.BuisnessListener;
import org.cnrs.lam.dis.etc.ui.Messenger;
import org.cnrs.lam.dis.etc.ui.UIManager;

/**
 *
 * @author Nikolaos Apostolakos
 */
final class BuisnessListenerImpl implements BuisnessListener {

    /** The bundle for retrieving the i18n strings for the messages.*/
    private static ResourceBundle bundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/controller/messages");
    /** The logger of the class.*/
    private static Logger logger = Logger.getLogger(BuisnessListenerImpl.class.getName());

    private Validator validator;
    private Calculator calculator;
    private Messenger messenger;
    private UIManager uiManager;
    private DatasetProvider datasetProvider;
    private PluginManager pluginManager;

    public BuisnessListenerImpl(Validator validator, Calculator calculator, Messenger messenger,
            UIManager uiManager, DatasetProvider datasetProvider, PluginManager pluginManager) {
        this.validator = validator;
        this.calculator = calculator;
        this.messenger = messenger;
        this.uiManager = uiManager;
        this.datasetProvider = datasetProvider;
        this.pluginManager = pluginManager;
    }

    @Override
    public void executeCalculation() {
        logger.info("A calculation is requested");
        
        // If the current session is invalid do not proceed with the calculation
        if (!validateCurrentSession()) {
            return;
        }

        // Because the calculation might take some time it is started in a new
        // thread and this method returns without waiting
        CalculationThread actionThread = new CalculationThread();

        // Notify the User Interface that a long action will take place
        messenger.actionStarted(bundle.getString("WAIT_FOR_CALCULATION"), actionThread);

        // Because the calculation might take some time it is started in a new
        // thread and this method returns without waiting
        actionThread.start();
    }

    /**
     * Validates the current session and if the validation fails it notifies
     * the user about the validation reasons.
     * @return True if the validation is successful, false if it fails
     */
    private boolean validateCurrentSession() {
        List<String> validationErrors = validator.validateSession(CurrentSessionContainer.getCurrentSession());
        if (!validationErrors.isEmpty()) {
            logger.info("Calculation aborted because the session validation failed");
            StringBuilder message = new StringBuilder();
            message.append(bundle.getString("SESSION_VALIDATION_ERROR"));
            for (String error : validationErrors) {
                message.append(ConfigFactory.getConfig().getLineSeparator());
                message.append(error);
            }
            messenger.error(message.toString());
        }
        return validationErrors.isEmpty();
    }

    @Override
    public void runPlugin(Class plugin) {
        logger.info("Launching plugin " + plugin.getName());
        try {
            pluginManager.runPlugin(plugin);
        } catch (PluginException ex) {
            logger.error("Failed to launch plugin. Reason : " + ex.getMessage(), ex);
            messenger.error("Failed to lunch the plugin. Please see log file for more details.");
        }
    }
        

    private class CalculationThread extends Thread {
        @Override
        public void run() {
            long timeBefore = System.currentTimeMillis();
            // Perform the calculation
            CalculationResults result = null;
            try {
            	Session session = CurrentSessionContainer.getCurrentSession();
                result = calculator.performCalculation(session, datasetProvider);
                if(CautionMessage.getInstance().getMessages().size() > 0) {
                    for(String key : CautionMessage.getInstance().getMessages().keySet()) {
                        messenger.info(CautionMessage.getInstance().getMessages().get(key).toString());
                        CautionMessage.getInstance().removeMessage(key);
                    }
                }

                // The calculation was performed successfully, so we present the
                // result to the user.
                uiManager.showCalculationResult(result);
                
                long timeAfter = System.currentTimeMillis();
                System.out.println("Execution time : "+((timeAfter-timeBefore)/1000)+"s.");
            } catch (ValidationError ex) {
                logger.error("The calculation failed because the session is not valid", ex);
                messenger.error(bundle.getString("SESSION_VALIDATION_ERROR") + ConfigFactory.getConfig().getLineSeparator() + ex.getMessage());
            } catch (Exception ex) {
                // Here we know that the calculation is not successful. We notify
                // the user interface that the action is stoped (to give control
                // back to the user) and we present an error message
                logger.error("The calculation failed because: " + ex.getMessage(), ex);
                messenger.error("Error : " + ex.toString());
                // We do not want to present any results, so we return here
            } finally {
            	messenger.actionStoped(this);
            	
            	// To reset warning about range
                // CommonRangeFactory.setCanDisplayRangeError(true);
			}
            return;
        }
    }

    @Override
    public void addPluginsFromJar(File jarFile) {
        try {
            pluginManager.addPluginsFromJar(jarFile);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            messenger.error("Failed to import plugins from jar because:"
                    + ConfigFactory.getConfig().getLineSeparator() + ex.getMessage());
        }
        uiManager.pluginListChanged();
    }

    @Override
    public void reloadPlugins() {
        new SwingWorker() {
            @Override
            protected Object doInBackground() throws Exception {
                messenger.actionStarted(bundle.getString("RELOADING_PLUGINS_MESSAGE"), Thread.currentThread());
                pluginManager.reloadPlugins();
                messenger.actionStoped(Thread.currentThread());
                return null;
            }
            @Override
            protected void done() {
                uiManager.pluginListChanged();
            }
        }.execute();
    }

}
