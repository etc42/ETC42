/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;
import org.cnrs.lam.dis.etc.datamodel.ComponentInfo;
import org.cnrs.lam.dis.etc.datamodel.Dataset.Type;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.cnrs.lam.dis.etc.persistence.PersistenceDeviceException;
import org.cnrs.lam.dis.etc.persistence.DatasetManager;
import org.cnrs.lam.dis.etc.persistence.SessionManager;
import org.cnrs.lam.dis.etc.plugins.PluginManager;
import org.cnrs.lam.dis.etc.ui.InfoProvider;
import org.cnrs.lam.dis.etc.ui.Messenger;
import org.javatuples.Triplet;

/**
 * The <code>InfoProviderImpl</code> class is the implementation of the
 * <code>InfoProvider</code> the <b>Controller</b> will give to the <b>User
 * Interface</b> for retrieving information about the available datasets,
 * instruments, sites, sources and observing parameters.
 *
 * @author Nikolaos Apostolakos
 */
final class InfoProviderImpl implements InfoProvider {

    /** The bundle for retrieving the i18n strings for the messages.*/
    private static ResourceBundle bundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/controller/messages");
    /** The logger of the class.*/
    private static Logger logger = Logger.getLogger(InfoProviderImpl.class.getName());
    
    /** The DatasetManager which is used for retrieving the available datasets.*/
    private DatasetManager datasetManager;
    /** The SessionManager which is used for retrieving the available components.*/
    private SessionManager sessionManager;
    /** The messenger which is used for communication with the user.*/
    private Messenger messenger;
    private PluginManager pluginManager;

    InfoProviderImpl(DatasetManager datasetManager, SessionManager sessionManager,
            Messenger messenger, PluginManager pluginManager) {
        this.datasetManager = datasetManager;
        this.sessionManager = sessionManager;
        this.messenger = messenger;
        this.pluginManager = pluginManager;
    }

    /**
     * Returns a list of the datasets of the fiven type, which are available
     * for loading. The method uses for retrieving the list the DatasetManager
     * provided by the persistence framework. If there is any error during
     * the retrieval of the list from the persistence device, this method will
     * notify the User Interface by the Messenger interface for the error and
     * then it will return an emtpy list. If the empty string is passed as
     * namespace ("", not null), then all the datasets of the given type are returned.
     * @param type The type of the datasets to retrieve the list for
     * @return A list with information about the available datasets for loading
     */
    @Override
    public List<DatasetInfo> getAvailableDatasetList(Type type, String namespace) {
        try {
            return datasetManager.getAvailableDatasetList(type, namespace);
        } catch (PersistenceDeviceException ex) {
            logger.error("Failed to retrieve dataset list from the persistence device", ex);
            messenger.error(bundle.getString("RETRIEVE_DATASET_LIST_ERROR"));
            return new ArrayList<DatasetInfo>(0);
        }
    }

    //TODO: Implement to return the sessions list
    @Override
    public List<String> getAvailableSessionList() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Returns a list of the instruments which are available
     * for loading. The method uses for retrieving the list the DatasetManager
     * provided by the persistence framework. If there is any error during
     * the retrieval of the list from the persistence device, this method will
     * notify the User Interface by the Messenger interface for the error and
     * then it will return an emtpy list. The returned list will contain also
     * the current instrument, even if it is not persisted.
     * @return A list with information about the available instruments for loading
     */
    @Override
    public List<ComponentInfo> getAvailableInstrumentList() {
        try {
            List<ComponentInfo> availableInstrumentList = sessionManager.getAvailableInstrumentList();
            ComponentInfo currentInfo = CurrentSessionContainer.getCurrentSession().getInstrument().getInfo();
            if (!availableInstrumentList.contains(currentInfo)) {
                availableInstrumentList.add(currentInfo);
            }
            return availableInstrumentList;
        } catch (PersistenceDeviceException ex) {
            messenger.error(bundle.getString("RETRIEVE_INSTRUMENT_LIST_ERROR"));
            logger.error("Failed to retrieve instrument list from the persistence device", ex);
            return new ArrayList<ComponentInfo>(0);
        }
    }

    /**
     * Returns a list of the sites which are available
     * for loading. The method uses for retrieving the list the DatasetManager
     * provided by the persistence framework. If there is any error during
     * the retrieval of the list from the persistence device, this method will
     * notify the User Interface by the Messenger interface for the error and
     * then it will return an emtpy list. The returned list will contain also
     * the current site, even if it is not persisted.
     * @return A list with information about the available sites for loading
     */
    @Override
    public List<ComponentInfo> getAvailableSiteList() {
        try {
            List<ComponentInfo> availableSiteList = sessionManager.getAvailableSiteList();
            ComponentInfo currentInfo = CurrentSessionContainer.getCurrentSession().getSite().getInfo();
            if (!availableSiteList.contains(currentInfo)) {
                availableSiteList.add(currentInfo);
            }
            return availableSiteList;
        } catch (PersistenceDeviceException ex) {
            messenger.error(bundle.getString("RETRIEVE_SITE_LIST_ERROR"));
            logger.error("Failed to retrieve site list from the persistence device", ex);
            return new ArrayList<ComponentInfo>(0);
        }
    }

    /**
     * Returns a list of the sources which are available
     * for loading. The method uses for retrieving the list the DatasetManager
     * provided by the persistence framework. If there is any error during
     * the retrieval of the list from the persistence device, this method will
     * notify the User Interface by the Messenger interface for the error and
     * then it will return an emtpy list. The returned list will contain also
     * the current source, even if it is not persisted.
     * @return A list with information about the available sources for loading
     */
    @Override
    public List<ComponentInfo> getAvailableSourceList() {
        try {
            List<ComponentInfo> availableSourceList = sessionManager.getAvailableSourceList();
            ComponentInfo currentInfo = CurrentSessionContainer.getCurrentSession().getSource().getInfo();
            if (!availableSourceList.contains(currentInfo)) {
                availableSourceList.add(currentInfo);
            }
            return availableSourceList;
        } catch (PersistenceDeviceException ex) {
            messenger.error(bundle.getString("RETRIEVE_SOURCE_LIST_ERROR"));
            logger.error("Failed to retrieve source list from the persistence device", ex);
            return new ArrayList<ComponentInfo>(0);
        }
    }

    /**
     * Returns a list of the observing parameters which are available
     * for loading. The method uses for retrieving the list the DatasetManager
     * provided by the persistence framework. If there is any error during
     * the retrieval of the list from the persistence device, this method will
     * notify the User Interface by the Messenger interface for the error and
     * then it will return an emtpy list. The returned list will contain also
     * the current observing parameters, even if it is not persisted.
     * @return A list with information about the available obs param for loading
     */
    @Override
    public List<ComponentInfo> getAvailableObsParamList() {
        try {
            List<ComponentInfo> availableObsParamList = sessionManager.getAvailableObsParamList();
            ComponentInfo currentInfo = CurrentSessionContainer.getCurrentSession().getObsParam().getInfo();
            if (!availableObsParamList.contains(currentInfo)) {
                availableObsParamList.add(currentInfo);
            }
            return availableObsParamList;
        } catch (PersistenceDeviceException ex) {
            messenger.error(bundle.getString("RETRIEVE_OBS_PARAM_LIST_ERROR"));
            logger.error("Failed to retrieve observing parameters list from the persistence device", ex);
            return new ArrayList<ComponentInfo>(0);
        }
    }

    @Override
    public List<Triplet<Class, List<String>, String>> getAvailablePluginList() {
        return pluginManager.getAvailablePlugins();
    }

}
