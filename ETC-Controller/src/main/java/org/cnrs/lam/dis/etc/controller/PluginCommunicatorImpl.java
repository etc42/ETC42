/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.controller;

import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.cnrs.lam.dis.etc.calculator.Calculator;
import org.cnrs.lam.dis.etc.dataimportexport.DatasetImpl;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.Dataset.Type;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.cnrs.lam.dis.etc.datamodel.DatasetProvider;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.cnrs.lam.dis.etc.persistence.DatasetManager;
import org.cnrs.lam.dis.etc.persistence.PersistenceDeviceException;
import org.cnrs.lam.dis.etc.plugins.CalculationFailed;
import org.cnrs.lam.dis.etc.plugins.DatasetCreationException;
import org.cnrs.lam.dis.etc.plugins.NameExistsException;
import org.cnrs.lam.dis.etc.plugins.PluginCommunicator;
import org.cnrs.lam.dis.etc.ui.InfoProvider;
import org.cnrs.lam.dis.etc.ui.UIManager;

/**
 */
public class PluginCommunicatorImpl implements PluginCommunicator {

    private static Logger logger = Logger.getLogger(PluginCommunicatorImpl.class.getName());
    private DatasetProvider datasetProvider;
    private InfoProvider infoProvider;
    private DatasetManager datasetManager;
    private UIManager uiManager;
    private Calculator calculator;

    public PluginCommunicatorImpl(DatasetProvider datasetProvider, InfoProvider infoProvider
            , DatasetManager datasetManager, UIManager uiManager, Calculator calculator) {
        this.datasetProvider = datasetProvider;
        this.infoProvider = infoProvider;
        this.datasetManager = datasetManager;
        this.uiManager = uiManager;
        this.calculator = calculator;
    }

    @Override
    public Session getCurrentSession() {
        return CurrentSessionContainer.getCurrentSession();
    }

    @Override
    public Dataset getDataset(Type type, DatasetInfo datasetInfo, String option) {
        return datasetProvider.getDataset(type, datasetInfo, option);
    }

    @Override
    public List<DatasetInfo> getAvailableDatasetList(Type type, String namespace) {
        if (namespace == null || namespace.equals(""))
            return null;
        return infoProvider.getAvailableDatasetList(type, namespace);
    }

    @Override
    public void createDataset(Type type, DatasetInfo info, String xUnit
            , String yUnit, Map<Double, Double> data)
            throws NameExistsException, DatasetCreationException {
        try {
            DatasetImpl dataset = new DatasetImpl(info, type, data, xUnit, yUnit, Dataset.DataType.FUNCTION);
            datasetManager.saveDataset(dataset);
        } catch (org.cnrs.lam.dis.etc.persistence.NameExistsException ex) {
            logger.info("Plugin failed to create dataset because name already existed", ex);
            throw new NameExistsException();
        } catch (PersistenceDeviceException ex) {
            logger.info("Plugin failed to create dataset because of a persistence exception", ex);
            throw new DatasetCreationException("Failed to create dataset", ex);
        }
        updateCurrentSessionForDataset(type, info);
    }

    @Override
    public void createMultiDataset(Type type, DatasetInfo info, String xUnit, String yUnit,
            Map<String, Map<Double, Double>> data) throws NameExistsException, DatasetCreationException {
        try {
            datasetManager.saveMultiDataset(type, info, xUnit, yUnit, data, type.getPossibleDataTypes()[0]);
        } catch (org.cnrs.lam.dis.etc.persistence.NameExistsException ex) {
            logger.info("Plugin failed to create multi-dataset because name already existed", ex);
            throw new NameExistsException();
        } catch (PersistenceDeviceException ex) {
            logger.info("Plugin failed to create multi-dataset because of a persistence exception", ex);
            throw new DatasetCreationException(ex.getMessage(), ex);
        }
        updateCurrentSessionForDataset(type, info);
    }

    private void updateCurrentSessionForDataset(Type type, DatasetInfo info) {
        // First update the session instance
        Session currentSession = CurrentSessionContainer.getCurrentSession();
        switch (type) {
            case FILTER_TRANSMISSION:
                currentSession.getInstrument().setFilterTransmission(info);
                break;
            case FWHM:
                currentSession.getInstrument().setFwhm(info);
                break;
            case SPECTRAL_RESOLUTION:
                currentSession.getInstrument().setSpectralResolution(info);
                break;
            case TRANSMISSION:
                currentSession.getInstrument().setTransmission(info);
                break;
            case ZODIACAL:
                currentSession.getSite().setZodiacalProfile(info);
                break;
            case GALACTIC:
                currentSession.getSite().setGalacticProfile(info);
                break;
            case SKY_ABSORPTION:
                currentSession.getSite().setSkyAbsorption(info);
                break;
            case SKY_EMISSION:
                currentSession.getSite().setSkyEmission(info);
                break;
            case SKY_EXTINCTION:
                currentSession.getSite().setSkyExtinction(info);
                break;
            case SPECTRAL_DIST_TEMPLATE:
                currentSession.getSource().setSpectralDistributionTemplate(info);
                break;
        }

        // Finally notify the UIManager that the session has been modified to
        // update the users view
        uiManager.sessionModified();
    }

    @Override
    public void sessionModified() {
        uiManager.sessionModified();
    }

    @Override
    public CalculationResults runSimulation() throws CalculationFailed {
        try {
            return calculator.performCalculation(getCurrentSession(), datasetProvider);
        } catch (Exception ex) {
            throw new CalculationFailed(ex.getMessage(), ex);
        }
    }

    @Override
    public void showResults(CalculationResults results) {
        uiManager.showCalculationResult(results);
    }
}
