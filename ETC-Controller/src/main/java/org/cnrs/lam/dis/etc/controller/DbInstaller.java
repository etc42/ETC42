/*
 * =============================================================================
 *
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 *
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 *
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import org.apache.log4j.Logger;
import org.cnrs.lam.dis.etc.configuration.ConfigFactory;
import org.cnrs.lam.dis.etc.dataimportexport.DataImporter;
import org.cnrs.lam.dis.etc.dataimportexport.WrongFormatException;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.cnrs.lam.dis.etc.persistence.DatasetManager;
import org.javatuples.Pair;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Nikolaos Apostolakos
 */
class DbInstaller {
    
    private static Logger logger = Logger.getLogger(DbInstaller.class);
    
    public static void installDb(String dbDir) {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection("jdbc:derby:db;create=true");
            Statement statement = connection.createStatement();
            createTables(statement);
            statement.close();
        } catch (IOException ex) {
            logger.error("DbInstaller failed to read the SQL script", ex);
            System.exit(-1);
        } catch (SQLException ex) {
            logger.error("DbInstaller failed to create the database", ex);
            System.exit(-1);
        } finally {
            closeConnection(connection);
        }
    }
    
    private static void closeConnection(Connection connection) {
        if (connection == null)
            return;
        try {
            connection.close();
        } catch (SQLException ex) {
            logger.warn("DbInstaller failed to close database connection", ex);
        }
    }
    
    private static void createTables(Statement statement) throws IOException, SQLException {
        logger.info("Creating default database tables...");
        InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream("sql/CreateTables.sql");
        BufferedReader script = new BufferedReader(new InputStreamReader(in));
        while (script.ready()) {
            StringBuilder command = new StringBuilder();
            String line;
            do {
                line = script.readLine();
                if (!line.startsWith("--")) {
                    if (line.indexOf("--") != -1) {
                        line = line.substring(0, line.indexOf("--"));
                    }
                    command.append(line);
                }
            } while (line.indexOf(';') == -1);
            String commandStr = command.toString();
            commandStr = commandStr.substring(0, commandStr.lastIndexOf(';'));
            logger.info(commandStr);
            statement.execute(commandStr);
        }
        logger.info("Default database tables created successfully");
    }
    
    static Pair<Scanner, Boolean> getDatasetList(URL url, DataImporter dataImporter, DatasetManager datasetManager) throws IOException {
    	InputStream inStream;
        boolean isDefault;
        if (url == null) {
            logger.info("Initializing default database with default datasets...");
            inStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("installdata/datasets/datasetList");
            isDefault = true;
        }
        else {
            logger.info("Importing datasets from " + url.toString());
            inStream = url.openStream();
            isDefault = false;
        }
        Pair<Scanner, Boolean> in = new Pair<Scanner, Boolean>(new Scanner(inStream), isDefault);
        return in;
    }
    
    static void importDatasetList(URL url, DataImporter dataImporter, DatasetManager datasetManager) throws IOException {
    	Pair<Scanner, Boolean> inPair = getDatasetList(url, dataImporter, datasetManager);
    	Scanner in = inPair.getValue0();
    	boolean isDefault = inPair.getValue1();
        in.useDelimiter("}");
        while (in.hasNext()) {
            String datasetInfo = in.next();
            datasetInfo = datasetInfo.substring(datasetInfo.indexOf('{')+1).trim();
            importDataset(datasetInfo, dataImporter, datasetManager, isDefault);
        }
        logger.info("Default database initialized successfully");
    }
    
    static void handleJSON(String name, DataImporter dataImporter, DatasetManager datasetManager)
            throws IOException, SQLException {
        String server = ConfigFactory.getConfig().getFixturesUrlsDefault();
        
        if (System.getProperty("etc.server") != null) {
            server = System.getProperty("etc.server");
        }
        
        logger.info("Importing fixtures " + name + " from JSON file" );
        if (!server.endsWith("/"))
            server += "/";
        String jsonString = "";
        BufferedReader json = new BufferedReader(new InputStreamReader(new URL(server + "fixtures.json").openStream()));
        while (json.ready()) {
            jsonString += json.readLine();
        }
        JSONArray jsonArray = new JSONArray(jsonString);
        JSONObject jObject = jsonArray.getJSONObject(0);
        int i = 0;
        while (!jObject.getString("fixtures_name").equalsIgnoreCase(name) && ++i < jsonArray.length()) {
            jObject = jsonArray.getJSONObject(i);
        }
        if (i >= jsonArray.length()) {
           logger.info("Fixtures " + name + " not found in JSON file");
           return;
        }
        jsonArray = jObject.getJSONArray("content");
        jObject = jsonArray.getJSONObject(0);
        i = 0;
        while (!jObject.getString("version").equals(ConfigFactory.getConfig().getVersion()) && ++i < jsonArray.length()) {
            jObject = jsonArray.getJSONObject(i);
        }
        if (i >= jsonArray.length()) {
           logger.info("Current version not found in JSON file for fixtures " + name);
           return;
        }
        jsonArray = jObject.getJSONArray("content");
        for (i = 0; i < jsonArray.length(); i++) {
            jObject = jsonArray.getJSONObject(i);
            URL url = new URL(jObject.getString("name"));
            String type = jObject.getString("type");
            if (type.equalsIgnoreCase("sql"))
                importSqlScript(url);
            else if (type.equalsIgnoreCase("datasetlist"))
                importDatasetList(url, dataImporter, datasetManager);
        }
        logger.info("JSON file handled successfully");
    }
    
    static void importSqlScript(URL url) throws IOException, SQLException {
        Connection connection = DriverManager.getConnection("jdbc:derby:db;create=true");
        Statement statement   = connection.createStatement();
        logger.info("Initializing default database from " + url.toString());
        BufferedReader script = new BufferedReader(new InputStreamReader(url.openStream()));
        
        while (script.ready()) {
            StringBuilder command = new StringBuilder();
            String line;
            do {
                line = script.readLine().trim();
                if(line != null && line.trim().length() > 0){
                    if (!line.startsWith("--")) {
                        if (line.indexOf("--") != -1) {
                            line = line.substring(0, line.indexOf("--"));
                        }
                        command.append(line);
                    }
                }
            } while (line != null && line.trim().length() > 0 && line.indexOf(';') == -1);
            String commandStr = command.toString();
            
            if(command.length() > 0){
                commandStr = commandStr.substring(0, commandStr.lastIndexOf(';'));
                logger.info(commandStr);
                statement.execute(commandStr);
            }
        }
        logger.info("Default database tables created successfully");
    }
    
    public static void importDataset(String datasetInfo, DataImporter dataImporter, DatasetManager datasetManager, boolean isDefault) {
        logger.info("Importing default dataset...");
        Map<String, String> datasetInfoMap = new HashMap<String, String>();
        for (String pair : datasetInfo.split("\n")) {
            logger.info(pair.trim());
            if(pair.indexOf('=') >= 0){
                String key = pair.substring(0, pair.indexOf('=')).trim();
                String value = pair.substring(pair.indexOf('=')+1).trim();
                datasetInfoMap.put(key, value);
            }
        }
        try {
            if (datasetInfoMap.containsKey("file")) { // Single dataset
                Dataset dataset = readDataset(datasetInfoMap, dataImporter, isDefault);
                datasetManager.saveDataset(dataset);
            } else { // multi dataset
                Map<String, Map<Double, Double>> optionList = new HashMap<String, Map<Double, Double>>();
                Dataset dataset = null;
                for (int i=1; datasetInfoMap.containsKey("option"+i); i++) {
                    String option = datasetInfoMap.get("option"+i);
                    logger.info("Handling option " + option + " of multi-dataset");
                    datasetInfoMap.put("file", datasetInfoMap.get("file"+i));
                    dataset = readDataset(datasetInfoMap, dataImporter, isDefault);
                    optionList.put(option, dataset.getData());
                }
                datasetManager.saveMultiDataset(dataset.getType(), dataset.getInfo(), dataset.getXUnit()
                        , dataset.getYUnit(), optionList, dataset.getDataType());
            }
            logger.info("Default dataset imported successfully");
        } catch (Exception ex) {
            logger.error("Failed to import dataset from file " + datasetInfoMap.get("file") + ". Reason: " + ex.getMessage(), ex);
        }
    }
    
    private static Dataset readDataset(Map<String, String> datasetInfoMap
            , DataImporter dataImporter, boolean isDefault) throws IOException, WrongFormatException {
        String file = datasetInfoMap.get("file");
        InputStream inStream;
        if (isDefault)
            inStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("installdata/datasets/" + file);
        else
            inStream = new URL(file).openStream();
        File dataFile = new File(ConfigFactory.getConfig().getTempDir(), file);
        dataFile.getParentFile().mkdirs();
        dataFile.deleteOnExit();
        BufferedReader in = new BufferedReader(new InputStreamReader(inStream));
        FileWriter out = new FileWriter(dataFile);
        String line;
        while ((line=in.readLine()) != null) {
            out.write(line + "\n");
        }
        in.close();
        out.close();
        DatasetInfo info = new DatasetInfo(datasetInfoMap.get("name"), null, datasetInfoMap.get("description"));
        Dataset.Type type = Dataset.Type.valueOf(datasetInfoMap.get("type"));
        Dataset.DataType dataType = Dataset.DataType.valueOf(datasetInfoMap.get("dataType"));
        return dataImporter.importDatasetFromFile(dataFile, type, info, datasetInfoMap.get("xUnit"), datasetInfoMap.get("yUnit"), dataType);
    }
}
