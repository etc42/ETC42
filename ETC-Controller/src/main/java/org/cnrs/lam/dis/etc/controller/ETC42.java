/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.controller;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.cnrs.lam.dis.etc.calculator.Calculator;
import org.cnrs.lam.dis.etc.calculator.CalculatorFactory;
import org.cnrs.lam.dis.etc.calculator.Validator;
import org.cnrs.lam.dis.etc.configuration.ConfigFactory;
import org.cnrs.lam.dis.etc.dataimportexport.DataExporter;
import org.cnrs.lam.dis.etc.dataimportexport.DataImportExportFactory;
import org.cnrs.lam.dis.etc.dataimportexport.DataImporter;
import org.cnrs.lam.dis.etc.dataimportexport.SampController;
import org.cnrs.lam.dis.etc.datamodel.ComponentInfo;
import org.cnrs.lam.dis.etc.datamodel.DatasetProvider;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.cnrs.lam.dis.etc.persistence.DatasetManager;
import org.cnrs.lam.dis.etc.persistence.NameExistsException;
import org.cnrs.lam.dis.etc.persistence.PersistenceDeviceException;
import org.cnrs.lam.dis.etc.persistence.PersistenceFactory;
import org.cnrs.lam.dis.etc.persistence.SessionManager;
import org.cnrs.lam.dis.etc.plugins.PluginCommunicator;
import org.cnrs.lam.dis.etc.plugins.PluginFactory;
import org.cnrs.lam.dis.etc.plugins.PluginManager;
import org.cnrs.lam.dis.etc.ui.BuisnessListener;
import org.cnrs.lam.dis.etc.ui.DatasetListener;
import org.cnrs.lam.dis.etc.ui.InfoProvider;
import org.cnrs.lam.dis.etc.ui.InstrumentListener;
import org.cnrs.lam.dis.etc.ui.Messenger;
import org.cnrs.lam.dis.etc.ui.ObsParamListener;
import org.cnrs.lam.dis.etc.ui.SiteListener;
import org.cnrs.lam.dis.etc.ui.SourceListener;
import org.cnrs.lam.dis.etc.ui.UIFactory;
import org.cnrs.lam.dis.etc.ui.UIManager;
import org.cnrs.lam.dis.etc.ui.generic.SampListenerHolder;
import org.cnrs.lam.dis.etc.ui.swing.SplashScreen;
import org.cnrs.lam.dis.etc.ui.swing.importer.FileImportWorker;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class ETC42 {

	private static final Logger logger = Logger.getLogger(ETC42.class);

	public static void main(String[] args){
		// Initialize the logging
		initializeLogger();

		SplashScreen.getInstance().setProgression("Initialization");

		// Set the directory where the database is stored
		System.setProperty("derby.system.home", ConfigFactory.getConfig().getEtcHome());

		logger.info("***** ETC launched *****");
		logger.info("Available memory: " + Runtime.getRuntime().maxMemory());
		ConfigFactory.getConfig().setCommandLineArguments(args);
		// Do the ETC installation if necessary
		/*if (System.getProperty("etc.fixtures") != null) {
            logger.info("Cleaning database for fixtures");
            try {
                FileUtils.deleteDirectory(new File(ConfigFactory.getConfig().getEtcHome() + ConfigFactory.getConfig().getFileSeparator() + "db"));
                logger.info("Database cleansed");
            } catch (IOException ex) {
                logger.info("Database already empty");
            }
        }*/
		Installer.install();

		// Get the objects from persistence module
		DatasetManager datasetManager = PersistenceFactory.getDatasetManager();
		SessionManager sessionManager = PersistenceFactory.getSessionManager();

		SplashScreen.getInstance().setProgression("Loading");
		Messenger messenger = UIFactory.getMessenger();

		SplashScreen.getInstance().setProgression("Loading plugin manager");
		PluginManager pluginManager = PluginFactory.getPluginManager();

		SplashScreen.getInstance().setProgression("Loading info provider");
		InfoProvider infoProvider = new InfoProviderImpl(datasetManager, sessionManager, messenger, pluginManager);

		SplashScreen.getInstance().setProgression("Loading user interface");
		UIManager uiManager = UIFactory.getUIManager(infoProvider);

		SplashScreen.getInstance().setProgression("Loading calculator");
		Calculator calculator = CalculatorFactory.getCalculator();
		Validator validator = CalculatorFactory.getValidator();

		SplashScreen.getInstance().setProgression("Loading dataset provider");
		DatasetProvider datasetProvider = new DatasetProviderImpl(datasetManager);

		SplashScreen.getInstance().setProgression("Loading plugin comunicator");
		PluginCommunicator pluginCommunicator = new PluginCommunicatorImpl(datasetProvider, infoProvider, datasetManager, uiManager, calculator);
		pluginManager.setCommunicator(pluginCommunicator);

		SplashScreen.getInstance().setProgression("Loading location component");
		pluginManager.setLocationComponent(UIFactory.getLocationComponent());

		SplashScreen.getInstance().setProgression("Loading buisness listener");
		BuisnessListener buisnessListener = new BuisnessListenerImpl(validator, calculator, messenger, uiManager, datasetProvider, pluginManager);
		uiManager.setBuisnessListener(buisnessListener);

		SplashScreen.getInstance().setProgression("Loading data importer/exporter");
		DataExporter dataExporter = DataImportExportFactory.getDataExporter();
		DataImporter dataImporter = DataImportExportFactory.getDataImporter();

		SplashScreen.getInstance().setProgression("Loading instrument listener");
		InstrumentListener instrumentListener = new InstrumentListenerImpl(sessionManager, datasetManager, messenger, uiManager, dataExporter, dataImporter, datasetProvider);
		uiManager.setInstrumentListener(instrumentListener);

		SplashScreen.getInstance().setProgression("Loading site listener");
		SiteListener siteListener = new SiteListenerImpl(sessionManager, datasetManager, messenger, uiManager, dataExporter, dataImporter, datasetProvider);
		uiManager.setSiteListener(siteListener);

		SplashScreen.getInstance().setProgression("Loading source listener");
		SourceListener sourceListener = new SourceListenerImpl(sessionManager, datasetManager, messenger, uiManager, dataExporter, dataImporter, datasetProvider);
		uiManager.setSourceListener(sourceListener);

		SplashScreen.getInstance().setProgression("Loading observation parameters listener");
		ObsParamListener obsParamListener = new ObsParamListenerImpl(sessionManager, datasetManager, messenger, uiManager, dataExporter, dataImporter, datasetProvider);
		uiManager.setObsParamListener(obsParamListener);

		SplashScreen.getInstance().setProgression("Loading dataset listener");
		DatasetListener datasetListener = new DatasetListenerImpl(datasetManager, messenger, uiManager, dataImporter, dataExporter, datasetProvider);
		uiManager.setDatasetListener(datasetListener);

		SplashScreen.getInstance().setProgression("Loading SAMP controller");
		SampController sampController = DataImportExportFactory.getSampController();
		SampImportListener sampImportListener = new SampImportListener(messenger, datasetManager, uiManager);
		sampController.setSampListener(sampImportListener);
		SampUiListener sampUiListener = new SampUiListener(sampController, messenger);
		uiManager.setSampListener(sampUiListener);

		SplashScreen.getInstance().setProgression("Loading content");
		Session session = new SessionImpl();
		if (Installer.isNewDatabase()) {
			try {
				DbInstaller.importDatasetList(null, dataImporter, datasetManager);
			} catch (IOException ex) {
				logger.error("Failed to import default datasets", ex);
			}
		}
		if (System.getProperty("etc.fixtures") != null) {
			String[] fixtures = System.getProperty("etc.fixtures").replace(" ","").split(",");

			for (String fixture : fixtures) {
				try {
					DbInstaller.handleJSON(fixture, dataImporter, datasetManager);
				} catch (IOException ex) {
					logger.error(fixture + " not found", ex);
				} catch (SQLException ex) {
					logger.error("Failed to import "+fixture, ex);
				}
			}
		}

		SplashScreen.getInstance().setProgression("Loading session");        
		try {
			if (sessionManager.getAvailableInstrumentList().contains(new ComponentInfo(System.getProperty("etc.instrument"), null)))
				session.setInstrument(sessionManager.loadInstrument(new ComponentInfo(System.getProperty("etc.instrument"), null)));
			else 
				session.setInstrument(sessionManager.createNewInstrument(new ComponentInfo("new", null)));
			if (sessionManager.getAvailableSiteList().contains(new ComponentInfo(System.getProperty("etc.site"), null)))
				session.setSite(sessionManager.loadSite(new ComponentInfo(System.getProperty("etc.site"), null)));
			else 
				session.setSite(sessionManager.createNewSite(new ComponentInfo("new", null)));
			session.setSource(sessionManager.createNewSource(new ComponentInfo("new", null)));
			session.setObsParam(sessionManager.createNewObsParam(new ComponentInfo("new", null)));
		} catch (NameExistsException ex) {
			logger.error("It appears that a component with name 'new' is persited", ex);
			System.exit(0);
		} catch (PersistenceDeviceException ex) {
			logger.error("Failure with the persistence device when accessing available componenents or creating the 'new' component", ex);
			System.exit(0);
		} catch (Throwable ex) {
			logger.error("It appears there is already an ETC instance connected to the database", ex);
			messenger.error(java.util.ResourceBundle.getBundle("org.cnrs.lam.dis.etc.controller.messages").getString("ALREADY_RUNNING_MESSAGE"));
			System.exit(0);
		}
		CurrentSessionContainer.setCurrentSession(session);
		uiManager.setSession(session);  
		
		if (Installer.isNewDatabase()) {
			SplashScreen.getInstance().setProgression("Installing default preset");
			FileImportWorker worker = null;
			try {
				worker = Installer.importPresets();
			} catch (IOException e1) {}
			if (worker != null) {				
				do {
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) { break; }
				} while(worker.isDone() == false);
			}
		}


		// Do any updates on the database structure if this version of ETC is using
		// a newer version of the ETC DB schema
		DbUpdater.updateDatabase();

		UIFactory.setFrameVisible(true);
		SplashScreen.getInstance().setProgression("Loading done !");
		
		// If we have a new installation we notify the UI manager to prompt for
		// importing components
		if (Installer.isNewDatabase() && System.getProperty("etc.fixtures") == null) {
			uiManager.handleFirstInstallation();
		}
		
		// Connects to the SAMP hub for the VO Launcher
		SampListenerHolder.getSampListener().connectToLauncher();
	}

	/**
	 * Initializes the log4j. The configuration of the logging is stored in the file log4j.properties
	 * in the ETC home directory (normally .ETC). This method is going to create this file if it
	 * does not exist with default values (saving in the file etc.log). If the name of the file
	 * to save the log is not set in the lof4j.properties, then the default file etc.log is used.
	 * This method also stops the logging of the EclipseLink from appearing on the console.
	 */
	static void initializeLogger() {
		// Turn off logging for EclipseLink
		System.setProperty("eclipselink.logging.level", "OFF");
		// Now setup the log4j for the ETC
		Properties properties = new Properties();
		String logConfigurationFile = ConfigFactory.getConfig().getEtcHome() + ConfigFactory.getConfig().getFileSeparator() + "log4j.properties";
		try {
			properties.load(new FileReader(logConfigurationFile));
		} catch (IOException ex) {
			System.out.println("Missing file " + logConfigurationFile + ". Creating default one.");
			properties.setProperty("log4j.rootLogger", "OFF, Rfa");
			properties.setProperty("log4j.logger.org.cnrs.lam.dis", "DEBUG");
			properties.setProperty("log4j.appender.Rfa", "org.apache.log4j.RollingFileAppender");
			properties.setProperty("log4j.appender.Rfa.MaxFileSize", "100KB");
			properties.setProperty("log4j.appender.Rfa.MaxBackupIndex", "2");
			properties.setProperty("log4j.appender.Rfa.layout", "org.apache.log4j.PatternLayout");
			properties.setProperty("log4j.appender.Rfa.layout.ConversionPattern", "%d [%t] %-5p %-50c - %m%n");
			try {
				properties.store(new FileWriter(logConfigurationFile), "Generated by ETC");
			} catch (IOException ex1) {
				// We don't really care if we fail to save the file, because this doesn't stop us from anything
				// So we just print out a message and we continue normally
				System.out.println("Failed to create file " + logConfigurationFile + ". Continuing normally...");
			}
		}
		if (properties.getProperty("log4j.appender.Rfa.File") == null) {
			String outFile = ConfigFactory.getConfig().getEtcHome() + ConfigFactory.getConfig().getFileSeparator() + "etc.log";
			properties.setProperty("log4j.appender.Rfa.File", outFile);
		}
		PropertyConfigurator.configure(properties);
	}

}
