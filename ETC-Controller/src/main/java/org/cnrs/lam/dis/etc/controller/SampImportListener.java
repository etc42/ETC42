/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.controller;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import org.cnrs.lam.dis.etc.dataimportexport.SampListener;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.cnrs.lam.dis.etc.persistence.DatasetManager;
import org.cnrs.lam.dis.etc.persistence.NameExistsException;
import org.cnrs.lam.dis.etc.persistence.PersistenceDeviceException;
import org.cnrs.lam.dis.etc.ui.Messenger;
import org.cnrs.lam.dis.etc.ui.UIManager;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class SampImportListener implements SampListener {

    private static ResourceBundle bundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/controller/messages");
    private Messenger messenger;
    private DatasetManager datasetManager;
    private UIManager uiManager;

    public SampImportListener(Messenger messenger, DatasetManager datasetManager, UIManager uiManager) {
        this.messenger = messenger;
        this.datasetManager = datasetManager;
        this.uiManager = uiManager;
    }

    @Override
    public boolean confirmImport() {
        String message = bundle.getString("IMPORT_FROM_SAMP_MESSAGE");
        List<String> list = new ArrayList<String>(2);
        list.add(bundle.getString("YES_OPTION"));
        list.add(bundle.getString("NO_OPTION"));
        String decision = messenger.decision(message, list);
        if (decision == null || decision.equals(list.get(1))) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public DatasetInfo getNewDatasetInfo() {
        DatasetInfo info = messenger.getNewDatasetInfo();
        String namespace = CurrentSessionContainer.getCurrentSession().getSource().getInfo().getName();
        return new DatasetInfo(info.getName(), namespace, info.getDescription());
    }

    @Override
    public void importFailed() {
        messenger.error(bundle.getString("IMPORT_SAMP_FAILED"));
    }

    @Override
    public void datasetImported(Dataset dataset) {
        try {
            datasetManager.saveDataset(dataset);
        } catch (NameExistsException ex) {
            messenger.error(MessageFormat.format(bundle.getString("IMPORT_NAME_EXISTS"), dataset.getInfo().getName()));
        } catch (PersistenceDeviceException ex) {
            messenger.error(bundle.getString("IMPORT_DEVICE_ERROR"));
        }
        CurrentSessionContainer.getCurrentSession().getSource().setSpectralDistributionTemplate(dataset.getInfo());
        uiManager.sessionModified();
    }

}
