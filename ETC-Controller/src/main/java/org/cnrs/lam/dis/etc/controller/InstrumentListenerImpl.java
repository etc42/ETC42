/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.controller;

import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;
import org.cnrs.lam.dis.etc.dataimportexport.DataExporter;
import org.cnrs.lam.dis.etc.dataimportexport.DataImporter;
import org.cnrs.lam.dis.etc.dataimportexport.InstrumentWithDatasets;
import org.cnrs.lam.dis.etc.dataimportexport.WrongFormatException;
import org.cnrs.lam.dis.etc.datamodel.ComponentInfo;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.DatasetProvider;
import org.cnrs.lam.dis.etc.datamodel.Instrument;
import org.cnrs.lam.dis.etc.persistence.DatasetManager;
import org.cnrs.lam.dis.etc.persistence.InUseException;
import org.cnrs.lam.dis.etc.persistence.NameExistsException;
import org.cnrs.lam.dis.etc.persistence.NameNotFoundException;
import org.cnrs.lam.dis.etc.persistence.PersistenceDeviceException;
import org.cnrs.lam.dis.etc.persistence.PersistenceFactory;
import org.cnrs.lam.dis.etc.persistence.SessionManager;
import org.cnrs.lam.dis.etc.ui.InstrumentListener;
import org.cnrs.lam.dis.etc.ui.Messenger;
import org.cnrs.lam.dis.etc.ui.UIManager;

/**
 *
 * @author Nikolaos Apostolakos
 */
final class InstrumentListenerImpl implements InstrumentListener {

    /** The bundle for retrieving the i18n strings for the messages.*/
    private static ResourceBundle bundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/controller/messages");
    /** The logger of the class.*/
    private static Logger logger = Logger.getLogger(InstrumentListenerImpl.class.getName());

    private SessionManager sessionManager;
    private DatasetManager datasetManager;
    private Messenger messenger;
    private UIManager uiManager;
    private DataExporter dataExporter;
    private DataImporter dataImporter;
    private DatasetProvider datasetProvider;

    public InstrumentListenerImpl(SessionManager sessionManager, DatasetManager datasetManager, Messenger messenger,
            UIManager uiManager, DataExporter dataExporter, DataImporter dataImporter,
            DatasetProvider datasetProvider) {
        this.sessionManager = sessionManager;
        this.datasetManager = datasetManager;
        this.messenger = messenger;
        this.uiManager = uiManager;
        this.dataExporter = dataExporter;
        this.dataImporter = dataImporter;
        this.datasetProvider = datasetProvider;
    }

    public void createNewInstrument(ComponentInfo newInstrumentInfo) {
        //First check if the current instrument has been modified and if it is
        // ask the user what he wants to do and do it
        if (isCurrentInstrumentModified()) {
            if (!handleModifiedCurrentInstrument()) {
                return;
            }
        }

        // Create the instance of the new instrument
        Instrument newInstrument = getNewInstrumentFromSessionManager(newInstrumentInfo);
        if (newInstrument == null) {
            return;
        }

        // Set the new instrument as the current one and notify the user interface
        CurrentSessionContainer.getCurrentSession().setInstrument(newInstrument);
        uiManager.sessionModified();
    }

    /**
     * Creates a new instance of an instrument by using the createNewInstrument()
     * method of the session manager. If the creation fails it shows the necessary
     * messages to the user and it returns null.
     * @param info The name and description of the new instrument
     * @return The new instrument instance
     */
    private Instrument getNewInstrumentFromSessionManager(ComponentInfo info) {
        Instrument newInstrument = null;
        try {
            newInstrument = sessionManager.createNewInstrument(info);
        } catch (NameExistsException ex) {
            logger.warn("Did not create new instrument because there is already " +
                    "an instrument with name " + info.getName(), ex);
            messenger.error(MessageFormat.format(bundle.getString("CREATE_INSTRUMENT_NAME_EXISTS"), info.getName()));
        } catch (PersistenceDeviceException ex) {
            logger.warn("Failed to create new instrument with name " +
                    info.getName() + " because there of an error from the percistence device.", ex);
            messenger.error(MessageFormat.format(bundle.getString("CREATE_INSTRUMENT_DEVICE_ERROR"), info.getName()));
        }
        return newInstrument;
    }

    /**
     * This method handles the case when an action will replace the current
     * instrument, but the current instrument is modified. It presents to the
     * user a dialog for selecting what he wants to do (save the changes, ignore
     * the changes or cancel the action). If the user selects to save the changes
     * then this method will persist the current instrument.
     * @return true if the action can continue, false otherwise
     */
    private boolean handleModifiedCurrentInstrument() {
        boolean canContinue = false;
        // Get the decision of the user
        String message = bundle.getString("CURRENT_INSTRUMENT_MODIFIED");
        List<String> list = new ArrayList<String>(3);
        list.add(bundle.getString("SAVE_OPTION"));
        list.add(bundle.getString("IGNORE_OPTION"));
        list.add(bundle.getString("CANCEL_OPTION"));
        String decision = messenger.decision(message, list);

        if (decision == null || decision.equals(list.get(2))) {
            // Here we know the user wants to cancel so return false to stop the calling action
            canContinue = false;
        } else if (decision.equals(list.get(1))) {
            // Ignore the modified data. Let the calling action to continue without saving the data
            canContinue = true;
        } else if (decision.equals(list.get(0))) {
            // Save the current data and allow the calling action to continue if successful
            canContinue = persistCurrentInstrument();
        }
        return canContinue;
    }

    public void loadInstrument(ComponentInfo instrumentInfo) {
        // First check if the requested instrument is the current instrument.
        // If it is do nothing.
        if (instrumentInfo.getName().equals(CurrentSessionContainer.getCurrentSession().
                getInstrument().getInfo().getName())) {
            return;
        }
        // Check if the current instrument has been modified and if it is
        // ask the user what he wants to do and do it
        if (isCurrentInstrumentModified()) {
            if (!handleModifiedCurrentInstrument()) {
                return;
            }
        }
        // Load the instance of the instrument
        Instrument loadedInstrument = loadInstrumentFromSessionManager(instrumentInfo);
        if (loadedInstrument == null) {
            return;
        }
        // Set the loaded instrument as the current one and notify the user interface
        CurrentSessionContainer.getCurrentSession().setInstrument(loadedInstrument);
        uiManager.sessionModified();
    }

    /**
     * Loads an instance of an instrument by using the loadInstrument()
     * method of the session manager. If the load fails it shows the necessary
     * messages to the user and it returns null.
     * @param info The name of the instrument to load
     * @return The loaded instrument instance
     */
    private Instrument loadInstrumentFromSessionManager(ComponentInfo info) {
        Instrument loadedInstrument = null;
        try {
            loadedInstrument = sessionManager.loadInstrument(info);
        } catch (NameNotFoundException ex) {
            logger.warn("Did not load the instrument because there is no " +
                    "instrument with name " + info.getName(), ex);
            messenger.error(MessageFormat.format(bundle.getString("LOAD_INSTRUMENT_NOT_FOUND"), info.getName()));
        } catch (PersistenceDeviceException ex) {
            logger.warn("Failed to load instrument with name " + info.getName() +
                    " because of an error from the percistence device.", ex);
            messenger.error(MessageFormat.format(bundle.getString("LOAD_INSTRUMENT_DEVICE_ERROR"), info.getName()));
        }
        return loadedInstrument;
    }

    public void reloadCurrentInstrument() {
        // Check if the instrument exist in the database. If not give a message
        if (!sessionManager.isInstrumentInDatabase(CurrentSessionContainer.getCurrentSession().getInstrument())) {
            messenger.warn(bundle.getString("RELOAD_INSTRUMENT_NOT_IN_DB_MESSAGE"));
            return;
        }
        // Check if the instrument has been modified. If it is not we don't need to do anything
        if (!isCurrentInstrumentModified()) {
            return;
        }
        // Ask the user if he really wants to do the reload and lose any changes
        if (!getUserConfirmationForReload()) {
            return;
        }
        // Reload the curernt instrument from the persistence device
        ComponentInfo currentInfo = CurrentSessionContainer.getCurrentSession().getInstrument().getInfo();
        Instrument loadedInstrument = loadInstrumentFromSessionManager(currentInfo);
        if (loadedInstrument == null) {
            return;
        }
        // Set the loaded instrument as the current one and notify the user interface
        CurrentSessionContainer.getCurrentSession().setInstrument(loadedInstrument);
        uiManager.sessionModified();
    }

    /**
     * Gets a confirmation from the user to reload the current instrument.
     * @return true if the user confirms, false otherwise
     */
    private boolean getUserConfirmationForReload() {
        boolean confirmed = false;
        String message = bundle.getString("CONFIRM_INSTRUMENT_RELOAD");
        List<String> list = new ArrayList<String>(2);
        list.add(bundle.getString("YES_OPTION"));
        list.add(bundle.getString("NO_OPTION"));
        String decision = messenger.decision(message, list);

        if (decision == null || decision.equals(list.get(1))) {
            // Here we know the user wants to cancel so return false to stop the calling action
            confirmed = false;
        } else if (decision.equals(list.get(0))) {
            // Got the confirmation for reloading
            confirmed = true;
        }
        return confirmed;
    }

    /**
     * Handles a user request for saving the current instrument. If the current
     * instrument has not been modified (its representation in the persistence
     * device has the same information) nothing is done. Otherwise it persists
     * the instrument and it updates the representation shown to the user.
     */
    public void saveCurrentInstrument() {
        // If the current instrument has not been modified we do not need to do anything
        if (!isCurrentInstrumentModified()) {
            return;
        }

        // Persist the current instrument. On success notify the user
        // interface to update the user representation for possible changes
        // (like dataset namespaces, etc)
        if (persistCurrentInstrument()) {
            uiManager.sessionModified();
        }
    }

    /**
     * Tries to persist all the modifications done to the current instrument.
     * If it succeed it returns true. If there is any problem with the persistence
     * device it shows an error to the user and it returns false.
     * @return true if the current instrument is persisted successfully, false otherwise
     */
    private boolean persistCurrentInstrument() {
        // Check if the current instrument is the "new" instrument. In this case saving is not allowed
        if (CurrentSessionContainer.getCurrentSession().getInstrument().getInfo().getName().equals("new")) {
            messenger.warn(bundle.getString("SAVE_NEW_INSTRUMENT_MESSAGE"));
            return false;
        }
        boolean persisted = false;
        Instrument currentInstrument = CurrentSessionContainer.getCurrentSession().getInstrument();
        try {
            sessionManager.saveInstrument(currentInstrument);
            persisted = true;
        } catch (NameExistsException ex) {
            logger.warn("Failed to persist the current instrument because " +
                    "it is never persisted and there is already " +
                    "an instrument with name " + currentInstrument.getInfo().getName(), ex);
            messenger.error(MessageFormat.format(bundle.getString("CREATE_INSTRUMENT_NAME_EXISTS"), currentInstrument.getInfo().getName()));
        } catch (PersistenceDeviceException ex) {
            logger.error("Failed to persist the current instrument (" +
                    currentInstrument.getInfo().getName() + ") because of a device error", ex);
            String message = MessageFormat.format(bundle.getString("SAVE_INSTRUMENT_DEVICE_ERROR"),
                    currentInstrument.getInfo().getName());
            messenger.error(message);
            persisted = false;
        }
        return persisted;
    }

    /**
     * Checks if the current instrument is different from its representation in
     * the persistence device. If they are the same it returns true. If they
     * are different or there is no representation in the persistence device
     * or the persistence device is not accessible it returns true.
     * @return false if the current instrument is the same with its representation
     * in the persistence device, true otherwise
     */
    @Override
    public boolean isCurrentInstrumentModified() {
        boolean modified = true;
        Instrument currentInstrument = CurrentSessionContainer.getCurrentSession().getInstrument();
        try {
            modified = !sessionManager.isInstrumentPersisted(currentInstrument);
        } catch (PersistenceDeviceException ex) {
            logger.error("There was a persistence device error while checking " +
                    "if instrument " + currentInstrument.getInfo().getName() + " is persisted. " +
                    "Non persistence of the instrument is assumed.", ex);
            modified = true;
        }
        return modified;
    }

    public void saveCurrentInstrumentAs(ComponentInfo newInstrumentInfo) {
        // Try to save the current instrument with a different name
        Instrument currentInstrument = CurrentSessionContainer.getCurrentSession().getInstrument();
        Instrument newInstrument = null;
        try {
            newInstrument = sessionManager.saveInstrumentAs(currentInstrument, newInstrumentInfo);
        } catch (NameExistsException ex) {
            logger.warn("Did not copied instrument because there is already " +
                    "an instrument with name " + newInstrumentInfo.getName(), ex);
            messenger.error(MessageFormat.format(bundle.getString("CREATE_INSTRUMENT_NAME_EXISTS"), newInstrumentInfo.getName()));
        } catch (PersistenceDeviceException ex) {
            logger.warn("Failed to copy instrument with name " +
                    newInstrumentInfo.getName() + " because of an error from the percistence device.", ex);
            messenger.error(MessageFormat.format(bundle.getString("CREATE_INSTRUMENT_DEVICE_ERROR"), newInstrumentInfo.getName()));
        }
        // If everything was fine update the session and the user interface
        if (newInstrument != null) {
            CurrentSessionContainer.getCurrentSession().setInstrument(newInstrument);
            uiManager.sessionModified();
        }
    }

    public void deleteInstrument(ComponentInfo instrumentInfo) {
        // Check if the instrument to delete is the current instrument. Then we do nothing
        if (CurrentSessionContainer.getCurrentSession().getInstrument().getInfo().getName().equals(instrumentInfo.getName())) {
            messenger.error(MessageFormat.format(bundle.getString("DELETE_INSTRUMENT_CURRENT"), instrumentInfo.getName()));
            return;
        }
        // Get a confirmation from the user that he really wants to delete the instrument
        String message = MessageFormat.format(bundle.getString("DELETE_INSTRUMENT_CONFIRM"),
                instrumentInfo.getName());
        List<String> options = new ArrayList<String>(2);
        options.add(bundle.getString("DELETE_OPTION"));
        options.add(bundle.getString("CANCEL_OPTION"));
        if(!options.get(0).equals(messenger.decision(message, options))) {
            return;
        }
        // Delete the instrument from the persistence device by using the SessionManager
        // provided by the persistence framework
        logger.info("User requested deletion of instrument " + instrumentInfo.getName());
        try {
            sessionManager.deleteInstrument(instrumentInfo);
            logger.info("Instrument was deleted successfully");
            message = MessageFormat.format(bundle.getString("DELETE_INSTRUMENT_SUCCESS"), instrumentInfo.getName());
            messenger.info(message);
            uiManager.sessionModified();
        } catch (NameNotFoundException ex) {
            logger.warn("Failed to delete instrument " + instrumentInfo.getName() +
                    " because it does not exist in persistence device", ex);
            message = MessageFormat.format(bundle.getString("DELETE_INSTRUMENT_NOT_FOUND"), instrumentInfo.getName());
            messenger.error(message);
        } catch (InUseException ex) {
            StringBuffer usersBuffer = new StringBuffer();
            for (String session : ex.getUsers()) {
                usersBuffer.append(session);
                usersBuffer.append(", ");
            }
            String users = usersBuffer.toString();
            users = users.substring(0, users.length() - 2);
            logger.warn("Failed to delete instrument " + instrumentInfo.getName() +
                    " because it is in use by " + users, ex);
            message = MessageFormat.format(bundle.getString("DELETE_INSTRUMENT_IN_USE"), instrumentInfo.getName(), users);
            messenger.error(message);
        } catch (PersistenceDeviceException ex) {
            logger.warn("Failed to delete instrument " + instrumentInfo.getName() +
                    " because there was an error while accessing the persistence device", ex);
            message = MessageFormat.format(bundle.getString("DELETE_INSTRUMENT_DEVICE_ERROR"), instrumentInfo.getName());
            messenger.error(message);
        }
    }

    public void importFromFile(File file, ComponentInfo instrumentInfo) {
        // Check if the current instrument has been modified and if it is
        // ask the user what he wants to do and do it
        if (isCurrentInstrumentModified()) {
            if (!handleModifiedCurrentInstrument()) {
                return;
            }
        }
        Thread importThread = new ImportInstrumentThread(file, instrumentInfo);
        messenger.actionStarted(bundle.getString("IMPORT_INSTRUMENT_MESSAGE"), importThread);
        importThread.start();
    }

    private class ImportInstrumentThread extends Thread {
        private File file;
        private ComponentInfo instrumentInfo;

        public ImportInstrumentThread(File file, ComponentInfo instrumentInfo) {
            super();
            this.file = file;
            this.instrumentInfo = instrumentInfo;
        }

        @Override
        public void run() {
            boolean ok = true;
            InstrumentWithDatasets instrument = null;
            try {
                instrument = dataImporter.importInstrumentFromDataFile(file, instrumentInfo);
            } catch (IOException ex) {
                logger.warn("Failed to import instrument because of IO problem", ex);
                String message = MessageFormat.format(bundle.getString("IMPORT_INSTRUMENT_ERROR"), instrumentInfo.getName());
                messenger.error(message);
                ok = false;
            } catch (WrongFormatException ex) {
                logger.info("Failed to import instrument because of wrong format", ex);
                String message = MessageFormat.format(bundle.getString("IMPORT_INSTRUMENT_WRONG_FORMAT"), file.getName());
                messenger.error(message);
                ok = false;
            }
            if (!ok) {
                messenger.actionStoped(this);
                return;
            }
            PersistenceFactory.beginTransaction();
            try {
                Dataset dataset = instrument.getFilterTransmissionDataset();
                if (dataset != null) {
                    datasetManager.saveDataset(dataset);
                }
                dataset = instrument.getFwhmDataset();
                if (dataset != null) {
                    datasetManager.saveDataset(dataset);
                }
                dataset = instrument.getSpectralResolutionDataset();
                if (dataset != null) {
                    datasetManager.saveDataset(dataset);
                }
                dataset = instrument.getTransmissionDataset();
                if (dataset != null) {
                    datasetManager.saveDataset(dataset);
                }
                sessionManager.saveInstrumentAs(instrument, instrumentInfo);
                PersistenceFactory.commitTransaction();
            } catch (NameExistsException ex) {
                PersistenceFactory.rollbackTransaction();
                logger.info("Failed to import instrument because of name already exists", ex);
                String message = MessageFormat.format(bundle.getString("CREATE_INSTRUMENT_NAME_EXISTS"), instrumentInfo.getName());
                messenger.error(message);
                ok = false;
            } catch (PersistenceDeviceException ex) {
                PersistenceFactory.rollbackTransaction();
                logger.warn("Failed to import instrument because of an error on persistence device", ex);
                String message = MessageFormat.format(bundle.getString("IMPORT_INSTRUMENT_ERROR"), instrumentInfo.getName());
                messenger.error(message);
                ok = false;
            }
            if (!ok) {
                messenger.actionStoped(this);
                return;
            }
            Instrument persistedInstrument = null;
            try {
                persistedInstrument = sessionManager.loadInstrument(instrumentInfo);
            } catch (NameNotFoundException ex) {
                logger.warn("Instrument just imported not found in persistence device", ex);
                String message = MessageFormat.format(bundle.getString("IMPORT_INSTRUMENT_ERROR"), instrumentInfo.getName());
                messenger.error(message);
                ok = false;
            } catch (PersistenceDeviceException ex) {
                logger.warn("Failed to retrieve just imported instrument from persistence device", ex);
                String message = MessageFormat.format(bundle.getString("IMPORT_INSTRUMENT_ERROR"), instrumentInfo.getName());
                messenger.error(message);
                ok = false;
            }
            messenger.actionStoped(this);
            if (!ok) {
                return;
            }
            CurrentSessionContainer.getCurrentSession().setInstrument(persistedInstrument);
            uiManager.sessionModified();
        }
    }

    public void exportCurrentInstrumentInFile(File file) {
        Instrument currentInstrument = CurrentSessionContainer.getCurrentSession().getInstrument();
        try {
            dataExporter.exportInstrumentInFile(file, currentInstrument, datasetProvider);
        } catch (IOException ex) {
            logger.warn("Failed to export instrument", ex);
            String message = MessageFormat.format(bundle.getString("EXPORT_INSTRUMENT_ERROR"), currentInstrument.getInfo().getName());
            messenger.error(message);
        }
    }

}
