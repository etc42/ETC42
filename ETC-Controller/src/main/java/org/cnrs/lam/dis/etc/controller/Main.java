/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import org.apache.log4j.Logger;
import org.cnrs.lam.dis.etc.ui.swing.SplashScreen;

/**
 */
public class Main {

	private static final Logger logger = Logger.getLogger(ETC42.class);
	private static final long MAX_HEAP_REQUIRED = 1400000000L;


	public static void main(String[] args) {

		SplashScreen.getInstance().setVisible(true);
		SplashScreen.getInstance().setProgression("Initialization");

		// First we initialize the logger configuration
		ETC42.initializeLogger();
		logger.info("Starting ETC-42...");
		boolean enoughMemory = false;
		// Check if the user wants to skip the memory test
		if (System.getProperty("etc.allowSmallHeap") != null) {
			logger.info("Skipping the memory check because user gave the etc.allowSmallHeap parameter...");
			enoughMemory = true;
		}
		// We check the memory size
		if (!enoughMemory && Runtime.getRuntime().maxMemory() >= MAX_HEAP_REQUIRED) {
			enoughMemory = true;
		}
		// Now we run ETC in this process or in a new one if we need more memory
		if (enoughMemory) {
			ETC42.main(args);
		} else {
			logger.info("ETC-42 was launched with insufficient max heap size ("
					+ Runtime.getRuntime().maxMemory() + "). Trying to relaunch with more memory.");
			launchInNewProcess(args);
		}
		SplashScreen.getInstance().setVisible(false);
	}

	private static void launchInNewProcess(String[] args) {
		// Get the class path
		String classPath = System.getProperty("java.class.path");
		classPath.replaceAll(" ", "\\ ");
		// Get the parameters the user gave. Note that we do that ONLY for the ETC parameters
		StringBuilder parameters = new StringBuilder();
		if (System.getProperty("etc.uiMode") != null) {
			parameters.append(" ").append("-Detc.uiMode=").append(System.getProperty("etc.uiMode"));
		}
		//Load defaults xml file if database is empty
		if (System.getProperty("etc.fixtures") != null) {
			parameters.append(" ").append("-Detc.fixtures=").append(System.getProperty("etc.fixtures"));
		}
		if (System.getProperty("etc.server") != null) {
			parameters.append(" ").append("-Detc.server=").append(System.getProperty("etc.server"));
		}
		if (System.getProperty("etc.instrument") != null) {
			parameters.append(" ").append("-Detc.instrument=").append(System.getProperty("etc.instrument"));
		}
		if (System.getProperty("etc.site") != null) {
			parameters.append(" ").append("-Detc.site=").append(System.getProperty("etc.site"));
		}
		if (System.getProperty("etc.home") != null) {
			parameters.append(" ").append("-Detc.home=").append(System.getProperty("etc.home"));
		}
		if (System.getProperty("calculator.disable.cache") != null) {
			parameters.append(" ").append("-Dcalculator.disable.cache");
		}
		StringBuilder arguments = new StringBuilder();
		for (String arg : args) {
			arguments.append(arg).append(" ");
		}
		String execCommand = "java -cp " + classPath + " -Xmx1536m "
				+ parameters + " org.cnrs.lam.dis.etc.controller.ETC42 " + arguments;
		logger.info("Restarting ETC-42 with command:");
		logger.info(execCommand);
		try {
			Process process = Runtime.getRuntime().exec(execCommand);
			new PrintToStreamThread(process.getInputStream(), System.out).start();
			new PrintToStreamThread(process.getErrorStream(), System.err).start();
			new RedirectStandardInputThread(process.getOutputStream()).start();
		} catch (IOException ex) {
			logger.error("Failed to relaunch ETC-42.", ex);
		}
	}

	private static class PrintToStreamThread extends Thread {
		private InputStream streamToRead;
		private PrintStream streamToWrite;

		public PrintToStreamThread(InputStream streamToRead, PrintStream streamToWrite) {
			this.streamToRead = streamToRead;
			this.streamToWrite = streamToWrite;
		}

		@Override
		public void run() {
			try {
				int charRed = 0;
				byte[] buffer = new byte[1024];
				while ((charRed = streamToRead.read(buffer)) != -1) {
					streamToWrite.write(buffer, 0, charRed);
				}
			} catch (IOException ex) {
			}
		}
	}

	private static class RedirectStandardInputThread extends Thread {
		private OutputStream streamToWrite;

		public RedirectStandardInputThread(OutputStream streamToWrite) {
			this.streamToWrite = streamToWrite;
		}

		@Override
		public void run() {
			try {
				int c = -1;
				while ((c = System.in.read()) != -1) {
					streamToWrite.write(c);
					streamToWrite.flush();
				}
			} catch (IOException ex) {
			}
		}
	}

}
