/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.controller;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.TreeMap;
import javax.swing.JOptionPane;
import org.apache.log4j.Logger;
import org.cnrs.lam.dis.etc.calculator.util.Units;
import org.cnrs.lam.dis.etc.dataimportexport.DataImportExportFactory;
import org.cnrs.lam.dis.etc.dataimportexport.DataImporter;
import org.cnrs.lam.dis.etc.datamodel.FilterBand;
import org.cnrs.lam.dis.etc.persistence.DatasetManager;
import org.cnrs.lam.dis.etc.persistence.PersistenceFactory;
import org.cnrs.lam.dis.etc.ui.swing.SplashScreen;
import org.cnrs.lam.dis.etc.ui.swing.importer.ImportHelper;
import org.javatuples.Pair;

/**
 *
 * @author Nikolaos Apostolakos
 */
public final class DbUpdater {

	private static final int DB_VERSION = 31;
	private static Logger logger = Logger.getLogger(DbUpdater.class);
	private static ResourceBundle bundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/ui/swing/messages");

	private DbUpdater() {
	}

	private static void updateFromVersion0To1(Statement statement) throws SQLException {
		statement.executeUpdate("ALTER TABLE OBS_PARAM ADD SPECTRAL_QUANTUM_TYPE VARCHAR(255) NOT NULL DEFAULT 'SPECTRAL_RESOLUTION_ELEMENT'");
	}

	private static void updateFromVersion1To2(Statement statement) throws SQLException {
		statement.executeUpdate("ALTER TABLE SOURCE ADD TEMPERATURE DOUBLE");
		statement.executeUpdate("ALTER TABLE SOURCE ADD TEMPERATURE_UNIT VARCHAR(20) DEFAULT 'K'");
	}

	private static void updateFromVersion2To3(Statement statement) throws SQLException {
		statement.executeUpdate("ALTER TABLE SITE ADD SKY_CONTRIBUTING_ON_SPACE SMALLINT DEFAULT 0");
	}

	private static void updateFromVersion3To4(Statement statement) throws SQLException {
		statement.executeUpdate("ALTER TABLE INSTRUMENT ADD STREHL_RATIO BIGINT");
		statement.executeUpdate("ALTER TABLE INSTRUMENT ADD MOFFAT_A BIGINT");
		statement.executeUpdate("ALTER TABLE INSTRUMENT ADD MOFFAT_B BIGINT");
	}

	private static void updateFromVersion4To5(Statement statement) throws SQLException {
		statement.executeUpdate("ALTER TABLE SITE DROP COLUMN LIMITED_ON_TYPE");
		statement.executeUpdate("ALTER TABLE SITE ADD SEEING_LIMITED SMALLINT DEFAULT 0");
	}    

	private static void updateFromVersion5To6(Statement statement) throws SQLException {
		statement.executeUpdate("ALTER TABLE SOURCE ADD EMISSION_LINE_FWHM DOUBLE");
		statement.executeUpdate("ALTER TABLE SOURCE ADD EMISSION_LINE_FWHM_UNIT VARCHAR(20) DEFAULT '\u00C5'");
	}

	private static void updateFromVersion6To7(Statement statement) throws SQLException {
		statement.executeUpdate("ALTER TABLE INSTRUMENT DROP COLUMN STREHL_RATIO");
		statement.executeUpdate("ALTER TABLE INSTRUMENT DROP COLUMN MOFFAT_A");
		statement.executeUpdate("ALTER TABLE INSTRUMENT DROP COLUMN MOFFAT_B");
	}

	private static void updateFromVersion7To8(Statement statement) throws SQLException {
		statement.executeUpdate("ALTER TABLE INSTRUMENT ADD STREHL_RATIO DOUBLE");
		statement.executeUpdate("ALTER TABLE INSTRUMENT ADD REF_WAVELENGTH DOUBLE");
		statement.executeUpdate("ALTER TABLE INSTRUMENT ADD REF_WAVELENGTH_UNIT VARCHAR(20) DEFAULT '\u00C5'");
	}

	private static void updateFromVersion8To9(Statement statement) throws SQLException {
		statement.executeUpdate("ALTER TABLE SOURCE ADD SPECTRAL_DISTRIBUTION_TEMPLATE_TYPE VARCHAR(255) NOT NULL DEFAULT 'TEMPLATE'");
		statement.executeUpdate("ALTER TABLE SITE ADD ZODIACAL_PROFILE_TYPE VARCHAR(255) NOT NULL DEFAULT 'TEMPLATE'");
		statement.executeUpdate("ALTER TABLE SITE ADD GALACTIC_PROFILE_TYPE VARCHAR(255) NOT NULL DEFAULT 'TEMPLATE'");
		statement.executeUpdate("ALTER TABLE SITE ADD SKY_EMISSION_TYPE VARCHAR(255) NOT NULL DEFAULT 'TEMPLATE'");
	}

	private static void updateFromVersion9To10(Statement statement) throws SQLException {        
		// Change the unit of spectral resolution from angstrom to angstrom per pixel
		statement.executeUpdate("UPDATE DATASET_INFO SET Y_UNIT = '" + Units.getAngstromPerPixel()
		+ "' WHERE TYPE='SPECTRAL_RESOLUTION' AND Y_UNIT = '" + Units.ANGSTROM + "'");

		// Modify all the units to be from 20 characters 50
		statement.executeUpdate("ALTER TABLE DATASET_INFO ALTER COLUMN X_UNIT SET DATA TYPE VARCHAR(50)");
		statement.executeUpdate("ALTER TABLE DATASET_INFO ALTER COLUMN Y_UNIT SET DATA TYPE VARCHAR(50)");
		statement.executeUpdate("ALTER TABLE OBS_PARAM ALTER COLUMN DIT_UNIT SET DATA TYPE VARCHAR(50)");
		statement.executeUpdate("ALTER TABLE OBS_PARAM ALTER COLUMN EXPOSURE_TIME_UNIT SET DATA TYPE VARCHAR(50)");
		statement.executeUpdate("ALTER TABLE OBS_PARAM ALTER COLUMN SNR_UNIT SET DATA TYPE VARCHAR(50)");
		statement.executeUpdate("ALTER TABLE OBS_PARAM ALTER COLUMN SNR_LAMBDA_UNIT SET DATA TYPE VARCHAR(50)");
		statement.executeUpdate("ALTER TABLE SITE ALTER COLUMN SEEING_UNIT SET DATA TYPE VARCHAR(50)");
		statement.executeUpdate("ALTER TABLE SOURCE ALTER COLUMN EMISSION_LINE_WAVELENGTH_UNIT SET DATA TYPE VARCHAR(50)");
		statement.executeUpdate("ALTER TABLE SOURCE ALTER COLUMN EMISSION_LINE_FWHM_UNIT SET DATA TYPE VARCHAR(50)");
		statement.executeUpdate("ALTER TABLE SOURCE ALTER COLUMN EXTENDED_SOURCE_RADIUS_UNIT SET DATA TYPE VARCHAR(50)");
		statement.executeUpdate("ALTER TABLE SOURCE ALTER COLUMN TEMPERATURE_UNIT SET DATA TYPE VARCHAR(50)");
		statement.executeUpdate("ALTER TABLE INSTRUMENT ALTER COLUMN DARK_UNIT SET DATA TYPE VARCHAR(50)");
		statement.executeUpdate("ALTER TABLE INSTRUMENT ALTER COLUMN DIAMETER_UNIT SET DATA TYPE VARCHAR(50)");
		statement.executeUpdate("ALTER TABLE INSTRUMENT ALTER COLUMN FIBER_DIAMETER_UNIT SET DATA TYPE VARCHAR(50)");
		statement.executeUpdate("ALTER TABLE INSTRUMENT ALTER COLUMN PIXEL_SCALE_UNIT SET DATA TYPE VARCHAR(50)");
		statement.executeUpdate("ALTER TABLE INSTRUMENT ALTER COLUMN RANGE_MAX_UNIT SET DATA TYPE VARCHAR(50)");
		statement.executeUpdate("ALTER TABLE INSTRUMENT ALTER COLUMN RANGE_MIN_UNIT SET DATA TYPE VARCHAR(50)");
		statement.executeUpdate("ALTER TABLE INSTRUMENT ALTER COLUMN READOUT_UNIT SET DATA TYPE VARCHAR(50)");
		statement.executeUpdate("ALTER TABLE INSTRUMENT ALTER COLUMN SLIT_LENGTH_UNIT SET DATA TYPE VARCHAR(50)");
		statement.executeUpdate("ALTER TABLE INSTRUMENT ALTER COLUMN SLIT_WIDTH_UNIT SET DATA TYPE VARCHAR(50)");
		statement.executeUpdate("ALTER TABLE INSTRUMENT ALTER COLUMN REF_WAVELENGTH_UNIT SET DATA TYPE VARCHAR(50)");

		// Add the emission line flux column
		statement.executeUpdate("ALTER TABLE SOURCE ADD EMISSION_LINE_FLUX DOUBLE");
		statement.executeUpdate("ALTER TABLE SOURCE ADD EMISSION_LINE_FLUX_UNIT VARCHAR(50) DEFAULT 'erg/cm^2/s'");
	}

	/**
	 * Creates the infrastructure for multi-datasets, which can have different
	 * data, depending on an option.
	 * @param connection
	 * @throws SQLException 
	 */
	private static void updateFromVersion10To11(Statement statement) throws SQLException {
		// Create the table which is mapping the pairs (dataset_info, option) to dataset data
		statement.execute("CREATE TABLE DATASET_OPTION_MAP (DATASET_INFO BIGINT NOT NULL, OPTION_KEY VARCHAR(200) NOT NULL, DATASET BIGINT, PRIMARY KEY (DATASET_INFO, OPTION_KEY))");
		// Create for every existing (non multi) dataset an entry in the new table with null OPTION_KEY
		statement.executeUpdate("INSERT INTO DATASET_OPTION_MAP (DATASET_INFO, OPTION_KEY, DATASET) SELECT ID AS DATASET_INFO, '' AS OPTION_KEY, DATASET FROM DATASET_INFO");
		// Remove the (not used any more) column DATASET from the DATASET_INFO table
		statement.executeUpdate("ALTER TABLE DATASET_INFO DROP COLUMN DATASET");
	}

	private static void updateFromVersion11To12(Statement statement) throws SQLException {
		// Change all the sky emission datasets to be multi-datasets with only the darkest sky brightness
		statement.executeUpdate("INSERT INTO DATASET_OPTION_MAP (DATASET_INFO, OPTION_KEY, DATASET) SELECT DATASET_INFO, 'darkest' AS OPTION_KEY, DATASET FROM DATASET_OPTION_MAP "
				+ "WHERE DATASET_INFO IN (SELECT ID FROM APP.DATASET_INFO WHERE TYPE = 'SKY_EMISSION' AND OPTION_KEY = '')");
		statement.executeUpdate("DELETE FROM DATASET_OPTION_MAP WHERE DATASET_INFO IN (SELECT ID FROM APP.DATASET_INFO WHERE TYPE = 'SKY_EMISSION') AND OPTION_KEY = ''");
	}

	private static void updateFromVersion12To13(Statement statement) throws SQLException {
		// Drop the sky contributing on space column, as this option is not available any more
		statement.execute("ALTER TABLE SITE DROP COLUMN SKY_CONTRIBUTING_ON_SPACE");
		statement.execute("ALTER TABLE SITE DROP COLUMN SKY_EMISSION_TYPE");
	}

	private static void updateFromVersion13To14(Statement statement) throws SQLException {
		// Add the column to keep the selected sky emission option
		statement.execute("ALTER TABLE SITE ADD SKY_EMISSION_SELECTED_OPTION VARCHAR(200)");
		// Set its value as the currently selected day of the moon
		statement.execute("UPDATE SITE SET SKY_EMISSION_SELECTED_OPTION = TRIM(CAST(DAY_OF_MOON AS CHAR(200)))");
		// Drop the old and now not used column
		statement.execute("ALTER TABLE SITE DROP COLUMN DAY_OF_MOON");
	}

	private static void updateFromVersion14To15(Statement statement) throws SQLException {
		// Set all the fixed SNR units to be the emty string. Before it was set
		// wrongly to ???
		statement.execute("UPDATE OBS_PARAM SET SNR_UNIT = ''");
	}

	// This update is quite crusial. It changes the datasets from being stored as
	// rows in the table DATA_PAIR to be stored as BLOBs.
	private static void updateFromVersion15To16(Statement statement) throws SQLException {        
		// First we create the new column
		statement.execute("ALTER TABLE DATASET ADD COLUMN DATA_BLOB BLOB (2G)");

		// Now we find the IDs of all the datasets
		ResultSet datasetIdResultSet = statement.executeQuery("SELECT ID FROM DATASET");
		List<Long> datasetIdList = new ArrayList<Long>();
		while (datasetIdResultSet.next()) {
			datasetIdList.add(datasetIdResultSet.getLong("ID"));
		}

		// For each dataset we create the map with its data and we save it in the new column
		for (long datasetId : datasetIdList) {
			ResultSet dataResultSet = statement.executeQuery("SELECT X_VALUE, Y_VALUE FROM DATA_PAIR WHERE DATASET = " + datasetId);
			Map<Double, Double> data = new TreeMap<Double, Double>();
			while (dataResultSet.next()) {
				double x = dataResultSet.getDouble("X_VALUE");
				double y = dataResultSet.getDouble("Y_VALUE");
				data.put(x, y);
			}
			// Now we save the data in the new column
			// First we write the serialized map in a byte array
			ByteArrayOutputStream baos;
			try {
				baos = new ByteArrayOutputStream();
				ObjectOutputStream oos = new ObjectOutputStream(baos);
				oos.writeObject(data);
				// Now we get the bytes and we create a byte input stream
				byte[] mapByteArray = baos.toByteArray();
				ByteArrayInputStream bais = new ByteArrayInputStream(mapByteArray);
				// Finally we update the value in the table
				PreparedStatement preparedStatement = statement.getConnection().prepareStatement("UPDATE DATASET SET DATA_BLOB = ? WHERE ID = ?");
				preparedStatement.setBinaryStream(1, bais);
				preparedStatement.setLong(2, datasetId);
				preparedStatement.execute();
				oos.close();
				preparedStatement.close();
			} catch (IOException iOException) {
				logger.error("Failure during the serialization of the existing dataset values", iOException);
				throw new SQLException("Failure during the serialization of the existing dataset values", iOException);
			}
		}

		// Now we drop the DATA_PAIR table
		statement.execute("DROP TABLE DATA_PAIR");
	}

	// Removes the fixed SNR wavelength band option
	private static void updateFromVersion16To17(Statement statement) throws SQLException {        
		// First set the SNR_LAMBDA column of all the obs params wich are set to BAND
		ResultSet bandObsParamResultSet = statement.executeQuery("select * from APP.OBS_PARAM where SNR_BAND_TYPE = 'BAND'");
		List<Pair<Long, FilterBand>> bandObsParamList = new ArrayList<Pair<Long, FilterBand>>();
		while (bandObsParamResultSet.next()) {
			long id = bandObsParamResultSet.getLong("ID");
			String bandString = bandObsParamResultSet.getString("SNR_BAND");
			bandObsParamList.add(new Pair<Long, FilterBand>(id, FilterBand.valueOf(bandString)));
		}
		for (Pair<Long, FilterBand> bandObsParam : bandObsParamList) {
			long id = bandObsParam.getValue0();
			FilterBand band = bandObsParam.getValue1();
			statement.executeUpdate("UPDATE OBS_PARAM SET SNR_LAMBDA = " + band.centralWavelength() + " WHERE ID = " + id);
		}

		// Now drop all the absolete columns
		statement.execute("ALTER TABLE OBS_PARAM DROP COLUMN SNR_BAND");
		statement.execute("ALTER TABLE OBS_PARAM DROP COLUMN SNR_BAND_TYPE");
	}

	// Adds the spectral resolution type and the delta lambda per pixel
	private static void updateFromVersion17To18(Statement statement) throws SQLException {
		statement.executeUpdate("ALTER TABLE INSTRUMENT ADD DELTA_LAMBDA_PER_PIXEL DOUBLE");
		statement.executeUpdate("ALTER TABLE INSTRUMENT ADD DELTA_LAMBDA_PER_PIXEL_UNIT VARCHAR(50) DEFAULT '\u00C5/pixel'");
		statement.executeUpdate("ALTER TABLE INSTRUMENT ADD SPECTRAL_RESOLUTION_TYPE VARCHAR(255) NOT NULL DEFAULT 'CONSTANT_DELTA_LAMBDA'");
	}

	// Change the band magnitude combobox to a wavelength combobox
	private static void updateFromVersion18To19(Statement statement) throws SQLException {
		// We create the new columns which keep the wavelength of the AB magnitude
		statement.executeUpdate("ALTER TABLE SOURCE ADD MAGNITUDE_WAVELENGTH DOUBLE");
		statement.executeUpdate("ALTER TABLE SOURCE ADD MAGNITUDE_WAVELENGTH_UNIT VARCHAR(50) DEFAULT '\u00C5'");

		// We copy any old values to the new columns
		ResultSet sourcesResultSet = statement.executeQuery("SELECT ID, BAND FROM SOURCE");
		List<Pair<Long, Integer>> sourceList = new ArrayList<Pair<Long, Integer>>();
		while (sourcesResultSet.next()){
			long id = sourcesResultSet.getLong("ID");
			int band = FilterBand.valueOf(sourcesResultSet.getString("BAND")).centralWavelength();
			sourceList.add(new Pair<Long, Integer>(id, band));
		}
		for (Pair<Long, Integer> source : sourceList) {
			long id = source.getValue0();
			int band = source.getValue1();
			statement.executeUpdate("UPDATE SOURCE SET MAGNITUDE_WAVELENGTH = " + band + " WHERE ID = " + id);
		}

		// Now drop all the absolete columns
		statement.execute("ALTER TABLE SOURCE DROP COLUMN BAND");
	}

	// Adds the extra signal and background noise
	private static void updateFromVersion19To20(Statement statement) throws SQLException {        
		statement.executeUpdate("ALTER TABLE OBS_PARAM ADD EXTRA_BACKGROUND_NOISE_TYPE VARCHAR(255) NOT NULL DEFAULT 'ONLY_CALCULATED_BACKGROUND_NOISE'");
		statement.executeUpdate("ALTER TABLE OBS_PARAM ADD EXTRA_SIGNAL_TYPE VARCHAR(255) NOT NULL DEFAULT 'ONLY_CALCULATED_SIGNAL'");
		statement.executeUpdate("ALTER TABLE OBS_PARAM ADD EXTRA_BACKGROUND_NOISE_DATASET BIGINT");
		statement.executeUpdate("ALTER TABLE OBS_PARAM ADD EXTRA_SIGNAL_DATASET BIGINT");
	}

	// Adds the extra signal and background noise dataset types
	private static void updateFromVersion20To21(Statement statement) throws SQLException {        
		statement.executeUpdate("ALTER TABLE OBS_PARAM ADD EXTRA_BACKGROUND_NOISE_DATASET_TYPE VARCHAR(255) NOT NULL DEFAULT 'FUNCTION'");
		statement.executeUpdate("ALTER TABLE OBS_PARAM ADD EXTRA_SIGNAL_DATASET_TYPE VARCHAR(255) NOT NULL DEFAULT 'FUNCTION'");
	}

	// Adds the extra signal and background noise for imaging
	private static void updateFromVersion21To22(Statement statement) throws SQLException {        
		statement.executeUpdate("ALTER TABLE OBS_PARAM ADD EXTRA_BACKGROUND_NOISE DOUBLE");
		statement.executeUpdate("ALTER TABLE OBS_PARAM ADD EXTRA_BACKGROUND_NOISE_UNIT VARCHAR(50) DEFAULT 'e\u207B/s'");
		statement.executeUpdate("ALTER TABLE OBS_PARAM ADD EXTRA_SIGNAL DOUBLE");
		statement.executeUpdate("ALTER TABLE OBS_PARAM ADD EXTRA_SIGNAL_UNIT VARCHAR(50) DEFAULT 'e\u207B/s'");
	}

	// Adds the fixed SNR type
	private static void updateFromVersion22To23(Statement statement) throws SQLException {        
		statement.executeUpdate("ALTER TABLE OBS_PARAM ADD FIXED_SNR_TYPE VARCHAR(255) NOT NULL DEFAULT 'TOTAL'");
	}

	// Adds the double gaussian PSF
	private static void updateFromVersion23To24(Statement statement) throws SQLException {        
		statement.executeUpdate("ALTER TABLE INSTRUMENT ADD PSF_DOUBLE_GAUSSIAN_MULTIPLIER DOUBLE");
		statement.executeUpdate("ALTER TABLE INSTRUMENT ADD PSF_DOUBLE_GAUSSIAN_FWHM_1 BIGINT");
		statement.executeUpdate("ALTER TABLE INSTRUMENT ADD PSF_DOUBLE_GAUSSIAN_FWHM_2 BIGINT");
	}

	// Adds the magnitude type option for extended sources
	private static void updateFromVersion24To25(Statement statement) throws SQLException {        
		statement.executeUpdate("ALTER TABLE SOURCE ADD EXTENDED_MAGNITUDE_TYPE VARCHAR(255) NOT NULL DEFAULT 'PEAK_VALUE'");
	}

	// Adds the data type in the dataset
	private static void updateFromVersion25To26(Statement statement) throws SQLException {        
		statement.executeUpdate("ALTER TABLE DATASET_INFO ADD DATA_TYPE VARCHAR(255) NOT NULL DEFAULT 'FUNCTION'");

		// We need to set the data types for the datasets the user has already set the existing parameter
		statement.executeUpdate("UPDATE DATASET_INFO AS DI SET DATA_TYPE = (SELECT EXTRA_BACKGROUND_NOISE_DATASET_TYPE FROM OBS_PARAM AS OP WHERE OP.EXTRA_BACKGROUND_NOISE_DATASET = DI.ID) "
				+ "WHERE DI.TYPE = 'EXTRA_BACKGROUND_NOISE' AND (SELECT COUNT(*) FROM OBS_PARAM AS OP WHERE OP.EXTRA_BACKGROUND_NOISE_DATASET = DI.ID) > 0");
		statement.executeUpdate("UPDATE DATASET_INFO AS DI SET DATA_TYPE = (SELECT EXTRA_SIGNAL_DATASET_TYPE FROM OBS_PARAM AS OP WHERE OP.EXTRA_SIGNAL_DATASET = DI.ID) "
				+ "WHERE DI.TYPE = 'EXTRA_SIGNAL' AND (SELECT COUNT(*) FROM OBS_PARAM AS OP WHERE OP.EXTRA_SIGNAL_DATASET = DI.ID) > 0");
		statement.executeUpdate("UPDATE DATASET_INFO AS DI SET DATA_TYPE = (SELECT ZODIACAL_PROFILE_TYPE FROM SITE AS S WHERE S.ZODIACAL_PROFILE = DI.ID) "
				+ "WHERE DI.TYPE = 'ZODIACAL' AND (SELECT COUNT(*) FROM SITE AS S WHERE S.ZODIACAL_PROFILE = DI.ID) > 0");
		statement.executeUpdate("UPDATE DATASET_INFO AS DI SET DATA_TYPE = (SELECT GALACTIC_PROFILE_TYPE FROM SITE AS S WHERE S.GALACTIC_PROFILE = DI.ID) "
				+ "WHERE DI.TYPE = 'GALACTIC' AND (SELECT COUNT(*) FROM SITE AS S WHERE S.GALACTIC_PROFILE = DI.ID) > 0");
		statement.executeUpdate("UPDATE DATASET_INFO AS DI SET DATA_TYPE = (SELECT SPECTRAL_DISTRIBUTION_TEMPLATE_TYPE FROM SOURCE AS S WHERE S.SPECTRAL_DISTRIBUTION_TEMPLATE = DI.ID) "
				+ "WHERE DI.TYPE = 'SPECTRAL_DIST_TEMPLATE' AND (SELECT COUNT(*) FROM SOURCE AS S WHERE S.SPECTRAL_DISTRIBUTION_TEMPLATE = DI.ID) > 0");

		// We remove the per Angstrom from the templates which have it
		statement.executeUpdate("UPDATE DATASET_INFO SET Y_UNIT = 'erg/cm^2/s/arcsec^2' WHERE DATA_TYPE = 'TEMPLATE' AND Y_UNIT = 'erg/cm^2/s/\u00C5/arcsec^2'");
		statement.executeUpdate("UPDATE DATASET_INFO SET Y_UNIT = 'erg/cm^2/s' WHERE DATA_TYPE = 'TEMPLATE' AND Y_UNIT = 'erg/cm^2/s/\u00C5'");
		statement.executeUpdate("UPDATE DATASET_INFO SET Y_UNIT = 'e\u207B/s' WHERE DATA_TYPE = 'TEMPLATE' AND Y_UNIT = 'e\u207B/s/\u00C5'");
		statement.executeUpdate("UPDATE DATASET_INFO SET Y_UNIT = 'e\u207B/arcsec^2/s' WHERE DATA_TYPE = 'TEMPLATE' AND Y_UNIT = 'e\u207B/arcsec^2/\u00C5/s'");
	}

	// Removes the individual parameters for dataset data types
	private static void updateFromVersion26To27(Statement statement) throws SQLException {        
		statement.executeUpdate("ALTER TABLE OBS_PARAM DROP COLUMN EXTRA_BACKGROUND_NOISE_DATASET_TYPE");
		statement.executeUpdate("ALTER TABLE OBS_PARAM DROP COLUMN EXTRA_SIGNAL_DATASET_TYPE");
		statement.executeUpdate("ALTER TABLE SITE DROP COLUMN ZODIACAL_PROFILE_TYPE");
		statement.executeUpdate("ALTER TABLE SITE DROP COLUMN GALACTIC_PROFILE_TYPE");
		statement.executeUpdate("ALTER TABLE SOURCE DROP COLUMN SPECTRAL_DISTRIBUTION_TEMPLATE_TYPE");
	}

	// Adds the PSF size type and the related columns
	private static void updateFromVersion27To28(Statement statement) throws SQLException {        
		statement.executeUpdate("ALTER TABLE INSTRUMENT ADD PSF_SIZE_TYPE VARCHAR(255) NOT NULL DEFAULT 'AUTO'");
		statement.executeUpdate("ALTER TABLE INSTRUMENT ADD FIXED_PSF_SIZE DOUBLE");
		statement.executeUpdate("ALTER TABLE INSTRUMENT ADD FIXED_PSF_SIZE_UNIT VARCHAR(50) DEFAULT 'arcsec'");
		statement.executeUpdate("ALTER TABLE INSTRUMENT ADD PSF_SIZE_FUNCTION BIGINT");
		statement.executeUpdate("ALTER TABLE INSTRUMENT ADD PSF_SIZE_PERCENTAGE DOUBLE");
	}

	// Adds the atmospheric transmission profile and type
	private static void updateFromVersion28To29(Statement statement) throws SQLException {        
		statement.executeUpdate("ALTER TABLE SITE ADD ATMOSPHERIC_TRANSMISSION BIGINT");
		statement.executeUpdate("ALTER TABLE SITE ADD ATMOSPHERIC_TRANSMISSION_TYPE VARCHAR(255) NOT NULL DEFAULT 'ABSORPTION_EXTINCTION'");
	}

	// Adds dataset for zodiacal lights
	private static void updateFromVersion29To30(Statement statement) throws SQLException {        
		DataImporter dataImporter = DataImportExportFactory.getDataImporter();
		DatasetManager datasetManager = PersistenceFactory.getDatasetManager();

		try {
			Pair<Scanner, Boolean> inPair = DbInstaller.getDatasetList(null, dataImporter, datasetManager);
			Scanner in = inPair.getValue0();
			boolean isDefault = inPair.getValue1();
			in.useDelimiter("}");
			while (in.hasNext()) {
				String datasetInfo = in.next();
				datasetInfo = datasetInfo.substring(datasetInfo.indexOf('{')+1).trim();
				if (datasetInfo.contains("file=HighEclipticZodiacalLight.dat") || datasetInfo.contains("file=LowEclipticZodiacalLight.dat")) {
					DbInstaller.importDataset(datasetInfo, dataImporter, datasetManager, isDefault);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// Correct dataset for zodiacal lights :
	// Remove old one and re-add the new one
	private static void updateFromVersion30To31(Statement statement) throws SQLException {
		DataImporter dataImporter = DataImportExportFactory.getDataImporter();
		DatasetManager datasetManager = PersistenceFactory.getDatasetManager();
		
		statement.executeUpdate("DELETE FROM DATASET_OPTION_MAP WHERE DATASET_INFO IN (SELECT ID FROM APP.DATASET_INFO WHERE TYPE = 'ZODIACAL')");
		statement.executeUpdate("DELETE FROM DATASET WHERE ID IN (SELECT ID FROM APP.DATASET_INFO WHERE TYPE = 'ZODIACAL')");
		statement.executeUpdate("DELETE FROM DATASET_INFO WHERE TYPE = 'ZODIACAL'");

		// Install datasets
		try {
			Pair<Scanner, Boolean> inPair = DbInstaller.getDatasetList(null, dataImporter, datasetManager);
			Scanner in = inPair.getValue0();
			boolean isDefault = inPair.getValue1();
			in.useDelimiter("}");
			while (in.hasNext()) {
				String datasetInfo = in.next();
				datasetInfo = datasetInfo.substring(datasetInfo.indexOf('{')+1).trim();
				if (datasetInfo.contains("file=Galactic.dat")
				|| datasetInfo.contains("file=HighEclipticZodiacalLight.dat")
				|| datasetInfo.contains("file=LowEclipticZodiacalLight.dat")) {
					System.out.println(datasetInfo);
					DbInstaller.importDataset(datasetInfo, dataImporter, datasetManager, isDefault);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		ArrayList<String> presetsResources = new ArrayList<String>();

		// Sources presets
		presetsResources.add("/installdata/presets/sources/BB-T10000K-MAGJ0-z0.xml");
		presetsResources.add("/installdata/presets/sources/BB-T5000K-MAGJ20-z0.xml");
		presetsResources.add("/installdata/presets/sources/BB-T5000K-MAGJ20-z1.xml");
		presetsResources.add("/installdata/presets/sources/Sources-Halpha - CFRS-03.125.xml");
		presetsResources.add("/installdata/presets/sources/Sources-Halpha - CFRS-03.1534.xml");
		presetsResources.add("/installdata/presets/sources/Sources-Halpha - CFRS-14.0600.xml");

		// Sites
		presetsResources.add("/installdata/presets/sites/Demo - Ground site.xml");
		presetsResources.add("/installdata/presets/sites/Demo - Space site.xml");

		// Instruments
		presetsResources.add("/installdata/presets/instruments/Demo - Imaging Instrument 1m Telescope.xml");
		presetsResources.add("/installdata/presets/instruments/Demo - Spectroscopic instrument 1m Telescope.xml");

		// Obs params
		presetsResources.add("/installdata/presets/obs_params/Demo - Obs Parameters - Int 1s.xml");

		ImportHelper.importPresets(presetsResources, DbUpdater.class);
	}


	static void updateDatabase() {
		Connection connection = null;
		try {
			connection = DriverManager.getConnection("jdbc:derby:db");
			// Set auto commit to false to begin the transaction
			connection.setAutoCommit(true);
			int oldVersion = getOldVersion(connection);
			if (oldVersion < DB_VERSION) {
				SplashScreen.getInstance().setProgression("Updating DB");
				runUpdates(connection, oldVersion);
			}
			// Commit the transaction
		} catch (IllegalAccessException ex) {
			reflectionException(connection, ex);
		} catch (InvocationTargetException ex) {
			reflectionException(connection, ex);
		} catch (NoSuchMethodException ex) {
			reflectionException(connection, ex);
		} catch (SQLException ex) {
			rollbackTransaction(connection);
			logger.error("DbUpdater failed to check or update the database", ex);
			System.exit(-1);
		} finally {
			closeConnection(connection);
		}
	}

	private static void reflectionException(Connection connection, Exception ex) {
		rollbackTransaction(connection);
		logger.error("Failed to call patch via reflection", ex);
		System.exit(-1);
	}

	private static void runUpdates(Connection connection, int oldVersion) throws NoSuchMethodException, 
	IllegalAccessException, InvocationTargetException, SQLException {
		logger.info("Database is out of date (" + oldVersion + " instead of " +
				DB_VERSION + ")");
		// Ask for the user permission to update the database
		SplashScreen parent = null;
		if (SplashScreen.getInstance().isVisible()) {
			parent = SplashScreen.getInstance();
		}
		int selection = JOptionPane.showConfirmDialog(parent,
				"Current installed database schema is out of date.\n" +
						"Do you want me to update it?\n" +
						"WARNING:\n" +
						"This operation will might make major changes to the database, so\n" +
						"it is wise to make a backup of your important data",
						null, JOptionPane.YES_NO_OPTION);
		if (selection == JOptionPane.YES_OPTION) {
			// Here we need to call all the update methods necessary. For this we
			// use reflection (first class functions would be nicer, but java doesn't have them :( )
			for (int i = oldVersion; i < DB_VERSION; i++) {
				logger.info("Updating the database from version " + i + " to version " + (i+1) + "...");
				Statement statement = connection.createStatement();

				DecimalFormat formatter = new DecimalFormat("0"); //used to format int to string
				String methodName = "updateFromVersion" + formatter.format(i) + "To" + formatter.format(i+1);
				// We need to used the getDeclaredMethod and the setAccessible because we are accessing private methods
				Method patchMathod = DbUpdater.class.getDeclaredMethod(methodName, Statement.class);
				patchMathod.setAccessible(true);
				patchMathod.invoke(null, statement);

				// We update the version of the database
				statement.executeUpdate("UPDATE ETC_INFO SET DB_VERSION = " + (i+1));
				statement.close();
				logger.info("Updated database to version " + (i+1));
			}
		}
	}

	private static void rollbackTransaction(Connection connection) {
		if (connection == null)
			return;
		try {
			connection.rollback();
		} catch (SQLException ex) {
			logger.error("DbUpdater failed to rollback the transaction", ex);
		}
	}


	private static void closeConnection(Connection connection) {
		if (connection == null)
			return;
		try {
			connection.close();
		} catch (SQLException ex) {
			logger.warn("DbUpdater failed to close database connection", ex);
		}
	}

	private static int getOldVersion(Connection connection) throws SQLException {
		// First check if the database is that old that it doesn't have the
		// ETC_INFO table
		if (!etcInfoTableExists(connection)) {
			logger.info("ETC_INFO table does not exist. Creating it now...");
			createEtcInfoTable(connection);
			return 0;
		}
		// Here we know the table exists, so we get the version from it
		return getOldVersionFromTable(connection);
	}

	private static int getOldVersionFromTable(Connection connection) throws SQLException {
		Statement statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery("SELECT MAX(DB_VERSION) FROM ETC_INFO");
		resultSet.next();
		int result = resultSet.getInt(1);
		statement.close();
		return result;
	}

	private static boolean etcInfoTableExists(Connection connection) throws SQLException {
		DatabaseMetaData metaData = connection.getMetaData();
		ResultSet tables = metaData.getTables(null, null, "ETC_INFO", null);
		boolean result = tables.next();
		tables.close();
		return result;
	}

	private static void createEtcInfoTable(Connection connection) throws SQLException {
		Statement statement = connection.createStatement();
		statement.executeUpdate("CREATE TABLE ETC_INFO (DB_VERSION INT NOT NULL)");
		statement.executeUpdate("INSERT INTO ETC_INFO VALUES (0)");
		statement.close();
		logger.info("Created the ETC_INFO table and set the DB_VERSION to 0");
	}

}
