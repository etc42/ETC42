/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.controller;

import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;
import org.cnrs.lam.dis.etc.dataimportexport.DataExporter;
import org.cnrs.lam.dis.etc.dataimportexport.DataImporter;
import org.cnrs.lam.dis.etc.dataimportexport.SourceWithDatasets;
import org.cnrs.lam.dis.etc.dataimportexport.WrongFormatException;
import org.cnrs.lam.dis.etc.datamodel.ComponentInfo;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.DatasetProvider;
import org.cnrs.lam.dis.etc.datamodel.Source;
import org.cnrs.lam.dis.etc.persistence.DatasetManager;
import org.cnrs.lam.dis.etc.persistence.InUseException;
import org.cnrs.lam.dis.etc.persistence.NameExistsException;
import org.cnrs.lam.dis.etc.persistence.NameNotFoundException;
import org.cnrs.lam.dis.etc.persistence.PersistenceDeviceException;
import org.cnrs.lam.dis.etc.persistence.PersistenceFactory;
import org.cnrs.lam.dis.etc.persistence.SessionManager;
import org.cnrs.lam.dis.etc.ui.Messenger;
import org.cnrs.lam.dis.etc.ui.SourceListener;
import org.cnrs.lam.dis.etc.ui.UIManager;

/**
 *
 * @author Nikolaos Apostolakos
 */
final class SourceListenerImpl implements SourceListener {

    /** The bundle for retrieving the i18n strings for the messages.*/
    private static ResourceBundle bundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/controller/messages");
    /** The logger of the class.*/
    private static Logger logger = Logger.getLogger(SourceListenerImpl.class.getName());

    private SessionManager sessionManager;
    private DatasetManager datasetManager;
    private Messenger messenger;
    private UIManager uiManager;
    private DataExporter dataExporter;
    private DataImporter dataImporter;
    private DatasetProvider datasetProvider;

    public SourceListenerImpl(SessionManager sessionManager, DatasetManager datasetManager, Messenger messenger,
            UIManager uiManager, DataExporter dataExporter, DataImporter dataImporter,
            DatasetProvider datasetProvider) {
        this.sessionManager = sessionManager;
        this.datasetManager = datasetManager;
        this.messenger = messenger;
        this.uiManager = uiManager;
        this.dataExporter = dataExporter;
        this.dataImporter = dataImporter;
        this.datasetProvider = datasetProvider;
    }

    public void createNewSource(ComponentInfo newSourceInfo) {
        //First check if the current source has been modified and if it is
        // ask the user what he wants to do and do it
        if (isCurrentSourceModified()) {
            if (!handleModifiedCurrentSource()) {
                return;
            }
        }

        // Create the instance of the new source
        Source newSource = getNewSourceFromSessionManager(newSourceInfo);
        if (newSource == null) {
            return;
        }

        // Set the new source as the current one and notify the user interface
        CurrentSessionContainer.getCurrentSession().setSource(newSource);
        uiManager.sessionModified();
    }

    /**
     * Creates a new instance of an source by using the createNewSource()
     * method of the session manager. If the creation fails it shows the necessary
     * messages to the user and it returns null.
     * @param info The name and description of the new source
     * @return The new source instance
     */
    private Source getNewSourceFromSessionManager(ComponentInfo info) {
        Source newSource = null;
        try {
            newSource = sessionManager.createNewSource(info);
        } catch (NameExistsException ex) {
            logger.warn("Did not create new source because there is already " +
                    "an source with name " + info.getName(), ex);
            messenger.error(MessageFormat.format(bundle.getString("CREATE_SOURCE_NAME_EXISTS"), info.getName()));
        } catch (PersistenceDeviceException ex) {
            logger.warn("Failed to create new source with name " +
                    info.getName() + " because there of an error from the percistence device.", ex);
            messenger.error(MessageFormat.format(bundle.getString("CREATE_SOURCE_DEVICE_ERROR"), info.getName()));
        }
        return newSource;
    }

    /**
     * This method handles the case when an action will replace the current
     * source, but the current source is modified. It presents to the
     * user a dialog for selecting what he wants to do (save the changes, ignore
     * the changes or cancel the action). If the user selects to save the changes
     * then this method will persist the current source.
     * @return true if the action can continue, false otherwise
     */
    private boolean handleModifiedCurrentSource() {
        boolean canContinue = false;
        // Get the decision of the user
        String message = bundle.getString("CURRENT_SOURCE_MODIFIED");
        List<String> list = new ArrayList<String>(3);
        list.add(bundle.getString("SAVE_OPTION"));
        list.add(bundle.getString("IGNORE_OPTION"));
        list.add(bundle.getString("CANCEL_OPTION"));
        String decision = messenger.decision(message, list);

        if (decision == null || decision.equals(list.get(2))) {
            // Here we know the user wants to cancel so return false to stop the calling action
            canContinue = false;
        } else if (decision.equals(list.get(1))) {
            // Ignore the modified data. Let the calling action to continue without saving the data
            canContinue = true;
        } else if (decision.equals(list.get(0))) {
            // Save the current data and allow the calling action to continue if successful
            canContinue = persistCurrentSource();
        }
        return canContinue;
    }

    public void loadSource(ComponentInfo sourceInfo) {
        // First check if the requested source is the current source.
        // If it is do nothing.
        if (sourceInfo.getName().equals(CurrentSessionContainer.getCurrentSession().
                getSource().getInfo().getName())) {
            return;
        }
        // Check if the current source has been modified and if it is
        // ask the user what he wants to do and do it
        if (isCurrentSourceModified()) {
            if (!handleModifiedCurrentSource()) {
                return;
            }
        }
        // Load the instance of the source
        Source loadedSource = loadSourceFromSessionManager(sourceInfo);
        if (loadedSource == null) {
            return;
        }
        // Set the loaded source as the current one and notify the user interface
        CurrentSessionContainer.getCurrentSession().setSource(loadedSource);
        uiManager.sessionModified();
    }

    /**
     * Loads an instance of an source by using the loadSource()
     * method of the session manager. If the load fails it shows the necessary
     * messages to the user and it returns null.
     * @param info The name of the source to load
     * @return The loaded source instance
     */
    private Source loadSourceFromSessionManager(ComponentInfo info) {
        Source loadedSource = null;
        try {
            loadedSource = sessionManager.loadSource(info);
        } catch (NameNotFoundException ex) {
            logger.warn("Did not load the source because there is no " +
                    "source with name " + info.getName(), ex);
            messenger.error(MessageFormat.format(bundle.getString("LOAD_SOURCE_NOT_FOUND"), info.getName()));
        } catch (PersistenceDeviceException ex) {
            logger.warn("Failed to load source with name " + info.getName() +
                    " because of an error from the percistence device.", ex);
            messenger.error(MessageFormat.format(bundle.getString("LOAD_SOURCE_DEVICE_ERROR"), info.getName()));
        }
        return loadedSource;
    }

    public void reloadCurrentSource() {
        // Check if the source exist in the database. If not give a message
        if (!sessionManager.isSourceInDatabase(CurrentSessionContainer.getCurrentSession().getSource())) {
            messenger.warn(bundle.getString("RELOAD_SOURCE_NOT_IN_DB_MESSAGE"));
            return;
        }
        // Check if the source has been modified. If it is not we don't need to do anything
        if (!isCurrentSourceModified()) {
            return;
        }
        // Ask the user if he really wants to do the reload and lose any changes
        if (!getUserConfirmationForReload()) {
            return;
        }
        // Reload the curernt source from the persistence device
        ComponentInfo currentInfo = CurrentSessionContainer.getCurrentSession().getSource().getInfo();
        Source loadedSource = loadSourceFromSessionManager(currentInfo);
        if (loadedSource == null) {
            return;
        }
        // Set the loaded source as the current one and notify the user interface
        CurrentSessionContainer.getCurrentSession().setSource(loadedSource);
        uiManager.sessionModified();
    }

    /**
     * Gets a confirmation from the user to reload the current source.
     * @return true if the user confirms, false otherwise
     */
    private boolean getUserConfirmationForReload() {
        boolean confirmed = false;
        String message = bundle.getString("CONFIRM_SOURCE_RELOAD");
        List<String> list = new ArrayList<String>(2);
        list.add(bundle.getString("YES_OPTION"));
        list.add(bundle.getString("NO_OPTION"));
        String decision = messenger.decision(message, list);

        if (decision == null || decision.equals(list.get(1))) {
            // Here we know the user wants to cancel so return false to stop the calling action
            confirmed = false;
        } else if (decision.equals(list.get(0))) {
            // Got the confirmation for reloading
            confirmed = true;
        }
        return confirmed;
    }

    /**
     * Handles a user request for saving the current source. If the current
     * source has not been modified (its representation in the persistence
     * device has the same information) nothing is done. Otherwise it persists
     * the source and it updates the representation shown to the user.
     */
    public void saveCurrentSource() {
        // If the current source has not been modified we do not need to do anything
        if (!isCurrentSourceModified()) {
            return;
        }

        // Persist the current source. On success notify the user
        // interface to update the user representation for possible changes
        // (like dataset namespaces, etc)
        if (persistCurrentSource()) {
            uiManager.sessionModified();
        }
    }

    /**
     * Tries to persist all the modifications done to the current source.
     * If it succeed it returns true. If there is any problem with the persistence
     * device it shows an error to the user and it returns false.
     * @return true if the current source is persisted successfully, false otherwise
     */
    private boolean persistCurrentSource() {
        // Check if the current source is the "new" source. In this case saving is not allowed
        if (CurrentSessionContainer.getCurrentSession().getSource().getInfo().getName().equals("new")) {
            messenger.warn(bundle.getString("SAVE_NEW_SOURCE_MESSAGE"));
            return false;
        }
        boolean persisted = false;
        Source currentSource = CurrentSessionContainer.getCurrentSession().getSource();
        try {
            sessionManager.saveSource(currentSource);
            persisted = true;
        } catch (NameExistsException ex) {
            logger.warn("Failed to persist the current source because " +
                    "it is never persisted and there is already " +
                    "an source with name " + currentSource.getInfo().getName(), ex);
            messenger.error(MessageFormat.format(bundle.getString("CREATE_SOURCE_NAME_EXISTS"), currentSource.getInfo().getName()));
        } catch (PersistenceDeviceException ex) {
            logger.error("Failed to persist the current source (" +
                    currentSource.getInfo().getName() + ") because of a device error", ex);
            String message = MessageFormat.format(bundle.getString("SAVE_SOURCE_DEVICE_ERROR"),
                    currentSource.getInfo().getName());
            messenger.error(message);
            persisted = false;
        }
        return persisted;
    }

    /**
     * Checks if the current source is different from its representation in
     * the persistence device. If they are the same it returns true. If they
     * are different or there is no representation in the persistence device
     * or the persistence device is not accessible it returns true.
     * @return false if the current source is the same with its representation
     * in the persistence device, true otherwise
     */
    public boolean isCurrentSourceModified() {
        boolean modified = true;
        Source currentSource = CurrentSessionContainer.getCurrentSession().getSource();
        try {
            modified = !sessionManager.isSourcePersisted(currentSource);
        } catch (PersistenceDeviceException ex) {
            logger.error("There was a persistence device error while checking " +
                    "if source " + currentSource.getInfo().getName() + " is persisted. " +
                    "Non persistence of the source is assumed.", ex);
            modified = true;
        }
        return modified;
    }

    public void saveCurrentSourceAs(ComponentInfo newSourceInfo) {
        // Try to save the current source with a different name
        Source currentSource = CurrentSessionContainer.getCurrentSession().getSource();
        Source newSource = null;
        try {
            newSource = sessionManager.saveSourceAs(currentSource, newSourceInfo);
        } catch (NameExistsException ex) {
            logger.warn("Did not copied source because there is already " +
                    "an source with name " + newSourceInfo.getName(), ex);
            messenger.error(MessageFormat.format(bundle.getString("CREATE_SOURCE_NAME_EXISTS"), newSourceInfo.getName()));
        } catch (PersistenceDeviceException ex) {
            logger.warn("Failed to copy source with name " +
                    newSourceInfo.getName() + " because of an error from the percistence device.", ex);
            messenger.error(MessageFormat.format(bundle.getString("CREATE_SOURCE_DEVICE_ERROR"), newSourceInfo.getName()));
        }
        // If everything was fine update the session and the user interface
        if (newSource != null) {
            CurrentSessionContainer.getCurrentSession().setSource(newSource);
            uiManager.sessionModified();
        }
    }

    public void deleteSource(ComponentInfo sourceInfo) {
        // Check if the source to delete is the current source. Then we do nothing
        if (CurrentSessionContainer.getCurrentSession().getSource().getInfo().getName().equals(sourceInfo.getName())) {
            messenger.error(MessageFormat.format(bundle.getString("DELETE_SOURCE_CURRENT"), sourceInfo.getName()));
            return;
        }
        // Get a confirmation from the user that he really wants to delete the source
        String message = MessageFormat.format(bundle.getString("DELETE_SOURCE_CONFIRM"),
                sourceInfo.getName());
        List<String> options = new ArrayList<String>(2);
        options.add(bundle.getString("DELETE_OPTION"));
        options.add(bundle.getString("CANCEL_OPTION"));
        if(!options.get(0).equals(messenger.decision(message, options))) {
            return;
        }
        // Delete the source from the persistence device by using the SessionManager
        // provided by the persistence framework
        logger.info("User requested deletion of source " + sourceInfo.getName());
        try {
            sessionManager.deleteSource(sourceInfo);
            logger.info("Source was deleted successfully");
            message = MessageFormat.format(bundle.getString("DELETE_SOURCE_SUCCESS"), sourceInfo.getName());
            messenger.info(message);
            uiManager.sessionModified();
        } catch (NameNotFoundException ex) {
            logger.warn("Failed to delete source " + sourceInfo.getName() +
                    " because it does not exist in persistence device", ex);
            message = MessageFormat.format(bundle.getString("DELETE_SOURCE_NOT_FOUND"), sourceInfo.getName());
            messenger.error(message);
        } catch (InUseException ex) {
            StringBuffer usersBuffer = new StringBuffer();
            for (String session : ex.getUsers()) {
                usersBuffer.append(session);
                usersBuffer.append(", ");
            }
            String users = usersBuffer.toString();
            users = users.substring(0, users.length() - 2);
            logger.warn("Failed to delete source " + sourceInfo.getName() +
                    " because it is in use by " + users, ex);
            message = MessageFormat.format(bundle.getString("DELETE_SOURCE_IN_USE"), sourceInfo.getName(), users);
            messenger.error(message);
        } catch (PersistenceDeviceException ex) {
            logger.warn("Failed to delete source " + sourceInfo.getName() +
                    " because there was an error while accessing the persistence device", ex);
            message = MessageFormat.format(bundle.getString("DELETE_SOURCE_DEVICE_ERROR"), sourceInfo.getName());
            messenger.error(message);
        }
    }

    public void importFromFile(File file, ComponentInfo sourceInfo) {
        // Check if the current source has been modified and if it is
        // ask the user what he wants to do and do it
        if (isCurrentSourceModified()) {
            if (!handleModifiedCurrentSource()) {
                return;
            }
        }
        Thread importThread = new ImportSourceThread(file, sourceInfo);
        messenger.actionStarted(bundle.getString("IMPORT_SOURCE_MESSAGE"), importThread);
        importThread.start();
    }

    private class ImportSourceThread extends Thread {
        private File file;
        private ComponentInfo sourceInfo;

        public ImportSourceThread(File file, ComponentInfo sourceInfo) {
            super();
            this.file = file;
            this.sourceInfo = sourceInfo;
        }

        @Override
        public void run() {
            boolean ok = true;
            SourceWithDatasets source = null;
            try {
                source = dataImporter.importSourceFromFile(file, sourceInfo);
            } catch (IOException ex) {
                logger.warn("Failed to import source because of IO problem", ex);
                String message = MessageFormat.format(bundle.getString("IMPORT_SOURCE_ERROR"), sourceInfo.getName());
                messenger.error(message);
                ok = false;
            } catch (WrongFormatException ex) {
                logger.info("Failed to import source because of wrong format", ex);
                String message = MessageFormat.format(bundle.getString("IMPORT_SOURCE_WRONG_FORMAT"), file.getName());
                messenger.error(message);
                ok = false;
            }
            if (!ok) {
                messenger.actionStoped(this);
                return;
            }
            PersistenceFactory.beginTransaction();
            try {
                Dataset dataset = source.getSpectralDistributionTemplateDataset();
                if (dataset != null) {
                    datasetManager.saveDataset(dataset);
                }
                sessionManager.saveSourceAs(source, sourceInfo);
                PersistenceFactory.commitTransaction();
            } catch (NameExistsException ex) {
                PersistenceFactory.rollbackTransaction();
                logger.info("Failed to import source because of name already exists", ex);
                String message = MessageFormat.format(bundle.getString("CREATE_SOURCE_NAME_EXISTS"), sourceInfo.getName());
                messenger.error(message);
                ok = false;
            } catch (PersistenceDeviceException ex) {
                PersistenceFactory.rollbackTransaction();
                logger.warn("Failed to import source because of an error on persistence device", ex);
                String message = MessageFormat.format(bundle.getString("IMPORT_SOURCE_ERROR"), sourceInfo.getName());
                messenger.error(message);
                ok = false;
            }
            if (!ok) {
                messenger.actionStoped(this);
                return;
            }
            Source persistedSource = null;
            try {
                persistedSource = sessionManager.loadSource(sourceInfo);
            } catch (NameNotFoundException ex) {
                logger.warn("Source just imported not found in persistence device", ex);
                String message = MessageFormat.format(bundle.getString("IMPORT_SOURCE_ERROR"), sourceInfo.getName());
                messenger.error(message);
                ok = false;
            } catch (PersistenceDeviceException ex) {
                logger.warn("Failed to retrieve just imported source from persistence device", ex);
                String message = MessageFormat.format(bundle.getString("IMPORT_SOURCE_ERROR"), sourceInfo.getName());
                messenger.error(message);
                ok = false;
            }
            messenger.actionStoped(this);
            if (!ok) {
                return;
            }
            CurrentSessionContainer.getCurrentSession().setSource(persistedSource);
            uiManager.sessionModified();
        }
    }

    public void exportCurrentSourceInFile(File file) {
        Source currentSource = CurrentSessionContainer.getCurrentSession().getSource();
        try {
            dataExporter.exportSourceInFile(file, currentSource, datasetProvider);
        } catch (IOException ex) {
            logger.warn("Failed to export source", ex);
            String message = MessageFormat.format(bundle.getString("EXPORT_SOURCE_ERROR"), currentSource.getInfo().getName());
            messenger.error(message);
        }
    }

}
