/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import org.cnrs.lam.dis.etc.datamodel.ComponentInfo;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.cnrs.lam.dis.etc.datamodel.Instrument;
import org.cnrs.lam.dis.etc.datamodel.ObsParam;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.cnrs.lam.dis.etc.datamodel.Site;
import org.cnrs.lam.dis.etc.datamodel.Source;
import org.cnrs.lam.dis.etc.persistence.DatasetManager;
import org.cnrs.lam.dis.etc.persistence.PersistenceDeviceException;
import org.cnrs.lam.dis.etc.persistence.SessionManager;
import org.cnrs.lam.dis.etc.ui.InfoProvider;
import org.cnrs.lam.dis.etc.ui.Messenger;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.MockitoAnnotations;
import org.mockito.Mock;
import static org.mockito.BDDMockito.*;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class InfoProviderImplTest {

    @Mock Messenger messenger;
    @Mock DatasetManager datasetManager;
    @Mock SessionManager sessionManager;
    @Mock Instrument instrument;
    @Mock Site site;
    @Mock Source source;
    @Mock ObsParam obsParam;
    @Mock Session session;
    ResourceBundle bundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/controller/messages");

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        when(instrument.getInfo()).thenReturn(new ComponentInfo("instrument", null));
        when(site.getInfo()).thenReturn(new ComponentInfo("site", null));
        when(source.getInfo()).thenReturn(new ComponentInfo("source", null));
        when(obsParam.getInfo()).thenReturn(new ComponentInfo("obsParam", null));
        when(session.getInstrument()).thenReturn(instrument);
        when(session.getSite()).thenReturn(site);
        when(session.getSource()).thenReturn(source);
        when(session.getObsParam()).thenReturn(obsParam);
        CurrentSessionContainer.setCurrentSession(session);
    }

    /**
     * Tests that when the User Interface requests for a list of available
     * datasets, the list returned by thePersistence Framework is transfered
     * correctly. The test list contains two datasets with different names,
     * namespaces and rescriptions.
     */
    @Test
    public void getAvailableDatasetList_Normal() throws Exception {
        //given
        List<DatasetInfo> infoList = new ArrayList<DatasetInfo>();
        infoList.add(new DatasetInfo("One", "namespace1", "Description1"));
        infoList.add(new DatasetInfo("Two", "namespace2", "Description2"));
        given(datasetManager.getAvailableDatasetList(Dataset.Type.FWHM, null)).willReturn(infoList);
        
        //when
        InfoProvider infoProvider = new InfoProviderImpl(datasetManager, sessionManager, messenger, null);
        List<DatasetInfo> result = infoProvider.getAvailableDatasetList(Dataset.Type.FWHM, null);

        //then
        assertEquals(2, result.size());
        assertEquals("One", result.get(0).getName());
        assertEquals("Two", result.get(1).getName());
        assertEquals("namespace1", result.get(0).getNamespace());
        assertEquals("namespace2", result.get(1).getNamespace());
        assertEquals("Description1", result.get(0).getDescription());
        assertEquals("Description2", result.get(1).getDescription());
    }

    /**
     * Tests that when there are no datasets available the User Interface will
     * receive an empty list.
     */
    @Test
    public void getAvailableDatasetList_Empty() throws Exception {
        //given
        given(datasetManager.getAvailableDatasetList(Dataset.Type.FWHM, null)).willReturn(new ArrayList<DatasetInfo>());
        
        //when
        InfoProvider infoProvider = new InfoProviderImpl(datasetManager, sessionManager, messenger, null);
        List<DatasetInfo> result = infoProvider.getAvailableDatasetList(Dataset.Type.FWHM, null);

        //then
        assertNotNull(result);
        assertTrue(result.isEmpty());
    }

    /**
     * Tests that when there is a problem with retrieving the datasets list
     * from the persistence device the User Interface will receive an empty
     * list and the user will be notified with an error message.
     */
    @Test
    public void getAvailableDatasetList_IOProblem() throws Exception {
        //given
        given(datasetManager.getAvailableDatasetList(Dataset.Type.FWHM, null)).willThrow(new PersistenceDeviceException());
        
        //when
        InfoProvider infoProvider = new InfoProviderImpl(datasetManager, sessionManager, messenger, null);
        List<DatasetInfo> result = infoProvider.getAvailableDatasetList(Dataset.Type.FWHM, null);
        
        //then
        verify(messenger).error(bundle.getString("RETRIEVE_DATASET_LIST_ERROR"));
        assertNotNull(result);
        assertTrue(result.isEmpty());
    }

    /**
     * Tests that when the User Interface requests for a list of available
     * instruments, the list returned by the Persistence Framework is transfered
     * correctly. The test list contains two instruments with different names
     *  and descriptions.
     */
    @Test
    public void getAvailableInstrumentList_Normal() throws Exception {
        //given
        List<ComponentInfo> componentList = new ArrayList<ComponentInfo>();
        componentList.add(new ComponentInfo("One", "Description1"));
        componentList.add(new ComponentInfo("Two", "Description2"));
        given(sessionManager.getAvailableInstrumentList()).willReturn(componentList);
        
        //when
        InfoProvider infoProvider = new InfoProviderImpl(datasetManager, sessionManager, messenger, null);
        List<ComponentInfo> result = infoProvider.getAvailableInstrumentList();
        
        //then
        assertEquals(3, result.size());
        assertEquals("One", result.get(0).getName());
        assertEquals("Two", result.get(1).getName());
        assertEquals("instrument", result.get(2).getName());
        assertEquals("Description1", result.get(0).getDescription());
        assertEquals("Description2", result.get(1).getDescription());
        assertNull(result.get(2).getDescription());
    }

    /**
     * Tests that when there are no instruments available the User Interface will
     * receive an empty list.
     */
    @Test
    public void getAvailableInstrumentList_Empty() throws Exception {
        //given
        given(sessionManager.getAvailableInstrumentList()).willReturn(new ArrayList<ComponentInfo>());
        
        //when
        InfoProvider infoProvider = new InfoProviderImpl(datasetManager, sessionManager, messenger, null);
        List<ComponentInfo> result = infoProvider.getAvailableInstrumentList();
        
        //then
        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals("instrument", result.get(0).getName());
        assertNull(result.get(0).getDescription());
    }

    /**
     * Tests that when there is a problem with retrieving the instruments list
     * from the persistence device the User Interface will receive an empty
     * list and the user will be notified with an error message.
     */
    @Test
    public void getAvailableInstrumentList_IOProblem() throws Exception {
        //given
        given(sessionManager.getAvailableInstrumentList()).willThrow(new PersistenceDeviceException());
        
        //when
        InfoProvider infoProvider = new InfoProviderImpl(datasetManager, sessionManager, messenger, null);
        List<ComponentInfo> result = infoProvider.getAvailableInstrumentList();
        
        //then
        verify(messenger).error(bundle.getString("RETRIEVE_INSTRUMENT_LIST_ERROR"));
        assertNotNull(result);
        assertTrue(result.isEmpty());
    }

    /**
     * Tests that when the User Interface requests for a list of available
     * sites, the list returned by the Persistence Framework is transfered
     * correctly. The test list contains two sites with different names
     *  and descriptions.
     */
    @Test
    public void getAvailableSiteList_Normal() throws Exception {
        //given
        List<ComponentInfo> componentList = new ArrayList<ComponentInfo>();
        componentList.add(new ComponentInfo("One", "Description1"));
        componentList.add(new ComponentInfo("Two", "Description2"));
        given(sessionManager.getAvailableSiteList()).willReturn(componentList);
        
        //when
        InfoProvider infoProvider = new InfoProviderImpl(datasetManager, sessionManager, messenger, null);
        List<ComponentInfo> result = infoProvider.getAvailableSiteList();
        
        //then
        assertEquals(3, result.size());
        assertEquals("One", result.get(0).getName());
        assertEquals("Two", result.get(1).getName());
        assertEquals("site", result.get(2).getName());
        assertEquals("Description1", result.get(0).getDescription());
        assertEquals("Description2", result.get(1).getDescription());
        assertNull(result.get(2).getDescription());
    }

    /**
     * Tests that when there are no sites available the User Interface will
     * receive an empty list.
     */
    @Test
    public void getAvailableSiteList_Empty() throws Exception {
        //given
        given(sessionManager.getAvailableSiteList()).willReturn(new ArrayList<ComponentInfo>());
        
        //when
        InfoProvider infoProvider = new InfoProviderImpl(datasetManager, sessionManager, messenger, null);
        List<ComponentInfo> result = infoProvider.getAvailableSiteList();
        
        //then
        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals("site", result.get(0).getName());
        assertNull(result.get(0).getDescription());
    }

    /**
     * Tests that when there is a problem with retrieving the sites list
     * from the persistence device the User Interface will receive an empty
     * list and the user will be notified with an error message.
     */
    @Test
    public void getAvailableSiteList_IOProblem() throws Exception {
        //given
        given(sessionManager.getAvailableSiteList()).willThrow(new PersistenceDeviceException());
        
        //when
        InfoProvider infoProvider = new InfoProviderImpl(datasetManager, sessionManager, messenger, null);
        List<ComponentInfo> result = infoProvider.getAvailableSiteList();
        
        //then
        verify(messenger).error(bundle.getString("RETRIEVE_SITE_LIST_ERROR"));
        assertNotNull(result);
        assertTrue(result.isEmpty());
    }

    /**
     * Tests that when the User Interface requests for a list of available
     * sources, the list returned by the Persistence Framework is transfered
     * correctly. The test list contains two sources with different names
     *  and descriptions.
     */
    @Test
    public void getAvailableSourceList_Normal() throws Exception {
        //given
        List<ComponentInfo> componentList = new ArrayList<ComponentInfo>();
        componentList.add(new ComponentInfo("One", "Description1"));
        componentList.add(new ComponentInfo("Two", "Description2"));
        given(sessionManager.getAvailableSourceList()).willReturn(componentList);
        
        //when
        InfoProvider infoProvider = new InfoProviderImpl(datasetManager, sessionManager, messenger, null);
        List<ComponentInfo> result = infoProvider.getAvailableSourceList();
        
        //then
        assertEquals(3, result.size());
        assertEquals("One", result.get(0).getName());
        assertEquals("Two", result.get(1).getName());
        assertEquals("source", result.get(2).getName());
        assertEquals("Description1", result.get(0).getDescription());
        assertEquals("Description2", result.get(1).getDescription());
        assertNull(result.get(2).getDescription());
    }

    /**
     * Tests that when there are no sources available the User Interface will
     * receive an empty list.
     */
    @Test
    public void getAvailableSourceList_Empty() throws Exception {
        //given
        given(sessionManager.getAvailableSourceList()).willReturn(new ArrayList<ComponentInfo>());
        
        //when
        InfoProvider infoProvider = new InfoProviderImpl(datasetManager, sessionManager, messenger, null);
        List<ComponentInfo> result = infoProvider.getAvailableSourceList();
        
        //then
        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals("source", result.get(0).getName());
        assertNull(result.get(0).getDescription());
    }

    /**
     * Tests that when there is a problem with retrieving the sources list
     * from the persistence device the User Interface will receive an empty
     * list and the user will be notified with an error message.
     */
    @Test
    public void getAvailableSourceList_IOProblem() throws Exception {
        //given
        given(sessionManager.getAvailableSourceList()).willThrow(new PersistenceDeviceException());
        
        //when
        InfoProvider infoProvider = new InfoProviderImpl(datasetManager, sessionManager, messenger, null);
        List<ComponentInfo> result = infoProvider.getAvailableSourceList();
        
        //then
        verify(messenger).error(bundle.getString("RETRIEVE_SOURCE_LIST_ERROR"));
        assertNotNull(result);
        assertTrue(result.isEmpty());
    }

    /**
     * Tests that when the User Interface requests for a list of available
     * obsParams, the list returned by the Persistence Framework is transfered
     * correctly. The test list contains two obsParams with different names
     *  and descriptions.
     */
    @Test
    public void getAvailableObsParamList_Normal() throws Exception {
        //given
        List<ComponentInfo> componentList = new ArrayList<ComponentInfo>();
        componentList.add(new ComponentInfo("One", "Description1"));
        componentList.add(new ComponentInfo("Two", "Description2"));
        given(sessionManager.getAvailableObsParamList()).willReturn(componentList);
        
        //when
        InfoProvider infoProvider = new InfoProviderImpl(datasetManager, sessionManager, messenger, null);
        List<ComponentInfo> result = infoProvider.getAvailableObsParamList();
        
        //then
        assertEquals(3, result.size());
        assertEquals("One", result.get(0).getName());
        assertEquals("Two", result.get(1).getName());
        assertEquals("obsParam", result.get(2).getName());
        assertEquals("Description1", result.get(0).getDescription());
        assertEquals("Description2", result.get(1).getDescription());
        assertNull(result.get(2).getDescription());
    }

    /**
     * Tests that when there are no obsParams available the User Interface will
     * receive an empty list.
     */
    @Test
    public void getAvailableObsParamList_Empty() throws Exception {
        //given
        given(sessionManager.getAvailableObsParamList()).willReturn(new ArrayList<ComponentInfo>());
        
        //when
        InfoProvider infoProvider = new InfoProviderImpl(datasetManager, sessionManager, messenger, null);
        List<ComponentInfo> result = infoProvider.getAvailableObsParamList();
        
        //then
        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals("obsParam", result.get(0).getName());
        assertNull(result.get(0).getDescription());
    }

    /**
     * Tests that when there is a problem with retrieving the obsParams list
     * from the persistence device the User Interface will receive an empty
     * list and the user will be notified with an error message.
     */
    @Test
    public void getAvailableObsParamList_IOProblem() throws Exception {
        //given
        given(sessionManager.getAvailableObsParamList()).willThrow(new PersistenceDeviceException());
        
        //when
        InfoProvider infoProvider = new InfoProviderImpl(datasetManager, sessionManager, messenger, null);
        List<ComponentInfo> result = infoProvider.getAvailableObsParamList();
        
        //then
        verify(messenger).error(bundle.getString("RETRIEVE_OBS_PARAM_LIST_ERROR"));
        assertNotNull(result);
        assertTrue(result.isEmpty());
    }
}