/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.controller;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import org.cnrs.lam.dis.etc.datamodel.ComponentInfo;
import org.cnrs.lam.dis.etc.datamodel.Instrument;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.cnrs.lam.dis.etc.persistence.InUseException;
import org.cnrs.lam.dis.etc.persistence.NameExistsException;
import org.cnrs.lam.dis.etc.persistence.NameNotFoundException;
import org.cnrs.lam.dis.etc.persistence.PersistenceDeviceException;
import org.cnrs.lam.dis.etc.persistence.SessionManager;
import org.cnrs.lam.dis.etc.ui.Messenger;
import org.cnrs.lam.dis.etc.ui.UIManager;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.mockito.Mock;
import static org.mockito.BDDMockito.*;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class InstrumentListenerImplTest {

    @Mock SessionManager sessionManager;
    @Mock Messenger messenger;
    @Mock UIManager uiManager;
    @Mock Instrument currentInstrument;
    @Mock Session session;
    ResourceBundle bundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/controller/messages");

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        when(currentInstrument.getInfo()).thenReturn(new ComponentInfo("instrument", "InstrumentDescription"));
        when(session.getInstrument()).thenReturn(currentInstrument);
        CurrentSessionContainer.setCurrentSession(session);
    }

    @Test
    public void saveCurrentInstrument_AlreadyPersisted() throws Exception {
        //given
        given(sessionManager.isInstrumentPersisted(currentInstrument)).willReturn(true);
        
        //when
        InstrumentListenerImpl instrumentListener = new InstrumentListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        instrumentListener.saveCurrentInstrument();
        
        //then
        verify(sessionManager, never()).saveInstrument(currentInstrument);
        verify(uiManager, never()).sessionModified();
    }

    /**
     * This test checks that when there is a persistence device reading error
     * for which the check if the current currentInstrument is already persisted fails,
     * the system treats the current currentInstrument as modified and it proceeds with
     * saving it.
     */
    @Test
    public void saveCurrentInstrument_ErrorWhileCheckingIfAlreadyPersisted() throws Exception {
        //given
        given(sessionManager.isInstrumentPersisted(currentInstrument)).willThrow(new PersistenceDeviceException());
        
        //when
        InstrumentListenerImpl instrumentListener = new InstrumentListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        instrumentListener.saveCurrentInstrument();
        
        //then
        verify(sessionManager).saveInstrument(currentInstrument);
    }

    @Test
    public void saveCurrentInstrument_ErrorWhilePersisting() throws Exception {
        //given
        given(sessionManager.isInstrumentPersisted(currentInstrument)).willReturn(false);
        willThrow(new PersistenceDeviceException()).given(sessionManager).saveInstrument(currentInstrument);
        
        //when
        InstrumentListenerImpl instrumentListener = new InstrumentListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        instrumentListener.saveCurrentInstrument();
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("SAVE_INSTRUMENT_DEVICE_ERROR"), "instrument"));
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void saveCurrentInstrument_Success() throws Exception {
        //given
        given(sessionManager.isInstrumentPersisted(currentInstrument)).willReturn(false);
        
        //when
        InstrumentListenerImpl instrumentListener = new InstrumentListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        instrumentListener.saveCurrentInstrument();
        
        //then
        verify(sessionManager).saveInstrument(currentInstrument);
        verify(uiManager).sessionModified();
    }

    @Test
    public void createNewInstrument_CurrentInstrumentModifiedCanceled() throws Exception {
        //given
        given(sessionManager.isInstrumentPersisted(currentInstrument)).willReturn(false);
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_INSTRUMENT_MODIFIED"), "instrument");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("CANCEL_OPTION"));
        
        //when
        InstrumentListenerImpl instrumentListener = new InstrumentListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        ComponentInfo newInfo = new ComponentInfo("newInstrument", "newDescription");
        instrumentListener.createNewInstrument(newInfo);
        
        //then
        // Check that the current currentInstrument wasn't saved
        verify(sessionManager, never()).saveInstrument(any(Instrument.class));
        // Check that the current isntrument is not changed
        verify(session, never()).setInstrument(any(Instrument.class));
        // Check that he UI is not notified for session changes
        verify(uiManager, never()).sessionModified();
    }

    /**
     * This checks that if the isInstrumentPersisted() method fails with an exception
     * we have the same behavior like the current currentInstrument is modified.
     */
    @Test
    public void createNewInstrument_CurrentInstrumentModifiedCheckError() throws Exception {
        //given
        given(sessionManager.isInstrumentPersisted(currentInstrument)).willThrow(new PersistenceDeviceException());
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_INSTRUMENT_MODIFIED"), "instrument");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("CANCEL_OPTION"));
        
        //when
        InstrumentListenerImpl instrumentListener = new InstrumentListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        ComponentInfo newInfo = new ComponentInfo("newInstrument", "newDescription");
        instrumentListener.createNewInstrument(newInfo);
        
        //then
        // Check that the current currentInstrument wasn't saved
        verify(sessionManager, never()).saveInstrument(any(Instrument.class));
        // Check that the current isntrument is not changed
        verify(session, never()).setInstrument(any(Instrument.class));
        // Check that he UI is not notified for session changes
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void createNewInstrument_currentInstrumentModifiedSaveError() throws Exception {
        //given
        given(sessionManager.isInstrumentPersisted(currentInstrument)).willReturn(false);
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_INSTRUMENT_MODIFIED"), "instrument");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("SAVE_OPTION"));
        willThrow(new PersistenceDeviceException()).given(sessionManager).saveInstrument(currentInstrument);
        
        //when
        InstrumentListenerImpl instrumentListener = new InstrumentListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        ComponentInfo newInfo = new ComponentInfo("newInstrument", "newDescription");
        instrumentListener.createNewInstrument(newInfo);
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("SAVE_INSTRUMENT_DEVICE_ERROR"), "instrument"));
        // Check that the current isntrument is not changed
        verify(session, never()).setInstrument(any(Instrument.class));
        // Check that he UI is not notified for session changes
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void createNewInstrument_currentInstrumentModifiedSaveSuccess() throws Exception {
        //given
        given(sessionManager.isInstrumentPersisted(currentInstrument)).willReturn(false);
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_INSTRUMENT_MODIFIED"), "instrument");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("SAVE_OPTION"));
        ComponentInfo newInfo = new ComponentInfo("newInstrument", "newDescription");
        Instrument newInstrument = mock(Instrument.class);
        given(newInstrument.getInfo()).willReturn(newInfo);
        given(sessionManager.createNewInstrument(newInfo)).willReturn(newInstrument);
        
        //when
        InstrumentListenerImpl instrumentListener = new InstrumentListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        instrumentListener.createNewInstrument(newInfo);
        
        //then
        // Check that the current currentInstrument was saved
        verify(sessionManager).saveInstrument(currentInstrument);
        // Check that the current isntrument is changed
        verify(session).setInstrument(newInstrument);
        // Check that he UI is notified for session changes
        verify(uiManager).sessionModified();
    }

    @Test
    public void createNewInstrument_currentInstrumentModifiedSaveIgnore() throws Exception {
        //given
        given(sessionManager.isInstrumentPersisted(currentInstrument)).willReturn(false);
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_INSTRUMENT_MODIFIED"), "instrument");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("IGNORE_OPTION"));
        ComponentInfo newInfo = new ComponentInfo("newInstrument", "newDescription");
        Instrument newInstrument = mock(Instrument.class);
        given(newInstrument.getInfo()).willReturn(newInfo);
        given(sessionManager.createNewInstrument(newInfo)).willReturn(newInstrument);
        
        //when
        InstrumentListenerImpl instrumentListener = new InstrumentListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        instrumentListener.createNewInstrument(newInfo);
        
        //then
        // Check that the current currentInstrument was not saved
        verify(sessionManager, never()).saveInstrument(currentInstrument);
        // Check that the current isntrument is changed
        verify(session).setInstrument(newInstrument);
        // Check that he UI is notified for session changes
        verify(uiManager).sessionModified();
    }

    @Test
    public void createNewInstrument_NameExists() throws Exception {
        //given
        given(sessionManager.isInstrumentPersisted(currentInstrument)).willReturn(false);
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_INSTRUMENT_MODIFIED"), "instrument");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("IGNORE_OPTION"));
        ComponentInfo newInfo = new ComponentInfo("newInstrument", "newDescription");
        given(sessionManager.createNewInstrument(newInfo)).willThrow(new NameExistsException());
        
        //when
        InstrumentListenerImpl instrumentListener = new InstrumentListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        instrumentListener.createNewInstrument(newInfo);
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("CREATE_INSTRUMENT_NAME_EXISTS"), "newInstrument"));
        // Check that the current currentInstrument was not saved
        verify(sessionManager, never()).saveInstrument(currentInstrument);
        // Check that the current isntrument is not changed
        verify(session, never()).setInstrument(any(Instrument.class));
        // Check that he UI is notified for session changes
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void createNewInstrument_CreateFailedDeviceError() throws Exception {
        //given
        given(sessionManager.isInstrumentPersisted(currentInstrument)).willReturn(false);
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_INSTRUMENT_MODIFIED"), "instrument");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("IGNORE_OPTION"));
        ComponentInfo newInfo = new ComponentInfo("newInstrument", "newDescription");
        given(sessionManager.createNewInstrument(newInfo)).willThrow(new PersistenceDeviceException());
        
        //when
        InstrumentListenerImpl instrumentListener = new InstrumentListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        instrumentListener.createNewInstrument(newInfo);
        
        //then
        verify(messenger).error(bundle.getString("CREATE_INSTRUMENT_DEVICE_ERROR"));
        // Check that the current currentInstrument was not saved
        verify(sessionManager, never()).saveInstrument(currentInstrument);
        // Check that the current isntrument is not changed
        verify(session, never()).setInstrument(any(Instrument.class));
        // Check that he UI is notified for session changes
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void loadInstrument_IsCurrent() throws Exception {
        //when
        InstrumentListenerImpl instrumentListener = new InstrumentListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        ComponentInfo loadInfo = new ComponentInfo("instrument", "description");
        instrumentListener.loadInstrument(loadInfo);

        //then
        // Check that the current isntrument is not changed
        verify(session, never()).setInstrument(any(Instrument.class));
        // Check that he UI is not notified for session changes
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void loadInstrument_CurrentInstrumentModifiedCanceled() throws Exception {
        //given
        given(sessionManager.isInstrumentPersisted(currentInstrument)).willReturn(false);
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_INSTRUMENT_MODIFIED"), "instrument");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("CANCEL_OPTION"));
        
        //when
        InstrumentListenerImpl instrumentListener = new InstrumentListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        ComponentInfo loadInfo = new ComponentInfo("loadInstrument", "loadDescription");
        instrumentListener.loadInstrument(loadInfo);
        
        //then
        verify(sessionManager, never()).saveInstrument(currentInstrument);
        // Check that the current isntrument is not changed
        verify(session, never()).setInstrument(any(Instrument.class));
        // Check that he UI is not notified for session changes
        verify(uiManager, never()).sessionModified();
    }

    /**
     * This checks that if the isInstrumentPersisted() method fails with an exception
     * we have the same behavior like the current currentInstrument is modified.
     */
    @Test
    public void loadInstrument_CurrentInstrumentModifiedCheckError() throws Exception {
        //given
        given(sessionManager.isInstrumentPersisted(currentInstrument)).willThrow(new PersistenceDeviceException());
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_INSTRUMENT_MODIFIED"), "instrument");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("CANCEL_OPTION"));
        
        //when
        InstrumentListenerImpl instrumentListener = new InstrumentListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        ComponentInfo loadInfo = new ComponentInfo("loadInstrument", "loadDescription");
        instrumentListener.loadInstrument(loadInfo);
        
        //then
        verify(sessionManager, never()).saveInstrument(currentInstrument);
        // Check that the current isntrument is not changed
        verify(session, never()).setInstrument(any(Instrument.class));
        // Check that he UI is not notified for session changes
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void loadInstrument_CurrentInstrumentModifiedSaveError() throws Exception {
        //given
        given(sessionManager.isInstrumentPersisted(currentInstrument)).willReturn(false);
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_INSTRUMENT_MODIFIED"), "instrument");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("SAVE_OPTION"));
        willThrow(new PersistenceDeviceException()).given(sessionManager).saveInstrument(currentInstrument);
        
        //when
        InstrumentListenerImpl instrumentListener = new InstrumentListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        ComponentInfo loadInfo = new ComponentInfo("loadInstrument", "loadDescription");
        instrumentListener.loadInstrument(loadInfo);
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("SAVE_INSTRUMENT_DEVICE_ERROR"), "instrument"));
        // Check that the current isntrument is not changed
        verify(session, never()).setInstrument(any(Instrument.class));
        // Check that he UI is not notified for session changes
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void loadInstrument_currentInstrumentModifiedSaveSuccess() throws Exception {
        //given
        given(sessionManager.isInstrumentPersisted(currentInstrument)).willReturn(false);
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_INSTRUMENT_MODIFIED"), "instrument");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("SAVE_OPTION"));
        ComponentInfo loadInfo = new ComponentInfo("loadInstrument", "loadDescription");
        Instrument loadInstrument = mock(Instrument.class);
        given(loadInstrument.getInfo()).willReturn(loadInfo);
        given(sessionManager.loadInstrument(loadInfo)).willReturn(loadInstrument);
        
        //when
        InstrumentListenerImpl instrumentListener = new InstrumentListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        instrumentListener.loadInstrument(loadInfo);
        
        //then
        verify(sessionManager).saveInstrument(currentInstrument);
        // Check that the current isntrument is changed
        verify(session).setInstrument(loadInstrument);
        // Check that he UI is notified for session changes
        verify(uiManager).sessionModified();
    }

    @Test
    public void loadInstrument_currentInstrumentModifiedSaveIgnore() throws Exception {
        //given
        given(sessionManager.isInstrumentPersisted(currentInstrument)).willReturn(false);
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_INSTRUMENT_MODIFIED"), "instrument");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("IGNORE_OPTION"));
        ComponentInfo loadInfo = new ComponentInfo("loadInstrument", "loadDescription");
        Instrument loadInstrument = mock(Instrument.class);
        given(loadInstrument.getInfo()).willReturn(loadInfo);
        given(sessionManager.loadInstrument(loadInfo)).willReturn(loadInstrument);
        
        //when
        InstrumentListenerImpl instrumentListener = new InstrumentListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        instrumentListener.loadInstrument(loadInfo);
        
        //then
        verify(sessionManager, never()).saveInstrument(currentInstrument);
        // Check that the current isntrument is changed
        verify(session).setInstrument(loadInstrument);
        // Check that he UI is notified for session changes
        verify(uiManager).sessionModified();
    }

    @Test
    public void loadInstrument_NameNotFound() throws Exception {
        //given
        given(sessionManager.isInstrumentPersisted(currentInstrument)).willReturn(true);
        ComponentInfo loadInfo = new ComponentInfo("loadInstrument", "loadDescription");
        given(sessionManager.loadInstrument(loadInfo)).willThrow(new NameNotFoundException());
        
        //when
        InstrumentListenerImpl instrumentListener = new InstrumentListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        instrumentListener.loadInstrument(loadInfo);
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("LOAD_INSTRUMENT_NOT_FOUND"), "loadInstrument"));
        verify(sessionManager, never()).saveInstrument(currentInstrument);
        // Check that the current isntrument is not changed
        verify(session, never()).setInstrument(any(Instrument.class));
        // Check that he UI is not notified for session changes
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void loadInstrument_LoadFailedDeviceError() throws Exception {
        //given
        given(sessionManager.isInstrumentPersisted(currentInstrument)).willReturn(true);
        ComponentInfo loadInfo = new ComponentInfo("loadInstrument", "loadDescription");
        given(sessionManager.loadInstrument(loadInfo)).willThrow(new PersistenceDeviceException());
        
        //when
        InstrumentListenerImpl instrumentListener = new InstrumentListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        instrumentListener.loadInstrument(loadInfo);
        
        //then
        verify(messenger).error(bundle.getString("LOAD_INSTRUMENT_DEVICE_ERROR"));
        verify(sessionManager, never()).saveInstrument(currentInstrument);
        // Check that the current isntrument is not changed
        verify(session, never()).setInstrument(any(Instrument.class));
        // Check that he UI is not notified for session changes
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void reloadCurrentInstrument_NotModified() throws Exception {
        //given
        given(sessionManager.isInstrumentInDatabase(currentInstrument)).willReturn(true);
        given(sessionManager.isInstrumentPersisted(currentInstrument)).willReturn(true);
        
        //when
        InstrumentListenerImpl instrumentListener = new InstrumentListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        instrumentListener.reloadCurrentInstrument();
        
        //then
        verify(sessionManager, never()).loadInstrument(any(ComponentInfo.class));
        verify(session, never()).setInstrument(any(Instrument.class));
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void reloadCurrentInstrument_AbortedByUser() throws Exception {
        //given
        given(sessionManager.isInstrumentInDatabase(currentInstrument)).willReturn(true);
        given(sessionManager.isInstrumentPersisted(currentInstrument)).willReturn(false);
        String confirmMessage = bundle.getString("CONFIRM_INSTRUMENT_RELOAD");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("NO_OPTION"));
        
        //when
        InstrumentListenerImpl instrumentListener = new InstrumentListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        instrumentListener.reloadCurrentInstrument();
        
        //then
        verify(sessionManager, never()).loadInstrument(any(ComponentInfo.class));
        verify(session, never()).setInstrument(any(Instrument.class));
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void reloadCurrentInstrument_NameNotFound() throws Exception {
        //given
        given(sessionManager.isInstrumentInDatabase(currentInstrument)).willReturn(true);
        given(sessionManager.isInstrumentPersisted(currentInstrument)).willReturn(false);
        String confirmMessage = bundle.getString("CONFIRM_INSTRUMENT_RELOAD");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("YES_OPTION"));
        given(sessionManager.loadInstrument(currentInstrument.getInfo())).willThrow(new NameNotFoundException());
        
        //when
        InstrumentListenerImpl instrumentListener = new InstrumentListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        instrumentListener.reloadCurrentInstrument();
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("LOAD_INSTRUMENT_NOT_FOUND"), "instrument"));
        verify(session, never()).setInstrument(any(Instrument.class));
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void reloadCurrentInstrument_DeviceError() throws Exception {
        //given
        given(sessionManager.isInstrumentInDatabase(currentInstrument)).willReturn(true);
        given(sessionManager.isInstrumentPersisted(currentInstrument)).willReturn(false);
        String confirmMessage = bundle.getString("CONFIRM_INSTRUMENT_RELOAD");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("YES_OPTION"));
        given(sessionManager.loadInstrument(currentInstrument.getInfo())).willThrow(new PersistenceDeviceException());
        
        //when
        InstrumentListenerImpl instrumentListener = new InstrumentListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        instrumentListener.reloadCurrentInstrument();
        
        //then
        verify(messenger).error(bundle.getString("LOAD_INSTRUMENT_DEVICE_ERROR"));
        verify(session, never()).setInstrument(any(Instrument.class));
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void reloadCurrentInstrument_Success() throws Exception {
        //given
        given(sessionManager.isInstrumentInDatabase(currentInstrument)).willReturn(true);
        given(sessionManager.isInstrumentPersisted(currentInstrument)).willReturn(false);
        String confirmMessage = bundle.getString("CONFIRM_INSTRUMENT_RELOAD");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("YES_OPTION"));
        Instrument reloadedInstrument = mock(Instrument.class);
        given(sessionManager.loadInstrument(currentInstrument.getInfo())).willReturn(reloadedInstrument);
        
        //when
        InstrumentListenerImpl instrumentListener = new InstrumentListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        instrumentListener.reloadCurrentInstrument();
        
        //then
        verify(session).setInstrument(reloadedInstrument);
        verify(uiManager).sessionModified();
    }

    @Test
    public void saveInstrumentAs_NameExists() throws Exception {
        //given
        ComponentInfo newInfo = new ComponentInfo("newInstrument", "newDescription");
        given(sessionManager.saveInstrumentAs(currentInstrument, newInfo)).willThrow(new NameExistsException());
        
        //when
        InstrumentListenerImpl instrumentListener = new InstrumentListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        instrumentListener.saveCurrentInstrumentAs(newInfo);
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("CREATE_INSTRUMENT_NAME_EXISTS"), "newInstrument"));
        verify(session, never()).setInstrument(any(Instrument.class));
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void saveInstrumentAs_DeviceError() throws Exception {
        //given
        ComponentInfo newInfo = new ComponentInfo("newInstrument", "newDescription");
        given(sessionManager.saveInstrumentAs(currentInstrument, newInfo)).willThrow(new PersistenceDeviceException());
        
        //when
        InstrumentListenerImpl instrumentListener = new InstrumentListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        instrumentListener.saveCurrentInstrumentAs(newInfo);
        
        //then
        verify(messenger).error(bundle.getString("CREATE_INSTRUMENT_DEVICE_ERROR"));
        verify(session, never()).setInstrument(any(Instrument.class));
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void saveInstrumentAs_Success() throws Exception {
        //given
        ComponentInfo newInfo = new ComponentInfo("newInstrument", "newDescription");
        Instrument newInstrument = mock(Instrument.class);
        given(newInstrument.getInfo()).willReturn(newInfo);
        given(sessionManager.saveInstrumentAs(currentInstrument, newInfo)).willReturn(newInstrument);
        
        //when
        InstrumentListenerImpl instrumentListener = new InstrumentListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        instrumentListener.saveCurrentInstrumentAs(newInfo);
        
        //then
        verify(session).setInstrument(newInstrument);
        verify(uiManager).sessionModified();
    }
    
    @Test
    public void deleteInstrument_Current() throws Exception {
        //when
        InstrumentListenerImpl instrumentListener = new InstrumentListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        instrumentListener.deleteInstrument(currentInstrument.getInfo());
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("DELETE_INSTRUMENT_CURRENT"), currentInstrument.getInfo()));
        verify(sessionManager, never()).deleteInstrument(any(ComponentInfo.class));
    }

    @Test
    public void deleteInstrument_CanceledByUser() throws Exception {
        //given
        String confirmMessage =  MessageFormat.format(bundle.getString("DELETE_INSTRUMENT_CONFIRM"), "Test");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("CANCEL_OPTION"));
        ComponentInfo componentInfo = new ComponentInfo("Test", "Description");
        
        //when
        InstrumentListenerImpl instrumentListener = new InstrumentListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        instrumentListener.deleteInstrument(componentInfo);
        
        //then
        verify(sessionManager, never()).deleteInstrument(any(ComponentInfo.class));
    }

    @Test
    public void deleteInstrument_Success() throws Exception {
        //given
        String confirmMessage =  MessageFormat.format(bundle.getString("DELETE_INSTRUMENT_CONFIRM"), "Test");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("DELETE_OPTION"));
        ComponentInfo componentInfo = new ComponentInfo("Test", "Description");
        
        //when
        InstrumentListenerImpl instrumentListener = new InstrumentListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        instrumentListener.deleteInstrument(componentInfo);
        
        //then
        verify(sessionManager).deleteInstrument(componentInfo);
        verify(messenger).info(MessageFormat.format(bundle.getString("DELETE_INSTRUMENT_SUCCESS"), "Test"));
    }

    @Test
    public void deleteInstrument_InUse() throws Exception {
        //given
        String confirmMessage =  MessageFormat.format(bundle.getString("DELETE_INSTRUMENT_CONFIRM"), "Test");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("DELETE_OPTION"));
        ComponentInfo componentInfo = new ComponentInfo("Test", "Description");
        List<String> inUseList = new ArrayList<String>();
        inUseList.add("Session");
        willThrow(new InUseException(inUseList)).given(sessionManager).deleteInstrument(componentInfo);
        
        //when
        InstrumentListenerImpl instrumentListener = new InstrumentListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        instrumentListener.deleteInstrument(componentInfo);
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("DELETE_INSTRUMENT_IN_USE"), "Test", "Session"));
    }

    @Test
    public void deleteInstrument_NameNotFound() throws Exception {
        //given
        String confirmMessage =  MessageFormat.format(bundle.getString("DELETE_INSTRUMENT_CONFIRM"), "Test");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("DELETE_OPTION"));
        ComponentInfo componentInfo = new ComponentInfo("Test", "Description");
        willThrow(new NameNotFoundException()).given(sessionManager).deleteInstrument(componentInfo);
        
        //when
        InstrumentListenerImpl instrumentListener = new InstrumentListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        instrumentListener.deleteInstrument(componentInfo);
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("DELETE_INSTRUMENT_NOT_FOUND"), "Test"));
    }

    @Test
    public void deleteInstrument_DeviceError() throws Exception {
        //given
        String confirmMessage =  MessageFormat.format(bundle.getString("DELETE_INSTRUMENT_CONFIRM"), "Test");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("DELETE_OPTION"));
        ComponentInfo componentInfo = new ComponentInfo("Test", "Description");
        willThrow(new PersistenceDeviceException()).given(sessionManager).deleteInstrument(componentInfo);
        
        //when
        InstrumentListenerImpl instrumentListener = new InstrumentListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        instrumentListener.deleteInstrument(componentInfo);
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("DELETE_INSTRUMENT_DEVICE_ERROR"), "Test"));
    }
}