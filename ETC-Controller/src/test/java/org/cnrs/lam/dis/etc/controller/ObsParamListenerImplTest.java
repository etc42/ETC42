/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.controller;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import org.cnrs.lam.dis.etc.datamodel.ComponentInfo;
import org.cnrs.lam.dis.etc.datamodel.ObsParam;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.cnrs.lam.dis.etc.persistence.InUseException;
import org.cnrs.lam.dis.etc.persistence.NameExistsException;
import org.cnrs.lam.dis.etc.persistence.NameNotFoundException;
import org.cnrs.lam.dis.etc.persistence.PersistenceDeviceException;
import org.cnrs.lam.dis.etc.persistence.SessionManager;
import org.cnrs.lam.dis.etc.ui.Messenger;
import org.cnrs.lam.dis.etc.ui.UIManager;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.mockito.Mock;
import static org.mockito.BDDMockito.*;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class ObsParamListenerImplTest {

    @Mock SessionManager sessionManager;
    @Mock Messenger messenger;
    @Mock UIManager uiManager;
    @Mock ObsParam currentObsParam;
    @Mock Session session;
    ResourceBundle bundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/controller/messages");

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        when(currentObsParam.getInfo()).thenReturn(new ComponentInfo("obsParam", "ObsParamDescription"));
        when(session.getObsParam()).thenReturn(currentObsParam);
        CurrentSessionContainer.setCurrentSession(session);
    }

    @Test
    public void saveCurrentObsParam_AlreadyPersisted() throws Exception {
        //given
        given(sessionManager.isObsParamPersisted(currentObsParam)).willReturn(true);
        
        //when
        ObsParamListenerImpl obsParamListener = new ObsParamListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        obsParamListener.saveCurrentObsParam();
        
        //then
        verify(sessionManager, never()).saveObsParam(currentObsParam);
        verify(uiManager, never()).sessionModified();
    }

    /**
     * This test checks that when there is a persistence device reading error
     * for which the check if the current currentObsParam is already persisted fails,
     * the system treats the current currentObsParam as modified and it proceeds with
     * saving it.
     */
    @Test
    public void saveCurrentObsParam_ErrorWhileCheckingIfAlreadyPersisted() throws Exception {
        //given
        given(sessionManager.isObsParamPersisted(currentObsParam)).willThrow(new PersistenceDeviceException());
        
        //when
        ObsParamListenerImpl obsParamListener = new ObsParamListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        obsParamListener.saveCurrentObsParam();
        
        //then
        verify(sessionManager).saveObsParam(currentObsParam);
    }

    @Test
    public void saveCurrentObsParam_ErrorWhilePersisting() throws Exception {
        //given
        given(sessionManager.isObsParamPersisted(currentObsParam)).willReturn(false);
        willThrow(new PersistenceDeviceException()).given(sessionManager).saveObsParam(currentObsParam);
        
        //when
        ObsParamListenerImpl obsParamListener = new ObsParamListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        obsParamListener.saveCurrentObsParam();
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("SAVE_OBS_PARAM_DEVICE_ERROR"), "obsParam"));
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void saveCurrentObsParam_Success() throws Exception {
        //given
        given(sessionManager.isObsParamPersisted(currentObsParam)).willReturn(false);
        
        //when
        ObsParamListenerImpl obsParamListener = new ObsParamListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        obsParamListener.saveCurrentObsParam();
        
        //then
        verify(sessionManager).saveObsParam(currentObsParam);
        verify(uiManager).sessionModified();
    }

    @Test
    public void createNewObsParam_CurrentObsParamModifiedCanceled() throws Exception {
        //given
        given(sessionManager.isObsParamPersisted(currentObsParam)).willReturn(false);
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_OBS_PARAM_MODIFIED"), "obsParam");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("CANCEL_OPTION"));
        
        //when
        ObsParamListenerImpl obsParamListener = new ObsParamListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        ComponentInfo newInfo = new ComponentInfo("newObsParam", "newDescription");
        obsParamListener.createNewObsParam(newInfo);
        
        //then
        // Check that the current currentObsParam wasn't saved
        verify(sessionManager, never()).saveObsParam(any(ObsParam.class));
        // Check that the current isntrument is not changed
        verify(session, never()).setObsParam(any(ObsParam.class));
        // Check that he UI is not notified for session changes
        verify(uiManager, never()).sessionModified();
    }

    /**
     * This checks that if the isObsParamPersisted() method fails with an exception
     * we have the same behavior like the current currentObsParam is modified.
     */
    @Test
    public void createNewObsParam_CurrentObsParamModifiedCheckError() throws Exception {
        //given
        given(sessionManager.isObsParamPersisted(currentObsParam)).willThrow(new PersistenceDeviceException());
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_OBS_PARAM_MODIFIED"), "obsParam");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("CANCEL_OPTION"));
        
        //when
        ObsParamListenerImpl obsParamListener = new ObsParamListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        ComponentInfo newInfo = new ComponentInfo("newObsParam", "newDescription");
        obsParamListener.createNewObsParam(newInfo);
        
        //then
        // Check that the current currentObsParam wasn't saved
        verify(sessionManager, never()).saveObsParam(any(ObsParam.class));
        // Check that the current isntrument is not changed
        verify(session, never()).setObsParam(any(ObsParam.class));
        // Check that he UI is not notified for session changes
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void createNewObsParam_currentObsParamModifiedSaveError() throws Exception {
        //given
        given(sessionManager.isObsParamPersisted(currentObsParam)).willReturn(false);
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_OBS_PARAM_MODIFIED"), "obsParam");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("SAVE_OPTION"));
        willThrow(new PersistenceDeviceException()).given(sessionManager).saveObsParam(currentObsParam);
        
        //when
        ObsParamListenerImpl obsParamListener = new ObsParamListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        ComponentInfo newInfo = new ComponentInfo("newObsParam", "newDescription");
        obsParamListener.createNewObsParam(newInfo);
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("SAVE_OBS_PARAM_DEVICE_ERROR"), "obsParam"));
        // Check that the current isntrument is not changed
        verify(session, never()).setObsParam(any(ObsParam.class));
        // Check that he UI is not notified for session changes
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void createNewObsParam_currentObsParamModifiedSaveSuccess() throws Exception {
        //given
        given(sessionManager.isObsParamPersisted(currentObsParam)).willReturn(false);
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_OBS_PARAM_MODIFIED"), "obsParam");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("SAVE_OPTION"));
        ComponentInfo newInfo = new ComponentInfo("newObsParam", "newDescription");
        ObsParam newObsParam = mock(ObsParam.class);
        given(newObsParam.getInfo()).willReturn(newInfo);
        given(sessionManager.createNewObsParam(newInfo)).willReturn(newObsParam);
        
        //when
        ObsParamListenerImpl obsParamListener = new ObsParamListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        obsParamListener.createNewObsParam(newInfo);
        
        //then
        // Check that the current currentObsParam was saved
        verify(sessionManager).saveObsParam(currentObsParam);
        // Check that the current isntrument is changed
        verify(session).setObsParam(newObsParam);
        // Check that he UI is notified for session changes
        verify(uiManager).sessionModified();
    }

    @Test
    public void createNewObsParam_currentObsParamModifiedSaveIgnore() throws Exception {
        //given
        given(sessionManager.isObsParamPersisted(currentObsParam)).willReturn(false);
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_OBS_PARAM_MODIFIED"), "obsParam");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("IGNORE_OPTION"));
        ComponentInfo newInfo = new ComponentInfo("newObsParam", "newDescription");
        ObsParam newObsParam = mock(ObsParam.class);
        given(newObsParam.getInfo()).willReturn(newInfo);
        given(sessionManager.createNewObsParam(newInfo)).willReturn(newObsParam);
        
        //when
        ObsParamListenerImpl obsParamListener = new ObsParamListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        obsParamListener.createNewObsParam(newInfo);
        
        //then
        // Check that the current currentObsParam was not saved
        verify(sessionManager, never()).saveObsParam(currentObsParam);
        // Check that the current isntrument is changed
        verify(session).setObsParam(newObsParam);
        // Check that he UI is notified for session changes
        verify(uiManager).sessionModified();
    }

    @Test
    public void createNewObsParam_NameExists() throws Exception {
        //given
        given(sessionManager.isObsParamPersisted(currentObsParam)).willReturn(false);
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_OBS_PARAM_MODIFIED"), "obsParam");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("IGNORE_OPTION"));
        ComponentInfo newInfo = new ComponentInfo("newObsParam", "newDescription");
        given(sessionManager.createNewObsParam(newInfo)).willThrow(new NameExistsException());
        
        //when
        ObsParamListenerImpl obsParamListener = new ObsParamListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        obsParamListener.createNewObsParam(newInfo);
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("CREATE_OBS_PARAM_NAME_EXISTS"), "newObsParam"));
        // Check that the current currentObsParam was not saved
        verify(sessionManager, never()).saveObsParam(currentObsParam);
        // Check that the current isntrument is not changed
        verify(session, never()).setObsParam(any(ObsParam.class));
        // Check that he UI is notified for session changes
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void createNewObsParam_CreateFailedDeviceError() throws Exception {
        //given
        given(sessionManager.isObsParamPersisted(currentObsParam)).willReturn(false);
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_OBS_PARAM_MODIFIED"), "obsParam");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("IGNORE_OPTION"));
        ComponentInfo newInfo = new ComponentInfo("newObsParam", "newDescription");
        given(sessionManager.createNewObsParam(newInfo)).willThrow(new PersistenceDeviceException());
        
        //when
        ObsParamListenerImpl obsParamListener = new ObsParamListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        obsParamListener.createNewObsParam(newInfo);
        
        //then
        verify(messenger).error(bundle.getString("CREATE_OBS_PARAM_DEVICE_ERROR"));
        // Check that the current currentObsParam was not saved
        verify(sessionManager, never()).saveObsParam(currentObsParam);
        // Check that the current isntrument is not changed
        verify(session, never()).setObsParam(any(ObsParam.class));
        // Check that he UI is notified for session changes
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void loadObsParam_IsCurrent() throws Exception {
        //when
        ObsParamListenerImpl obsParamListener = new ObsParamListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        ComponentInfo loadInfo = new ComponentInfo("obsParam", "description");
        obsParamListener.loadObsParam(loadInfo);

        //then
        // Check that the current isntrument is not changed
        verify(session, never()).setObsParam(any(ObsParam.class));
        // Check that he UI is not notified for session changes
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void loadObsParam_CurrentObsParamModifiedCanceled() throws Exception {
        //given
        given(sessionManager.isObsParamPersisted(currentObsParam)).willReturn(false);
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_OBS_PARAM_MODIFIED"), "obsParam");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("CANCEL_OPTION"));
        
        //when
        ObsParamListenerImpl obsParamListener = new ObsParamListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        ComponentInfo loadInfo = new ComponentInfo("loadObsParam", "loadDescription");
        obsParamListener.loadObsParam(loadInfo);
        
        //then
        verify(sessionManager, never()).saveObsParam(currentObsParam);
        // Check that the current isntrument is not changed
        verify(session, never()).setObsParam(any(ObsParam.class));
        // Check that he UI is not notified for session changes
        verify(uiManager, never()).sessionModified();
    }

    /**
     * This checks that if the isObsParamPersisted() method fails with an exception
     * we have the same behavior like the current currentObsParam is modified.
     */
    @Test
    public void loadObsParam_CurrentObsParamModifiedCheckError() throws Exception {
        //given
        given(sessionManager.isObsParamPersisted(currentObsParam)).willThrow(new PersistenceDeviceException());
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_OBS_PARAM_MODIFIED"), "obsParam");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("CANCEL_OPTION"));
        
        //when
        ObsParamListenerImpl obsParamListener = new ObsParamListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        ComponentInfo loadInfo = new ComponentInfo("loadObsParam", "loadDescription");
        obsParamListener.loadObsParam(loadInfo);
        
        //then
        verify(sessionManager, never()).saveObsParam(currentObsParam);
        // Check that the current isntrument is not changed
        verify(session, never()).setObsParam(any(ObsParam.class));
        // Check that he UI is not notified for session changes
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void loadObsParam_CurrentObsParamModifiedSaveError() throws Exception {
        //given
        given(sessionManager.isObsParamPersisted(currentObsParam)).willReturn(false);
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_OBS_PARAM_MODIFIED"), "obsParam");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("SAVE_OPTION"));
        willThrow(new PersistenceDeviceException()).given(sessionManager).saveObsParam(currentObsParam);
        
        //when
        ObsParamListenerImpl obsParamListener = new ObsParamListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        ComponentInfo loadInfo = new ComponentInfo("loadObsParam", "loadDescription");
        obsParamListener.loadObsParam(loadInfo);
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("SAVE_OBS_PARAM_DEVICE_ERROR"), "obsParam"));
        // Check that the current isntrument is not changed
        verify(session, never()).setObsParam(any(ObsParam.class));
        // Check that he UI is not notified for session changes
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void loadObsParam_currentObsParamModifiedSaveSuccess() throws Exception {
        //given
        given(sessionManager.isObsParamPersisted(currentObsParam)).willReturn(false);
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_OBS_PARAM_MODIFIED"), "obsParam");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("SAVE_OPTION"));
        ComponentInfo loadInfo = new ComponentInfo("loadObsParam", "loadDescription");
        ObsParam loadObsParam = mock(ObsParam.class);
        given(loadObsParam.getInfo()).willReturn(loadInfo);
        given(sessionManager.loadObsParam(loadInfo)).willReturn(loadObsParam);
        
        //when
        ObsParamListenerImpl obsParamListener = new ObsParamListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        obsParamListener.loadObsParam(loadInfo);
        
        //then
        verify(sessionManager).saveObsParam(currentObsParam);
        // Check that the current isntrument is changed
        verify(session).setObsParam(loadObsParam);
        // Check that he UI is notified for session changes
        verify(uiManager).sessionModified();
    }

    @Test
    public void loadObsParam_currentObsParamModifiedSaveIgnore() throws Exception {
        //given
        given(sessionManager.isObsParamPersisted(currentObsParam)).willReturn(false);
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_OBS_PARAM_MODIFIED"), "obsParam");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("IGNORE_OPTION"));
        ComponentInfo loadInfo = new ComponentInfo("loadObsParam", "loadDescription");
        ObsParam loadObsParam = mock(ObsParam.class);
        given(loadObsParam.getInfo()).willReturn(loadInfo);
        given(sessionManager.loadObsParam(loadInfo)).willReturn(loadObsParam);
        
        //when
        ObsParamListenerImpl obsParamListener = new ObsParamListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        obsParamListener.loadObsParam(loadInfo);
        
        //then
        verify(sessionManager, never()).saveObsParam(currentObsParam);
        // Check that the current isntrument is changed
        verify(session).setObsParam(loadObsParam);
        // Check that he UI is notified for session changes
        verify(uiManager).sessionModified();
    }

    @Test
    public void loadObsParam_NameNotFound() throws Exception {
        //given
        given(sessionManager.isObsParamPersisted(currentObsParam)).willReturn(true);
        ComponentInfo loadInfo = new ComponentInfo("loadObsParam", "loadDescription");
        given(sessionManager.loadObsParam(loadInfo)).willThrow(new NameNotFoundException());
        
        //when
        ObsParamListenerImpl obsParamListener = new ObsParamListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        obsParamListener.loadObsParam(loadInfo);
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("LOAD_OBS_PARAM_NOT_FOUND"), "loadObsParam"));
        verify(sessionManager, never()).saveObsParam(currentObsParam);
        // Check that the current isntrument is not changed
        verify(session, never()).setObsParam(any(ObsParam.class));
        // Check that he UI is not notified for session changes
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void loadObsParam_LoadFailedDeviceError() throws Exception {
        //given
        given(sessionManager.isObsParamPersisted(currentObsParam)).willReturn(true);
        ComponentInfo loadInfo = new ComponentInfo("loadObsParam", "loadDescription");
        given(sessionManager.loadObsParam(loadInfo)).willThrow(new PersistenceDeviceException());
        
        //when
        ObsParamListenerImpl obsParamListener = new ObsParamListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        obsParamListener.loadObsParam(loadInfo);
        
        //then
        verify(messenger).error(bundle.getString("LOAD_OBS_PARAM_DEVICE_ERROR"));
        verify(sessionManager, never()).saveObsParam(currentObsParam);
        // Check that the current isntrument is not changed
        verify(session, never()).setObsParam(any(ObsParam.class));
        // Check that he UI is not notified for session changes
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void reloadCurrentObsParam_NotModified() throws Exception {
        //given
        given(sessionManager.isObsParamInDatabase(currentObsParam)).willReturn(true);
        given(sessionManager.isObsParamPersisted(currentObsParam)).willReturn(true);
        
        //when
        ObsParamListenerImpl obsParamListener = new ObsParamListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        obsParamListener.reloadCurrentObsParam();
        
        //then
        verify(sessionManager, never()).loadObsParam(any(ComponentInfo.class));
        verify(session, never()).setObsParam(any(ObsParam.class));
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void reloadCurrentObsParam_AbortedByUser() throws Exception {
        //given
        given(sessionManager.isObsParamInDatabase(currentObsParam)).willReturn(true);
        given(sessionManager.isObsParamPersisted(currentObsParam)).willReturn(false);
        String confirmMessage = bundle.getString("CONFIRM_OBS_PARAM_RELOAD");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("NO_OPTION"));
        
        //when
        ObsParamListenerImpl obsParamListener = new ObsParamListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        obsParamListener.reloadCurrentObsParam();
        
        //then
        verify(sessionManager, never()).loadObsParam(any(ComponentInfo.class));
        verify(session, never()).setObsParam(any(ObsParam.class));
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void reloadCurrentObsParam_NameNotFound() throws Exception {
        //given
        given(sessionManager.isObsParamInDatabase(currentObsParam)).willReturn(true);
        given(sessionManager.isObsParamPersisted(currentObsParam)).willReturn(false);
        String confirmMessage = bundle.getString("CONFIRM_OBS_PARAM_RELOAD");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("YES_OPTION"));
        given(sessionManager.loadObsParam(currentObsParam.getInfo())).willThrow(new NameNotFoundException());
        
        //when
        ObsParamListenerImpl obsParamListener = new ObsParamListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        obsParamListener.reloadCurrentObsParam();
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("LOAD_OBS_PARAM_NOT_FOUND"), "obsParam"));
        verify(session, never()).setObsParam(any(ObsParam.class));
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void reloadCurrentObsParam_DeviceError() throws Exception {
        //given
        given(sessionManager.isObsParamInDatabase(currentObsParam)).willReturn(true);
        given(sessionManager.isObsParamPersisted(currentObsParam)).willReturn(false);
        String confirmMessage = bundle.getString("CONFIRM_OBS_PARAM_RELOAD");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("YES_OPTION"));
        given(sessionManager.loadObsParam(currentObsParam.getInfo())).willThrow(new PersistenceDeviceException());
        
        //when
        ObsParamListenerImpl obsParamListener = new ObsParamListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        obsParamListener.reloadCurrentObsParam();
        
        //then
        verify(messenger).error(bundle.getString("LOAD_OBS_PARAM_DEVICE_ERROR"));
        verify(session, never()).setObsParam(any(ObsParam.class));
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void reloadCurrentObsParam_Success() throws Exception {
        //given
        given(sessionManager.isObsParamInDatabase(currentObsParam)).willReturn(true);
        given(sessionManager.isObsParamPersisted(currentObsParam)).willReturn(false);
        String confirmMessage = bundle.getString("CONFIRM_OBS_PARAM_RELOAD");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("YES_OPTION"));
        ObsParam reloadedObsParam = mock(ObsParam.class);
        given(sessionManager.loadObsParam(currentObsParam.getInfo())).willReturn(reloadedObsParam);
        
        //when
        ObsParamListenerImpl obsParamListener = new ObsParamListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        obsParamListener.reloadCurrentObsParam();
        
        //then
        verify(session).setObsParam(reloadedObsParam);
        verify(uiManager).sessionModified();
    }

    @Test
    public void saveObsParamAs_NameExists() throws Exception {
        //given
        ComponentInfo newInfo = new ComponentInfo("newObsParam", "newDescription");
        given(sessionManager.saveObsParamAs(currentObsParam, newInfo)).willThrow(new NameExistsException());
        
        //when
        ObsParamListenerImpl obsParamListener = new ObsParamListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        obsParamListener.saveCurrentObsParamAs(newInfo);
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("CREATE_OBS_PARAM_NAME_EXISTS"), "newObsParam"));
        verify(session, never()).setObsParam(any(ObsParam.class));
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void saveObsParamAs_DeviceError() throws Exception {
        //given
        ComponentInfo newInfo = new ComponentInfo("newObsParam", "newDescription");
        given(sessionManager.saveObsParamAs(currentObsParam, newInfo)).willThrow(new PersistenceDeviceException());
        
        //when
        ObsParamListenerImpl obsParamListener = new ObsParamListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        obsParamListener.saveCurrentObsParamAs(newInfo);
        
        //then
        verify(messenger).error(bundle.getString("CREATE_OBS_PARAM_DEVICE_ERROR"));
        verify(session, never()).setObsParam(any(ObsParam.class));
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void saveObsParamAs_Success() throws Exception {
        //given
        ComponentInfo newInfo = new ComponentInfo("newObsParam", "newDescription");
        ObsParam newObsParam = mock(ObsParam.class);
        given(newObsParam.getInfo()).willReturn(newInfo);
        given(sessionManager.saveObsParamAs(currentObsParam, newInfo)).willReturn(newObsParam);
        
        //when
        ObsParamListenerImpl obsParamListener = new ObsParamListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        obsParamListener.saveCurrentObsParamAs(newInfo);
        
        //then
        verify(session).setObsParam(newObsParam);
        verify(uiManager).sessionModified();
    }
    
    @Test
    public void deleteObsParam_Current() throws Exception {
        //when
        ObsParamListenerImpl obsParamListener = new ObsParamListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        obsParamListener.deleteObsParam(currentObsParam.getInfo());
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("DELETE_OBS_PARAM_CURRENT"), currentObsParam.getInfo()));
        verify(sessionManager, never()).deleteObsParam(any(ComponentInfo.class));
    }

    @Test
    public void deleteObsParam_CanceledByUser() throws Exception {
        //given
        String confirmMessage =  MessageFormat.format(bundle.getString("DELETE_OBS_PARAM_CONFIRM"), "Test");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("CANCEL_OPTION"));
        ComponentInfo componentInfo = new ComponentInfo("Test", "Description");
        
        //when
        ObsParamListenerImpl obsParamListener = new ObsParamListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        obsParamListener.deleteObsParam(componentInfo);
        
        //then
        verify(sessionManager, never()).deleteObsParam(any(ComponentInfo.class));
    }

    @Test
    public void deleteObsParam_Success() throws Exception {
        //given
        String confirmMessage =  MessageFormat.format(bundle.getString("DELETE_OBS_PARAM_CONFIRM"), "Test");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("DELETE_OPTION"));
        ComponentInfo componentInfo = new ComponentInfo("Test", "Description");
        
        //when
        ObsParamListenerImpl obsParamListener = new ObsParamListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        obsParamListener.deleteObsParam(componentInfo);
        
        //then
        verify(sessionManager).deleteObsParam(componentInfo);
        verify(messenger).info(MessageFormat.format(bundle.getString("DELETE_OBS_PARAM_SUCCESS"), "Test"));
    }

    @Test
    public void deleteObsParam_InUse() throws Exception {
        //given
        String confirmMessage =  MessageFormat.format(bundle.getString("DELETE_OBS_PARAM_CONFIRM"), "Test");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("DELETE_OPTION"));
        ComponentInfo componentInfo = new ComponentInfo("Test", "Description");
        List<String> inUseList = new ArrayList<String>();
        inUseList.add("Session");
        willThrow(new InUseException(inUseList)).given(sessionManager).deleteObsParam(componentInfo);
        
        //when
        ObsParamListenerImpl obsParamListener = new ObsParamListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        obsParamListener.deleteObsParam(componentInfo);
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("DELETE_OBS_PARAM_IN_USE"), "Test", "Session"));
    }

    @Test
    public void deleteObsParam_NameNotFound() throws Exception {
        //given
        String confirmMessage =  MessageFormat.format(bundle.getString("DELETE_OBS_PARAM_CONFIRM"), "Test");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("DELETE_OPTION"));
        ComponentInfo componentInfo = new ComponentInfo("Test", "Description");
        willThrow(new NameNotFoundException()).given(sessionManager).deleteObsParam(componentInfo);
        
        //when
        ObsParamListenerImpl obsParamListener = new ObsParamListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        obsParamListener.deleteObsParam(componentInfo);
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("DELETE_OBS_PARAM_NOT_FOUND"), "Test"));
    }

    @Test
    public void deleteObsParam_DeviceError() throws Exception {
        //given
        String confirmMessage =  MessageFormat.format(bundle.getString("DELETE_OBS_PARAM_CONFIRM"), "Test");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("DELETE_OPTION"));
        ComponentInfo componentInfo = new ComponentInfo("Test", "Description");
        willThrow(new PersistenceDeviceException()).given(sessionManager).deleteObsParam(componentInfo);
        
        //when
        ObsParamListenerImpl obsParamListener = new ObsParamListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        obsParamListener.deleteObsParam(componentInfo);
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("DELETE_OBS_PARAM_DEVICE_ERROR"), "Test"));
    }
}