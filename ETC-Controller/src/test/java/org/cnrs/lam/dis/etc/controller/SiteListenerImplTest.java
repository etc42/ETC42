/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.controller;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import org.cnrs.lam.dis.etc.datamodel.ComponentInfo;
import org.cnrs.lam.dis.etc.datamodel.Site;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.cnrs.lam.dis.etc.persistence.InUseException;
import org.cnrs.lam.dis.etc.persistence.NameExistsException;
import org.cnrs.lam.dis.etc.persistence.NameNotFoundException;
import org.cnrs.lam.dis.etc.persistence.PersistenceDeviceException;
import org.cnrs.lam.dis.etc.persistence.SessionManager;
import org.cnrs.lam.dis.etc.ui.Messenger;
import org.cnrs.lam.dis.etc.ui.UIManager;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.BDDMockito.*;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class SiteListenerImplTest {

    @Mock SessionManager sessionManager;
    @Mock Messenger messenger;
    @Mock UIManager uiManager;
    @Mock Site currentSite;
    @Mock Session session;
    ResourceBundle bundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/controller/messages");

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        when(currentSite.getInfo()).thenReturn(new ComponentInfo("site", "SiteDescription"));
        when(session.getSite()).thenReturn(currentSite);
        CurrentSessionContainer.setCurrentSession(session);
    }

    @Test
    public void saveCurrentSite_AlreadyPersisted() throws Exception {
        //given
        given(sessionManager.isSitePersisted(currentSite)).willReturn(true);
        
        //when
        SiteListenerImpl siteListener = new SiteListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        siteListener.saveCurrentSite();
        
        //then
        verify(sessionManager, never()).saveSite(currentSite);
        verify(uiManager, never()).sessionModified();
    }

    /**
     * This test checks that when there is a persistence device reading error
     * for which the check if the current currentSite is already persisted fails,
     * the system treats the current currentSite as modified and it proceeds with
     * saving it.
     */
    @Test
    public void saveCurrentSite_ErrorWhileCheckingIfAlreadyPersisted() throws Exception {
        //given
        given(sessionManager.isSitePersisted(currentSite)).willThrow(new PersistenceDeviceException());
        
        //when
        SiteListenerImpl siteListener = new SiteListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        siteListener.saveCurrentSite();
        
        //then
        verify(sessionManager).saveSite(currentSite);
    }

    @Test
    public void saveCurrentSite_ErrorWhilePersisting() throws Exception {
        //given
        given(sessionManager.isSitePersisted(currentSite)).willReturn(false);
        willThrow(new PersistenceDeviceException()).given(sessionManager).saveSite(currentSite);
        
        //when
        SiteListenerImpl siteListener = new SiteListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        siteListener.saveCurrentSite();
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("SAVE_SITE_DEVICE_ERROR"), "site"));
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void saveCurrentSite_Success() throws Exception {
        //given
        given(sessionManager.isSitePersisted(currentSite)).willReturn(false);
        
        //when
        SiteListenerImpl siteListener = new SiteListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        siteListener.saveCurrentSite();
        
        //then
        verify(sessionManager).saveSite(currentSite);
        verify(uiManager).sessionModified();
    }

    @Test
    public void createNewSite_CurrentSiteModifiedCanceled() throws Exception {
        //given
        given(sessionManager.isSitePersisted(currentSite)).willReturn(false);
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_SITE_MODIFIED"), "site");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("CANCEL_OPTION"));
        
        //when
        SiteListenerImpl siteListener = new SiteListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        ComponentInfo newInfo = new ComponentInfo("newSite", "newDescription");
        siteListener.createNewSite(newInfo);
        
        //then
        // Check that the current currentSite wasn't saved
        verify(sessionManager, never()).saveSite(any(Site.class));
        // Check that the current isntrument is not changed
        verify(session, never()).setSite(any(Site.class));
        // Check that he UI is not notified for session changes
        verify(uiManager, never()).sessionModified();
    }

    /**
     * This checks that if the isSitePersisted() method fails with an exception
     * we have the same behavior like the current currentSite is modified.
     */
    @Test
    public void createNewSite_CurrentSiteModifiedCheckError() throws Exception {
        //given
        given(sessionManager.isSitePersisted(currentSite)).willThrow(new PersistenceDeviceException());
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_SITE_MODIFIED"), "site");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("CANCEL_OPTION"));
        
        //when
        SiteListenerImpl siteListener = new SiteListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        ComponentInfo newInfo = new ComponentInfo("newSite", "newDescription");
        siteListener.createNewSite(newInfo);
        
        //then
        // Check that the current currentSite wasn't saved
        verify(sessionManager, never()).saveSite(any(Site.class));
        // Check that the current isntrument is not changed
        verify(session, never()).setSite(any(Site.class));
        // Check that he UI is not notified for session changes
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void createNewSite_currentSiteModifiedSaveError() throws Exception {
        //given
        given(sessionManager.isSitePersisted(currentSite)).willReturn(false);
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_SITE_MODIFIED"), "site");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("SAVE_OPTION"));
        willThrow(new PersistenceDeviceException()).given(sessionManager).saveSite(currentSite);
        
        //when
        SiteListenerImpl siteListener = new SiteListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        ComponentInfo newInfo = new ComponentInfo("newSite", "newDescription");
        siteListener.createNewSite(newInfo);
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("SAVE_SITE_DEVICE_ERROR"), "site"));
        // Check that the current isntrument is not changed
        verify(session, never()).setSite(any(Site.class));
        // Check that he UI is not notified for session changes
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void createNewSite_currentSiteModifiedSaveSuccess() throws Exception {
        //given
        given(sessionManager.isSitePersisted(currentSite)).willReturn(false);
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_SITE_MODIFIED"), "site");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("SAVE_OPTION"));
        ComponentInfo newInfo = new ComponentInfo("newSite", "newDescription");
        Site newSite = mock(Site.class);
        given(newSite.getInfo()).willReturn(newInfo);
        given(sessionManager.createNewSite(newInfo)).willReturn(newSite);
        
        //when
        SiteListenerImpl siteListener = new SiteListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        siteListener.createNewSite(newInfo);
        
        //then
        // Check that the current currentSite was saved
        verify(sessionManager).saveSite(currentSite);
        // Check that the current isntrument is changed
        verify(session).setSite(newSite);
        // Check that he UI is notified for session changes
        verify(uiManager).sessionModified();
    }

    @Test
    public void createNewSite_currentSiteModifiedSaveIgnore() throws Exception {
        //given
        given(sessionManager.isSitePersisted(currentSite)).willReturn(false);
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_SITE_MODIFIED"), "site");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("IGNORE_OPTION"));
        ComponentInfo newInfo = new ComponentInfo("newSite", "newDescription");
        Site newSite = mock(Site.class);
        given(newSite.getInfo()).willReturn(newInfo);
        given(sessionManager.createNewSite(newInfo)).willReturn(newSite);
        
        //when
        SiteListenerImpl siteListener = new SiteListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        siteListener.createNewSite(newInfo);
        
        //then
        // Check that the current currentSite was not saved
        verify(sessionManager, never()).saveSite(currentSite);
        // Check that the current isntrument is changed
        verify(session).setSite(newSite);
        // Check that he UI is notified for session changes
        verify(uiManager).sessionModified();
    }

    @Test
    public void createNewSite_NameExists() throws Exception {
        //given
        given(sessionManager.isSitePersisted(currentSite)).willReturn(false);
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_SITE_MODIFIED"), "site");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("IGNORE_OPTION"));
        ComponentInfo newInfo = new ComponentInfo("newSite", "newDescription");
        given(sessionManager.createNewSite(newInfo)).willThrow(new NameExistsException());
        
        //when
        SiteListenerImpl siteListener = new SiteListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        siteListener.createNewSite(newInfo);
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("CREATE_SITE_NAME_EXISTS"), "newSite"));
        // Check that the current currentSite was not saved
        verify(sessionManager, never()).saveSite(currentSite);
        // Check that the current isntrument is not changed
        verify(session, never()).setSite(any(Site.class));
        // Check that he UI is notified for session changes
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void createNewSite_CreateFailedDeviceError() throws Exception {
        //given
        given(sessionManager.isSitePersisted(currentSite)).willReturn(false);
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_SITE_MODIFIED"), "site");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("IGNORE_OPTION"));
        ComponentInfo newInfo = new ComponentInfo("newSite", "newDescription");
        given(sessionManager.createNewSite(newInfo)).willThrow(new PersistenceDeviceException());
        
        //when
        SiteListenerImpl siteListener = new SiteListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        siteListener.createNewSite(newInfo);
        
        //then
        verify(messenger).error(bundle.getString("CREATE_SITE_DEVICE_ERROR"));
        // Check that the current currentSite was not saved
        verify(sessionManager, never()).saveSite(currentSite);
        // Check that the current isntrument is not changed
        verify(session, never()).setSite(any(Site.class));
        // Check that he UI is notified for session changes
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void loadSite_IsCurrent() throws Exception {
        //when
        SiteListenerImpl siteListener = new SiteListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        ComponentInfo loadInfo = new ComponentInfo("site", "description");
        siteListener.loadSite(loadInfo);

        //then
        // Check that the current isntrument is not changed
        verify(session, never()).setSite(any(Site.class));
        // Check that he UI is not notified for session changes
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void loadSite_CurrentSiteModifiedCanceled() throws Exception {
        //given
        given(sessionManager.isSitePersisted(currentSite)).willReturn(false);
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_SITE_MODIFIED"), "site");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("CANCEL_OPTION"));
        
        //when
        SiteListenerImpl siteListener = new SiteListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        ComponentInfo loadInfo = new ComponentInfo("loadSite", "loadDescription");
        siteListener.loadSite(loadInfo);
        
        //then
        verify(sessionManager, never()).saveSite(currentSite);
        // Check that the current isntrument is not changed
        verify(session, never()).setSite(any(Site.class));
        // Check that he UI is not notified for session changes
        verify(uiManager, never()).sessionModified();
    }

    /**
     * This checks that if the isSitePersisted() method fails with an exception
     * we have the same behavior like the current currentSite is modified.
     */
    @Test
    public void loadSite_CurrentSiteModifiedCheckError() throws Exception {
        //given
        given(sessionManager.isSitePersisted(currentSite)).willThrow(new PersistenceDeviceException());
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_SITE_MODIFIED"), "site");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("CANCEL_OPTION"));
        
        //when
        SiteListenerImpl siteListener = new SiteListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        ComponentInfo loadInfo = new ComponentInfo("loadSite", "loadDescription");
        siteListener.loadSite(loadInfo);
        
        //then
        verify(sessionManager, never()).saveSite(currentSite);
        // Check that the current isntrument is not changed
        verify(session, never()).setSite(any(Site.class));
        // Check that he UI is not notified for session changes
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void loadSite_CurrentSiteModifiedSaveError() throws Exception {
        //given
        given(sessionManager.isSitePersisted(currentSite)).willReturn(false);
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_SITE_MODIFIED"), "site");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("SAVE_OPTION"));
        willThrow(new PersistenceDeviceException()).given(sessionManager).saveSite(currentSite);
        
        //when
        SiteListenerImpl siteListener = new SiteListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        ComponentInfo loadInfo = new ComponentInfo("loadSite", "loadDescription");
        siteListener.loadSite(loadInfo);
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("SAVE_SITE_DEVICE_ERROR"), "site"));
        // Check that the current isntrument is not changed
        verify(session, never()).setSite(any(Site.class));
        // Check that he UI is not notified for session changes
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void loadSite_currentSiteModifiedSaveSuccess() throws Exception {
        //given
        given(sessionManager.isSitePersisted(currentSite)).willReturn(false);
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_SITE_MODIFIED"), "site");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("SAVE_OPTION"));
        ComponentInfo loadInfo = new ComponentInfo("loadSite", "loadDescription");
        Site loadSite = mock(Site.class);
        given(loadSite.getInfo()).willReturn(loadInfo);
        given(sessionManager.loadSite(loadInfo)).willReturn(loadSite);
        
        //when
        SiteListenerImpl siteListener = new SiteListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        siteListener.loadSite(loadInfo);
        
        //then
        verify(sessionManager).saveSite(currentSite);
        // Check that the current isntrument is changed
        verify(session).setSite(loadSite);
        // Check that he UI is notified for session changes
        verify(uiManager).sessionModified();
    }

    @Test
    public void loadSite_currentSiteModifiedSaveIgnore() throws Exception {
        //given
        given(sessionManager.isSitePersisted(currentSite)).willReturn(false);
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_SITE_MODIFIED"), "site");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("IGNORE_OPTION"));
        ComponentInfo loadInfo = new ComponentInfo("loadSite", "loadDescription");
        Site loadSite = mock(Site.class);
        given(loadSite.getInfo()).willReturn(loadInfo);
        given(sessionManager.loadSite(loadInfo)).willReturn(loadSite);
        
        //when
        SiteListenerImpl siteListener = new SiteListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        siteListener.loadSite(loadInfo);
        
        //then
        verify(sessionManager, never()).saveSite(currentSite);
        // Check that the current isntrument is changed
        verify(session).setSite(loadSite);
        // Check that he UI is notified for session changes
        verify(uiManager).sessionModified();
    }

    @Test
    public void loadSite_NameNotFound() throws Exception {
        //given
        given(sessionManager.isSitePersisted(currentSite)).willReturn(true);
        ComponentInfo loadInfo = new ComponentInfo("loadSite", "loadDescription");
        given(sessionManager.loadSite(loadInfo)).willThrow(new NameNotFoundException());
        
        //when
        SiteListenerImpl siteListener = new SiteListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        siteListener.loadSite(loadInfo);
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("LOAD_SITE_NOT_FOUND"), "loadSite"));
        verify(sessionManager, never()).saveSite(currentSite);
        // Check that the current isntrument is not changed
        verify(session, never()).setSite(any(Site.class));
        // Check that he UI is not notified for session changes
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void loadSite_LoadFailedDeviceError() throws Exception {
        //given
        given(sessionManager.isSitePersisted(currentSite)).willReturn(true);
        ComponentInfo loadInfo = new ComponentInfo("loadSite", "loadDescription");
        given(sessionManager.loadSite(loadInfo)).willThrow(new PersistenceDeviceException());
        
        //when
        SiteListenerImpl siteListener = new SiteListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        siteListener.loadSite(loadInfo);
        
        //then
        verify(messenger).error(bundle.getString("LOAD_SITE_DEVICE_ERROR"));
        verify(sessionManager, never()).saveSite(currentSite);
        // Check that the current isntrument is not changed
        verify(session, never()).setSite(any(Site.class));
        // Check that he UI is not notified for session changes
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void reloadCurrentSite_NotModified() throws Exception {
        //given
        given(sessionManager.isSiteInDatabase(currentSite)).willReturn(true);
        given(sessionManager.isSitePersisted(currentSite)).willReturn(true);
        
        //when
        SiteListenerImpl siteListener = new SiteListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        siteListener.reloadCurrentSite();
        
        //then
        verify(sessionManager, never()).loadSite(any(ComponentInfo.class));
        verify(session, never()).setSite(any(Site.class));
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void reloadCurrentSite_AbortedByUser() throws Exception {
        //given
        given(sessionManager.isSiteInDatabase(currentSite)).willReturn(true);
        given(sessionManager.isSitePersisted(currentSite)).willReturn(false);
        String confirmMessage = bundle.getString("CONFIRM_SITE_RELOAD");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("NO_OPTION"));
        
        //when
        SiteListenerImpl siteListener = new SiteListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        siteListener.reloadCurrentSite();
        
        //then
        verify(sessionManager, never()).loadSite(any(ComponentInfo.class));
        verify(session, never()).setSite(any(Site.class));
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void reloadCurrentSite_NameNotFound() throws Exception {
        //given
        given(sessionManager.isSiteInDatabase(currentSite)).willReturn(true);
        given(sessionManager.isSitePersisted(currentSite)).willReturn(false);
        String confirmMessage = bundle.getString("CONFIRM_SITE_RELOAD");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("YES_OPTION"));
        given(sessionManager.loadSite(currentSite.getInfo())).willThrow(new NameNotFoundException());
        
        //when
        SiteListenerImpl siteListener = new SiteListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        siteListener.reloadCurrentSite();
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("LOAD_SITE_NOT_FOUND"), "site"));
        verify(session, never()).setSite(any(Site.class));
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void reloadCurrentSite_DeviceError() throws Exception {
        //given
        given(sessionManager.isSiteInDatabase(currentSite)).willReturn(true);
        given(sessionManager.isSitePersisted(currentSite)).willReturn(false);
        String confirmMessage = bundle.getString("CONFIRM_SITE_RELOAD");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("YES_OPTION"));
        given(sessionManager.loadSite(currentSite.getInfo())).willThrow(new PersistenceDeviceException());
        
        //when
        SiteListenerImpl siteListener = new SiteListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        siteListener.reloadCurrentSite();
        
        //then
        verify(messenger).error(bundle.getString("LOAD_SITE_DEVICE_ERROR"));
        verify(session, never()).setSite(any(Site.class));
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void reloadCurrentSite_Success() throws Exception {
        //given
        given(sessionManager.isSiteInDatabase(currentSite)).willReturn(true);
        given(sessionManager.isSitePersisted(currentSite)).willReturn(false);
        String confirmMessage = bundle.getString("CONFIRM_SITE_RELOAD");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("YES_OPTION"));
        Site reloadedSite = mock(Site.class);
        given(sessionManager.loadSite(currentSite.getInfo())).willReturn(reloadedSite);
        
        //when
        SiteListenerImpl siteListener = new SiteListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        siteListener.reloadCurrentSite();
        
        //then
        verify(session).setSite(reloadedSite);
        verify(uiManager).sessionModified();
    }

    @Test
    public void saveSiteAs_NameExists() throws Exception {
        //given
        ComponentInfo newInfo = new ComponentInfo("newSite", "newDescription");
        given(sessionManager.saveSiteAs(currentSite, newInfo)).willThrow(new NameExistsException());
        
        //when
        SiteListenerImpl siteListener = new SiteListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        siteListener.saveCurrentSiteAs(newInfo);
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("CREATE_SITE_NAME_EXISTS"), "newSite"));
        verify(session, never()).setSite(any(Site.class));
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void saveSiteAs_DeviceError() throws Exception {
        //given
        ComponentInfo newInfo = new ComponentInfo("newSite", "newDescription");
        given(sessionManager.saveSiteAs(currentSite, newInfo)).willThrow(new PersistenceDeviceException());
        
        //when
        SiteListenerImpl siteListener = new SiteListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        siteListener.saveCurrentSiteAs(newInfo);
        
        //then
        verify(messenger).error(bundle.getString("CREATE_SITE_DEVICE_ERROR"));
        verify(session, never()).setSite(any(Site.class));
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void saveSiteAs_Success() throws Exception {
        //given
        ComponentInfo newInfo = new ComponentInfo("newSite", "newDescription");
        Site newSite = mock(Site.class);
        given(newSite.getInfo()).willReturn(newInfo);
        given(sessionManager.saveSiteAs(currentSite, newInfo)).willReturn(newSite);
        
        //when
        SiteListenerImpl siteListener = new SiteListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        siteListener.saveCurrentSiteAs(newInfo);
        
        //then
        verify(session).setSite(newSite);
        verify(uiManager).sessionModified();
    }
    
    @Test
    public void deleteSite_Current() throws Exception {
        //when
        SiteListenerImpl siteListener = new SiteListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        siteListener.deleteSite(currentSite.getInfo());
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("DELETE_SITE_CURRENT"), currentSite.getInfo()));
        verify(sessionManager, never()).deleteSite(any(ComponentInfo.class));
    }

    @Test
    public void deleteSite_CanceledByUser() throws Exception {
        //given
        String confirmMessage =  MessageFormat.format(bundle.getString("DELETE_SITE_CONFIRM"), "Test");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("CANCEL_OPTION"));
        ComponentInfo componentInfo = new ComponentInfo("Test", "Description");
        
        //when
        SiteListenerImpl siteListener = new SiteListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        siteListener.deleteSite(componentInfo);
        
        //then
        verify(sessionManager, never()).deleteSite(any(ComponentInfo.class));
    }

    @Test
    public void deleteSite_Success() throws Exception {
        //given
        String confirmMessage =  MessageFormat.format(bundle.getString("DELETE_SITE_CONFIRM"), "Test");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("DELETE_OPTION"));
        ComponentInfo componentInfo = new ComponentInfo("Test", "Description");
        
        //when
        SiteListenerImpl siteListener = new SiteListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        siteListener.deleteSite(componentInfo);
        
        //then
        verify(sessionManager).deleteSite(componentInfo);
        verify(messenger).info(MessageFormat.format(bundle.getString("DELETE_SITE_SUCCESS"), "Test"));
    }

    @Test
    public void deleteSite_InUse() throws Exception {
        //given
        String confirmMessage =  MessageFormat.format(bundle.getString("DELETE_SITE_CONFIRM"), "Test");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("DELETE_OPTION"));
        ComponentInfo componentInfo = new ComponentInfo("Test", "Description");
        List<String> inUseList = new ArrayList<String>();
        inUseList.add("Session");
        willThrow(new InUseException(inUseList)).given(sessionManager).deleteSite(componentInfo);
        
        //when
        SiteListenerImpl siteListener = new SiteListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        siteListener.deleteSite(componentInfo);
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("DELETE_SITE_IN_USE"), "Test", "Session"));
    }

    @Test
    public void deleteSite_NameNotFound() throws Exception {
        //given
        String confirmMessage =  MessageFormat.format(bundle.getString("DELETE_SITE_CONFIRM"), "Test");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("DELETE_OPTION"));
        ComponentInfo componentInfo = new ComponentInfo("Test", "Description");
        willThrow(new NameNotFoundException()).given(sessionManager).deleteSite(componentInfo);
        
        //when
        SiteListenerImpl siteListener = new SiteListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        siteListener.deleteSite(componentInfo);
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("DELETE_SITE_NOT_FOUND"), "Test"));
    }

    @Test
    public void deleteSite_DeviceError() throws Exception {
        //given
        String confirmMessage =  MessageFormat.format(bundle.getString("DELETE_SITE_CONFIRM"), "Test");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("DELETE_OPTION"));
        ComponentInfo componentInfo = new ComponentInfo("Test", "Description");
        willThrow(new PersistenceDeviceException()).given(sessionManager).deleteSite(componentInfo);
        
        //when
        SiteListenerImpl siteListener = new SiteListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        siteListener.deleteSite(componentInfo);
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("DELETE_SITE_DEVICE_ERROR"), "Test"));
    }
}