/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.controller;

import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.cnrs.lam.dis.etc.datamodel.DatasetMockCreator;
import org.cnrs.lam.dis.etc.persistence.DatasetManager;
import org.cnrs.lam.dis.etc.persistence.NameNotFoundException;
import org.cnrs.lam.dis.etc.persistence.PersistenceDeviceException;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.MockitoAnnotations;
import org.mockito.Mock;
import static org.mockito.BDDMockito.*;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class DatasetProviderImplTest {

    @Mock DatasetManager datasetManager;
    @Mock Dataset testDataset;
    DatasetInfo testInfo;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        testDataset = DatasetMockCreator.createFwhm();
        testInfo = testDataset.getInfo();
        when(datasetManager.loadDataset(Dataset.Type.FWHM, testInfo, null)).thenReturn(testDataset);
    }

    @Test
    public void getDataset_Normal() {
        //when
        DatasetProviderImpl datasetProvider = new DatasetProviderImpl(datasetManager);
        Dataset result = datasetProvider.getDataset(Dataset.Type.FWHM, testInfo);
        
        //then
        assertEquals(testDataset, result);
    }

    @Test
    public void getDataset_NameNotFound() throws Exception {
        //given
        given(datasetManager.loadDataset(Dataset.Type.FWHM, testInfo, null)).willThrow(new NameNotFoundException());
        
        //when
        DatasetProviderImpl datasetProvider = new DatasetProviderImpl(datasetManager);
        Dataset result = datasetProvider.getDataset(Dataset.Type.FWHM, testInfo);
        
        //then
        assertNull(result);
    }

    @Test
    public void getDataset_IOProblem() throws Exception {
        //given
        given(datasetManager.loadDataset(Dataset.Type.FWHM, testInfo, null)).willThrow(new PersistenceDeviceException());
        
        //when
        DatasetProviderImpl datasetProvider = new DatasetProviderImpl(datasetManager);
        Dataset result = datasetProvider.getDataset(Dataset.Type.FWHM, testInfo);
        
        //then
        assertNull(result);
    }
}