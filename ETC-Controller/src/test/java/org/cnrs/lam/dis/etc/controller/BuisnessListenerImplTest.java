/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.controller;

import java.util.ArrayList;
import java.util.List;
import org.cnrs.lam.dis.etc.configuration.ConfigFactory;
import java.util.ResourceBundle;
import org.cnrs.lam.dis.etc.calculator.CalculationError;
import org.cnrs.lam.dis.etc.calculator.Calculator;
import org.cnrs.lam.dis.etc.calculator.Validator;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.cnrs.lam.dis.etc.datamodel.DatasetProvider;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.cnrs.lam.dis.etc.ui.BuisnessListener;
import org.cnrs.lam.dis.etc.ui.Messenger;
import org.cnrs.lam.dis.etc.ui.UIManager;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.mockito.Mock;
import static org.mockito.BDDMockito.*;
import org.mockito.InOrder;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class BuisnessListenerImplTest {

    @Mock Messenger messenger;
    @Mock Validator validator;
    @Mock Calculator calculator;
    @Mock UIManager uiManager;
    @Mock DatasetProvider datasetProvider;
    private ResourceBundle bundle;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        bundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/controller/messages");
    }

    @Test
    public void executeCalculation_ValidationFailed() {
        //given
        List<String> errorList = new ArrayList<String>();
        errorList.add("Reason1");
        errorList.add("Reason2");
        given(validator.validateSession(any(Session.class))).willReturn(errorList);
        StringBuffer expectedMessage = new StringBuffer();
        expectedMessage.append(bundle.getString("SESSION_VALIDATION_ERROR"))
                .append(ConfigFactory.getConfig().getLineSeparator())
                .append("Reason1")
                .append(ConfigFactory.getConfig().getLineSeparator())
                .append("Reason2");
        
        //when
        BuisnessListener buisnessListener = new BuisnessListenerImpl(validator, calculator, messenger, uiManager, datasetProvider, null);
        buisnessListener.executeCalculation();
        
        //then
        verify(messenger).error(expectedMessage.toString());
        verify(messenger, never()).actionStarted(any(String.class), any(Thread.class));
        verify(uiManager, never()).showCalculationResult(any(CalculationResults.class));
    }

    @Test
    public void executeCalculation_CalculationFailed() throws Exception {
        //given
        given(validator.validateSession(any(Session.class))).willReturn(new ArrayList<String>());
        given(calculator.performCalculation(any(Session.class), any(DatasetProvider.class))).willThrow(new CalculationError());
        
        //when
        BuisnessListener buisnessListener = new BuisnessListenerImpl(validator, calculator, messenger, uiManager, datasetProvider, null);
        buisnessListener.executeCalculation();
        //then
        InOrder inOrder = inOrder(messenger);
        inOrder.verify(messenger).actionStarted(eq(bundle.getString("WAIT_FOR_CALCULATION")), any(Thread.class));
        //inOrder.verify(messenger).error(bundle.getString("CALCULATION_ERROR"));
        verify(uiManager, never()).showCalculationResult(any(CalculationResults.class));
    }

    @Test
    public void executeCalculation_Success() throws Exception {
        //given
        given(validator.validateSession(any(Session.class))).willReturn(new ArrayList<String>());
        given(calculator.performCalculation(any(Session.class), any(DatasetProvider.class))).willReturn(new CalculationResults());
        
        //when
        BuisnessListener buisnessListener = new BuisnessListenerImpl(validator, calculator, messenger, uiManager, datasetProvider, null);
        buisnessListener.executeCalculation();
        Thread.sleep(100);
        
        //then
        InOrder inOrder = inOrder(messenger);
        inOrder.verify(messenger).actionStarted(eq(bundle.getString("WAIT_FOR_CALCULATION")), any(Thread.class));
        inOrder.verify(messenger).actionStoped(any(Thread.class));
        verify(uiManager).showCalculationResult(notNull(CalculationResults.class));
    }

}