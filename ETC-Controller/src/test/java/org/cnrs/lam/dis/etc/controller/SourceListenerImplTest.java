/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.controller;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import org.cnrs.lam.dis.etc.datamodel.ComponentInfo;
import org.cnrs.lam.dis.etc.datamodel.Source;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.cnrs.lam.dis.etc.persistence.InUseException;
import org.cnrs.lam.dis.etc.persistence.NameExistsException;
import org.cnrs.lam.dis.etc.persistence.NameNotFoundException;
import org.cnrs.lam.dis.etc.persistence.PersistenceDeviceException;
import org.cnrs.lam.dis.etc.persistence.SessionManager;
import org.cnrs.lam.dis.etc.ui.Messenger;
import org.cnrs.lam.dis.etc.ui.UIManager;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.BDDMockito.*;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class SourceListenerImplTest {

    @Mock SessionManager sessionManager;
    @Mock Messenger messenger;
    @Mock UIManager uiManager;
    @Mock Source currentSource;
    @Mock Session session;
    ResourceBundle bundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/controller/messages");

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        when(currentSource.getInfo()).thenReturn(new ComponentInfo("source", "SourceDescription"));
        when(session.getSource()).thenReturn(currentSource);
        CurrentSessionContainer.setCurrentSession(session);
    }

    @Test
    public void saveCurrentSource_AlreadyPersisted() throws Exception {
        //given
        given(sessionManager.isSourcePersisted(currentSource)).willReturn(true);
        
        //when
        SourceListenerImpl sourceListener = new SourceListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        sourceListener.saveCurrentSource();
        
        //then
        verify(sessionManager, never()).saveSource(currentSource);
        verify(uiManager, never()).sessionModified();
    }

    /**
     * This test checks that when there is a persistence device reading error
     * for which the check if the current currentSource is already persisted fails,
     * the system treats the current currentSource as modified and it proceeds with
     * saving it.
     */
    @Test
    public void saveCurrentSource_ErrorWhileCheckingIfAlreadyPersisted() throws Exception {
        //given
        given(sessionManager.isSourcePersisted(currentSource)).willThrow(new PersistenceDeviceException());
        
        //when
        SourceListenerImpl sourceListener = new SourceListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        sourceListener.saveCurrentSource();
        
        //then
        verify(sessionManager).saveSource(currentSource);
    }

    @Test
    public void saveCurrentSource_ErrorWhilePersisting() throws Exception {
        //given
        given(sessionManager.isSourcePersisted(currentSource)).willReturn(false);
        willThrow(new PersistenceDeviceException()).given(sessionManager).saveSource(currentSource);
        
        //when
        SourceListenerImpl sourceListener = new SourceListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        sourceListener.saveCurrentSource();
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("SAVE_SOURCE_DEVICE_ERROR"), "source"));
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void saveCurrentSource_Success() throws Exception {
        //given
        given(sessionManager.isSourcePersisted(currentSource)).willReturn(false);
        
        //when
        SourceListenerImpl sourceListener = new SourceListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        sourceListener.saveCurrentSource();
        
        //then
        verify(sessionManager).saveSource(currentSource);
        verify(uiManager).sessionModified();
    }

    @Test
    public void createNewSource_CurrentSourceModifiedCanceled() throws Exception {
        //given
        given(sessionManager.isSourcePersisted(currentSource)).willReturn(false);
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_SOURCE_MODIFIED"), "source");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("CANCEL_OPTION"));
        
        //when
        SourceListenerImpl sourceListener = new SourceListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        ComponentInfo newInfo = new ComponentInfo("newSource", "newDescription");
        sourceListener.createNewSource(newInfo);
        
        //then
        // Check that the current currentSource wasn't saved
        verify(sessionManager, never()).saveSource(any(Source.class));
        // Check that the current isntrument is not changed
        verify(session, never()).setSource(any(Source.class));
        // Check that he UI is not notified for session changes
        verify(uiManager, never()).sessionModified();
    }

    /**
     * This checks that if the isSourcePersisted() method fails with an exception
     * we have the same behavior like the current currentSource is modified.
     */
    @Test
    public void createNewSource_CurrentSourceModifiedCheckError() throws Exception {
        //given
        given(sessionManager.isSourcePersisted(currentSource)).willThrow(new PersistenceDeviceException());
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_SOURCE_MODIFIED"), "source");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("CANCEL_OPTION"));
        
        //when
        SourceListenerImpl sourceListener = new SourceListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        ComponentInfo newInfo = new ComponentInfo("newSource", "newDescription");
        sourceListener.createNewSource(newInfo);
        
        //then
        // Check that the current currentSource wasn't saved
        verify(sessionManager, never()).saveSource(any(Source.class));
        // Check that the current isntrument is not changed
        verify(session, never()).setSource(any(Source.class));
        // Check that he UI is not notified for session changes
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void createNewSource_currentSourceModifiedSaveError() throws Exception {
        //given
        given(sessionManager.isSourcePersisted(currentSource)).willReturn(false);
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_SOURCE_MODIFIED"), "source");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("SAVE_OPTION"));
        willThrow(new PersistenceDeviceException()).given(sessionManager).saveSource(currentSource);
        
        //when
        SourceListenerImpl sourceListener = new SourceListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        ComponentInfo newInfo = new ComponentInfo("newSource", "newDescription");
        sourceListener.createNewSource(newInfo);
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("SAVE_SOURCE_DEVICE_ERROR"), "source"));
        // Check that the current isntrument is not changed
        verify(session, never()).setSource(any(Source.class));
        // Check that he UI is not notified for session changes
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void createNewSource_currentSourceModifiedSaveSuccess() throws Exception {
        //given
        given(sessionManager.isSourcePersisted(currentSource)).willReturn(false);
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_SOURCE_MODIFIED"), "source");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("SAVE_OPTION"));
        ComponentInfo newInfo = new ComponentInfo("newSource", "newDescription");
        Source newSource = mock(Source.class);
        given(newSource.getInfo()).willReturn(newInfo);
        given(sessionManager.createNewSource(newInfo)).willReturn(newSource);
        
        //when
        SourceListenerImpl sourceListener = new SourceListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        sourceListener.createNewSource(newInfo);
        
        //then
        // Check that the current currentSource was saved
        verify(sessionManager).saveSource(currentSource);
        // Check that the current isntrument is changed
        verify(session).setSource(newSource);
        // Check that he UI is notified for session changes
        verify(uiManager).sessionModified();
    }

    @Test
    public void createNewSource_currentSourceModifiedSaveIgnore() throws Exception {
        //given
        given(sessionManager.isSourcePersisted(currentSource)).willReturn(false);
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_SOURCE_MODIFIED"), "source");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("IGNORE_OPTION"));
        ComponentInfo newInfo = new ComponentInfo("newSource", "newDescription");
        Source newSource = mock(Source.class);
        given(newSource.getInfo()).willReturn(newInfo);
        given(sessionManager.createNewSource(newInfo)).willReturn(newSource);
        
        //when
        SourceListenerImpl sourceListener = new SourceListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        sourceListener.createNewSource(newInfo);
        
        //then
        // Check that the current currentSource was not saved
        verify(sessionManager, never()).saveSource(currentSource);
        // Check that the current isntrument is changed
        verify(session).setSource(newSource);
        // Check that he UI is notified for session changes
        verify(uiManager).sessionModified();
    }

    @Test
    public void createNewSource_NameExists() throws Exception {
        //given
        given(sessionManager.isSourcePersisted(currentSource)).willReturn(false);
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_SOURCE_MODIFIED"), "source");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("IGNORE_OPTION"));
        ComponentInfo newInfo = new ComponentInfo("newSource", "newDescription");
        given(sessionManager.createNewSource(newInfo)).willThrow(new NameExistsException());
        
        //when
        SourceListenerImpl sourceListener = new SourceListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        sourceListener.createNewSource(newInfo);
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("CREATE_SOURCE_NAME_EXISTS"), "newSource"));
        // Check that the current currentSource was not saved
        verify(sessionManager, never()).saveSource(currentSource);
        // Check that the current isntrument is not changed
        verify(session, never()).setSource(any(Source.class));
        // Check that he UI is notified for session changes
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void createNewSource_CreateFailedDeviceError() throws Exception {
        //given
        given(sessionManager.isSourcePersisted(currentSource)).willReturn(false);
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_SOURCE_MODIFIED"), "source");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("IGNORE_OPTION"));
        ComponentInfo newInfo = new ComponentInfo("newSource", "newDescription");
        given(sessionManager.createNewSource(newInfo)).willThrow(new PersistenceDeviceException());
        
        //when
        SourceListenerImpl sourceListener = new SourceListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        sourceListener.createNewSource(newInfo);
        
        //then
        verify(messenger).error(bundle.getString("CREATE_SOURCE_DEVICE_ERROR"));
        // Check that the current currentSource was not saved
        verify(sessionManager, never()).saveSource(currentSource);
        // Check that the current isntrument is not changed
        verify(session, never()).setSource(any(Source.class));
        // Check that he UI is notified for session changes
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void loadSource_IsCurrent() throws Exception {
        //when
        SourceListenerImpl sourceListener = new SourceListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        ComponentInfo loadInfo = new ComponentInfo("source", "description");
        sourceListener.loadSource(loadInfo);

        //then
        // Check that the current isntrument is not changed
        verify(session, never()).setSource(any(Source.class));
        // Check that he UI is not notified for session changes
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void loadSource_CurrentSourceModifiedCanceled() throws Exception {
        //given
        given(sessionManager.isSourcePersisted(currentSource)).willReturn(false);
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_SOURCE_MODIFIED"), "source");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("CANCEL_OPTION"));
        
        //when
        SourceListenerImpl sourceListener = new SourceListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        ComponentInfo loadInfo = new ComponentInfo("loadSource", "loadDescription");
        sourceListener.loadSource(loadInfo);
        
        //then
        verify(sessionManager, never()).saveSource(currentSource);
        // Check that the current isntrument is not changed
        verify(session, never()).setSource(any(Source.class));
        // Check that he UI is not notified for session changes
        verify(uiManager, never()).sessionModified();
    }

    /**
     * This checks that if the isSourcePersisted() method fails with an exception
     * we have the same behavior like the current currentSource is modified.
     */
    @Test
    public void loadSource_CurrentSourceModifiedCheckError() throws Exception {
        //given
        given(sessionManager.isSourcePersisted(currentSource)).willThrow(new PersistenceDeviceException());
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_SOURCE_MODIFIED"), "source");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("CANCEL_OPTION"));
        
        //when
        SourceListenerImpl sourceListener = new SourceListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        ComponentInfo loadInfo = new ComponentInfo("loadSource", "loadDescription");
        sourceListener.loadSource(loadInfo);
        
        //then
        verify(sessionManager, never()).saveSource(currentSource);
        // Check that the current isntrument is not changed
        verify(session, never()).setSource(any(Source.class));
        // Check that he UI is not notified for session changes
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void loadSource_CurrentSourceModifiedSaveError() throws Exception {
        //given
        given(sessionManager.isSourcePersisted(currentSource)).willReturn(false);
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_SOURCE_MODIFIED"), "source");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("SAVE_OPTION"));
        willThrow(new PersistenceDeviceException()).given(sessionManager).saveSource(currentSource);
        
        //when
        SourceListenerImpl sourceListener = new SourceListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        ComponentInfo loadInfo = new ComponentInfo("loadSource", "loadDescription");
        sourceListener.loadSource(loadInfo);
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("SAVE_SOURCE_DEVICE_ERROR"), "source"));
        // Check that the current isntrument is not changed
        verify(session, never()).setSource(any(Source.class));
        // Check that he UI is not notified for session changes
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void loadSource_currentSourceModifiedSaveSuccess() throws Exception {
        //given
        given(sessionManager.isSourcePersisted(currentSource)).willReturn(false);
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_SOURCE_MODIFIED"), "source");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("SAVE_OPTION"));
        ComponentInfo loadInfo = new ComponentInfo("loadSource", "loadDescription");
        Source loadSource = mock(Source.class);
        given(loadSource.getInfo()).willReturn(loadInfo);
        given(sessionManager.loadSource(loadInfo)).willReturn(loadSource);
        
        //when
        SourceListenerImpl sourceListener = new SourceListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        sourceListener.loadSource(loadInfo);
        
        //then
        verify(sessionManager).saveSource(currentSource);
        // Check that the current isntrument is changed
        verify(session).setSource(loadSource);
        // Check that he UI is notified for session changes
        verify(uiManager).sessionModified();
    }

    @Test
    public void loadSource_currentSourceModifiedSaveIgnore() throws Exception {
        //given
        given(sessionManager.isSourcePersisted(currentSource)).willReturn(false);
        String confirmMessage = MessageFormat.format(bundle.getString("CURRENT_SOURCE_MODIFIED"), "source");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("IGNORE_OPTION"));
        ComponentInfo loadInfo = new ComponentInfo("loadSource", "loadDescription");
        Source loadSource = mock(Source.class);
        given(loadSource.getInfo()).willReturn(loadInfo);
        given(sessionManager.loadSource(loadInfo)).willReturn(loadSource);
        
        //when
        SourceListenerImpl sourceListener = new SourceListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        sourceListener.loadSource(loadInfo);
        
        //then
        verify(sessionManager, never()).saveSource(currentSource);
        // Check that the current isntrument is changed
        verify(session).setSource(loadSource);
        // Check that he UI is notified for session changes
        verify(uiManager).sessionModified();
    }

    @Test
    public void loadSource_NameNotFound() throws Exception {
        //given
        given(sessionManager.isSourcePersisted(currentSource)).willReturn(true);
        ComponentInfo loadInfo = new ComponentInfo("loadSource", "loadDescription");
        given(sessionManager.loadSource(loadInfo)).willThrow(new NameNotFoundException());
        
        //when
        SourceListenerImpl sourceListener = new SourceListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        sourceListener.loadSource(loadInfo);
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("LOAD_SOURCE_NOT_FOUND"), "loadSource"));
        verify(sessionManager, never()).saveSource(currentSource);
        // Check that the current isntrument is not changed
        verify(session, never()).setSource(any(Source.class));
        // Check that he UI is not notified for session changes
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void loadSource_LoadFailedDeviceError() throws Exception {
        //given
        given(sessionManager.isSourcePersisted(currentSource)).willReturn(true);
        ComponentInfo loadInfo = new ComponentInfo("loadSource", "loadDescription");
        given(sessionManager.loadSource(loadInfo)).willThrow(new PersistenceDeviceException());
        
        //when
        SourceListenerImpl sourceListener = new SourceListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        sourceListener.loadSource(loadInfo);
        
        //then
        verify(messenger).error(bundle.getString("LOAD_SOURCE_DEVICE_ERROR"));
        verify(sessionManager, never()).saveSource(currentSource);
        // Check that the current isntrument is not changed
        verify(session, never()).setSource(any(Source.class));
        // Check that he UI is not notified for session changes
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void reloadCurrentSource_NotModified() throws Exception {
        //given
        given(sessionManager.isSourceInDatabase(currentSource)).willReturn(true);
        given(sessionManager.isSourcePersisted(currentSource)).willReturn(true);
        
        //when
        SourceListenerImpl sourceListener = new SourceListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        sourceListener.reloadCurrentSource();
        
        //then
        verify(sessionManager, never()).loadSource(any(ComponentInfo.class));
        verify(session, never()).setSource(any(Source.class));
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void reloadCurrentSource_AbortedByUser() throws Exception {
        //given
        given(sessionManager.isSourceInDatabase(currentSource)).willReturn(true);
        given(sessionManager.isSourcePersisted(currentSource)).willReturn(false);
        String confirmMessage = bundle.getString("CONFIRM_SOURCE_RELOAD");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("NO_OPTION"));
        
        //when
        SourceListenerImpl sourceListener = new SourceListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        sourceListener.reloadCurrentSource();
        
        //then
        verify(sessionManager, never()).loadSource(any(ComponentInfo.class));
        verify(session, never()).setSource(any(Source.class));
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void reloadCurrentSource_NameNotFound() throws Exception {
        //given
        given(sessionManager.isSourceInDatabase(currentSource)).willReturn(true);
        given(sessionManager.isSourcePersisted(currentSource)).willReturn(false);
        String confirmMessage = bundle.getString("CONFIRM_SOURCE_RELOAD");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("YES_OPTION"));
        given(sessionManager.loadSource(currentSource.getInfo())).willThrow(new NameNotFoundException());
        
        //when
        SourceListenerImpl sourceListener = new SourceListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        sourceListener.reloadCurrentSource();
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("LOAD_SOURCE_NOT_FOUND"), "source"));
        verify(session, never()).setSource(any(Source.class));
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void reloadCurrentSource_DeviceError() throws Exception {
        //given
        given(sessionManager.isSourceInDatabase(currentSource)).willReturn(true);
        given(sessionManager.isSourcePersisted(currentSource)).willReturn(false);
        String confirmMessage = bundle.getString("CONFIRM_SOURCE_RELOAD");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("YES_OPTION"));
        given(sessionManager.loadSource(currentSource.getInfo())).willThrow(new PersistenceDeviceException());
        
        //when
        SourceListenerImpl sourceListener = new SourceListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        sourceListener.reloadCurrentSource();
        
        //then
        verify(messenger).error(bundle.getString("LOAD_SOURCE_DEVICE_ERROR"));
        verify(session, never()).setSource(any(Source.class));
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void reloadCurrentSource_Success() throws Exception {
        //given
        given(sessionManager.isSourceInDatabase(currentSource)).willReturn(true);
        given(sessionManager.isSourcePersisted(currentSource)).willReturn(false);
        String confirmMessage = bundle.getString("CONFIRM_SOURCE_RELOAD");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("YES_OPTION"));
        Source reloadedSource = mock(Source.class);
        given(sessionManager.loadSource(currentSource.getInfo())).willReturn(reloadedSource);
        
        //when
        SourceListenerImpl sourceListener = new SourceListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        sourceListener.reloadCurrentSource();
        
        //then
        verify(session).setSource(reloadedSource);
        verify(uiManager).sessionModified();
    }

    @Test
    public void saveSourceAs_NameExists() throws Exception {
        //given
        ComponentInfo newInfo = new ComponentInfo("newSource", "newDescription");
        given(sessionManager.saveSourceAs(currentSource, newInfo)).willThrow(new NameExistsException());
        
        //when
        SourceListenerImpl sourceListener = new SourceListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        sourceListener.saveCurrentSourceAs(newInfo);
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("CREATE_SOURCE_NAME_EXISTS"), "newSource"));
        verify(session, never()).setSource(any(Source.class));
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void saveSourceAs_DeviceError() throws Exception {
        //given
        ComponentInfo newInfo = new ComponentInfo("newSource", "newDescription");
        given(sessionManager.saveSourceAs(currentSource, newInfo)).willThrow(new PersistenceDeviceException());
        
        //when
        SourceListenerImpl sourceListener = new SourceListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        sourceListener.saveCurrentSourceAs(newInfo);
        
        //then
        verify(messenger).error(bundle.getString("CREATE_SOURCE_DEVICE_ERROR"));
        verify(session, never()).setSource(any(Source.class));
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void saveSourceAs_Success() throws Exception {
        //given
        ComponentInfo newInfo = new ComponentInfo("newSource", "newDescription");
        Source newSource = mock(Source.class);
        given(newSource.getInfo()).willReturn(newInfo);
        given(sessionManager.saveSourceAs(currentSource, newInfo)).willReturn(newSource);
        
        //when
        SourceListenerImpl sourceListener = new SourceListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        sourceListener.saveCurrentSourceAs(newInfo);
        
        //then
        verify(session).setSource(newSource);
        verify(uiManager).sessionModified();
    }
    
    @Test
    public void deleteSource_Current() throws Exception {
        //when
        SourceListenerImpl sourceListener = new SourceListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        sourceListener.deleteSource(currentSource.getInfo());
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("DELETE_SOURCE_CURRENT"), currentSource.getInfo()));
        verify(sessionManager, never()).deleteSource(any(ComponentInfo.class));
    }

    @Test
    public void deleteSource_CanceledByUser() throws Exception {
        //given
        String confirmMessage =  MessageFormat.format(bundle.getString("DELETE_SOURCE_CONFIRM"), "Test");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("CANCEL_OPTION"));
        ComponentInfo componentInfo = new ComponentInfo("Test", "Description");
        
        //when
        SourceListenerImpl sourceListener = new SourceListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        sourceListener.deleteSource(componentInfo);
        
        //then
        verify(sessionManager, never()).deleteSource(any(ComponentInfo.class));
    }

    @Test
    public void deleteSource_Success() throws Exception {
        //given
        String confirmMessage =  MessageFormat.format(bundle.getString("DELETE_SOURCE_CONFIRM"), "Test");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("DELETE_OPTION"));
        ComponentInfo componentInfo = new ComponentInfo("Test", "Description");
        
        //when
        SourceListenerImpl sourceListener = new SourceListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        sourceListener.deleteSource(componentInfo);
        
        //then
        verify(sessionManager).deleteSource(componentInfo);
        verify(messenger).info(MessageFormat.format(bundle.getString("DELETE_SOURCE_SUCCESS"), "Test"));
    }

    @Test
    public void deleteSource_InUse() throws Exception {
        //given
        String confirmMessage =  MessageFormat.format(bundle.getString("DELETE_SOURCE_CONFIRM"), "Test");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("DELETE_OPTION"));
        ComponentInfo componentInfo = new ComponentInfo("Test", "Description");
        List<String> inUseList = new ArrayList<String>();
        inUseList.add("Session");
        willThrow(new InUseException(inUseList)).given(sessionManager).deleteSource(componentInfo);
        
        //when
        SourceListenerImpl sourceListener = new SourceListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        sourceListener.deleteSource(componentInfo);
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("DELETE_SOURCE_IN_USE"), "Test", "Session"));
    }

    @Test
    public void deleteSource_NameNotFound() throws Exception {
        //given
        String confirmMessage =  MessageFormat.format(bundle.getString("DELETE_SOURCE_CONFIRM"), "Test");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("DELETE_OPTION"));
        ComponentInfo componentInfo = new ComponentInfo("Test", "Description");
        willThrow(new NameNotFoundException()).given(sessionManager).deleteSource(componentInfo);
        
        //when
        SourceListenerImpl sourceListener = new SourceListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        sourceListener.deleteSource(componentInfo);
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("DELETE_SOURCE_NOT_FOUND"), "Test"));
    }

    @Test
    public void deleteSource_DeviceError() throws Exception {
        //given
        String confirmMessage =  MessageFormat.format(bundle.getString("DELETE_SOURCE_CONFIRM"), "Test");
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("DELETE_OPTION"));
        ComponentInfo componentInfo = new ComponentInfo("Test", "Description");
        willThrow(new PersistenceDeviceException()).given(sessionManager).deleteSource(componentInfo);
        
        //when
        SourceListenerImpl sourceListener = new SourceListenerImpl(sessionManager, null, messenger, uiManager, null, null, null);
        sourceListener.deleteSource(componentInfo);
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("DELETE_SOURCE_DEVICE_ERROR"), "Test"));
    }
}