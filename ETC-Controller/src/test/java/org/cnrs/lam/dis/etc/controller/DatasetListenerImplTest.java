/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.controller;

import cds.savot.model.SavotVOTable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import org.cnrs.lam.dis.etc.dataimportexport.DataImporter;
import org.cnrs.lam.dis.etc.dataimportexport.WrongFormatException;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.cnrs.lam.dis.etc.datamodel.DatasetMockCreator;
import org.cnrs.lam.dis.etc.datamodel.Instrument;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.cnrs.lam.dis.etc.persistence.DatasetManager;
import org.cnrs.lam.dis.etc.persistence.InUseException;
import org.cnrs.lam.dis.etc.persistence.NameExistsException;
import org.cnrs.lam.dis.etc.persistence.NameNotFoundException;
import org.cnrs.lam.dis.etc.persistence.PersistenceDeviceException;
import org.cnrs.lam.dis.etc.ui.Messenger;
import org.cnrs.lam.dis.etc.ui.UIManager;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.BDDMockito.*;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class DatasetListenerImplTest {
    
    @Mock DatasetManager datasetManager;
    @Mock Messenger messenger;
    @Mock UIManager uiManager;
    @Mock DataImporter dataImporter;
    @Mock Instrument instrument;
    @Mock Session session;
    Dataset fwhmDataset;
    DatasetInfo fwhmInfo;
    
    private ResourceBundle bundle;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        
        fwhmDataset = DatasetMockCreator.createFwhm();
        fwhmInfo = fwhmDataset.getInfo();
        when(session.getInstrument()).thenReturn(instrument);
        CurrentSessionContainer.setCurrentSession(session);
                
        bundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/controller/messages");
    }

    @Test
    public void previewDataset_NameNotFound() throws Exception {
        //given
        given(datasetManager.loadDataset(any(Dataset.Type.class), any(DatasetInfo.class), anyString())).willThrow(new NameNotFoundException());
        
        //when
        DatasetListenerImpl datasetListener = new DatasetListenerImpl(datasetManager, messenger, uiManager, dataImporter, null, null);
        datasetListener.previewDataset(Dataset.Type.FWHM, new DatasetInfo("Test", "namespace", "Description"), null);
        
        //then
        verify(messenger).error(bundle.getString("DATASET_NOT_FOUND"));
        verify(uiManager, never()).showChart(anyString(), any(SavotVOTable.class), any(File.class));
    }

    @Test
    public void previewDataset_IOProblem() throws Exception {
        //given
        given(datasetManager.loadDataset(any(Dataset.Type.class), any(DatasetInfo.class), anyString())).willThrow(new PersistenceDeviceException());
        
        //when
        DatasetListenerImpl datasetListener = new DatasetListenerImpl(datasetManager, messenger, uiManager, dataImporter, null, null);
        datasetListener.previewDataset(Dataset.Type.FWHM, new DatasetInfo("Test", "namespace", "Description"), null);
        
        //then
        verify(messenger).error(bundle.getString("RETRIEVE_DATASET_ERROR"));
        verify(uiManager, never()).showChart(anyString(), any(SavotVOTable.class), any(File.class));
    }

    @Test
    public void previewDataset_Normal() throws Exception {
        //given
        given(datasetManager.loadDataset(any(Dataset.Type.class), any(DatasetInfo.class), anyString())).willReturn(fwhmDataset);

        //when
        DatasetListenerImpl datasetListener = new DatasetListenerImpl(datasetManager, messenger, uiManager, dataImporter, null, null);
        datasetListener.previewDataset(Dataset.Type.FWHM, fwhmInfo, null);
        
        //then
        verify(uiManager).showChart(eq(fwhmInfo.getName()), any(SavotVOTable.class), any(File.class));
    }

    @Test
    public void deleteDataset_CanceledByUser() throws Exception {
        //given
        String message = MessageFormat.format(bundle.getString("DELETE_DATASET_CONFIRM"), fwhmInfo.toString());
        given(messenger.decision(eq(message), anyListOf(String.class))).willReturn(bundle.getString("CANCEL_OPTION"));
        
        //when
        DatasetListenerImpl datasetListener = new DatasetListenerImpl(datasetManager, messenger, uiManager, dataImporter, null, null);
        datasetListener.deleteDataset(Dataset.Type.FWHM, fwhmInfo);
        
        //then
        verify(messenger).decision(eq(message), anyListOf(String.class));
        verify(datasetManager, never()).deleteDataset(any(Dataset.Type.class), any(DatasetInfo.class));
    }

    @Test
    public void deleteDataset_Success() throws Exception {
        //given
        String confirmMessage = MessageFormat.format(bundle.getString("DELETE_DATASET_CONFIRM"), fwhmInfo.toString());
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("DELETE_OPTION"));
        
        //when
        DatasetListenerImpl datasetListener = new DatasetListenerImpl(datasetManager, messenger, uiManager, dataImporter, null, null);
        datasetListener.deleteDataset(Dataset.Type.FWHM, fwhmInfo);
        
        //then
        verify(messenger).decision(eq(confirmMessage), anyListOf(String.class));
        verify(datasetManager).deleteDataset(Dataset.Type.FWHM, fwhmInfo);
        String successMessage = MessageFormat.format(bundle.getString("DELETE_DATASET_SUCCESS"), fwhmInfo.toString());
        verify(messenger).info(successMessage);
    }

    @Test
    public void deleteDataset_InUse() throws Exception {
        //given
        String confirmMessage = MessageFormat.format(bundle.getString("DELETE_DATASET_CONFIRM"), fwhmInfo.toString());
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("DELETE_OPTION"));
        List<String> inUseComponents = new ArrayList<String>();
        inUseComponents.add("Component1");
        inUseComponents.add("Component2");
        willThrow(new InUseException(inUseComponents)).given(datasetManager).deleteDataset(Dataset.Type.FWHM, fwhmInfo);
        
        //when
        DatasetListenerImpl datasetListener = new DatasetListenerImpl(datasetManager, messenger, uiManager, dataImporter, null, null);
        datasetListener.deleteDataset(Dataset.Type.FWHM, fwhmInfo);
        
        //then
        verify(messenger).decision(eq(confirmMessage), anyListOf(String.class));
        verify(datasetManager).deleteDataset(Dataset.Type.FWHM, fwhmInfo);
        String inUseMessage = MessageFormat.format(bundle.getString("DELETE_DATASET_IN_USE"), fwhmInfo.toString(), "\nComponent1\nComponent2");
        verify(messenger).error(inUseMessage);
        verify(messenger, never()).info(anyString());
    }

    @Test
    public void deleteDataset_NameNotFound() throws Exception {
        //given
        String confirmMessage = MessageFormat.format(bundle.getString("DELETE_DATASET_CONFIRM"), fwhmInfo.toString());
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("DELETE_OPTION"));
        willThrow(new NameNotFoundException()).given(datasetManager).deleteDataset(Dataset.Type.FWHM, fwhmInfo);
        
        //when
        DatasetListenerImpl datasetListener = new DatasetListenerImpl(datasetManager, messenger, uiManager, dataImporter, null, null);
        datasetListener.deleteDataset(Dataset.Type.FWHM, fwhmInfo);
        
        //then
        verify(messenger).decision(eq(confirmMessage), anyListOf(String.class));
        verify(datasetManager).deleteDataset(Dataset.Type.FWHM, fwhmInfo);
        String notFoundMessage = MessageFormat.format(bundle.getString("DELETE_DATASET_NOT_FOUND"), fwhmInfo.toString());
        verify(messenger).error(notFoundMessage);
        verify(messenger, never()).info(anyString());
    }

    @Test
    public void deleteDataset_DeviceError() throws Exception {
        //given
        String confirmMessage = MessageFormat.format(bundle.getString("DELETE_DATASET_CONFIRM"), fwhmInfo.toString());
        given(messenger.decision(eq(confirmMessage), anyListOf(String.class))).willReturn(bundle.getString("DELETE_OPTION"));
        willThrow(new PersistenceDeviceException()).given(datasetManager).deleteDataset(Dataset.Type.FWHM, fwhmInfo);
        
        //when
        DatasetListenerImpl datasetListener = new DatasetListenerImpl(datasetManager, messenger, uiManager, dataImporter, null, null);
        datasetListener.deleteDataset(Dataset.Type.FWHM, fwhmInfo);
        
        //then
        verify(messenger).decision(eq(confirmMessage), anyListOf(String.class));
        verify(datasetManager).deleteDataset(Dataset.Type.FWHM, fwhmInfo);
        String deviceErrorMessage = MessageFormat.format(bundle.getString("DELETE_DATASET_DEVICE_ERROR"), fwhmInfo.toString());
        verify(messenger).error(deviceErrorMessage);
        verify(messenger, never()).info(anyString());
    }

    @Test
    public void importFromFile_FileNotFound() throws Exception {
        //given
        File file = new File("path/file");
        given(messenger.requestInput(eq(bundle.getString("REQUEST_X_UNIT")), anyListOf(String.class))).willReturn("x");
        given(messenger.requestInput(eq(bundle.getString("REQUEST_Y_UNIT")), anyListOf(String.class))).willReturn("y");
        given(dataImporter.importDatasetFromComponentFile(file, Dataset.Type.FWHM, fwhmInfo)).willThrow(new WrongFormatException());
        given(dataImporter.importDatasetFromFile(file, Dataset.Type.FWHM, fwhmInfo, "x", "y", Dataset.DataType.FUNCTION)).willThrow(new FileNotFoundException());
        
        //when
        DatasetListenerImpl datasetListener = new DatasetListenerImpl(datasetManager, messenger, uiManager, dataImporter, null, null);
        datasetListener.importFromFile(file, Dataset.Type.FWHM, fwhmInfo, Dataset.DataType.FUNCTION);
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("IMPORT_FILE_NOT_FOUND"), "path/file"));
        verify(datasetManager, never()).saveDataset(any(Dataset.class));
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void importFromFile_IOProblem() throws Exception {
        //given
        File file = new File("path/file");
        given(messenger.requestInput(eq(bundle.getString("REQUEST_X_UNIT")), anyListOf(String.class))).willReturn("x");
        given(messenger.requestInput(eq(bundle.getString("REQUEST_Y_UNIT")), anyListOf(String.class))).willReturn("y");
        given(dataImporter.importDatasetFromComponentFile(file, Dataset.Type.FWHM, fwhmInfo)).willThrow(new WrongFormatException());
        given(dataImporter.importDatasetFromFile(file, Dataset.Type.FWHM, fwhmInfo, "x", "y", Dataset.DataType.FUNCTION)).willThrow(new IOException());
        
        //when
        DatasetListenerImpl datasetListener = new DatasetListenerImpl(datasetManager, messenger, uiManager, dataImporter, null, null);
        datasetListener.importFromFile(file, Dataset.Type.FWHM, fwhmInfo, Dataset.DataType.FUNCTION);
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("IMPORT_FILE_ERROR"), "path/file"));
        verify(datasetManager, never()).saveDataset(any(Dataset.class));
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void importFromFile_WrongFormat() throws Exception {
        //given
        File file = new File("path/file");
        given(messenger.requestInput(eq(bundle.getString("REQUEST_X_UNIT")), anyListOf(String.class))).willReturn("x");
        given(messenger.requestInput(eq(bundle.getString("REQUEST_Y_UNIT")), anyListOf(String.class))).willReturn("y");
        given(dataImporter.importDatasetFromComponentFile(file, Dataset.Type.FWHM, fwhmInfo)).willThrow(new WrongFormatException());
        given(dataImporter.importDatasetFromFile(file, Dataset.Type.FWHM, fwhmInfo, "x", "y", Dataset.DataType.FUNCTION)).willThrow(new WrongFormatException("wrong format message"));
        
        //when
        DatasetListenerImpl datasetListener = new DatasetListenerImpl(datasetManager, messenger, uiManager, dataImporter, null, null);
        datasetListener.importFromFile(file, Dataset.Type.FWHM, fwhmInfo, Dataset.DataType.FUNCTION);
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("IMPORT_FILE_FORMAT_ERROR"), "wrong format message"));
        verify(datasetManager, never()).saveDataset(any(Dataset.class));
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void importFromFile_NameExists() throws Exception {
        //given
        File file = new File("path/file");
        given(messenger.requestInput(eq(bundle.getString("REQUEST_X_UNIT")), anyListOf(String.class))).willReturn("x");
        given(messenger.requestInput(eq(bundle.getString("REQUEST_Y_UNIT")), anyListOf(String.class))).willReturn("y");
        given(dataImporter.importDatasetFromComponentFile(file, Dataset.Type.FWHM, fwhmInfo)).willThrow(new WrongFormatException());
        given(dataImporter.importDatasetFromFile(file, Dataset.Type.FWHM, fwhmInfo, "x", "y", Dataset.DataType.FUNCTION)).willReturn(fwhmDataset);
        willThrow(new NameExistsException()).given(datasetManager).saveDataset(fwhmDataset);
        
        //when
        DatasetListenerImpl datasetListener = new DatasetListenerImpl(datasetManager, messenger, uiManager, dataImporter, null, null);
        datasetListener.importFromFile(file, Dataset.Type.FWHM, fwhmInfo, Dataset.DataType.FUNCTION);
        
        //then
        verify(messenger).error(MessageFormat.format(bundle.getString("IMPORT_NAME_EXISTS"), fwhmInfo.toString()));
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void importFromFile_PersistenceError() throws Exception {
        //given
        File file = new File("path/file");
        given(messenger.requestInput(eq(bundle.getString("REQUEST_X_UNIT")), anyListOf(String.class))).willReturn("x");
        given(messenger.requestInput(eq(bundle.getString("REQUEST_Y_UNIT")), anyListOf(String.class))).willReturn("y");
        given(dataImporter.importDatasetFromComponentFile(file, Dataset.Type.FWHM, fwhmInfo)).willThrow(new WrongFormatException());
        given(dataImporter.importDatasetFromFile(file, Dataset.Type.FWHM, fwhmInfo, "x", "y", Dataset.DataType.FUNCTION)).willReturn(fwhmDataset);
        willThrow(new PersistenceDeviceException()).given(datasetManager).saveDataset(fwhmDataset);
        
        //when
        DatasetListenerImpl datasetListener = new DatasetListenerImpl(datasetManager, messenger, uiManager, dataImporter, null, null);
        datasetListener.importFromFile(file, Dataset.Type.FWHM, fwhmInfo, Dataset.DataType.FUNCTION);
        
        //then
        verify(messenger).error(bundle.getString("IMPORT_DEVICE_ERROR"));
        verify(uiManager, never()).sessionModified();
    }

    @Test
    public void importFromFile_Success() throws Exception {
        //given
        File file = new File("path/file");
        given(messenger.requestInput(eq(bundle.getString("REQUEST_X_UNIT")), anyListOf(String.class))).willReturn("x");
        given(messenger.requestInput(eq(bundle.getString("REQUEST_Y_UNIT")), anyListOf(String.class))).willReturn("y");
        given(dataImporter.importDatasetFromComponentFile(file, Dataset.Type.FWHM, fwhmInfo)).willThrow(new WrongFormatException());
        given(dataImporter.importDatasetFromFile(file, Dataset.Type.FWHM, fwhmInfo, "x", "y", Dataset.DataType.FUNCTION)).willReturn(fwhmDataset);
        
        //when
        DatasetListenerImpl datasetListener = new DatasetListenerImpl(datasetManager, messenger, uiManager, dataImporter, null, null);
        datasetListener.importFromFile(file, Dataset.Type.FWHM, fwhmInfo, Dataset.DataType.FUNCTION);
        
        //then
        verify(datasetManager).saveDataset(fwhmDataset);
        verify(instrument).setFwhm(fwhmInfo);
        verify(uiManager).sessionModified();
    }
}