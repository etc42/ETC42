/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.ui.swing.obsparam;

import java.util.ResourceBundle;
import javax.swing.JDialog;
import javax.swing.JFrame;
import org.cnrs.lam.dis.etc.datamodel.ComponentInfo;
import org.cnrs.lam.dis.etc.ui.ObsParamListener;
import org.cnrs.lam.dis.etc.ui.SessionHelper;
import org.cnrs.lam.dis.etc.ui.generic.ObsParamListenerHolder;
import org.cnrs.lam.dis.etc.ui.swing.TestingConstants;
import org.fest.swing.edt.GuiActionRunner;
import org.fest.swing.edt.GuiQuery;
import org.fest.swing.fixture.DialogFixture;
import org.fest.swing.fixture.FrameFixture;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.BDDMockito.*;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class SaveAsObsParamDialogTest {

    private FrameFixture window;
    @Mock ObsParamListener listener;
    private static ResourceBundle bundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/ui/swing/messages");

    @BeforeClass
    public static void setUpClass() throws Exception {
        Thread.sleep(100);
    }

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        
        SessionHelper.clearListeners();
        JFrame frame = GuiActionRunner.execute(new GuiQuery<JFrame>() {
            @Override
			protected JFrame executeInEDT() {
                SaveAsObsParamDialogFrame testFrame = new SaveAsObsParamDialogFrame();
                ObsParamListenerHolder.setObsParamListener(listener);
                return testFrame;
            }
        });
        window = new FrameFixture(frame);
        window.robot.settings().delayBetweenEvents(TestingConstants.FEST_DELAY_BETWEEN_EVENTS);
        window.robot.settings().eventPostingDelay(TestingConstants.FEST_EVENT_POSTING_DELAY);
        window.show(); // shows the frame to test
    }

    @After
    public void tearDown() {
        window.cleanUp();
    }

    @Test
    public void correctValuesOnLabels() {
        window.button("dialogButton").click();
        assertEquals(bundle.getString("SAVE_AS_OBS_PARAM_DIALOG_TITLE"), ((JDialog) window.dialog("dialog").component()).getTitle());
        window.dialog("dialog").label("nameLabel").requireText(bundle.getString("NAME"));
        window.dialog("dialog").label("descriptionLabel").requireText(bundle.getString("DESCRIPTION"));
        window.dialog("dialog").button("cancelButton").requireText(bundle.getString("CANCEL_BUTTON"));
        window.dialog("dialog").button("saveAsButton").requireText(bundle.getString("SAVE_AS_BUTTON"));
    }

    @Test
    public void cancelButtonClosesDialog() {
        //when
        window.button("dialogButton").click();
        DialogFixture dialog = window.dialog("dialog");
        dialog.button("cancelButton").click();
        
        //then
        dialog.requireNotVisible();
        verify(listener, never()).saveCurrentObsParamAs(any(ComponentInfo.class));
    }

    @Test
    public void cancelButtonClearsTheFields() {
        //when
        window.button("dialogButton").click();
        window.dialog("dialog").textBox("nameTextField").enterText("name");
        window.dialog("dialog").textBox("descriptionTextField").enterText("desc");
        window.dialog("dialog").button("cancelButton").click();
        window.button("dialogButton").click();
        
        //then
        window.dialog("dialog").textBox("nameTextField").requireEmpty();
        window.dialog("dialog").textBox("descriptionTextField").requireEmpty();
    }

    @Test
    public void saveAsButtonDoesNothingIfNameEnpty() {
        //when
        window.button("dialogButton").click();
        window.dialog("dialog").button("saveAsButton").click();
        window.dialog("dialog");
        
        //then
        verify(listener, never()).saveCurrentObsParamAs(any(ComponentInfo.class));
    }

    @Test
    public void saveAsButtonSuccess() {
        //when
        window.button("dialogButton").click();
        DialogFixture dialog = window.dialog("dialog");
        dialog.textBox("nameTextField").enterText("name");
        dialog.textBox("descriptionTextField").enterText("desc");
        dialog.button("saveAsButton").click();
        
        //then
        dialog.requireNotVisible();
        verify(listener).saveCurrentObsParamAs(new ComponentInfo("name", "desc"));
    }

    @Test
    public void saveAsButtonClearsTheFields() {
        //when
        window.button("dialogButton").click();
        window.dialog("dialog").textBox("nameTextField").enterText("name");
        window.dialog("dialog").textBox("descriptionTextField").enterText("desc");
        window.dialog("dialog").button("saveAsButton").click();
        window.button("dialogButton").click();
        
        //then
        window.dialog("dialog").textBox("nameTextField").requireEmpty();
        window.dialog("dialog").textBox("descriptionTextField").requireEmpty();
    }

}