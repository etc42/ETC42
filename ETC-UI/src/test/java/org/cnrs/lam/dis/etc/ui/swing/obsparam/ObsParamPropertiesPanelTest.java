/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.ui.swing.obsparam;

import org.cnrs.lam.dis.etc.ui.SessionHelper;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.JFrame;
import org.cnrs.lam.dis.etc.datamodel.Instrument;
import org.cnrs.lam.dis.etc.datamodel.ObsParam;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.cnrs.lam.dis.etc.ui.generic.SessionHolder;
import org.cnrs.lam.dis.etc.ui.swing.TestingConstants;
import org.fest.swing.edt.GuiActionRunner;
import org.fest.swing.edt.GuiQuery;
import org.fest.swing.fixture.FrameFixture;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.BDDMockito.*;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class ObsParamPropertiesPanelTest {

    private FrameFixture window;
    @Mock Session session;
    @Mock ObsParam obsParam;
    @Mock Instrument instrument;
    private static ResourceBundle bundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/ui/swing/messages");

    @BeforeClass
    public static void setUpClass() throws Exception {
        Thread.sleep(100);
    }

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        
        when(obsParam.getTimeSampleType()).thenReturn(ObsParam.TimeSampleType.DIT);
        when(obsParam.getFixedParameter()).thenReturn(ObsParam.FixedParameter.EXPOSURE_TIME);
        when(obsParam.getSpectralQuantumType()).thenReturn(ObsParam.SpectralQuantumType.SPECTRAL_RESOLUTION_ELEMENT);
        when(obsParam.getFixedSnrType()).thenReturn(ObsParam.FixedSnrType.TOTAL);
        obsParam.setFixedSnrType(ObsParam.FixedSnrType.TOTAL);
        
        when(instrument.getInstrumentType()).thenReturn(Instrument.InstrumentType.SPECTROGRAPH);
        
        when(session.getObsParam()).thenReturn(obsParam);
        when(session.getInstrument()).thenReturn(instrument);
        
        SessionHelper.clearListeners();
        JFrame frame = GuiActionRunner.execute(new GuiQuery<JFrame>() {
            @Override
			protected JFrame executeInEDT() {
                ObsParamPropertiesPanelFrame testFrame = new ObsParamPropertiesPanelFrame();
                SessionHelper.setSession(session);
                return testFrame;
            }
        });
        window = new FrameFixture(frame);
        window.robot.settings().delayBetweenEvents(TestingConstants.FEST_DELAY_BETWEEN_EVENTS);
        window.robot.settings().eventPostingDelay(TestingConstants.FEST_EVENT_POSTING_DELAY);
        window.show(); // shows the frame to test
    }

    @After
    public void tearDown() {
        window.cleanUp();
    }

    @Test
    public void correctValuesOnLabels() {
        window.panel("myPanel").radioButton("ditRadioButton").requireText(bundle.getString("DIT"));
        window.panel("myPanel").label("ditUnitLabel").requireText(session.getObsParam().getDitUnit());
        window.panel("myPanel").radioButton("noExpoRadioButton").requireText(bundle.getString("NO_EXPO"));
        window.panel("myPanel").label("fixedParameterLabel").requireText(bundle.getString("FIXED_PARAMETER"));
    }

    @Test
    public void comboBoxOptions() {
        List<String> expected = new ArrayList<String>();
        String[] actual = window.panel("myPanel").comboBox("fixedParameterComboBox").contents();
        assertArrayEquals(new String[] {bundle.getString("FIXED_SNR"), bundle.getString("FIXED_EXPOSURE_TIME")}, actual);
    }

    @Test
    public void testTimeSampleRadioButtons() {
        window.panel("myPanel").radioButton("ditRadioButton").click();
        window.panel("myPanel").textBox("ditTextField").requireEnabled();
        window.panel("myPanel").textBox("noExpoTextField").requireDisabled();
        window.panel("myPanel").radioButton("noExpoRadioButton").click();
        window.panel("myPanel").textBox("ditTextField").requireDisabled();
        window.panel("myPanel").textBox("noExpoTextField").requireEnabled();
        window.panel("myPanel").radioButton("ditRadioButton").click();
        window.panel("myPanel").textBox("ditTextField").requireEnabled();
        window.panel("myPanel").textBox("noExpoTextField").requireDisabled();
        window.panel("myPanel").radioButton("noExpoRadioButton").click();
        window.panel("myPanel").textBox("ditTextField").requireDisabled();
        window.panel("myPanel").textBox("noExpoTextField").requireEnabled();
    }

    @Test
    public void fixedParameterPanelShown() {
        window.panel("myPanel").comboBox("fixedParameterComboBox").selectItem(bundle.getString("FIXED_SNR"));
        window.panel("myPanel").panel("snrPanel");
        try {
            window.panel("myPanel").panel("exposureTimePanel");
            fail("Exposure time panel is visible");
        } catch (Exception e) {}
        window.panel("myPanel").comboBox("fixedParameterComboBox").selectItem(bundle.getString("FIXED_EXPOSURE_TIME"));
        try {
            window.panel("myPanel").panel("snrPanel");
            fail("SNR panel is visible");
        } catch (Exception e) {}
        window.panel("myPanel").panel("exposureTimePanel");
        window.panel("myPanel").comboBox("fixedParameterComboBox").selectItem(bundle.getString("FIXED_SNR"));
        window.panel("myPanel").panel("snrPanel");
        try {
            window.panel("myPanel").panel("exposureTimePanel");
            fail("Exposure time panel is visible");
        } catch (Exception e) {}
        window.panel("myPanel").comboBox("fixedParameterComboBox").selectItem(bundle.getString("FIXED_EXPOSURE_TIME"));
        try {
            window.panel("myPanel").panel("snrPanel");
            fail("SNR panel is visible");
        } catch (Exception e) {}
        window.panel("myPanel").panel("exposureTimePanel");
    }

    @Test
    public void timeSampleTypeUpdatedCorrectlyInSession() {
        //when
        window.panel("myPanel").radioButton("ditRadioButton").click();
        window.panel("myPanel").radioButton("noExpoRadioButton").click();
        window.panel("myPanel").radioButton("ditRadioButton").click();
        window.panel("myPanel").radioButton("noExpoRadioButton").click();
        
        //then
        InOrder inOrder = inOrder(obsParam);
        inOrder.verify(obsParam).setTimeSampleType(ObsParam.TimeSampleType.DIT);
        inOrder.verify(obsParam).setTimeSampleType(ObsParam.TimeSampleType.NO_EXPO);
        inOrder.verify(obsParam).setTimeSampleType(ObsParam.TimeSampleType.DIT);
        inOrder.verify(obsParam).setTimeSampleType(ObsParam.TimeSampleType.NO_EXPO);
    }

    @Test
    public void timeSampleTypeUpdatedCorrectlyInGUI() {
        given(obsParam.getTimeSampleType()).willReturn(ObsParam.TimeSampleType.DIT);
        SessionHolder.notifyForModification();
        window.panel("myPanel").radioButton("ditRadioButton").requireSelected();
        window.panel("myPanel").radioButton("noExpoRadioButton").requireNotSelected();
        window.panel("myPanel").textBox("ditTextField").requireEnabled();
        window.panel("myPanel").textBox("noExpoTextField").requireDisabled();

        given(obsParam.getTimeSampleType()).willReturn(ObsParam.TimeSampleType.NO_EXPO);
        SessionHolder.notifyForModification();
        window.panel("myPanel").radioButton("ditRadioButton").requireNotSelected();
        window.panel("myPanel").radioButton("noExpoRadioButton").requireSelected();
        window.panel("myPanel").textBox("ditTextField").requireDisabled();
        window.panel("myPanel").textBox("noExpoTextField").requireEnabled();

        given(obsParam.getTimeSampleType()).willReturn(ObsParam.TimeSampleType.DIT);
        SessionHolder.notifyForModification();
        window.panel("myPanel").radioButton("ditRadioButton").requireSelected();
        window.panel("myPanel").radioButton("noExpoRadioButton").requireNotSelected();
        window.panel("myPanel").textBox("ditTextField").requireEnabled();
        window.panel("myPanel").textBox("noExpoTextField").requireDisabled();

        given(obsParam.getTimeSampleType()).willReturn(ObsParam.TimeSampleType.NO_EXPO);
        SessionHolder.notifyForModification();
        window.panel("myPanel").radioButton("ditRadioButton").requireNotSelected();
        window.panel("myPanel").radioButton("noExpoRadioButton").requireSelected();
        window.panel("myPanel").textBox("ditTextField").requireDisabled();
        window.panel("myPanel").textBox("noExpoTextField").requireEnabled();
    }

    @Test
    public void ditUpdatedCorrectlyInSession() {
        //given
        window.panel("myPanel").radioButton("ditRadioButton").click();
        given(obsParam.getDit()).willReturn(10.);

        //when
        window.panel("myPanel").textBox("ditTextField").deleteText();
        window.panel("myPanel").textBox("ditTextField").enterText("1.5");
        window.panel("myPanel").radioButton("ditRadioButton").focus();
        
        window.panel("myPanel").textBox("ditTextField").deleteText();
        window.panel("myPanel").textBox("ditTextField").enterText("a");
        window.panel("myPanel").radioButton("ditRadioButton").focus();

        window.panel("myPanel").textBox("ditTextField").deleteText();
        window.panel("myPanel").textBox("ditTextField").enterText("3");
        window.panel("myPanel").radioButton("ditRadioButton").focus();

        window.panel("myPanel").textBox("ditTextField").deleteText();
        window.panel("myPanel").textBox("ditTextField").enterText("a");
        window.panel("myPanel").radioButton("ditRadioButton").focus();
        
        //then
        InOrder inOrder = inOrder(obsParam);
        inOrder.verify(obsParam).setDit(1.5);
        inOrder.verify(obsParam).setDit(0.);
        inOrder.verify(obsParam).setDit(3);
        // inOrder.verify(obsParam).setDit(0.);
    }

    @Test
    public void ditUpdatedCorrectlyInGUI() {
        given(obsParam.getTimeSampleType()).willReturn(ObsParam.TimeSampleType.DIT);
        
        given(obsParam.getDit()).willReturn(1.5);
        SessionHolder.notifyForModification();
        window.panel("myPanel").textBox("ditTextField").requireText("1.5");
        
        given(obsParam.getDit()).willReturn(0.);
        SessionHolder.notifyForModification();
        window.panel("myPanel").textBox("ditTextField").requireText("0.0");
        
        given(obsParam.getDit()).willReturn(10.);
        SessionHolder.notifyForModification();
        window.panel("myPanel").textBox("ditTextField").requireText("10.0");
    }

    @Test
    public void noExpoUpdatedCorrectlyInSession() {
        //given
        window.panel("myPanel").radioButton("noExpoRadioButton").click();
        given(obsParam.getNoExpo()).willReturn(10);

        //when
        window.panel("myPanel").textBox("noExpoTextField").deleteText();
        window.panel("myPanel").textBox("noExpoTextField").enterText("1");
        window.panel("myPanel").radioButton("noExpoRadioButton").focus();

        window.panel("myPanel").textBox("noExpoTextField").deleteText();
        window.panel("myPanel").textBox("noExpoTextField").enterText("a");
        window.panel("myPanel").radioButton("noExpoRadioButton").focus();

        window.panel("myPanel").textBox("noExpoTextField").deleteText();
        window.panel("myPanel").textBox("noExpoTextField").enterText("3");
        window.panel("myPanel").radioButton("noExpoRadioButton").focus();

        window.panel("myPanel").textBox("noExpoTextField").deleteText();
        window.panel("myPanel").textBox("noExpoTextField").enterText("1.5");
        window.panel("myPanel").radioButton("noExpoRadioButton").focus();
        
        //then
        InOrder inOrder = inOrder(obsParam);
        inOrder.verify(obsParam).setNoExpo(1);
        inOrder.verify(obsParam).setNoExpo(0);
        inOrder.verify(obsParam).setNoExpo(3);
        // inOrder.verify(obsParam).setNoExpo(0);
    }

    @Test
    public void noExpoUpdatedCorrectlyInGUI() {
        given(obsParam.getTimeSampleType()).willReturn(ObsParam.TimeSampleType.NO_EXPO);
        
        given(obsParam.getNoExpo()).willReturn(1);
        SessionHolder.notifyForModification();
        window.panel("myPanel").textBox("noExpoTextField").requireText("1");
        
        given(obsParam.getNoExpo()).willReturn(0);
        SessionHolder.notifyForModification();
        window.panel("myPanel").textBox("noExpoTextField").requireText("0");
        
        given(obsParam.getNoExpo()).willReturn(10);
        SessionHolder.notifyForModification();
        window.panel("myPanel").textBox("noExpoTextField").requireText("10");
    }

    @Test
    public void fixedParameterUpdatedCorrectlyInSession() {
        //when
        window.panel("myPanel").comboBox("fixedParameterComboBox").selectItem(bundle.getString("FIXED_SNR"));
        window.panel("myPanel").comboBox("fixedParameterComboBox").selectItem(bundle.getString("FIXED_EXPOSURE_TIME"));
        window.panel("myPanel").comboBox("fixedParameterComboBox").selectItem(bundle.getString("FIXED_SNR"));
        window.panel("myPanel").comboBox("fixedParameterComboBox").selectItem(bundle.getString("FIXED_EXPOSURE_TIME"));
        
        //then
        InOrder inOrder = inOrder(obsParam);
        inOrder.verify(obsParam).setFixedParameter(ObsParam.FixedParameter.SNR);
        inOrder.verify(obsParam).setFixedParameter(ObsParam.FixedParameter.EXPOSURE_TIME);
        inOrder.verify(obsParam).setFixedParameter(ObsParam.FixedParameter.SNR);
        inOrder.verify(obsParam).setFixedParameter(ObsParam.FixedParameter.EXPOSURE_TIME);
    }

    @Test
    public void fixedParameterUpdatedCorrectlyInGUI() {
        given(obsParam.getFixedParameter()).willReturn(ObsParam.FixedParameter.SNR);
        SessionHolder.notifyForModification();
        window.panel("myPanel").comboBox("fixedParameterComboBox").requireSelection(bundle.getString("FIXED_SNR"));
        window.panel("myPanel").panel("snrPanel");
        try {
            window.panel("myPanel").panel("exposureTimePanel");
            fail("Exposure time panel is visible");
        } catch (Exception e) {}
        
        given(obsParam.getFixedParameter()).willReturn(ObsParam.FixedParameter.EXPOSURE_TIME);
        SessionHolder.notifyForModification();
        window.panel("myPanel").comboBox("fixedParameterComboBox").requireSelection(bundle.getString("FIXED_EXPOSURE_TIME"));
        try {
            window.panel("myPanel").panel("snrPanel");
            fail("SNR panel is visible");
        } catch (Exception e) {}
        
        given(obsParam.getFixedParameter()).willReturn(ObsParam.FixedParameter.SNR);
        SessionHolder.notifyForModification();
        window.panel("myPanel").comboBox("fixedParameterComboBox").requireSelection(bundle.getString("FIXED_SNR"));
        window.panel("myPanel").panel("snrPanel");
        try {
            window.panel("myPanel").panel("exposureTimePanel");
            fail("Exposure time panel is visible");
        } catch (Exception e) {}
        
        given(obsParam.getFixedParameter()).willReturn(ObsParam.FixedParameter.EXPOSURE_TIME);
        SessionHolder.notifyForModification();
        window.panel("myPanel").comboBox("fixedParameterComboBox").requireSelection(bundle.getString("FIXED_EXPOSURE_TIME"));
        try {
            window.panel("myPanel").panel("snrPanel");
            fail("SNR panel is visible");
        } catch (Exception e) {}
    }

    @Test
    public void snrSubPanelGetsTheSessionFine() throws Exception {
        //given
        given(obsParam.getSnr()).willReturn(1.5);
        given(obsParam.getFixedParameter()).willReturn(ObsParam.FixedParameter.SNR);
        
        //when
        SessionHolder.notifyForModification();
        
        //then
        window.panel("myPanel").textBox("snrValueTextField").requireText("1.5");
        
        //when
        window.panel("myPanel").textBox("snrValueTextField").selectAll();
        window.panel("myPanel").textBox("snrValueTextField").enterText("10");
        window.panel("myPanel").comboBox("fixedParameterComboBox").focus();
        
        //then
        Thread.sleep(10);
        verify(obsParam).setSnr(10.);
    }

    @Test
    public void exposureTimeSubPanelGetsTheSessionFine() throws Exception {
        //given
        given(obsParam.getExposureTime()).willReturn(1.5);
        given(obsParam.getFixedParameter()).willReturn(ObsParam.FixedParameter.EXPOSURE_TIME);
        
        //when
        SessionHolder.notifyForModification();
        
        //then
        window.panel("myPanel").textBox("exposureTimeTextField").requireText("1.5");
        
        //when
        window.panel("myPanel").textBox("exposureTimeTextField").selectAll();
        window.panel("myPanel").textBox("exposureTimeTextField").enterText("10");
        window.panel("myPanel").radioButton("noExpoRadioButton").focus();
        
        //then
        Thread.sleep(10);
        verify(obsParam).setExposureTime(10.);
    }

}