/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.ui.swing.generic;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JTextField;
import org.cnrs.lam.dis.etc.ui.swing.TestingConstants;
import org.fest.swing.edt.GuiActionRunner;
import org.fest.swing.edt.GuiQuery;
import org.fest.swing.fixture.FrameFixture;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class IntegerTextFieldTest {

    private FrameFixture window;
    private IntegerTextField textField;
    private JTextField dummy;

    public IntegerTextFieldTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
        Thread.sleep(100);
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
        JFrame frame = GuiActionRunner.execute(new GuiQuery<JFrame>() {
            @Override
			protected JFrame executeInEDT() {
                textField = new IntegerTextField();
                textField.setName("integerTextField");
                dummy = new JTextField();
                dummy.setName("dummy");
                JFrame testFrame = new JFrame();
                testFrame.setMinimumSize(new Dimension(200, 200));
                testFrame.setLayout(new BorderLayout());
                testFrame.add(textField, BorderLayout.CENTER);
                testFrame.add(dummy, BorderLayout.SOUTH);
                return testFrame;
            }
        });
        window = new FrameFixture(frame);
        window.robot.settings().delayBetweenEvents(TestingConstants.FEST_DELAY_BETWEEN_EVENTS);
        window.robot.settings().eventPostingDelay(TestingConstants.FEST_EVENT_POSTING_DELAY);
        window.show(); // shows the frame to test
    }

    @After
    public void tearDown() {
        window.cleanUp();
    }

     @Test
     public void colorSwitches() {
         textField.setForeground(Color.GRAY);
         textField.setNormalForeground(Color.GRAY);
         textField.setErrorForeground(Color.PINK);

         window.textBox("integerTextField").foreground().requireEqualTo(Color.PINK);
         window.textBox("integerTextField").enterText("1");
         window.textBox("dummy").focus();
         window.textBox("integerTextField").foreground().requireEqualTo(Color.GRAY);
         window.textBox("integerTextField").deleteText();
         window.textBox("integerTextField").enterText("a");
         window.textBox("dummy").focus();
         window.textBox("integerTextField").foreground().requireEqualTo(Color.PINK);
         window.textBox("integerTextField").deleteText();
         window.textBox("integerTextField").enterText("3");
         window.textBox("dummy").focus();
         window.textBox("integerTextField").foreground().requireEqualTo(Color.GRAY);
         window.textBox("integerTextField").deleteText();
         window.textBox("integerTextField").enterText("1.5");
         window.textBox("dummy").focus();
         window.textBox("integerTextField").foreground().requireEqualTo(Color.PINK);
     }

     @Test
     public void flagSwitches() {
         window.textBox("integerTextField").enterText("1");
         window.textBox("dummy").focus();
         assertTrue(((IntegerTextField) window.textBox("integerTextField").component()).isCurrentValueValid());
         window.textBox("integerTextField").deleteText();
         window.textBox("integerTextField").enterText("a");
         window.textBox("dummy").focus();
         assertFalse(((IntegerTextField) window.textBox("integerTextField").component()).isCurrentValueValid());
         window.textBox("integerTextField").deleteText();
         window.textBox("integerTextField").enterText("3");
         window.textBox("dummy").focus();
         assertTrue(((IntegerTextField) window.textBox("integerTextField").component()).isCurrentValueValid());
         window.textBox("integerTextField").deleteText();
         window.textBox("integerTextField").enterText("1.5");
         window.textBox("dummy").focus();
         assertFalse(((IntegerTextField) window.textBox("integerTextField").component()).isCurrentValueValid());
     }

     @Test
     public void correctValuesAreReturned() {
         textField.setDefaultValue(5);

         window.textBox("integerTextField").enterText("1");
         window.textBox("dummy").focus();
         assertEquals(1, ((IntegerTextField) window.textBox("integerTextField").component()).getValue());
         window.textBox("integerTextField").deleteText();
         window.textBox("integerTextField").enterText("a");
         window.textBox("dummy").focus();
         assertEquals(5, ((IntegerTextField) window.textBox("integerTextField").component()).getValue());
         window.textBox("integerTextField").deleteText();
         window.textBox("integerTextField").enterText("10");
         window.textBox("dummy").focus();
         assertEquals(10, ((IntegerTextField) window.textBox("integerTextField").component()).getValue());
         window.textBox("integerTextField").deleteText();
         window.textBox("integerTextField").enterText("1.5");
         window.textBox("dummy").focus();
         assertEquals(5, ((IntegerTextField) window.textBox("integerTextField").component()).getValue());
     }

}