/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.ui.swing.obsparam;

import org.cnrs.lam.dis.etc.ui.SessionHelper;
import org.cnrs.lam.dis.etc.ui.generic.SessionHolder;
import org.cnrs.lam.dis.etc.datamodel.Instrument;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.JFrame;
import org.cnrs.lam.dis.etc.datamodel.ComponentInfo;
import org.cnrs.lam.dis.etc.datamodel.ObsParam;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.cnrs.lam.dis.etc.ui.InfoProvider;
import org.cnrs.lam.dis.etc.ui.ObsParamListener;
import org.cnrs.lam.dis.etc.ui.generic.InfoProviderHolder;
import org.cnrs.lam.dis.etc.ui.generic.ObsParamListenerHolder;
import org.cnrs.lam.dis.etc.ui.swing.TestingConstants;
import org.fest.swing.edt.GuiActionRunner;
import org.fest.swing.edt.GuiQuery;
import org.fest.swing.fixture.DialogFixture;
import org.fest.swing.fixture.FrameFixture;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.BDDMockito.*;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class ObsParamPanelTest {

    private FrameFixture window;
    @Mock Session session;
    @Mock ObsParam obsParam;
    @Mock Instrument instrument;
    @Mock ObsParamListener obsParamListener;
    @Mock InfoProvider infoProvider;
    ResourceBundle bundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/ui/swing/messages");

    @BeforeClass
    public static void setUpClass() throws Exception {
        Thread.sleep(100);
    }

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        
        List<ComponentInfo> obsParamList = new ArrayList<ComponentInfo>();
        obsParamList.add(new ComponentInfo("ObsParam1", "Description1"));
        obsParamList.add(new ComponentInfo("ObsParam2", "Description2"));
        obsParamList.add(new ComponentInfo("ObsParam3", "Description3"));
        when(infoProvider.getAvailableObsParamList()).thenReturn(obsParamList);
        
        ComponentInfo info = infoProvider.getAvailableObsParamList().get(0);
        when(obsParam.getInfo()).thenReturn(info);
        when(obsParam.getTimeSampleType()).thenReturn(ObsParam.TimeSampleType.DIT);
        when(obsParam.getFixedParameter()).thenReturn(ObsParam.FixedParameter.EXPOSURE_TIME);
        when(obsParam.getSpectralQuantumType()).thenReturn(ObsParam.SpectralQuantumType.SPECTRAL_RESOLUTION_ELEMENT);
        when(obsParam.getExtraSignalType()).thenReturn(ObsParam.ExtraSignalType.ONLY_CALCULATED_SIGNAL);
        when(obsParam.getExtraBackgroundNoiseType()).thenReturn(ObsParam.ExtraBackgroundNoiseType.ONLY_CALCULATED_BACKGROUND_NOISE);
        when(obsParam.getFixedSnrType()).thenReturn(ObsParam.FixedSnrType.TOTAL);
        
        when(instrument.getInfo()).thenReturn(new ComponentInfo("instrument", "desc"));
        when(instrument.getInstrumentType()).thenReturn(Instrument.InstrumentType.SPECTROGRAPH);
        
        when(session.getObsParam()).thenReturn(obsParam);
        when(session.getInstrument()).thenReturn(instrument);
        
        SessionHelper.clearListeners();
        JFrame frame = GuiActionRunner.execute(new GuiQuery<JFrame>() {
            @Override
            protected JFrame executeInEDT() {
                ObsParamPanelFrame testFrame = new ObsParamPanelFrame();
                InfoProviderHolder.setInfoProvider(infoProvider);
                ObsParamListenerHolder.setObsParamListener(obsParamListener);
                SessionHelper.setSession(session);
                return testFrame;
            }
        });
        window = new FrameFixture(frame);
        window.robot.settings().delayBetweenEvents(TestingConstants.FEST_DELAY_BETWEEN_EVENTS);
        window.robot.settings().eventPostingDelay(TestingConstants.FEST_EVENT_POSTING_DELAY);
        window.show(); // shows the frame to test
    }

    @After
    public void tearDown() {
        window.cleanUp();
    }

    @Test
    public void correctValuesOnLabels() {
        window.panel("myPanel").label("obsParamLabel").requireText(bundle.getString("OBS_PARAM"));
        window.panel("myPanel").label("choiceLabel").requireText(bundle.getString("OBS_PARAM_CHOICE"));
        window.panel("myPanel").label("descriptionLabel").requireText(bundle.getString("DESCRIPTION"));
        window.panel("myPanel").button("newButton").requireText(bundle.getString("NEW_BUTTON"));
        window.panel("myPanel").button("reloadButton").requireText(bundle.getString("RELOAD_BUTTON"));
        window.panel("myPanel").button("saveButton").requireText(bundle.getString("SAVE_BUTTON"));
        window.panel("myPanel").button("saveAsButton").requireText(bundle.getString("SAVE_AS_BUTTON"));
    }

    @Test
    public void checkCorrectObsParamList() {
        List<String> expected = new ArrayList<String>();
        for (ComponentInfo info : infoProvider.getAvailableObsParamList()) {
            expected.add(" " + info.getName() + " : " + info.getDescription());
        }
        assertArrayEquals(expected.toArray(), window.panel("myPanel").comboBox("choiceComboBox").contents());
    }

    @Test
    public void nameUpdatedCorrectlyFromSession() {
        //given
        ComponentInfo info = infoProvider.getAvailableObsParamList().get(1);
        given(obsParam.getInfo()).willReturn(info);

        //when
        SessionHolder.notifyForModification();

        //then
        String expected = " " + obsParam.getInfo().getName() + " : " + obsParam.getInfo().getDescription();
        window.panel("myPanel").comboBox("choiceComboBox").requireSelection(expected);
        window.panel("myPanel").textBox("descriptionTextField").requireText(obsParam.getInfo().getDescription());
        
        verify(obsParamListener, never()).createNewObsParam(any(ComponentInfo.class));
        verify(obsParamListener, never()).loadObsParam(any(ComponentInfo.class));
        verify(obsParamListener, never()).reloadCurrentObsParam();
        verify(obsParamListener, never()).saveCurrentObsParamAs(any(ComponentInfo.class));
        verify(obsParamListener, never()).saveCurrentObsParam();
    }

    @Test
    public void sessionIsPassedToTheSubPanel() {
        assertNotNull(((ObsParamPropertiesPanel) window.panel("obsParamPropertiesPanel").component()).getSession());
    }

    @Test
    public void loadObsParamWhenSelectedInComboBox() {
        //when
        window.panel("myPanel").comboBox("choiceComboBox").selectItem(1);

        //then
        ComponentInfo info = infoProvider.getAvailableObsParamList().get(1);
        verify(obsParamListener, atLeastOnce()).loadObsParam(info);
        
        verify(obsParamListener, never()).createNewObsParam(any(ComponentInfo.class));
        verify(obsParamListener, never()).reloadCurrentObsParam();
        verify(obsParamListener, never()).saveCurrentObsParamAs(any(ComponentInfo.class));
        verify(obsParamListener, never()).saveCurrentObsParam();
    }

    @Test
    public void ignoreLoadObsParamWhenSelectedTheSame() {
        //when
        window.panel("myPanel").comboBox("choiceComboBox").selectItem(0);

        //then
        verify(obsParamListener, never()).createNewObsParam(any(ComponentInfo.class));
        verify(obsParamListener, never()).loadObsParam(any(ComponentInfo.class));
        verify(obsParamListener, never()).reloadCurrentObsParam();
        verify(obsParamListener, never()).saveCurrentObsParamAs(any(ComponentInfo.class));
        verify(obsParamListener, never()).saveCurrentObsParam();
    }

    @Test
    public void checkNewButtonWorks() {
        //when
        window.button("newButton").click();
        DialogFixture dialog = window.dialog("newDialog");
        dialog.textBox("nameTextField").enterText("newName");
        dialog.textBox("descriptionTextField").enterText("newDesc");
        dialog.button("createButton").click();
        
        //then
        dialog.requireNotVisible();
        verify(obsParamListener).createNewObsParam(new ComponentInfo("newName", "newDesc"));

        verify(obsParamListener, never()).loadObsParam(any(ComponentInfo.class));
        verify(obsParamListener, never()).reloadCurrentObsParam();
        verify(obsParamListener, never()).saveCurrentObsParamAs(any(ComponentInfo.class));
        verify(obsParamListener, never()).saveCurrentObsParam();
    }

    @Test
    public void checkReloadButtonWorks() throws Exception {
        //when
        window.button("reloadButton").click();

        //then
        Thread.sleep(10);
        verify(obsParamListener).reloadCurrentObsParam();
        
        verify(obsParamListener, never()).createNewObsParam(any(ComponentInfo.class));
        verify(obsParamListener, never()).loadObsParam(any(ComponentInfo.class));
        verify(obsParamListener, never()).saveCurrentObsParamAs(any(ComponentInfo.class));
        verify(obsParamListener, never()).saveCurrentObsParam();
    }

    @Test
    public void checkSaveButtonWorks() {
        //when
        window.button("saveButton").click();

        //then
        verify(obsParamListener).saveCurrentObsParam();
        
        verify(obsParamListener, never()).createNewObsParam(any(ComponentInfo.class));
        verify(obsParamListener, never()).loadObsParam(any(ComponentInfo.class));
        verify(obsParamListener, never()).reloadCurrentObsParam();
        verify(obsParamListener, never()).saveCurrentObsParamAs(any(ComponentInfo.class));
    }

    @Test
    public void checkSaveAsButtonWorks() {
        //when
        window.button("saveAsButton").click();
        window.dialog("saveAsDialog").textBox("nameTextField").enterText("newName");
        window.dialog("saveAsDialog").textBox("descriptionTextField").enterText("newDesc");
        window.dialog("saveAsDialog").button("saveAsButton").click();

        //then
        verify(obsParamListener).saveCurrentObsParamAs(new ComponentInfo("newName", "newDesc"));

        verify(obsParamListener, never()).createNewObsParam(any(ComponentInfo.class));
        verify(obsParamListener, never()).loadObsParam(any(ComponentInfo.class));
        verify(obsParamListener, never()).reloadCurrentObsParam();
        verify(obsParamListener, never()).saveCurrentObsParam();
    }
}