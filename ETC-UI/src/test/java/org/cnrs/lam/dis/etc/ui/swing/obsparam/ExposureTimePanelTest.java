/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.ui.swing.obsparam;

import org.cnrs.lam.dis.etc.ui.SessionHelper;
import org.cnrs.lam.dis.etc.ui.generic.SessionHolder;
import java.util.ResourceBundle;
import javax.swing.JFrame;
import org.cnrs.lam.dis.etc.datamodel.ObsParam;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.cnrs.lam.dis.etc.ui.swing.TestingConstants;
import org.cnrs.lam.dis.etc.ui.swing.UserMode;
import org.fest.swing.edt.GuiActionRunner;
import org.fest.swing.edt.GuiQuery;
import org.fest.swing.fixture.FrameFixture;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.mockito.BDDMockito.*;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class ExposureTimePanelTest {

    private FrameFixture window;
    private static ResourceBundle bundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/ui/swing/messages");
    @Mock ObsParam obsParam;
    @Mock Session session;

    public ExposureTimePanelTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
        Thread.sleep(100);
    }

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        
        when(session.getObsParam()).thenReturn(obsParam);
        
        SessionHelper.clearListeners();
        JFrame frame = GuiActionRunner.execute(new GuiQuery<JFrame>() {
            @Override
			protected JFrame executeInEDT() {
                ExposureTimePanelFrame testFrame = new ExposureTimePanelFrame();
                SessionHelper.setSession(session);
                return testFrame;
            }
        });
        window = new FrameFixture(frame);
        window.robot.settings().delayBetweenEvents(TestingConstants.FEST_DELAY_BETWEEN_EVENTS);
        window.robot.settings().eventPostingDelay(TestingConstants.FEST_EVENT_POSTING_DELAY);
        window.show(); // shows the frame to test
    }

    @After
    public void tearDown() {
        window.cleanUp();
    }

    @Test
    public void correctValuesOnLabels() {
        window.panel("myPanel").label("exposureTimeLabel").requireText(bundle.getString("OBS_PARAM_EXPOSURE_TIME"));
        window.panel("myPanel").label("exposureTimeUnitLabel").requireText(session.getObsParam().getExposureTimeUnit());
    }

    @Test
    public void exposureTimeUpdatedCorrectlyInSession() {
        //given
        given(obsParam.getExposureTime()).willReturn(10.);

        //when
        window.panel("myPanel").textBox("exposureTimeTextField").deleteText();
        window.panel("myPanel").textBox("exposureTimeTextField").enterText("1.5");
        window.textBox("dummy").focus();
        
        window.panel("myPanel").textBox("exposureTimeTextField").deleteText();
        window.panel("myPanel").textBox("exposureTimeTextField").enterText("a");
        window.textBox("dummy").focus();
        
        window.panel("myPanel").textBox("exposureTimeTextField").deleteText();
        window.panel("myPanel").textBox("exposureTimeTextField").enterText("1.5");
        window.textBox("dummy").focus();
        
        window.panel("myPanel").textBox("exposureTimeTextField").deleteText();
        window.panel("myPanel").textBox("exposureTimeTextField").enterText("a");
        window.textBox("dummy").focus();
        
        //then
        InOrder inOrder = inOrder(obsParam);
        inOrder.verify(obsParam).setExposureTime(1.5);
        inOrder.verify(obsParam).setExposureTime(0.);
        inOrder.verify(obsParam).setExposureTime(1.5);
        // inOrder.verify(obsParam).setExposureTime(0.);
    }

    @Test
    public void exposureTimeUpdatedCorrectlyInGUI() {
        given(obsParam.getExposureTime()).willReturn(1.5);
        SessionHolder.notifyForModification();
        window.panel("myPanel").textBox("exposureTimeTextField").requireText("1.5");
        
        given(obsParam.getExposureTime()).willReturn(0.);
        SessionHolder.notifyForModification();
        window.panel("myPanel").textBox("exposureTimeTextField").requireText("0.0");
        
        given(obsParam.getExposureTime()).willReturn(10.);
        SessionHolder.notifyForModification();
        window.panel("myPanel").textBox("exposureTimeTextField").requireText("10.0");
    }

    @Test
    public void userModes() {
        ((ExposureTimePanel) window.panel("myPanel").component()).setUserMode(UserMode.EXPERT);
        window.panel("myPanel").textBox("exposureTimeTextField").requireEditable();
        ((ExposureTimePanel) window.panel("myPanel").component()).setUserMode(UserMode.BEGINNER);
        window.panel("myPanel").textBox("exposureTimeTextField").requireNotEditable();
    }
}