/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.ihm;

import java.awt.*;
import java.util.Vector;

import javax.swing.*;

// Import from org.jfree

import org.cnrs.lam.dis.etc.ihm.tools.ETCXYSeries;
import org.cnrs.lam.dis.etc.ihm.tools.EtcChartPanel;
import org.cnrs.lam.dis.etc.ihm.tools.EtcTabbedPane;
import org.cnrs.lam.dis.etc.ihm.tools.IncorrectParentException;
import org.jfree.chart.*;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeriesCollection;


/**
 *
 * @author  distag
 */
public class PlotPanel extends JPanel {
    
    private static final long serialVersionUID = 1L;
    //private int nombreGraphResult;
    //To_Plotter mySampleChart = new To_Plotter();
    private EtcTabbedPane PlotTabedPane;
    private JTabbedPane globalTabPane;
    private LogPanel logPanel;
    
    public JTabbedPane getGlobalTabPane() {
        return globalTabPane;
    }

    /** Creates new form PlotPanel */
    public PlotPanel() {
        
    	initComponents();
        
    }
    /**
     * Initialisation du conteneur d'onglets pour les graphs
     */
    private void initComponents()
    {
    	globalTabPane = new JTabbedPane();
        PlotTabedPane = new EtcTabbedPane();
        logPanel = new LogPanel();
        globalTabPane.addTab("Graphics", PlotTabedPane);
        globalTabPane.addTab("Command history", logPanel);
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(globalTabPane, GroupLayout.DEFAULT_SIZE, 376, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(globalTabPane, GroupLayout.DEFAULT_SIZE, 276, Short.MAX_VALUE)
                .addContainerGap())
        );
    }
    
   
    // ***********-*----*-**-**-*-*-*-*-*-*-*-*
    // Méthode créées par GL de maniére à restructurer les principes de créations de graph
    
    /**
	 * This function returns the Exponent used to express the double
	 * @param a double xEy
	 * @return y with 1<x<10
	 */ 
    public int getExposant(double in){
		
		double absIn = Math.abs(in);
    	int out = (int)Math.log10(absIn);
		//if (out < 0 )
		return out<0?out-1:out;
		
	}
    
    /**
     * Méthode principale de création de graph
     * @param onglet nom que prend l'onglet
     * @param titre titre que prend le graph visible au dessus du graph
     * @param nomAxeX nom que prend l'axe horizontal
     * @param nomAxeY nom que prend l'axr vertical
     * @param x valeurs de X
     * @param y valeurs de Y
     */
    public void makeAnyGraph(String onglet, String titre, String nomAxeX, String nomAxeY, double[] x, double[] y) throws IncorrectParentException
    {
    	
    	double moyenneY = 0.0;
		double coefOK = 1.0;
		for (int i = 0 ; i < y.length ; i++ )
		{
			moyenneY += y[i];
		}
		moyenneY = moyenneY / y.length;
		int coefY = getExposant(moyenneY);
		
		if (coefY < 1E-3 && coefY!=0)
		{
		nomAxeY = "E" + coefY + " " + nomAxeY;
		coefOK = Double.valueOf("1E"+coefY);
		}
		
		for (int i = 0 ; i < y.length ; i++ )
		{
			y[i] = y[i] / coefOK;
		}
    	
    	ButtonTabComponent JButton = new ButtonTabComponent(PlotTabedPane); 
    	XYDataset xydataset = makeDataset(onglet,x,y,titre,nomAxeX,nomAxeY,coefOK);
    	JFreeChart jfreechart = makeGraph(xydataset,titre, nomAxeX, nomAxeY);
    	
    	EtcChartPanel chartpanel = new EtcChartPanel(jfreechart);
        chartpanel.setPreferredSize(new Dimension(500, 270));
        PlotTabedPane.addTab(onglet, chartpanel);
        PlotTabedPane.setTabComponentAt(PlotTabedPane.getTabCount()-1, JButton);
        PlotTabedPane.setSelectedIndex(PlotTabedPane.getTabCount()-1);

    }
 
    /**
     *  Création de Dataset utilisés pour créér les graphs
     * @param x 
     * @param y
     * @return
     */
    public XYDataset makeDataset(String nom,double[] x, double[] y, String typeSerie, String unitX, String unitY, double coefY)
    {
    	ETCXYSeries xyseries = new ETCXYSeries(nom,typeSerie,unitX,unitY,coefY);
    	for (int n = 0; n < x.length ; n++)
    	{
    		xyseries.add(x[n], y[n]);
    	}
    	
    	return new XYSeriesCollection(xyseries);
    }
    
    /**
     *  Création de Dataset utilisés pour créér les graphs
     *  avec plusieurs courbes. On suppose que tous les tableaux ont la meme taille
     * @param x 
     * @param y
     * @return
     */
    public XYDataset makeMultiDataset(String [] legends, Vector<double[]> coordsX, Vector<double[]> coordsY,
    		String unitX, String unitY){
    	 Vector<ETCXYSeries>  data=new Vector<ETCXYSeries>(legends.length);
    	
    	for (int i = 0; i < legends.length; i++) {
    		// Suite a evolution de l'objet ETCXYSeries avec un coefY
    		// Ajout ici de coefY = 1 (donc neutre)
    		// Meilleur choix possible pour du multiplot ...
    		data.add(new ETCXYSeries(legends[i],"",unitX,unitY,1.0));
		}
    	
    	for (int i = 0; i < data.capacity(); i++) {
    		for (int j = 0; j < coordsX.elementAt(0).length; j++) {
    			data.elementAt(i).add(coordsX.elementAt(i)[j], coordsY.elementAt(i)[j]);
			}
			
		}
    	

    	XYSeriesCollection result=new XYSeriesCollection();
    	for (int i = 0; i < data.capacity(); i++) {
    		result.addSeries(data.elementAt(i));
    	}
    	return result;
    }
   
    /**
     * Méthode de création de graph a partir de dataset (eventuellement, avec plusieurs series)
     * @param onglet nom que prend l'onglet
     * @param titre titre que prend le graph visible au dessus du graph
     * @param nomAxeX nom que prend l'axe horizontal
     * @param nomAxeY nom que prend l'axr vertical
     */
    public void makeAnyGraphFromDataSet(String onglet, String titre, String nomAxeX, String nomAxeY,  XYDataset ds) throws IncorrectParentException
    {
    	
    	ButtonTabComponent JButton = new ButtonTabComponent(PlotTabedPane); 
    	JFreeChart jfreechart = makeGraph(ds,titre, nomAxeX, nomAxeY);
    	
    	EtcChartPanel chartpanel = new EtcChartPanel(jfreechart);
        chartpanel.setPreferredSize(new Dimension(500, 270));
        PlotTabedPane.addTab(onglet, chartpanel);
        PlotTabedPane.setTabComponentAt(PlotTabedPane.getTabCount()-1, JButton);
        PlotTabedPane.setSelectedIndex(PlotTabedPane.getTabCount()-1);

    }
    
    
    /**
     * Methode de création de graph avec des constantes fixées en dur 
     * (Orientation verticale, legende true, tooltips true, urls false)
     */
    public static JFreeChart makeGraph(XYDataset xyd, String titre, String axeX, String axeY)
    {
    	JFreeChart jfreechart = ChartFactory.createXYLineChart(
    			titre, 
    			axeX,
    			axeY,
    			xyd,
    			PlotOrientation.VERTICAL,
    			true,
    			true,
    			false);
    	XYPlot xyplot = (XYPlot)jfreechart.getPlot();        
	    xyplot.setDomainCrosshairVisible(true);	
	    xyplot.setRangeCrosshairVisible(true);
    	
    	return jfreechart;
    }
 
    
    public LogPanel getLogOut(){
    	return this.logPanel;
    }
    
    public EtcTabbedPane getPlotTabedPane() {
		return PlotTabedPane;
	}
    
    // Fin GL *******************************************
    
}
