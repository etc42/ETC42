/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.ui.swing.result;

import cds.savot.model.SavotVOTable;
import cds.savot.pull.SavotPullEngine;
import cds.savot.pull.SavotPullParser;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.net.URI;
import javax.swing.TransferHandler;
import org.apache.log4j.Logger;
import org.cnrs.lam.cesam.vo.dnd.VoDataFlavors;
import org.cnrs.lam.cesam.vo.dnd.VoTable1DHelper;
import org.cnrs.lam.cesam.vo.dnd.VoTable1DSetHelper;
import org.cnrs.lam.cesam.vo.dnd.VoTableHolder;
import org.cnrs.lam.cesam.vo.dnd.components.VoTable1DSetChartPanel;
import org.cnrs.lam.cesam.vo.dnd.components.VoTableChartDataChangeListener;
import org.cnrs.lam.dis.etc.ui.swing.generic.ClosableTabComponent;
import org.javatuples.Pair;

/**
 */
public class ChartsTabbedPane extends LogoTabbedPane {
    
    private static final Logger logger = Logger.getLogger(ChartsTabbedPane.class);

    public ChartsTabbedPane() {
        this.setTransferHandler(new PanelTransferHandler());
    }
    
    public void addChart(String title, SavotVOTable voTable, File voTableFile) {
        VoTable1DSetChartPanel chartPanel = new VoTable1DSetChartPanel();
        chartPanel.addVoTable(voTable, voTableFile);
        chartPanel.addDataChangeListener(new CloseWhenEmptyListener(chartPanel, this));
        add(title, chartPanel);
        ClosableTabComponent tabComponent = new ClosableTabComponent(this, title);
        tabComponent.setTransferHandler(new TabTransferHandler(chartPanel));
        int index = getTabCount() - 1;
        setTabComponentAt(index, tabComponent);
        setSelectedIndex(index);
    }
    
    public void addSetChart(String title, SavotVOTable voTable, File voTableFile, Number selected) {
        VoTable1DSetChartPanel chartPanel = new VoTable1DSetChartPanel();
        chartPanel.addVoTable(voTable, voTableFile, selected);
        chartPanel.addDataChangeListener(new CloseWhenEmptyListener(chartPanel, this));
        add(title, chartPanel);
        ClosableTabComponent tabComponent = new ClosableTabComponent(this, title);
        tabComponent.setTransferHandler(new TabTransferHandler(chartPanel));
        int index = getTabCount() - 1;
        setTabComponentAt(index, tabComponent);
        setSelectedIndex(index);
    }
    
    private class CloseWhenEmptyListener implements VoTableChartDataChangeListener {
        
        private VoTable1DSetChartPanel cartPanel;
        private ChartsTabbedPane tabbedPane;

        public CloseWhenEmptyListener(VoTable1DSetChartPanel cartPanel, ChartsTabbedPane tabbedPane) {
            this.cartPanel = cartPanel;
            this.tabbedPane = tabbedPane;
        }

        @Override
        public void voTableAdded(SavotVOTable voTable) {
            // We do nothing here
        }

        @Override
        public void voTableRemoved(SavotVOTable voTable) {
            // We do nothing here
        }

        @Override
        public void lastVoTableRemoved() {
            tabbedPane.remove(cartPanel);
        }

        @Override
        public void voTableSetSelectionChanged(SavotVOTable voTable, Number newSelected) {
            // We do nothing here
        }
    }
    
    /**
     * This class is responsible for handling drops of VOTables representing sets of
     * or single 1D datasets, by adding them to the chart.
     */
    private class TabTransferHandler extends TransferHandler {
        
        private VoTable1DSetChartPanel chartPanel;

        public TabTransferHandler(VoTable1DSetChartPanel chartPanel) {
            this.chartPanel = chartPanel;
        }
        
        @Override
        public boolean canImport(TransferSupport support) {
            if (chartPanel.isDragHappening()) {
                return false;
            }
            for (DataFlavor dataFlavor : support.getTransferable().getTransferDataFlavors()) {
                if (VoDataFlavors.voTableUriFlavor.isMimeTypeEqual(dataFlavor)) {
                    String dataTypeParam = dataFlavor.getParameter("data-type");
                    if (VoDataFlavors.VoTableDataType.TABLE_1D.toString().equals(dataTypeParam)
                            || VoDataFlavors.VoTableDataType.TABLE_1D_SET.toString().equals(dataTypeParam)) {
                        return true;
                    }
                }
            }
            return false;
        }

        @Override
        public boolean importData(TransferSupport support) {
            try {
                Transferable transferable = support.getTransferable();
                // We get the VOTable and its file
                SavotVOTable voTable = null;
                File voTableFile = null;
                // If we are at the same JVM we might can get the instances from the holder
                if (transferable.isDataFlavorSupported(VoDataFlavors.voTableHolderKeyFlavor)) {
                    VoTableHolder.Key holderKey = (VoTableHolder.Key) transferable.getTransferData(VoDataFlavors.voTableHolderKeyFlavor);
                    Pair<SavotVOTable, File> retrievedPair = VoTableHolder.getInstance().retrieve(holderKey);
                    if (retrievedPair != null) {
                        voTable = retrievedPair.getValue0();
                        voTableFile = retrievedPair.getValue1();
                    }
                }
                // If the voTableFile is still not set we retrieve its URL from
                // the transferable
                if (voTableFile == null) {
                    BufferedReader reader = new BufferedReader(VoDataFlavors.voTableUriFlavor.getReaderForText(transferable));
                    String uriString = reader.readLine();
                    URI uri = new URI(uriString);
                    voTableFile = new File(uri);
                }
                // Now if we reached here and the voTable is null means that we
                // haven't retrieve it from the holder, so we read it from the
                // file
                if (voTable == null) {
                    SavotPullParser parser = new SavotPullParser(new FileInputStream(voTableFile), SavotPullEngine.FULL, null);
                    voTable = parser.getVOTable();
                }
                DataFlavor flavor = null;
                for (DataFlavor dataFlavor : transferable.getTransferDataFlavors()) {
                    if (VoDataFlavors.voTableUriFlavor.isMimeTypeEqual(dataFlavor)) {
                        flavor = dataFlavor;
                    }
                }
                VoDataFlavors.VoTableDataType dataType = null;
                String dataTypeParam = flavor.getParameter("data-type");
                if (VoDataFlavors.VoTableDataType.TABLE_1D.toString().equals(dataTypeParam)) {
                    dataType = VoDataFlavors.VoTableDataType.TABLE_1D;
                }
                if (VoDataFlavors.VoTableDataType.TABLE_1D_SET.toString().equals(dataTypeParam)) {
                    dataType = VoDataFlavors.VoTableDataType.TABLE_1D_SET;
                }
                switch (dataType) {
                    case TABLE_1D:
                        chartPanel.addVoTable(voTable, voTableFile);
                        break;
                    case TABLE_1D_SET:
                        String selectedParam = flavor.getParameter("selected-value");
                        Number selected = null;
                        if (selectedParam != null) {
                            selected = VoTable1DSetHelper.convertZValue(voTable, selectedParam);
                        }
                        chartPanel.addVoTable(voTable, voTableFile, selected);
                        break;
                }
            } catch (Exception ex) {
                logger.error("Problem when retrieving the VOTable during drag and drop", ex);
                return false;
            }
            return true;
        }
    }
    
    /**
     * This class is responsible for handling drops of VOTables representing sets of
     * or single 1D datasets, by adding them to a new chart.
     */
    private class PanelTransferHandler extends TransferHandler {
        
        @Override
        public boolean canImport(TransferHandler.TransferSupport support) {
            for (DataFlavor dataFlavor : support.getTransferable().getTransferDataFlavors()) {
                if (VoDataFlavors.voTableUriFlavor.isMimeTypeEqual(dataFlavor)) {
                    String dataTypeParam = dataFlavor.getParameter("data-type");
                    if (VoDataFlavors.VoTableDataType.TABLE_1D.toString().equals(dataTypeParam)
                            || VoDataFlavors.VoTableDataType.TABLE_1D_SET.toString().equals(dataTypeParam)) {
                        return true;
                    }
                }
            }
            return false;
        }

        @Override
        public boolean importData(TransferHandler.TransferSupport support) {
            try {
                Transferable transferable = support.getTransferable();
                // We get the VOTable and its file
                SavotVOTable voTable = null;
                File voTableFile = null;
                // If we are at the same JVM we might can get the instances from the holder
                if (transferable.isDataFlavorSupported(VoDataFlavors.voTableHolderKeyFlavor)) {
                    VoTableHolder.Key holderKey = (VoTableHolder.Key) transferable.getTransferData(VoDataFlavors.voTableHolderKeyFlavor);
                    Pair<SavotVOTable, File> retrievedPair = VoTableHolder.getInstance().retrieve(holderKey);
                    if (retrievedPair != null) {
                        voTable = retrievedPair.getValue0();
                        voTableFile = retrievedPair.getValue1();
                    }
                }
                // If the voTableFile is still not set we retrieve its URL from
                // the transferable
                if (voTableFile == null) {
                    BufferedReader reader = new BufferedReader(VoDataFlavors.voTableUriFlavor.getReaderForText(transferable));
                    String uriString = reader.readLine();
                    URI uri = new URI(uriString);
                    voTableFile = new File(uri);
                }
                // Now if we reached here and the voTable is null means that we
                // haven't retrieve it from the holder, so we read it from the
                // file
                if (voTable == null) {
                    SavotPullParser parser = new SavotPullParser(new FileInputStream(voTableFile), SavotPullEngine.FULL, null);
                    voTable = parser.getVOTable();
                }
                DataFlavor flavor = null;
                for (DataFlavor dataFlavor : transferable.getTransferDataFlavors()) {
                    if (VoDataFlavors.voTableUriFlavor.isMimeTypeEqual(dataFlavor)) {
                        flavor = dataFlavor;
                    }
                }
                VoDataFlavors.VoTableDataType dataType = null;
                String dataTypeParam = flavor.getParameter("data-type");
                if (VoDataFlavors.VoTableDataType.TABLE_1D.toString().equals(dataTypeParam)) {
                    dataType = VoDataFlavors.VoTableDataType.TABLE_1D;
                }
                if (VoDataFlavors.VoTableDataType.TABLE_1D_SET.toString().equals(dataTypeParam)) {
                    dataType = VoDataFlavors.VoTableDataType.TABLE_1D_SET;
                }
                switch (dataType) {
                    case TABLE_1D:
                        String name = VoTable1DHelper.getTableName(voTable);
                        ChartsTabbedPane.this.addChart(name, voTable, voTableFile);
                        break;
                    case TABLE_1D_SET:
                        String selectedParam = flavor.getParameter("selected-value");
                        Number selected = null;
                        if (selectedParam != null) {
                            selected = VoTable1DSetHelper.convertZValue(voTable, selectedParam);
                        }
                        name = VoTable1DSetHelper.getSetName(voTable);
                        ChartsTabbedPane.this.addSetChart(name, voTable, voTableFile, selected);
                        break;
                }
            } catch (Exception ex) {
                logger.error("Problem when retrieving the VOTable during drag and drop", ex);
                return false;
            }
            return true;
        }
    }

}
