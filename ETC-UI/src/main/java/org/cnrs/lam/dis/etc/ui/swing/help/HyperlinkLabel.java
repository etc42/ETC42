package org.cnrs.lam.dis.etc.ui.swing.help;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.font.TextAttribute;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class HyperlinkLabel extends JLabel {
	private static final long serialVersionUID = 3840694910150135142L;
	
	public HyperlinkLabel(final String text, final String href, int position) {
		super(text, position);
		
		if (position == SwingConstants.CENTER) {
			this.setText("<html><div style='text-align:center;'><u>" + text + "</u></div></html>");
		}

		Font font = getFont();
		Map attributes = font.getAttributes();
		attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
		setFont(font.deriveFont(attributes));
		
		setForeground(Color.BLUE);
		setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		addMouseListener(new MouseAdapter()
		{
			public void mouseClicked(MouseEvent e)  
			{
				try {
					if (isUrlMail(href)) {
						UtilsLoader.openMail(href);
					} else {
						UtilsLoader.openWebpage(new URL(href));
					}
				} catch (MalformedURLException e1) {}				
			}  
		});
	}
	
	public HyperlinkLabel(String text, final String href) {
		this(text, href, SwingConstants.LEFT);
	}
	
	public boolean isUrlMail(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
 }
}
