/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.ui.swing;

import java.util.List;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.cnrs.lam.dis.etc.ui.Messenger;

/**
 */
public class MessengerImpl implements Messenger {
    
    private EtcFrame defaultParent;
    private MessageHandler handler;

    public MessengerImpl(EtcFrame parent) {
        defaultParent = parent;
        setDefaultHandler();
    }
    
    public final void setDefaultHandler() {
        handler = new DefaultMessageHandler(defaultParent);
    }
    
    public final void setHandler(MessageHandler handler) {
        this.handler = handler;
    }

    @Override
    public void warn(String message) {
        handler.warn(message);
    }

    @Override
    public String requestInput(String message, List<String> possibilities) {
        return handler.requestInput(message, possibilities);
    }

    @Override
    public void info(String message) {
        handler.info(message);
    }

    @Override
    public DatasetInfo getNewDatasetInfo() {
        return handler.getNewDatasetInfo();
    }

    @Override
    public void error(String message) {
        handler.error(message);
    }

    @Override
    public String decision(String message, List<String> options) {
        return handler.decision(message, options);
    }

    @Override
    public void actionStoped(Thread thread) {
        handler.actionStoped(thread);
    }

    @Override
    public void actionStarted(String message, Thread thread) {
        handler.actionStarted(message, thread);
    }
    
}
