/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.ui.swing;

import cds.savot.model.SavotVOTable;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.KeyboardFocusManager;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javax.swing.Box;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.apache.log4j.Logger;
import org.cnrs.lam.cesam.vo.dnd.VoTable1DHelper;
import org.cnrs.lam.dis.etc.configuration.ConfigFactory;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.cnrs.lam.dis.etc.datamodel.ComponentInfo;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.Image;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.cnrs.lam.dis.etc.ui.BuisnessListener;
import org.cnrs.lam.dis.etc.ui.DatasetListener;
import org.cnrs.lam.dis.etc.ui.InfoProvider;
import org.cnrs.lam.dis.etc.ui.InstrumentListener;
import org.cnrs.lam.dis.etc.ui.ObsParamListener;
import org.cnrs.lam.dis.etc.ui.SampListener;
import org.cnrs.lam.dis.etc.ui.SessionListener;
import org.cnrs.lam.dis.etc.ui.SiteListener;
import org.cnrs.lam.dis.etc.ui.SourceListener;
import org.cnrs.lam.dis.etc.ui.generic.BuisnessListenerHolder;
import org.cnrs.lam.dis.etc.ui.generic.DatasetListenerHolder;
import org.cnrs.lam.dis.etc.ui.generic.InfoProviderHolder;
import org.cnrs.lam.dis.etc.ui.generic.InstrumentListenerHolder;
import org.cnrs.lam.dis.etc.ui.generic.MultiDeleteMessageHandler;
import org.cnrs.lam.dis.etc.ui.generic.ObsParamListenerHolder;
import org.cnrs.lam.dis.etc.ui.generic.SampListenerHolder;
import org.cnrs.lam.dis.etc.ui.generic.SessionHolder;
import org.cnrs.lam.dis.etc.ui.generic.SiteListenerHolder;
import org.cnrs.lam.dis.etc.ui.generic.SourceListenerHolder;
import org.cnrs.lam.dis.etc.ui.swing.config.ConfigurationDialogTool;
import org.cnrs.lam.dis.etc.ui.swing.dataset.DatasetManagementTool;
import org.cnrs.lam.dis.etc.ui.swing.generic.ClosableTabComponent;
import org.cnrs.lam.dis.etc.ui.swing.generic.MultiSelectDialog;
import org.cnrs.lam.dis.etc.ui.swing.help.AboutDialog;
import org.cnrs.lam.dis.etc.ui.swing.help.ContributionDialog;
import org.cnrs.lam.dis.etc.ui.swing.help.UtilsLoader;
import org.cnrs.lam.dis.etc.ui.swing.importer.MultiImportTool;
import org.cnrs.lam.dis.etc.ui.swing.result.ChartsTabbedPane;
import org.cnrs.lam.dis.etc.ui.swing.result.ImagesTabbedPane;
import org.cnrs.lam.dis.etc.ui.swing.result.ResultsPanel;
import org.cnrs.lam.dis.etc.ui.swing.result.LogoTabbedPane;
import org.javatuples.Triplet;
import org.json.JSONObject;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class EtcFrame extends javax.swing.JFrame implements org.cnrs.lam.dis.etc.ui.UIManager {
	private static final long serialVersionUID = 1L;

	private static Logger logger = Logger.getLogger(EtcFrame.class);
    private ProgressDialog progressDialog = new ProgressDialog(null, true);
    private final ResourceBundle bundle = java.util.ResourceBundle.getBundle("org/cnrs/lam/dis/etc/ui/swing/messages");
    private LogoTabbedPane resultsTabbedPane = new LogoTabbedPane();
    private ImagesTabbedPane imagesTabbedPane = new ImagesTabbedPane();
    private CommandHistoryPanel commandHistoryPanel = new CommandHistoryPanel();
    private ChartsTabbedPane chartsTabbedPane = new ChartsTabbedPane();
    
    private static EtcFrame instance;
    public static EtcFrame getInstance() {
    	return instance;
    }

    public final static String DOCUMENTATION_URL = "https://cesam.lam.fr/Apps/ETC/documentation/";
    
    /** Creates new form EtcFrame */
    public EtcFrame() {
        initComponents();
        progressDialog.setTitle(bundle.getString("PROGRESS_DIALOG_TITLE"));
        java.awt.Image logo = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/org/cnrs/lam/dis/etc/ui/swing/images/Etc-big.png"));
        resultsTabbedPane.setLogo(logo);
        chartsTabbedPane.setLogo(logo);
        imagesTabbedPane.setLogo(logo);
        outputTabbedPane.add(bundle.getString("RESULTS_PANEL_TITLE"), resultsTabbedPane);
        outputTabbedPane.add(bundle.getString("CHARTS_PANEL_TITLE"), chartsTabbedPane);
        outputTabbedPane.add(bundle.getString("IMAGES_PANEL_TITLE"), imagesTabbedPane);
        outputTabbedPane.add(bundle.getString("COMMAND_HISTORY_PANEL_TITLE"), commandHistoryPanel);
        
        // TODO: Revove the plot panel
//        plotPanel.getGlobalTabPane().add(resultsTabbedPane, 0);
//        plotPanel.getGlobalTabPane().setTabComponentAt(0, new JLabel(bundle.getString(("RESULTS_PANEL_TITLE"))));
//        plotPanel.getGlobalTabPane().add(imagesTabbedPane, 1);
//        plotPanel.getGlobalTabPane().setTabComponentAt(1, new JLabel(bundle.getString(("IMAGES_PANEL_TITLE"))));
//        plotPanel.getGlobalTabPane().add(datasetSetTabbedPane, 2);
//        plotPanel.getGlobalTabPane().setTabComponentAt(2, new JLabel("Dataset Sets"));
//        imagesTabbedPane.setPlotPanel(plotPanel);
//        datasetSetTabbedPane.setPlotPanel(plotPanel);
        
        instrumentScrollPane.getVerticalScrollBar().setUnitIncrement(15);
        siteScrollPane.getVerticalScrollBar().setUnitIncrement(15);
        sourceScrollPanel.getVerticalScrollBar().setUnitIncrement(15);
        obsParamScrollPane.getVerticalScrollBar().setUnitIncrement(15);
        
        instance = this;
    }

    public <T extends Number> void showImage(String title, Image<T> image) {
        imagesTabbedPane.showImage(title, image);
        outputTabbedPane.setSelectedComponent(imagesTabbedPane);
    }

    public <K extends Number, I extends Number> void showImageSet(String title, Map<K, Image<I>> imageMap, String keyUnit) {
        imagesTabbedPane.showImageSet(title, imageMap, keyUnit);
        outputTabbedPane.setSelectedComponent(imagesTabbedPane);
    }
    
    public CommandHistoryPanel getCommandHistoryPanel() {
        return commandHistoryPanel;
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {   	
        fileChooser = new javax.swing.JFileChooser();
        nameDescriptionDialog = new org.cnrs.lam.dis.etc.ui.swing.generic.NameDescriptionDialog();
        new javax.swing.ButtonGroup();
        plotPanel = new org.cnrs.lam.dis.etc.ihm.PlotPanel();
        toolBar = new javax.swing.JToolBar();
        instrumentButton = new javax.swing.JButton();
        siteButton = new javax.swing.JButton();
        sourceButton = new javax.swing.JButton();
        obsParamButton = new javax.swing.JButton();
        executeButton = new javax.swing.JButton();
        splitPane = new javax.swing.JSplitPane();
        inputPanel = new javax.swing.JPanel();
        instrumentScrollPane = new javax.swing.JScrollPane();
        instrumentPanel = new org.cnrs.lam.dis.etc.ui.swing.instrument.InstrumentPanel();
        siteScrollPane = new javax.swing.JScrollPane();
        sitePanel = new org.cnrs.lam.dis.etc.ui.swing.site.SitePanel();
        sourceScrollPanel = new javax.swing.JScrollPane();
        sourcePanel = new org.cnrs.lam.dis.etc.ui.swing.source.SourcePanel();
        obsParamScrollPane = new javax.swing.JScrollPane();
        obsParamPanel = new org.cnrs.lam.dis.etc.ui.swing.obsparam.ObsParamPanel();
        outputTabbedPane = new javax.swing.JTabbedPane();
        menuBar = new javax.swing.JMenuBar();
        if ("gnome-shell".equals(System.getenv("DESKTOP_SESSION"))) {
            try {
                Class<?> xwm = Class.forName("sun.awt.X11.XWM");
                Field awt_wmgr = xwm.getDeclaredField("awt_wmgr");
                awt_wmgr.setAccessible(true);
                Field other_wm = xwm.getDeclaredField("OTHER_WM");
                other_wm.setAccessible(true);
                if (awt_wmgr.get(null).equals(other_wm.get(null))) {
                    Field metacity_wm = xwm.getDeclaredField("METACITY_WM");
                    metacity_wm.setAccessible(true);
                    awt_wmgr.set(null, metacity_wm.get(null));
                    logger.info("bug #120 workaround enabled (Gnome 3 mouse misbehavior)");
                }
            } catch (Exception x) {
                logger.info(null, x);
            }
        }
        fileMenu = new javax.swing.JMenu();
        componentMenu = new javax.swing.JMenu();
        multiImportMenuItem = new javax.swing.JMenuItem();
        exportMenu = new javax.swing.JMenu();
        exportInstrumentMenuItem = new javax.swing.JMenuItem();
        exportSiteMenuItem = new javax.swing.JMenuItem();
        exportsourceMenuItem = new javax.swing.JMenuItem();
        exportObsParamMenuItem = new javax.swing.JMenuItem();
        deleteMenu = new javax.swing.JMenu();
        deleteInstrumentMenuItem = new javax.swing.JMenuItem();
        deleteSiteMenuItem = new javax.swing.JMenuItem();
        deleteSourceMenuItem = new javax.swing.JMenuItem();
        deleteObsParamMenuItem = new javax.swing.JMenuItem();
        datasetManagementToolMenuItem = new javax.swing.JMenuItem();
        OptionsMenuItem = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        quitMenuItem = new javax.swing.JMenuItem();
        sampMenu = new javax.swing.JMenu();
        sampConnectMenuItem = new javax.swing.JMenuItem();
        sampDisconnectMenuItem = new javax.swing.JMenuItem();
        pluginsMenu = new javax.swing.JMenu();
        importPluginsMenuItem = new javax.swing.JMenuItem();
        reloadPluginsMenuItem = new javax.swing.JMenuItem();
        pluginsMenuSeparator = new javax.swing.JPopupMenu.Separator();
        
        helpMenu = new javax.swing.JMenu();
        helpMenuItem = new javax.swing.JMenuItem();
        contributionMenuItem = new javax.swing.JMenuItem();
        aboutMenuItem = new javax.swing.JMenuItem();

        fileChooser.setName("fileChooser"); // NOI18N

        nameDescriptionDialog.setLocationRelativeTo(this);
        nameDescriptionDialog.setModal(true);
        nameDescriptionDialog.setName("nameDescriptionDialog"); // NOI18N

        plotPanel.setName("plotPanel"); // NOI18N

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("org/cnrs/lam/dis/etc/ui/swing/messages"); // NOI18N
        setTitle(bundle.getString("ETC_FRAME_TITLE")); // NOI18N
        
        // MAC app name
        System.setProperty("apple.laf.useScreenMenuBar", "true");
        System.setProperty("com.apple.mrj.application.apple.menu.about.name", bundle.getString("ETC_FRAME_TITLE"));
        
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/org/cnrs/lam/dis/etc/ui/swing/images/Etc-small.png")));
        setName("etcFrame"); // NOI18N
        addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
			public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        toolBar.setFloatable(false);
        toolBar.setName("toolBar"); // NOI18N

        instrumentButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/cnrs/lam/dis/etc/ui/swing/images/Instrument.png"))); // NOI18N
        instrumentButton.setText(bundle.getString("INSTRUMENT")); // NOI18N
        instrumentButton.setFocusable(false);
        instrumentButton.setName("instrumentButton"); // NOI18N
        instrumentButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                instrumentButtonActionPerformed(evt);
            }
        });
        toolBar.add(instrumentButton);

        siteButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/cnrs/lam/dis/etc/ui/swing/images/Site.png"))); // NOI18N
        siteButton.setText(bundle.getString("SITE")); // NOI18N
        siteButton.setFocusable(false);
        siteButton.setName("siteButton"); // NOI18N
        siteButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                siteButtonActionPerformed(evt);
            }
        });
        toolBar.add(siteButton);

        sourceButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/cnrs/lam/dis/etc/ui/swing/images/Source.png"))); // NOI18N
        sourceButton.setText(bundle.getString("SOURCE")); // NOI18N
        sourceButton.setFocusable(false);
        sourceButton.setName("sourceButton"); // NOI18N
        sourceButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                sourceButtonActionPerformed(evt);
            }
        });
        toolBar.add(sourceButton);

        obsParamButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/cnrs/lam/dis/etc/ui/swing/images/ObsParam.png"))); // NOI18N
        obsParamButton.setText(bundle.getString("OBS_PARAM")); // NOI18N
        obsParamButton.setFocusable(false);
        obsParamButton.setName("obsParamButton"); // NOI18N
        obsParamButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                obsParamButtonActionPerformed(evt);
            }
        });
        toolBar.add(obsParamButton);

        executeButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/cnrs/lam/dis/etc/ui/swing/images/Execute.png"))); // NOI18N
        executeButton.setText(bundle.getString("EXECUTE")); // NOI18N
        executeButton.setFocusable(false);
        executeButton.setName("executeButton"); // NOI18N
        executeButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                executeButtonActionPerformed(evt);
            }
        });
        toolBar.add(Box.createHorizontalGlue());
        toolBar.add(executeButton);

        splitPane.setDividerLocation(560);
        splitPane.setName("splitPane"); // NOI18N

        inputPanel.setName("inputPanel"); // NOI18N
        inputPanel.setLayout(new java.awt.CardLayout());

        instrumentPanel.setName("instrumentPanel"); // NOI18N
        instrumentScrollPane.setViewportView(instrumentPanel);

        inputPanel.add(instrumentScrollPane, "instrument");

        sitePanel.setName("sitePanel"); // NOI18N
        siteScrollPane.setViewportView(sitePanel);

        inputPanel.add(siteScrollPane, "site");

        sourcePanel.setName("sourcePanel"); // NOI18N
        sourceScrollPanel.setViewportView(sourcePanel);

        inputPanel.add(sourceScrollPanel, "source");

        obsParamPanel.setName("obsParamPanel"); // NOI18N
        obsParamScrollPane.setViewportView(obsParamPanel);

        inputPanel.add(obsParamScrollPane, "obsparam");

        splitPane.setLeftComponent(inputPanel);
        splitPane.setRightComponent(outputTabbedPane);

        menuBar.setName("menuBar"); // NOI18N

        fileMenu.setText(bundle.getString("MENU_FILE")); // NOI18N
        fileMenu.setName("fileMenu"); // NOI18N

        componentMenu.setText(bundle.getString("MENU_COMPONENT")); // NOI18N
        componentMenu.setName("componentMenu"); // NOI18N

        multiImportMenuItem.setText(bundle.getString("MENU_MULTI_IMPORT")); // NOI18N
        multiImportMenuItem.setName("multiImportMenuItem"); // NOI18N
        multiImportMenuItem.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                multiImportMenuItemActionPerformed(evt);
            }
        });
        componentMenu.add(multiImportMenuItem);

        exportMenu.setText(bundle.getString("MENU_EXPORT")); // NOI18N
        exportMenu.setName("exportMenu"); // NOI18N

        exportInstrumentMenuItem.setText(bundle.getString("MENU_EXPORT_INSTRUMENT")); // NOI18N
        exportInstrumentMenuItem.setName("exportInstrumentMenuItem"); // NOI18N
        exportInstrumentMenuItem.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                exportInstrumentMenuItemActionPerformed(evt);
            }
        });
        exportMenu.add(exportInstrumentMenuItem);

        exportSiteMenuItem.setText(bundle.getString("MENU_EXPORT_SITE")); // NOI18N
        exportSiteMenuItem.setName("exportSiteMenuItem"); // NOI18N
        exportSiteMenuItem.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                exportSiteMenuItemActionPerformed(evt);
            }
        });
        exportMenu.add(exportSiteMenuItem);

        exportsourceMenuItem.setText(bundle.getString("MENU_EXPORT_SOURCE")); // NOI18N
        exportsourceMenuItem.setName("exportsourceMenuItem"); // NOI18N
        exportsourceMenuItem.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                exportsourceMenuItemActionPerformed(evt);
            }
        });
        exportMenu.add(exportsourceMenuItem);

        exportObsParamMenuItem.setText(bundle.getString("MENU_EXPORT_OBS_PARAM")); // NOI18N
        exportObsParamMenuItem.setName("exportObsParamMenuItem"); // NOI18N
        exportObsParamMenuItem.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                exportObsParamMenuItemActionPerformed(evt);
            }
        });
        exportMenu.add(exportObsParamMenuItem);

        componentMenu.add(exportMenu);

        deleteMenu.setText(bundle.getString("MENU_DELETE")); // NOI18N
        deleteMenu.setActionCommand(bundle.getString("MENU_DELETE")); // NOI18N
        deleteMenu.setName("deleteMenu"); // NOI18N

        deleteInstrumentMenuItem.setText(bundle.getString("MENU_DELETE_INSTRUMENT")); // NOI18N
        deleteInstrumentMenuItem.setName("deleteInstrumentMenuItem"); // NOI18N
        deleteInstrumentMenuItem.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteInstrumentMenuItemActionPerformed(evt);
            }
        });
        deleteMenu.add(deleteInstrumentMenuItem);

        deleteSiteMenuItem.setText(bundle.getString("MENU_DELETE_SITE")); // NOI18N
        deleteSiteMenuItem.setName("deleteSiteMenuItem"); // NOI18N
        deleteSiteMenuItem.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteSiteMenuItemActionPerformed(evt);
            }
        });
        deleteMenu.add(deleteSiteMenuItem);

        deleteSourceMenuItem.setText(bundle.getString("MENU_DELETE_SOURCE")); // NOI18N
        deleteSourceMenuItem.setName("deleteSourceMenuItem"); // NOI18N
        deleteSourceMenuItem.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteSourceMenuItemActionPerformed(evt);
            }
        });
        deleteMenu.add(deleteSourceMenuItem);

        deleteObsParamMenuItem.setText(bundle.getString("MENU_DELETE_OBS_PARAM")); // NOI18N
        deleteObsParamMenuItem.setName("deleteObsParamMenuItem"); // NOI18N
        deleteObsParamMenuItem.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteObsParamMenuItemActionPerformed(evt);
            }
        });
        deleteMenu.add(deleteObsParamMenuItem);

        componentMenu.add(deleteMenu);

        fileMenu.add(componentMenu);

        datasetManagementToolMenuItem.setText(bundle.getString("MENU_DATASET_MANAGEMENT_TOOL")); // NOI18N
        datasetManagementToolMenuItem.setName("datasetManagementToolMenuItem"); // NOI18N
        datasetManagementToolMenuItem.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                datasetManagementToolMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(datasetManagementToolMenuItem);

        OptionsMenuItem.setText(bundle.getString("MENU_OPTIONS_TOOL")); // NOI18N
        OptionsMenuItem.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                OptionsMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(OptionsMenuItem);
        fileMenu.add(jSeparator1);

        quitMenuItem.setText(bundle.getString("MENU_QUIT")); // NOI18N
        quitMenuItem.setName("quitMenuItem"); // NOI18N
        quitMenuItem.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                quitMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(quitMenuItem);

        menuBar.add(fileMenu);

        sampMenu.setText(bundle.getString("MENU_SAMP")); // NOI18N
        sampMenu.setName("sampMenu"); // NOI18N

        sampConnectMenuItem.setText(bundle.getString("MENU_SAMP_CONNECT")); // NOI18N
        sampConnectMenuItem.setName("sampConnectMenuItem"); // NOI18N
        sampConnectMenuItem.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                sampConnectMenuItemActionPerformed(evt);
            }
        });
        sampMenu.add(sampConnectMenuItem);

        sampDisconnectMenuItem.setText(bundle.getString("MENU_SAMP_DISCONNECT")); // NOI18N
        sampDisconnectMenuItem.setName("sampDisconnectMenuItem"); // NOI18N
        sampDisconnectMenuItem.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                sampDisconnectMenuItemActionPerformed(evt);
            }
        });
        sampMenu.add(sampDisconnectMenuItem);

        menuBar.add(sampMenu);

        pluginsMenu.setText(bundle.getString("MENU_PLUGINS")); // NOI18N
        pluginsMenu.setName("pluginsMenu"); // NOI18N

        importPluginsMenuItem.setText(bundle.getString("MENU_IMPORT_PLUGINS")); // NOI18N
        importPluginsMenuItem.setName("importPluginsMenuItem"); // NOI18N
        importPluginsMenuItem.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                importPluginsMenuItemActionPerformed(evt);
            }
        });
        pluginsMenu.add(importPluginsMenuItem);

        reloadPluginsMenuItem.setText(bundle.getString("MENU_RELOAD_PLUGINS")); // NOI18N
        reloadPluginsMenuItem.setName("reloadPluginsMenuItem"); // NOI18N
        reloadPluginsMenuItem.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                reloadPluginsMenuItemActionPerformed(evt);
            }
        });
        pluginsMenu.add(reloadPluginsMenuItem);

        pluginsMenuSeparator.setName("pluginsMenuSeparator"); // NOI18N
        pluginsMenu.add(pluginsMenuSeparator);

        menuBar.add(pluginsMenu);
        
        helpMenuItem.setText("Help"); // NOI18N
        helpMenuItem.setName("HelpMenuItem"); // NOI18N
        helpMenuItem.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                try {
					UtilsLoader.openWebpage(new URL(DOCUMENTATION_URL));
				} catch (MalformedURLException e) {}
            }
        });
        helpMenu.add(helpMenuItem);
        
        contributionMenuItem.setText("Contribute"); // NOI18N
        contributionMenuItem.setName("contributeMenuItem"); // NOI18N
        contributionMenuItem.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
            	ContributionDialog.show(EtcFrame.this);
            }
        });
        helpMenu.add(contributionMenuItem);
        
        aboutMenuItem.setText("About"); // NOI18N
        aboutMenuItem.setName("aboutMenuItem"); // NOI18N
        aboutMenuItem.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	AboutDialog.show(EtcFrame.this);
            }
        });
        helpMenu.add(new JSeparator());
        helpMenu.add(aboutMenuItem);
        

        helpMenu.setText("?");
        
        
        menuBar.add(helpMenu);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(toolBar, javax.swing.GroupLayout.DEFAULT_SIZE, 961, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(splitPane, javax.swing.GroupLayout.DEFAULT_SIZE, 949, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(toolBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(splitPane, javax.swing.GroupLayout.DEFAULT_SIZE, 626, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void instrumentButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_instrumentButtonActionPerformed
        ((CardLayout) inputPanel.getLayout()).show(inputPanel, "instrument");
    }//GEN-LAST:event_instrumentButtonActionPerformed

    private void siteButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_siteButtonActionPerformed
        ((CardLayout) inputPanel.getLayout()).show(inputPanel, "site");
    }//GEN-LAST:event_siteButtonActionPerformed

    private void sourceButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sourceButtonActionPerformed
        ((CardLayout) inputPanel.getLayout()).show(inputPanel, "source");
    }//GEN-LAST:event_sourceButtonActionPerformed

    private void obsParamButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_obsParamButtonActionPerformed
        ((CardLayout) inputPanel.getLayout()).show(inputPanel, "obsparam");
    }//GEN-LAST:event_obsParamButtonActionPerformed

    private void quitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_quitMenuItemActionPerformed
        shutdown();
    }//GEN-LAST:event_quitMenuItemActionPerformed

    private void executeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_executeButtonActionPerformed
        // Fix for bug #17. When pressing the execute button the focus lost event
        // of the currently selected text field is not executed until after the
        // execution of the button event, so we go through the listeners of the
        // text field and we notify them accordingly.
    	
    	instrumentPanel.getSpectrographPanel().updateMinRangeInSession();
    	instrumentPanel.getSpectrographPanel().updateMaxRangeInSession();

        Component focusOwner = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
        if (focusOwner != null && focusOwner instanceof JTextField) {
            JTextField focusOwnerTextField = (JTextField) focusOwner;
            for (FocusListener listener : focusOwnerTextField.getFocusListeners()) {
                listener.focusLost(new FocusEvent(focusOwner, FocusEvent.FOCUS_LOST));
            }
        }
        BuisnessListenerHolder.getBuisnessListener().executeCalculation();
    }//GEN-LAST:event_executeButtonActionPerformed

    private void exportInstrumentMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exportInstrumentMenuItemActionPerformed
        int selectedOption = fileChooser.showSaveDialog(this);
        if (selectedOption == JFileChooser.APPROVE_OPTION) {
            InstrumentListenerHolder.getInstrumentListener().exportCurrentInstrumentInFile(fileChooser.getSelectedFile());
        }
    }//GEN-LAST:event_exportInstrumentMenuItemActionPerformed

    private void exportSiteMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exportSiteMenuItemActionPerformed
        int selectedOption = fileChooser.showSaveDialog(this);
        if (selectedOption == JFileChooser.APPROVE_OPTION) {
            SiteListenerHolder.getSiteListener().exportCurrentSiteInFile(fileChooser.getSelectedFile());
        }
    }//GEN-LAST:event_exportSiteMenuItemActionPerformed

    private void exportsourceMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exportsourceMenuItemActionPerformed
        int selectedOption = fileChooser.showSaveDialog(this);
        if (selectedOption == JFileChooser.APPROVE_OPTION) {
            SourceListenerHolder.getSourceListener().exportCurrentSourceInFile(fileChooser.getSelectedFile());
        }
    }//GEN-LAST:event_exportsourceMenuItemActionPerformed

    private void exportObsParamMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exportObsParamMenuItemActionPerformed
        int selectedOption = fileChooser.showSaveDialog(this);
        if (selectedOption == JFileChooser.APPROVE_OPTION) {
            ObsParamListenerHolder.getObsParamListener().exportCurrentObsParamInFile(fileChooser.getSelectedFile());
        }
    }//GEN-LAST:event_exportObsParamMenuItemActionPerformed

    private void sampConnectMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sampConnectMenuItemActionPerformed
        SampListenerHolder.getSampListener().connect();
    }//GEN-LAST:event_sampConnectMenuItemActionPerformed

    private void sampDisconnectMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sampDisconnectMenuItemActionPerformed
        SampListenerHolder.getSampListener().disconnect();
    }//GEN-LAST:event_sampDisconnectMenuItemActionPerformed

    private void deleteInstrumentMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteInstrumentMenuItemActionPerformed
        List<ComponentInfo> availableInstrumentList = InfoProviderHolder.getInfoProvider().getAvailableInstrumentList();
        MultiSelectDialog<ComponentInfo> dialog = new MultiSelectDialog<ComponentInfo>
                (this, true, bundle.getString("SELECT_INSTRUMENT_TO_DELETE"), availableInstrumentList);
        dialog.setVisible(true);
        List<ComponentInfo> selectedList = dialog.getSelectedList();
        if (selectedList.isEmpty()) {
            return;
        }
        StringBuilder message = new StringBuilder(bundle.getString("CONFIRM_MULTI_DELETE_INSTRUMENT")).append("\n");
        for (ComponentInfo info : selectedList) {
            message.append(info).append("\n");
        }
        int option = JOptionPane.showConfirmDialog(this, message.toString(), null, JOptionPane.YES_NO_OPTION);
        if (option != JOptionPane.YES_OPTION) {
            return;
        }
        MultiDeleteMessageHandler messageHandler = new MultiDeleteMessageHandler(this);
        MessengerHolder.setHandler(messageHandler);
        for (ComponentInfo info : selectedList) {
            InstrumentListenerHolder.getInstrumentListener().deleteInstrument(info);
        }
        MessengerHolder.setDefaultHandler();
        List<String> errorMessageList = messageHandler.getErrorMessageList();
        if (errorMessageList.isEmpty()) {
            MessengerHolder.getMessenger(this).info(bundle.getString("MULTI_DELETE_INSTRUMENT_SUCCESS"));
        } else {
            message = new StringBuilder();
            for (String error : errorMessageList) {
                message.append(error).append("\n");
            }
            MessengerHolder.getMessenger(this).error(message.toString());
        }
    }//GEN-LAST:event_deleteInstrumentMenuItemActionPerformed

    private void deleteSiteMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteSiteMenuItemActionPerformed
        List<ComponentInfo> availableSiteList = InfoProviderHolder.getInfoProvider().getAvailableSiteList();
        MultiSelectDialog<ComponentInfo> dialog = new MultiSelectDialog<ComponentInfo>
                (this, true, bundle.getString("SELECT_SITE_TO_DELETE"), availableSiteList);
        dialog.setVisible(true);
        List<ComponentInfo> selectedList = dialog.getSelectedList();
        if (selectedList.isEmpty()) {
            return;
        }
        StringBuilder message = new StringBuilder(bundle.getString("CONFIRM_MULTI_DELETE_SITE")).append("\n");
        for (ComponentInfo info : selectedList) {
            message.append(info).append("\n");
        }
        int option = JOptionPane.showConfirmDialog(this, message.toString(), null, JOptionPane.YES_NO_OPTION);
        if (option != JOptionPane.YES_OPTION) {
            return;
        }
        MultiDeleteMessageHandler messageHandler = new MultiDeleteMessageHandler(this);
        MessengerHolder.setHandler(messageHandler);
        for (ComponentInfo info : selectedList) {
            SiteListenerHolder.getSiteListener().deleteSite(info);
        }
        MessengerHolder.setDefaultHandler();
        List<String> errorMessageList = messageHandler.getErrorMessageList();
        if (errorMessageList.isEmpty()) {
            MessengerHolder.getMessenger(this).info(bundle.getString("MULTI_DELETE_SITE_SUCCESS"));
        } else {
            message = new StringBuilder();
            for (String error : errorMessageList) {
                message.append(error).append("\n");
            }
            MessengerHolder.getMessenger(this).error(message.toString());
        }
    }//GEN-LAST:event_deleteSiteMenuItemActionPerformed

    private void deleteSourceMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteSourceMenuItemActionPerformed
        List<ComponentInfo> availableSourceList = InfoProviderHolder.getInfoProvider().getAvailableSourceList();
        MultiSelectDialog<ComponentInfo> dialog = new MultiSelectDialog<ComponentInfo>
                (this, true, bundle.getString("SELECT_SOURCE_TO_DELETE"), availableSourceList);
        dialog.setVisible(true);
        List<ComponentInfo> selectedList = dialog.getSelectedList();
        if (selectedList.isEmpty()) {
            return;
        }
        StringBuilder message = new StringBuilder(bundle.getString("CONFIRM_MULTI_DELETE_SOURCE")).append("\n");
        for (ComponentInfo info : selectedList) {
            message.append(info).append("\n");
        }
        int option = JOptionPane.showConfirmDialog(this, message.toString(), null, JOptionPane.YES_NO_OPTION);
        if (option != JOptionPane.YES_OPTION) {
            return;
        }
        MultiDeleteMessageHandler messageHandler = new MultiDeleteMessageHandler(this);
        MessengerHolder.setHandler(messageHandler);
        for (ComponentInfo info : selectedList) {
            SourceListenerHolder.getSourceListener().deleteSource(info);
        }
        MessengerHolder.setDefaultHandler();
        List<String> errorMessageList = messageHandler.getErrorMessageList();
        if (errorMessageList.isEmpty()) {
            MessengerHolder.getMessenger(this).info(bundle.getString("MULTI_DELETE_SOURCE_SUCCESS"));
        } else {
            message = new StringBuilder();
            for (String error : errorMessageList) {
                message.append(error).append("\n");
            }
            MessengerHolder.getMessenger(this).error(message.toString());
        }
    }//GEN-LAST:event_deleteSourceMenuItemActionPerformed

    private void deleteObsParamMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteObsParamMenuItemActionPerformed
        List<ComponentInfo> availableObsParamList = InfoProviderHolder.getInfoProvider().getAvailableObsParamList();
        MultiSelectDialog<ComponentInfo> dialog = new MultiSelectDialog<ComponentInfo>
                (this, true, bundle.getString("SELECT_OBS_PARAM_TO_DELETE"), availableObsParamList);
        dialog.setVisible(true);
        List<ComponentInfo> selectedList = dialog.getSelectedList();
        if (selectedList.isEmpty()) {
            return;
        }
        StringBuilder message = new StringBuilder(bundle.getString("CONFIRM_MULTI_DELETE_OBS_PARAM")).append("\n");
        for (ComponentInfo info : selectedList) {
            message.append(info).append("\n");
        }
        int option = JOptionPane.showConfirmDialog(this, message.toString(), null, JOptionPane.YES_NO_OPTION);
        if (option != JOptionPane.YES_OPTION) {
            return;
        }
        MultiDeleteMessageHandler messageHandler = new MultiDeleteMessageHandler(this);
        MessengerHolder.setHandler(messageHandler);
        for (ComponentInfo info : selectedList) {
            ObsParamListenerHolder.getObsParamListener().deleteObsParam(info);
        }
        MessengerHolder.setDefaultHandler();
        List<String> errorMessageList = messageHandler.getErrorMessageList();
        if (errorMessageList.isEmpty()) {
            MessengerHolder.getMessenger(this).info(bundle.getString("MULTI_DELETE_OBS_PARAM_SUCCESS"));
        } else {
            message = new StringBuilder();
            for (String error : errorMessageList) {
                message.append(error).append("\n");
            }
            MessengerHolder.getMessenger(this).error(message.toString());
        }
    }//GEN-LAST:event_deleteObsParamMenuItemActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        shutdown();
    }//GEN-LAST:event_formWindowClosing

    private void multiImportMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_multiImportMenuItemActionPerformed
        MultiImportTool tool = new MultiImportTool(this, true);
        tool.setVisible(true);
    }//GEN-LAST:event_multiImportMenuItemActionPerformed

    private void datasetManagementToolMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_datasetManagementToolMenuItemActionPerformed
        DatasetManagementTool tool = new DatasetManagementTool(this, true);
        tool.setVisible(true);
    }//GEN-LAST:event_datasetManagementToolMenuItemActionPerformed

private void OptionsMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_OptionsMenuItemActionPerformed
        ConfigurationDialogTool tool = new ConfigurationDialogTool(this, true);
        tool.setVisible(true);
}//GEN-LAST:event_OptionsMenuItemActionPerformed

    private void importPluginsMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_importPluginsMenuItemActionPerformed
        int selectedOption = fileChooser.showOpenDialog(this);
        if (selectedOption == JFileChooser.APPROVE_OPTION) {
            BuisnessListenerHolder.getBuisnessListener().addPluginsFromJar(fileChooser.getSelectedFile());
        }
    }//GEN-LAST:event_importPluginsMenuItemActionPerformed

    private void reloadPluginsMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_reloadPluginsMenuItemActionPerformed
        BuisnessListenerHolder.getBuisnessListener().reloadPlugins();
    }//GEN-LAST:event_reloadPluginsMenuItemActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem OptionsMenuItem;
    private javax.swing.JMenu componentMenu;
    private javax.swing.JMenuItem datasetManagementToolMenuItem;
    private javax.swing.JMenuItem deleteInstrumentMenuItem;
    private javax.swing.JMenu deleteMenu;
    private javax.swing.JMenuItem deleteObsParamMenuItem;
    private javax.swing.JMenuItem deleteSiteMenuItem;
    private javax.swing.JMenuItem deleteSourceMenuItem;
    private javax.swing.JButton executeButton;
    private javax.swing.JMenuItem exportInstrumentMenuItem;
    private javax.swing.JMenu exportMenu;
    private javax.swing.JMenuItem exportObsParamMenuItem;
    private javax.swing.JMenuItem exportSiteMenuItem;
    private javax.swing.JMenuItem exportsourceMenuItem;
    private javax.swing.JFileChooser fileChooser;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JMenuItem importPluginsMenuItem;
    private javax.swing.JPanel inputPanel;
    private javax.swing.JButton instrumentButton;
    private org.cnrs.lam.dis.etc.ui.swing.instrument.InstrumentPanel instrumentPanel;
    private javax.swing.JScrollPane instrumentScrollPane;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenuItem multiImportMenuItem;
    private org.cnrs.lam.dis.etc.ui.swing.generic.NameDescriptionDialog nameDescriptionDialog;
    private javax.swing.JButton obsParamButton;
    private org.cnrs.lam.dis.etc.ui.swing.obsparam.ObsParamPanel obsParamPanel;
    private javax.swing.JScrollPane obsParamScrollPane;
    private javax.swing.JTabbedPane outputTabbedPane;
    private org.cnrs.lam.dis.etc.ihm.PlotPanel plotPanel;
    private javax.swing.JMenu pluginsMenu;
    private javax.swing.JPopupMenu.Separator pluginsMenuSeparator;
    private javax.swing.JMenuItem quitMenuItem;
    private javax.swing.JMenuItem reloadPluginsMenuItem;
    private javax.swing.JMenuItem sampConnectMenuItem;
    private javax.swing.JMenuItem sampDisconnectMenuItem;
    private javax.swing.JMenu sampMenu;
    private javax.swing.JButton siteButton;
    private org.cnrs.lam.dis.etc.ui.swing.site.SitePanel sitePanel;
    private javax.swing.JScrollPane siteScrollPane;
    private javax.swing.JButton sourceButton;
    private org.cnrs.lam.dis.etc.ui.swing.source.SourcePanel sourcePanel;
    private javax.swing.JScrollPane sourceScrollPanel;
    private javax.swing.JSplitPane splitPane;
    private javax.swing.JToolBar toolBar;
    
    private javax.swing.JMenu helpMenu;
    private javax.swing.JMenuItem helpMenuItem;
    private javax.swing.JMenuItem contributionMenuItem;
    private javax.swing.JMenuItem aboutMenuItem;
    // End of variables declaration//GEN-END:variables

    @Override
    public void setSession(Session session) {
        SessionHolder.setSession(session);
        handleMode();
    }

    @Override
    public void sessionModified() {
        SessionHolder.notifyForModification();
    }

    @Override
    public void setInfoProvider(InfoProvider provider) {
        InfoProviderHolder.setInfoProvider(provider);
        // Now that we have the info provider we can popupate the Plugins menu
        populatePluginsMenu();
    }

    @Override
    public void setSessionListener(SessionListener listener) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void setInstrumentListener(InstrumentListener listener) {
        InstrumentListenerHolder.setInstrumentListener(listener);
    }

    @Override
    public void setSiteListener(SiteListener listener) {
        SiteListenerHolder.setSiteListener(listener);
    }

    @Override
    public void setSourceListener(SourceListener listener) {
        SourceListenerHolder.setSourceListener(listener);
    }

    @Override
    public void setObsParamListener(ObsParamListener listener) {
        ObsParamListenerHolder.setObsParamListener(listener);
    }

    @Override
    public void setDatasetListener(DatasetListener listener) {
        DatasetListenerHolder.setDatasetListener(listener);
    }

    @Override
    public void setBuisnessListener(BuisnessListener listener) {
        BuisnessListenerHolder.setBuisnessListener(listener);
    }

    @Override
    public void showPlot(String title, String xDescription, String yDescription, String xUnit, String yUnit, List<Dataset> datasetList) {
        if (datasetList.size() == 1) {
            Map<Double, Double> data = datasetList.get(0).getData();
            SavotVOTable voTable = VoTable1DHelper.createVoTable(title, xDescription, xUnit, yDescription, yUnit, data);
            showChart(title, voTable, null);
        }
    }
    
    @Override
    public void showChart(String title, SavotVOTable voTable, File voTableFile) {
        chartsTabbedPane.addChart(title, voTable, voTableFile);
        outputTabbedPane.setSelectedComponent(chartsTabbedPane);
    }
    
    public void showSetChart(String title, SavotVOTable voTable, File voTableFile) {
        chartsTabbedPane.addSetChart(title, voTable, voTableFile, null);
        outputTabbedPane.setSelectedComponent(chartsTabbedPane);
    }

    @Override
    public void showCalculationResult(CalculationResults result) {
        String resultName = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
        ResultsPanel resultsPanel = new ResultsPanel(resultName, result, this
                , Level.valueOf(ConfigFactory.getConfig().getResultLevel()));
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setViewportView(resultsPanel);
        scrollPane.getVerticalScrollBar().setUnitIncrement(15);
        resultsTabbedPane.add(resultName, scrollPane);
        ClosableTabComponent resultTabComponent = new ClosableTabComponent(resultsTabbedPane, resultName);
        int index = resultsTabbedPane.getTabCount() - 1;
        resultsTabbedPane.setTabComponentAt(index, resultTabComponent);
        resultsTabbedPane.setSelectedIndex(index);
        outputTabbedPane.setSelectedComponent(resultsTabbedPane);
    }

    @Override
    public void setSampListener(SampListener listener) {
        SampListenerHolder.setSampListener(listener);
    }

    private void shutdown() {
        if (InstrumentListenerHolder.getInstrumentListener().isCurrentInstrumentModified() ||
                SiteListenerHolder.getSiteListener().isCurrentSiteModified() ||
                SourceListenerHolder.getSourceListener().isCurrentSourceModified() ||
                ObsParamListenerHolder.getObsParamListener().isCurrentObsParamModified()) {
            int selected = JOptionPane.showConfirmDialog(this, bundle.getString("SAVE_ON_QUIT"), null, JOptionPane.YES_NO_OPTION);
            if (selected != JOptionPane.YES_OPTION)
                return;
        }
        System.exit(0);
    }

    @Override
    public void pluginListChanged() {
        pluginsMenu.removeAll();
        pluginsMenu.add(importPluginsMenuItem);
        pluginsMenu.add(reloadPluginsMenuItem);
        pluginsMenu.add(pluginsMenuSeparator);
        populatePluginsMenu();
    }

    private void populatePluginsMenu() {
        InfoProvider infoProvider = InfoProviderHolder.getInfoProvider();
        for (Triplet<Class, List<String>, String> triplet : infoProvider.getAvailablePluginList()) {
            final Class pluginClass = triplet.getValue0();
            List<String> menuPath = triplet.getValue1();
            String tooltip = triplet.getValue2();
            JMenuItem menuItem = createPluginMenuItem(pluginsMenu, menuPath);
            if (menuItem == null) {
                MessengerHolder.getMessenger(this)
                    .error("Failed to create menu item for plugin. Please see log file for details.");
            }
            menuItem.setToolTipText(tooltip);
            menuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    BuisnessListenerHolder.getBuisnessListener().runPlugin(pluginClass);
                }
            });
        }
    }

    private JMenuItem createPluginMenuItem(JMenu menu, List<String> menuPath) {
        if (menu == null || menuPath.isEmpty())
            return null;
        // Get the first name from the list
        String firstName = menuPath.get(0).trim();
        // If it is the final name then create the menu item and return it
        if (menuPath.size() == 1) {
            return createAndAttachMenuItem(menu, firstName);
        } else {
            // If it is a sub-menu we create the menu and we do recursion
            JMenu newMenu = createAndAttachSubMenu(menu, firstName);
            List<String> newPath = new ArrayList<String>();
            for (int i = 1; i < menuPath.size(); i++) {
                newPath.add(menuPath.get(i));
            }
            return createPluginMenuItem(newMenu, newPath);
        }
    }

    private JMenuItem createAndAttachMenuItem(JMenu menu, String firstName) {
        // First find the name which must be used for this menu item, for the
        // case there are other menu items with the same name
        int originalLength = firstName.length();
        for (int i = 0; menuContainsName(menu, firstName) != null; i++) {
            firstName = firstName.substring(0, originalLength);
            firstName = firstName + i;
        }
        logger.info("Creating plugin menu item " + menu.getText() + "->" + firstName);
        JMenuItem newItem = new JMenuItem(firstName);
        menu.add(newItem);
        return newItem;
    }

    private JMenuItem menuContainsName(JMenu menu, String firstName) {
        for (int i = 0; i < menu.getItemCount(); i++) {
            JMenuItem item = menu.getItem(i);
            if (item != null && firstName.equals(item.getText())) {
                return item;
            }
        }
        return null;
    }

    private JMenu createAndAttachSubMenu(JMenu menu, String firstName) {
        JMenuItem oldItem = menuContainsName(menu, firstName);
        if (oldItem == null) {
            logger.info("Creating plugin sub menu " + menu.getText() + "->" + firstName);
            JMenu newMenu = new JMenu(firstName);
            menu.add(newMenu);
            return newMenu;
        } else {
            logger.error("Failed to create plugin submenu " + menu.getText() + "->"
                    + firstName + " because there is a different type of item with"
                    + " the same name");
            return (oldItem instanceof JMenu) ? (JMenu) oldItem : null;
        }
    }

    @Override
    public void handleFirstInstallation() {
        int selection = JOptionPane.showConfirmDialog(this, bundle.getString("FIRST_INSTALL_IMPORT_MESSAGE"), null, JOptionPane.YES_NO_OPTION);
        if (selection == JOptionPane.YES_OPTION) {
            MultiImportTool tool = new MultiImportTool(this, true);
            tool.setVisible(true);
        }
    }
    
    public void handleMode() {
        if (System.getProperty("etc.fixtures") != null) {
            
            String server = ConfigFactory.getConfig().getFixturesUrlsDefault();
            if (System.getProperty("etc.server") != null) {
                server = System.getProperty("etc.server");
            }
        
            try {
                if (!server.endsWith("/"))
                    server += "/";
                String jsonString = "";
                BufferedReader json = new BufferedReader(new InputStreamReader(
                        new URL(server + System.getProperty("etc.fixtures") + "/mode.json").openStream()));
                while (json.ready()) {
                    jsonString += json.readLine();
                }
                JSONObject jObject = new JSONObject(jsonString);
                instrumentPanel.handleMode(jObject.getJSONObject("instrument"));
                sitePanel.handleMode(jObject.getJSONObject("site"));
                deleteInstrumentMenuItem.setEnabled(false);
                deleteSiteMenuItem.setEnabled(false);
            } catch (IOException ex) {
                logger.error("mode.json file not found. Default mode will be used.");
            }
        }
    }

}
