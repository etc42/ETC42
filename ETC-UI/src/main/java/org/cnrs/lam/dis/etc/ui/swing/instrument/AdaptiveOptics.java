/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.ui.swing.instrument;

import org.cnrs.lam.dis.etc.ui.swing.EtcPanel;
import org.cnrs.lam.dis.etc.ui.swing.UserMode;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author distag2010
 */
public class AdaptiveOptics extends EtcPanel {

    /** Creates new form AdaptiveOptics */
    public AdaptiveOptics() {
        initComponents();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        strehlRatioPanel = new javax.swing.JPanel();
        strehlRatioLabel = new javax.swing.JLabel();
        strehlRatioTextField = new org.cnrs.lam.dis.etc.ui.swing.generic.PercentageTextField();
        strehlRatioUnitLabel = new javax.swing.JLabel();
        refWavelengthLabel = new javax.swing.JLabel();
        refWavelengthTextField = new org.cnrs.lam.dis.etc.ui.swing.generic.DoubleTextField();
        refWavelengthUnitLabel = new javax.swing.JLabel();

        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("org/cnrs/lam/dis/etc/ui/swing/messages"); // NOI18N
        strehlRatioPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, bundle.getString("STREHL_RATIO"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("DejaVu Sans", 0, 12))); // NOI18N
        strehlRatioPanel.setName("strehlRatioPanel"); // NOI18N

        strehlRatioLabel.setText(bundle.getString("STREHL_RATIO_LABEL")); // NOI18N
        strehlRatioLabel.setName("strehlRatioLabel"); // NOI18N

        strehlRatioTextField.setName("strehlRatioTextField"); // NOI18N
        strehlRatioTextField.setNormalForeground(new java.awt.Color(0, 0, 0));
        strehlRatioTextField.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                strehlRatioTextFieldActionPerformed(evt);
            }
        });
        strehlRatioTextField.addFocusListener(new java.awt.event.FocusAdapter() {
            @Override
			public void focusLost(java.awt.event.FocusEvent evt) {
                strehlRatioTextFieldFocusLost(evt);
            }
        });

        strehlRatioUnitLabel.setText("%");
        strehlRatioUnitLabel.setName("strehlRatioUnitLabel"); // NOI18N

        refWavelengthLabel.setText(bundle.getString("REFERENCE_WAVELENGTH")); // NOI18N
        refWavelengthLabel.setName("refWavelengthLabel"); // NOI18N

        refWavelengthTextField.setName("refWavelengthTextField"); // NOI18N
        refWavelengthTextField.setNormalForeground(new java.awt.Color(0, 0, 0));
        refWavelengthTextField.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                refWavelengthTextFieldActionPerformed(evt);
            }
        });
        refWavelengthTextField.addFocusListener(new java.awt.event.FocusAdapter() {
            @Override
			public void focusLost(java.awt.event.FocusEvent evt) {
                refWavelengthTextFieldFocusLost(evt);
            }
        });

        refWavelengthUnitLabel.setText("---");
        refWavelengthUnitLabel.setName("refWavelengthUnitLabel"); // NOI18N

        javax.swing.GroupLayout strehlRatioPanelLayout = new javax.swing.GroupLayout(strehlRatioPanel);
        strehlRatioPanel.setLayout(strehlRatioPanelLayout);
        strehlRatioPanelLayout.setHorizontalGroup(
            strehlRatioPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(strehlRatioPanelLayout.createSequentialGroup()
                .addGroup(strehlRatioPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(strehlRatioLabel, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(refWavelengthLabel, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(strehlRatioPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(strehlRatioTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 229, Short.MAX_VALUE)
                    .addComponent(refWavelengthTextField, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(strehlRatioPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(refWavelengthUnitLabel, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(strehlRatioUnitLabel, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );
        strehlRatioPanelLayout.setVerticalGroup(
            strehlRatioPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(strehlRatioPanelLayout.createSequentialGroup()
                .addGroup(strehlRatioPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(strehlRatioLabel)
                    .addComponent(strehlRatioTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(strehlRatioUnitLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(strehlRatioPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(refWavelengthLabel)
                    .addComponent(refWavelengthTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(refWavelengthUnitLabel))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(strehlRatioPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(strehlRatioPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void strehlRatioTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_strehlRatioTextFieldActionPerformed
        updateStrehlRatioInSession();
    }//GEN-LAST:event_strehlRatioTextFieldActionPerformed

    private void strehlRatioTextFieldFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_strehlRatioTextFieldFocusLost
        updateStrehlRatioInSession();
    }//GEN-LAST:event_strehlRatioTextFieldFocusLost

    private void refWavelengthTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_refWavelengthTextFieldActionPerformed
        updateRefWavelentgthInSession();
    }//GEN-LAST:event_refWavelengthTextFieldActionPerformed

    private void refWavelengthTextFieldFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_refWavelengthTextFieldFocusLost
        updateRefWavelentgthInSession();
    }//GEN-LAST:event_refWavelengthTextFieldFocusLost

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel refWavelengthLabel;
    private org.cnrs.lam.dis.etc.ui.swing.generic.DoubleTextField refWavelengthTextField;
    private javax.swing.JLabel refWavelengthUnitLabel;
    private javax.swing.JLabel strehlRatioLabel;
    private javax.swing.JPanel strehlRatioPanel;
    private org.cnrs.lam.dis.etc.ui.swing.generic.PercentageTextField strehlRatioTextField;
    private javax.swing.JLabel strehlRatioUnitLabel;
    // End of variables declaration//GEN-END:variables

    private void updateStrehlRatioInSession() {
        if (getSession().getInstrument().getStrehlRatio() == strehlRatioTextField.getValue())
            return;
        getSession().getInstrument().setStrehlRatio(strehlRatioTextField.getValue());
        addLogLine("set instrument StrehlRatio " + strehlRatioTextField.getValue() + "<br/>");
    }

    private void updateRefWavelentgthInSession() {
        if (getSession().getInstrument().getRefWavelength() == refWavelengthTextField.getValue())
            return;
        getSession().getInstrument().setRefWavelength(refWavelengthTextField.getValue());
        addLogLine("set instrument RefWavelength " + refWavelengthTextField.getValue() + "<br/>");
    }

    @Override
    protected void sessionModified() {
        strehlRatioTextField.setValue(getSession().getInstrument().getStrehlRatio());
        refWavelengthTextField.setValue(getSession().getInstrument().getRefWavelength());
        refWavelengthUnitLabel.setText(getSession().getInstrument().getRefWavelengthUnit());
    }

    @Override
    public void setUserMode(UserMode mode) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    public void handleMode(JSONObject jObject) {
        JSONArray elements = jObject.getJSONArray("disabled");
        for (int i = 0; i < elements.length(); i++) {
            if (elements.getString(i).equalsIgnoreCase("strehl Ratio"))
                strehlRatioTextField.setEnabled(false);
            else if (elements.getString(i).equalsIgnoreCase("ref Wavelength"))
                refWavelengthTextField.setEnabled(false);
        }
    }
}
