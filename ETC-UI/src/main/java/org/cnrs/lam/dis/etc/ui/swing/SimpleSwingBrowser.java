package org.cnrs.lam.dis.etc.ui.swing;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JToolBar;

import org.cnrs.lam.dis.etc.ui.swing.importer.XmlSelectorWebBrowserPanel;
import org.javatuples.Pair;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;
import org.w3c.dom.events.Event;
import org.w3c.dom.events.EventListener;
import org.w3c.dom.events.EventTarget;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.concurrent.Worker;
import javafx.concurrent.Worker.State;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.web.WebHistory;
import javafx.scene.web.WebView;

public class SimpleSwingBrowser extends JPanel {
	
	public static final String EVENT_TYPE_CLICK = "click";
    public static final String EVENT_TYPE_MOUSEOVER = "mouseover";
    public static final String EVENT_TYPE_MOUSEOUT = "mouseclick";

	private static final long serialVersionUID = 2712352829273864743L;
	private WebView browser;
	private JFXPanel browserWrapper = new JFXPanel();
	private JButton btnPrevious = new JButton("Previous");
	private JButton btnNext = new JButton("Next");
	private JToolBar toolbar;
	private XmlSelectorWebBrowserPanel parent;
	
	Runnable browserThread = new Runnable() {
		@Override
		public void run() {
			setLayout(new java.awt.BorderLayout());
			
			browser = new WebView();
			browser.setVisible(true);
			Scene scene = new Scene(browser);
			browserWrapper.setScene(scene);
			
			// Toolbar
			toolbar = new JToolBar("Browser");

			btnPrevious.setEnabled(false);
			btnPrevious.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					goBack();
				}
			});
			toolbar.add(btnPrevious);

			btnNext.setEnabled(false);
			btnNext.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					goForward();
				}
			});
			
			toolbar.add(btnNext);
			
			toolbar.setFloatable(false);
			add(toolbar, BorderLayout.PAGE_START);
			add(browserWrapper);
			
			browser.getEngine().getLoadWorker().stateProperty().addListener(new ChangeListener<State>() {
	            @Override
	            public void changed(ObservableValue ov, State oldState, State newState) {
	                if (newState == Worker.State.SUCCEEDED) {
	                	updateHistorybuttonsState();
	                    EventListener listener = new EventListener() {
	                        @Override
	                        public void handleEvent(Event ev) {
	                            String domEventType = ev.getType();

	                            if (domEventType.equals(EVENT_TYPE_CLICK)) {
	                                String href = ((Element)ev.getTarget()).getAttribute("href");
	                                if (!href.startsWith("#")) {
	                                	parent.onUrlClicked(browser.getEngine().getLocation(), ev, href);
	                                }
	                            } 
	                        }
	                    };
	                    
	                    Document doc = browser.getEngine().getDocument();
	                    NodeList nodeList = doc.getElementsByTagName("a");
	                    for (int i = 0; i < nodeList.getLength(); i++) {
	                        ((EventTarget) nodeList.item(i)).addEventListener(EVENT_TYPE_CLICK, listener, false);
	                    }
	                }
	            }
	        });
		}
		
	};

	public SimpleSwingBrowser(XmlSelectorWebBrowserPanel parent) {
		super();
		this.parent = parent;
		initComponents(parent);
	}
	
	public WebView getBrowser() {
		return this.browser;
	}
	
	private void updateHistorybuttonsState() {
		Pair<Boolean, Boolean> canUndoRedo = getHistoryState();
		System.out.println(canUndoRedo);
		
		btnPrevious.setEnabled(canUndoRedo.getValue0());
		btnNext.setEnabled(canUndoRedo.getValue1());
	}

	private void initComponents(final XmlSelectorWebBrowserPanel parent) {
		Platform.setImplicitExit(false);
		Platform.runLater(browserThread);
	}

	private static String toURL(String str) {
		try {
			return new URL(str).toExternalForm();
		} catch (MalformedURLException exception) {
			return null;
		}
	}

	public void loadURL(final String url) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				String tmp = toURL(url);

				if (url == null) {
					tmp = toURL(url);
				}

				browser.getEngine().load(tmp);
			}
		});
	}
	
	public Pair<Boolean, Boolean> getHistoryState() {
		final WebHistory history = browser.getEngine().getHistory();
		ObservableList<WebHistory.Entry> entryList = history.getEntries();
		int currentIndex = history.getCurrentIndex();
		boolean canUndo = currentIndex > 0;
		boolean canRedo = currentIndex < entryList.size() - 1;
		
		return new Pair<Boolean, Boolean>(canUndo, canRedo);
	}

	public Pair<Boolean, Boolean> goBack() {
		final WebHistory history = browser.getEngine().getHistory();
		int currentIndex = history.getCurrentIndex();
		boolean canUndo = currentIndex > 0;
		
		if (canUndo) {			
			Platform.runLater(new Runnable() {
				public void run() {
					history.go(-1);
				}
			});
		}
		return getHistoryState();
	}

	public Pair<Boolean, Boolean> goForward() {
		final WebHistory history = browser.getEngine().getHistory();
		ObservableList<WebHistory.Entry> entryList = history.getEntries();
		int currentIndex = history.getCurrentIndex();
		boolean canRedo = currentIndex < entryList.size() - 1;

		if (canRedo) {			
			Platform.runLater(new Runnable() {
				public void run() {
					history.go(1);
				}
			});
		}
		return getHistoryState();
	}
}