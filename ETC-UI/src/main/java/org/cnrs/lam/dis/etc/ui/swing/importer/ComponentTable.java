/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.ui.swing.importer;

import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.URL;
import java.text.MessageFormat;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import org.cnrs.lam.dis.etc.datamodel.ComponentType;
import org.cnrs.lam.dis.etc.ui.swing.generic.TableColumnResizer;
import org.javatuples.Triplet;

/**
 */
public class ComponentTable extends JTable {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ResourceBundle bundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/ui/swing/messages");
    private ComponentType type = null;
    private List<String> usedNameList = new LinkedList<String>();
    private ComponentTableModel tableModel = new ComponentTableModel();
    private ImageIcon deleteIcon = new ImageIcon(getClass().getResource("/org/cnrs/lam/dis/etc/ui/swing/images/Delete.png"));

    public ComponentTable() {
        setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        setCellSelectionEnabled(false);
        setModel(tableModel);
        // Set the renderer of the first column to always draw the delete icon
        getColumnModel().getColumn(0).setCellRenderer(new DefaultTableCellRenderer() {
            /**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
            protected void setValue(Object value) {
                super.setIcon(deleteIcon);
            }
        });
        addMouseListener(new DeleteComponentMouseListener());
    }

    /**
     * Sets the type of components this table keeps. Note that this must be set
     * correctly, as it will be used for checking the database for name conflicts.
     * @param type The type of components this table keeps
     */
    public void setType(ComponentType type) {
        this.type = type;
    }
    
    /**
     * Adds a component to the table. Note that if the given name creates a conflict
     * with the names in the database or the existing names in the table, the
     * conflict is solved automatically and the name is changed.
     * @param name The name of the new component
     * @param description The description of the new component
     * @param url The URL of the new component
     */
    public void addComponent(String name, String description, URL url) {
        tableModel.addComponent(name, description, url);
    }
    
    /**
     * Returns the components information the table keeps. It returns a Triplet
     * with the name, description and URL of the component, in this order.
     * @return A list with the information of all the components the table contains
     */
    public List<Triplet<String, String, URL>> getComponentList() {
        return Collections.unmodifiableList(tableModel.dataList);
    }
    
    private class DeleteComponentMouseListener extends MouseAdapter {

        @Override
        public void mouseClicked(MouseEvent e) {
            if (ComponentTable.this.getSelectedColumn() != 0) {
                return;
            }
            int index = ComponentTable.this.getSelectedRow();
            ((ComponentTableModel) ComponentTable.this.getModel()).removeComponent(index);
        }
        
    }
    
    private class ComponentTableModel extends AbstractTableModel {
        
        /**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private List<Triplet<String, String, URL>> dataList = new LinkedList<Triplet<String, String, URL>>();
        
        private void adjustColumnWidths() {
            TableColumnResizer.adjustColumnPreferredWidths(ComponentTable.this);
            if (!dataList.isEmpty()) {
                TableCellRenderer rend = ComponentTable.this.getCellRenderer(0, 0);
                Component comp = rend.getTableCellRendererComponent(ComponentTable.this, null, false, false, 0, 0);
                getColumnModel().getColumn(0).setPreferredWidth(comp.getPreferredSize().width + 1);
            }
        }
        
        void addComponent(String name, String description, URL url) {
            // We first solve any name conflicts with the names in the database
            String uniqueName = ImportHelper.getNonConflictName(name, type, usedNameList);
            // We add the component info in the list
            dataList.add(new Triplet<String, String, URL>(uniqueName, description, url));
            // We add the new name in the used names list so it cannot be used again
            usedNameList.add(uniqueName);
            fireTableDataChanged();
            adjustColumnWidths();
        }
        
        void removeComponent(int index) {
            if (index < 0 || index >= dataList.size())
                return;
            Triplet<String, String, URL> removedInfo = dataList.remove(index);
            usedNameList.remove(removedInfo.getValue0());
            fireTableDataChanged();
            adjustColumnWidths();
        }

        @Override
        public int getRowCount() {
            return dataList.size();
        }

        @Override
        public int getColumnCount() {
            return 4;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            Triplet<String, String, URL> info = dataList.get(rowIndex);
            switch (columnIndex) {
                case 1:
                    return info.getValue0();
                case 2:
                    return info.getValue1();
                case 3:
                    return info.getValue2();
                default:
                    return null;
            }
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            switch (columnIndex) {
                case 0:
                    return ImageIcon.class;
                case 1:
                case 2:
                case 3:
                    return String.class;
                default:
                    return null;
            }
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            switch (columnIndex) {
                case 1:
                case 2:
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
            switch (columnIndex) {
                case 1:
                    setName(rowIndex, aValue.toString());
                    break;
                case 2:
                    setDescription(rowIndex, aValue.toString());
                    break;
            }
            ComponentTable.this.repaint();
            adjustColumnWidths();
        }
        
        private void setName(int index, String name) {
            Triplet<String, String, URL> info = dataList.get(index);
            if (info.getValue0().equals(name))
                return;
            String uniqueName = ImportHelper.getNonConflictName(name, type, usedNameList);
            int option = JOptionPane.YES_OPTION;
            if (!uniqueName.equals(name)) {
                option = JOptionPane.showConfirmDialog(ComponentTable.this, MessageFormat.format(bundle.getString("NAME_CONFLICT_RESOLVE"), name, uniqueName), null, JOptionPane.YES_NO_OPTION);
            }
            if (option == JOptionPane.YES_OPTION) {
                dataList.remove(info);
                dataList.add(index, info.setAt0(uniqueName));
                ComponentTable.this.usedNameList.remove(name);
                ComponentTable.this.usedNameList.add(uniqueName);
            }
        }
        
        private void setDescription(int index, String description) {
            Triplet<String, String, URL> info = dataList.get(index);
            dataList.remove(info);
            dataList.add(index, info.setAt1(description));
        }

        @Override
        public String getColumnName(int column) {
            switch (column) {
                case 1:
                    return bundle.getString("IMPORT_TABLE_NAME");
                case 2:
                    return bundle.getString("IMPORT_TABLE_DESCRIPTION");
                case 3:
                    return bundle.getString("IMPORT_TABLE_URL");
                default:
                    return null;
            }
        }
    }
    
}
