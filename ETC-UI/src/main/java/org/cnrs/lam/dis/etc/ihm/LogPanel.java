/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.ihm;


import java.awt.event.ActionEvent;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import javax.swing.JButton;
import javax.swing.JPopupMenu;

public class LogPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	private JEditorPane areaLogOut = null;
	private StringBuffer logOut = new StringBuffer();
        private JPopupMenu popupMenu = new JPopupMenu();
        private JButton clearButton = new JButton();
        ResourceBundle bundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/ui/swing/messages");
	//private JEditorPane JEditorPane = null;

	/**
	 * This is the default constructor
	 */
	public LogPanel() {
		super();
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
            this.setSize(new Dimension(770, 520));
            this.setLayout(new BorderLayout());
            JScrollPane scrollArea = new JScrollPane(getAreaLogOut(), ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            //	scrollArea.setViewportView(getJEditorPane());
            this.add(scrollArea, BorderLayout.CENTER);

            clearButton.setText(bundle.getString("CLEAR"));
            clearButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    LogPanel.this.resetText();
                }
            });
            JPanel topPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
            topPanel.add(clearButton);
            this.add(topPanel, BorderLayout.NORTH);
            
            resetText();
	}

	/**
	 * This method initializes areaLogOut	
	 * 	
	 * @return javax.swing.JEditorPane	
	 */
	private JEditorPane getAreaLogOut() {
		if (areaLogOut == null) {
			areaLogOut = new JEditorPane();
			//areaLogOut.setLineWrap(true);
			areaLogOut.setEditable(false);
			areaLogOut.setContentType("text/html");
		}
		return areaLogOut;
	}
	/**
	 *  Rajoute du texte   celle d j  pr sente
	 * @param log
	 */
	public void setAreaText(String log){
		logOut.append("<p style=\"margin-top: 0\">").append(log).append("</p>");
		areaLogOut.setText(logOut.toString());
	}

    private void resetText() {
        logOut = new StringBuffer("<head><style type=\"text/css\">"
                                + "<!-- body {font-size:11px; font-family:sans-serif} -->"
                                + "</style></head>");
        areaLogOut.setText(logOut.toString());
    }



}  //  @jve:decl-index=0:visual-constraint="15,33"
