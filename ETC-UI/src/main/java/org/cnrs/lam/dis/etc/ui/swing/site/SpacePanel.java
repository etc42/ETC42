/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.ui.swing.site;

import java.io.File;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFileChooser;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.Dataset.Type;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.cnrs.lam.dis.etc.ui.generic.DatasetListenerHolder;
import org.cnrs.lam.dis.etc.ui.generic.InfoProviderHolder;
import org.cnrs.lam.dis.etc.ui.swing.UserMode;
import org.cnrs.lam.dis.etc.ui.swing.generic.CollapsiblePanel;
import org.cnrs.lam.dis.etc.ui.swing.generic.DatasetDataTypeDialog;
import org.cnrs.lam.dis.etc.ui.swing.generic.InfoListCellRenderer;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class SpacePanel extends CollapsiblePanel {
    
    private DatasetDataTypeDialog datasetDataTypeDialog;

    /** Creates new form SpacePanel */
    public SpacePanel() {
        initComponents();
        datasetDataTypeDialog = new DatasetDataTypeDialog();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        nameDescriptionDialog = new org.cnrs.lam.dis.etc.ui.swing.generic.NameDescriptionDialog();
        fileChooser = new javax.swing.JFileChooser();
        zodiacalCheckBox = new javax.swing.JCheckBox();
        zodiacalComboBox = new javax.swing.JComboBox();
        plotZodiacalButton = new javax.swing.JButton();
        addZodiacalButton = new javax.swing.JButton();
        galacticCheckBox = new javax.swing.JCheckBox();
        galacticComboBox = new javax.swing.JComboBox();
        plotGalacticButton = new javax.swing.JButton();
        addGalacticButton = new javax.swing.JButton();

        nameDescriptionDialog.setModal(true);
        nameDescriptionDialog.setName("nameDescriptionDialog"); // NOI18N

        fileChooser.setName("fileChooser"); // NOI18N

        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("org/cnrs/lam/dis/etc/ui/swing/messages"); // NOI18N
        setBorder(javax.swing.BorderFactory.createTitledBorder(null, bundle.getString("SPACE"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("DejaVu Sans", 0, 12))); // NOI18N

        zodiacalCheckBox.setText(bundle.getString("ZODIACAL")); // NOI18N
        zodiacalCheckBox.setName("zodiacalCheckBox"); // NOI18N
        zodiacalCheckBox.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                zodiacalCheckBoxActionPerformed(evt);
            }
        });

        zodiacalComboBox.setName("zodiacalComboBox"); // NOI18N
        zodiacalComboBox.setRenderer(new InfoListCellRenderer());
        zodiacalComboBox.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                zodiacalComboBoxActionPerformed(evt);
            }
        });

        plotZodiacalButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/cnrs/lam/dis/etc/ui/swing/images/Plot.png"))); // NOI18N
        plotZodiacalButton.setName("plotZodiacalButton"); // NOI18N
        plotZodiacalButton.setPreferredSize(new java.awt.Dimension(28, 28));
        plotZodiacalButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                plotZodiacalButtonActionPerformed(evt);
            }
        });

        addZodiacalButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/cnrs/lam/dis/etc/ui/swing/images/Add.png"))); // NOI18N
        addZodiacalButton.setName("addZodiacalButton"); // NOI18N
        addZodiacalButton.setPreferredSize(new java.awt.Dimension(28, 28));
        addZodiacalButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                addZodiacalButtonActionPerformed(evt);
            }
        });

        galacticCheckBox.setText(bundle.getString("GALACTIC")); // NOI18N
        galacticCheckBox.setName("galacticCheckBox"); // NOI18N
        galacticCheckBox.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                galacticCheckBoxActionPerformed(evt);
            }
        });

        galacticComboBox.setName("galacticComboBox"); // NOI18N
        galacticComboBox.setRenderer(new InfoListCellRenderer());
        galacticComboBox.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                galacticComboBoxActionPerformed(evt);
            }
        });

        plotGalacticButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/cnrs/lam/dis/etc/ui/swing/images/Plot.png"))); // NOI18N
        plotGalacticButton.setName("plotGalacticButton"); // NOI18N
        plotGalacticButton.setPreferredSize(new java.awt.Dimension(28, 28));
        plotGalacticButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                plotGalacticButtonActionPerformed(evt);
            }
        });

        addGalacticButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/cnrs/lam/dis/etc/ui/swing/images/Add.png"))); // NOI18N
        addGalacticButton.setName("addGalacticButton"); // NOI18N
        addGalacticButton.setPreferredSize(new java.awt.Dimension(28, 28));
        addGalacticButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                addGalacticButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(zodiacalCheckBox)
                    .addComponent(galacticCheckBox)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(zodiacalComboBox, 0, 264, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(addZodiacalButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(plotZodiacalButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(galacticComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(addGalacticButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(plotGalacticButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(zodiacalCheckBox)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(zodiacalComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(addZodiacalButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(plotZodiacalButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addComponent(galacticCheckBox)
                .addGap(2, 2, 2)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(galacticComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(addGalacticButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(plotGalacticButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void zodiacalCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_zodiacalCheckBoxActionPerformed
        boolean contributes = zodiacalCheckBox.isSelected();
        getSession().getSite().setZodiacalContributing(contributes);
        addLogLine("set site ZodiacalContributing " + contributes + "<br/>");
        zodiacalContributes(contributes);
    }//GEN-LAST:event_zodiacalCheckBoxActionPerformed

    private void galacticCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_galacticCheckBoxActionPerformed
        boolean contributes = galacticCheckBox.isSelected();
        getSession().getSite().setGalacticContributing(contributes);
        addLogLine("set site GalacticContributing " + contributes + "<br/>");
        galacticContributes(contributes);
    }//GEN-LAST:event_galacticCheckBoxActionPerformed

    private void zodiacalComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_zodiacalComboBoxActionPerformed
        getSession().getSite().setZodiacalProfile((DatasetInfo) zodiacalComboBox.getSelectedItem());
        addLogLine("set site ZodiacalProfile " + getSession().getSite().getZodiacalProfile() + "<br/>");
    }//GEN-LAST:event_zodiacalComboBoxActionPerformed

    private void galacticComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_galacticComboBoxActionPerformed
        getSession().getSite().setGalacticProfile((DatasetInfo) galacticComboBox.getSelectedItem());
        addLogLine("set site GalacticProfile " + getSession().getSite().getGalacticProfile() + "<br/>");
    }//GEN-LAST:event_galacticComboBoxActionPerformed

    private void addZodiacalButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addZodiacalButtonActionPerformed
        importDataset(Type.ZODIACAL);
        addLogLine("<b>--- Importing datasets is not yet supported in command line mode ---</b><br/>");
    }//GEN-LAST:event_addZodiacalButtonActionPerformed

    private void addGalacticButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addGalacticButtonActionPerformed
        importDataset(Type.GALACTIC);
        addLogLine("<b>--- Importing datasets is not yet supported in command line mode ---</b><br/>");
    }//GEN-LAST:event_addGalacticButtonActionPerformed

    private void plotZodiacalButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_plotZodiacalButtonActionPerformed
        DatasetListenerHolder.getDatasetListener().previewDataset(Type.ZODIACAL, getSession().getSite().getZodiacalProfile(), null);
    }//GEN-LAST:event_plotZodiacalButtonActionPerformed

    private void plotGalacticButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_plotGalacticButtonActionPerformed
        DatasetListenerHolder.getDatasetListener().previewDataset(Type.GALACTIC, getSession().getSite().getGalacticProfile(), null);
    }//GEN-LAST:event_plotGalacticButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addGalacticButton;
    private javax.swing.JButton addZodiacalButton;
    private javax.swing.JFileChooser fileChooser;
    private javax.swing.JCheckBox galacticCheckBox;
    private javax.swing.JComboBox galacticComboBox;
    private org.cnrs.lam.dis.etc.ui.swing.generic.NameDescriptionDialog nameDescriptionDialog;
    private javax.swing.JButton plotGalacticButton;
    private javax.swing.JButton plotZodiacalButton;
    private javax.swing.JCheckBox zodiacalCheckBox;
    private javax.swing.JComboBox zodiacalComboBox;
    // End of variables declaration//GEN-END:variables

    private void importDataset(Type type) {
        int selectedOption = fileChooser.showOpenDialog(this);
        if (selectedOption != JFileChooser.APPROVE_OPTION) {
            return;
        }
        File file = fileChooser.getSelectedFile();
        nameDescriptionDialog.setVisible(this, true);
        if (!nameDescriptionDialog.isOkSelected()) {
            return;
        }
        String name = nameDescriptionDialog.getSelectedName();
        String description = nameDescriptionDialog.getSelectedDescription();
        String namespace = getSession().getSite().getInfo().getName();
        datasetDataTypeDialog.setVisible(this, true, type);
        if (!datasetDataTypeDialog.isOkSelected())
            return;
        Dataset.DataType dataType = datasetDataTypeDialog.getSelectedDataType();
        DatasetListenerHolder.getDatasetListener().importFromFile(file, type, new DatasetInfo(name, namespace, description), dataType);
    }

    private void zodiacalContributes(boolean contributes) {
        zodiacalComboBox.setEnabled(contributes);
        addZodiacalButton.setEnabled(contributes);
        plotZodiacalButton.setEnabled(contributes);
    }

    private void galacticContributes(boolean contributes) {
        galacticComboBox.setEnabled(contributes);
        addGalacticButton.setEnabled(contributes);
        plotGalacticButton.setEnabled(contributes);
    }

    @Override
    protected void sessionModified() {
        boolean contributes = getSession().getSite().isZodiacalContributing();
        zodiacalCheckBox.setSelected(contributes);
        if (System.getProperty("etc.fixtures") == null)
            zodiacalContributes(contributes);
        contributes = getSession().getSite().isGalacticContributing();
        galacticCheckBox.setSelected(contributes);
        if (System.getProperty("etc.fixtures") == null)
            galacticContributes(contributes);
        zodiacalComboBox.setModel(new DefaultComboBoxModel(InfoProviderHolder.getInfoProvider().getAvailableDatasetList(
                Type.ZODIACAL, getSession().getSite().getInfo().getName()).toArray()));
        zodiacalComboBox.setSelectedItem(getSession().getSite().getZodiacalProfile());
        galacticComboBox.setModel(new DefaultComboBoxModel(InfoProviderHolder.getInfoProvider().getAvailableDatasetList(
                Type.GALACTIC, getSession().getSite().getInfo().getName()).toArray()));
        galacticComboBox.setSelectedItem(getSession().getSite().getGalacticProfile());
    }

    @Override
    public void setUserMode(UserMode mode) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    public void handleMode(JSONObject jObject) {
        JSONArray elements = jObject.getJSONArray("disabled");
        for (int i = 0; i < elements.length(); i++) {
            if (elements.getString(i).equalsIgnoreCase("zodiacal check"))
                zodiacalCheckBox.setEnabled(false);
            else if (elements.getString(i).equalsIgnoreCase("zodiacal"))
                zodiacalComboBox.setEnabled(false);
            else if (elements.getString(i).equalsIgnoreCase("add zodiacal"))
                addZodiacalButton.setEnabled(false);
            else if (elements.getString(i).equalsIgnoreCase("galactic check"))
                galacticCheckBox.setEnabled(false);
            else if (elements.getString(i).equalsIgnoreCase("galactic"))
                galacticComboBox.setEnabled(false);
            else if (elements.getString(i).equalsIgnoreCase("add galactic"))
                addGalacticButton.setEnabled(false);
        }
    }
    
}
