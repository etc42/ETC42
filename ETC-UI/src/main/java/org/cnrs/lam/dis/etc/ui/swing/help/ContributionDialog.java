package org.cnrs.lam.dis.etc.ui.swing.help;

import java.awt.Cursor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

public class ContributionDialog extends HelpDialog {

	private static final long serialVersionUID = 3773082265462219174L;
	private static ContributionDialog instance = null;
	
	private final static String SOURCE_URL = "https://gitlab.lam.fr/etc42/ETC42";

	public ContributionDialog(JFrame parent) {
		super(parent, "Contribute");
		
		JPanel wrapper = new JPanel();
		wrapper.setLayout(new BoxLayout(wrapper, BoxLayout.PAGE_AXIS));
		wrapper.setBorder(new EmptyBorder(15, 15, 15, 15));
		
		
		String intro = "<html>"
			+ "<div><p>We are always looking for contributors.</p>" + 
			"<p>A contribution can be a feature request, any issue you can find, or even preset sharing.</p>" + 
			"<ul>" + 
			"<li>If you have any feature request, you can create an issue with the \"enhancement\" label.</li><br>" + 
			"<li>To notify us you found an issue in the software, you can create an issue with the \"bug\" or \"critical\" label.</li><br>" + 
			"<li>Have you made your own source, instrument, site or observing parameters ?<br>Or even a more simple preset you would like to share, such as a sky emission template ?<br>You are very welcome to share it with the community.<br>Contact us so we can add it in the official repository !<br>Of course, your name will be added in the contributors list, and in the preset name itself.</li><br>" + 
			"</ul>" + 
			"</div>"
			+ "<div style='text-align:center;'>Thank you for your incoming contributions !</div></html>";
		JLabel introLabel = new JLabel(intro, SwingConstants.CENTER);		
		wrapper.add(introLabel);
		HyperlinkLabel contactUrlLabel = new HyperlinkLabel(AboutDialog.CONTACT_URL, AboutDialog.CONTACT_URL, SwingConstants.CENTER);
		contactUrlLabel.setBorder(new EmptyBorder(0, 0, 15, 0));
		wrapper.add(contactUrlLabel);
		
		
		
		// Code source linking
		String source = "<html><div style='text-align: center;'><br>You can access the ETC-42 repository:</div><br></html>";
		JLabel sourceLabel = new JLabel(source, SwingConstants.CENTER);
		HyperlinkLabel sourceURLLabel = new HyperlinkLabel(SOURCE_URL, SOURCE_URL, SwingConstants.CENTER);
		sourceURLLabel.setBorder(new EmptyBorder(0, 0, 15, 0));
		
		JPanel important = new JPanel();
		important.setLayout(new BoxLayout(important, BoxLayout.Y_AXIS));
		important.setBorder(BorderFactory.createTitledBorder("Repository"));
		important.add(sourceLabel);
		important.add(sourceURLLabel);
		important.setAlignmentX(LEFT_ALIGNMENT);
		wrapper.add(important);
		
		getContentPane().add(wrapper);
		pack();
	}
	
	public static void show(JFrame parent) {
		if (instance == null) {
			instance = new ContributionDialog(parent);
		}
		instance.setVisible(true);
	}

}
