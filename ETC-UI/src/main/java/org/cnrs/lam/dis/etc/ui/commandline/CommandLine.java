/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.ui.commandline;

import cds.savot.model.SavotVOTable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import org.apache.log4j.Logger;
import org.cnrs.lam.dis.etc.configuration.ConfigFactory;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.DoubleDatasetResult;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.DoubleValueResult;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.LongDatasetResult;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.LongDoubleDatasetResult;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.LongValueResult;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Result;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.StringResult;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.cnrs.lam.dis.etc.ui.BuisnessListener;
import org.cnrs.lam.dis.etc.ui.DatasetListener;
import org.cnrs.lam.dis.etc.ui.InfoProvider;
import org.cnrs.lam.dis.etc.ui.InstrumentListener;
import org.cnrs.lam.dis.etc.ui.Messenger;
import org.cnrs.lam.dis.etc.ui.ObsParamListener;
import org.cnrs.lam.dis.etc.ui.SampListener;
import org.cnrs.lam.dis.etc.ui.SessionListener;
import org.cnrs.lam.dis.etc.ui.SiteListener;
import org.cnrs.lam.dis.etc.ui.SourceListener;
import org.cnrs.lam.dis.etc.ui.UIManager;
import org.cnrs.lam.dis.etc.ui.commandline.commands.Command;
import org.cnrs.lam.dis.etc.ui.commandline.commands.CommandFactory;
import org.cnrs.lam.dis.etc.ui.generic.BuisnessListenerHolder;
import org.cnrs.lam.dis.etc.ui.generic.DatasetListenerHolder;
import org.cnrs.lam.dis.etc.ui.generic.InfoProviderHolder;
import org.cnrs.lam.dis.etc.ui.generic.InstrumentListenerHolder;
import org.cnrs.lam.dis.etc.ui.generic.ObsParamListenerHolder;
import org.cnrs.lam.dis.etc.ui.generic.SampListenerHolder;
import org.cnrs.lam.dis.etc.ui.generic.SessionHolder;
import org.cnrs.lam.dis.etc.ui.generic.SiteListenerHolder;
import org.cnrs.lam.dis.etc.ui.generic.SourceListenerHolder;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class CommandLine extends Thread implements Messenger, UIManager {
    
    private static final Logger logger = Logger.getLogger(CommandLine.class);

    private static CommandLine instance = null;
    private volatile boolean insideAction = false;
    private volatile Thread actionThread = null;

    public static CommandLine getInstance() {
        if (instance == null) {
            instance = new CommandLine();
            instance.start();
        }
        return instance;
    }

    @Override
    public void run() {
        // First wait until the session is not null, to be sure the initialization
        // of the rest has finished. We do that for maximum 30 seconds. Note that
        // we have to do that because this is a different thread and it might be
        // initialized earlier than the persistence module. This could lead to a
        // failure when the command line runs with a script file as parameter.
        double timestamp = Calendar.getInstance().getTimeInMillis();
        while (SessionHolder.getSession() == null) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                // We ignore this exception
            }
            if (Calendar.getInstance().getTimeInMillis() > timestamp + 30000) {
                logger.warn("Session has not been initialized after 30 seconds. "
                        + "Continuing the command line UI mode.");
                break;
            }
        }
        // Check the command line arguments for a script parameter
        CommandLineArgumentsHandler argumentsHandler = new CommandLineArgumentsHandler
                (ConfigFactory.getConfig().getCommandLineArguments());
        if (argumentsHandler.getScriptFile() != null) {
            try {
                ScannerHolder.setScanner(new Scanner(argumentsHandler.getScriptFile()));
                PromptHolder.setPrompt("");
            } catch (FileNotFoundException ex) {
                System.out.println("Script " + argumentsHandler.getScriptFile() + " does not exist");
                System.exit(0);
            }
        } else {
            ScannerHolder.setScanner(new Scanner(System.in));
        }
        System.out.println("ETC42 - Command Mode");
        System.out.println("--------------------");
        System.out.println("");
        while (true) {
            // If we are inside a long action wait for the thread to finish
            while (insideAction) {
                try {
                    actionThread.join(1000);
                } catch (Exception e) {
                }
            }
            System.out.print(PromptHolder.getPrompt());
            System.out.flush();
            String userCommand = null;
            try {
                userCommand = ScannerHolder.getScanner().next();
            } catch (Exception e) {
                // We come here if the scanner was changed to read a script and the
                // script finished. So, set again to read from the standard in and
                // continue or exit if the -e parameter is given with a script name
                if (argumentsHandler.isExitAfterScriptExecution()) {
                    System.exit(0);
                }
                ScannerHolder.setScanner(new Scanner(System.in));
                // Reset the prompt
                PromptHolder.setPrompt(null);
                continue;
            }
            // If we are inside a long action wait for the thread to finish
            // This test is happening extra for the case the user had enough time
            // to give the new command before an action finished. This command
            // will be executed after the action is done.
            while (insideAction) {
                try {
                    actionThread.join(1000);
                } catch (Exception e) {
                }
            }
            Command command = CommandFactory.getCommand(userCommand);
            if (command == null) {
                System.out.println(userCommand + ": Command not found");
                continue;
            }
            command.execute();
        }
    }

    @Override
    public void info(String message) {
        System.out.println(message);
    }

    @Override
    public void warn(String message) {
        System.out.println("WARNING");
        System.out.println(message);
    }

    @Override
    public void error(String message) {
        System.out.println("ERROR");
        System.out.println(message);
    }

    @Override
    public void actionStarted(String message, Thread thread) {
        insideAction = true;
        actionThread = thread;
        System.out.println(message);
    }

    @Override
    public void actionStoped(Thread thread) {
        insideAction = false;
        //actionThread.notifyAll();
    }

    @Override
    public String decision(String message, List<String> options) {
        System.out.println(message);
        for (int i = 0; i < options.size(); i++) {
            System.out.println(" " + (i+1) + ". " + options.get(i));
        }
        System.out.print("Selection: ");
        int selected;
        while ((selected = requestDecisionSelection(options.size())) == -1) {
            // Do nothing here
        }
        return options.get(selected - 1);
    }

    private int requestDecisionSelection(int numberOfOptions) {
        try {
            int selected = ScannerHolder.getScanner().nextInt();
            if (selected < 1 || selected > numberOfOptions)
                throw new Exception();
            return selected;
        } catch (Exception ex) {
            System.out.println("Please select a number from 1 to " + numberOfOptions);
            return -1;
        }
    }

    @Override
    public String requestInput(String message, List<String> possibilities) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public DatasetInfo getNewDatasetInfo() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void setSession(Session session) {
        SessionHolder.setSession(session);
    }

    @Override
    public void sessionModified() {
        // We do nothing here, as the command line does not have a representation of the session
    }

    @Override
    public void setInfoProvider(InfoProvider provider) {
        InfoProviderHolder.setInfoProvider(provider);
    }

    @Override
    public void setSessionListener(SessionListener listener) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void setInstrumentListener(InstrumentListener listener) {
        InstrumentListenerHolder.setInstrumentListener(listener);
    }

    @Override
    public void setSiteListener(SiteListener listener) {
        SiteListenerHolder.setSiteListener(listener);
    }

    @Override
    public void setSourceListener(SourceListener listener) {
        SourceListenerHolder.setSourceListener(listener);
    }

    @Override
    public void setObsParamListener(ObsParamListener listener) {
        ObsParamListenerHolder.setObsParamListener(listener);
    }

    @Override
    public void setDatasetListener(DatasetListener listener) {
        DatasetListenerHolder.setDatasetListener(listener);
    }

    @Override
    public void setBuisnessListener(BuisnessListener listener) {
        BuisnessListenerHolder.setBuisnessListener(listener);
    }

    @Override
    public void showPlot(String title, String xDescription, String yDescription, String xUnit, String yUnit, List<Dataset> datasetList) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void showCalculationResult(CalculationResults result) {
        Map<String, Result> results  = new HashMap<String, Result>();
        switch (ResultOutputHolder.getLevel()) {
            case DEBUG:
                results.putAll(result.getResults(CalculationResults.Level.DEBUG));
            case INTERMEDIATE_UNIMPORTANT:
                results.putAll(result.getResults(CalculationResults.Level.INTERMEDIATE_UNIMPORTANT));
            case INTERMEDIATE_IMPORTANT:
                results.putAll(result.getResults(CalculationResults.Level.INTERMEDIATE_IMPORTANT));
            case FINAL:
                results.putAll(result.getResults(CalculationResults.Level.FINAL));
        }
        if (results.isEmpty())
            return;
        System.out.println("");
        System.out.println("Calculation Results");
        System.out.println("-------------------");
        for (Map.Entry<String, Result> entry : results.entrySet()) {
            saveResult(entry.getKey(), entry.getValue());
        }
        System.out.println("");
    }

    @Override
    public void setSampListener(SampListener listener) {
        SampListenerHolder.setSampListener(listener);
    }

    private void saveResult(String name, Result result) {
        File file = new File(CurrentDirectoryHolder.getCurrentDirectory(), ResultOutputHolder.getPrefix() + name);
        file.getParentFile().mkdirs();
        if (result instanceof DoubleValueResult)
            saveResult(file, (DoubleValueResult) result);
        else if (result instanceof LongValueResult)
            saveResult(file, (LongValueResult) result);
        else if (result instanceof StringResult)
            saveResult(file, (StringResult) result);
        else if (result instanceof DoubleDatasetResult)
            saveResult(file, (DoubleDatasetResult) result);
        else if (result instanceof LongDatasetResult)
            saveResult(file, (LongDatasetResult) result);
        else if (result instanceof LongDoubleDatasetResult)
            saveResult(file, (LongDoubleDatasetResult) result);
    }

    private void saveResult(File file, DoubleValueResult doubleValueResult) {
        String result = doubleValueResult.getName() + " = " + doubleValueResult.getValue();
        if (doubleValueResult.getUnit() != null && doubleValueResult.getUnit().length() != 0)
            result += " (" + doubleValueResult.getUnit() + ")";
        System.out.println(result);
        try {
            FileWriter out = new FileWriter(file);
            out.write(result);
            out.write("\n");
            out.close();
        } catch (IOException ex) {
            System.out.println("Failed to create output file " + file);
        }
    }

    private void saveResult(File file, LongValueResult longValueResult) {
        String result = longValueResult.getName() + " = " + longValueResult.getValue();
        if (longValueResult.getUnit() != null && longValueResult.getUnit().length() != 0)
            result += " (" + longValueResult.getUnit() + ")";
        System.out.println(result);
        try {
            FileWriter out = new FileWriter(file);
            out.write(result);
            out.write("\n");
            out.close();
        } catch (IOException ex) {
            System.out.println("Failed to create output file " + file);
        }
    }

    private void saveResult(File file, StringResult stringResult) {
        String result = stringResult.getName() + " = " + stringResult.getValue();
        System.out.println(result);
        try {
            FileWriter out = new FileWriter(file);
            out.write(result);
            out.write("\n");
            out.close();
        } catch (IOException ex) {
            System.out.println("Failed to create output file " + file);
        }
    }

    private void saveResult(File file, DoubleDatasetResult doubleDatasetResult) {
        System.out.println(doubleDatasetResult.getName() + " data saved in file " + file);
        try {
            FileWriter out = new FileWriter(file);
            out.write("Units: ");
            if (doubleDatasetResult.getXUnit() == null || doubleDatasetResult.getXUnit().length() == 0)
                out.write("none\t");
            else
                out.write(doubleDatasetResult.getXUnit() + "\t");
            if (doubleDatasetResult.getYUnit() == null || doubleDatasetResult.getYUnit().length() == 0)
                out.write("none\n\n");
            else
                out.write(doubleDatasetResult.getYUnit() + "\n\n");
            for (Map.Entry<Double, Double> entry : doubleDatasetResult.getValues().entrySet()) {
                out.write(entry.getKey() + "\t" + entry.getValue() + "\n");
            }
            out.close();
        } catch (IOException ex) {
            System.out.println("Failed to create output file " + file);
        }
    }

    private void saveResult(File file, LongDatasetResult longDatasetResult) {
        System.out.println(longDatasetResult.getName() + " data saved in file " + file);
        try {
            FileWriter out = new FileWriter(file);
            out.write("Units: ");
            if (longDatasetResult.getXUnit() == null || longDatasetResult.getXUnit().length() == 0)
                out.write("none\t");
            else
                out.write(longDatasetResult.getXUnit() + "\t");
            if (longDatasetResult.getYUnit() == null || longDatasetResult.getYUnit().length() == 0)
                out.write("none\n\n");
            else
                out.write(longDatasetResult.getYUnit() + "\n\n");
            for (Map.Entry<Long, Long> entry : longDatasetResult.getValues().entrySet()) {
                out.write(entry.getKey() + "\t" + entry.getValue() + "\n");
            }
            out.close();
        } catch (IOException ex) {
            System.out.println("Failed to create output file " + file);
        }
    }

    private void saveResult(File file, LongDoubleDatasetResult longDoubleDatasetResult) {
        System.out.println(longDoubleDatasetResult.getName() + " data saved in file " + file);
        try {
            FileWriter out = new FileWriter(file);
            out.write("Units: ");
            if (longDoubleDatasetResult.getXUnit() == null || longDoubleDatasetResult.getXUnit().length() == 0)
                out.write("none\t");
            else
                out.write(longDoubleDatasetResult.getXUnit() + "\t");
            if (longDoubleDatasetResult.getYUnit() == null || longDoubleDatasetResult.getYUnit().length() == 0)
                out.write("none\n\n");
            else
                out.write(longDoubleDatasetResult.getYUnit() + "\n\n");
            for (Map.Entry<Long, Double> entry : longDoubleDatasetResult.getValues().entrySet()) {
                out.write(entry.getKey() + "\t" + entry.getValue() + "\n");
            }
            out.close();
        } catch (IOException ex) {
            System.out.println("Failed to create output file " + file);
        }
    }

    @Override
    public void pluginListChanged() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void handleFirstInstallation() {
        // do nothing here (for the moment)
    }

    @Override
    public void showChart(String title, SavotVOTable voTable, File voTableFile) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
