/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.ui.commandline.commands;

import java.util.HashMap;
import java.util.Map;
import org.cnrs.lam.dis.etc.datamodel.Dataset.Type;

/**
 *
 * @author Nikolaos Apostolakos
 */
final class Helper {

    private static Map<String, Type> datasetTypeMap;
    static {
        datasetTypeMap = new HashMap<String, Type>();
        datasetTypeMap.put("FilterTransmission", Type.FILTER_TRANSMISSION);
        datasetTypeMap.put("Fwhm", Type.FWHM);
        datasetTypeMap.put("SpectralResolution", Type.SPECTRAL_RESOLUTION);
        datasetTypeMap.put("Transmission", Type.TRANSMISSION);
        datasetTypeMap.put("GalacticProfile", Type.GALACTIC);
        datasetTypeMap.put("ZodiacalProfile", Type.ZODIACAL);
        datasetTypeMap.put("SkyAbsorption", Type.SKY_ABSORPTION);
        datasetTypeMap.put("SkyEmission", Type.SKY_EMISSION);
        datasetTypeMap.put("SkyExtinction", Type.SKY_EXTINCTION);
        datasetTypeMap.put("SpectralDistributionTemplate", Type.SPECTRAL_DIST_TEMPLATE);
    }

    public static Map<String, Type> getDatasetTypeMap() {
        return datasetTypeMap;
    }

}
