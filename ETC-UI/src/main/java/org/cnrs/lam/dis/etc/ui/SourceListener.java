/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.ui;

import java.io.File;
import org.cnrs.lam.dis.etc.datamodel.ComponentInfo;

/**
 *
 * @author Nikolaos Apostolakos
 */
public interface SourceListener {

    /**
     * This method is called when the listener gets notified that the user
     * wants to create a new source. This source will become the
     * current source and it will replace the old one.
     * @param newSourceInfo The name and description of the new source
     */
    public void createNewSource(ComponentInfo newSourceInfo);

    /**
     * This method is called when the listener gets notified that the user
     * wants to load a specific source. This source will be loaded as the source of
     * the current session and it will replace the old one. The implementations
     * of the listener should take care so that unsaved information of the old
     * source does not get lost without the willness of the user.
     *
     * @param sourceInfo The information to identify the source to load
     */
    public void loadSource(ComponentInfo sourceInfo);

    /**
     * This method is called when the listener gets notified that the user
     * wants to reload the current source. This means that the user wants
     * to undo all the unsaved modifications. The implementations
     * of the listener should take care so that unsaved information of the old
     * source does not get lost without the willness of the user.
     */
    public void reloadCurrentSource();

    /**
     * This method is called when the listener gets notified that the user
     * wants to save the modifications he has done at the current source.
     */
    public void saveCurrentSource();

    /**
     * This method is called when the listener gets notified that the user
     * wants to save the current source (together with any modifications
     * done from last loaded) as a new source.
     * @param newSourceInfo Contains the name and the description of the
     * new source.
     */
    public void saveCurrentSourceAs(ComponentInfo newSourceInfo);

    public void deleteSource(ComponentInfo sourceInfo);

    /**
     * This method is called when the listener gets notified that the user
     * wants to import a source form a file.
     * @param file The file to import from
     * @param sourceInfo The name of the new source
     */
    public void importFromFile(File file, ComponentInfo sourceInfo);

    public void exportCurrentSourceInFile(File file);

    public boolean isCurrentSourceModified();

}
