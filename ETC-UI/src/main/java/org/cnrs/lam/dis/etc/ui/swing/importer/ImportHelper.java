/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.ui.swing.importer;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.log4j.Logger;
import org.cnrs.lam.dis.etc.configuration.ConfigFactory;
import org.cnrs.lam.dis.etc.datamodel.ComponentInfo;
import org.cnrs.lam.dis.etc.datamodel.ComponentType;
import org.cnrs.lam.dis.etc.ui.generic.InfoProviderHolder;
import org.javatuples.Triplet;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 */
public class ImportHelper {
    
    private static Logger logger = Logger.getLogger(ImportHelper.class);
    
    public static FileImportWorker importPresets(List<String> presets, Class<?> resourceClass) {
    	ArrayList<Triplet<String, String, URL>> sources = new ArrayList<Triplet<String, String, URL>>();
		ArrayList<Triplet<String, String, URL>> sites = new ArrayList<Triplet<String, String, URL>>();
		ArrayList<Triplet<String, String, URL>> instruments = new ArrayList<Triplet<String, String, URL>>();
		ArrayList<Triplet<String, String, URL>> obs_params = new ArrayList<Triplet<String, String, URL>>();

		for (String resource : presets) {
			URL resourceURL = resourceClass.getResource(resource);
			Triplet<ComponentType, String, String> infoFromUrl = ImportHelper.getInfoFromUrl(resourceURL);
			switch (infoFromUrl.getValue0()) {
			case SOURCE:
				sources.add(new Triplet<String, String, URL>(infoFromUrl.getValue1(), infoFromUrl.getValue2(), resourceURL));
				break;
			case SITE:
				sites.add(new Triplet<String, String, URL>(infoFromUrl.getValue1(), infoFromUrl.getValue2(), resourceURL));
				break;
			case INSTRUMENT:
				instruments.add(new Triplet<String, String, URL>(infoFromUrl.getValue1(), infoFromUrl.getValue2(), resourceURL));
				break;
			case OBS_PARAM:
				obs_params.add(new Triplet<String, String, URL>(infoFromUrl.getValue1(), infoFromUrl.getValue2(), resourceURL));
			}
		}

		FileImportWorker worker = new FileImportWorker(null, null, instruments, sites, sources, obs_params);
		worker.execute();
		
		return worker;
    }
    
    public static Triplet<ComponentType, String, String> getInfoFromUrl(URL url) {    	
        File tempFile = downloadUrlBeginning(url);
        return getInfoFromXml(tempFile);
    }
    
    public static File getResourceAsFile(URL url) throws InterruptedException {
    	File downloadedFile = null;
    	try {
            int bufSize = 8 * 1024;
            BufferedInputStream urlStream = new BufferedInputStream(url.openStream(), bufSize);
            File tempDir = new File(ConfigFactory.getConfig().getTempDir());
            tempDir.mkdirs();
            String fileName = url.getFile();
            if (fileName.contains("/")) {
                fileName = fileName.substring(fileName.lastIndexOf('/') + 1);
            }
            downloadedFile = new File(tempDir, fileName);
            downloadedFile.delete();
            downloadedFile.createNewFile();
            downloadedFile.deleteOnExit();
            BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(downloadedFile), bufSize);
            byte[] buffer = new byte[bufSize];
            int read = -1;
            while ((read = urlStream.read(buffer)) >= 0) {
                out.write(buffer, 0, read);
            }
            out.flush();
            urlStream.close();
            out.close();
        } catch (IOException ex) {
            logger.error("Failed to create temporary file", ex);
        }
    	
		return downloadedFile;
    }

    private static File downloadUrlBeginning(URL url) {
        File downloadedFile = null;
        try {
        	
            File tempDir = new File(ConfigFactory.getConfig().getTempDir());
            tempDir.mkdirs();
            String fileName = url.getFile();
            if (fileName.contains("/")) {
                fileName = fileName.substring(fileName.lastIndexOf('/') + 1);
            }
            downloadedFile = new File(tempDir, fileName);
            downloadedFile.delete();
            downloadedFile.createNewFile();
            downloadedFile.deleteOnExit();
        	
            URLConnection connection = url.openConnection();

            // Then write in the file
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            FileWriter fw = new FileWriter(downloadedFile);
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
            	fw.write(inputLine);
            }
            fw.close();
        } catch (IOException ex) {
            logger.error("Failed to create temporary file", ex);
            downloadedFile = null;
        }
        return downloadedFile;
    }
    
    public static Triplet<ComponentType, String, String> getInfoFromXml(File file) {
        // First we check that we actually have an XML file
        if (!isXmlFile(file)) {
            return null;
        }
        SAXParserFactory spf = SAXParserFactory.newInstance();
        ComponentHandler handler = new ComponentHandler();
        ComponentType type = null;
        String name = null;
        String description = null;
        try {
            SAXParser parser = spf.newSAXParser();
            parser.parse(file, handler);
        } catch (InfoTagFound ex) {
            name = handler.name;
            description = handler.description;
        } catch (Exception ex) {
            String fileName = file.getName();
            if (fileName.length() > 4 && fileName.substring(fileName.length() - 4).equalsIgnoreCase(".xml")) {
                fileName = fileName.substring(0, fileName.length() - 4);
            }
            name = fileName;
        }
        type = handler.type;
        return new Triplet<ComponentType, String, String>(type, name, description);
    }

    // Checks that the given file starts as an XML file. Note that it uses the
    // fact that all the ETC exported files start with the XML declaration
    private static boolean isXmlFile(File file) {
        boolean isXml = false;
        try {
            FileReader reader = new FileReader(file);
            int i = 0;
            char[] start = new char[5];
            while (reader.ready() && i < 5) {
                int c = reader.read();
                if (c != ' ' && c != '\t' && c != '\n' && c != '\r') {
                    start[i] = (char) c;
                    i++;
                }
            }
            if (i == 5) {
                String startString = new String(start);
                if ("<?xml".equalsIgnoreCase(startString)) {
                    isXml = true;
                }
            }
            reader.close();
        } catch (Exception ex) {
            return false;
        }
        
        return isXml;
    }
    
    private static class ComponentHandler extends DefaultHandler {
        
        private ComponentType type = null;
        private String name = null;
        private String description = null;

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            // Set the type of the component
            if ("instrument".equalsIgnoreCase(qName)) {
                type = ComponentType.INSTRUMENT;
                return;
            }
            if ("site".equalsIgnoreCase(qName)) {
                type = ComponentType.SITE;
                return;
            }
            if ("source".equalsIgnoreCase(qName)) {
                type = ComponentType.SOURCE;
                return;
            }
            if ("observingParameters".equalsIgnoreCase(qName)) {
                type = ComponentType.OBS_PARAM;
                return;
            }
            // If we have the info tag we parse the name and the description and we terminate the parsing
            if ("info".equalsIgnoreCase(qName)) {
                name = attributes.getValue("name");
                description = attributes.getValue("description");
                throw new InfoTagFound();
            }
            // Here we know that we have one of the elements after the info, so we terminate
            logger.info("Imported component XML file does not contain the info element");
            throw new InfoTagNotFoundException();
        }
        
    }
    
    // This exception is used for terminating the parsing of the file if the
    // info tag does not exist.
    private static class InfoTagNotFoundException extends SAXException {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
    }
    
    // This exception is used for terminating the parsing of the file if the
    // info tag has been found and the name and description are parsed.
    private static class InfoTagFound extends SAXException {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
    }
    
    public static String getNonConflictName(String name, ComponentType type, Collection<String> forbiddenNames) {
        if (name == null)
            return null;
        List<ComponentInfo> existingComponentList = null;
        switch (type) {
            case INSTRUMENT:
                existingComponentList = InfoProviderHolder.getInfoProvider().getAvailableInstrumentList();
                break;
            case SITE:
                existingComponentList = InfoProviderHolder.getInfoProvider().getAvailableSiteList();
                break;
            case SOURCE:
                existingComponentList = InfoProviderHolder.getInfoProvider().getAvailableSourceList();
                break;
            case OBS_PARAM:
                existingComponentList = InfoProviderHolder.getInfoProvider().getAvailableObsParamList();
                break;
        }
        if (forbiddenNames != null) {
            for (String forbidenName : forbiddenNames) {
                existingComponentList.add(new ComponentInfo(forbidenName, null));
            }
        }
        int i = 0;
        ComponentInfo info = new ComponentInfo(name, null);
        while (existingComponentList.contains(info)) {
            i++;
            info = new ComponentInfo(name + "_" + i, null);
        }
        return info.getName();
    }
    
}
