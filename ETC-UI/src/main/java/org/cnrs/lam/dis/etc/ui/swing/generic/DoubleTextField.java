/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.ui.swing.generic;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JTextField;

/**
 * <p>This is a text field which can be used to show double values. For geting
 * and seting the value of the DoubleTextField only the getValue() and
 * setValue() methods should be called (not the getText() and setText() of the
 * JTextField).</p> When a non double value is typed in the text field it is
 * highlighted with the color given by the errorForground property. In this case
 * the getValue() method returns the value of the property defaultValue.
 *
 * @author Nikolaos Apostolakos
 */
public class DoubleTextField extends JTextField {

    /**
     * Constructs a new instance of DoubleTextField. This instance is checking
     * if its text represents a double on actionPerformed and focusLost events.
     */
    public DoubleTextField() {
        super();
        addActionListener(new ActionListener() {
            @Override
			public void actionPerformed(ActionEvent e) {
                checkIfDouble();
            }
        });
        addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                checkIfDouble();
            }
        });
        addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                checkIfDouble();
            }
        });
    }

    /** The flag used to store if the current value is invalid or not.*/
    private boolean currentValueValid = false;

    /**
     * Returns if the current value of the text field is a valid double
     * representation or not. If this method returns false, the getValue()
     * method will return the default value.
     * @return true if the current text represents a double, false otherwise
     */
    public boolean isCurrentValueValid() {
        return currentValueValid;
    }

    /** The variable where the normal foreground color is stored.*/
    private Color normalForeground = getBackground();

    /**
     * Returns the foreground color to be used when the text represents
     * a double.
     * @return The normal foreground color
     */
    public Color getNormalForeground() {
        return normalForeground;
    }

    /**
     * Sets the foreground color to be used when the text represents
     * a double.
     * @param normalForeground The foreground color
     */
    public void setNormalForeground(Color normalForeground) {
        this.normalForeground = normalForeground;
        if (currentValueValid) {
            super.setForeground(normalForeground);
        }
    }

    /**
     * Sets the foreground of the DoubleTextField which is used when the text
     * represents a double. This method sets the normal foreground.
     * @param fg The foreground color
     */
    @Override
    public void setForeground(Color fg) {
        normalForeground = fg;
        if (currentValueValid) {
            super.setForeground(fg);
        }
    }

    /** The color to use when the text of the text field is not a double.*/
    private Color errorForeground = Color.RED;

    /**
     * Returns the foreground color to be used when the text does not represents
     * a double.
     * @return The error foreground color
     */
    public Color getErrorForeground() {
        return errorForeground;
    }

    /**
     * Sets the foreground color to be used when the text does not represents
     * a double.
     * @param errorForeground The foreground color
     */
    public void setErrorForeground(Color errorForeground) {
        this.errorForeground = errorForeground;
        if (!currentValueValid) {
            super.setForeground(errorForeground);
        }
    }

    /** This value is returned by the getValue() method when the current text
     * is not representing a double.*/
    private double defaultValue = 0;

    /**
     * Returns the default value. This is the value returned by the getValue()
     * method when the current text is not representing a double.
     * @return The default value
     */
    public double getDefaultValue() {
        return defaultValue;
    }

    /**
     * Sets the default value. This is the value returned by the getValue()
     * method when the current text is not representing a double.
     * @param defaultValue The default value
     */
    public void setDefaultValue(double defaultValue) {
        this.defaultValue = defaultValue;
    }

    /**
     * Sets the value of the text field.
     * @param value The new value
     */
    public void setValue(double value) {
        setText(Double.toString(value));
        if (!currentValueValid) {
            currentValueValid = true;
            super.setForeground(normalForeground);
        }
    }

    /**
     * Returns the double which the text of the field is representing or the
     * defaultValue if the text is not representing a double.
     * @return The value of the text field
     */
    public double getValue() {
        double value = defaultValue;
        try {
            value = Double.parseDouble(getText());
        } catch (NumberFormatException e) {
            // Do nothing here, as we will return the default value
        }
        return value;
    }

    /**
     * This method checks if the current text of the text field represents a
     * double. If it is it returns true, if it is not it returns false. It also
     * sets correctly the foreground color and the curreentValueValid flag.
     * @return True if the text represents a double, false otherwise
     */
    private boolean checkIfDouble() {
        try {
            Double.parseDouble(getText());
            if (!currentValueValid) {
                currentValueValid = true;
                super.setForeground(normalForeground);
            }
        } catch (NumberFormatException e) {
            if (currentValueValid) {
                currentValueValid = false;
                super.setForeground(errorForeground);
            }
            return false;
        }
        return true;
    }

}
