/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.ui;

import java.util.List;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;

/**
 *
 * @author Nikolaos Apostolakos
 */
public interface Messenger {

    /**
     * Presents an info message to the user. The info messages are used to
     * notify the user of some events that require its attention.
     * @param message
     */
    public void info(String message);

    public void warn(String message);

    /**
     * Presents an error message to the user. The error messages describe
     * problems that do not allow the correct execution of the program and most
     * probably stop the current action.
     * 
     * @param message A description of the error to show to the user
     */
    public void error(String message);

    /**
     * Notifies the user that a long action is started and it blocks the user
     * input untill the actionStoped() is called.
     * @param message The message to show to the user
     * @param thread The thread executing the long action
     */
    public void actionStarted(String message, Thread thread);

    /**
     * Unblocks the user input (blocked by actionStarted method) after a long action has finished.
     * @param thread The thread that was executing the long action
     */
    public void actionStoped(Thread thread);

    /**
     * Presents a decision message to the user. This kind of message is
     * interactive and the user can chose one of the given options. The method
     * returns the option the user chosed. <b>NOTE:</b> This method should
     * be implemented in a synchronous way.
     *
     * @param message A description of the decision the user has to make
     * @param options The available options for the user to chose from
     * @return The option the user chosed
     */
    public String decision(String message, List<String> options);

    /**
     * Requests a string input from the user. The method returns the string
     * given from the user. The second parameter is a list of the available options.
     * If the list is null, the user can give any string he wants. If the user
     * didn't give any input, null is returned.
     * <b>NOTE:</b> This method should be implemented in a synchronous way.
     * @param message A description of the input requested
     * @param possibilities A list with the possible inputs, or null if the user
     * can give any input
     * @return A string representation of the users input or null if the user
     * canceled the action
     */
    public String requestInput(String message, List<String> possibilities);

    /**
     * Requests the name and description of a dataset from the user. It returns
     * a {@code DatasetInfo} object containing this information.
     * <b>NOTE:</b> This method should be implemented in
     * a synchronous way.
     * @return A {@code DatasetInfo} containing the name and description the
     * user entered
     */
    public DatasetInfo getNewDatasetInfo();
}
