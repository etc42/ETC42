/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.ui.swing.generic;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import org.apache.log4j.Logger;
import org.cnrs.lam.dis.etc.ui.swing.EtcPanel;

/**
 *
 * @author Nikolaos Apostolakos
 */
public abstract class CollapsiblePanel extends EtcPanel {

    private static Logger logger = Logger.getLogger(CollapsiblePanel.class);
    private boolean collapsed = false;
    private JPanel collapsedPanel;

    private JPanel getCollapsedPanel() {
        if (collapsedPanel == null) {
            collapsedPanel = new JPanel();
        }
        return collapsedPanel;
    }

    public CollapsiblePanel() {
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getX() >= 2 && e.getX() <= 16 && e.getY() >= 2 && e.getY() <= 16) {
                    collapsed = !collapsed;
                    setVisible(false);
                    setVisible(true);
                }
            }
        });
    }

    @Override
    public void setBorder(Border border) {
        if (border instanceof TitledBorder) {
            TitledBorder titledBorder = (TitledBorder) border;
            titledBorder.setTitle("   " + titledBorder.getTitle());
        }
        super.setBorder(border);
        getCollapsedPanel().setBorder(border);
    }

    @Override
    public void paint(Graphics g) {
        if (collapsed) {
            resizeCollapsedPanel();
            getCollapsedPanel().print(g);
        } else {
            super.paint(g);
        }
        drawArrow(g);
    }

    @Override
    public Dimension getPreferredSize() {
        if (collapsed) {
            return new Dimension((int) super.getPreferredSize().getWidth(), (int) getCollapsedPanel().getPreferredSize().getHeight());
        } else {
            return super.getPreferredSize();
        }
    }

    private void resizeCollapsedPanel() {
        Dimension size = new Dimension(getWidth(), (int) getCollapsedPanel().getPreferredSize().getHeight());
        getCollapsedPanel().setSize(size);
        getCollapsedPanel().setPreferredSize(size);
    }

    private void drawArrow(Graphics g) {
        try {
            Image image = null;
            if (collapsed) {
                image = Toolkit.getDefaultToolkit().getImage(Thread.currentThread().getContextClassLoader().getResource("org/cnrs/lam/dis/etc/ui/swing/images/Expand.png"));
            } else {
                image = Toolkit.getDefaultToolkit().getImage(Thread.currentThread().getContextClassLoader().getResource("org/cnrs/lam/dis/etc/ui/swing/images/Collapse.png"));
            }
            g.drawImage(image, 4, 1, this);
        } catch (Exception e) {
            logger.warn("Failed to draw collapsible panel icon", e);
            // after logging we ignore the exception and allow the method to return nomrally
        }
    }

}
