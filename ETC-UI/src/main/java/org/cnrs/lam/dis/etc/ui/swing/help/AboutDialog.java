package org.cnrs.lam.dis.etc.ui.swing.help;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.cnrs.lam.dis.etc.configuration.ConfigFactory;

public class AboutDialog extends HelpDialog {

	private static final long serialVersionUID = -679981628077230532L;
	private static AboutDialog instance = null;
	
	public final static String LICENSE_URL = "https://www.cecill.info/licences/Licence_CeCILL_V2.1-en.html";
	public final static String PROJECT_URL = "https://projets.lam.fr/projects/etc/news";
	public final static String CONTACT_URL = "etc42@lam.fr";

	private final static String[] authors = {
		"C. Surace",
		"J.C. Meunier",
		"N. Apostolakos",
		"P.Y. Chabaud",
		"A. Gross",
		"J. Penguen",
		"A. Sapone"
	};
	
	private final static String[] contributors = {
		"J.G. Cuby",
		"V. Le Brun",
		"G. Leleu",
		"J. Liu"
	};
	
	private JLabel logoLamLabel;
	private JLabel logoCesamLabel;
	
	private String formatList(String[] list, int lineBreakIndex) {
		String listString = "";
		for (int i = 0; i < list.length; i++) {
			String item = list[i];
			listString += item;
			if (i < list.length - 1) {
				listString += ", ";
			}
			if ( (i+1) % lineBreakIndex == 0) {
				listString += "<br>";
			}
		}
		return listString;
	}

	public AboutDialog(JFrame parent) {
		super(parent, "About ETC-42");
		
		getContentPane().remove(buttonPane);

		// LogoETC
		Image logoImage = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/org/cnrs/lam/dis/etc/ui/swing/images/Etc-big.png"));
		ImageIcon logo = new ImageIcon(logoImage.getScaledInstance(180, 180, Image.SCALE_SMOOTH));
		JLabel logoLabel = new JLabel(logo);
		logoLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

		// LogoLAM
		Image logoLamImage = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/org/cnrs/lam/dis/etc/ui/swing/images/LAM.jpg"));
		ImageIcon logoLam = new ImageIcon(logoLamImage.getScaledInstance(110, 35, Image.SCALE_SMOOTH));
		logoLamLabel = new JLabel(logoLam);
		logoLamLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
		logoLamLabel.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		logoLamLabel.setBorder(new EmptyBorder(0, 0, 0, 30));
		logoLamLabel.addMouseListener(new MouseAdapter()  
		{
			public void mouseClicked(MouseEvent e)  
			{  
				try {
					UtilsLoader.openWebpage(new URL("https://www.lam.fr/"));
				} catch (MalformedURLException e1) {}
			}  
		}); 
		// LogoCesam
		Image logoCesamImage = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/org/cnrs/lam/dis/etc/ui/swing/images/Cesam.png"));
		ImageIcon logoCesam = new ImageIcon(logoCesamImage.getScaledInstance(61, 35, Image.SCALE_SMOOTH));
		logoCesamLabel = new JLabel(logoCesam);
		logoCesamLabel.setAlignmentX(Component.RIGHT_ALIGNMENT);
		logoCesamLabel.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		logoCesamLabel.setBorder(new EmptyBorder(0, 30, 0, 0));
		logoCesamLabel.addMouseListener(new MouseAdapter()  
		{
			@Override
			public void mouseClicked(MouseEvent e)  
			{  
				try {
					UtilsLoader.openWebpage(new URL("https://www.lam.fr/cesam/"));
				} catch (MalformedURLException e1) {}
			}  
		}); 
		
		// Title
		JLabel titleLabel = new JLabel("ETC-42 Version: " + ConfigFactory.getConfig().getVersion());
		Font font = titleLabel.getFont();
		Font boldFont = new Font(font.getFontName(), Font.BOLD, font.getSize());
		titleLabel.setFont(boldFont);

		// Licence
		JLabel distributedLabel = new JLabel("Distributed under ");
		distributedLabel.setAlignmentX(LEFT_ALIGNMENT);
		JLabel distributedLabelEnd = new JLabel(" license");
		distributedLabelEnd.setAlignmentX(LEFT_ALIGNMENT);
		HyperlinkLabel licenceURLLabel = new HyperlinkLabel("CeCILL", LICENSE_URL);
		
		// Authors
		String authorsString = formatList(authors, 3);
		JLabel authorsLabel = new JLabel("<html><b>Authors:</b> " + authorsString + "</html>");
		authorsLabel.setAlignmentX(LEFT_ALIGNMENT);
		authorsLabel.setBorder(new EmptyBorder(15, 0, 15, 0));
		
		String contributorsString = formatList(contributors, 4);
		JLabel contributorsLabel = new JLabel("<html><b>Contributors:</b> " + contributorsString + "</html>");
		contributorsLabel.setAlignmentX(LEFT_ALIGNMENT);
		
		// contact
		JLabel contactLabel = new JLabel("Contact: ");
		contactLabel.setAlignmentX(LEFT_ALIGNMENT);
		
		Font fontContact = contactLabel.getFont();
		Font boldFontContact = new Font(fontContact.getFontName(), Font.BOLD, fontContact.getSize());
		contactLabel.setFont(boldFontContact);
		HyperlinkLabel contactUrlLabel = new HyperlinkLabel(CONTACT_URL, CONTACT_URL);
		
		// Publication
		JLabel publicationLabel = new JLabel("ETC-42 has been published in the following abstracts : ");
		HyperlinkLabel publicationUrlLabelA = new HyperlinkLabel("2015ASPC..495..391G", "http://adsabs.harvard.edu/abs/2015ASPC..495..391G");
		HyperlinkLabel publicationUrlLabelB = new HyperlinkLabel("2012ASPC..461..885A", "http://adsabs.harvard.edu/abs/2012ASPC..461..885A");
		HyperlinkLabel publicationUrlLabelC = new HyperlinkLabel("2011ASPC..442..559S", "http://adsabs.harvard.edu/abs/2011ASPC..442..559S");
		
		// Project webpage
		JLabel projectLabel = new JLabel("Project homepage: ");
		projectLabel.setAlignmentX(LEFT_ALIGNMENT);
		HyperlinkLabel projectUrlLabel = new HyperlinkLabel(PROJECT_URL, PROJECT_URL);
		projectUrlLabel.setAlignmentX(LEFT_ALIGNMENT);
		
		// Panels
		JPanel wrapper = new JPanel();
		BoxLayout layout = new BoxLayout(wrapper, BoxLayout.LINE_AXIS);
		wrapper.setLayout(layout);
		wrapper.setBorder(new EmptyBorder(15, 15, 15, 15));
		
		Box logosPane = Box.createHorizontalBox();
		logosPane.add(logoLamLabel);
		logosPane.add(logoCesamLabel);
		logosPane.setBorder(new EmptyBorder(30, 30, 0, 30));
		
		JPanel leftPane = new JPanel();
		leftPane.setLayout(new BoxLayout(leftPane, BoxLayout.Y_AXIS));
		leftPane.add(logoLabel);
		leftPane.add(logosPane);
		leftPane.setAlignmentY(CENTER_ALIGNMENT);
		
		JPanel licensePane = new JPanel();
		licensePane.setLayout(new BoxLayout(licensePane, BoxLayout.LINE_AXIS));
		licensePane.add(distributedLabel);
		licensePane.add(licenceURLLabel);
		licensePane.add(distributedLabelEnd);
		licensePane.setAlignmentX(LEFT_ALIGNMENT);
		
		JPanel contactPane = new JPanel();
		contactPane.setLayout(new BoxLayout(contactPane, BoxLayout.LINE_AXIS));
		contactPane.setBorder(new EmptyBorder(15, 0, 15, 0));
		contactPane.add(contactLabel);
		contactPane.add(contactUrlLabel);
		contactPane.setAlignmentX(LEFT_ALIGNMENT);
		
		JPanel publicationPane = new JPanel();
		publicationPane.setLayout(new BoxLayout(publicationPane, BoxLayout.PAGE_AXIS));
		publicationPane.setBorder(new EmptyBorder(15, 0, 15, 0));
		publicationPane.add(publicationLabel);
		
		JPanel publicationsLinePane = new JPanel();
		publicationsLinePane.setLayout(new BoxLayout(publicationsLinePane, BoxLayout.LINE_AXIS));
		publicationsLinePane.add(publicationUrlLabelA);
		publicationsLinePane.add(new JLabel(", "));
		publicationsLinePane.add(publicationUrlLabelB);
		publicationsLinePane.add(new JLabel(", "));
		publicationsLinePane.add(publicationUrlLabelC);
		publicationsLinePane.setAlignmentX(LEFT_ALIGNMENT);
		
		publicationsLinePane.setAlignmentX(Component.LEFT_ALIGNMENT);
		publicationPane.add(publicationsLinePane);
		
		JPanel projectWebPagePane = new JPanel();
		projectWebPagePane.setLayout(new BoxLayout(projectWebPagePane, BoxLayout.LINE_AXIS));
		projectWebPagePane.setBorder(new EmptyBorder(15, 0, 0, 0));
		projectWebPagePane.add(projectLabel);
		projectWebPagePane.add(projectUrlLabel);
		
		JPanel contentPane = new JPanel();
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
		contentPane.setAlignmentY(CENTER_ALIGNMENT);
		contentPane.setAlignmentX(LEFT_ALIGNMENT);
		
		titleLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
		contentPane.add(titleLabel);
		
		licensePane.setAlignmentX(Component.LEFT_ALIGNMENT);
		contentPane.add(licensePane);
		
		authorsLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
		contentPane.add(authorsLabel);
		
		contributorsLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
		contentPane.add(contributorsLabel);
		
		projectWebPagePane.setAlignmentX(Component.LEFT_ALIGNMENT);
		contentPane.add(projectWebPagePane);
		
		publicationPane.setAlignmentX(Component.LEFT_ALIGNMENT);
		contentPane.add(publicationPane);
		
		contactPane.setAlignmentX(Component.LEFT_ALIGNMENT);
		contentPane.add(contactPane);
		
		wrapper.add(leftPane);
		wrapper.add(contentPane);
		
		getContentPane().add(wrapper);
		
		// Contribute link from about
		JButton contributeButton = new JButton("Contribute");
		final JFrame dialogOwner = new JFrame();
		dialogOwner.setLocationRelativeTo(this);
		
		contributeButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                ContributionDialog.show(dialogOwner);
            }
        });
		this.buttonPane.add(contributeButton);
		
		JPanel buttonPaneWrapper = new JPanel();
		buttonPaneWrapper.add(buttonPane);
		buttonPaneWrapper.setAlignmentX(Component.LEFT_ALIGNMENT);
		contentPane.add(buttonPaneWrapper);
		
		pack();
	}

	public static void show(JFrame parent) {
		if (instance == null) {
			instance = new AboutDialog(parent);
		}
		instance.setVisible(true);
	}

}
