/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.ui.swing.generic;

import java.awt.Color;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;
import javax.swing.*;
import org.cnrs.lam.dis.etc.datamodel.FilterBand;

/**
 */
public class WavelengthComboBox extends JComboBox {
    
    public WavelengthComboBox() {
        List<String> optionList = new ArrayList<String>();
        for (FilterBand band : FilterBand.values()) {
            optionList.add(buildBandOptionString(band));
        }
        this.setModel(new DefaultComboBoxModel(optionList.toArray()));
        
        getEditor().getEditorComponent().addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                getEditor().getEditorComponent().setForeground(checkIfDouble() ? normalForeground : errorForeground);
            }
            
        });

    }
    
    private String buildBandOptionString(FilterBand band) {
        StringBuilder option = new StringBuilder();
        option.append(band.centralWavelength());
        option.append(" (");
        option.append(band.name());
        option.append(" central wavelength)");
        return option.toString();
    }
    
    public void setWavelength(double wavelength) {
        FilterBand band = null;
        for (FilterBand filterBand : FilterBand.values()) {
            if (filterBand.centralWavelength() == wavelength) {
                band = filterBand;
            }
        }
        String optionString = (band == null) ?
                Double.toString(wavelength) :
                buildBandOptionString(band);
        int index = -1;
        for (int i = 0; i < getItemCount(); i++) {
            if (getItemAt(i).equals(optionString)) {
                index = i;
                break;
            }
        }
        if (index == -1) {
            ((MutableComboBoxModel) getModel()).addElement(optionString);
        }
        setSelectedItem(optionString);
    }
    
    public double getWavelength() {
        String selectedOption = (String) getSelectedItem();
        int spaceIndex = selectedOption.indexOf(' ');
        if (spaceIndex > 0) {
            selectedOption = selectedOption.substring(0, spaceIndex);
        }
        double value = 0;
        try {
            value = Double.parseDouble(selectedOption);
        } catch (NumberFormatException numberFormatException) {
            return 0;
        }
        return value;
    }

    /** The variable where the normal foreground color is stored.*/
    private Color normalForeground = Color.BLACK;
    
    /** The color to use when the text is not a double.*/
    private Color errorForeground = Color.RED;
    
    /**
     * Returns the foreground color to be used when the text does not represents
     * a double.
     * @return The error foreground color
     */
    public Color getErrorForeground() {
        return errorForeground;
    }
    
    /** Check if the value entered in the combobox is a double, if it doesn't, 
     * the text will be colored in red
     */
    public boolean checkIfDouble(){
        JTextField comboTextField = (JTextField)getEditor().getEditorComponent();
        String value = comboTextField.getText();
        int spaceIndex = value.indexOf(' ');
        if (spaceIndex > 0) {
            value = value.substring(0, spaceIndex);
        }
        try {
            Double.parseDouble(value);
        } catch (NumberFormatException numberFormatException){
           return false; 
        }
        return true;
    }
}