/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.ui.swing.generic;

import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class ClosableTabComponent extends JPanel implements ActionListener, MouseListener {

    private final JTabbedPane parentPanel;
    JButton button;

    public ClosableTabComponent(JTabbedPane parentPanel, String title) {
        if (parentPanel == null)
            throw new NullPointerException("Closable tab cannot be created without a parrent");
        this.parentPanel = parentPanel;
        add(new JLabel(title));
        button = new JButton();
        button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/cnrs/lam/dis/etc/ui/swing/images/Close.png")));
        button.setSize(12, 12);
        button.setPreferredSize(new Dimension(12, 12));
        button.setMaximumSize(new Dimension(12, 12));
        button.setBorderPainted(false);
        button.setMargin(new Insets(0, 0, 0, 0));
        button.addActionListener(this);
        button.addMouseListener(this);
        add(button);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int position = parentPanel.indexOfTabComponent(this);
        if (position != -1) {
            parentPanel.remove(position);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        // do nothing
    }

    @Override
    public void mousePressed(MouseEvent e) {
        // do nothing
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // do nothing
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/cnrs/lam/dis/etc/ui/swing/images/CloseOver.png")));
    }

    @Override
    public void mouseExited(MouseEvent e) {
        button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/cnrs/lam/dis/etc/ui/swing/images/Close.png")));
    }

}
