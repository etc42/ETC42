/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.ui.commandline.commands;

import java.lang.reflect.Method;
import java.util.List;
import org.cnrs.lam.dis.etc.datamodel.ComponentInfo;
import org.cnrs.lam.dis.etc.datamodel.Dataset.Type;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.cnrs.lam.dis.etc.ui.commandline.ScannerHolder;
import org.cnrs.lam.dis.etc.ui.generic.InfoProviderHolder;
import org.cnrs.lam.dis.etc.ui.generic.SessionHolder;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class Set implements Command {

    @Override
    public void execute() {
        String userInput = ScannerHolder.getScanner().nextLine().trim();
        if (userInput.indexOf(' ') == -1) {
            System.out.println("Wrong arguments. Please run 'help set'.");
            return;
        }
        String type = userInput.substring(0, userInput.indexOf(' ')).trim();
        String parameter = userInput.substring(userInput.indexOf(' ')).trim();
        if ("instrument".equals(type) || "i".equals(type)) {
            setParameter(SessionHolder.getSession().getInstrument(), parameter);
        } else if ("site".equals(type) || "si".equals(type)) {
            setParameter(SessionHolder.getSession().getSite(), parameter);
        } else if ("source".equals(type) || "so".equals(type)) {
            setParameter(SessionHolder.getSession().getSource(), parameter);
        } else if ("obsparam".equals(type) || "o".equals(type)) {
            setParameter(SessionHolder.getSession().getObsParam(), parameter);
        } else {
            System.out.println("Unknown type : " + type + ". Please give one of the following:");
            System.out.println("instrument, i, site, si, source, so, obsparam, o");
        }
    }

    @Override
    public String help() {
        return "Description: Sets a parameter of a component.\n" +
               "Usage: set <type> <parameter> <value>\n" +
               "where <type> can be any of the following:\n" +
               "instrument or i: to load an instrument\n" +
               "site or si: to load a site\n" +
               "source or so: to load a source\n" +
               "obsparam or o: to load observing parameters\n" +
               ",<parameter> is the name of the parameter to be set\n" +
               "and <value> is the new value of the parameter (optional).";
    }

    private void setParameter(Object object, String parameter) {
        // Break the parameter to name and value
        String parameterName = parameter.trim();
        String parameterValue = null;
        int spaceIndex = parameterName.indexOf(' ');
        if (spaceIndex != -1) {
            parameterValue = parameterName.substring(spaceIndex).trim();
            if (parameterValue.length() == 0)
                parameterValue = null;
            parameterName = parameter.trim().substring(0, spaceIndex).trim();
        }
        // Check if we can set this parameter
        Method setter = null;
        for (Method method : object.getClass().getMethods()) {
            if (method.getName().equals("set" + parameterName)) {
                setter = method;
                break;
            }
        }
        if (setter == null) {
            System.out.println("Unknown parameter : " + parameterName);
            return;
        }
        Class parameterType = setter.getParameterTypes()[0];
        if (parameterType == Integer.TYPE) {
            readInteger(object, setter, parameterValue);
        } else if (parameterType == Double.TYPE) {
            readDouble(object, setter, parameterValue);
        } else if (parameterType == Boolean.TYPE) {
            readBoolean(object, setter, parameterValue);
        } else if (parameterType == DatasetInfo.class) {
            readDatasetInfo(object, setter, parameterValue);
        } else if (parameterType.isEnum()) {
            readEnum(parameterType, object, setter, parameterValue);
        }
    }

    private void readInteger(Object object, Method setter, String valueString) {
        if (valueString == null) {
            System.out.print("New value ");
            // Check for units
            try {
                String unitGetterName = "get";
                unitGetterName += setter.getName().substring(3);
                unitGetterName += "Unit";
                Method unitMethod = object.getClass().getMethod(unitGetterName);
                String unit = (String) unitMethod.invoke(object);
                System.out.print("(expressed in " + unit + ") ");
            } catch (Exception e) {
                // If there is an exception we do not present the unit
            }
        }
        int value = 0;
        try {
            if (valueString == null) {
                System.out.print(": ");
                value = ScannerHolder.getScanner().nextInt();
                // Ignore anything the user put after the number
                ScannerHolder.getScanner().nextLine();
            } else
                value = Integer.parseInt(valueString);
        } catch (Exception e) {
            System.out.println("Sorry... You must enter an integer value");
            return;
        }
        try {
            setter.invoke(object, value);
        } catch (Exception e) {
            System.out.println("Sorry... I failed to do what I was supposed to...");
        }
    }

    private void readDouble(Object object, Method setter, String valueString) {
        if (valueString == null) {
            System.out.print("New value ");
            // Check for units
            try {
                String unitGetterName = "get";
                unitGetterName += setter.getName().substring(3);
                unitGetterName += "Unit";
                Method unitMethod = object.getClass().getMethod(unitGetterName);
                String unit = (String) unitMethod.invoke(object);
                System.out.print("(expressed in " + unit + ") ");
            } catch (Exception e) {
                // If there is an exception we do not present the unit
            }
        }
        double value = 0;
        try {
            if (valueString == null) {
                System.out.print(": ");
                value = ScannerHolder.getScanner().nextDouble();
                // Ignore anything the user put after the number
                ScannerHolder.getScanner().nextLine();
            } else
                value = Double.valueOf(valueString);
        } catch (Exception e) {
            System.out.println("Sorry... You must enter a double value");
            return;
        }
        try {
            setter.invoke(object, value);
        } catch (Exception e) {
            System.out.println("Sorry... I failed to do what I was supposed to...");
        }
    }

    private void readBoolean(Object object, Method setter, String valueString) {
        if (valueString == null) {
            System.out.print("New value ");
            // Check for units
            try {
                String unitGetterName = "get";
                unitGetterName += setter.getName().substring(3);
                unitGetterName += "Unit";
                Method unitMethod = object.getClass().getMethod(unitGetterName);
                String unit = (String) unitMethod.invoke(object);
                System.out.print("(expressed in " + unit + ") ");
            } catch (Exception e) {
                // If there is an exception we do not present the unit
            }
        }
        boolean value = false;
        try {
            if (valueString == null) {
                System.out.print("New value (true/false) : ");
                value = ScannerHolder.getScanner().nextBoolean();
                // Ignore anything the user put after the number
                ScannerHolder.getScanner().nextLine();
            } else
                value = Boolean.valueOf(valueString);
        } catch (Exception e) {
            System.out.println("Sorry... You must enter a boolean value");
            return;
        }
        try {
            setter.invoke(object, value);
        } catch (Exception e) {
            System.out.println("Sorry... I failed to do what I was supposed to...");
        }
    }

    private void readDatasetInfo(Object object, Method setter, String nameString) {
        DatasetInfo userInfo = null;
        if (nameString == null) {
            System.out.println("Available datasets:");
        } else {
            if (!"null".equals(nameString.trim())) {
                int dotIndex = nameString.trim().indexOf('.');
                if (dotIndex == -1) {
                    userInfo = new DatasetInfo(nameString.trim(), null, null);
                } else {
                    userInfo = new DatasetInfo(nameString.trim().substring(dotIndex + 1), nameString.trim().substring(0, dotIndex), null);
                }
            }
        }
        Type type = Helper.getDatasetTypeMap().get(setter.getName().substring(3));
        String namespace = null;
        try {
            ComponentInfo info = (ComponentInfo) object.getClass().getMethod("getInfo").invoke(object);
            namespace = info.getName();
        } catch (Exception e) {
            System.out.println("Sorry... I failed to do what I was supposed to...");
            return;
        }
        if (userInfo != null && userInfo.getNamespace() != null && !userInfo.getNamespace().equals(namespace)) {
            System.out.println("Wrong namespace : " + userInfo.getNamespace());
            return;
        }
        List<DatasetInfo> availableDatasetList = InfoProviderHolder.getInfoProvider().getAvailableDatasetList(type, namespace);
        DatasetInfo selectedInfo = null;
        if (nameString == null) {
            int i = 0;
            for (DatasetInfo info : availableDatasetList) {
                System.out.println(" " + i + ". " + info);
                i++;
            }
            int selected = -1;
            try {
                System.out.print("Dataset to use: ");
                selected = ScannerHolder.getScanner().nextInt();
                // Ignore the rest of the line
                ScannerHolder.getScanner().nextLine();
                selectedInfo = availableDatasetList.get(selected);
            } catch (Exception e) {
                System.out.println("Sorry... You must enter a number between 0 and " + (availableDatasetList.size() - 1));
                return;
            }
        } else {
            if (availableDatasetList.contains(userInfo)) {
                selectedInfo = userInfo;
            } else {
                System.out.print("Invalid dataset");
                System.out.println(userInfo == null ? "" : " " + userInfo);
                return;
            }
        }
        try {
            setter.invoke(object, selectedInfo);
        } catch (Exception e) {
            System.out.println("Sorry... I failed to do what I was supposed to...");
        }
    }

    private void readEnum(Class parameterType, Object object, Method setter, String userValue) {
        if (userValue == null) {
            System.out.println("Options:");
        }
        Object selectedEnum = null;
        Object[] enumConstants = parameterType.getEnumConstants();
        for (int i = 0; i < enumConstants.length; i++) {
            if (userValue == null) {
                System.out.println(" " + (i + 1) + ". " + enumConstants[i]);
            } else {
                if (((Enum) enumConstants[i]).name().equals(userValue))
                    selectedEnum = enumConstants[i];
            }
        }
        if (userValue == null) {
            int selected = -1;
            try {
                System.out.print("Selection: ");
                selected = ScannerHolder.getScanner().nextInt();
                // Ignore the rest of the line
                ScannerHolder.getScanner().nextLine();
                selectedEnum = enumConstants[selected - 1];
            } catch (Exception e) {
                System.out.println("Sorry... You must enter a number between 1 and " + enumConstants.length);
                return;
            }
        } else {
            if (selectedEnum == null) {
                System.out.println("Invalid value : " + userValue);
                return;
            }
        }
        try {
            setter.invoke(object, selectedEnum);
        } catch (Exception e) {
            System.out.println("Sorry... I failed to do what I was supposed to...");
        }
    }

}
