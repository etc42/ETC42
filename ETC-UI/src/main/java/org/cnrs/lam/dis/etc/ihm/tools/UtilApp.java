/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.ihm.tools;

/**
 * Diverses methodes utilisées dans l'Application :
 * - version, titre de l'Applic, icones,... 
 * 
 * Pour l'instant dans cette Classe, on a ajouté
 * une méthode 'Example_DataGraphLog' qui intégre
 * les DATA pour les Graphiques (Source/Templates)   
 * 
 * @author distag
 *
 */

import javax.swing.*;

public class UtilApp
{
	private String strAppName;
	private String strAppInfo;
	private String strVersion;
	private String strCopyright;
	// Icones
	private String iconPath = new String("/org/cnrs/lam/dis/etc/ihm/icons/");
	private String iconOAMP = new String("logoamp.jpg");
	// Select Panels
	private String iconInstrument = new String("satellite.png"); 
	private String iconSite = new String("Site.png");
	private String iconSource = new String("Source.png");
	private String iconObserving = new String("obersvparam.png");
	private String iconPlot = new String("forward.png");
	// Buttons
	private String iconAdd = new String("add.png");
	private String iconDelete = new String("delete.png");
	private String iconSave = new String("arrow_refresh.png");
	private String iconSaveAs = new String("Save.png");
	private String iconLoupe = new String("Loupe.png");
	
	// Constructeur par defaut
	public UtilApp(){
		strAppName = new String("E.T.C.");
		strAppInfo = new String("Exposure Time Calculator");
		strVersion = new String("00.99");
		strCopyright = new String("L.A.M.  © 2010");		
	}
	
	// Uniquement en lecture
	public String getAppName() {
		return strAppName;
	}
	public String getAppInfo() {
		return strAppInfo;
	}
	public String getVersion() {
		return strVersion;
	}
	public String getCopyright() {
		return strCopyright;
	}
	public String getIconPath() {
		return iconPath;
	}
	public String getLogoOAMP() {
		return iconPath + iconOAMP;
	}
	// Modules
	public String getIconInstrument() {
		return iconPath + iconInstrument;
	}
	public String getIconSite() {
		return iconPath + iconSite;
	}
	public String getIconSource() {
		return iconPath + iconSource;
	}
	public String getIconObserving() {
		return iconPath + iconObserving;
	}
	public String getIconPlot() {
		return iconPath + iconPlot;
	}
	// Panels
	public String getIconAdd() {
		return iconPath + iconAdd;
	}
	public String getIconDelete() {
		return iconPath + iconDelete;
	}
	public String getIconSave() {
		return iconPath + iconSave;
	}
	public String getIconSaveAs() {
		return iconPath + iconSaveAs;
	}
	public String getIconLoupe() {
		return iconPath + iconLoupe;
	}

	// Affichage d'une Boite de Dialogue en 
	// attendant une implémentation future
	public void ShowToDo (JFrame parent){
		JOptionPane.showMessageDialog(parent, 
		"Fonctionnalités en cours de développement",
		"En construction...", 
		JOptionPane.INFORMATION_MESSAGE);
	}
	
}
