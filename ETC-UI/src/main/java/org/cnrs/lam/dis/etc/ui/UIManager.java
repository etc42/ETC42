/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.ui;

import cds.savot.model.SavotVOTable;
import java.io.File;
import java.util.List;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.Session;

/**
 *
 * @author Nikolaos Apostolakos
 */
public interface UIManager {

    /**
     * Sets the seesion that the user interface presents to the user and updates
     * by the user actions (the current session).
     * @param session the session to present
     */
    public void setSession(Session session);

    /**
     * Informs the UIManager that the current session is modified, so it will
     * update the representation shown to the user. This is necessary when
     * other parts of the system modify the session.
     */
    public void sessionModified();

    /**
     * Sets the <code>InfoProvider</code> the user interface will use.
     * @param provider tne info provider
     */
    public void setInfoProvider(InfoProvider provider);

    /**
     * Sets the listener which will be notified for session events generated
     * by the user interface.
     * @param listener The session listener
     */
    public void setSessionListener(SessionListener listener);

    /**
     * Sets the listener which will be notified for instrument events generated
     * by the user interface.
     * @param listener The instrument listener
     */
    public void setInstrumentListener(InstrumentListener listener);

    /**
     * Sets the listener which will be notified for site events generated
     * by the user interface.
     * @param listener The site listener
     */
    public void setSiteListener(SiteListener listener);

    /**
     * Sets the listener which will be notified for source events generated
     * by the user interface.
     * @param listener The source listener
     */
    public void setSourceListener(SourceListener listener);

    /**
     * Sets the listener which will be notified for observing parameters events generated
     * by the user interface.
     * @param listener The observing parameters listener
     */
    public void setObsParamListener(ObsParamListener listener);

    /**
     * Sets the listener which will be notified for dataset events generated
     * by the user interface.
     * @param listener The dataset listener
     */
    public void setDatasetListener(DatasetListener listener);

    /**
     * Sets the listener which will be notified for buisness events generated
     * by the user interface.
     * @param listener The buisness listener
     */
    public void setBuisnessListener(BuisnessListener listener);

    /**
     * Shows a plot containing any number of datasets to the user.
     * @param title The title of the plot
     * @param xDescription The description of the X axis
     * @param yDescription The description of the Y axis
     * @param xUnit The unit of the X axis
     * @param yUnit The unit of the Y axis
     * @param datasetList A list of datasets to be contained in the plot
     * @deprecated Use {@link #showChart(java.lang.String, cds.savot.model.SavotVOTable, java.io.File)} instead
     */
    @Deprecated
	public void showPlot(String title, String xDescription, String yDescription,
            String xUnit, String yUnit, List<Dataset> datasetList);

    /**
     * Presents the result of a calculation of SNR and exposure time.
     * @param result An object containing the results of the calculation
     */
    public void showCalculationResult(CalculationResults result);

    public void setSampListener(SampListener listener);
    
    public void pluginListChanged();
    
    public void handleFirstInstallation();
    
    public void showChart(String title, SavotVOTable voTable, File voTableFile);
    
}
