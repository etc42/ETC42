/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.ui;

import java.io.File;
import org.cnrs.lam.dis.etc.datamodel.ComponentInfo;

/**
 *
 * @author Nikolaos Apostolakos
 */
public interface InstrumentListener {

    /**
     * This method is called when the listener gets notified that the user
     * wants to create a new instrument. This instrument will become the
     * current instrument and it will replace the old one.
     * @param newInstrumentInfo The name and description of the new instrument
     */
    public void createNewInstrument(ComponentInfo newInstrumentInfo);

    /**
     * This method is called when the listener gets notified that the user
     * wants to load a specific instrument. This instrument will be loaded as the isntrument of
     * the current session and it will replace the old one. The implementations
     * of the listener should take care so that unsaved information of the old
     * instrument does not get lost without the willness of the user.
     *
     * @param instrumentInfo The information to identify the instrument to load
     */
    public void loadInstrument(ComponentInfo instrumentInfo);

    /**
     * This method is called when the listener gets notified that the user
     * wants to reload the current instrument. This means that the user wants
     * to undo all the unsaved modifications. The implementations
     * of the listener should take care so that unsaved information of the old
     * instrument does not get lost without the willness of the user.
     */
    public void reloadCurrentInstrument();

    /**
     * This method is called when the listener gets notified that the user
     * wants to save the modifications he has done at the current instrument.
     */
    public void saveCurrentInstrument();

    /**
     * This method is called when the listener gets notified that the user
     * wants to save the current instrument (together with any modifications
     * done from last loaded) as a new instrument.
     * @param newInstrumentInfo Contains the name and the description of the
     * new instrument
     */
    public void saveCurrentInstrumentAs(ComponentInfo newInstrumentInfo);

    public void deleteInstrument(ComponentInfo instrumentInfo);

    /**
     * This method is called when the listener gets notified that the user
     * wants to import an instrument form a file.
     * @param file The file to import from
     * @param insrumentInfo The name of the new instrument
     */
    public void importFromFile(File file, ComponentInfo instrumentInfo);

    public void exportCurrentInstrumentInFile(File file);

    public boolean isCurrentInstrumentModified();
}
