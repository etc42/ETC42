package org.cnrs.lam.dis.etc.ui.swing.importer;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.function.Consumer;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import org.cnrs.lam.dis.etc.configuration.ConfigFactory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.apache.log4j.Logger;

public class OfficialRepositoryTreeView extends javax.swing.JPanel {

	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(XmlSelectorWebBrowserPanel.class);
	private JTree tree;
	private DefaultMutableTreeNode root = new DefaultMutableTreeNode("Presets Repository");
	private DefaultTreeModel treeModel = new DefaultTreeModel(this.root);

	public OfficialRepositoryTreeView() {
		this.initComponents();
	}

	private void initComponents() {
		this.setLayout(new java.awt.BorderLayout());

		this.tree = new JTree();
		tree.addMouseListener(new MouseAdapter() {

			@Override
			public void mousePressed(MouseEvent e) {
				int selRow = tree.getRowForLocation(e.getX(), e.getY());
				TreePath selPath = tree.getPathForLocation(e.getX(), e.getY());

				if (selRow != -1 && e.getClickCount() == 2 && selPath.getLastPathComponent() instanceof XMLTreeNode) {
					XMLTreeNode node = (XMLTreeNode) selPath.getLastPathComponent();
					if (node.getHref() != null) {
						MultiImportTool.processUrlSelection(node.getHref());
					}
				}
			}
		});

		// Construct the tree by analyzing structure on the online official
		// repository
		try {
			URL officialRepoUrl = new URL(ConfigFactory.getConfig().getOfficialRepoUrlDefault() + "/");
			this.constructTree(this.root, officialRepoUrl, true);
		} catch (MalformedURLException e) {}

		// Apply empty or filled model
		this.tree.setModel(treeModel);

		// Expand all expendables rows
		this.add(this.tree, java.awt.BorderLayout.CENTER);
		for (int i = 0; i < this.tree.getRowCount(); i++) {
			tree.expandRow(i);
		}

	}

	private void constructTree(final DefaultMutableTreeNode node, final URL url, final boolean isRoot) {

		try {
			org.jsoup.nodes.Document htmlDoc = Jsoup.connect(url.toString()).get();
			Elements links = htmlDoc.getElementsByTag("a");

			links.forEach(new Consumer<Element>() {

				@Override
				public void accept(Element link) {
					String text = link.getAllElements().html();

					boolean isFolder = text.endsWith("/");
					boolean isXmlFile = text.endsWith(".xml");

					String versionFilter = "";
					if (isRoot) {
						String version = ConfigFactory.getConfig().getVersion();
						if (version.contains("-")) {
							version = version.split("-")[0];
						}
						String[] splitted = version.split("\\.");
						versionFilter = splitted[0] + "." + splitted[1];
					}

					try {
						if (isFolder) {
							String cleanedName = text.substring(0, text.length() - 1);
							System.out.println("VersionFilter" + versionFilter + " - cleanedName : " + cleanedName);
							if (!isRoot || (isRoot && versionFilter.equals(cleanedName))) {
								DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(cleanedName);
								node.add(newNode);
								URL href = new URL(url.toString() + link.attr("href"));
								constructTree(newNode, href, false);
							}
						} else if (isXmlFile) {
							URL href = new URL(url.toString() + link.attr("href"));
							XMLTreeNode newNode = new XMLTreeNode(text.substring(0, text.length() - 4), href);
							node.add(newNode);
						}
					} catch (MalformedURLException e) {
					}
				}
			});

		} catch (MalformedURLException e) {
			logger.info("URL is wrong : ", e);
			System.out.println(e.getMessage());
		} catch (IOException e) {
			logger.info("IOException while parsing url : ", e);
			System.out.println(e.getMessage());
		}
		this.tree.setModel(this.treeModel);
	}

	public class XMLTreeNode extends DefaultMutableTreeNode {
		private static final long serialVersionUID = 1L;
		private final URL href;
		private final String label;

		public XMLTreeNode(String label, URL href) {
			this.href = href;
			this.label = label;
		}

		@Override
		public String toString() {
			return this.label;
		}

		public URL getHref() {
			return href;
		}
	}
}