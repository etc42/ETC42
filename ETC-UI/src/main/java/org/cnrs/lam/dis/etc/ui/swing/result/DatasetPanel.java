/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.ui.swing.result;

import cds.savot.model.SavotVOTable;
import cds.savot.writer.SavotWriter;
import java.awt.Font;
import java.io.File;
import java.util.Calendar;
import java.util.Map;
import java.util.ResourceBundle;
import org.cnrs.lam.cesam.vo.dnd.DragAndDropHelper;
import org.cnrs.lam.cesam.vo.dnd.VoDataFlavors;
import org.cnrs.lam.cesam.vo.dnd.VoTable1DHelper;
import org.cnrs.lam.dis.etc.configuration.ConfigFactory;
import org.cnrs.lam.dis.etc.ui.generic.SampListenerHolder;
import org.cnrs.lam.dis.etc.ui.swing.EtcFrame;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class DatasetPanel<X extends Number, Y extends Number> extends javax.swing.JPanel {

    private static ResourceBundle bundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/ui/swing/messages");
    private Map<X, Y> data;
    private String xUnit;
    private String yUnit;
    private EtcFrame etcFrame;
    private String name;
    private SavotVOTable voTable;
    private File voTableFile;
    private String chartTitle;

    /** Creates new form DatasetPanel */
    private DatasetPanel() {
        initComponents();
    }
    
    public DatasetPanel(String parrentName, String name, Map<X, Y> data, String xUnit,
            String yUnit, EtcFrame etcFrame) {
        this();
        String translatedName = name;
        try {
            translatedName = bundle.getString("RESULT_" + name);
        } catch (Exception ex) {
            // if we cannot retrieve the translation we just use the original name
        }
        nameLabel.setText(translatedName);
        sendVoTableButton.setToolTipText(bundle.getString("HUB_BUTTON")+" - "+nameLabel.getText());
        plotButton.setToolTipText(bundle.getString("GRAPH_BUTTON")+" - "+nameLabel.getText());
        
        this.name = name;
        this.data = data;
        this.xUnit = xUnit;
        this.yUnit = yUnit;
        this.etcFrame = etcFrame;
        this.chartTitle = translatedName + " (" + parrentName + ")";
        
        // We create the VOTable
        String xLabelTranslated = "";
        try {
            xLabelTranslated = bundle.getString("RESULT_" + name + "_X_LABEL");
        } catch (Exception ex) {
            // if we cannot retrieve the translation we just use nothing
        }
        String yLabelTranslated = "";
        try {
            yLabelTranslated = bundle.getString("RESULT_" + name + "_Y_LABEL");
        } catch (Exception ex) {
            // if we cannot retrieve the translation we just use nothing
        }
        this.voTable = VoTable1DHelper.createVoTable(chartTitle, xLabelTranslated, xUnit, yLabelTranslated, yUnit, data);
        
        // We create the VOTable file
        voTableFile = null;
        String fileName = "ETC_" + Calendar.getInstance().getTimeInMillis() + ".vot";
        File tempDir = new File(ConfigFactory.getConfig().getTempDir());
        tempDir.mkdirs();
        voTableFile = new File(tempDir, fileName);
        SavotWriter writer = new SavotWriter();
        writer.generateDocument(voTable, voTableFile.getAbsolutePath());
        voTableFile.deleteOnExit();
        
        DragAndDropHelper.enableVoTableDrag(this, voTable, voTableFile
                , VoDataFlavors.VoTableDataType.TABLE_1D, null, null);
    }

    public void setOverallFont(Font font) {
        nameLabel.setFont(font);
        separatorLabel.setFont(font);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        nameLabel = new javax.swing.JLabel();
        separatorLabel = new javax.swing.JLabel();
        plotButton = new javax.swing.JButton();
        sendVoTableButton = new javax.swing.JButton();

        nameLabel.setText("name");
        nameLabel.setName("nameLabel"); // NOI18N

        separatorLabel.setText(":");
        separatorLabel.setName("separatorLabel"); // NOI18N

        plotButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/cnrs/lam/dis/etc/ui/swing/images/Plot.png"))); // NOI18N
        plotButton.setName("plotButton"); // NOI18N
        plotButton.setPreferredSize(new java.awt.Dimension(25, 25));
        plotButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                plotButtonActionPerformed(evt);
            }
        });

        sendVoTableButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/cnrs/lam/dis/etc/ui/swing/images/Broadcast.png"))); // NOI18N
        sendVoTableButton.setToolTipText("");
        sendVoTableButton.setName("sendVoTableButton"); // NOI18N
        sendVoTableButton.setPreferredSize(new java.awt.Dimension(25, 25));
        sendVoTableButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                sendVoTableButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(nameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 350, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(separatorLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(plotButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sendVoTableButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                .addComponent(separatorLabel)
                .addComponent(plotButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(sendVoTableButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(nameLabel))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void plotButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_plotButtonActionPerformed
        etcFrame.showChart(chartTitle, voTable, voTableFile);
    }//GEN-LAST:event_plotButtonActionPerformed

    private void sendVoTableButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sendVoTableButtonActionPerformed
        SampListenerHolder.getSampListener().sendAsVoTable(name, data, xUnit, yUnit);
    }//GEN-LAST:event_sendVoTableButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel nameLabel;
    private javax.swing.JButton plotButton;
    private javax.swing.JButton sendVoTableButton;
    private javax.swing.JLabel separatorLabel;
    // End of variables declaration//GEN-END:variables

}
