/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.ui.swing.result;

import cds.savot.model.SavotVOTable;
import cds.savot.writer.SavotWriter;
import java.awt.Font;
import java.io.File;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TreeMap;
import org.cnrs.lam.cesam.vo.dnd.DragAndDropHelper;
import org.cnrs.lam.cesam.vo.dnd.VoDataFlavors;
import org.cnrs.lam.cesam.vo.dnd.VoTable1DSetHelper;
import org.cnrs.lam.dis.etc.configuration.ConfigFactory;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.cnrs.lam.dis.etc.ui.swing.EtcFrame;

/**
 *
 * @author nikoapos
 */
public class DatasetSetPanel<K extends Number> extends javax.swing.JPanel {

    private static ResourceBundle bundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/ui/swing/messages");
    private EtcFrame etcFrame;
    private String name;
    private String keyUnit;
    private String xName;
    private String xUnit;
    private String yName;
    private String yUnit;
    private SavotVOTable voTable;
    private File voTableFile;
    private String chartTitle;
    
    /**
     * Creates new form DatasetSetPanel
     */
    private DatasetSetPanel() {
        initComponents();
    }
    
    public DatasetSetPanel(String parrentName, String name, String keyUnit,
            Map<K, CalculationResults.DoubleDatasetResult> datasetMap, EtcFrame etcFrame) {
        this();
        String translatedName = name;
        try {
            translatedName = bundle.getString("RESULT_" + name);
        } catch (Exception ex) {
            // if we cannot retrieve the translation we just use the original name
        }
        nameLabel.setText(translatedName);
        showButton.setToolTipText(bundle.getString("GRAPH_BUTTON")+" - "+nameLabel.getText());
        this.name = name;
        this.keyUnit = keyUnit;
        this.etcFrame = etcFrame;
        this.chartTitle = translatedName + " (" + parrentName + ")";
        
        // We create the VOTable
        xName = " ";
        try {
            xName = bundle.getString("RESULT_" + name + "_X_LABEL");
        } catch (Exception ex) {
        }
        yName = " ";
        try {
            yName = bundle.getString("RESULT_" + name + "_Y_LABEL");
        } catch (Exception ex) {
        }
        xUnit = datasetMap.values().iterator().next().getXUnit();
        yUnit = datasetMap.values().iterator().next().getYUnit();
        Map<K, Map<Double, Double>> dataMap = new TreeMap<K, Map<Double, Double>>();
        // If we have too many datasets we are going to keep arround 30-60 values
        List<K> keysToUse = new LinkedList<K>(datasetMap.keySet());
        while (keysToUse.size() >= 60) {
            for (int i = 1; i < keysToUse.size() - 1; i += 2) {
                keysToUse.remove(i);
            }
        }
        for (K z : keysToUse) {
            CalculationResults.DoubleDatasetResult xy = datasetMap.get(z);
            Map<Double, Double> xyData = new TreeMap<Double, Double>();
            for (Map.Entry<Double, Double> xyEntry : xy.getValues().entrySet()) {
                Double x = xyEntry.getKey();
                Double y = xyEntry.getValue();
                xyData.put(x, y);
            }
            dataMap.put(z, xyData);
        }
        voTable = VoTable1DSetHelper.create1DSetVoTable(chartTitle, xName, xUnit, yName, yUnit, "Unknown", keyUnit, dataMap);
        
        // We create the VOTable file
        voTableFile = null;
        String fileName = "ETC_" + Calendar.getInstance().getTimeInMillis() + ".vot";
        File tempDir = new File(ConfigFactory.getConfig().getTempDir());
        tempDir.mkdirs();
        voTableFile = new File(tempDir, fileName);
        SavotWriter writer = new SavotWriter();
        writer.generateDocument(voTable, voTableFile.getAbsolutePath());
        voTableFile.deleteOnExit();
        
        DragAndDropHelper.enableVoTableDrag(this, voTable, voTableFile
                , VoDataFlavors.VoTableDataType.TABLE_1D_SET, null, null);
    }

    public void setOverallFont(Font font) {
        nameLabel.setFont(font);
        separatorLabel.setFont(font);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        showButton = new javax.swing.JButton();
        separatorLabel = new javax.swing.JLabel();
        nameLabel = new javax.swing.JLabel();

        showButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/cnrs/lam/dis/etc/ui/swing/images/Plot.png"))); // NOI18N
        showButton.setName("showButton"); // NOI18N
        showButton.setPreferredSize(new java.awt.Dimension(25, 25));
        showButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                showButtonActionPerformed(evt);
            }
        });

        separatorLabel.setText(":");
        separatorLabel.setName("separatorLabel"); // NOI18N

        nameLabel.setText("name");
        nameLabel.setName("nameLabel"); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(nameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 350, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(separatorLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(showButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                .addComponent(nameLabel)
                .addComponent(separatorLabel)
                .addComponent(showButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void showButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_showButtonActionPerformed
        etcFrame.showSetChart(chartTitle, voTable, voTableFile);
    }//GEN-LAST:event_showButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel nameLabel;
    private javax.swing.JLabel separatorLabel;
    private javax.swing.JButton showButton;
    // End of variables declaration//GEN-END:variables
}
