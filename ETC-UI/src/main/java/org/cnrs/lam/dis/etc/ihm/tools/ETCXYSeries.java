/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.ihm.tools;

import org.jfree.data.xy.XYDataItem;
import org.jfree.data.xy.XYSeries;

public class ETCXYSeries extends XYSeries{

	private String typeSerie;
	private String unitX;
	private String unitY;
	private double coefY;
	/**
	 * 
	 * @param typeSerie correspondance de votre serie a un type de Serie
	 * @param unitX unite de l'abscisse  de votre serie
	 * @param unitY unite de l'ordonnee de votre serie
	 */
	public ETCXYSeries(Comparable key, String typeSerie, String unitX, String unitY, double coefY) {
		super(key);
		this.typeSerie = typeSerie;
		this.unitX = unitX;
		this.unitY = unitY;
		this.coefY = coefY;
	}
	
	public double getCoefY(){
		
		return this.coefY;
		
		
	}
	/**
	 * renvoie le type de votre ETCXYSeries
	 * @return
	 */
	public String getTypeSeries(){
		return this.typeSerie;
	}
	
	public void setTypeSeries(String typeSerie){
		this.typeSerie = typeSerie;
	}
	public String getUnitX(){
		return this.unitX;
	}
	
	public void setUnitX(String unitX){
		this.unitX = unitX;
	}
	public String getUnitY(){
		return this.unitY;
	}
	
	public void setUnitY(String unitY){
		this.unitY = unitY;
	}

    /**
     * Changes the coefY of the series and adjusts the data correctly.
     * @param newCoefY The new coefY 
     */
    void changeCoefY(double newCoefY) {
        if (newCoefY == coefY)
            return;
        double factor = coefY / newCoefY;
        for (Object o : data) {
            XYDataItem item = (XYDataItem) o;
            item.setY(item.getYValue() * factor);
        }
    }

}
