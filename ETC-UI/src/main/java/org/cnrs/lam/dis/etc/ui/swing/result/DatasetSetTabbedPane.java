/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.ui.swing.result;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TreeMap;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.DoubleDatasetResult;
import org.cnrs.lam.dis.etc.ihm.PlotPanel;
import org.cnrs.lam.dis.etc.ihm.tools.EtcChartPanel;
import org.cnrs.lam.dis.etc.ihm.tools.IncorrectParentException;
import org.cnrs.lam.dis.etc.ui.swing.generic.ClosableTabComponent;
import org.jfree.chart.JFreeChart;
import org.jfree.data.xy.XYDataset;

/**
 *
 * @author nikoapos
 */
public class DatasetSetTabbedPane extends JTabbedPane {
    
    private static ResourceBundle bundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/ui/swing/messages");
    private PlotPanel plotPanel;

    public void setPlotPanel(PlotPanel plotPanel) {
        this.plotPanel = plotPanel;
    }

    public <K extends Number> void showDatasetSet(String title, Map<K, CalculationResults.DoubleDatasetResult> datasetMap, String keyUnit) {
        add(title, new DatasetSetTabbedPane.ShowDatasetSetPanel(datasetMap, keyUnit));
        ClosableTabComponent resultTabComponent = new ClosableTabComponent(this, title);
        int index = getTabCount() - 1;
        setTabComponentAt(index, resultTabComponent);
        setSelectedIndex(index);
        plotPanel.getGlobalTabPane().setSelectedComponent(this);
    }

    private class ShowDatasetSetPanel<K extends Number> extends JPanel implements ChangeListener {
        private Map<K, CalculationResults.DoubleDatasetResult> datasetMap;
        private Map<Integer, K> keyMap;
        private JSlider slider;
        private JLabel selectedValue;
        private EtcChartPanel datasetPanel;
        private String keyUnit;
        private NumberFormat formatter = new DecimalFormat("#0.##");

        public ShowDatasetSetPanel(Map<K, CalculationResults.DoubleDatasetResult> imageMap, String keyUnit) {
            this.datasetMap = imageMap;
            this.keyUnit = keyUnit;
            this.setLayout(new BorderLayout(5,5));
            // Create a map between the keys of the map and increasing numbers
            // so we can select easily which image the user selects with the slider
            keyMap = new TreeMap<Integer, K>();
            int i = 1;
            for (K key : imageMap.keySet()) {
                keyMap.put(i, key);
                i++;
            }
            // Create the slider
            JPanel sliderPanel = new JPanel(new BorderLayout());
            slider = new JSlider(1, i-1);
            slider.addChangeListener(this);
            slider.setSnapToTicks(true);
            sliderPanel.add(slider, BorderLayout.CENTER);
            selectedValue = new JLabel();
            sliderPanel.add(selectedValue, BorderLayout.EAST);
            this.add(sliderPanel, BorderLayout.SOUTH);
            // Set the first dataset as selected
            slider.setValue(1);
        }

        @Override
        public void stateChanged(ChangeEvent e) {
            if (datasetPanel != null) {
                this.remove(datasetPanel);
            }
            datasetPanel = makeDatasetPanel(datasetMap.get(keyMap.get(slider.getValue())));
            this.add(datasetPanel, BorderLayout.CENTER);
            selectedValue.setText(formatter.format(keyMap.get(slider.getValue())) + (keyUnit == null ? "" : (" " + keyUnit)));
        }

        private EtcChartPanel makeDatasetPanel(DoubleDatasetResult dataset) {
            String name = dataset.getName();
            String title = "";
            try {
                title = bundle.getString("RESULT_" + name);
            } catch (Exception ex) {
                title = name;
            }
            String xLabelTranslated = "";
            try {
                xLabelTranslated = bundle.getString("RESULT_" + name + "_X_LABEL");
            } catch (Exception ex) {
            }
            String yLabelTranslated = "";
            try {
                yLabelTranslated = bundle.getString("RESULT_" + name + "_Y_LABEL");
            } catch (Exception ex) {
            }
            String xUnit = dataset.getXUnit();
            String yUnit = dataset.getYUnit();
            String xLabel = xLabelTranslated + ((xUnit != null && !xUnit.isEmpty()) ? (" (" + xUnit + ")") : "");
            String yLabel = yLabelTranslated + ((yUnit != null && !yUnit.isEmpty()) ? (" (" + yUnit + ")") : "");
            Map<Double, Double> data = dataset.getValues();
            double[] xValues = new double[data.size()];
            double[] yValues = new double[data.size()];
            int i = 0;
            for (Map.Entry<Double, Double> entry : data.entrySet()) {
                xValues[i] = entry.getKey().doubleValue();
                yValues[i] = entry.getValue().doubleValue();
                i++;
            }
           
            // ********************************************************************
            // The following are copied from the method makeAnyGraph of class PlotPanel
            double moyenneY = 0.0;
	           double coefOK = 1.0;
            for (i = 0; i < yValues.length; i++) {
                moyenneY += yValues[i];
            }
            moyenneY = moyenneY / yValues.length;
            int coefY = plotPanel.getExposant(moyenneY);

            if (coefY < 1E-3 && coefY != 0) {
                yLabel = "E" + coefY + " " + yLabel;
                coefOK = Double.valueOf("1E" + coefY);
            }

            for (i = 0; i < yValues.length; i++) {
                yValues[i] = yValues[i] / coefOK;
            }
            XYDataset xydataset = plotPanel.makeDataset(title,xValues,yValues,title,xLabel,yLabel,coefOK);
            JFreeChart jfreechart = PlotPanel.makeGraph(xydataset,title, xLabel, yLabel);
            EtcChartPanel chartpanel = null;
            try {
                chartpanel = new EtcChartPanel(jfreechart);
                chartpanel.setPreferredSize(new Dimension(500, 270));
            } catch (IncorrectParentException ex) {
                ex.printStackTrace();
            }
            // ********************************************************************
            return chartpanel;
        }
    }
    
}
