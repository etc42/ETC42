/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.ui.swing.config;

import java.awt.Rectangle;
import javax.swing.JDialog;
import javax.swing.JTabbedPane;


/**
 *
 * @author stagcesam
 */
public class ConfigurationDialogTool extends JDialog{
    
    private JTabbedPane tabbedPane = new JTabbedPane();
    private CalculatorPanel calculatorPanelTest = new CalculatorPanel(this);
    private ImportUrlConfigPanel importUrlConfigPanel = new ImportUrlConfigPanel(this);
    private ResultLevelPanel resultLevelPanel = new ResultLevelPanel(this);
    private ResultOrderPanel resultOrderPanelTest = new ResultOrderPanel(this);
    
    /** Creates new form NewJFrame */
    public ConfigurationDialogTool(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
//        this.setBounds(new Rectangle(0,0,600,350));
        this.add(tabbedPane);
//        tabbedPane.setBounds(10, 10, 380, 290);
        
        calculatorPanelTest.setVisible(true);
        importUrlConfigPanel.setVisible(true);
        resultLevelPanel.setVisible(true);
//        resultOrderPanelTest.setVisible(true);
        
        tabbedPane.setBounds(new Rectangle(0,0,450,300));
        
        tabbedPane.addTab("Calculator", calculatorPanelTest);
        tabbedPane.addTab("Import URLs", importUrlConfigPanel);
        tabbedPane.addTab("Result Level", resultLevelPanel);
//        tabbedPane.addTab("Result Order", resultOrderPanelTest);
        this.pack();
        this.setLocationRelativeTo(parent);
    }
    
}
