/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.ui.commandline.commands;

import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.cnrs.lam.dis.etc.ui.commandline.ResultOutputHolder;
import org.cnrs.lam.dis.etc.ui.commandline.ScannerHolder;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class OutLevel implements Command {

    @Override
    public void execute() {
        try {
            int level = ScannerHolder.getScanner().nextInt();
            switch (level) {
                case 1:
                    ResultOutputHolder.setLevel(Level.FINAL);
                    break;
                case 2:
                    ResultOutputHolder.setLevel(Level.INTERMEDIATE_IMPORTANT);
                    break;
                case 3:
                    ResultOutputHolder.setLevel(Level.INTERMEDIATE_UNIMPORTANT);
                    break;
                case 4:
                    ResultOutputHolder.setLevel(Level.DEBUG);
                    break;
                default:
                    throw new Exception();
            }
            System.out.println("Output level is set to " + ResultOutputHolder.getLevel());
        } catch (Exception e) {
            System.out.println("Sorry... the level must be a number between 1 and 4.");
        }
        // Ignore the rest of the line
        ScannerHolder.getScanner().nextLine();
    }

    @Override
    public String help() {
        return "Description: Sets the level of results to be saved.\n" +
               "Usage: outlevel <level>\n" +
               "where <level> can be one of the following:\n" +
               "1: Only final results are saved\n" +
               "1: Also important intermediate results are saved\n" +
               "2: All the intermediate results are saved\n" +
               "3: All the results are saved (debug mode)\n";
    }

}
