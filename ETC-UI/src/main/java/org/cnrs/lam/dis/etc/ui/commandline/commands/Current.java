/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.ui.commandline.commands;

import java.lang.reflect.Method;
import org.apache.log4j.Logger;
import org.cnrs.lam.dis.etc.datamodel.Instrument;
import org.cnrs.lam.dis.etc.datamodel.ObsParam;
import org.cnrs.lam.dis.etc.datamodel.Site;
import org.cnrs.lam.dis.etc.datamodel.Source;
import org.cnrs.lam.dis.etc.ui.commandline.ScannerHolder;
import org.cnrs.lam.dis.etc.ui.generic.SessionHolder;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class Current implements Command {

    private Logger logger = Logger.getLogger(Current.class);

    @Override
    public void execute() {
        String component = ScannerHolder.getScanner().nextLine().trim();
        if ("".equals(component)) {
            System.out.println("Instrument : " + SessionHolder.getSession().getInstrument().getInfo().toString());
            System.out.println("Site : " + SessionHolder.getSession().getSite().getInfo().toString());
            System.out.println("Source : " + SessionHolder.getSession().getSource().getInfo().toString());
            System.out.println("Observing Parameters : " + SessionHolder.getSession().getObsParam().getInfo().toString());
        } else if ("instrument".equals(component) || "i".equals(component)) {
            Instrument instrument = SessionHolder.getSession().getInstrument();
            System.out.println("Current instrument: " + instrument.getInfo().toString());
            listContents(Instrument.class, instrument);
        } else if ("site".equals(component) || "si".equals(component)) {
            Site site = SessionHolder.getSession().getSite();
            System.out.println("Current site: " + site.getInfo().toString());
            listContents(Site.class, site);
        } else if ("source".equals(component) || "so".equals(component)) {
            Source source = SessionHolder.getSession().getSource();
            System.out.println("Current source: " + source.getInfo().toString());
            listContents(Source.class, source);
        } else if ("obsparam".equals(component) || "o".equals(component)) {
            ObsParam obsParam = SessionHolder.getSession().getObsParam();
            System.out.println("Current observing parameters: " + obsParam.getInfo().toString());
            listContents(ObsParam.class, obsParam);
        } else {
            System.out.println("Cannot give info for : " + component + ". Please give one of the following:");
            System.out.println("instrument, i, site, si, source, so, obsparam, o");
        }
    }

    private void listContents(Class type, Object object) throws SecurityException {
        for (Method method : type.getMethods()) {
            String methodName = method.getName();
            if (!(methodName.startsWith("get") || methodName.startsWith("is")) || methodName.endsWith("Unit")) {
                continue;
            }
            String propertyName = methodName.startsWith("get") ? methodName.substring(3) : methodName.substring(2);
            if ("Info".equals(propertyName))
                continue;
            System.out.print(propertyName + ": ");
            try {
                System.out.print(method.invoke(object));
            } catch (Exception ex) {
                logger.error("Failed to get the value for " + propertyName + " via reflection", ex);
            }
            try {
                Method unitMethod = type.getMethod(methodName + "Unit");
                System.out.print(" " + unitMethod.invoke(object));
            } catch (Exception ex) {
               // We do nothing here, because the exception is thrown from lack of the method
            }
            System.out.println("");
        }
    }

    @Override
    public String help() {
        return "Description: Gives information about the current configuration.\n" +
               "Usage: current <type>\n" +
               "where <type> can be any of the following:\n" +
               "(empty): to get the names of the components in use\n" +
               "instrument or i: to get information about the current instrument\n" +
               "site or si: to get information about the current site\n" +
               "source or so: to get information about the current source\n" +
               "obsparam or o: to get information about the current observing parameters\n";
    }

}
