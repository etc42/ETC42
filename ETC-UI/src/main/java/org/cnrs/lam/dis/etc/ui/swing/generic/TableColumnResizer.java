/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.ui.swing.generic;

import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

/**
 */
public class TableColumnResizer {
 
	public static void adjustColumnPreferredWidths(JTable table) {
		if (table == null || table.getColumnCount() == 0) {
			return;
		}
 
		// strategy - get max width for cells in column and
		// make that the preferred width
		TableColumnModel columnModel = table.getColumnModel();
 
		for (int col = 0; col < table.getColumnCount(); col++) {
			try {
				int maxwidth = 0;
				for (int row = 0; row < table.getRowCount(); row++) {
					TableCellRenderer rend = table.getCellRenderer(row, col);
					Object value = table.getValueAt(row, col);
					Component comp = rend.getTableCellRendererComponent(table, value, false, false, row, col);
					maxwidth = Math.max(comp.getPreferredSize().width, maxwidth);
				}
 
				TableColumn column = columnModel.getColumn(col);
				TableCellRenderer headerRenderer = column.getHeaderRenderer();
				if (headerRenderer == null) {
					headerRenderer = table.getTableHeader().getDefaultRenderer();
				}
				Object headerValue = column.getHeaderValue();
				Component headerComp = headerRenderer.getTableCellRendererComponent(table, headerValue, false, false, 0, col);
				maxwidth = Math.max(maxwidth, headerComp.getPreferredSize().width);
 
				column.setPreferredWidth(maxwidth + 10);
 
			} catch (Exception e) {
			}
		}
	}
 
	public static void adjustColumnPreferredWidths(JTable table, int columnIndex) {
		if (table == null || table.getColumnCount() == 0) {
			return;
		}
 
		// strategy - get max width for cells in column and
		// make that the preferred width
		TableColumnModel columnModel = table.getColumnModel();
 
		try {
			int maxwidth = 0;
			for (int row = 0; row < table.getRowCount(); row++) {
				TableCellRenderer rend = table.getCellRenderer(row, columnIndex);
				Object value = table.getValueAt(row, columnIndex);
				Component comp = rend.getTableCellRendererComponent(table, value, false, false, row, columnIndex);
				maxwidth = Math.max(comp.getPreferredSize().width, maxwidth);
			}
 
			TableColumn column = columnModel.getColumn(columnIndex);
			TableCellRenderer headerRenderer = column.getHeaderRenderer();
			if (headerRenderer == null) {
				headerRenderer = table.getTableHeader().getDefaultRenderer();
			}
			Object headerValue = column.getHeaderValue();
			Component headerComp = headerRenderer.getTableCellRendererComponent(table, headerValue, false, false, 0, columnIndex);
			maxwidth = Math.max(maxwidth, headerComp.getPreferredSize().width);
 
			column.setPreferredWidth(maxwidth + 10);
 
		} catch (Exception e) { }
	}
 
}
