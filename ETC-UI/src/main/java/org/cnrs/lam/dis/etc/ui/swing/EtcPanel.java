/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.ui.swing;

import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import org.apache.log4j.Logger;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.cnrs.lam.dis.etc.ui.generic.SessionHolder;
import org.cnrs.lam.dis.etc.ui.generic.SessionModifiedListener;

/**
 *
 * @author Nikolaos Apostolakos
 */
public abstract class EtcPanel extends JPanel {
	private static final long serialVersionUID = -4256212433176828557L;

	/** The logger of the class.*/
    private static Logger logger = Logger.getLogger(EtcPanel.class.getName());

    private SessionModifiedListener sessionModifiedListener = new SessionModifiedListener() {
        @Override
        public void sessionModified() {
            if (getSession() != null) {
                // First dissable the action listeners of all the combo boxes. This
                // is because the GUI should only be updated, but seting the selected
                // item of a combo box fires the action event which will update the
                // session, etc (which we do not want here)
                EtcPanel.this.dissableComboBoxesActionListeners();
                // Perform any actions to update the GUI
                EtcPanel.this.sessionModified();
                // Enable again all the listeners of the combo boxes
                EtcPanel.this.enableComboBoxesActionListeners();
            }
        }
    };

    public EtcPanel() {
        SessionHolder.addListener(sessionModifiedListener);
    }

    public final Session getSession() {
        return SessionHolder.getSession();
    }

    /**
     * This method must be overridden to update the GUI with the values in the
     * session retrieved with the getSession() method. Note that any action listeners
     * of ComboBoxes will NOT be triggered when the selected item is changed.
     */
    protected abstract void sessionModified();

    public abstract void setUserMode(UserMode mode);

    protected final void notifyAllForSessionModified() {
        SessionHolder.notifyForModification();
    }
    
    private Map<JComboBox, List<ActionListener>> comboBoxActionListenerMap
            = new HashMap<JComboBox, List<ActionListener>>();
    
    private void dissableComboBoxesActionListeners() {
        List<JComboBox> comboBoxes = getAllComboBoxes(this);
        for (JComboBox comboBox : comboBoxes) {
            List<ActionListener> actionListenerList = Arrays.asList(comboBox.getActionListeners());
            for (ActionListener actionListener : actionListenerList) {
                comboBox.removeActionListener(actionListener);
            }
            comboBoxActionListenerMap.put(comboBox, actionListenerList);
        }
    }
    
    private void enableComboBoxesActionListeners() {
        for (Map.Entry<JComboBox, List<ActionListener>> entry : comboBoxActionListenerMap.entrySet()) {
            JComboBox comboBox = entry.getKey();
            for (ActionListener listener : entry.getValue()) {
                comboBox.addActionListener(listener);
            }
        }
        comboBoxActionListenerMap.clear();
    }

    /**
     * This method returns all the combo boxes which are contained in the given container.
     * If the container contains other containers, the method goes in them and checks
     * for combo boxes also. It stops only when if finds EtcPanels, which will handle
     * their own combo boxes. This is done for the case normal JPanels are used
     * for organizing the combo boxes in an EtcPanel.
     * @return A list of all the ComboBoxes this Panel contains
     */
    private List<JComboBox> getAllComboBoxes(Container container) {
        List<JComboBox> comboBoxList = new ArrayList<JComboBox>();
        for (Component component : container.getComponents()) {
            // If the component is an EtcPanel we ignore it (it will handle its
            // own combo boxes)
            if (component instanceof EtcPanel) {
                continue;
            }
            // If the component is a combo box we add it to the list
            if (component instanceof JComboBox) {
                comboBoxList.add((JComboBox) component);
                continue;
            }
            // If the component is a container itself we add its combo boxes in the list
            if (component instanceof Container) {
                comboBoxList.addAll(getAllComboBoxes((Container) component));
            }
        }
        return comboBoxList;
    }
    
    private CommandHistoryPanel historyPanel;
    private String lastLog;
    
    /**
     * Adds the given line to the log panel. If the given line is null it is ignored.
     * Also repeated lines are ignored. The input line should be in HTML and it is
     * responsible for line breaks, etc.
     * @param line The line to add to the log panel
     */
    protected void addLogLine(String line) {
        if (line == null || line.equals(lastLog))
            return;
        if (historyPanel == null)
            historyPanel = getCommandHistoryPanel(this.getParent());
        // If the logPanel is still null means we cannot log, so we just return
        // This will be the case during testing, where the panel is not contained
        // in an EtcFrame.
        if (historyPanel == null)
            return;
        historyPanel.setAreaText(line);
        lastLog = line;
    }

    private CommandHistoryPanel getCommandHistoryPanel(Container container) {
        if (container == null) {
            logger.warn("ERROR!!! EtcPanel " + this + " is not contained in an EtcFrame."
                    + " This should never be the case (with exception JUnit tests).");
            return null;
        }
        if (container instanceof EtcFrame) {
            return ((EtcFrame) container).getCommandHistoryPanel();
        }
        return getCommandHistoryPanel(container.getParent());
    }

}
