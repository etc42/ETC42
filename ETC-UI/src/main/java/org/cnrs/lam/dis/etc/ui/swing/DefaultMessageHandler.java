/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.ui.swing;

import java.util.List;
import java.util.ResourceBundle;
import javax.swing.JOptionPane;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.cnrs.lam.dis.etc.ui.swing.generic.NameDescriptionDialog;

/**
 */
public class DefaultMessageHandler implements MessageHandler {
    
    private EtcFrame parent;
    private final ResourceBundle bundle = java.util.ResourceBundle.getBundle("org/cnrs/lam/dis/etc/ui/swing/messages");
    private ProgressDialog progressDialog;
    private NameDescriptionDialog nameDescriptionDialog;

    public DefaultMessageHandler(EtcFrame parent) {
        this.parent = parent;
        progressDialog = new ProgressDialog(parent, true);
        nameDescriptionDialog = new NameDescriptionDialog();
        nameDescriptionDialog.setLocationRelativeTo(parent);
        nameDescriptionDialog.setModal(true);
        nameDescriptionDialog.setName("nameDescriptionDialog");
    }

    @Override
    public void info(String message) {
        JOptionPane.showMessageDialog(parent, message, bundle.getString("INFO_DIALOG_TITLE"), JOptionPane.INFORMATION_MESSAGE);
    }

    @Override
    public void warn(String message) {
        JOptionPane.showMessageDialog(parent, message, bundle.getString("WARN_DIALOG_TITLE"), JOptionPane.WARNING_MESSAGE);
    }

    @Override
    public void error(String message) {
    	System.out.println("ERREUR : " + message);
        JOptionPane.showMessageDialog(parent, message, bundle.getString("ERROR_DIALOG_TITLE"), JOptionPane.ERROR_MESSAGE);
    }

    @Override
    public void actionStarted(String message, Thread thread) {
        progressDialog.setMessage(message);
        progressDialog.setLocationRelativeTo(parent);
        parent.setEnabled(false);
        new Thread(new Runnable() {
            @Override
            public void run() {
                progressDialog.setVisible(true);
            }
        }).start();
    }

    @Override
    public void actionStoped(Thread thread) {
        progressDialog.setVisible(false);
        parent.setEnabled(true);
        parent.setFocusable(true);
    }

    @Override
    public String decision(String message, List<String> options) {
        int selected = JOptionPane.showOptionDialog(parent, message, bundle.getString("DECISION_DIALOG_TITLE"),
                JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, options.toArray(), null);
        if (selected == JOptionPane.CLOSED_OPTION) {
            return null;
        }
        return options.get(selected);
    }

    @Override
    public String requestInput(String message, List<String> possibilities) {
        if (possibilities != null) {
            return (String) JOptionPane.showInputDialog(parent, message, null, JOptionPane.PLAIN_MESSAGE, null, possibilities.toArray(), "");
        } else {
            return JOptionPane.showInputDialog(message);
        }
    }

    @Override
    public DatasetInfo getNewDatasetInfo() {
        nameDescriptionDialog.setVisible(parent, true);
        if (!nameDescriptionDialog.isOkSelected()) {
            return null;
        }
        String name = nameDescriptionDialog.getSelectedName();
        String description = nameDescriptionDialog.getSelectedDescription();
        return new DatasetInfo(name, null, description);
    }
    
}
