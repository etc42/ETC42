package org.cnrs.lam.dis.etc.ui.swing.importer;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import javax.swing.ProgressMonitor;
import javax.swing.SwingWorker;

import org.apache.log4j.Logger;
import org.cnrs.lam.dis.etc.configuration.ConfigFactory;
import org.cnrs.lam.dis.etc.datamodel.ComponentInfo;
import org.cnrs.lam.dis.etc.datamodel.ComponentType;
import org.cnrs.lam.dis.etc.ui.generic.InstrumentListenerHolder;
import org.cnrs.lam.dis.etc.ui.generic.ObsParamListenerHolder;
import org.cnrs.lam.dis.etc.ui.generic.SiteListenerHolder;
import org.cnrs.lam.dis.etc.ui.generic.SourceListenerHolder;
import org.cnrs.lam.dis.etc.ui.swing.DefaultMessageHandler;
import org.cnrs.lam.dis.etc.ui.swing.EtcFrame;
import org.cnrs.lam.dis.etc.ui.swing.MessengerHolder;
import org.javatuples.Pair;
import org.javatuples.Triplet;

public class FileImportWorker extends SwingWorker<Void, Pair<String, Integer>> {

	private ProgressMonitor progressMonitor;
	private List<String> errorMessageList = Collections.synchronizedList(new LinkedList<String>());
	private List<Triplet<String, String, URL>> instrumentList;
	private List<Triplet<String, String, URL>> siteList;
	private List<Triplet<String, String, URL>> sourceList;
	private List<Triplet<String, String, URL>> obsParamList;
	private MultiImportTool importTool = null;
	private volatile int totalSize = 0;
	private int sizeDone = 0;
	private ResourceBundle bundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/ui/swing/messages");
	private static Logger logger = Logger.getLogger(FileImportWorker.class);
	private BlockingQueue<Thread> threadToWaitQueue = new ArrayBlockingQueue<Thread>(1);


	public FileImportWorker(
			MultiImportTool importTool,
			ProgressMonitor progressMonitor,
			List<Triplet<String, String, URL>> instrumentList,
			List<Triplet<String, String, URL>> siteList,
			List<Triplet<String, String, URL>> sourceList,
			List<Triplet<String, String, URL>> obsParamList) {
		this.importTool = importTool;
		this.progressMonitor = progressMonitor;
		if (this.progressMonitor == null) {
			this.progressMonitor = new ProgressMonitor(
				EtcFrame.getInstance(),
				bundle.getString("MULTI_IMPORT_MESSAGE"),
				" ", 0, 100
			);
			this.progressMonitor.setMillisToDecideToPopup(0);
			this.progressMonitor.setMillisToPopup(0);
			this.progressMonitor.setProgress(0);
		}
		this.instrumentList = instrumentList;
		this.siteList = siteList;
		this.sourceList = sourceList;
		this.obsParamList = obsParamList;
	}

	@Override
	protected Void doInBackground() throws Exception {
		// We mute the MultiImportWindow
		if (importTool != null)
			importTool.setEnabled(false);

		// We need to handle all the messages comming, so we set our own handler

		EtcFrame etcFrame = EtcFrame.getInstance();
		MessengerHolder.setHandler(new DefaultMessageHandler(etcFrame) {
			private final ResourceBundle controllerBundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/controller/messages");
			
			// When we get the message that an action was started we need to
			// wait for it to finish so we add its thread in the waiting queue
			@Override
			public void actionStarted(String message, Thread thread) {
				boolean inTheQueue = false;
				while (!inTheQueue) {
					try {
						threadToWaitQueue.put(thread);
						inTheQueue = true;
					} catch (InterruptedException ex) {
					}
				}
			}
			
			@Override
			public void actionStoped(Thread thread) {
				// We do nothing here
			}
			
			@Override
			public void error(String message) {
				errorMessageList.add(message);
			}
			
			@Override
			public void warn(String message) {
				errorMessageList.add(message);
			}
			
			@Override
			public String decision(String message, List<String> options) {
				return controllerBundle.getString("IGNORE_OPTION");
			}
			
		});


		// Calculate the total size
		totalSize += 2 * instrumentList.size();
		totalSize += 2 * siteList.size();
		totalSize += 2 * sourceList.size();
		totalSize += 2 * obsParamList.size();

		// Import all the components
		importComponentsFromList(ComponentType.INSTRUMENT, instrumentList);
		importComponentsFromList(ComponentType.SITE, siteList);
		importComponentsFromList(ComponentType.SOURCE, sourceList);
		importComponentsFromList(ComponentType.OBS_PARAM, obsParamList);

		// Set the messenger back to the default
		MessengerHolder.setDefaultHandler();

		// We show any error messages
		if (!errorMessageList.isEmpty()) {
			StringBuilder errorMessage = new StringBuilder(bundle.getString("MULTI_IMPORT_ERROR"));
			for (String error : errorMessageList) {
				errorMessage.append(error);
				errorMessage.append("\n");
			}
			MessengerHolder.getMessenger(EtcFrame.getInstance()).error(errorMessage.toString());
		}

		return null;
	}

	private void importComponentsFromList(ComponentType componentType, List<Triplet<String, String, URL>> componentList) {
		for (Triplet<String, String, URL> triplet : componentList) {
			// If the importing is canceled we return here
			if (isCancelled())
				return;

			String name = triplet.getValue0();
			String description = triplet.getValue1();
			URL url = triplet.getValue2();

			// Download the file
			publish(new Pair<String, Integer>("Downloading " + name, sizeDone));
			// We try to let other threads to run and then we check if we have been canceled
			Thread.yield();
			if (isCancelled()) {
				return;
			}
			File downloadedFile = null;
			try {
				int bufSize = 8 * 1024;
				BufferedInputStream urlStream = new BufferedInputStream(url.openStream(), bufSize);
				File tempDir = new File(ConfigFactory.getConfig().getTempDir());
				tempDir.mkdirs();
				String fileName = url.getFile();
				if (fileName.contains("/")) {
					fileName = fileName.substring(fileName.lastIndexOf('/') + 1);
				}
				downloadedFile = new File(tempDir, fileName);
				downloadedFile.delete();
				downloadedFile.createNewFile();
				downloadedFile.deleteOnExit();
				BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(downloadedFile), bufSize);
				byte[] buffer = new byte[bufSize];
				int read = -1;
				while ((read = urlStream.read(buffer)) >= 0) {
					out.write(buffer, 0, read);
				}
				out.flush();
				urlStream.close();
				out.close();
				sizeDone++;
			} catch (IOException ex) {
				logger.error("Failed to create temporary file", ex);
				errorMessageList.add("Failed to download " + url);
				sizeDone += 2;
				continue;
			}

			// If the importing is canceled we return here
			if (isCancelled())
				return;

			// Now import the file
			publish(new Pair<String, Integer>("Importing " + name, sizeDone));
			// We try to let other threads to run and then we check if we have been canceled
			Thread.yield();
			if (isCancelled()) {
				return;
			}
			ComponentInfo info = new ComponentInfo(name, description);
			switch (componentType) {
			case INSTRUMENT:
				InstrumentListenerHolder.getInstrumentListener().importFromFile(downloadedFile, info);
				break;
			case SITE:
				SiteListenerHolder.getSiteListener().importFromFile(downloadedFile, info);
				break;
			case SOURCE:
				SourceListenerHolder.getSourceListener().importFromFile(downloadedFile, info);
				break;
			case OBS_PARAM:
				ObsParamListenerHolder.getObsParamListener().importFromFile(downloadedFile, info);
				break;
			}
			// The handler will start a new thread for doing the job. we wait till
			// it will finish
			Thread importingThread = null;
			while (importingThread == null) {
				try {
					importingThread = threadToWaitQueue.take();
				} catch (InterruptedException ex) {
				}
			}
			while (importingThread.isAlive()) {
				try {
					importingThread.join();
				} catch (InterruptedException ex) {
				}
			}
			sizeDone++;
		}
	}

	@Override
	protected void process(List<Pair<String, Integer>> chunks) {
		Pair<String, Integer> pair = chunks.get(chunks.size() -1);
		if (pair.getValue0() != null) {
			progressMonitor.setNote(pair.getValue0());
		}
		progressMonitor.setProgress((int)(pair.getValue1() * 100. / totalSize));
		if (progressMonitor.isCanceled()) {
			this.cancel(false);
		}
	}

	@Override
	protected void done() {
		// We close the progress monitor and the multi import window
		progressMonitor.setProgress(101);
		if (importTool != null)
			importTool.dispose();
	}

}