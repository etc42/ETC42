package org.cnrs.lam.dis.etc.ui.swing;

import java.awt.Color;
import java.awt.Component;
import java.awt.Image;
import java.awt.Toolkit;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class SplashScreen extends JDialog {

	private static final long serialVersionUID = 4138764285362948040L;
	private static SplashScreen instance = null;
	private JPanel wrapper = new JPanel();
	private ImageIcon logo;
	
	private ImageIcon logoCesam;
	private ImageIcon logoLam;
	
	
	private JLabel progressionText = new JLabel("");
	
	public static SplashScreen getInstance() {
		if (instance == null) {
			instance = new SplashScreen();
		}
		
		return instance;
	}

	public SplashScreen() {
		super();
		setUndecorated(true);
		setType(javax.swing.JFrame.Type.UTILITY);
		setLocationRelativeTo(null);
		setAutoRequestFocus(false);
		setModal(false);
		
		int borderSize = 20;		
		wrapper.setLayout(new BoxLayout(wrapper, BoxLayout.Y_AXIS));
		
		// LogoETC
		Image logoImage = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/org/cnrs/lam/dis/etc/ui/swing/images/Etc-big.png"));
		logo = new ImageIcon(logoImage.getScaledInstance(125, 125, Image.SCALE_SMOOTH));
		JLabel logoLabel = new JLabel(logo);
		logoLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		logoLabel.setBorder(new EmptyBorder(borderSize, borderSize, borderSize, borderSize));
		wrapper.add(logoLabel);
		
		// Loading part
		Box horizontalLoaderGroup = Box.createHorizontalBox();
		
		// Loading text
		progressionText.setAlignmentY(Component.CENTER_ALIGNMENT);
		progressionText.setBorder(new EmptyBorder(0, borderSize, 0, 0));
		
		// Loading icon
		Image loadingImage = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/org/cnrs/lam/dis/etc/ui/swing/images/loading.gif"));
		JLabel loadingLabel = new JLabel(new ImageIcon(loadingImage));
		loadingLabel.setAlignmentY(Component.CENTER_ALIGNMENT);
		
		horizontalLoaderGroup.add(loadingLabel);
		horizontalLoaderGroup.add(progressionText);
		
		wrapper.add(horizontalLoaderGroup);
		
		Box horizontalLogoGroup = Box.createHorizontalBox();
		
		// LogoLAM
		Image logoLamImage = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/org/cnrs/lam/dis/etc/ui/swing/images/LAM.jpg"));
		logoLam = new ImageIcon(logoLamImage.getScaledInstance(110, 35, Image.SCALE_SMOOTH));
		JLabel logoLamLabel = new JLabel(logoLam);
		logoLamLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
		logoLamLabel.setBorder(new EmptyBorder(borderSize, borderSize, borderSize, borderSize * 3));
		horizontalLogoGroup.add(logoLamLabel);
		
		
		// LogoCesam
		Image logoCesamImage = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/org/cnrs/lam/dis/etc/ui/swing/images/Cesam.png"));
		logoCesam = new ImageIcon(logoCesamImage.getScaledInstance(61, 35, Image.SCALE_SMOOTH));
		JLabel logoCesamLabel = new JLabel(logoCesam);
		logoCesamLabel.setAlignmentX(Component.RIGHT_ALIGNMENT);
		logoCesamLabel.setBorder(new EmptyBorder(borderSize, borderSize * 3, borderSize, borderSize));
		horizontalLogoGroup.add(logoCesamLabel);

		wrapper.add(horizontalLogoGroup);
		
		wrapper.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
		
		add(wrapper);
		pack();
		setBounds(this.getX()-(this.getWidth()/2), this.getY()-(this.getHeight()/2), this.getWidth(), this.getHeight());
		revalidate();
	}
	
	public void revalidate() {
		this.wrapper.revalidate();
	}
	
	public void setProgression(String progressionText) {
		this.progressionText.setText(progressionText + "...");
	}
}
