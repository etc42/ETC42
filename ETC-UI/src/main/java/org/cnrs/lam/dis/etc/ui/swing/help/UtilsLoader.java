package org.cnrs.lam.dis.etc.ui.swing.help;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

public class UtilsLoader {
	public static void openWebpage(URI uri) {
	    Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
	    if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
	        try {
	            desktop.browse(uri);
	        } catch (Exception e) {}
	    }
	}
	
	public static void openMail(String urlString) {
		Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
		if (desktop != null && desktop.isSupported(Desktop.Action.MAIL)) {
			try {
				Desktop.getDesktop().mail(new URI("mailto:" + urlString + "?subject=ETC-42"));
			} catch (IOException | URISyntaxException e) {
				System.out.println(e.getMessage());
			}
		}
	}
	
	public static void openWebpage(URL url) {
	    try {
	        openWebpage(url.toURI());
	    } catch (URISyntaxException e) {}
	}
}
