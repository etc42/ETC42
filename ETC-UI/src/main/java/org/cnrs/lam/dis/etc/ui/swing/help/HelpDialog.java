package org.cnrs.lam.dis.etc.ui.swing.help;


import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class HelpDialog extends JDialog implements ActionListener {

	private static final long serialVersionUID = 690722433853815375L;
	protected JPanel buttonPane = new JPanel();
	
	public HelpDialog(JFrame parent, String title) {
		super(parent, title, true);

		
		JButton okButton = new JButton("OK");
		buttonPane.add(okButton);
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		okButton.addActionListener(this);
		
		pack();
		setLocationRelativeTo(parent);
		setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
		setResizable(false);
	}

	public HelpDialog(JFrame parent, String title, String htmlText) {
		this(parent, title);
		
		htmlText = "<html>" + htmlText + "</html>";

		JPanel textPane = new JPanel();
		JLabel textLabel = new JLabel(htmlText, SwingConstants.CENTER);
		textLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		textPane.add(textLabel);
		getContentPane().add(textPane);

		pack();
	}
	
	public void actionPerformed(ActionEvent e) {
		setVisible(false); 
		dispose();
	}
}