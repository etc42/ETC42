/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.ui.commandline.commands;

import org.cnrs.lam.dis.etc.datamodel.ComponentInfo;
import org.cnrs.lam.dis.etc.ui.commandline.ScannerHolder;
import org.cnrs.lam.dis.etc.ui.generic.InstrumentListenerHolder;
import org.cnrs.lam.dis.etc.ui.generic.ObsParamListenerHolder;
import org.cnrs.lam.dis.etc.ui.generic.SiteListenerHolder;
import org.cnrs.lam.dis.etc.ui.generic.SourceListenerHolder;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class Load implements Command {

    @Override
    public void execute() {
        String userInput = ScannerHolder.getScanner().nextLine().trim();
        if (userInput.indexOf(' ') == -1) {
            System.out.println("Wrong arguments. Please run 'help load'.");
            return;
        }
        String type = userInput.substring(0, userInput.indexOf(' ')).trim();
        String name = userInput.substring(userInput.indexOf(' ')).trim();
        if ("instrument".equals(type) || "i".equals(type)) {
            InstrumentListenerHolder.getInstrumentListener().loadInstrument(new ComponentInfo(name, null));
        } else if ("site".equals(type) || "si".equals(type)) {
            SiteListenerHolder.getSiteListener().loadSite(new ComponentInfo(name, null));
        } else if ("source".equals(type) || "so".equals(type)) {
            SourceListenerHolder.getSourceListener().loadSource(new ComponentInfo(name, null));
        } else if ("obsparam".equals(type) || "o".equals(type)) {
            ObsParamListenerHolder.getObsParamListener().loadObsParam(new ComponentInfo(name, null));
        } else {
            System.out.println("Unknown type : " + type + ". Please give one of the following:");
            System.out.println("instrument, i, site, si, source, so, obsparam, o");
        }
    }

    @Override
    public String help() {
        return "Description: Loads a component.\n" +
               "Usage: load <type> <name>\n" +
               "where <type> can be any of the following:\n" +
               "instrument or i: to load an instrument\n" +
               "site or si: to load a site\n" +
               "source or so: to load a source\n" +
               "obsparam or o: to load observing parameters\n" +
               "and <name> is the name of the component to be loaded\n";
    }

}
