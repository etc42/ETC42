/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.ui.swing.result;

import java.awt.BorderLayout;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TreeMap;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.cnrs.lam.dis.etc.datamodel.Image;
import org.cnrs.lam.dis.etc.ui.swing.generic.ClosableTabComponent;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class ImagesTabbedPane extends LogoTabbedPane {

    private static ResourceBundle bundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/ui/swing/messages");

    public <T extends Number> void showImage(String title, Image<T> image) {
        add(title, new ShowImagePanel(image));
        ClosableTabComponent resultTabComponent = new ClosableTabComponent(this, title);
        int index = getTabCount() - 1;
        setTabComponentAt(index, resultTabComponent);
        setSelectedIndex(index);
    }

    public <K extends Number, I extends Number> void showImageSet(String title, Map<K, Image<I>> imageMap, String keyUnit) {
        add (title, new ShowImageSetPanel(imageMap, keyUnit));
        ClosableTabComponent resultTabComponent = new ClosableTabComponent(this, title);
        int index = getTabCount() - 1;
        setTabComponentAt(index, resultTabComponent);
        setSelectedIndex(index);
    }

    private class ShowImageSetPanel<K extends Number, I extends Number> extends JPanel implements ChangeListener {
        private Map<K, Image<I>> imageMap;
        private Map<Integer, K> keyMap;
        private JSlider slider;
        private JLabel selectedValue;
        private ShowImagePanel<I> imagePanel;
        private String keyUnit;
        private NumberFormat formatter = new DecimalFormat("#0.##");

        public ShowImageSetPanel(Map<K, Image<I>> imageMap, String keyUnit) {
            this.imageMap = imageMap;
            this.keyUnit = keyUnit;
            this.setLayout(new BorderLayout(5,5));
            // Create a map between the keys of the map and increasing numbers
            // so we can select easily which image the user selects with the slider
            keyMap = new TreeMap<Integer, K>();
            int i = 1;
            for (K key : imageMap.keySet()) {
                keyMap.put(i, key);
                i++;
            }
            // Create the slider
            JPanel sliderPanel = new JPanel(new BorderLayout());
            slider = new JSlider(1, i-1);
            slider.addChangeListener(this);
            slider.setSnapToTicks(true);
            sliderPanel.add(slider, BorderLayout.CENTER);
            selectedValue = new JLabel();
            sliderPanel.add(selectedValue, BorderLayout.EAST);
            this.add(sliderPanel, BorderLayout.SOUTH);
            // Set the first image as selected
            slider.setValue(1);
        }

        @Override
        public void stateChanged(ChangeEvent e) {
            if (imagePanel != null) {
                this.remove(imagePanel);
            }
            imagePanel = new ShowImagePanel<I>(imageMap.get(keyMap.get(slider.getValue())));
            this.add(imagePanel, BorderLayout.CENTER);
            selectedValue.setText(formatter.format(keyMap.get(slider.getValue())) + (keyUnit == null ? "" : (" " + keyUnit)));
        }
    }

}
