/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.ui;

import java.util.List;
import org.cnrs.lam.dis.etc.datamodel.ComponentInfo;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.javatuples.Triplet;

/**
 * The <code>InfoProvider</code> object is designed to provide to the GUI part
 * of ETC different kinds of information necessary for presentation to the
 * user, like the lists of different instruments, sites, etc available by the
 * system.
 *
 * @author Nikolaos Apostolakos
 */
public interface InfoProvider {

    /**
     * Implementations of this method should return a list of the available
     * datasets of the given type and namespace, which are available for loading.
     * If the namespace is null only the globaly available datasets are returned. The returned
     * list contains objects of type <code>DatasetInfo</code> which contain the
     * name, namespace and description of the dataset. If there are no datasets
     * available it should return an empty list. If the empty string is passed as
     * namespace ("", not null), then all the datasets of the given type are returned.
     * @param type The type of datasets to return
     * @return A list of the available datasets for the given type
     */
    public List<DatasetInfo> getAvailableDatasetList(Dataset.Type type, String namespace);

    //TODO: this needs to be defined when session is shorted out
    /**
     * To be defined....
     * @return
     */
    public List<String> getAvailableSessionList();

    /**
     * Implementations of this method should return a list of the available
     * instruments for loading. The returned list contains objects of type
     * <code>ComponentInfo</code>, which contain the name and a description of
     * the instrument. If there are no instruments available it should return
     * an empty list.
     *
     * @return A list of the available instruments for loading
     */
    public List<ComponentInfo> getAvailableInstrumentList();

    /**
     * Implementations of this method should return a list of the available
     * sites for loading. The returned list contains objects of type
     * <code>ComponentInfo</code>, which contain the name and a description of
     * the site. If there are no sites available it should return an empty list.
     *
     * @return A list of the available sites for loading
     */
    public List<ComponentInfo> getAvailableSiteList();

    /**
     * Implementations of this method should return a list of the available
     * sources for loading. The returned list contains objects of type
     * <code>ComponentInfo</code>, which contain the name and a description of
     * the source. If there are no sources available it should return an empty
     * list.
     *
     * @return A list of the available sources for loading
     */
    public List<ComponentInfo> getAvailableSourceList();

    /**
     * Implementations of this method should return a list of the available
     * observing parameters for loading. The returned list contains objects of
     * type <code>ComponentInfo</code>, which contain the name and a description
     * of the observing parameters. If there are no observing parameters
     * available it should return an empty list.
     *
     * @return A list of the available observing parameters for loading
     */
    public List<ComponentInfo> getAvailableObsParamList();
    
    /**
     * <p>Returns a list of {@link org.javatuples.Triplet} which contain the necessary
     * information for the available plugins. The triplets contain the following
     * things (in this order):</p>
     * <ol>
     * <li>A {@link java.lang.Class} representing the type of the plugin</li>
     * <li>A list containing the menu path and name (which is ordered)</li>
     * <li>A String containing the tooltip description</li>
     * </ol>
     * @return The available plugins
     */
    public List<Triplet<Class, List<String>, String>> getAvailablePluginList();
    
}
