/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.ui.commandline.commands;

import org.cnrs.lam.dis.etc.datamodel.ComponentInfo;
import org.cnrs.lam.dis.etc.ui.commandline.ScannerHolder;
import org.cnrs.lam.dis.etc.ui.generic.InfoProviderHolder;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class List implements Command {

    @Override
    public void execute() {
        String component = ScannerHolder.getScanner().nextLine().trim();
        if ("instrument".equals(component) || "i".equals(component)) {
            System.out.println("Available instruments:");
            for (ComponentInfo info : InfoProviderHolder.getInfoProvider().getAvailableInstrumentList()) {
                System.out.println(info.toString());
            }
        } else if ("site".equals(component) || "si".equals(component)) {
            System.out.println("Available sites:");
            for (ComponentInfo info : InfoProviderHolder.getInfoProvider().getAvailableSiteList()) {
                System.out.println(info.toString());
            }
        } else if ("source".equals(component) || "so".equals(component)) {
            System.out.println("Available sources:");
            for (ComponentInfo info : InfoProviderHolder.getInfoProvider().getAvailableSourceList()) {
                System.out.println(info.toString());
            }
        } else if ("obsparam".equals(component) || "o".equals(component)) {
            System.out.println("Available observing parameters:");
            for (ComponentInfo info : InfoProviderHolder.getInfoProvider().getAvailableObsParamList()) {
                System.out.println(info.toString());
            }
        } else {
            System.out.println("Cannot list : " + component + ". Please give one of the following:");
            System.out.println("instrument, i, site, si, source, so, obsparam, o");
        }
    }

    @Override
    public String help() {
        return "Description: Gives a list of the available components.\n" +
               "Usage: list <type>\n" +
               "where <type> can be one of the following:\n" +
               "instrument or i: to get the available instruments\n" +
               "site or si: to get the available sites\n" +
               "source or so: to get the available sources\n" +
               "obsparam or o: to get the available observing parameters\n";
    }

}
