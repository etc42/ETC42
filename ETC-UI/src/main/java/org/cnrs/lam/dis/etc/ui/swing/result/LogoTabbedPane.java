/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.ui.swing.result;

import java.awt.Graphics;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JTabbedPane;

/**
 */
public class LogoTabbedPane extends JTabbedPane {
    
    private ImageIcon logo = null;

    public Image getLogo() {
        return (logo == null) ? null : logo.getImage();
    }

    public void setLogo(Image logo) {
        if (logo == null) {
            this.logo = null;
        } else {
            this.logo = new ImageIcon(logo);
        }
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        if (logo != null && this.getTabCount() == 0) {
            int width = this.getWidth();
            int height = this.getHeight();
            int logoWidth = logo.getIconWidth();
            int logoHeight = logo.getIconHeight();
            if (width >= logoWidth && height >= logoHeight) {
                // Here we do not need to make any scaling
                logo.paintIcon(this, g, (width - logoWidth) / 2, (height - logoHeight) / 2);
            } else {
                // here we must scale the logo
                int size = Math.min(width, height);
                Image scaledImage = logo.getImage().getScaledInstance(size, size, Image.SCALE_FAST);
                ImageIcon scaledLogo = new ImageIcon(scaledImage);
                scaledLogo.paintIcon(this, g, (width - size) / 2, (height - size) / 2);
            }
        }
    }

}
