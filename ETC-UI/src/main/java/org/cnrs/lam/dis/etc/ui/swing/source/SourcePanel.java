/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.ui.swing.source;

import java.awt.event.ItemListener;
import javax.swing.DefaultComboBoxModel;
import org.cnrs.lam.dis.etc.datamodel.ComponentInfo;
import org.cnrs.lam.dis.etc.ui.generic.InfoProviderHolder;
import org.cnrs.lam.dis.etc.ui.generic.SourceListenerHolder;
import org.cnrs.lam.dis.etc.ui.swing.EtcPanel;
import org.cnrs.lam.dis.etc.ui.swing.UserMode;
import org.cnrs.lam.dis.etc.ui.swing.generic.InfoListCellRenderer;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class SourcePanel extends EtcPanel {

    /** Creates new form SourcePanel */
    public SourcePanel() {
        initComponents();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        saveAsDialog = new org.cnrs.lam.dis.etc.ui.swing.source.SaveAsSourceDialog();
        newDialog = new org.cnrs.lam.dis.etc.ui.swing.source.NewSourceDialog();
        sourceLabel = new javax.swing.JLabel();
        choiceLabel = new javax.swing.JLabel();
        descriptionLabel = new javax.swing.JLabel();
        choiceComboBox = new javax.swing.JComboBox();
        descriptionTextField = new javax.swing.JTextField();
        newButton = new javax.swing.JButton();
        reloadButton = new javax.swing.JButton();
        saveButton = new javax.swing.JButton();
        saveAsButton = new javax.swing.JButton();
        magnitudePanel = new org.cnrs.lam.dis.etc.ui.swing.source.MagnitudePanel();
        spatialDistributionPanel = new org.cnrs.lam.dis.etc.ui.swing.source.SpatialDistributionPanel();
        spectralDistributionPanel = new org.cnrs.lam.dis.etc.ui.swing.source.SpectralDistributionPanel();

        saveAsDialog.setLocationRelativeTo(this);
        saveAsDialog.setModal(true);
        saveAsDialog.setName("saveAsDialog"); // NOI18N

        newDialog.setLocationRelativeTo(this);
        newDialog.setModal(true);
        newDialog.setName("newDialog"); // NOI18N

        sourceLabel.setFont(new java.awt.Font("DejaVu Sans", 1, 18));
        sourceLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/cnrs/lam/dis/etc/ui/swing/images/Source.png"))); // NOI18N
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("org/cnrs/lam/dis/etc/ui/swing/messages"); // NOI18N
        sourceLabel.setText(bundle.getString("SOURCE")); // NOI18N
        sourceLabel.setName("sourceLabel"); // NOI18N

        choiceLabel.setText(bundle.getString("SOURCE_CHOICE")); // NOI18N
        choiceLabel.setName("choiceLabel"); // NOI18N

        descriptionLabel.setText(bundle.getString("DESCRIPTION")); // NOI18N
        descriptionLabel.setName("descriptionLabel"); // NOI18N

        choiceComboBox.setName("choiceComboBox"); // NOI18N
        choiceComboBox.setRenderer(new InfoListCellRenderer());
        choiceComboBox.addItemListener(new java.awt.event.ItemListener() {
            @Override
			public void itemStateChanged(java.awt.event.ItemEvent evt) {
                choiceComboBoxItemStateChanged(evt);
            }
        });

        descriptionTextField.setEditable(false);
        descriptionTextField.setName("descriptionTextField"); // NOI18N

        newButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/cnrs/lam/dis/etc/ui/swing/images/Add.png"))); // NOI18N
        newButton.setText(bundle.getString("NEW_BUTTON")); // NOI18N
        newButton.setName("newButton"); // NOI18N
        newButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                newButtonActionPerformed(evt);
            }
        });

        reloadButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/cnrs/lam/dis/etc/ui/swing/images/Reload.png"))); // NOI18N
        reloadButton.setText(bundle.getString("RELOAD_BUTTON")); // NOI18N
        reloadButton.setName("reloadButton"); // NOI18N
        reloadButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                reloadButtonActionPerformed(evt);
            }
        });

        saveButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/cnrs/lam/dis/etc/ui/swing/images/Save.png"))); // NOI18N
        saveButton.setText(bundle.getString("SAVE_BUTTON")); // NOI18N
        saveButton.setName("saveButton"); // NOI18N
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });

        saveAsButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/cnrs/lam/dis/etc/ui/swing/images/SaveAs.png"))); // NOI18N
        saveAsButton.setText(bundle.getString("SAVE_AS_BUTTON")); // NOI18N
        saveAsButton.setName("saveAsButton"); // NOI18N
        saveAsButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveAsButtonActionPerformed(evt);
            }
        });

        magnitudePanel.setName("magnitudePanel"); // NOI18N

        spatialDistributionPanel.setName("spatialDistributionPanel"); // NOI18N

        spectralDistributionPanel.setName("spectralDistributionPanel"); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(magnitudePanel, javax.swing.GroupLayout.DEFAULT_SIZE, 428, Short.MAX_VALUE)
                    .addComponent(sourceLabel)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(descriptionLabel)
                            .addComponent(choiceLabel))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(descriptionTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 347, Short.MAX_VALUE)
                            .addComponent(choiceComboBox, 0, 347, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(newButton, javax.swing.GroupLayout.DEFAULT_SIZE, 102, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(reloadButton, javax.swing.GroupLayout.DEFAULT_SIZE, 102, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(saveButton, javax.swing.GroupLayout.DEFAULT_SIZE, 102, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(saveAsButton, javax.swing.GroupLayout.DEFAULT_SIZE, 104, Short.MAX_VALUE))
                    .addComponent(spatialDistributionPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 428, Short.MAX_VALUE)
                    .addComponent(spectralDistributionPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 428, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(sourceLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(choiceLabel)
                    .addComponent(choiceComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(descriptionLabel)
                    .addComponent(descriptionTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(newButton)
                    .addComponent(reloadButton)
                    .addComponent(saveButton)
                    .addComponent(saveAsButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(magnitudePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(spatialDistributionPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(spectralDistributionPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void newButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newButtonActionPerformed
        newDialog.setLocationRelativeTo(this);
        newDialog.setVisible(true);
        addLogLine("<b>--- Command new source is not yet supported in command line mode ---</b><br/>");
    }//GEN-LAST:event_newButtonActionPerformed

    private void reloadButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_reloadButtonActionPerformed
        SourceListenerHolder.getSourceListener().reloadCurrentSource();
        addLogLine("<b>--- Command reload source is not yet supported in command line mode ---</b><br/>");
    }//GEN-LAST:event_reloadButtonActionPerformed

    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
        SourceListenerHolder.getSourceListener().saveCurrentSource();
        addLogLine("save source " + choiceComboBox.getSelectedItem() + "<br/>");
    }//GEN-LAST:event_saveButtonActionPerformed

    private void saveAsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveAsButtonActionPerformed
        saveAsDialog.setLocationRelativeTo(this);
        saveAsDialog.setVisible(true);
        addLogLine("<b>--- Command saveas source is not yet supported in command line mode ---</b><br/>");
    }//GEN-LAST:event_saveAsButtonActionPerformed

    private void choiceComboBoxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_choiceComboBoxItemStateChanged
        SourceListenerHolder.getSourceListener().loadSource((ComponentInfo) choiceComboBox.getSelectedItem());
        addLogLine("load source " + choiceComboBox.getSelectedItem() + "<br/>");
        setChoiseSelectedItem();
    }//GEN-LAST:event_choiceComboBoxItemStateChanged


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox choiceComboBox;
    private javax.swing.JLabel choiceLabel;
    private javax.swing.JLabel descriptionLabel;
    private javax.swing.JTextField descriptionTextField;
    private org.cnrs.lam.dis.etc.ui.swing.source.MagnitudePanel magnitudePanel;
    private javax.swing.JButton newButton;
    private org.cnrs.lam.dis.etc.ui.swing.source.NewSourceDialog newDialog;
    private javax.swing.JButton reloadButton;
    private javax.swing.JButton saveAsButton;
    private org.cnrs.lam.dis.etc.ui.swing.source.SaveAsSourceDialog saveAsDialog;
    private javax.swing.JButton saveButton;
    private javax.swing.JLabel sourceLabel;
    private org.cnrs.lam.dis.etc.ui.swing.source.SpatialDistributionPanel spatialDistributionPanel;
    private org.cnrs.lam.dis.etc.ui.swing.source.SpectralDistributionPanel spectralDistributionPanel;
    // End of variables declaration//GEN-END:variables

    @Override
    protected void sessionModified() {
        if (getSession() == null || getSession().getSource() == null)
            return;
        choiceComboBox.setModel(new DefaultComboBoxModel(InfoProviderHolder.getInfoProvider().getAvailableSourceList().toArray()));
        setChoiseSelectedItem();
        descriptionTextField.setText(getSession().getSource().getInfo().getDescription());
    }

    private void setChoiseSelectedItem() {
        // We don't want the listener that handles the changes of the combo box
        // to pick this up and try to load, so we remove temporarly the listener
        ItemListener[] listeners = choiceComboBox.getItemListeners();
        for (ItemListener listener : listeners) {
            choiceComboBox.removeItemListener(listener);
        }
        choiceComboBox.setSelectedItem(getSession().getSource().getInfo());
        // Now we can set the listeners back
        for (ItemListener listener : listeners) {
            choiceComboBox.addItemListener(listener);
        }
    }

    @Override
    public void setUserMode(UserMode mode) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
