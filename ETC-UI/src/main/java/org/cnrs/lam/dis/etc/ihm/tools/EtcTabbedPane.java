/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.ihm.tools;



import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Rectangle;


import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;

import org.cnrs.lam.dis.etc.ihm.ButtonTabComponent;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.entity.LegendItemEntity;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

/**
 * Classe qui gère les onglets.
 * @author ludovic
 */
public class EtcTabbedPane extends JTabbedPane implements MouseListener, MouseMotionListener{

	private static final Dimension tabSizeMin = new Dimension(1,1);
	private static final Dimension tabSizeMax = new Dimension(1,1);
	private static final Color systemColor = new Color(240,240,240);
	private int lastIndex = -1;

        /**
         * index de l'onglet en cours de déplacement.
         */
        private int indexMoving = -1;

        /**
         * Constructeur
         */
	public EtcTabbedPane(){

            super();

            initialize();
            
		
	}

        /**
         *  Rajoute un onglet avec un titre et son composant
         * @param title
         * @param component
         */
    @Override
	public void addTab(String title, Component component){
             super.addTab(title, component);
             //-- On ajoute le bouton de fermeture
             ButtonTabComponent cmdClose = new ButtonTabComponent(this);
             super.setTabComponentAt(this.getTabCount()-1, cmdClose);
	}

    
    public void addTab(EtcChartPanel component){
		addTab(component.getChart().getTitle().getText(), component);
	}



    /**
     * Reset la couleur   celle du system si un  l ment n'est pas s lectionner
     */
    public void lastIndexResetColor(){
    	if(lastIndex != -1 && lastIndex < this.getTabCount()){
    		((ButtonTabComponent)this.getTabComponentAt(lastIndex)).changeColor(systemColor);
    	}
    }

        /**
         *	G re la fin de d placemnt d'un objet lie
         *
         * @param index : index du tab, si -1 création d'un nouvel onglet 
         * @param lie
         */
        
    public void moveCurveTo(int indexDest, int indexSource, LegendItemEntity lie) throws IncorrectParentException{
    	String title = lie.getSeriesKey().toString().split("\\(\\d*\\)\\s\\s")[0];
    	((ETCXYSeries)((XYSeriesCollection)lie.getDataset()).getSeries(lie.getSeriesKey())).setKey(title);
    	/*
    	 * Si on se trouve dans le cas o  l'on d place l'objet vers rien
    	 * on cr   un nouvel onglet et on le charge avec la s rie
    	 * 
    	 */
    	if (indexDest == -1){
    		String type = ((ETCXYSeries)((XYSeriesCollection)lie.getDataset()).getSeries(title)).getTypeSeries();
    		String unitX = ((ETCXYSeries)((XYSeriesCollection)lie.getDataset()).getSeries(title)).getUnitX() ;
    		String unitY = ((ETCXYSeries)((XYSeriesCollection)lie.getDataset()).getSeries(title)).getUnitY();
    		indexDest =  newOnglet(title, type,
    				unitX,
    				unitY);
    		((XYSeriesCollection)((EtcChartPanel)
    				this.getComponentAt(indexDest)).getChart().getXYPlot().getDataset())
    				.addSeries(
    						((XYSeriesCollection)lie.getDataset()).getSeries(title));

    	}
    	else{
    		/*
    		 * Si on se d place sur un onglet existant :
    		 * test sur la nomination des  l ments pr sents
    		 * 
    		 */
    		int k = 0;
    		int countRepeat = 0;
    		String obj;
    		while(k< ((XYSeriesCollection)((EtcChartPanel)
    				this.getComponentAt(indexDest)).getChart().getXYPlot().getDataset()).getSeriesCount()){
    			obj = ((XYSeriesCollection)((EtcChartPanel)
    					this.getComponentAt(indexDest)).getChart().getXYPlot().getDataset()).getSeries(k)
    					.getKey().toString();
    			if(title.equals(obj.split("\\(\\d*\\)\\s")[0])){
    				if(obj.split("\\(").length !=1
    						&& obj.split("\\(")[1].split("\\)").length!=1
    						&& obj.split("\\(")[1].split("\\)")[1].equals("  ")
    						&& Integer.valueOf(obj.split("\\(")[1].split("\\)")[0]) >= countRepeat){
    					countRepeat = Integer.valueOf(obj.split("\\(")[1].split("\\)")[0])+1;
    				}else{
    					countRepeat ++;
    				}
    			}

    			k++;
    		}
    		//Si il ya des  l ments portant le m me nom 
    		if(countRepeat!=0){
    			XYSeries serie = ((XYSeriesCollection)lie.getDataset()).getSeries(title);
    			title = title+"("+countRepeat+")  ";
    			serie.setKey(title);
    			((XYSeriesCollection)((EtcChartPanel)
    					this.getComponentAt(indexDest)).getChart().getXYPlot().getDataset())
    					.addSeries(serie);
    		}
    		// ici on se trouve forcmeent dans le cas d'un multiplot
    		this.setTitleAt(indexDest, "MultiPlot");
    		((EtcChartPanel)this.getComponentAt(indexDest))
    		.getChart().setTitle("MultiPlot");
    		((EtcChartPanel)this.getComponentAt(indexDest))
    		.getChart().getXYPlot().getRangeAxis().setLabel("value");


    	}


    	//-- On vérifie que le graphique n'est pas vide sinon on l'efface --

    	//-- On supprime le graphique déplacé --
    	((XYSeriesCollection)(lie.getDataset()))
    	.removeSeries(
    			((XYSeriesCollection)lie.getDataset()).getSeries(title));

    	//Focus sur l' lement de destination
    	this.setSelectedIndex(indexDest);

    	//suppression de l'onglet si pas de s ries
    	if (getChartAt(indexSource).getChart().getXYPlot().getSeriesCount() == 0){

    		this.removeTabAt(indexSource);
    	}
    	//Si la s rie est unique apr s le d placemnt alors il retrouve ses propri t s
    	else if( getChartAt(indexSource).getChart().getXYPlot().getSeriesCount() == 1 ){
    		ETCXYSeries etcSerie = (ETCXYSeries) ((XYSeriesCollection)getChartAt(indexSource).getChart().getXYPlot().getDataset(0)).getSeries(0);
    		etcSerie.setKey(etcSerie.getKey().toString().split("\\(\\d*\\)\\s")[0]);
    		getChartAt(indexSource).getChart().setTitle(etcSerie.getTypeSeries());
    		getChartAt(indexSource).getChart().getXYPlot().getDomainAxis().setLabel(etcSerie.getUnitX());
    		getChartAt(indexSource).getChart().getXYPlot().getRangeAxis().setLabel(etcSerie.getUnitY());
    		this.setTitleAt(indexSource, ((XYSeriesCollection)getChartAt(indexSource).getChart().getXYPlot().getDataset(0)).getSeries(0).getKey().toString());
    	}


    }

    /**
     * Création d'un nouvel onglet ...
     * @return
     */
    private int newOnglet(String onglet, String title, String xLegend , String yLegend) throws IncorrectParentException{

    	//-- On crée un graphique vide --
    	JFreeChart jf = ChartFactory.createXYLineChart(title, xLegend, yLegend, new XYSeriesCollection() , PlotOrientation.VERTICAL, true, true, true);
    	EtcChartPanel cp = new EtcChartPanel(jf);
    	this.add(onglet, cp);
    	return this.getTabCount()-1;


    }

    /**
     * 
     * @param index
     * @return le Chart Panel à l'index donné
     */
    public ChartPanel getChartAt(int index){
    	return ((ChartPanel) this.getComponentAt(index));
    }

        
        
	/**
	 * @param tabbedpane JTabbedPane associ e
	 * @param x Coordonn e X
	 * @param y Coordonn e Y
	 * @return Retourne l'indice de l'onglet aux coordonnées x,y si il n'en existe pas retourne 1
	 */
	public int indexOfChild( int x, int y) {

                int result = -1;

                int i = 0;


                while (i< this.getTabCount() && result == -1 ){
                    Rectangle r = this.getBoundsAt(i);
			if (r.contains(x, y)){
				result = i;

                         }
                    i++;
                          
                }

                return result;
	}

        /**
         * Méthode chargée au démarrage
         */
	public void initialize(){



                
		//-- --//
		this.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
		this.setMinimumSize(tabSizeMin);
		this.setMaximumSize(tabSizeMax);
		
                
                //-- Gestion de l'évenement double clic sur un onglet --
                this.addMouseListener(this);
                this.addMouseMotionListener(this);

	}


        
	/**
         * Méthode qui permet de superposer les graphiques
         * @param indexSource : index source
         * @param indexDestination : index destination
         */
	public void superposeTab(int indexSource, int indexDestination){
			//Si la source est diff rente de la destination (immpose multiplot)
            if (indexSource != indexDestination){
            	//renome la destination
                this.setTitleAt(indexDestination, "MultiPlot");
                getChartAt(indexDestination).getChart().setTitle("MultiPlot");
                
                // For the case the datasets have different coefY, we need to
                // scale all the data to one coefY. Find the coefY which must
                // be used
                double finalCoefY = 0;
                // First check the coefY of all the source charts
                XYPlot xyPlot = getChartAt(indexSource).getChart().getXYPlot();
                for (int i = 0; i < xyPlot.getDatasetCount(); i++) {
                    for (int j = 0; j < ((XYSeriesCollection) xyPlot.getDataset(i)).getSeriesCount(); j++) {
                        XYSeries series = ((XYSeriesCollection) xyPlot.getDataset(i)).getSeries(j);
                        double coefY = ((ETCXYSeries) series).getCoefY();
                        if (finalCoefY < coefY)
                            finalCoefY = coefY;
                    }
                }
                // Then check the coefY of all the destination charts
                xyPlot = getChartAt(indexDestination).getChart().getXYPlot();
                for (int i = 0; i < xyPlot.getDatasetCount(); i++) {
                    for (int j = 0; j < ((XYSeriesCollection) xyPlot.getDataset(i)).getSeriesCount(); j++) {
                        XYSeries series = ((XYSeriesCollection) xyPlot.getDataset(i)).getSeries(j);
                        double coefY = ((ETCXYSeries) series).getCoefY();
                        if (finalCoefY < coefY)
                            finalCoefY = coefY;
                    }
                }
                
                // Scale all the plots of the source chart
                xyPlot = getChartAt(indexSource).getChart().getXYPlot();
                for (int i = 0; i < xyPlot.getDatasetCount(); i++) {
                    for (int j = 0; j < ((XYSeriesCollection) xyPlot.getDataset(i)).getSeriesCount(); j++) {
                        XYSeries series = ((XYSeriesCollection) xyPlot.getDataset(i)).getSeries(j);
                        ((ETCXYSeries) series).changeCoefY(finalCoefY);
                    }
                }
                
                // Scale all the plots of the destination chart
                xyPlot = getChartAt(indexDestination).getChart().getXYPlot();
                for (int i = 0; i < xyPlot.getDatasetCount(); i++) {
                    for (int j = 0; j < ((XYSeriesCollection) xyPlot.getDataset(i)).getSeriesCount(); j++) {
                        XYSeries series = ((XYSeriesCollection) xyPlot.getDataset(i)).getSeries(j);
                        ((ETCXYSeries) series).changeCoefY(finalCoefY);
                    }
                }
                
                int k;
                int countRepeat;
                for (int i=0; i<getChartAt(indexSource).getChart().getXYPlot().getDatasetCount(); i++){
                	k=0;
                	countRepeat = 0;
                    for ( int j=0; j<((XYSeriesCollection) getChartAt(indexSource).getChart().getXYPlot().getDataset(i)).getSeriesCount(); j++){
                    	String obj;
                    	String objSource = ((XYSeriesCollection)
                				getChartAt(indexSource).getChart().getXYPlot().getDataset(i))
                				.getSeries(j).getKey().toString();
                    	while(k<getChartAt(indexDestination).getChart().getXYPlot().getDataset(i).getSeriesCount()){
                    		obj = ((XYSeriesCollection)
                    				getChartAt(indexDestination).getChart().getXYPlot().getDataset(i))
                    				.getSeries(k).getKey().toString();
                    		System.out.println(obj);
                    		if(objSource.split("\\(\\d*\\)\\s")[0].equals(obj.split("\\(\\d*\\)\\s")[0])){
                    			if(obj.split("\\(").length !=1
                    					&& obj.split("\\(")[1].split("\\)").length!=1
                    						&& obj.split("\\(")[1].split("\\)")[1].equals("  ")
                    							&& Integer.valueOf(obj.split("\\(")[1].split("\\)")[0]) >= countRepeat){
                    				countRepeat = Integer.valueOf(obj.split("\\(")[1].split("\\)")[0])+1;
                    			}else{
                        			countRepeat ++;
                    			}
                    				
                    		}
                    		k++;
                    	}
                    	XYSeries serie = ((XYSeriesCollection)
                				getChartAt(indexSource).getChart().getXYPlot().getDataset(i))
                				.getSeries(j);
                    	if(countRepeat != 0){
                    		serie.setKey(serie.getKey().toString().split("\\(\\d*\\)\\s")[0]+"("+countRepeat+")  ");
                    	}
                        ((XYSeriesCollection) getChartAt(indexDestination).getChart().getXYPlot()
                        		.getDataset(0)).addSeries(((XYSeriesCollection)
                        				getChartAt(indexSource).getChart().getXYPlot().getDataset(i))
                        				.getSeries(j));

                    }
                }
                String label = finalCoefY != 1 ? "E" + (int)Math.log10(finalCoefY) + " value" : "value";
                getChartAt(indexDestination).getChart().getXYPlot().getRangeAxis().setLabel(label);
                this.setSelectedIndex(indexDestination);
                ((ButtonTabComponent)this.getTabComponentAt(indexDestination)).changeColor(systemColor);
                this.removeTabAt(indexSource);
             }

	}

    /**
     *
     * Evenement au clic de souris.
     * @param e
     */
    @Override
	public void mouseClicked(MouseEvent e) {

       int index = indexOfChild(e.getX(), e.getY());
       if ((e.getClickCount() == 2) && ( index != -1) ) {
            String title = JOptionPane.showInputDialog("New Title", this.getTitleAt(index));

            if (title != null){
                this.setTitleAt(this.getSelectedIndex(), title);
                if(((XYSeriesCollection)getChartAt(this.getSelectedIndex())
                		.getChart().getXYPlot().getDataset(0)).getSeriesCount()==1)
                ((XYSeriesCollection)getChartAt(this.getSelectedIndex())
                		.getChart().getXYPlot().getDataset(0))
                			.getSeries(0).setKey(title);
            }

       }
    }

    @Override
	public void mousePressed(MouseEvent e) {}

    /**
     * 
     * @param e
     */
    @Override
	public void mouseReleased(MouseEvent e) {

        this.setCursor(Cursor.getDefaultCursor());

        if (indexMoving != -1){
        	((ButtonTabComponent)this.getTabComponentAt(indexMoving)).changeColor(systemColor);
            int index2 = indexOfChild( e.getX(), e.getY());
            if (index2 != -1){
            	((ButtonTabComponent)this.getTabComponentAt(indexMoving)).changeColor(systemColor);
                this.superposeTab(indexMoving, index2);
            }
            indexMoving = -1;
            
        }
        lastIndexResetColor();
        lastIndex = -1;
        

    }

    @Override
	public void mouseEntered(MouseEvent e) {
        
    }

    @Override
	public void mouseExited(MouseEvent e) {
        
    }

    @Override
	public void mouseDragged(MouseEvent e) {
        
        if (indexMoving == -1){
            
            indexMoving = indexOfChild(e.getX(), e.getY());
            this.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
        }   
        else {
        	((ButtonTabComponent)this.getTabComponentAt(indexMoving)).changeColor(Color.red);
        	int indexElement = this.indexOfChild(e.getX(), e.getY());
        	changeColorTab(indexElement);
              
            }
        }
    public void changeColorTab(int indexElement){

        if (indexElement != -1 && indexMoving != indexElement ){
          //  System.out.println("-"+indexElement+"-"+lastIndex+"-");  
            if(lastIndex != indexElement){
            	//if(lastIndex!=-1)
                  //  ((org.cnrs.lam.dis.etc.ihm.tools.ButtonTabComponent)this.getTabComponentAt(lastIndex)).changeColor(systemColor);
            	lastIndexResetColor();
            	((ButtonTabComponent)this.getTabComponentAt(indexElement)).changeColor(Color.green);
            	lastIndex = indexElement;
            }
        }
    }
    @Override
	public void mouseMoved(MouseEvent e) {
        if (this.getCursor().equals(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR))){
            int index = indexOfChild( e.getX(), e.getY()) ;
            if (index != -1){
                this.getTabComponentAt(index).requestFocus();
            }
            else {
                for (int i=0; i<getTabCount(); i++){
                    getTabComponentAt(i).setBackground(Color.gray);
                }
            }
        }
    }
	


}
