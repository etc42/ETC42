/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.ui;

import java.io.File;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;

/**
 *
 * @author Nikolaos Apostolakos
 */
public interface DatasetListener {

    /**
     * This method is called when the listener gets notified that the user
     * wants to preview a dataset.
     * @param type The type of the dataset to preview
     * @param datasetInfo The namespace and name of the dataset
     * @param option The option if we have a multi-dataset or null otherwise
     */
    public void previewDataset(Dataset.Type type, DatasetInfo datasetInfo, String option);

    /**
     * This method is called when the listener gets notified that the user
     * wants to import a dataset from a file. This dataset should replace
     * the related dataset of the related component of the current session.
     * If the namespace is not set, then the dataset is imported for general
     * use and there is no modifications of the current session.
     * @param file The file containing the data
     * @param type The type of the dataset
     * @param info The namespace, name and description of the dataset
     * @param dataType The type of the data
     */
    public void importFromFile(File file, Dataset.Type type, DatasetInfo info, Dataset.DataType dataType);

    /**
     * This method is called when the listener gets notified that the user
     * wants to delete a dataset from the persistence device.
     * @param type The type of the dataset
     * @param datasetInfo The namespace and name of the dataset
     */
    public void deleteDataset(Dataset.Type type, DatasetInfo datasetInfo);

    /**
     * This method is called when the listener gets notified that the user
     * wants to export a dataset to a file. The dataset will be saved in an asci
     * file as tabulated values.
     * @param file The file to save the data
     * @param type The type of the dataset
     * @param info The namespace, name and description of the dataset
     */
    public void exportToFile(File file, Dataset.Type type, DatasetInfo info);
    
    /**
     * This method is called when the listener gets notified that the user wants
     * to copy a dataset to a different namespace.
     * @param type The type of the dataset to copy
     * @param datasetInfo The namespace and name of the dataset
     * @param newInfo The new namespace and name to copy the dataset to
     */
    public void copyToNamespace(Dataset.Type type, DatasetInfo datasetInfo, DatasetInfo newInfo);
}
