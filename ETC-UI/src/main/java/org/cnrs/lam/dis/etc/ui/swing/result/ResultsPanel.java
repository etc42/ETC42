/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.ui.swing.result;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Font;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TreeMap;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import org.cnrs.lam.dis.etc.configuration.ConfigFactory;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Level;
import org.cnrs.lam.dis.etc.datamodel.CalculationResults.Result;
import org.cnrs.lam.dis.etc.ui.swing.EtcFrame;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class ResultsPanel extends JPanel {

    private static ResourceBundle bundle = ResourceBundle.getBundle("org/cnrs/lam/dis/etc/ui/swing/messages");
    private String name;
    private Level level;
    private EtcFrame etcFrame;
    private JPanel container;

    public ResultsPanel(String name, CalculationResults results, EtcFrame etcFrame, Level level) {
        this.name = name;
        this.etcFrame = etcFrame;
        this.level = level;
        
        container = new JPanel();
        container.setBorder(new EmptyBorder(10, 10, 10, 10));
        container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));
        this.setLayout(new BorderLayout());
        this.add(container, BorderLayout.CENTER);

        addResultPanels(results);
        container.add(Box.createVerticalGlue());
    }

    private void addResultPanels(CalculationResults results) {
        Map<String, Result> finalResults = results.getResults(Level.FINAL);
        if (!finalResults.isEmpty()) {
            JLabel title = new JLabel(bundle.getString("RESULTS_FINAL"));
            Font font = new Font(getFont().getName(), Font.BOLD, getFont().getSize() + 2);
            title.setFont(font);
            title.setAlignmentX(Component.LEFT_ALIGNMENT);
            container.add(title);
            for (Result result : orderResults(finalResults, Level.FINAL).values()) {
                container.add(Box.createVerticalStrut(5));
                addResult(result);
            }
        }
        if (level == Level.FINAL)
            return;
        Map<String, Result> intermediateImportantResults = results.getResults(Level.INTERMEDIATE_IMPORTANT);
        if (!intermediateImportantResults.isEmpty()) {
            container.add(Box.createVerticalStrut(20));
            JLabel title = new JLabel(bundle.getString("RESULTS_INTERMEDIATE_IMPORTANT"));
            Font font = new Font(getFont().getName(), Font.BOLD, getFont().getSize() + 2);
            title.setFont(font);
            title.setAlignmentX(Component.LEFT_ALIGNMENT);
            container.add(title);
            for (Result result : orderResults(intermediateImportantResults, Level.INTERMEDIATE_IMPORTANT).values()) {
                container.add(Box.createVerticalStrut(5));
                addResult(result);
            }
        }
        if (level == Level.INTERMEDIATE_IMPORTANT)
            return;
        Map<String, Result> intermediateUnmportantResults = results.getResults(Level.INTERMEDIATE_UNIMPORTANT);
        if (!intermediateUnmportantResults.isEmpty()) {
            container.add(Box.createVerticalStrut(20));
            JLabel title = new JLabel(bundle.getString("RESULTS_INTERMEDIATE_UNIMPORTANT"));
            Font font = new Font(getFont().getName(), Font.BOLD, getFont().getSize() + 2);
            title.setFont(font);
            title.setAlignmentX(Component.LEFT_ALIGNMENT);
            container.add(title);
            for (Result result : orderResults(intermediateUnmportantResults, Level.INTERMEDIATE_UNIMPORTANT).values()) {
                container.add(Box.createVerticalStrut(5));
                addResult(result);
            }
        }
        if (level == Level.INTERMEDIATE_UNIMPORTANT)
            return;
        Map<String, Result> debugResults = results.getResults(Level.DEBUG);
        if (!debugResults.isEmpty()) {
            container.add(Box.createVerticalStrut(20));
            JLabel title = new JLabel(bundle.getString("RESULTS_DEBUG"));
            Font font = new Font(getFont().getName(), Font.BOLD, getFont().getSize() + 2);
            title.setFont(font);
            title.setAlignmentX(Component.LEFT_ALIGNMENT);
            container.add(title);
            for (Result result : orderResults(debugResults, Level.DEBUG).values()) {
                container.add(Box.createVerticalStrut(5));
                addResult(result);
            }
        }
    }

    private void addResult(Result result) {
        if (result instanceof CalculationResults.DoubleValueResult) {
            CalculationResults.DoubleValueResult doubleValueResult = (CalculationResults.DoubleValueResult) result;
            ValuePanel panel = new ValuePanel();
            panel.setFont(getFont());
            panel.setResultName(doubleValueResult.getName());
            panel.setResultValue(doubleValueResult.getValue());
            
            if(doubleValueResult.getName().equals("SIGNAL_TO_NOISE")){
               panel.setOverallFont(new Font("Dialog", Font.BOLD, 18));
            }
            
            if(doubleValueResult.getUnit() != null && doubleValueResult.getUnit().equals("e⁻")){
                panel.setResultValue((int)Math.round(doubleValueResult.getValue()));
            }
            
            panel.setResultUnit(doubleValueResult.getUnit());
            panel.setAlignmentX(Component.LEFT_ALIGNMENT);
            panel.setMaximumSize(panel.getPreferredSize());
            container.add(panel);
        }
        if (result instanceof CalculationResults.LongValueResult) {
            CalculationResults.LongValueResult longValueResult = (CalculationResults.LongValueResult) result;
            ValuePanel panel = new ValuePanel();
            panel.setResultName(longValueResult.getName());
            panel.setResultValue(longValueResult.getValue());
            panel.setResultUnit(longValueResult.getUnit());
            panel.setFont(getFont());
            panel.setAlignmentX(Component.LEFT_ALIGNMENT);
            panel.setMaximumSize(panel.getPreferredSize());
            container.add(panel);
        }
        if (result instanceof CalculationResults.DoubleDatasetResult) {
            CalculationResults.DoubleDatasetResult doubleDatasetResult = (CalculationResults.DoubleDatasetResult) result;
            DatasetPanel<Double, Double> panel = new DatasetPanel<Double, Double>(name, doubleDatasetResult.getName()
                    , doubleDatasetResult.getValues(), doubleDatasetResult.getXUnit(), doubleDatasetResult.getYUnit(), etcFrame);
            panel.setFont(getFont());
            panel.setAlignmentX(Component.LEFT_ALIGNMENT);
            panel.setMaximumSize(panel.getPreferredSize());
            container.add(panel);
        }
        if (result instanceof CalculationResults.LongDatasetResult) {
            CalculationResults.LongDatasetResult longDatasetResult = (CalculationResults.LongDatasetResult) result;
            DatasetPanel<Long, Long> panel = new DatasetPanel<Long, Long>(name, longDatasetResult.getName()
                    , longDatasetResult.getValues(), longDatasetResult.getXUnit(), longDatasetResult.getYUnit(), etcFrame);
            panel.setFont(getFont());
            panel.setAlignmentX(Component.LEFT_ALIGNMENT);
            panel.setMaximumSize(panel.getPreferredSize());
            container.add(panel);
        }
        if (result instanceof CalculationResults.LongDoubleDatasetResult) {
            CalculationResults.LongDoubleDatasetResult longDoubleDatasetResult = (CalculationResults.LongDoubleDatasetResult) result;
            DatasetPanel<Long, Double> panel = new DatasetPanel<Long, Double>(name, longDoubleDatasetResult.getName()
                    , longDoubleDatasetResult.getValues(), longDoubleDatasetResult.getXUnit(), longDoubleDatasetResult.getYUnit(), etcFrame);
            panel.setFont(getFont());
            panel.setAlignmentX(Component.LEFT_ALIGNMENT);
            panel.setMaximumSize(panel.getPreferredSize());
            container.add(panel);
        }
        if (result instanceof CalculationResults.StringResult) {
            CalculationResults.StringResult stringResult = (CalculationResults.StringResult) result;
            StringPanel panel = new StringPanel();
            panel.setResultName(stringResult.getName());
            panel.setResultValue(stringResult.getValue());
            panel.setFont(getFont());
            panel.setAlignmentX(Component.LEFT_ALIGNMENT);
            panel.setMaximumSize(panel.getPreferredSize());
            container.add(panel);
        }
        if (result instanceof CalculationResults.ImageResult) {
            CalculationResults.ImageResult imageResult = (CalculationResults.ImageResult) result;
            ImagePanel panel = new ImagePanel();
            panel.setEtcFrame(etcFrame);
            panel.setResultName(imageResult.getName());
            panel.setParrentName(name);
            panel.setImage(imageResult.getImage());
            panel.setFont(getFont());
            panel.setAlignmentX(Component.LEFT_ALIGNMENT);
            panel.setMaximumSize(panel.getPreferredSize());
            container.add(panel);
        }
        if (result instanceof CalculationResults.ImageSetResult) {
            CalculationResults.ImageSetResult imageSetResult = (CalculationResults.ImageSetResult) result;
            ImageSetPanel panel = new ImageSetPanel();
            panel.setEtcFrame(etcFrame);
            panel.setKeyUnit(imageSetResult.getKeyUnit());
            panel.setResultName(imageSetResult.getName());
            panel.setParrentName(name);
            panel.setImageMap(imageSetResult.getImageMap());
            panel.setFont(getFont());
            panel.setAlignmentX(Component.LEFT_ALIGNMENT);
            panel.setMaximumSize(panel.getPreferredSize());
            container.add(panel);
        }
        if (result instanceof CalculationResults.DoubleDatasetSetResult) {
            CalculationResults.DoubleDatasetSetResult datasetSetResult = (CalculationResults.DoubleDatasetSetResult) result;
            DatasetSetPanel panel = new DatasetSetPanel(name, datasetSetResult.getName(), datasetSetResult.getKeyUnit()
                    , datasetSetResult.getDatasetMap(), etcFrame);
            panel.setFont(getFont());
            panel.setAlignmentX(Component.LEFT_ALIGNMENT);
            panel.setMaximumSize(panel.getPreferredSize());
            container.add(panel);
        }
    }

    private Map<String, Result> orderResults(Map<String, Result> finalResults, Level level) {
        Map<String, Result> temp = new TreeMap<String, Result>(finalResults);
        Map<String, Result> result = new LinkedHashMap<String, Result>();
        List<String> toBeOrdered = null;
        switch (level) {
            case FINAL:
                toBeOrdered = ConfigFactory.getConfig().getResultOrderFinal();
                break;
            case INTERMEDIATE_IMPORTANT:
                toBeOrdered = ConfigFactory.getConfig().getResultOrderIntermediateImportant();
                break;
            case INTERMEDIATE_UNIMPORTANT:
                toBeOrdered = ConfigFactory.getConfig().getResultOrderIntermediateUnimportant();
                break;
            case DEBUG:
                toBeOrdered = ConfigFactory.getConfig().getResultOrderDebug();
                break;
        }
        for (String orderedName : toBeOrdered) {
            Result value = temp.remove(orderedName.trim());
            if (value != null)
                result.put(orderedName, value);
        }
        result.putAll(temp);
        return result;
    }

}
