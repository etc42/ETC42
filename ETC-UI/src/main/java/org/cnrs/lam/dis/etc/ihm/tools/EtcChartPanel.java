/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.ihm.tools;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileWriter;

import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import org.apache.log4j.Logger;


import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.entity.ChartEntity;
import org.jfree.chart.entity.LegendItemEntity;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeriesCollection;




/**
 * Classe qui remplace ChartPanel
 * Elle doit obligatoirement être une classe fille de EtcTabbedPane
 * Ne fonctionne qu'avec les types XYDataset !
 * @author ludovic
 */
public class EtcChartPanel extends ChartPanel {

    private static Logger logger = Logger.getLogger(EtcChartPanel.class);

    /**
     * Element selectionné
     */
    private ChartEntity selected;
    private boolean onMove = false;
    /**
     * Menu permettant la selection des couleurs.
     */
    private JMenu color = null;
    
    private JMenuItem saveToFile = null;
  
    

    /**
     * Constructeur
     * @param jf
     */
    public EtcChartPanel(JFreeChart jf) throws IncorrectParentException{

        super(jf);

        super.setPopupMenu(getPopupMenu());

        if (this.getParent() instanceof EtcTabbedPane){
            throw new IncorrectParentException("The parent must be an EtcTabbedPane Instance");
           
        }

    }

    /**
     * 
     * @param title : libellé de la couleur.
     * @param color : Couleur à appliquer.
     * @return Menuitem de la couleur
     */
    private JMenuItem createColorMenuItem(String title, final Color color){
        
        JMenuItem mIColor = new JMenuItem(title);

        mIColor.addActionListener(new ActionListener(){

            @Override
			public void actionPerformed(ActionEvent e) {
                
            	if (selected != null)
            	{
            	//((LegendItemEntity) selected).getDataset().toString();
            	
            	getChart().getXYPlot().
                getRendererForDataset(((XYDataset)((LegendItemEntity) selected).
                		getDataset())).
                setSeriesPaint(((XYDataset)((LegendItemEntity) selected).
                		getDataset()).
                		indexOf(((LegendItemEntity) selected).getSeriesKey()), color);
            	}
            	else {System.out.println("null");}

            }

        });

        return mIColor;
    }

    
    /**
     * Nouvelle version du popup menu qui ajoute la gestion des couleurs ..
     * @return
     */
    @Override
    public JPopupMenu getPopupMenu(){

        JPopupMenu jp = super.getPopupMenu();
        
        // Mise en place du menu de changement de couleurs
        color = new JMenu("Color");
        color.add(createColorMenuItem("Black", Color.black));
        color.add(createColorMenuItem("Red", Color.red));
        color.add(createColorMenuItem("Blue", Color.blue));
        color.add(createColorMenuItem("Cyan", Color.CYAN));
        color.add(createColorMenuItem("Pink", Color.PINK));
        color.add(createColorMenuItem("Yellow", Color.YELLOW));
        color.add(createColorMenuItem("Green", Color.GREEN));
        color.add(createColorMenuItem("Orange", Color.ORANGE));
        color.add(createColorMenuItem("Magenta", Color.MAGENTA));
        color.add(createColorMenuItem("White", Color.WHITE));
        
        saveToFile = new JMenuItem("Save ...");
        saveToFile.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				Object source = e.getSource();
				try {
					getXYfromPlot(((JMenuItem)source).getText().substring(5, ((JMenuItem)source).getText().length()));
				} catch (Exception e1) {
						logger.error(null, e1);
				}
			}
		});
        
        
        
        jp.add(color);
        jp.add(saveToFile);
        jp.addSeparator();
        jp.setSize(jp.getWidth()+10, jp.getHeight()+10);
        return jp;
    }
    
    
    /**
     * Cette fonction effectue la sauvegarde d'une courbe en fichier ascii
     * @param typeOfData est le nom de la courbe a sauvegarder
     * @throws Exception
     */
    public void getXYfromPlot(String typeOfData) throws Exception{
    	
    	String coef;
    	double coefY = 1.0;
    	// On identifie le Dataset la serie de donnees
    	int indexDataset = super.getChart().getXYPlot().indexOf((XYDataset)((LegendItemEntity) selected).getDataset());	
    	int indexSeries = super.getChart().getXYPlot().getDataset(indexDataset).indexOf(((LegendItemEntity) selected).getSeriesKey());
    	ETCXYSeries serie = (ETCXYSeries)((XYSeriesCollection)super.getChart().getXYPlot().getDataset(indexDataset)).getSeries(indexSeries);
    	
    	
    	
    	//On charge les valeurs de la serie de donnees
    	double[] listeX = new double[super.getChart().getXYPlot().getDataset(indexDataset).getItemCount(indexSeries)];
    	double[] listeY = new double[super.getChart().getXYPlot().getDataset(indexDataset).getItemCount(indexSeries)];
    	for (int i = 0 ; i < super.getChart().getXYPlot().getDataset(indexDataset).getItemCount(indexSeries) ; i++)
    	{
    		listeX[i]= (super.getChart().getXYPlot().getDataset(indexDataset).getXValue(indexSeries, i));
    		listeY[i]= (super.getChart().getXYPlot().getDataset(indexDataset).getYValue(indexSeries, i));
    		
    	}
    	
    	// On applique le coef mulitplicateur de Y
    	
    	coefY = serie.getCoefY();
    	for (int c = 0 ; c < listeY.length ; c++)
		 {
			listeY[c] = listeY[c]*coefY;
		 }
    		
    	    	
    	// On demande le nom et path du fichier via une popup
    	JFileChooser fc  = new JFileChooser();
    	File fileToSave = new File(typeOfData + ".dat");
    	FileWriter writer = null;
    	fc.setDialogTitle("Export data");
    	fc.setSelectedFile(fileToSave);
		 
    	// Apres validation de la popup
		 int returnVal = fc.showSaveDialog(this);
		 if (returnVal == JFileChooser.APPROVE_OPTION)
		 {
			 fileToSave = fc.getSelectedFile();
			 writer = new FileWriter(fileToSave, true);
			 // on ecrit une ligne pour chaque couple X et Y
			 for (int j = 0 ; j < listeX.length ; j++)
			 {
				 writer.write(listeX[j] + " " + listeY[j] + "\n");
			 }
			 writer.close();	 
		 } 
    }
    
    @Override
	public void mousePressed(MouseEvent e) {

       
        Object actuel = super.getEntityForPoint(e.getX(), e.getY());
       
        //-- On souhaite afficher le menu surcharge
        if (e.getButton() == MouseEvent.BUTTON3 && actuel instanceof LegendItemEntity ){
        	
        	// Concernant le changement de couleurs ...
        	selected = (ChartEntity) actuel;
        	this.setVisible(true);
            color.setVisible(true);
            
            // Concernant le SAVE
            saveToFile.setText("Save " + ((LegendItemEntity)actuel).getSeriesKey().toString());
            saveToFile.setVisible(true);
            
        }
        else
        {
        	 saveToFile.setVisible(false);
        }
                        
        super.mousePressed(e);
       
    }
    

    @Override
	public void mouseDragged(MouseEvent e){

        super.mouseDragged(e);
       
     
       if (super.getEntityForPoint(e.getX(), e.getY()) instanceof LegendItemEntity && !onMove){
          onMove = true;
          selected = super.getEntityForPoint(e.getX(), e.getY());
         
          this.getParent().setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
       }
        if (!(getParent() instanceof EtcTabbedPane)) {
            return;
        }
       int indexElement = ((EtcTabbedPane)this.getParent()).indexOfChild(e.getX()+30, e.getY()+30);
       ((EtcTabbedPane)this.getParent()).changeColorTab(indexElement);
       
    }

    @Override
	public void mouseReleased(MouseEvent e){
    	
        super.mouseReleased(e);
        
        if (!(getParent() instanceof EtcTabbedPane)) {
            return;
        }
        ((EtcTabbedPane)getParent()).lastIndexResetColor();
        if (onMove){

            getParent().setCursor(Cursor.getDefaultCursor());


            onMove=false;

            //-- On revoit la position de la souris --
            if (e.getY() <= 0){
        
                int index = ((EtcTabbedPane)this.getParent()).indexOfChild((this.getParent().getBounds().x + e.getX()), ( this.getBounds().y  - Math.abs(e.getY()) ) );
                try {
                	//TODO guillaume
                	// récupérer l'index du dataset du ChartPanel Source, ??
                	//et si == 0 et en même temps celui d'indice 1 existe, on annule (car c'est le premier de la liste)
                	
                	//((EtcTabbedPane) getParent()).getSelectedIndex().
                	//
                	if(index != ((EtcTabbedPane) getParent()).getSelectedIndex())
                		((EtcTabbedPane) super.getParent()).moveCurveTo(index, ((EtcTabbedPane) getParent()).getSelectedIndex(), (LegendItemEntity) selected);
                } catch (IncorrectParentException ex) {
                   System.exit(1);
                }

           }


        }
        color.setVisible(false);
        
    }

}
