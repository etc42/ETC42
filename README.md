ETC-42
===========================

Introduction
------------
The ETC-42 is a generic exposure time calculator.
It has initialy been written at LAM (Laboratoire d'astrophysique de Marseille).

Dependencies
------------
- Maven
- dvipng
- Latex full
- JRE 1.7+
- JDK 1.7+ (Open JDK isn't supported)

How to build
------------
To build, you need you use *Maven* as following in project root folder:
```
mvn package
```

Modules Description
-------------------
ETC-DataModel:
- This module defines the data structures which the different ETC modules use to communicate.

ETC-Persistence:
- This module provides the persistency level of the ETC application. The current implementation is using an embedded database to persist the ETC data.

ETC-Calculator:
- This module is responsible for performing SNR calculations.

ETC-UI:
- This module is resposible for the communication with the user (User Interface).
There are currently two implementations, a graphical interface build with Swing and a terminal mode interface.

ETC-DataImportExport:
- This module provides functionality for importing and exporting data from files or VO enabled applications.

ETC-Controller:
- This module works as a conector of all the above projects and implements the possible schenarios of usage of the program.

ETC-Jar:
- This module creates a single executable jar containing all the necessary files to be used for easy ETC distribution.

ETC-WebStart:
- This module creates a Web Start version of the ETC.

ETC-Documentation:
- This module creates the latex documentation of the ETC.

Where to get generated outputs ?
-----------------------------
The files to use for distribution (output of the build) are:
```
  - ETC-Jar/target/ETC-Jar-<version>.jar
    A stand alone executable jar
```
```
  - ETC-WebStart/target/ETC-WebStart-<version>.zip
    A zip file containing the web start version of ETC
```
```
  - ETC-Documentation/target
    In this directory are the pdf versions of the Latex documentation
```

Build the javadoc
-----------------
The ETC javadoc uses Latexlet to create scientific equations. For this reason
the following requirements must be met before building it:
  - dvipng installed and in the path
To create the javadoc execute the following command in the ETC directory:
```
mvn javadoc:javadoc
```
This will build the javadoc of each project separately and will put the files
in the directories <project>/target/site/apidocs

Common Problems and Solutions
-----------------------------
Here are listed some common problems during the ETC compilation and how to
solve them.

"invalid target release: 1.x" error:
If when building the ETC this error appears, most probably the JAVA_HOME library
is not set correctly. This is a common problem on Mac OS. The solution is to
set the JAVA_HOME variable to the correct directory. The java home directory
differs from system to system and it depends on the configuration, but the
common place for it (for Mac OS) is:
/Library/Java/Home 