/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.persistence.database.entities;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.cnrs.lam.dis.etc.datamodel.ComponentInfo;
import org.cnrs.lam.dis.etc.datamodel.Source;
import org.cnrs.lam.dis.etc.persistence.database.entities.exceptions.NonexistentEntityException;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class SourceEntityJpaController {

    public void create(SourceEntity sourceEntity) {
        EntityManager em = null;
        try {
            em = TransactionController.getEntityManager();
            TransactionController.beginTransaction();
            em.persist(sourceEntity);
            TransactionController.commitTransaction();
            sourceEntity.setModified(false);
        } catch (Exception ex) {
            TransactionController.rollbackTransaction();
        }
    }

    public void edit(SourceEntity sourceEntity) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = TransactionController.getEntityManager();
            TransactionController.beginTransaction();
            sourceEntity = em.merge(sourceEntity);
            TransactionController.commitTransaction();
            sourceEntity.setModified(false);
        } catch (Exception ex) {
            TransactionController.rollbackTransaction();
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = sourceEntity.getId();
                if (findSourceEntity(id) == null) {
                    throw new NonexistentEntityException("The sourceEntity with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = TransactionController.getEntityManager();
            TransactionController.beginTransaction();
            SourceEntity sourceEntity;
            try {
                sourceEntity = em.getReference(SourceEntity.class, id);
                sourceEntity.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The sourceEntity with id " + id + " no longer exists.", enfe);
            }
            em.remove(sourceEntity);
            TransactionController.commitTransaction();
            sourceEntity.setModified(true);
        } catch (Exception ex) {
            TransactionController.rollbackTransaction();
        }
    }

    public List<SourceEntity> findSourceEntityEntities() {
        return findSourceEntityEntities(true, -1, -1);
    }

    public List<SourceEntity> findSourceEntityEntities(int maxResults, int firstResult) {
        return findSourceEntityEntities(false, maxResults, firstResult);
    }

    private List<SourceEntity> findSourceEntityEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = TransactionController.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<SourceEntity> cq = cb.createQuery(SourceEntity.class);
        Root<SourceEntity> from = cq.from(SourceEntity.class);
        cq.select(cq.from(SourceEntity.class));
        cq.orderBy(cb.asc(from.get(SourceEntity_.name)));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public List<SourceEntity> findSourcesUsingDataset(DatasetInfoEntity datasetInfoEntity) {
        EntityManager em = TransactionController.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<SourceEntity> cq = cb.createQuery(SourceEntity.class);
        Root<SourceEntity> from = cq.from(SourceEntity.class);
        cq.where(cb.equal(from.get(SourceEntity_.spectralDistributionTemplateEntity), datasetInfoEntity));
        TypedQuery<SourceEntity> query = em.createQuery(cq);
        return query.getResultList();
    }

    public SourceEntity findSourceEntity(ComponentInfo info) {
        EntityManager em = TransactionController.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<SourceEntity> cq = cb.createQuery(SourceEntity.class);
        Root<SourceEntity> from = cq.from(SourceEntity.class);
        cq.where(cb.equal(from.get(SourceEntity_.name), info.getName()));
        TypedQuery<SourceEntity> query = em.createQuery(cq);
        for (SourceEntity result : query.getResultList()) {
            result.setModified(false);
            return result;
        }
        return null;
    }

    public SourceEntity findSourceEntity(Long id) {
        EntityManager em = TransactionController.getEntityManager();
        return em.find(SourceEntity.class, id);
    }

    public int getSourceEntityCount() {
        EntityManager em = TransactionController.getEntityManager();
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<SourceEntity> rt = cq.from(SourceEntity.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public Source createNewUnpersistedSourceEntity(ComponentInfo newSourceInfo) {
        SourceEntity source = new SourceEntity();
        source.setName(newSourceInfo.getName());
        source.setDescription(newSourceInfo.getDescription());
        source.setExtendedSourceProfile(Source.SurfaceBrightnessProfile.UNIFORM);
        source.setSpatialDistributionType(Source.SpatialDistributionType.POINT_SOURCE);
        source.setSpectralDistributionType(Source.SpectralDistributionType.FLAT);
        source.setEmissionLineWavelengthUnit("\u00C5");
        source.setEmissionLineFwhmUnit("\u00C5");
        source.setExtendedSourceRadiusUnit("arcsec");
        source.setTemperatureUnit("K");
        source.setMagnitudeWavelengthUnit("\u00C5");
        source.setEmissionLineFluxUnit("erg/cm^2/s");
        source.setExtendedMagnitudeType(Source.ExtendedMagnitudeType.PEAK_VALUE);
        source.setModified(false);
        return source;
    }

}
