/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.persistence.database.entities;

import javax.persistence.metamodel.SingularAttribute;
import org.cnrs.lam.dis.etc.datamodel.ObsParam;
import org.cnrs.lam.dis.etc.datamodel.ObsParam.ExtraBackgroundNoiseType;
import org.cnrs.lam.dis.etc.datamodel.ObsParam.ExtraSignalType;
import org.cnrs.lam.dis.etc.datamodel.ObsParam.FixedParameter;
import org.cnrs.lam.dis.etc.datamodel.ObsParam.SpectralQuantumType;
import org.cnrs.lam.dis.etc.datamodel.ObsParam.TimeSampleType;

@javax.persistence.metamodel.StaticMetamodel
(value=org.cnrs.lam.dis.etc.persistence.database.entities.ObsParamEntity.class)
@javax.annotation.Generated
(value="org.apache.openjpa.persistence.meta.AnnotationProcessor6",date="Mon Jan 31 14:16:55 CET 2011")
public class ObsParamEntity_ {
    public static volatile SingularAttribute<ObsParamEntity,String> description;
    public static volatile SingularAttribute<ObsParamEntity,Double> dit;
    public static volatile SingularAttribute<ObsParamEntity,String> ditUnit;
    public static volatile SingularAttribute<ObsParamEntity,Double> exposureTime;
    public static volatile SingularAttribute<ObsParamEntity,String> exposureTimeUnit;
    public static volatile SingularAttribute<ObsParamEntity,FixedParameter> fixedParameter;
    public static volatile SingularAttribute<ObsParamEntity,Long> id;
    public static volatile SingularAttribute<ObsParamEntity,String> name;
    public static volatile SingularAttribute<ObsParamEntity,Integer> noExpo;
    public static volatile SingularAttribute<ObsParamEntity,Double> snr;
    public static volatile SingularAttribute<ObsParamEntity,Double> snrLambda;
    public static volatile SingularAttribute<ObsParamEntity,String> snrLambdaUnit;
    public static volatile SingularAttribute<ObsParamEntity,String> snrUnit;
    public static volatile SingularAttribute<ObsParamEntity,TimeSampleType> timeSampleType;
    public static volatile SingularAttribute<ObsParamEntity,SpectralQuantumType> spectralQuantumType;
    public static volatile SingularAttribute<ObsParamEntity,ExtraBackgroundNoiseType> extraBackgroundNoiseType;
    public static volatile SingularAttribute<ObsParamEntity,ExtraSignalType> extraSignalType;
    public static volatile SingularAttribute<ObsParamEntity,DatasetInfoEntity> extraBackgroundNoiseDatasetEntity;
    public static volatile SingularAttribute<ObsParamEntity,DatasetInfoEntity> extraSignalDatasetEntity;
    public static volatile SingularAttribute<ObsParamEntity,Double> extraBackgroundNoise;
    public static volatile SingularAttribute<ObsParamEntity,String> extraBackgroundNoiseUnit;
    public static volatile SingularAttribute<ObsParamEntity,Double> extraSignal;
    public static volatile SingularAttribute<ObsParamEntity,String> extraSignalUnit;
    public static volatile SingularAttribute<ObsParamEntity,ObsParam.FixedSnrType> fixedSnrType;
}
