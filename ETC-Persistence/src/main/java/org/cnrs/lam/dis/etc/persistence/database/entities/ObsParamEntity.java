/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.persistence.database.entities;

import java.io.Serializable;
import javax.persistence.*;
import org.cnrs.lam.dis.etc.datamodel.ComponentInfo;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.cnrs.lam.dis.etc.datamodel.ObsParam;
import org.cnrs.lam.dis.etc.datamodel.ObsParam.FixedParameter;
import org.cnrs.lam.dis.etc.datamodel.ObsParam.TimeSampleType;

/**
 *
 * @author Nikolaos Apostolakos
 */
@Entity
@Table(name = "OBS_PARAM")
@NamedQueries({
    @NamedQuery(name = "ObsParamEntity.findAll", query = "SELECT o FROM ObsParamEntity o")})
public class ObsParamEntity implements Serializable, ObsParam {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Basic(optional = false)
    @Column(name = "NAME")
    private String name;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "DIT")
    private double dit;
    @Column(name = "DIT_UNIT")
    private String ditUnit;
    @Column(name = "EXPOSURE_TIME")
    private double exposureTime;
    @Column(name = "EXPOSURE_TIME_UNIT")
    private String exposureTimeUnit;
    @Basic(optional = false)
    @Column(name = "FIXED_PARAMETER")
    @Enumerated(EnumType.STRING)
    private FixedParameter fixedParameter;
    @Column(name = "NO_EXPO")
    private int noExpo;
    @Column(name = "SNR")
    private double snr;
    @Column(name = "SNR_UNIT")
    private String snrUnit;
    @Column(name = "SNR_LAMBDA")
    private double snrLambda;
    @Column(name = "SNR_LAMBDA_UNIT")
    private String snrLambdaUnit;
    @Basic(optional = false)
    @Column(name = "TIME_SAMPLE_TYPE")
    @Enumerated(EnumType.STRING)
    private TimeSampleType timeSampleType;
    @Basic(optional = false)
    @Column(name = "SPECTRAL_QUANTUM_TYPE")
    @Enumerated(EnumType.STRING)
    private SpectralQuantumType spectralQuantumType;
    @Basic(optional = false)
    @Column(name = "EXTRA_BACKGROUND_NOISE_TYPE")
    @Enumerated(EnumType.STRING)
    private ExtraBackgroundNoiseType extraBackgroundNoiseType;
    @Basic(optional = false)
    @Column(name = "EXTRA_SIGNAL_TYPE")
    @Enumerated(EnumType.STRING)
    private ExtraSignalType extraSignalType;
    @JoinColumn(name = "EXTRA_BACKGROUND_NOISE_DATASET", referencedColumnName = "ID")
    @ManyToOne
    private DatasetInfoEntity extraBackgroundNoiseDatasetEntity;
    @JoinColumn(name = "EXTRA_SIGNAL_DATASET", referencedColumnName = "ID")
    @ManyToOne
    private DatasetInfoEntity extraSignalDatasetEntity;
    @Column(name = "EXTRA_BACKGROUND_NOISE")
    private double extraBackgroundNoise;
    @Column(name = "EXTRA_BACKGROUND_NOISE_UNIT")
    private String extraBackgroundNoiseUnit;
    @Column(name = "EXTRA_SIGNAL")
    private double extraSignal;
    @Column(name = "EXTRA_SIGNAL_UNIT")
    private String extraSignalUnit;
    @Basic(optional = false)
    @Column(name = "FIXED_SNR_TYPE")
    @Enumerated(EnumType.STRING)
    private FixedSnrType fixedSnrType;

    private transient boolean modified = false;

    public boolean isModified() {
        return modified;
    }

    void setModified(boolean modified) {
        this.modified = modified;
    }

    public ObsParamEntity() {
    }

    public ObsParamEntity(Long id) {
        this.id = id;
    }

    public ObsParamEntity(Long id, String name, FixedParameter fixedParameter, TimeSampleType timeSampleType) {
        this.id = id;
        this.name = name;
        this.fixedParameter = fixedParameter;
        this.timeSampleType = timeSampleType;
    }

    public ObsParamEntity(String name, FixedParameter fixedParameter, TimeSampleType timeSampleType) {
        this.name = name;
        this.fixedParameter = fixedParameter;
        this.timeSampleType = timeSampleType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public double getDit() {
        return dit;
    }

    @Override
    public void setDit(double dit) {
        if (this.dit != dit)
            modified = true;
        this.dit = dit;
    }

    @Override
    public String getDitUnit() {
        return ditUnit;
    }

    public void setDitUnit(String ditUnit) {
        if (this.ditUnit != null && !this.ditUnit.equals(ditUnit))
            modified = true;
        this.ditUnit = ditUnit;
    }

    @Override
    public double getExposureTime() {
        return exposureTime;
    }

    @Override
    public void setExposureTime(double exposureTime) {
        if (this.exposureTime != exposureTime)
            modified = true;
        this.exposureTime = exposureTime;
    }

    @Override
    public String getExposureTimeUnit() {
        return exposureTimeUnit;
    }

    public void setExposureTimeUnit(String exposureTimeUnit) {
        if (this.exposureTimeUnit != null && !this.exposureTimeUnit.equals(exposureTimeUnit))
            modified = true;
        this.exposureTimeUnit = exposureTimeUnit;
    }

    @Override
    public int getNoExpo() {
        return noExpo;
    }

    @Override
    public void setNoExpo(int noExpo) {
        if (this.noExpo != noExpo)
            modified = true;
        this.noExpo = noExpo;
    }

    @Override
    public double getSnr() {
        return snr;
    }

    @Override
    public void setSnr(double snr) {
        if (this.snr != snr)
            modified = true;
        this.snr = snr;
    }

    @Override
    public String getSnrUnit() {
        return snrUnit;
    }

    public void setSnrUnit(String snrUnit) {
        if (this.snrUnit != null && !this.snrUnit.equals(snrUnit))
            modified = true;
        this.snrUnit = snrUnit;
    }

    @Override
    public double getSnrLambda() {
        return snrLambda;
    }

    @Override
    public void setSnrLambda(double snrLambda) {
        if (this.snrLambda != snrLambda)
            modified = true;
        this.snrLambda = snrLambda;
    }

    @Override
    public String getSnrLambdaUnit() {
        return snrLambdaUnit;
    }

    public void setSnrLambdaUnit(String snrLambdaUnit) {
        if (this.snrLambdaUnit != null && !this.snrLambdaUnit.equals(snrLambdaUnit))
            modified = true;
        this.snrLambdaUnit = snrLambdaUnit;
    }

    @Override
    public FixedParameter getFixedParameter() {
        return fixedParameter;
    }

    @Override
    public void setFixedParameter(FixedParameter fixedParameter) {
        if (this.fixedParameter != fixedParameter)
            modified = true;
        this.fixedParameter = fixedParameter;
    }

    @Override
    public TimeSampleType getTimeSampleType() {
        return timeSampleType;
    }

    @Override
    public void setTimeSampleType(TimeSampleType timeSampleType) {
        if (this.timeSampleType != timeSampleType)
            modified = true;
        this.timeSampleType = timeSampleType;
    }
    
    @Override
    public SpectralQuantumType getSpectralQuantumType() {
        return spectralQuantumType;
    }
    
    @Override
    public void setSpectralQuantumType(SpectralQuantumType spectralQuantumType) {
        if (this.spectralQuantumType != spectralQuantumType)
            modified = true;
        this.spectralQuantumType = spectralQuantumType;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ObsParamEntity)) {
            return false;
        }
        ObsParamEntity other = (ObsParamEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.cnrs.lam.dis.etc.persistence.database.entities.ObsParamEntity[id=" + id + "]";
    }

    @Override
    public ComponentInfo getInfo() {
        return new ComponentInfo(name, description);
    }

    @Override
    public ExtraBackgroundNoiseType getExtraBackgroundNoiseType() {
        return extraBackgroundNoiseType;
    }

    @Override
    public void setExtraBackgroundNoiseType(ExtraBackgroundNoiseType type) {
        if (this.extraBackgroundNoiseType != type)
            modified = true;
        this.extraBackgroundNoiseType = type;
    }

    @Override
    public ExtraSignalType getExtraSignalType() {
        return extraSignalType;
    }

    @Override
    public void setExtraSignalType(ExtraSignalType type) {
        if (this.extraSignalType != type)
            modified = true;
        this.extraSignalType = type;
    }

    public DatasetInfoEntity getExtraBackgroundNoiseDatasetEntity() {
        return extraBackgroundNoiseDatasetEntity;
    }

    public void setExtraBackgroundNoiseDatasetEntity(DatasetInfoEntity extraBackgroundNoiseDatasetEntity) {
        this.extraBackgroundNoiseDatasetEntity = extraBackgroundNoiseDatasetEntity;
    }

    public DatasetInfoEntity getExtraSignalDatasetEntity() {
        return extraSignalDatasetEntity;
    }

    public void setExtraSignalDatasetEntity(DatasetInfoEntity extraSignalDatasetEntity) {
        this.extraSignalDatasetEntity = extraSignalDatasetEntity;
    }

    @Override
    public DatasetInfo getExtraBackgroundNoiseDataset() {
        if (getExtraBackgroundNoiseDatasetEntity() == null)
            return null;
        return new DatasetInfo(getExtraBackgroundNoiseDatasetEntity().getName(), getExtraBackgroundNoiseDatasetEntity().getNamespace(), getExtraBackgroundNoiseDatasetEntity().getDescription());
    }

    @Override
    public void setExtraBackgroundNoiseDataset(DatasetInfo extraBackgroundNoise) {
        if ((this.extraBackgroundNoiseDatasetEntity == null && extraBackgroundNoise != null) ||
                (this.extraBackgroundNoiseDatasetEntity != null && extraBackgroundNoise == null) ||
                (this.extraBackgroundNoiseDatasetEntity != null && extraBackgroundNoise != null &&
                    !(this.extraBackgroundNoiseDatasetEntity.getName().equals(extraBackgroundNoise.getName()) &&
                        (this.extraBackgroundNoiseDatasetEntity.getNamespace() == null ?
                            extraBackgroundNoise.getNamespace() == null :
                            this.extraBackgroundNoiseDatasetEntity.getNamespace().equals(extraBackgroundNoise.getNamespace())
                        ))
                ))
            modified = true;
        if (extraBackgroundNoise == null) {
            setExtraBackgroundNoiseDatasetEntity(null);
            return;
        }
        DatasetInfoEntityJpaController controller = new DatasetInfoEntityJpaController();
        setExtraBackgroundNoiseDatasetEntity(controller.findDatasetEntity(Dataset.Type.EXTRA_BACKGROUND_NOISE, extraBackgroundNoise));
    }

    @Override
    public DatasetInfo getExtraSignalDataset() {
        if (getExtraSignalDatasetEntity() == null)
            return null;
        return new DatasetInfo(getExtraSignalDatasetEntity().getName(), getExtraSignalDatasetEntity().getNamespace(), getExtraSignalDatasetEntity().getDescription());
    }

    @Override
    public void setExtraSignalDataset(DatasetInfo extraSignal) {
        if ((this.extraSignalDatasetEntity == null && extraSignal != null) ||
                (this.extraSignalDatasetEntity != null && extraSignal == null) ||
                (this.extraSignalDatasetEntity != null && extraSignal != null &&
                    !(this.extraSignalDatasetEntity.getName().equals(extraSignal.getName()) &&
                        (this.extraSignalDatasetEntity.getNamespace() == null ?
                            extraSignal.getNamespace() == null :
                            this.extraSignalDatasetEntity.getNamespace().equals(extraSignal.getNamespace())
                        ))
                ))
            modified = true;
        if (extraSignal == null) {
            setExtraSignalDatasetEntity(null);
            return;
        }
        DatasetInfoEntityJpaController controller = new DatasetInfoEntityJpaController();
        setExtraSignalDatasetEntity(controller.findDatasetEntity(Dataset.Type.EXTRA_SIGNAL, extraSignal));
    }

    @Override
    public double getExtraBackgroundNoise() {
        return extraBackgroundNoise;
    }

    @Override
    public void setExtraBackgroundNoise(double extraBackgroundNoise) {
        if (this.extraBackgroundNoise != extraBackgroundNoise)
            modified = true;
        this.extraBackgroundNoise = extraBackgroundNoise;
    }

    @Override
    public String getExtraBackgroundNoiseUnit() {
        return extraBackgroundNoiseUnit;
    }

    public void setExtraBackgroundNoiseUnit(String unit) {
        if (this.extraBackgroundNoiseUnit != null && !this.extraBackgroundNoiseUnit.equals(unit))
            modified = true;
        this.extraBackgroundNoiseUnit = unit;
    }

    @Override
    public double getExtraSignal() {
        return extraSignal;
    }

    @Override
    public void setExtraSignal(double extraSignal) {
        if (this.extraSignal != extraSignal)
            modified = true;
        this.extraSignal = extraSignal;
    }

    @Override
    public String getExtraSignalUnit() {
        return extraSignalUnit;
    }

    public void setExtraSignalUnit(String unit) {
        if (this.extraSignalUnit != null && !this.extraSignalUnit.equals(unit))
            modified = true;
        this.extraSignalUnit = unit;
    }

    @Override
    public FixedSnrType getFixedSnrType() {
        return fixedSnrType;
    }

    @Override
    public void setFixedSnrType(FixedSnrType fixedSnrType) {
        if (this.fixedSnrType != fixedSnrType)
            modified = true;
        this.fixedSnrType = fixedSnrType;
    }

}
