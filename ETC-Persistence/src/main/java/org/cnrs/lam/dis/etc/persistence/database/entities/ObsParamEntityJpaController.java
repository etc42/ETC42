/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.persistence.database.entities;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.cnrs.lam.dis.etc.datamodel.ComponentInfo;
import org.cnrs.lam.dis.etc.datamodel.ObsParam;
import org.cnrs.lam.dis.etc.persistence.database.entities.exceptions.NonexistentEntityException;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class ObsParamEntityJpaController {

    public void create(ObsParamEntity obsParamEntity) {
        EntityManager em = null;
        try {
            em = TransactionController.getEntityManager();
            TransactionController.beginTransaction();
            em.persist(obsParamEntity);
            TransactionController.commitTransaction();
            obsParamEntity.setModified(false);
        } catch (Exception ex) {
            TransactionController.rollbackTransaction();
        }
    }

    public void edit(ObsParamEntity obsParamEntity) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = TransactionController.getEntityManager();
            TransactionController.beginTransaction();
            obsParamEntity = em.merge(obsParamEntity);
            TransactionController.commitTransaction();
            obsParamEntity.setModified(false);
        } catch (Exception ex) {
            TransactionController.rollbackTransaction();
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = obsParamEntity.getId();
                if (findObsParamEntity(id) == null) {
                    throw new NonexistentEntityException("The obsParamEntity with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = TransactionController.getEntityManager();
            TransactionController.beginTransaction();
            ObsParamEntity obsParamEntity;
            try {
                obsParamEntity = em.getReference(ObsParamEntity.class, id);
                obsParamEntity.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The obsParamEntity with id " + id + " no longer exists.", enfe);
            }
            em.remove(obsParamEntity);
            TransactionController.commitTransaction();
            obsParamEntity.setModified(true);
        } catch (Exception ex) {
            TransactionController.rollbackTransaction();
        }
    }

    public List<ObsParamEntity> findObsParamEntityEntities() {
        return findObsParamEntityEntities(true, -1, -1);
    }

    public List<ObsParamEntity> findObsParamEntityEntities(int maxResults, int firstResult) {
        return findObsParamEntityEntities(false, maxResults, firstResult);
    }

    private List<ObsParamEntity> findObsParamEntityEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = TransactionController.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<ObsParamEntity> cq = cb.createQuery(ObsParamEntity.class);
        Root<ObsParamEntity> from = cq.from(ObsParamEntity.class);
        cq.select(cq.from(ObsParamEntity.class));
        cq.orderBy(cb.asc(from.get(ObsParamEntity_.name)));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public List<ObsParamEntity> findObsParamsUsingDataset(DatasetInfoEntity datasetInfoEntity) {
        EntityManager em = TransactionController.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<ObsParamEntity> cq = cb.createQuery(ObsParamEntity.class);
        Root<ObsParamEntity> from = cq.from(ObsParamEntity.class);
        cq.where(cb.or(
                cb.equal(from.get(ObsParamEntity_.extraBackgroundNoiseDatasetEntity), datasetInfoEntity),
                cb.equal(from.get(ObsParamEntity_.extraSignalDatasetEntity), datasetInfoEntity)
        ));
        TypedQuery<ObsParamEntity> query = em.createQuery(cq);
        return query.getResultList();
    }

    public ObsParamEntity findObsParamEntity(ComponentInfo info) {
        EntityManager em = TransactionController.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<ObsParamEntity> cq = cb.createQuery(ObsParamEntity.class);
        Root<ObsParamEntity> from = cq.from(ObsParamEntity.class);
        cq.where(cb.equal(from.get(ObsParamEntity_.name), info.getName()));
        TypedQuery<ObsParamEntity> query = em.createQuery(cq);
        for (ObsParamEntity result : query.getResultList()) {
            result.setModified(false);
            return result;
        }
        return null;
    }

    public ObsParamEntity findObsParamEntity(Long id) {
        EntityManager em = TransactionController.getEntityManager();
        return em.find(ObsParamEntity.class, id);
    }

    public int getObsParamEntityCount() {
        EntityManager em = TransactionController.getEntityManager();
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<ObsParamEntity> rt = cq.from(ObsParamEntity.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public ObsParam createNewUnpersistedObsParamEntity(ComponentInfo newObsParamInfo) {
        ObsParamEntity obsParam = new ObsParamEntity();
        obsParam.setName(newObsParamInfo.getName());
        obsParam.setDescription(newObsParamInfo.getDescription());
        obsParam.setFixedParameter(ObsParam.FixedParameter.EXPOSURE_TIME);
        obsParam.setTimeSampleType(ObsParam.TimeSampleType.DIT);
        obsParam.setSpectralQuantumType(ObsParam.SpectralQuantumType.SPECTRAL_RESOLUTION_ELEMENT);
        obsParam.setDitUnit("sec");
        obsParam.setExposureTimeUnit("sec");
        obsParam.setSnrLambdaUnit("\u00C5");
        obsParam.setSnrUnit("");
        obsParam.setExtraBackgroundNoiseType(ObsParam.ExtraBackgroundNoiseType.ONLY_CALCULATED_BACKGROUND_NOISE);
        obsParam.setExtraSignalType(ObsParam.ExtraSignalType.ONLY_CALCULATED_SIGNAL);
        obsParam.setExtraBackgroundNoiseUnit("e\u207B/s");
        obsParam.setExtraSignalUnit("e\u207B/s");
        obsParam.setFixedSnrType(ObsParam.FixedSnrType.TOTAL);
        obsParam.setModified(false);
        return obsParam;
    }

}
