/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.persistence.database.entities;

import javax.persistence.metamodel.SingularAttribute;
import org.cnrs.lam.dis.etc.datamodel.Instrument.InstrumentType;
import org.cnrs.lam.dis.etc.datamodel.Instrument.PsfSizeType;
import org.cnrs.lam.dis.etc.datamodel.Instrument.PsfType;
import org.cnrs.lam.dis.etc.datamodel.Instrument.SpectralResolutionType;
import org.cnrs.lam.dis.etc.datamodel.Instrument.SpectrographType;

@javax.persistence.metamodel.StaticMetamodel
(value=org.cnrs.lam.dis.etc.persistence.database.entities.InstrumentEntity.class)
@javax.annotation.Generated
(value="org.apache.openjpa.persistence.meta.AnnotationProcessor6",date="Mon Jan 31 14:16:55 CET 2011")
public class InstrumentEntity_ {
    public static volatile SingularAttribute<InstrumentEntity,Double> dark;
    public static volatile SingularAttribute<InstrumentEntity,String> darkUnit;
    public static volatile SingularAttribute<InstrumentEntity,String> description;
    public static volatile SingularAttribute<InstrumentEntity,Double> diameter;
    public static volatile SingularAttribute<InstrumentEntity,String> diameterUnit;
    public static volatile SingularAttribute<InstrumentEntity,Double> fiberDiameter;
    public static volatile SingularAttribute<InstrumentEntity,String> fiberDiameterUnit;
    public static volatile SingularAttribute<InstrumentEntity,DatasetInfoEntity> filterTransmissionEntity;
    public static volatile SingularAttribute<InstrumentEntity,DatasetInfoEntity> fwhmEntity;
    public static volatile SingularAttribute<InstrumentEntity,Long> id;
    public static volatile SingularAttribute<InstrumentEntity,InstrumentType> instrumentType;
    public static volatile SingularAttribute<InstrumentEntity,Integer> nSlit;
    public static volatile SingularAttribute<InstrumentEntity,String> name;
    public static volatile SingularAttribute<InstrumentEntity,Double> obstruction;
    public static volatile SingularAttribute<InstrumentEntity,Double> pixelScale;
    public static volatile SingularAttribute<InstrumentEntity,String> pixelScaleUnit;
    public static volatile SingularAttribute<InstrumentEntity,PsfType> psfType;
    public static volatile SingularAttribute<InstrumentEntity,Double> rangeMax;
    public static volatile SingularAttribute<InstrumentEntity,String> rangeMaxUnit;
    public static volatile SingularAttribute<InstrumentEntity,Double> rangeMin;
    public static volatile SingularAttribute<InstrumentEntity,String> rangeMinUnit;
    public static volatile SingularAttribute<InstrumentEntity,Double> readout;
    public static volatile SingularAttribute<InstrumentEntity,String> readoutUnit;
    public static volatile SingularAttribute<InstrumentEntity,Double> slitLength;
    public static volatile SingularAttribute<InstrumentEntity,String> slitLengthUnit;
    public static volatile SingularAttribute<InstrumentEntity,Double> slitWidth;
    public static volatile SingularAttribute<InstrumentEntity,String> slitWidthUnit;
    public static volatile SingularAttribute<InstrumentEntity,DatasetInfoEntity> spectralResolutionEntity;
    public static volatile SingularAttribute<InstrumentEntity,SpectrographType> spectrographType;
    public static volatile SingularAttribute<InstrumentEntity,DatasetInfoEntity> transmissionEntity;
    public static volatile SingularAttribute<InstrumentEntity,Double> strehlRatio;
    public static volatile SingularAttribute<InstrumentEntity,Double> refWavelength;
    public static volatile SingularAttribute<InstrumentEntity,String> refWavelengthUnit;
    public static volatile SingularAttribute<InstrumentEntity,Double> deltaLambdaPerPixel;
    public static volatile SingularAttribute<InstrumentEntity,String> deltaLambdaPerPixelUnit;
    public static volatile SingularAttribute<InstrumentEntity,SpectralResolutionType> spectralResolutionType;
    public static volatile SingularAttribute<InstrumentEntity,Double> psfDoubleGaussianMultiplier;
    public static volatile SingularAttribute<InstrumentEntity,DatasetInfoEntity> psfDoubleGaussianFwhm1Entity;
    public static volatile SingularAttribute<InstrumentEntity,DatasetInfoEntity> psfDoubleGaussianFwhm2Entity;
    public static volatile SingularAttribute<InstrumentEntity,PsfSizeType> psfSizeType;
    public static volatile SingularAttribute<InstrumentEntity,Double> fixedPsfSize;
    public static volatile SingularAttribute<InstrumentEntity,String> fixedPsfSizeUnit;
    public static volatile SingularAttribute<InstrumentEntity,DatasetInfoEntity> psfSizeFunctionEntity;
    public static volatile SingularAttribute<InstrumentEntity,Double> psfSizePercentage;
}
