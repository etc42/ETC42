/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.persistence.database.entities;

import java.io.Serializable;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.MapKeyColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.cnrs.lam.dis.etc.datamodel.Dataset;

/**
 *
 * @author Nikolaos Apostolakos
 */
@Entity
@Table(name = "DATASET_INFO")
@NamedQueries({
    @NamedQuery(name = "DatasetInfoEntity.findAll", query = "SELECT d FROM DatasetInfoEntity d")})
public class DatasetInfoEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Basic(optional = false)
    @Column(name = "NAME")
    private String name;
    @Column(name = "NAMESPACE")
    private String namespace;
    @Column(name = "DESCRIPTION")
    private String description;
    @Basic(optional = false)
    @Column(name = "TYPE")
    @Enumerated(EnumType.STRING)
    private Dataset.Type type;
    @Column(name = "X_UNIT")
    private String xUnit;
    @Column(name = "Y_UNIT")
    private String yUnit;
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @MapKeyColumn(table="DATASET_OPTION_MAP", name="OPTION_KEY")
    @JoinTable(name = "DATASET_OPTION_MAP"
            , joinColumns = {@JoinColumn(name="DATASET_INFO", referencedColumnName="ID")}
            , inverseJoinColumns = {@JoinColumn(name="DATASET", referencedColumnName="ID")})
    private Map<String, DatasetEntity> datasetMap;
    @Basic(optional = false)
    @Column(name = "DATA_TYPE")
    @Enumerated(EnumType.STRING)
    private Dataset.DataType dataType;

    public DatasetInfoEntity() {
    }

    public DatasetInfoEntity(Long id) {
        this.id = id;
    }

    public DatasetInfoEntity(Long id, String name, Dataset.Type type, Dataset.DataType dataType) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.dataType = dataType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Dataset.Type getType() {
        return type;
    }

    public void setType(Dataset.Type type) {
        this.type = type;
    }

    public String getXUnit() {
        return xUnit;
    }

    public void setXUnit(String xUnit) {
        this.xUnit = xUnit;
    }

    public String getYUnit() {
        return yUnit;
    }

    public void setYUnit(String yUnit) {
        this.yUnit = yUnit;
    }

    public Map<String, DatasetEntity> getDatasetMap() {
        return datasetMap;
    }

    public void setDatasetMap(Map<String, DatasetEntity> datasetMap) {
        this.datasetMap = datasetMap;
    }

    public Dataset.DataType getDataType() {
        return dataType;
    }

    public void setDataType(Dataset.DataType dataType) {
        this.dataType = dataType;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DatasetInfoEntity)) {
            return false;
        }
        DatasetInfoEntity other = (DatasetInfoEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.cnrs.lam.dis.etc.persistence.database.entities.DatasetInfoEntity[id=" + id + "]";
    }

}
