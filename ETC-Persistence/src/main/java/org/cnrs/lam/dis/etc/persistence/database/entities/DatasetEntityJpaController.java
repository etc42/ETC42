/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.persistence.database.entities;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map.Entry;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import org.cnrs.lam.dis.etc.persistence.database.entities.exceptions.NonexistentEntityException;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class DatasetEntityJpaController {

    /*
     * Creates a new dataset entity in the database. Note that the given dataset
     * entity must not be linked to any dataset info yet.
     */
    public void create(DatasetEntity datasetEntity) {
        if (datasetEntity.getDatasetInfoEntityCollection() != null
                && !datasetEntity.getDatasetInfoEntityCollection().isEmpty()) {
            throw new RuntimeException("Tried to create a new dataset entity which "
                    + "was already linked to a DatasetInfo.");
        }
        if (datasetEntity.getDatasetInfoEntityCollection() == null) {
            datasetEntity.setDatasetInfoEntityCollection(new ArrayList<DatasetInfoEntity>());
        }
        EntityManager em = null;
        try {
            em = TransactionController.getEntityManager();
            TransactionController.beginTransaction();
            em.persist(datasetEntity);
            TransactionController.commitTransaction();
        } catch (Exception ex) {
            TransactionController.rollbackTransaction();
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = TransactionController.getEntityManager();
            TransactionController.beginTransaction();
            DatasetEntity datasetEntity;
            try {
                datasetEntity = em.getReference(DatasetEntity.class, id);
                datasetEntity.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The datasetEntity with id " + id + " no longer exists.", enfe);
            }
            Collection<DatasetInfoEntity> datasetInfoEntityCollection = datasetEntity.getDatasetInfoEntityCollection();
            for (DatasetInfoEntity datasetInfoEntity : datasetInfoEntityCollection) {
                String key = null;
                boolean found = false;
                for (Entry<String, DatasetEntity> entry : datasetInfoEntity.getDatasetMap().entrySet()) {
                    if (entry.getValue().getId() == id) {
                        key = entry.getKey();
                        found = true;
                    }
                }
                if (found) {
                    datasetInfoEntity.getDatasetMap().remove(key);
                    datasetInfoEntity = em.merge(datasetInfoEntity);
                }
            }
            em.remove(datasetEntity);
            TransactionController.commitTransaction();
        } catch (Exception ex) {
            TransactionController.rollbackTransaction();
        }
    }

    public List<DatasetEntity> findDatasetEntityEntities() {
        return findDatasetEntityEntities(true, -1, -1);
    }

    public List<DatasetEntity> findDatasetEntityEntities(int maxResults, int firstResult) {
        return findDatasetEntityEntities(false, maxResults, firstResult);
    }

    private List<DatasetEntity> findDatasetEntityEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = TransactionController.getEntityManager();
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(DatasetEntity.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public List<DatasetEntity> findDatasetEntityEntities(Long hash) {
        // ***********************************************************************
        // This is a workaround for bug #35. The problem is that the eclipselink
        // JPA implementation is not doing correctly the checks for maps in many
        // to many relationships, if the values of the map are not unique. For this
        // reason we return always false here, to assure that there will be a unique
        // dataset entry for every option of multi-datasets.
        if (true)
            return new ArrayList<DatasetEntity>();
        // ***********************************************************************
        EntityManager em = TransactionController.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<DatasetEntity> cq = cb.createQuery(DatasetEntity.class);
        Root<DatasetEntity> from = cq.from(DatasetEntity.class);
        cq.where(cb.equal(from.get(DatasetEntity_.hashKey), hash));
        TypedQuery<DatasetEntity> query = em.createQuery(cq);
        return query.getResultList();
    }

    public DatasetEntity findDatasetEntity(Long id) {
        EntityManager em = TransactionController.getEntityManager();
        return em.find(DatasetEntity.class, id);
    }

    public int getDatasetEntityCount() {
        EntityManager em = TransactionController.getEntityManager();
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<DatasetEntity> rt = cq.from(DatasetEntity.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
}
