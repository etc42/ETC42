/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.persistence.database.entities;

import javax.persistence.metamodel.SingularAttribute;
import org.cnrs.lam.dis.etc.datamodel.Site;

@javax.persistence.metamodel.StaticMetamodel
(value=org.cnrs.lam.dis.etc.persistence.database.entities.SiteEntity.class)
@javax.annotation.Generated
(value="org.apache.openjpa.persistence.meta.AnnotationProcessor6",date="Mon Jan 31 14:16:55 CET 2011")
public class SiteEntity_ {
    public static volatile SingularAttribute<SiteEntity,Double> airMass;
    public static volatile SingularAttribute<SiteEntity,String> description;
    public static volatile SingularAttribute<SiteEntity,Short> galacticContributing;
    public static volatile SingularAttribute<SiteEntity,DatasetInfoEntity> galacticProfileEntity;
    public static volatile SingularAttribute<SiteEntity,Long> id;
    public static volatile SingularAttribute<SiteEntity,String> locationType;
    public static volatile SingularAttribute<SiteEntity,String> name;
    public static volatile SingularAttribute<SiteEntity,Double> seeing;
    public static volatile SingularAttribute<SiteEntity,String> seeingUnit;
    public static volatile SingularAttribute<SiteEntity,DatasetInfoEntity> skyAbsorptionEntity;
    public static volatile SingularAttribute<SiteEntity,DatasetInfoEntity> skyEmissionEntity;
    public static volatile SingularAttribute<SiteEntity,String> skyEmissionSelectedOption;
    public static volatile SingularAttribute<SiteEntity,DatasetInfoEntity> skyExtinctionEntity;
    public static volatile SingularAttribute<SiteEntity,Short> zodiacalContributing;
    public static volatile SingularAttribute<SiteEntity,Short> seeingLimited;
    public static volatile SingularAttribute<SiteEntity,DatasetInfoEntity> zodiacalProfileEntity;
    public static volatile SingularAttribute<SiteEntity,DatasetInfoEntity> atmosphericTransmissionEntity;
    public static volatile SingularAttribute<SiteEntity, Site.AtmosphericTransmissionType> atmosphericTransmissionType;
}
