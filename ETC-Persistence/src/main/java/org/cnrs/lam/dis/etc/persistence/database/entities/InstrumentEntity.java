/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.persistence.database.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.cnrs.lam.dis.etc.datamodel.ComponentInfo;
import org.cnrs.lam.dis.etc.datamodel.Dataset.Type;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.cnrs.lam.dis.etc.datamodel.Instrument;
import org.cnrs.lam.dis.etc.datamodel.Instrument.InstrumentType;
import org.cnrs.lam.dis.etc.datamodel.Instrument.SpectrographType;

/**
 *
 * @author Nikolaos Apostolakos
 */
@Entity
@Table(name = "INSTRUMENT")
@NamedQueries({
    @NamedQuery(name = "InstrumentEntity.findAll", query = "SELECT i FROM InstrumentEntity i")})
public class InstrumentEntity implements Serializable, Instrument {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Basic(optional = false)
    @Column(name = "NAME")
    private String name;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "DARK")
    private double dark;
    @Column(name = "DARK_UNIT")
    private String darkUnit;
    @Column(name = "DIAMETER")
    private double diameter;
    @Column(name = "DIAMETER_UNIT")
    private String diameterUnit;
    @Column(name = "FIBER_DIAMETER")
    private double fiberDiameter;
    @Column(name = "FIBER_DIAMETER_UNIT")
    private String fiberDiameterUnit;
    @Basic(optional = false)
    @Column(name = "INSTRUMENT_TYPE")
    @Enumerated(EnumType.STRING)
    private InstrumentType instrumentType;
    @Column(name = "N_SLIT")
    private int nSlit;
    @Column(name = "OBSTRUCTION")
    private double obstruction;
    @Column(name = "PIXEL_SCALE")
    private double pixelScale;
    @Column(name = "PIXEL_SCALE_UNIT")
    private String pixelScaleUnit;
    @Column(name = "RANGE_MAX")
    private double rangeMax;
    @Column(name = "RANGE_MAX_UNIT")
    private String rangeMaxUnit;
    @Column(name = "RANGE_MIN")
    private double rangeMin;
    @Column(name = "RANGE_MIN_UNIT")
    private String rangeMinUnit;
    @Column(name = "READOUT")
    private double readout;
    @Column(name = "READOUT_UNIT")
    private String readoutUnit;
    @Column(name = "SLIT_LENGTH")
    private double slitLength;
    @Column(name = "SLIT_LENGTH_UNIT")
    private String slitLengthUnit;
    @Column(name = "SLIT_WIDTH")
    private double slitWidth;
    @Column(name = "SLIT_WIDTH_UNIT")
    private String slitWidthUnit;
    @Basic(optional = false)
    @Column(name = "PSF_TYPE")
    @Enumerated(EnumType.STRING)
    private PsfType psfType;
    @Basic(optional = false)
    @Column(name = "SPECTROGRAPH_TYPE")
    @Enumerated(EnumType.STRING)
    private SpectrographType spectrographType;
    @JoinColumn(name = "TRANSMISSION", referencedColumnName = "ID")
    @ManyToOne
    private DatasetInfoEntity transmissionEntity;
    @JoinColumn(name = "SPECTRAL_RESOLUTION", referencedColumnName = "ID")
    @ManyToOne
    private DatasetInfoEntity spectralResolutionEntity;
    @JoinColumn(name = "FWHM", referencedColumnName = "ID")
    @ManyToOne
    private DatasetInfoEntity fwhmEntity;
    @JoinColumn(name = "FILTER_TRANSMISSION", referencedColumnName = "ID")
    @ManyToOne
    private DatasetInfoEntity filterTransmissionEntity;
    private transient boolean modified = false;
    @Column(name = "STREHL_RATIO")
    private double strehlRatio;
    @Column(name = "REF_WAVELENGTH")
    private double refWavelength;
    @Column(name = "REF_WAVELENGTH_UNIT")
    private String refWavelengthUnit;
    @Column(name = "DELTA_LAMBDA_PER_PIXEL")
    private double deltaLambdaPerPixel;
    @Column(name = "DELTA_LAMBDA_PER_PIXEL_UNIT")
    private String deltaLambdaPerPixelUnit;
    @Basic(optional = false)
    @Column(name = "SPECTRAL_RESOLUTION_TYPE")
    @Enumerated(EnumType.STRING)
    private SpectralResolutionType spectralResolutionType;
    @Column(name = "PSF_DOUBLE_GAUSSIAN_MULTIPLIER")
    private double psfDoubleGaussianMultiplier;
    @JoinColumn(name = "PSF_DOUBLE_GAUSSIAN_FWHM_1", referencedColumnName = "ID")
    @ManyToOne
    private DatasetInfoEntity psfDoubleGaussianFwhm1Entity;
    @JoinColumn(name = "PSF_DOUBLE_GAUSSIAN_FWHM_2", referencedColumnName = "ID")
    @ManyToOne
    private DatasetInfoEntity psfDoubleGaussianFwhm2Entity;
    @Basic(optional = false)
    @Column(name = "PSF_SIZE_TYPE")
    @Enumerated(EnumType.STRING)
    private PsfSizeType psfSizeType;
    @Column(name = "FIXED_PSF_SIZE")
    private double fixedPsfSize;
    @Column(name = "FIXED_PSF_SIZE_UNIT")
    private String fixedPsfSizeUnit;
    @JoinColumn(name = "PSF_SIZE_FUNCTION", referencedColumnName = "ID")
    @ManyToOne
    private DatasetInfoEntity psfSizeFunctionEntity;
    @Column(name = "PSF_SIZE_PERCENTAGE")
    private double psfSizePercentage;

    public boolean isModified() {
        return modified;
    }

    void setModified(boolean modified) {
        this.modified = modified;
    }

    public InstrumentEntity() {
    }

    public InstrumentEntity(Long id) {
        this.id = id;
    }

    public InstrumentEntity(Long id, String name, InstrumentType instrumentType, PsfType psfType, SpectrographType spectrographType) {
        this.id = id;
        this.name = name;
        this.instrumentType = instrumentType;
        this.psfType = psfType;
        this.spectrographType = spectrographType;
    }

    public InstrumentEntity(String name, InstrumentType instrumentType, PsfType psfType, SpectrographType spectrographType) {
        this.name = name;
        this.instrumentType = instrumentType;
        this.psfType = psfType;
        this.spectrographType = spectrographType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        modified = true;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public double getDark() {
        return dark;
    }

    @Override
    public void setDark(double dark) {
        if (this.dark != dark)
            modified = true;
        this.dark = dark;
    }

    @Override
    public String getDarkUnit() {
        return darkUnit;
    }

    public void setDarkUnit(String darkUnit) {
        if (this.darkUnit != null && !this.darkUnit.equals(darkUnit))
            modified = true;
        this.darkUnit = darkUnit;
    }

    @Override
    public double getDiameter() {
        return diameter;
    }

    @Override
    public void setDiameter(double diameter) {
        if (this.diameter != diameter)
            modified = true;
        this.diameter = diameter;
    }

    @Override
    public String getDiameterUnit() {
        return diameterUnit;
    }

    public void setDiameterUnit(String diameterUnit) {
        if (this.diameterUnit != null && !this.diameterUnit.equals(diameterUnit))
            modified = true;
        this.diameterUnit = diameterUnit;
    }

    @Override
    public double getFiberDiameter() {
        return fiberDiameter;
    }

    @Override
    public void setFiberDiameter(double fiberDiameter) {
        if (this.fiberDiameter != fiberDiameter)
            modified = true;
        this.fiberDiameter = fiberDiameter;
    }

    @Override
    public String getFiberDiameterUnit() {
        return fiberDiameterUnit;
    }

    
    public void setFiberDiameterUnit(String fiberDiameterUnit) {
        if (this.fiberDiameterUnit != null && !this.fiberDiameterUnit.equals(fiberDiameterUnit))
            modified = true;
        this.fiberDiameterUnit = fiberDiameterUnit;
    }

    @Override
    public InstrumentType getInstrumentType() {
        return instrumentType;
    }

    @Override
    public void setInstrumentType(InstrumentType instrumentType) {
        if (this.instrumentType != instrumentType)
            modified = true;
        this.instrumentType = instrumentType;
    }

    @Override
    public int getNSlit() {
        return nSlit;
    }

    @Override
    public void setNSlit(int nSlit) {
        if (this.nSlit != nSlit)
            modified = true;
        this.nSlit = nSlit;
    }

    @Override
    public double getObstruction() {
        return obstruction;
    }

    @Override
    public void setObstruction(double obstruction) {
        if (this.obstruction != obstruction)
            modified = true;
        this.obstruction = obstruction;
    }

    @Override
    public double getPixelScale() {
        return pixelScale;
    }

    @Override
    public void setPixelScale(double pixelScale) {
        if (this.pixelScale != pixelScale)
            modified = true;
        this.pixelScale = pixelScale;
    }

    @Override
    public String getPixelScaleUnit() {
        return pixelScaleUnit;
    }

    public void setPixelScaleUnit(String pixelScaleUnit) {
        if (this.pixelScaleUnit != null && !this.pixelScaleUnit.equals(pixelScaleUnit))
            modified = true;
        this.pixelScaleUnit = pixelScaleUnit;
    }

    @Override
    public double getRangeMax() {
        return rangeMax;
    }

    @Override
    public void setRangeMax(double rangeMax) {
        if (this.rangeMax != rangeMax)
            modified = true;
        this.rangeMax = rangeMax;
    }

    @Override
    public String getRangeMaxUnit() {
        return rangeMaxUnit;
    }

    public void setRangeMaxUnit(String rangeMaxUnit) {
        if (this.rangeMaxUnit != null && !this.rangeMaxUnit.equals(rangeMaxUnit))
            modified = true;
        this.rangeMaxUnit = rangeMaxUnit;
    }

    @Override
    public double getRangeMin() {
        return rangeMin;
    }

    @Override
    public void setRangeMin(double rangeMin) {
        if (this.rangeMin != rangeMin)
            modified = true;
        this.rangeMin = rangeMin;
    }

    @Override
    public String getRangeMinUnit() {
        return rangeMinUnit;
    }

    public void setRangeMinUnit(String rangeMinUnit) {
        if (this.rangeMinUnit != null && !this.rangeMinUnit.equals(rangeMinUnit))
            modified = true;
        this.rangeMinUnit = rangeMinUnit;
    }

    @Override
    public double getReadout() {
        return readout;
    }

    @Override
    public void setReadout(double readout) {
        if (this.readout != readout)
            modified = true;
        this.readout = readout;
    }

    @Override
    public String getReadoutUnit() {
        return readoutUnit;
    }

    public void setReadoutUnit(String readoutUnit) {
        if (this.readoutUnit != null && !this.readoutUnit.equals(readoutUnit))
            modified = true;
        this.readoutUnit = readoutUnit;
    }

    @Override
    public double getSlitLength() {
        return slitLength;
    }

    @Override
    public void setSlitLength(double slitLength) {
        if (this.slitLength != slitLength)
            modified = true;
        this.slitLength = slitLength;
    }

    @Override
    public String getSlitLengthUnit() {
        return slitLengthUnit;
    }

    public void setSlitLengthUnit(String slitLengthUnit) {
        if (this.slitLengthUnit != null && !this.slitLengthUnit.equals(slitLengthUnit))
            modified = true;
        this.slitLengthUnit = slitLengthUnit;
    }

    @Override
    public double getSlitWidth() {
        return slitWidth;
    }

    @Override
    public void setSlitWidth(double slitWidth) {
        if (this.slitWidth != slitWidth)
            modified = true;
        this.slitWidth = slitWidth;
    }

    @Override
    public String getSlitWidthUnit() {
        return slitWidthUnit;
    }

    public void setSlitWidthUnit(String slitWidthUnit) {
        if (this.slitWidthUnit != null && !this.slitWidthUnit.equals(slitWidthUnit))
            modified = true;
        this.slitWidthUnit = slitWidthUnit;
    }

    @Override
    public PsfType getPsfType() {
        return psfType;
    }
    
    @Override
    public void setPsfType(PsfType psfType) {
        if (this.psfType != psfType)
            modified = true;
        this.psfType = psfType;
    }

    @Override
    public SpectrographType getSpectrographType() {
        return spectrographType;
    }

    @Override
    public void setSpectrographType(SpectrographType spectrographType) {
        if (this.spectrographType != spectrographType)
            modified = true;
        this.spectrographType = spectrographType;
    }

    public DatasetInfoEntity getFilterTransmissionEntity() {
        return filterTransmissionEntity;
    }

    public void setFilterTransmissionEntity(DatasetInfoEntity filterTransmissionEntity) {
        this.filterTransmissionEntity = filterTransmissionEntity;
    }

    public DatasetInfoEntity getFwhmEntity() {
        return fwhmEntity;
    }

    public void setFwhmEntity(DatasetInfoEntity fwhmEntity) {
        this.fwhmEntity = fwhmEntity;
    }

    public DatasetInfoEntity getSpectralResolutionEntity() {
        return spectralResolutionEntity;
    }

    public void setSpectralResolutionEntity(DatasetInfoEntity spectralResolutionEntity) {
        this.spectralResolutionEntity = spectralResolutionEntity;
    }

    public DatasetInfoEntity getTransmissionEntity() {
        return transmissionEntity;
    }

    public void setTransmissionEntity(DatasetInfoEntity transmissionEntity) {
        this.transmissionEntity = transmissionEntity;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InstrumentEntity)) {
            return false;
        }
        InstrumentEntity other = (InstrumentEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.cnrs.lam.dis.etc.persistence.database.entities.InstrumentEntity[id=" + id + "]";
    }

    @Override
    public ComponentInfo getInfo() {
        return new ComponentInfo(getName(), getDescription());
    }

    @Override
    public DatasetInfo getFwhm() {
        if (getFwhmEntity() == null)
            return null;
        return new DatasetInfo(getFwhmEntity().getName(), getFwhmEntity().getNamespace(), getFwhmEntity().getDescription());
    }

    @Override
    public void setFwhm(DatasetInfo fwhm) {
        if ((this.fwhmEntity == null && fwhm != null) || 
                (this.fwhmEntity != null && fwhm == null) ||
                (this.fwhmEntity != null && fwhm != null &&
                    !(this.fwhmEntity.getName().equals(fwhm.getName()) && 
                        (this.fwhmEntity.getNamespace() == null ?
                            fwhm.getNamespace() == null :
                            this.fwhmEntity.getNamespace().equals(fwhm.getNamespace())
                        ))
                ))
            modified = true;
        if (fwhm == null) {
            setFwhmEntity(null);
            return;
        }
        DatasetInfoEntityJpaController controller = new DatasetInfoEntityJpaController();
        setFwhmEntity(controller.findDatasetEntity(Type.FWHM, fwhm));
    }

    @Override
    public DatasetInfo getTransmission() {
        if (getTransmissionEntity() == null)
            return null;
        return new DatasetInfo(getTransmissionEntity().getName(), getTransmissionEntity().getNamespace(), getTransmissionEntity().getDescription());
    }

    @Override
    public void setTransmission(DatasetInfo transmission) {
        if ((this.transmissionEntity == null && transmission != null) ||
                (this.transmissionEntity != null && transmission == null) ||
                (this.transmissionEntity != null && transmission != null &&
                    !(this.transmissionEntity.getName().equals(transmission.getName()) &&
                        (this.transmissionEntity.getNamespace() == null ?
                            transmission.getNamespace() == null :
                            this.transmissionEntity.getNamespace().equals(transmission.getNamespace())
                        ))
                ))
            modified = true;
        if (transmission == null) {
            setTransmissionEntity(null);
            return;
        }
        DatasetInfoEntityJpaController controller = new DatasetInfoEntityJpaController();
        setTransmissionEntity(controller.findDatasetEntity(Type.TRANSMISSION, transmission));
    }

    @Override
    public DatasetInfo getSpectralResolution() {
        if (getSpectralResolutionEntity() == null) {
            return null;
        }
        return new DatasetInfo(getSpectralResolutionEntity().getName(), getSpectralResolutionEntity().getNamespace(), getSpectralResolutionEntity().getDescription());
    }

    @Override
    public void setSpectralResolution(DatasetInfo spectralResolution) {
        if ((this.spectralResolutionEntity == null && spectralResolution != null) ||
                (this.spectralResolutionEntity != null && spectralResolution == null) ||
                (this.spectralResolutionEntity != null && spectralResolution != null &&
                    !(this.spectralResolutionEntity.getName().equals(spectralResolution.getName()) &&
                        (this.spectralResolutionEntity.getNamespace() == null ?
                            spectralResolution.getNamespace() == null :
                            this.spectralResolutionEntity.getNamespace().equals(spectralResolution.getNamespace())
                        ))
                ))
            modified = true;
        if (spectralResolution == null) {
            setSpectralResolutionEntity(null);
            return;
        }
        DatasetInfoEntityJpaController controller = new DatasetInfoEntityJpaController();
        setSpectralResolutionEntity(controller.findDatasetEntity(Type.SPECTRAL_RESOLUTION, spectralResolution));
    }

    @Override
    public DatasetInfo getFilterTransmission() {
        if (getFilterTransmissionEntity() == null)
            return null;
        return new DatasetInfo(getFilterTransmissionEntity().getName(), getFilterTransmissionEntity().getNamespace(), getFilterTransmissionEntity().getDescription());
    }

    @Override
    public void setFilterTransmission(DatasetInfo filterTransmission) {
        if ((this.filterTransmissionEntity == null && filterTransmission != null) ||
                (this.filterTransmissionEntity != null && filterTransmission == null) ||
                (this.filterTransmissionEntity != null && filterTransmission != null &&
                    !(this.filterTransmissionEntity.getName().equals(filterTransmission.getName()) &&
                        (this.filterTransmissionEntity.getNamespace() == null ?
                            filterTransmission.getNamespace() == null :
                            this.filterTransmissionEntity.getNamespace().equals(filterTransmission.getNamespace())
                        ))
                ))
            modified = true;
        if (filterTransmission == null) {
            setFilterTransmissionEntity(null);
            return;
        }
        DatasetInfoEntityJpaController controller = new DatasetInfoEntityJpaController();
        setFilterTransmissionEntity(controller.findDatasetEntity(Type.FILTER_TRANSMISSION, filterTransmission));
    }
    
    @Override
    public double getStrehlRatio() {
        return strehlRatio;
    }
    
    @Override
    public void setStrehlRatio(double strehlRatio) {
        if (this.rangeMax != strehlRatio)
            modified = true;
        this.strehlRatio = strehlRatio;
    }
    
    @Override
    public double getRefWavelength() {
        return refWavelength;
    }
    
    @Override
    public void setRefWavelength(double refWavelength) {
        if (this.refWavelength != refWavelength)
            modified = true;
        this.refWavelength = refWavelength;
    }

    @Override
    public String getRefWavelengthUnit() {
        return refWavelengthUnit;
    }

    public void setRefWavelengthUnit(String refWavelengthUnit) {
        if (this.refWavelengthUnit != null && !this.refWavelengthUnit.equals(refWavelengthUnit))
            modified = true;
        this.refWavelengthUnit = refWavelengthUnit;
    }

    @Override
    public double getDeltaLambdaPerPixel() {
        return deltaLambdaPerPixel;
    }

    @Override
    public void setDeltaLambdaPerPixel(double deltaLambda) {
        if (this.deltaLambdaPerPixel != deltaLambda)
            modified = true;
        this.deltaLambdaPerPixel = deltaLambda;
    }

    @Override
    public String getDeltaLambdaPerPixelUnit() {
        return deltaLambdaPerPixelUnit;
    }
    
    public void setDeltaLambdaPerPixelUnit(String unit) {
        if (this.deltaLambdaPerPixelUnit != null && !this.deltaLambdaPerPixelUnit.equals(unit))
            modified = true;
        this.deltaLambdaPerPixelUnit = unit;
    }

    @Override
    public SpectralResolutionType getSpectralResolutionType() {
        return spectralResolutionType;
    }

    @Override
    public void setSpectralResolutionType(SpectralResolutionType type) {
        if (this.spectralResolutionType != type)
            modified = true;
        this.spectralResolutionType = type;
    }

    @Override
    public double getPsfDoubleGaussianMultiplier() {
        return psfDoubleGaussianMultiplier;
    }

    @Override
    public void setPsfDoubleGaussianMultiplier(double multiplier) {
        if (this.psfDoubleGaussianMultiplier != multiplier)
            modified = true;
        this.psfDoubleGaussianMultiplier = multiplier;
    }

    public DatasetInfoEntity getPsfDoubleGaussianFwhm1Entity() {
        return psfDoubleGaussianFwhm1Entity;
    }

    public void setPsfDoubleGaussianFwhm1Entity(DatasetInfoEntity entity) {
        this.psfDoubleGaussianFwhm1Entity = entity;
    }

    @Override
    public DatasetInfo getPsfDoubleGaussianFwhm1() {
        if (getPsfDoubleGaussianFwhm1Entity() == null)
            return null;
        return new DatasetInfo(getPsfDoubleGaussianFwhm1Entity().getName()
                , getPsfDoubleGaussianFwhm1Entity().getNamespace()
                , getPsfDoubleGaussianFwhm1Entity().getDescription());
    }

    @Override
    public void setPsfDoubleGaussianFwhm1(DatasetInfo info) {
        if ((this.psfDoubleGaussianFwhm1Entity == null && info != null) ||
                (this.psfDoubleGaussianFwhm1Entity != null && info == null) ||
                (this.psfDoubleGaussianFwhm1Entity != null && info != null &&
                    !(this.psfDoubleGaussianFwhm1Entity.getName().equals(info.getName()) &&
                        (this.psfDoubleGaussianFwhm1Entity.getNamespace() == null ?
                            info.getNamespace() == null :
                            this.psfDoubleGaussianFwhm1Entity.getNamespace().equals(info.getNamespace())
                        ))
                ))
            modified = true;
        if (info == null) {
            setPsfDoubleGaussianFwhm1Entity(null);
            return;
        }
        DatasetInfoEntityJpaController controller = new DatasetInfoEntityJpaController();
        setPsfDoubleGaussianFwhm1Entity(controller.findDatasetEntity(Type.PSF_DOUBLE_GAUSSIAN_FWHM_1, info));
    }

    public DatasetInfoEntity getPsfDoubleGaussianFwhm2Entity() {
        return psfDoubleGaussianFwhm2Entity;
    }

    public void setPsfDoubleGaussianFwhm2Entity(DatasetInfoEntity entity) {
        this.psfDoubleGaussianFwhm2Entity = entity;
    }

    @Override
    public DatasetInfo getPsfDoubleGaussianFwhm2() {
        if (getPsfDoubleGaussianFwhm2Entity() == null)
            return null;
        return new DatasetInfo(getPsfDoubleGaussianFwhm2Entity().getName()
                , getPsfDoubleGaussianFwhm2Entity().getNamespace()
                , getPsfDoubleGaussianFwhm2Entity().getDescription());
    }

    @Override
    public void setPsfDoubleGaussianFwhm2(DatasetInfo info) {
        if ((this.psfDoubleGaussianFwhm2Entity == null && info != null) ||
                (this.psfDoubleGaussianFwhm2Entity != null && info == null) ||
                (this.psfDoubleGaussianFwhm2Entity != null && info != null &&
                    !(this.psfDoubleGaussianFwhm2Entity.getName().equals(info.getName()) &&
                        (this.psfDoubleGaussianFwhm2Entity.getNamespace() == null ?
                            info.getNamespace() == null :
                            this.psfDoubleGaussianFwhm2Entity.getNamespace().equals(info.getNamespace())
                        ))
                ))
            modified = true;
        if (info == null) {
            setPsfDoubleGaussianFwhm2Entity(null);
            return;
        }
        DatasetInfoEntityJpaController controller = new DatasetInfoEntityJpaController();
        setPsfDoubleGaussianFwhm2Entity(controller.findDatasetEntity(Type.PSF_DOUBLE_GAUSSIAN_FWHM_2, info));
    }

    @Override
    public PsfSizeType getPsfSizeType() {
        return psfSizeType;
    }
    
    @Override
    public void setPsfSizeType(PsfSizeType type) {
        if (this.psfSizeType != type)
            modified = true;
        this.psfSizeType = type;
    }

    @Override
    public double getFixedPsfSize() {
        return fixedPsfSize;
    }

    @Override
    public void setFixedPsfSize(double size) {
        if (this.fixedPsfSize != size)
            modified = true;
        this.fixedPsfSize = size;
    }

    @Override
    public String getFixedPsfSizeUnit() {
        return fixedPsfSizeUnit;
    }
    
    public void setFixedPsfSizeUnit(String unit) {
        if (this.fixedPsfSizeUnit != null && !this.fixedPsfSizeUnit.equals(unit))
            modified = true;
        this.fixedPsfSizeUnit = unit;
    }

    @Override
    public double getPsfSizePercentage() {
        return psfSizePercentage;
    }

    @Override
    public void setPsfSizePercentage(double percentage) {
        if (this.psfSizePercentage != percentage)
            modified = true;
        this.psfSizePercentage = percentage;
    }

    public DatasetInfoEntity getPsfSizeFunctionEntity() {
        return psfSizeFunctionEntity;
    }

    public void setPsfSizeFunctionEntity(DatasetInfoEntity entity) {
        this.psfSizeFunctionEntity = entity;
    }

    @Override
    public DatasetInfo getPsfSizeFunction() {
        if (getPsfSizeFunctionEntity() == null)
            return null;
        return new DatasetInfo(getPsfSizeFunctionEntity().getName()
                , getPsfSizeFunctionEntity().getNamespace()
                , getPsfSizeFunctionEntity().getDescription());
    }

    @Override
    public void setPsfSizeFunction(DatasetInfo info) {
        if ((this.psfSizeFunctionEntity == null && info != null) ||
                (this.psfSizeFunctionEntity != null && info == null) ||
                (this.psfSizeFunctionEntity != null && info != null &&
                    !(this.psfSizeFunctionEntity.getName().equals(info.getName()) &&
                        (this.psfSizeFunctionEntity.getNamespace() == null ?
                            info.getNamespace() == null :
                            this.psfSizeFunctionEntity.getNamespace().equals(info.getNamespace())
                        ))
                ))
            modified = true;
        if (info == null) {
            setPsfSizeFunctionEntity(null);
            return;
        }
        DatasetInfoEntityJpaController controller = new DatasetInfoEntityJpaController();
        setPsfSizeFunctionEntity(controller.findDatasetEntity(Type.PSF_SIZE_FUNCTION, info));
    }
}
