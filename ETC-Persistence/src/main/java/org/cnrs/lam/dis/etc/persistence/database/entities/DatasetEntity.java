/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.persistence.database.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Nikolaos Apostolakos
 */
@Entity
@Table(name = "DATASET")
@NamedQueries({
    @NamedQuery(name = "DatasetEntity.findAll", query = "SELECT d FROM DatasetEntity d")})
public class DatasetEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Basic(optional = false)
    @Column(name = "HASH_KEY")
    private long hashKey;
    @Lob
    @Basic (fetch = FetchType.LAZY)
    @Column(name = "DATA_BLOB")
    private Map<Double, Double> dataBlob;
    @ManyToMany(mappedBy = "datasetMap")
    private Collection<DatasetInfoEntity> datasetInfoEntityCollection;

    public DatasetEntity() {
    }

    public DatasetEntity(Long id) {
        this.id = id;
    }

    public DatasetEntity(Long id, long hashKey) {
        this.id = id;
        this.hashKey = hashKey;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getHashKey() {
        return hashKey;
    }

    public void setHashKey(long hashKey) {
        this.hashKey = hashKey;
    }

    public Map<Double, Double> getDataBlob() {
        return dataBlob;
    }

    public void setDataBlob(Map<Double, Double> dataBlob) {
        this.dataBlob = dataBlob;
    }

    public Collection<DatasetInfoEntity> getDatasetInfoEntityCollection() {
        return datasetInfoEntityCollection;
    }

    public void setDatasetInfoEntityCollection(Collection<DatasetInfoEntity> datasetInfoEntityCollection) {
        this.datasetInfoEntityCollection = datasetInfoEntityCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DatasetEntity)) {
            return false;
        }
        DatasetEntity other = (DatasetEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.cnrs.lam.dis.etc.persistence.database.entities.DatasetEntity[id=" + id + "]";
    }

}
