/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.persistence.database.entities;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.cnrs.lam.dis.etc.datamodel.ComponentInfo;
import org.cnrs.lam.dis.etc.datamodel.Site;
import org.cnrs.lam.dis.etc.persistence.database.entities.exceptions.NonexistentEntityException;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class SiteEntityJpaController {

    public void create(SiteEntity siteEntity) {
        EntityManager em = null;
        try {
            em = TransactionController.getEntityManager();
            TransactionController.beginTransaction();
            em.persist(siteEntity);
            TransactionController.commitTransaction();
            siteEntity.setModified(false);
        } catch (Exception ex) {
            TransactionController.rollbackTransaction();
        }
    }

    public void edit(SiteEntity siteEntity) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = TransactionController.getEntityManager();
            TransactionController.beginTransaction();
            siteEntity = em.merge(siteEntity);
            TransactionController.commitTransaction();
            siteEntity.setModified(false);
        } catch (Exception ex) {
            TransactionController.rollbackTransaction();
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = siteEntity.getId();
                if (findSiteEntity(id) == null) {
                    throw new NonexistentEntityException("The siteEntity with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = TransactionController.getEntityManager();
            TransactionController.beginTransaction();
            SiteEntity siteEntity;
            try {
                siteEntity = em.getReference(SiteEntity.class, id);
                siteEntity.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The siteEntity with id " + id + " no longer exists.", enfe);
            }
            em.remove(siteEntity);
            TransactionController.commitTransaction();
            siteEntity.setModified(true);
        } catch (Exception ex) {
            TransactionController.rollbackTransaction();
        }
    }

    public List<SiteEntity> findSiteEntityEntities() {
        return findSiteEntityEntities(true, -1, -1);
    }

    public List<SiteEntity> findSiteEntityEntities(int maxResults, int firstResult) {
        return findSiteEntityEntities(false, maxResults, firstResult);
    }

    private List<SiteEntity> findSiteEntityEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = TransactionController.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<SiteEntity> cq = cb.createQuery(SiteEntity.class);
        Root<SiteEntity> from = cq.from(SiteEntity.class);
        cq.select(cq.from(SiteEntity.class));
        cq.orderBy(cb.asc(from.get(SiteEntity_.name)));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public List<SiteEntity> findSitesUsingDataset(DatasetInfoEntity datasetInfoEntity) {
        EntityManager em = TransactionController.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<SiteEntity> cq = cb.createQuery(SiteEntity.class);
        Root<SiteEntity> from = cq.from(SiteEntity.class);
        cq.where(cb.or(
                cb.equal(from.get(SiteEntity_.galacticProfileEntity), datasetInfoEntity),
                cb.equal(from.get(SiteEntity_.skyAbsorptionEntity), datasetInfoEntity),
                cb.equal(from.get(SiteEntity_.skyEmissionEntity), datasetInfoEntity),
                cb.equal(from.get(SiteEntity_.skyExtinctionEntity), datasetInfoEntity),
                cb.equal(from.get(SiteEntity_.zodiacalProfileEntity), datasetInfoEntity),
                cb.equal(from.get(SiteEntity_.atmosphericTransmissionEntity), datasetInfoEntity)
        ));
        TypedQuery<SiteEntity> query = em.createQuery(cq);
        return query.getResultList();
    }

    public SiteEntity findSiteEntity(ComponentInfo info) {
        EntityManager em = TransactionController.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<SiteEntity> cq = cb.createQuery(SiteEntity.class);
        Root<SiteEntity> from = cq.from(SiteEntity.class);
        cq.where(cb.equal(from.get(SiteEntity_.name), info.getName()));
        TypedQuery<SiteEntity> query = em.createQuery(cq);
        for (SiteEntity result : query.getResultList()) {
            result.setModified(false);
            return result;
        }
        return null;
    }

    public SiteEntity findSiteEntity(Long id) {
        EntityManager em = TransactionController.getEntityManager();
        return em.find(SiteEntity.class, id);
    }

    public int getSiteEntityCount() {
        EntityManager em = TransactionController.getEntityManager();
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<SiteEntity> rt = cq.from(SiteEntity.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public Site createNewUnpersistedSiteEntity(ComponentInfo newSiteInfo) {
        SiteEntity site = new SiteEntity();
        site.setName(newSiteInfo.getName());
        site.setDescription(newSiteInfo.getDescription());
        site.setLocationType(Site.LocationType.SPACE);
        site.setGalacticContributing(false);
        site.setZodiacalContributing(false);
        site.setSeeingLimited(false);
        site.setSeeingUnit("arcsec");
        site.setAtmosphericTransmissionType(Site.AtmosphericTransmissionType.ABSORPTION_EXTINCTION);
        site.setModified(false);
        return site;
    }

}
