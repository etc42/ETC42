/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.persistence.database.entities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.cnrs.lam.dis.etc.datamodel.Dataset.Type;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.cnrs.lam.dis.etc.persistence.PersistenceDeviceException;
import org.cnrs.lam.dis.etc.persistence.database.entities.exceptions.NonexistentEntityException;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class DatasetInfoEntityJpaController {

    public void create(DatasetInfoEntity datasetInfoEntity) {
        EntityManager em = null;
        try {
            em = TransactionController.getEntityManager();
            TransactionController.beginTransaction();
            
            Map<String, DatasetEntity> datasetMap = datasetInfoEntity.getDatasetMap();
            Map<String, DatasetEntity> mapLinkedInDb = new HashMap<String, DatasetEntity>();
            for (Map.Entry<String, DatasetEntity> entry : datasetMap.entrySet()) {
                String key = entry.getKey();
                DatasetEntity dataset = entry.getValue();
                dataset = em.getReference(dataset.getClass(), dataset.getId());
                mapLinkedInDb.put(key, dataset);
            }
            datasetInfoEntity.setDatasetMap(mapLinkedInDb);
            em.persist(datasetInfoEntity);
            for (DatasetEntity dataset : mapLinkedInDb.values()) {
                dataset.getDatasetInfoEntityCollection().add(datasetInfoEntity);
                dataset = em.merge(dataset);
            }
            TransactionController.commitTransaction();
        } catch (Exception ex) {
            TransactionController.rollbackTransaction();
        }
    }

    private void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = TransactionController.getEntityManager();
            TransactionController.beginTransaction();
            DatasetInfoEntity datasetInfoEntity;
            try {
                datasetInfoEntity = em.getReference(DatasetInfoEntity.class, id);
                datasetInfoEntity.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The datasetInfoEntity with id " + id + " no longer exists.", enfe);
            }
            for (DatasetEntity dataset : datasetInfoEntity.getDatasetMap().values()) {
                if (dataset != null) {
                    dataset.getDatasetInfoEntityCollection().remove(datasetInfoEntity);
                    dataset = em.merge(dataset);
                }
            }
            em.remove(datasetInfoEntity);
            TransactionController.commitTransaction();
        } catch (Exception ex) {
            TransactionController.rollbackTransaction();
        }
    }

    public List<DatasetInfoEntity> findDatasetInfoEntityEntities() {
        return findDatasetInfoEntityEntities(true, -1, -1);
    }

    public List<DatasetInfoEntity> findDatasetInfoEntityEntities(int maxResults, int firstResult) {
        return findDatasetInfoEntityEntities(false, maxResults, firstResult);
    }

    private List<DatasetInfoEntity> findDatasetInfoEntityEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = TransactionController.getEntityManager();
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(DatasetInfoEntity.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public List<DatasetInfoEntity> findDatasetInfoEntityEntities(Type type) {
        EntityManager em = TransactionController.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<DatasetInfoEntity> cq = cb.createQuery(DatasetInfoEntity.class);
        Root<DatasetInfoEntity> from = cq.from(DatasetInfoEntity.class);
        cq.where(cb.equal(from.get(DatasetInfoEntity_.type), type));
        cq.orderBy(cb.asc(from.get(DatasetInfoEntity_.namespace)), cb.asc(from.get(DatasetInfoEntity_.name)));
        TypedQuery<DatasetInfoEntity> query = em.createQuery(cq);
        return query.getResultList();
    }

    public List<DatasetInfoEntity> findDatasetInfoEntityEntities(Type type, String namespace) {
        EntityManager em = TransactionController.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<DatasetInfoEntity> cq = cb.createQuery(DatasetInfoEntity.class);
        Root<DatasetInfoEntity> from = cq.from(DatasetInfoEntity.class);
        cq.where(cb.and(
                cb.equal(from.get(DatasetInfoEntity_.type), type),
                cb.or(
                from.get(DatasetInfoEntity_.namespace).isNull(),
                cb.equal(from.get(DatasetInfoEntity_.namespace), namespace))));
        cq.orderBy(cb.asc(from.get(DatasetInfoEntity_.namespace)), cb.asc(from.get(DatasetInfoEntity_.name)));
        TypedQuery<DatasetInfoEntity> query = em.createQuery(cq);
        return query.getResultList();
    }

    public List<DatasetInfoEntity> findInstrumentDatasetsInNamespace(String namespace) {
        EntityManager em = TransactionController.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<DatasetInfoEntity> cq = cb.createQuery(DatasetInfoEntity.class);
        Root<DatasetInfoEntity> from = cq.from(DatasetInfoEntity.class);
        cq.where(cb.and(
                cb.equal(from.get(DatasetInfoEntity_.namespace), namespace),
                cb.or(
                    cb.equal(from.get(DatasetInfoEntity_.type), Type.FILTER_TRANSMISSION),
                    cb.equal(from.get(DatasetInfoEntity_.type), Type.FWHM),
                    cb.equal(from.get(DatasetInfoEntity_.type), Type.SPECTRAL_RESOLUTION),
                    cb.equal(from.get(DatasetInfoEntity_.type), Type.TRANSMISSION),
                    cb.equal(from.get(DatasetInfoEntity_.type), Type.PSF_DOUBLE_GAUSSIAN_FWHM_1),
                    cb.equal(from.get(DatasetInfoEntity_.type), Type.PSF_DOUBLE_GAUSSIAN_FWHM_2),
                    cb.equal(from.get(DatasetInfoEntity_.type), Type.PSF_SIZE_FUNCTION)
                )
        ));
        cq.orderBy(cb.asc(from.get(DatasetInfoEntity_.namespace)), cb.asc(from.get(DatasetInfoEntity_.name)));
        TypedQuery<DatasetInfoEntity> query = em.createQuery(cq);
        return query.getResultList();
    }

    public List<DatasetInfoEntity> findSiteDatasetsInNamespace(String namespace) {
        EntityManager em = TransactionController.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<DatasetInfoEntity> cq = cb.createQuery(DatasetInfoEntity.class);
        Root<DatasetInfoEntity> from = cq.from(DatasetInfoEntity.class);
        cq.where(cb.and(
                cb.equal(from.get(DatasetInfoEntity_.namespace), namespace),
                cb.or(
                    cb.equal(from.get(DatasetInfoEntity_.type), Type.GALACTIC),
                    cb.equal(from.get(DatasetInfoEntity_.type), Type.ZODIACAL),
                    cb.equal(from.get(DatasetInfoEntity_.type), Type.SKY_ABSORPTION),
                    cb.equal(from.get(DatasetInfoEntity_.type), Type.SKY_EMISSION),
                    cb.equal(from.get(DatasetInfoEntity_.type), Type.SKY_EXTINCTION),
                    cb.equal(from.get(DatasetInfoEntity_.type), Type.ATMOSPHERIC_TRANSMISSION)
                )
        ));
        cq.orderBy(cb.asc(from.get(DatasetInfoEntity_.namespace)), cb.asc(from.get(DatasetInfoEntity_.name)));
        TypedQuery<DatasetInfoEntity> query = em.createQuery(cq);
        return query.getResultList();
    }

    public List<DatasetInfoEntity> findSourceDatasetsInNamespace(String namespace) {
        EntityManager em = TransactionController.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<DatasetInfoEntity> cq = cb.createQuery(DatasetInfoEntity.class);
        Root<DatasetInfoEntity> from = cq.from(DatasetInfoEntity.class);
        cq.where(cb.and(
                cb.equal(from.get(DatasetInfoEntity_.namespace), namespace),
                cb.equal(from.get(DatasetInfoEntity_.type), Type.SPECTRAL_DIST_TEMPLATE)
        ));
        cq.orderBy(cb.asc(from.get(DatasetInfoEntity_.namespace)), cb.asc(from.get(DatasetInfoEntity_.name)));
        TypedQuery<DatasetInfoEntity> query = em.createQuery(cq);
        return query.getResultList();
    }

    public List<DatasetInfoEntity> findObsParamDatasetsInNamespace(String namespace) {
        EntityManager em = TransactionController.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<DatasetInfoEntity> cq = cb.createQuery(DatasetInfoEntity.class);
        Root<DatasetInfoEntity> from = cq.from(DatasetInfoEntity.class);
        cq.where(cb.and(
                cb.equal(from.get(DatasetInfoEntity_.namespace), namespace),
                cb.equal(from.get(DatasetInfoEntity_.type), Type.EXTRA_BACKGROUND_NOISE),
                cb.equal(from.get(DatasetInfoEntity_.type), Type.EXTRA_SIGNAL)
        ));
        cq.orderBy(cb.asc(from.get(DatasetInfoEntity_.namespace)), cb.asc(from.get(DatasetInfoEntity_.name)));
        TypedQuery<DatasetInfoEntity> query = em.createQuery(cq);
        return query.getResultList();
    }

    public DatasetInfoEntity findDatasetEntity(Type type, DatasetInfo info) {
        EntityManager em = TransactionController.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<DatasetInfoEntity> cq = cb.createQuery(DatasetInfoEntity.class);
        Root<DatasetInfoEntity> from = cq.from(DatasetInfoEntity.class);
        cq.where(cb.and(
                cb.equal(from.get(DatasetInfoEntity_.name), info.getName()),
                cb.or(
                from.get(DatasetInfoEntity_.namespace).isNull(),
                cb.equal(from.get(DatasetInfoEntity_.namespace), info.getNamespace())),
                cb.equal(from.get(DatasetInfoEntity_.type), type)));
        TypedQuery<DatasetInfoEntity> query = em.createQuery(cq);
        System.out.println(query.getResultList().size());
        for (DatasetInfoEntity result : query.getResultList()) {
            // The above query returns both the entries which have the same namespace
            // and the ones which have null as namespace. Here we do another check
            // to filter out fake results
            if ((info.getNamespace() == null && result.getNamespace() == null)
                    || info.getNamespace().equals(result.getNamespace())) {
                return result;
            }
        }
        return null;
    }

    public DatasetInfoEntity findDatasetInfoEntity(Long id) {
        EntityManager em = TransactionController.getEntityManager();
        return em.find(DatasetInfoEntity.class, id);
    }

    public int getDatasetInfoEntityCount() {
        EntityManager em = TransactionController.getEntityManager();
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<DatasetInfoEntity> rt = cq.from(DatasetInfoEntity.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public void deleteDatasetInfo(DatasetInfoEntity datasetInfo) throws PersistenceDeviceException {
        try {
            TransactionController.beginTransaction();
            DatasetInfoEntityJpaController datasetInfoController = new DatasetInfoEntityJpaController();
            DatasetEntityJpaController datasetController = new DatasetEntityJpaController();
            List<Long> datasetsToRemove = new ArrayList<Long>();
            for (DatasetEntity dataset : datasetInfo.getDatasetMap().values()) {
                if (dataset.getDatasetInfoEntityCollection().size() == 1) {
                    datasetsToRemove.add(dataset.getId());
//                    datasetController.destroy(dataset.getId());
                }
            }
            for (Long id : datasetsToRemove) {
                datasetController.destroy(id);
            }
            datasetInfoController.destroy(datasetInfo.getId());
            TransactionController.commitTransaction();
        } catch (Exception e) {
            TransactionController.rollbackTransaction();
            throw new PersistenceDeviceException(e);
        }
    }

}
