/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.persistence.database;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.Dataset.Type;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.cnrs.lam.dis.etc.persistence.DatasetManager;
import org.cnrs.lam.dis.etc.persistence.InUseException;
import org.cnrs.lam.dis.etc.persistence.NameExistsException;
import org.cnrs.lam.dis.etc.persistence.NameNotFoundException;
import org.cnrs.lam.dis.etc.persistence.PersistenceDeviceException;
import org.cnrs.lam.dis.etc.persistence.database.entities.DatasetEntity;
import org.cnrs.lam.dis.etc.persistence.database.entities.DatasetEntityJpaController;
import org.cnrs.lam.dis.etc.persistence.database.entities.DatasetInfoEntity;
import org.cnrs.lam.dis.etc.persistence.database.entities.DatasetInfoEntityJpaController;
import org.cnrs.lam.dis.etc.persistence.database.entities.InstrumentEntity;
import org.cnrs.lam.dis.etc.persistence.database.entities.InstrumentEntityJpaController;
import org.cnrs.lam.dis.etc.persistence.database.entities.ObsParamEntity;
import org.cnrs.lam.dis.etc.persistence.database.entities.ObsParamEntityJpaController;
import org.cnrs.lam.dis.etc.persistence.database.entities.SiteEntity;
import org.cnrs.lam.dis.etc.persistence.database.entities.SiteEntityJpaController;
import org.cnrs.lam.dis.etc.persistence.database.entities.SourceEntity;
import org.cnrs.lam.dis.etc.persistence.database.entities.SourceEntityJpaController;
import org.cnrs.lam.dis.etc.persistence.database.entities.TransactionController;
import org.cnrs.lam.dis.etc.persistence.database.utils.HashUtil;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class DatasetManagerImpl implements DatasetManager {

    private static Logger logger = Logger.getLogger(DatasetManagerImpl.class);

    @Override
    public List<DatasetInfo> getAvailableDatasetList(Type type, String namespace) throws PersistenceDeviceException {
        List<DatasetInfo> result = new ArrayList<DatasetInfo>();
        result.add(null);
        DatasetInfoEntityJpaController controller = new DatasetInfoEntityJpaController();
        List<DatasetInfoEntity> list = null;
        if ("".equals(namespace))
            list = controller.findDatasetInfoEntityEntities(type);
        else
            list = controller.findDatasetInfoEntityEntities(type, namespace);
        for (DatasetInfoEntity entity : list) {
            result.add(new DatasetInfo(entity.getName(), entity.getNamespace(), entity.getDescription()));
        }
        return result;
    }

    @Override
    public List<String> getDatasetOptionList(Type type, DatasetInfo datasetInfo) 
            throws NameNotFoundException, PersistenceDeviceException {
        DatasetInfoEntityJpaController controller = new DatasetInfoEntityJpaController();
        DatasetInfoEntity datasetInfoEntity = controller.findDatasetEntity(type, datasetInfo);
        if (datasetInfoEntity == null) {
            logger.info("Failed to load dataset " + datasetInfo + " because it "
                    + "wasn't found in the database");
            throw new NameNotFoundException("Failed to load dataset " + datasetInfo +
                    " because it wasn't found in the database");
        }
        Map<String, DatasetEntity> datasetMap = datasetInfoEntity.getDatasetMap();
        // If we have a single dataset we return null
        if (datasetMap.keySet().contains(""))
            return null;
        List<String> result = new ArrayList<String>();
        for (String option : datasetMap.keySet()) {
            result.add(option);
        }
        return result;
    }

    @Override
    public Dataset loadDataset(Type type, DatasetInfo datasetInfo, String option) 
            throws NameNotFoundException, PersistenceDeviceException {
        DatasetInfoEntityJpaController controller = new DatasetInfoEntityJpaController();
        DatasetInfoEntity datasetInfoEntity = controller.findDatasetEntity(type, datasetInfo);
        if (datasetInfoEntity == null) {
            logger.info("Failed to load dataset " + datasetInfo + " because it "
                    + "wasn't found in the database");
            throw new NameNotFoundException("Failed to load dataset " + datasetInfo +
                    " because it wasn't found in the database");
        }
        // Get the name, namespace and description
        String name = datasetInfoEntity.getName();
        String namespace = datasetInfoEntity.getNamespace();
        String description = datasetInfoEntity.getDescription();
        // Get the correct dataset for the case we have multi-dataset. Note that
        // if we have a single dataset then the map should have only one entry with
        // key the empty string
        String optionKey = option;
        if (optionKey == null)
            optionKey = "";
        DatasetEntity datasetEntity = datasetInfoEntity.getDatasetMap().get(optionKey);
        if (datasetEntity == null) {
            logger.info("Failed to load option " + optionKey + " from dataset " + datasetInfo
                    + " because this dataset doesn't have such option");
            throw new NameNotFoundException("Failed to load option " + optionKey + " from dataset "
                    + datasetInfo + " because this dataset doesn't have such option");
        }
        Map<Double, Double> data = datasetEntity.getDataBlob();
        String xUnit = datasetInfoEntity.getXUnit();
        String yUnit = datasetInfoEntity.getYUnit();
        Dataset.DataType dataType = datasetInfoEntity.getDataType();
        return new DatasetImpl(new DatasetInfo(name,namespace, description), type, data, xUnit, yUnit, option, dataType);
    }

    @Override
    public void saveDataset(Dataset dataset) throws NameExistsException, PersistenceDeviceException {
        String optionKey = dataset.getOption();
        DatasetInfoEntityJpaController datasetInfoController = new DatasetInfoEntityJpaController();
        // Get the DatasetInfo from the database
        DatasetInfoEntity datasetInfoEntity = datasetInfoController.findDatasetEntity(dataset.getType(), dataset.getInfo());
        // Check if there is already a dataset with the same name/option
        if (datasetInfoEntity != null) {
            if (optionKey == null) {
                logger.info("Failed to save dataset " + dataset.getInfo() + " because "
                        + "there is another dataset with the same name");
                throw new NameExistsException();
            }
            if (optionKey != null && datasetInfoEntity.getDatasetMap().containsKey(optionKey)) {
                logger.info("Failed to save option " + optionKey + " from dataset "
                        + dataset.getInfo() + " because it already has this option");
                throw new NameExistsException();
            }
            if (optionKey != null && datasetInfoEntity.getDatasetMap().containsKey("")) {
                logger.info("Failed to save option " + optionKey + " from dataset "
                        + dataset.getInfo() + " because the dataset is a single dataset");
                throw new NameExistsException();
            }
        }
        // Now if there is no dataset info for this dataset we create it
        if (datasetInfoEntity == null) {
            datasetInfoEntity = new DatasetInfoEntity();
            datasetInfoEntity.setName(dataset.getInfo().getName());
            datasetInfoEntity.setNamespace(dataset.getInfo().getNamespace());
            datasetInfoEntity.setDescription(dataset.getInfo().getDescription());
            datasetInfoEntity.setType(dataset.getType());
            datasetInfoEntity.setXUnit(dataset.getXUnit());
            datasetInfoEntity.setYUnit(dataset.getYUnit());
            datasetInfoEntity.setDatasetMap(new HashMap<String, DatasetEntity>());
            datasetInfoEntity.setDataType(dataset.getDataType());
        }
        // Begin a transaction so we can roll back everything if anything goes wrong
        TransactionController.beginTransaction();
        try {
            // To save space in the database we use the same entry in the DATASET
            // table if the data are the same. Here we do this check.
            Long hash = HashUtil.getHash(dataset.getData());
            DatasetEntityJpaController datasetController = new DatasetEntityJpaController();
            List<DatasetEntity> datasetEntityList = datasetController.findDatasetEntityEntities(hash);
            DatasetEntity datasetEntity = null;
            // Check if we have any dataset with the same hash
            for (DatasetEntity existingDataset : datasetEntityList) {
                // The second check is necessary because two datasets with the same hash might have different data
                if (compareData(dataset.getData(), existingDataset.getDataBlob())) {
                    datasetEntity = existingDataset;
                    break;
                }
            }
            // If the dataset doesn't exist we create it
            if (datasetEntity == null) {
                datasetEntity = new DatasetEntity();
                datasetEntity.setHashKey(hash);
                datasetEntity.setDataBlob(dataset.getData());
                datasetController.create(datasetEntity);
            }
            if (optionKey == null)
                optionKey = "";
            TransactionController.getEntityManager().flush();
            datasetInfoEntity.getDatasetMap().put(optionKey, datasetEntity);
            if (datasetInfoEntity.getId() == null)
            datasetInfoController.create(datasetInfoEntity);
            else
                TransactionController.getEntityManager().merge(datasetInfoEntity);
            TransactionController.getEntityManager().flush();
            TransactionController.commitTransaction();
        } catch (Exception ex) {
            logger.error("Failed to save dataset", ex);
            TransactionController.rollbackTransaction();
            throw new PersistenceDeviceException(ex);
        }
    }

    @Override
    public void saveMultiDataset(Type type, DatasetInfo info, String xUnit, String yUnit,
            Map<String, Map<Double, Double>> optionList, Dataset.DataType dataType)
            throws NameExistsException, PersistenceDeviceException {
        // Check if the dataset exists in the database
        DatasetInfoEntityJpaController datasetInfoController = new DatasetInfoEntityJpaController();
        DatasetInfoEntity datasetInfoEntity = datasetInfoController.findDatasetEntity(type, info);
        if (datasetInfoEntity != null) {
            logger.info("Failed to save multi-dataset " + info + " because "
                        + "there is another dataset with the same name");
                throw new NameExistsException();
        }
        //Create the dataset info entity
        datasetInfoEntity = new DatasetInfoEntity();
        datasetInfoEntity.setName(info.getName());
        datasetInfoEntity.setNamespace(info.getNamespace());
        datasetInfoEntity.setDescription(info.getDescription());
        datasetInfoEntity.setType(type);
        datasetInfoEntity.setXUnit(xUnit);
        datasetInfoEntity.setYUnit(yUnit);
        datasetInfoEntity.setDataType(dataType);
        
        HashMap<String, DatasetEntity> optionMap = new HashMap<String, DatasetEntity>();
        // Begin a transaction so we can roll back everything if anything goes wrong
        TransactionController.beginTransaction();
        try {
            // Go through the datasets, create them and set them in the datasetToCopyInfoEntity
            for (Map.Entry<String, Map<Double, Double>> optionPair : optionList.entrySet()) {
                String option = optionPair.getKey();
                if (option == null || option.equals("")) {
                    logger.error("Failed to create multi-dataset " + info + " because of "
                            + "invalid given option key");
                    throw new PersistenceDeviceException("Invalid value of multi-dataset option key");
                }
                Map<Double, Double> data = optionPair.getValue();
                // To save space in the database we use the same entry in the DATASET
                // table if the data are the same. Here we do this check.
                Long hash = HashUtil.getHash(data);
                DatasetEntityJpaController datasetController = new DatasetEntityJpaController();
                List<DatasetEntity> datasetEntityList = datasetController.findDatasetEntityEntities(hash);
                DatasetEntity datasetEntity = null;
                // Check if we have any dataset with the same hash
                for (DatasetEntity existingDataset : datasetEntityList) {
                    // The second check is necessary because two datasets with the same hash might have different data
                    if (compareData(data, existingDataset.getDataBlob())) {
                        datasetEntity = existingDataset;
                        break;
                    }
                }
                // If the dataset doesn't exist we create it
                if (datasetEntity == null) {
                    datasetEntity = new DatasetEntity();
                    datasetEntity.setHashKey(hash);
                    datasetEntity.setDataBlob(data);
                    datasetController.create(datasetEntity);
                }
                optionMap.put(option, datasetEntity);
            }
            TransactionController.getEntityManager().flush();
            datasetInfoEntity.setDatasetMap(optionMap);
            datasetInfoController.create(datasetInfoEntity);
            TransactionController.commitTransaction();
        } catch (Exception ex) {
            logger.error("Failed to save multi-dataset", ex);
            TransactionController.rollbackTransaction();
            throw new PersistenceDeviceException(ex);
        }
    }

    @Override
    public void deleteDataset(Type type, DatasetInfo datasetInfo) throws NameNotFoundException, InUseException, PersistenceDeviceException {
        DatasetInfoEntityJpaController datasetInfoController = new DatasetInfoEntityJpaController();
        DatasetInfoEntity datasetInfoEntity = datasetInfoController.findDatasetEntity(type, datasetInfo);
        if (datasetInfoEntity == null) {
            throw new NameNotFoundException();
        }
        InstrumentEntityJpaController instrumentController = new InstrumentEntityJpaController();
        SiteEntityJpaController siteController = new SiteEntityJpaController();
        SourceEntityJpaController sourceController = new SourceEntityJpaController();
        ObsParamEntityJpaController obsParamController = new ObsParamEntityJpaController();
        List<String> usedBy = new ArrayList<String>();
        for (InstrumentEntity instrument : instrumentController.findInstrumentsUsingDataset(datasetInfoEntity)) {
            usedBy.add("Instrument: " + instrument.getName());
        }
        for (SiteEntity site : siteController.findSitesUsingDataset(datasetInfoEntity)) {
            usedBy.add("Site: " + site.getName());
        }
        for (SourceEntity source : sourceController.findSourcesUsingDataset(datasetInfoEntity)) {
            usedBy.add("Source: " + source.getName());
        }
        for (ObsParamEntity obsParam : obsParamController.findObsParamsUsingDataset(datasetInfoEntity)) {
            usedBy.add("ObsParam: " + obsParam.getName());
        }
        if (!usedBy.isEmpty()) {
            throw new InUseException(usedBy);
        }
        try {
            datasetInfoController.deleteDatasetInfo(datasetInfoEntity);
        } catch (Exception e) {
            throw new PersistenceDeviceException(e);
        }
    }

    private boolean compareData(Map<Double, Double> data1, Map<Double, Double> data2) {
        if (data1.size() != data2.size()) {
            return false;
        }
        for (Map.Entry<Double, Double> entry : data1.entrySet()) {
            Double x1 = entry.getKey();
            Double y1 = entry.getValue();
            Double y2 = data2.get(x1);
            if (y2 == null || y1 != y2) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void copyToNamespace(Type type, DatasetInfo datasetInfo, DatasetInfo newInfo) throws NameNotFoundException, NameExistsException, PersistenceDeviceException {
        DatasetInfoEntityJpaController controller = new DatasetInfoEntityJpaController();
        // First get the dataset we want to copy 
        DatasetInfoEntity datasetToCopyInfoEntity = controller.findDatasetEntity(type, datasetInfo);
        if (datasetToCopyInfoEntity == null) {
            logger.info("Failed to copy dataset " + datasetInfo + " because it "
                    + "wasn't found in the database");
            throw new NameNotFoundException("Failed to copy dataset " + datasetInfo +
                    " because it wasn't found in the database");
        }
        // Check that there is no other dataset with the same name in the destination namespace
        DatasetInfoEntity targetDatasetInfoEntity = controller.findDatasetEntity(type, newInfo);
        if (targetDatasetInfoEntity != null) {
            logger.info("Failed to copy dataset " + datasetInfo + " to " + newInfo + " because "
                        + "there is another dataset with the same name");
                throw new NameExistsException();
        }
        // Now if there is no dataset info for this dataset we create it
        targetDatasetInfoEntity = new DatasetInfoEntity();
        targetDatasetInfoEntity.setName(newInfo.getName());
        targetDatasetInfoEntity.setNamespace(newInfo.getNamespace());
        targetDatasetInfoEntity.setDescription(newInfo.getDescription());
        targetDatasetInfoEntity.setType(datasetToCopyInfoEntity.getType());
        targetDatasetInfoEntity.setXUnit(datasetToCopyInfoEntity.getXUnit());
        targetDatasetInfoEntity.setYUnit(datasetToCopyInfoEntity.getYUnit());
        targetDatasetInfoEntity.setDatasetMap(datasetToCopyInfoEntity.getDatasetMap());
        targetDatasetInfoEntity.setDataType(datasetToCopyInfoEntity.getDataType());
        // Save the dataset info
        controller.create(targetDatasetInfoEntity);
    }

}
