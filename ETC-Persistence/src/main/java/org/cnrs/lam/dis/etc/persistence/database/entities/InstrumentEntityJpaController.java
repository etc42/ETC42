/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.persistence.database.entities;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.cnrs.lam.dis.etc.datamodel.ComponentInfo;
import org.cnrs.lam.dis.etc.datamodel.Instrument;
import org.cnrs.lam.dis.etc.datamodel.Instrument.InstrumentType;
import org.cnrs.lam.dis.etc.datamodel.Instrument.PsfType;
import org.cnrs.lam.dis.etc.datamodel.Instrument.SpectralResolutionType;
import org.cnrs.lam.dis.etc.datamodel.Instrument.SpectrographType;
import org.cnrs.lam.dis.etc.persistence.database.entities.exceptions.NonexistentEntityException;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class InstrumentEntityJpaController {

    public void create(InstrumentEntity instrumentEntity) {
        EntityManager em = null;
        try {
            em = TransactionController.getEntityManager();
            TransactionController.beginTransaction();
            em.persist(instrumentEntity);
            TransactionController.commitTransaction();
            instrumentEntity.setModified(false);
        } catch (Exception ex) {
            TransactionController.rollbackTransaction();
        }
    }

    public void edit(InstrumentEntity instrumentEntity) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = TransactionController.getEntityManager();
            TransactionController.beginTransaction();
            instrumentEntity = em.merge(instrumentEntity);
            TransactionController.commitTransaction();
            instrumentEntity.setModified(false);
        } catch (Exception ex) {
            TransactionController.rollbackTransaction();
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = instrumentEntity.getId();
                if (findInstrumentEntity(id) == null) {
                    throw new NonexistentEntityException("The instrumentEntity with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = TransactionController.getEntityManager();
            TransactionController.beginTransaction();
            InstrumentEntity instrumentEntity;
            try {
                instrumentEntity = em.getReference(InstrumentEntity.class, id);
                instrumentEntity.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The instrumentEntity with id " + id + " no longer exists.", enfe);
            }
            em.remove(instrumentEntity);
            TransactionController.commitTransaction();
            instrumentEntity.setModified(true);
        } catch (Exception ex) {
            TransactionController.rollbackTransaction();
        }
    }

    public List<InstrumentEntity> findInstrumentEntityEntities() {
        return findInstrumentEntityEntities(true, -1, -1);
    }

    public List<InstrumentEntity> findInstrumentEntityEntities(int maxResults, int firstResult) {
        return findInstrumentEntityEntities(false, maxResults, firstResult);
    }

    private List<InstrumentEntity> findInstrumentEntityEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = TransactionController.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<InstrumentEntity> cq = cb.createQuery(InstrumentEntity.class);
        Root<InstrumentEntity> from = cq.from(InstrumentEntity.class);
        cq.select(cq.from(InstrumentEntity.class));
        cq.orderBy(cb.asc(from.get(InstrumentEntity_.name)));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public List<InstrumentEntity> findInstrumentsUsingDataset(DatasetInfoEntity datasetInfoEntity) {
        EntityManager em = TransactionController.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<InstrumentEntity> cq = cb.createQuery(InstrumentEntity.class);
        Root<InstrumentEntity> from = cq.from(InstrumentEntity.class);
        cq.where(cb.or(
                cb.equal(from.get(InstrumentEntity_.filterTransmissionEntity), datasetInfoEntity),
                cb.equal(from.get(InstrumentEntity_.fwhmEntity), datasetInfoEntity),
                cb.equal(from.get(InstrumentEntity_.spectralResolutionEntity), datasetInfoEntity),
                cb.equal(from.get(InstrumentEntity_.transmissionEntity), datasetInfoEntity),
                cb.equal(from.get(InstrumentEntity_.psfDoubleGaussianFwhm1Entity), datasetInfoEntity),
                cb.equal(from.get(InstrumentEntity_.psfDoubleGaussianFwhm2Entity), datasetInfoEntity),
                cb.equal(from.get(InstrumentEntity_.psfSizeFunctionEntity), datasetInfoEntity)
        ));
        TypedQuery<InstrumentEntity> query = em.createQuery(cq);
        return query.getResultList();
    }

    public InstrumentEntity findInstrumentEntity(ComponentInfo info) {
        EntityManager em = TransactionController.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<InstrumentEntity> cq = cb.createQuery(InstrumentEntity.class);
        Root<InstrumentEntity> from = cq.from(InstrumentEntity.class);
        cq.where(cb.equal(from.get(InstrumentEntity_.name), info.getName()));
        TypedQuery<InstrumentEntity> query = em.createQuery(cq);
        for (InstrumentEntity result : query.getResultList()) {
            result.setModified(false);
            return result;
        }
        return null;
    }

    public InstrumentEntity findInstrumentEntity(Long id) {
        EntityManager em = TransactionController.getEntityManager();
        return em.find(InstrumentEntity.class, id);
    }

    public int getInstrumentEntityCount() {
        EntityManager em = TransactionController.getEntityManager();
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<InstrumentEntity> rt = cq.from(InstrumentEntity.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public InstrumentEntity createNewUnpersistedInstrumentEntity(ComponentInfo newInstrumentInfo) {
        InstrumentEntity instrument = new InstrumentEntity();
        instrument.setName(newInstrumentInfo.getName());
        instrument.setDescription(newInstrumentInfo.getDescription());
        instrument.setInstrumentType(InstrumentType.IMAGING);
        instrument.setPsfType(PsfType.AUTO);
        instrument.setSpectrographType(SpectrographType.SLIT);
        instrument.setDarkUnit("e/pixel/s");
        instrument.setDiameterUnit("cm");
        instrument.setFiberDiameterUnit("arcsec");
        instrument.setPixelScaleUnit("arcsec/pixel");
        instrument.setRangeMaxUnit("\u00C5");
        instrument.setRangeMinUnit("\u00C5");
        instrument.setReadoutUnit("e/pixel");
        instrument.setSlitLengthUnit("arcsec");
        instrument.setSlitWidthUnit("arcsec");
        instrument.setRefWavelengthUnit("\u00C5");
        instrument.setDeltaLambdaPerPixelUnit("\u00C5/pixel");
        instrument.setSpectralResolutionType(SpectralResolutionType.CONSTANT_DELTA_LAMBDA);
        instrument.setFixedPsfSizeUnit("arcsec");
        instrument.setPsfSizeType(Instrument.PsfSizeType.AUTO);
        instrument.setModified(false);
        return instrument;
    }

}
