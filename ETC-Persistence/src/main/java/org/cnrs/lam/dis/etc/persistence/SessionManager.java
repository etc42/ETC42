/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.persistence;

import java.util.List;
import org.cnrs.lam.dis.etc.datamodel.ComponentInfo;
import org.cnrs.lam.dis.etc.datamodel.Instrument;
import org.cnrs.lam.dis.etc.datamodel.ObsParam;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.cnrs.lam.dis.etc.datamodel.Site;
import org.cnrs.lam.dis.etc.datamodel.Source;

/**
 *
 * @author Nikolaos Apostolakos
 */
public interface SessionManager {

    //TODO: this needs to be defined when session is shorted out
    /**
     * To be defined....
     * @return
     */
    public List<String> getAvailableSessionList() throws PersistenceDeviceException;

    /**
     * Returns a list of the available instruments for loading. If there are no
     * available instruments it returns an empty list.
     * @return A list of the available instruments for loading
     * @throws PersistenceDeviceException
     */
    public List<ComponentInfo> getAvailableInstrumentList() throws PersistenceDeviceException;

    /**
     * Returns a list of the available sites for loading. If there are no
     * available sites it returns an empty list.
     * @return A list of the available sutes for loading
     * @throws PersistenceDeviceException
     */
    public List<ComponentInfo> getAvailableSiteList() throws PersistenceDeviceException;

    /**
     * Returns a list of the available sources for loading. If there are no
     * available sources it returns an empty list.
     * @return A list of the available sources for loading
     * @throws PersistenceDeviceException
     */
    public List<ComponentInfo> getAvailableSourceList() throws PersistenceDeviceException;

    /**
     * Returns a list of the available observing parameters for loading. If there
     * are no available observing parameters it returns an empty list.
     * @return A list of the available observing parameters for loading
     * @throws PersistenceDeviceException
     */
    public List<ComponentInfo> getAvailableObsParamList() throws PersistenceDeviceException;

    /**
     * Checks if the given instrument contains the same information as its
     * representation in the persistence device or not. If there is no such an
     * instrument in the persistence device at all, this method will return
     * false. Implementations of this method are encuraged to implement it in
     * a way to use as less accesses to the persistence device it self as
     * possible.
     * @param instrument The isntrument to check
     * @return true if the given instrument contains the same information as
     * its representation in the persistence device, false if it does not or
     * if there is no such instrument in the persistence device.
     * @throws PersistenceDeviceException
     */
    public boolean isInstrumentPersisted(Instrument instrument) throws PersistenceDeviceException;

    public boolean isInstrumentInDatabase(Instrument instrument);

    /**
     * Checks if the given source contains the same information as its
     * representation in the persistence device or not. If there is no such a
     * source in the persistence device at all, this method will return
     * false. Implementations of this method are encuraged to implement it in
     * a way to use as less accesses to the persistence device it self as
     * possible.
     * @param source The source to check
     * @return true if the given source contains the same information as
     * its representation in the persistence device, false if it does not or
     * if there is no such source in the persistence device.
     * @throws PersistenceDeviceException
     */
    public boolean isSourcePersisted(Source source) throws PersistenceDeviceException;

    public boolean isSourceInDatabase(Source source);

    /**
     * Checks if the given site contains the same information as its
     * representation in the persistence device or not. If there is no such a
     * site in the persistence device at all, this method will return
     * false. Implementations of this method are encuraged to implement it in
     * a way to use as less accesses to the persistence device it self as
     * possible.
     * @param site The site to check
     * @return true if the given site contains the same information as
     * its representation in the persistence device, false if it does not or
     * if there is no such site in the persistence device.
     * @throws PersistenceDeviceException
     */
    public boolean isSitePersisted(Site site) throws PersistenceDeviceException;

    public boolean isSiteInDatabase(Site site);

    /**
     * Checks if the given observing parameters contains the same information as its
     * representation in the persistence device or not. If there is no such an
     * observing parameters in the persistence device at all, this method will return
     * false. Implementations of this method are encuraged to implement it in
     * a way to use as less accesses to the persistence device it self as
     * possible.
     * @param obsParam The observing parameters to check
     * @return true if the given observing parameters contains the same information as
     * its representation in the persistence device, false if it does not or
     * if there is no such observing parameters in the persistence device.
     * @throws PersistenceDeviceException
     */
    public boolean isObsParamPersisted(ObsParam obsParam) throws PersistenceDeviceException;

    public boolean isObsParamInDatabase(ObsParam obsParam);

    //TODO: this needs to be defined when session is shorted out
    /**
     * To be defined....
     * @return
     */
    public Session loadSession(String name) throws NameNotFoundException;

    /**
     * Loads an instrument from the persistence device. If the instrument does
     * not exist in the persistent device a <code>NameNotFoundException</code>
     * is thrown.
     * @param instrumentInfo The instrument to be loaded. Only the name is used.
     * @return The Instrument instance with the information stored in the
     * persistence device
     * @throws NameNotFoundException If there is no instrument with this name in
     * the persistence device
     * @throws PersistenceDeviceException If there is any error during accessing the
     * persistence device
     */
    public Instrument loadInstrument(ComponentInfo instrumentInfo)
            throws NameNotFoundException, PersistenceDeviceException;

    /**
     * Loads a siet from the persistence device. If the site does
     * not exist in the persistent device a <code>NameNotFoundException</code>
     * is thrown.
     * @param siteInfo The site to be loaded. Only the name is used.
     * @return The Site instance with the information stored in the
     * persistence device
     * @throws NameNotFoundException If there is no site with this name in
     * the persistence device
     * @throws PersistenceDeviceException If there is any error during accessing the
     * persistence device
     */
    public Site loadSite(ComponentInfo siteInfo) throws NameNotFoundException,
            PersistenceDeviceException;

    /**
     * Loads a source from the persistence device. If the source does
     * not exist in the persistent device a <code>NameNotFoundException</code>
     * is thrown.
     * @param sourceInfo The source to be loaded. Only the name is used.
     * @return The Source instance with the information stored in the
     * persistence device
     * @throws NameNotFoundException If there is no source with this name in
     * the persistence device
     * @throws PersistenceDeviceException If there is any error during accessing the
     * persistence device
     */
    public Source loadSource(ComponentInfo sourceInfo) throws NameNotFoundException,
            PersistenceDeviceException;

    /**
     * Loads an observing parameters from the persistence device. If the objserving parameters does
     * not exist in the persistent device a <code>NameNotFoundException</code>
     * is thrown.
     * @param obsParamInfo The observing parameters to be loaded. Only the name is used.
     * @return The ObsParam instance with the information stored in the
     * persistence device
     * @throws NameNotFoundException If there is no observing parameters with this name in
     * the persistence device
     * @throws PersistenceDeviceException If there is any error during accessing the
     * persistence device
     */
    public ObsParam loadObsParam(ComponentInfo obsParamInfo)
            throws NameNotFoundException, PersistenceDeviceException;

    //TODO: this needs to be defined when session is shorted out
    /**
     * To be defined....
     * @return
     */
    public boolean saveSession(Session session);

    /**
     * Persists the given instrument in the persistence device. If the instrument
     * already exists, the information stored is updated with the one of the
     * given instrument. If it does not exist, a new instrument is persisted
     * with this name.
     * @param instrument The instrument to save
     * @throws PersistenceDeviceException If there is any error during storring the
     * data in the persistence device
     * @throws NameExistsException If a new instrument must be created, but there
     * is apready one with this name
     */
    public void saveInstrument(Instrument instrument) throws PersistenceDeviceException, NameExistsException;

    /**
     * Persists the given site in the persistence device. If the site
     * already exists, the information stored is updated with the one of the
     * given site. if it does not exist, a new site is persisted
     * with this name.
     * @param site The site to save
     * @throws PersistenceDeviceException If there is any error during storring the
     * data in the persistence device
     * @throws NameExistsException If a new site must be created, but there
     * is apready one with this name
     */
    public void saveSite(Site site) throws PersistenceDeviceException, NameExistsException;

    /**
     * Persists the given source in the persistence device. If the source
     * already exists, the information stored is updated with the one of the
     * given source. if it does not exist, a new source is persisted
     * with this name.
     * @param source The source to save
     * @throws PersistenceDeviceException If there is any error during storring the
     * data in the persistence device
     * @throws NameExistsException If a new source must be created, but there
     * is apready one with this name
     */
    public void saveSource(Source source) throws PersistenceDeviceException, NameExistsException;

    /**
     * Persists the given obs param in the persistence device. If the obs param
     * already exists, the information stored is updated with the one of the
     * given obs param. if it does not exist, a new obs param is persisted
     * with this name.
     * @param obsParam The observing parameters to save
     * @throws PersistenceDeviceException If there is any error during storring the
     * data in the persistence device
     * @throws NameExistsException If a new obs param must be created, but there
     * is apready one with this name
     */
    public void saveObsParam(ObsParam obsParam) throws PersistenceDeviceException, NameExistsException;

    //TODO: this needs to be defined when session is shorted out
    /**
     * To be defined....
     * @return
     */
    public void saveSessionAs(Session session, boolean override)
            throws NameExistsException, PersistenceDeviceException;

    /**
     * Creates a new instrument in the persistence device with the name and
     * description given by the newInfo parameter and the rest information from
     * the instrument parameter.This method should also take actions for
     * setting the namespaces of any involved datasets. If there is already
     * an instrument with the given name in the persistence device, then nothing
     * is done and a <code>NameExistsException</code> is thrown. The method
     * returns an object representing the new instrument. The data stored for
     * the old instrument are not altered in any way.
     * @param instrument The instrument to get the information from
     * @param newInfo The name and description of the new instrument
     * @return An object representing the new instrument
     * @throws NameExistsException If there is already an instrument with the
     * given name
     * @throws PersistenceDeviceException If there was any error during storring the
     * new instrument
     */
    public Instrument saveInstrumentAs(Instrument instrument, ComponentInfo newInfo)
            throws NameExistsException, PersistenceDeviceException;

    /**
     * Creates a new site in the persistence device with the name and
     * description given by the newInfo parameter and the rest information from
     * the site parameter.This method should also take actions for
     * setting the namespaces of any involved datasets. If there is already
     * a site with the given name in the persistence device, then nothing
     * is done and a <code>NameExistsException</code> is thrown. The method
     * returns an object representing the new site. The data stored for the
     * old site are not altered in any way.
     * @param site The site to get the information from
     * @param newInfo The name and description of the new site
     * @return An object representing the new site
     * @throws NameExistsException If there is already a site with the
     * given name
     * @throws PersistenceDeviceException If there was any error during storring the
     * new site
     */
    public Site saveSiteAs(Site site, ComponentInfo newInfo)
            throws NameExistsException, PersistenceDeviceException;

    /**
     * Creates a new source in the persistence device with the name and
     * description given by the newInfo parameter and the rest information from
     * the source parameter.This method should also take actions for
     * setting the namespaces of any involved datasets. If there is already
     * a source with the given name in the persistence device, then nothing
     * is done and a <code>NameExistsException</code> is thrown. The method
     * returns an object representing the new source. The data stored for
     * the old source are not altered in any way.
     * @param source The source to get the information from
     * @param newInfo The name and description of the new source
     * @return An object representing the new source
     * @throws NameExistsException If there is already a source with the
     * given name
     * @throws PersistenceDeviceException If there was any error during storring the
     * new source
     */
    public Source saveSourceAs(Source source, ComponentInfo newInfo)
            throws NameExistsException, PersistenceDeviceException;

    /**
     * Creates a new obs param in the persistence device with the name and
     * description given by the newInfo parameter and the rest information from
     * the obsParam parameter.This method should also take actions for
     * setting the namespaces of any involved datasets. If there is already
     * an obs param with the given name in the persistence device, then nothing
     * is done and a <code>NameExistsException</code> is thrown. The method
     * returns an object representing the new obs param. The data stored for
     * the old obs param are not altered in any way.
     * @param obsParam The observing parameters to get the information from
     * @param newInfo The name and description of the new observing parameters
     * @return An object representing the new observing parameters
     * @throws NameExistsException If there is already an obs param with the
     * given name
     * @throws PersistenceDeviceException If there was any error during storring the
     * new observing parameters
     */
    public ObsParam saveObsParamAs(ObsParam obsParam, ComponentInfo newInfo)
            throws NameExistsException, PersistenceDeviceException;

    //TODO: this needs to be defined when session is shorted out
    /**
     * To be defined....
     * @return
     */
    public Session createNewSession(String name) throws NameExistsException;

    /**
     * Creates a new instrument instance with the given name and description.
     * This new instrument is NOT persisted in the persistence device and it
     * must be saved with the saveInstrument method to be available if the
     * system restarts. If there is already an instrument with the given
     * name an exception is thrown.
     * @param newInstrumentInfo The name and description of the new instrument
     * @return An object representing the new instrument
     * @throws NameExistsException If there is already an instrument with the
     * given name
     * @throws PersistenceDeviceException If there is any problem with accessing
     * the persistence device
     */
    public Instrument createNewInstrument(ComponentInfo newInstrumentInfo)
            throws NameExistsException, PersistenceDeviceException;

    /**
     * Creates a new site instance with the given name and description.
     * This new site is NOT persisted in the persistence device and it
     * must be saved with the saveSite method to be available if the
     * system restarts. If there is already an site with the given
     * name an exception is thrown.
     * @param newSiteInfo The name and description of the new site
     * @return An object representing the new site
     * @throws NameExistsException If there is already a site with the
     * given name
     * @throws PersistenceDeviceException If there is any problem with accessing
     * the persistence device
     */
    public Site createNewSite(ComponentInfo newSiteInfo)
            throws NameExistsException, PersistenceDeviceException;

    /**
     * Creates a new source instance with the given name and description.
     * This new source is NOT persisted in the persistence device and it
     * must be saved with the saveSource method to be available if the
     * system restarts. If there is already an source with the given
     * name an exception is thrown.
     * @param newSourceInfo The name and description of the new source
     * @return An object representing the new source
     * @throws NameExistsException If there is already a source with the
     * given name
     * @throws PersistenceDeviceException If there is any problem with accessing
     * the persistence device
     */
    public Source createNewSource(ComponentInfo newSourceInfo)
            throws NameExistsException, PersistenceDeviceException;

    /**
     * Creates a new obs param instance with the given name and description.
     * This new obs param is NOT persisted in the persistence device and it
     * must be saved with the saveObsParam method to be available if the
     * system restarts. If there is already an obs param with the given
     * name an exception is thrown.
     * @param newObsParamInfo The name and description of the new obs param
     * @return An object representing the new obs param
     * @throws NameExistsException If there is already an obs param with the
     * given name
     * @throws PersistenceDeviceException If there is any problem with accessing
     * the persistence device
     */
    public ObsParam createNewObsParam(ComponentInfo newObsParamInfo)
            throws NameExistsException, PersistenceDeviceException;

    //TODO: this needs to be defined when session is shorted out
    /**
     * To be defined....
     * @return
     */
    public boolean deleteSession(String name) throws NameNotFoundException;

    /**
     * Deletes an instrument from the peristence device. Also any datasets
     * under the instruments namespace are deleted. The instrument cannot be
     * deleted if it is used by any session.
     * @param instrumentInfo The name ofthe instrument to delete
     * @throws NameNotFoundException If there is no instrument with this name
     * persisted in the persistence device
     * @throws InUseException If the instrument is used by a session
     * @throws PersistenceDeviceException If there is any problem with accessing
     * the persistence device
     */
    public void deleteInstrument(ComponentInfo instrumentInfo)
            throws NameNotFoundException, InUseException, PersistenceDeviceException;

    /**
     * Deletes a site from the peristence device. Also any datasets
     * under the sites namespace are deleted. The site cannot be
     * deleted if it is used by any session.
     * @param siteInfo The name ofthe site to delete
     * @throws NameNotFoundException If there is no site with this name
     * persisted in the persistence device
     * @throws InUseException If the site is used by a session
     * @throws PersistenceDeviceException If there is any problem with accessing
     * the persistence device
     */
    public void deleteSite(ComponentInfo siteInfo) throws NameNotFoundException,
            InUseException, PersistenceDeviceException;

    /**
     * Deletes an source from the peristence device. Also any datasets
     * under the sources namespace are deleted. The source cannot be
     * deleted if it is used by some session.
     * @param sourceInfo The name ofthe source to delete
     * @throws NameNotFoundException If there is no source with this name
     * persisted in the persistence device
     * @throws InUseException If the source is used by a session
     * @throws PersistenceDeviceException If there is any problem with accessing
     * the persistence device
     */
    public void deleteSource(ComponentInfo sourceInfo) throws NameNotFoundException,
            InUseException, PersistenceDeviceException;

    /**
     * Deletes an obs param from the peristence device. Also any datasets
     * under the obs params namespace are deleted. The obs param cannot be
     * deleted if it is used by some session.
     * @param obsParamInfo The name ofthe obs param to delete
     * @throws NameNotFoundException If there is no obs param with this name
     * persisted in the persistence device
     * @throws InUseException If the obs param is used by a session
     * @throws PersistenceDeviceException If there is any problem with accessing
     * the persistence device
     */
    public void deleteObsParam(ComponentInfo obsParamInfo) throws NameNotFoundException,
            InUseException, PersistenceDeviceException;

    public boolean checkConnection();
}
