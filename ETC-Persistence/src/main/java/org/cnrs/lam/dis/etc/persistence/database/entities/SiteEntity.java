/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.persistence.database.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.cnrs.lam.dis.etc.datamodel.ComponentInfo;
import org.cnrs.lam.dis.etc.datamodel.Dataset.Type;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.cnrs.lam.dis.etc.datamodel.Site;
import org.cnrs.lam.dis.etc.datamodel.Site.LocationType;

/**
 *
 * @author Nikolaos Apostolakos
 */
@Entity
@Table(name = "SITE")
@NamedQueries({
    @NamedQuery(name = "SiteEntity.findAll", query = "SELECT s FROM SiteEntity s")})
public class SiteEntity implements Serializable, Site {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Basic(optional = false)
    @Column(name = "NAME")
    private String name;
    @Column(name = "DESCRIPTION")
    private String description;
    @Basic(optional = false)
    @Column(name = "LOCATION_TYPE")
    @Enumerated(EnumType.STRING)
    private LocationType locationType;
    @Column(name = "AIR_MASS")
    private double airMass;
    @Column(name = "SEEING")
    private double seeing;
    @Column(name = "SEEING_UNIT")
    private String seeingUnit;
    @Column(name = "ZODIACAL_CONTRIBUTING")
    private boolean zodiacalContributing;
    @Column(name = "GALACTIC_CONTRIBUTING")
    private boolean galacticContributing;
    @Column(name = "SEEING_LIMITED")
    private boolean seeingLimited;
    @JoinColumn(name = "GALACTIC_PROFILE", referencedColumnName = "ID")
    @ManyToOne
    private DatasetInfoEntity galacticProfileEntity;
    @JoinColumn(name = "ZODIACAL_PROFILE", referencedColumnName = "ID")
    @ManyToOne
    private DatasetInfoEntity zodiacalProfileEntity;
    @JoinColumn(name = "SKY_EXTINCTION", referencedColumnName = "ID")
    @ManyToOne
    private DatasetInfoEntity skyExtinctionEntity;
    @JoinColumn(name = "SKY_ABSORPTION", referencedColumnName = "ID")
    @ManyToOne
    private DatasetInfoEntity skyAbsorptionEntity;
    @JoinColumn(name = "SKY_EMISSION", referencedColumnName = "ID")
    @ManyToOne
    private DatasetInfoEntity skyEmissionEntity;
    @Column(name = "SKY_EMISSION_SELECTED_OPTION")
    private String skyEmissionSelectedOption;
    @JoinColumn(name = "ATMOSPHERIC_TRANSMISSION", referencedColumnName = "ID")
    @ManyToOne
    private DatasetInfoEntity atmosphericTransmissionEntity;
    @Basic(optional = false)
    @Column(name = "ATMOSPHERIC_TRANSMISSION_TYPE")
    @Enumerated(EnumType.STRING)
    private AtmosphericTransmissionType atmosphericTransmissionType;

    private transient boolean modified = false;

    public boolean isModified() {
        return modified;
    }

    void setModified(boolean modified) {
        this.modified = modified;
    }

    public SiteEntity() {
    }

    public SiteEntity(Long id) {
        this.id = id;
    }

    public SiteEntity(Long id, String name, LocationType locationType) {
        this.id = id;
        this.name = name;
        this.locationType = locationType;
    }

    public SiteEntity(String name, LocationType locationType) {
        this.name = name;
        this.locationType = locationType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public LocationType getLocationType() {
        return locationType;
    }

    @Override
    public void setLocationType(LocationType locationType) {
        if (this.locationType != locationType)
            modified = true;
        this.locationType = locationType;
    }

    @Override
    public double getAirMass() {
        return airMass;
    }

    @Override
    public void setAirMass(double airMass) {
        if (this.airMass != airMass)
            modified = true;
        this.airMass = airMass;
    }

    @Override
    public double getSeeing() {
        return seeing;
    }

    @Override
    public void setSeeing(double seeing) {
        if (this.seeing != seeing)
            modified = true;
        this.seeing = seeing;
    }

    @Override
    public String getSeeingUnit() {
        return seeingUnit;
    }

    public void setSeeingUnit(String seeingUnit) {
        if (this.seeingUnit != null && !this.seeingUnit.equals(seeingUnit))
            modified = true;
        this.seeingUnit = seeingUnit;
    }

    @Override
    public boolean isZodiacalContributing() {
        return zodiacalContributing;
    }

    @Override
    public void setZodiacalContributing(boolean zodiacalContributing) {
        if (this.zodiacalContributing != zodiacalContributing)
            modified = true;
        this.zodiacalContributing = zodiacalContributing;
    }

    @Override
    public boolean isGalacticContributing() {
        return galacticContributing;
    }

    @Override
    public void setGalacticContributing(boolean galacticContributing) {
        if (this.galacticContributing != galacticContributing)
            modified = true;
        this.galacticContributing = galacticContributing;
    }
    
    @Override
    public boolean getSeeingLimited() {
        return seeingLimited;
    }
    
    @Override
    public void setSeeingLimited(boolean seeingLimited) {
        if (this.seeingLimited != seeingLimited)
            modified = true;
        this.seeingLimited = seeingLimited;
    }
    
    public DatasetInfoEntity getGalacticProfileEntity() {
        return galacticProfileEntity;
    }

    public void setGalacticProfileEntity(DatasetInfoEntity galacticProfileEntity) {
        this.galacticProfileEntity = galacticProfileEntity;
    }

    public DatasetInfoEntity getSkyAbsorptionEntity() {
        return skyAbsorptionEntity;
    }

    public void setSkyAbsorptionEntity(DatasetInfoEntity skyAbsorptionEntity) {
        this.skyAbsorptionEntity = skyAbsorptionEntity;
    }

    public DatasetInfoEntity getSkyEmissionEntity() {
        return skyEmissionEntity;
    }

    public void setSkyEmissionEntity(DatasetInfoEntity skyEmissionEntity) {
        this.skyEmissionEntity = skyEmissionEntity;
    }

    public DatasetInfoEntity getSkyExtinctionEntity() {
        return skyExtinctionEntity;
    }

    public void setSkyExtinctionEntity(DatasetInfoEntity skyExtinctionEntity) {
        this.skyExtinctionEntity = skyExtinctionEntity;
    }

    public DatasetInfoEntity getZodiacalProfileEntity() {
        return zodiacalProfileEntity;
    }

    public void setZodiacalProfileEntity(DatasetInfoEntity zodiacalProfileEntity) {
        this.zodiacalProfileEntity = zodiacalProfileEntity;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SiteEntity)) {
            return false;
        }
        SiteEntity other = (SiteEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.cnrs.lam.dis.etc.persistence.database.entities.SiteEntity[id=" + id + "]";
    }

    @Override
    public ComponentInfo getInfo() {
        return new ComponentInfo(getName(), getDescription());
    }

    @Override
    public DatasetInfo getSkyEmission() {
        if (getSkyEmissionEntity() == null)
            return null;
        List<String> optionList = new ArrayList<String>(getSkyEmissionEntity().getDatasetMap().keySet());
        return new DatasetInfo(getSkyEmissionEntity().getName(), getSkyEmissionEntity().getNamespace(), getSkyEmissionEntity().getDescription(), optionList);
    }

    @Override
    public void setSkyEmission(DatasetInfo skyEmission) {
        if ((this.skyEmissionEntity == null && skyEmission != null) ||
                (this.skyEmissionEntity != null && skyEmission == null) ||
                (this.skyEmissionEntity != null && skyEmission != null &&
                    !(this.skyEmissionEntity.getName().equals(skyEmission.getName()) &&
                        (this.skyEmissionEntity.getNamespace() == null ?
                            skyEmission.getNamespace() == null :
                            this.skyEmissionEntity.getNamespace().equals(skyEmission.getNamespace())
                        ))
                ))
            modified = true;
        if (skyEmission == null) {
            setSkyEmissionEntity(null);
            setSkyEmissionSelectedOption(null);
            return;
        }
        DatasetInfoEntityJpaController controller = new DatasetInfoEntityJpaController();
        setSkyEmissionEntity(controller.findDatasetEntity(Type.SKY_EMISSION, skyEmission));
        setSkyEmissionSelectedOption(getSkyEmissionEntity().getDatasetMap().keySet().iterator().next());
    }

    @Override
    public String getSkyEmissionSelectedOption() {
        return skyEmissionSelectedOption;
    }

    @Override
    public void setSkyEmissionSelectedOption(String skyEmissionSelectedOption) {
        if (this.skyEmissionSelectedOption != null && !this.skyEmissionSelectedOption.equals(skyEmissionSelectedOption))
            modified = true;
        this.skyEmissionSelectedOption = skyEmissionSelectedOption;
    }

    @Override
    public DatasetInfo getSkyAbsorption() {
        if (getSkyAbsorptionEntity() == null)
            return null;
        return new DatasetInfo(getSkyAbsorptionEntity().getName(), getSkyAbsorptionEntity().getNamespace(), getSkyAbsorptionEntity().getDescription());
    }

    @Override
    public void setSkyAbsorption(DatasetInfo skyAbsorption) {
        if ((this.skyAbsorptionEntity == null && skyAbsorption != null) ||
                (this.skyAbsorptionEntity != null && skyAbsorption == null) ||
                (this.skyAbsorptionEntity != null && skyAbsorption != null &&
                    !(this.skyAbsorptionEntity.getName().equals(skyAbsorption.getName()) &&
                        (this.skyAbsorptionEntity.getNamespace() == null ?
                            skyAbsorption.getNamespace() == null :
                            this.skyAbsorptionEntity.getNamespace().equals(skyAbsorption.getNamespace())
                        ))
                ))
            modified = true;
        if (skyAbsorption == null) {
            setSkyAbsorptionEntity(null);
            return;
        }
        DatasetInfoEntityJpaController controller = new DatasetInfoEntityJpaController();
        setSkyAbsorptionEntity(controller.findDatasetEntity(Type.SKY_ABSORPTION, skyAbsorption));
    }

    @Override
    public DatasetInfo getSkyExtinction() {
        if (getSkyExtinctionEntity() == null)
            return null;
        return new DatasetInfo(getSkyExtinctionEntity().getName(), getSkyExtinctionEntity().getNamespace(), getSkyExtinctionEntity().getDescription());
    }

    @Override
    public void setSkyExtinction(DatasetInfo skyExtinction) {
        if ((this.skyExtinctionEntity == null && skyExtinction != null) ||
                (this.skyExtinctionEntity != null && skyExtinction == null) ||
                (this.skyExtinctionEntity != null && skyExtinction != null &&
                    !(this.skyExtinctionEntity.getName().equals(skyExtinction.getName()) &&
                        (this.skyExtinctionEntity.getNamespace() == null ?
                            skyExtinction.getNamespace() == null :
                            this.skyExtinctionEntity.getNamespace().equals(skyExtinction.getNamespace())
                        ))
                ))
            modified = true;
        if (skyExtinction ==null) {
            setSkyExtinctionEntity(null);
            return;
        }
        DatasetInfoEntityJpaController controller = new DatasetInfoEntityJpaController();
        setSkyExtinctionEntity(controller.findDatasetEntity(Type.SKY_EXTINCTION, skyExtinction));
    }

    @Override
    public DatasetInfo getGalacticProfile() {
        if (getGalacticProfileEntity() == null)
            return null;
        return new DatasetInfo(getGalacticProfileEntity().getName(), getGalacticProfileEntity().getNamespace(), getGalacticProfileEntity().getDescription());
    }

    @Override
    public void setGalacticProfile(DatasetInfo profile) {
        if ((this.galacticProfileEntity == null && profile != null) ||
                (this.galacticProfileEntity != null && profile == null) ||
                (this.galacticProfileEntity != null && profile != null &&
                    !(this.galacticProfileEntity.getName().equals(profile.getName()) &&
                        (this.galacticProfileEntity.getNamespace() == null ?
                            profile.getNamespace() == null :
                            this.galacticProfileEntity.getNamespace().equals(profile.getNamespace())
                        ))
                ))
            modified = true;
        if (profile == null) {
            setGalacticProfileEntity(null);
            return;
        }
        DatasetInfoEntityJpaController controller = new DatasetInfoEntityJpaController();
        setGalacticProfileEntity(controller.findDatasetEntity(Type.GALACTIC, profile));
    }

    @Override
    public DatasetInfo getZodiacalProfile() {
        if(getZodiacalProfileEntity() == null)
            return null;
        return new DatasetInfo(getZodiacalProfileEntity().getName(), getZodiacalProfileEntity().getNamespace(), getZodiacalProfileEntity().getDescription());
    }

    @Override
    public void setZodiacalProfile(DatasetInfo profile) {
        if ((this.zodiacalProfileEntity == null && profile != null) ||
                (this.zodiacalProfileEntity != null && profile == null) ||
                (this.zodiacalProfileEntity != null && profile != null &&
                    !(this.zodiacalProfileEntity.getName().equals(profile.getName()) &&
                        (this.zodiacalProfileEntity.getNamespace() == null ?
                            profile.getNamespace() == null :
                            this.zodiacalProfileEntity.getNamespace().equals(profile.getNamespace())
                        ))
                ))
            modified = true;
        if (profile == null) {
            setZodiacalProfileEntity(null);
            return;
        }
        DatasetInfoEntityJpaController controller = new DatasetInfoEntityJpaController();
        setZodiacalProfileEntity(controller.findDatasetEntity(Type.ZODIACAL, profile));
    }

    public DatasetInfoEntity getAtmosphericTransmissionEntity() {
        return atmosphericTransmissionEntity;
    }

    public void setAtmosphericTransmissionEntity(DatasetInfoEntity atmosphericTransmissionEntity) {
        this.atmosphericTransmissionEntity = atmosphericTransmissionEntity;
    }

    @Override
    public DatasetInfo getAtmosphericTransmission() {
        if(getAtmosphericTransmissionEntity() == null)
            return null;
        return new DatasetInfo(getAtmosphericTransmissionEntity().getName(), getAtmosphericTransmissionEntity().getNamespace(), getAtmosphericTransmissionEntity().getDescription());
    }

    @Override
    public void setAtmosphericTransmission(DatasetInfo profile) {
        if ((this.atmosphericTransmissionEntity == null && profile != null) ||
                (this.atmosphericTransmissionEntity != null && profile == null) ||
                (this.atmosphericTransmissionEntity != null && profile != null &&
                    !(this.atmosphericTransmissionEntity.getName().equals(profile.getName()) &&
                        (this.atmosphericTransmissionEntity.getNamespace() == null ?
                            profile.getNamespace() == null :
                            this.atmosphericTransmissionEntity.getNamespace().equals(profile.getNamespace())
                        ))
                ))
            modified = true;
        if (profile == null) {
            setAtmosphericTransmissionEntity(null);
            return;
        }
        DatasetInfoEntityJpaController controller = new DatasetInfoEntityJpaController();
        setAtmosphericTransmissionEntity(controller.findDatasetEntity(Type.ATMOSPHERIC_TRANSMISSION, profile));
    }

    @Override
    public AtmosphericTransmissionType getAtmosphericTransmissionType() {
        return atmosphericTransmissionType;
    }

    @Override
    public void setAtmosphericTransmissionType(AtmosphericTransmissionType atmosphericTransmissionType) {
        if (this.atmosphericTransmissionType != atmosphericTransmissionType)
            modified = true;
        this.atmosphericTransmissionType = atmosphericTransmissionType;
    }

}
