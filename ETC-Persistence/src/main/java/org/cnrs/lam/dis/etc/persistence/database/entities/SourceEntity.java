/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.persistence.database.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.cnrs.lam.dis.etc.datamodel.ComponentInfo;
import org.cnrs.lam.dis.etc.datamodel.Dataset.Type;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.cnrs.lam.dis.etc.datamodel.Source;
import org.cnrs.lam.dis.etc.datamodel.Source.SpatialDistributionType;
import org.cnrs.lam.dis.etc.datamodel.Source.SpectralDistributionType;
import org.cnrs.lam.dis.etc.datamodel.Source.SurfaceBrightnessProfile;

/**
 *
 * @author Nikolaos Apostolakos
 */
@Entity
@Table(name = "SOURCE")
@NamedQueries({
    @NamedQuery(name = "SourceEntity.findAll", query = "SELECT s FROM SourceEntity s")})
public class SourceEntity implements Serializable, Source {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Basic(optional = false)
    @Column(name = "NAME")
    private String name;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "MAGNITUDE_WAVELENGTH")
    private double magnitudeWavelength;
    @Column(name = "MAGNITUDE_WAVELENGTH_UNIT")
    private String magnitudeWavelengthUnit;
    @Column(name = "EMISSION_LINE_WAVELENGTH")
    private double emissionLineWavelength;
    @Column(name = "EMISSION_LINE_WAVELENGTH_UNIT")
    private String emissionLineWavelengthUnit;
    @Column(name = "EMISSION_LINE_FWHM")
    private double emissionLineFwhm;
    @Column(name = "EMISSION_LINE_FWHM_UNIT")
    private String emissionLineFwhmUnit;
    @Basic(optional = false)
    @Column(name = "EXTENDED_SOURCE_PROFILE")
    @Enumerated(EnumType.STRING)
    private SurfaceBrightnessProfile extendedSourceProfile;
    @Column(name = "EXTENDED_SOURCE_RADIUS")
    private double extendedSourceRadius;
    @Column(name = "EXTENDED_SOURCE_RADIUS_UNIT")
    private String extendedSourceRadiusUnit;
    @Column(name = "MAGNITUDE")
    private double magnitude;
    @Column(name = "REDSHIFT")
    private double redshift;
    @Column(name = "TEMPERATURE")
    private double temperature;
    @Column(name = "TEMPERATURE_UNIT")
    private String temperatureUnit;
    @Basic(optional = false)
    @Column(name = "SPATIAL_DISTRIBUTION_TYPE")
    @Enumerated(EnumType.STRING)
    private SpatialDistributionType spatialDistributionType;
    @Basic(optional = false)
    @Column(name = "SPECTRAL_DISTRIBUTION_TYPE")
    @Enumerated(EnumType.STRING)
    private SpectralDistributionType spectralDistributionType;
    @JoinColumn(name = "SPECTRAL_DISTRIBUTION_TEMPLATE", referencedColumnName = "ID")
    @ManyToOne
    private DatasetInfoEntity spectralDistributionTemplateEntity;
    @Column(name = "EMISSION_LINE_FLUX")
    private double emissionLineFlux;
    @Column(name = "EMISSION_LINE_FLUX_UNIT")
    private String emissionLineFluxUnit;
    @Basic(optional = false)
    @Column(name = "EXTENDED_MAGNITUDE_TYPE")
    @Enumerated(EnumType.STRING)
    private ExtendedMagnitudeType extendedMagnitudeType;

    private transient boolean modified = false;

    public boolean isModified() {
        return modified;
    }

    void setModified(boolean modified) {
        this.modified = modified;
    }

    public SourceEntity() {
    }

    public SourceEntity(Long id) {
        this.id = id;
    }

    public SourceEntity(Long id, String name, SurfaceBrightnessProfile extendedSourceProfile, SpatialDistributionType spatialDistributionType, SpectralDistributionType spectralDistributionType) {
        this.id = id;
        this.name = name;
        this.extendedSourceProfile = extendedSourceProfile;
        this.spatialDistributionType = spatialDistributionType;
        this.spectralDistributionType = spectralDistributionType;
    }

    public SourceEntity(String name, SurfaceBrightnessProfile extendedSourceProfile, SpatialDistributionType spatialDistributionType, SpectralDistributionType spectralDistributionType) {
        this.name = name;
        this.extendedSourceProfile = extendedSourceProfile;
        this.spatialDistributionType = spatialDistributionType;
        this.spectralDistributionType = spectralDistributionType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public double getEmissionLineWavelength() {
        return emissionLineWavelength;
    }

    @Override
    public void setEmissionLineWavelength(double emissionLineWavelength) {
        if (this.emissionLineWavelength != emissionLineWavelength)
            modified = true;
        this.emissionLineWavelength = emissionLineWavelength;
    }

    @Override
    public String getEmissionLineWavelengthUnit() {
        return emissionLineWavelengthUnit;
    }

    public void setEmissionLineWavelengthUnit(String emissionLineWavelengthUnit) {
        if (this.emissionLineWavelengthUnit != null && !this.emissionLineWavelengthUnit.equals(emissionLineWavelengthUnit))
            modified = true;
        this.emissionLineWavelengthUnit = emissionLineWavelengthUnit;
    }
    
    @Override
    public double getEmissionLineFwhm() {
        return emissionLineFwhm;
    }
    
    @Override
    public void setEmissionLineFwhm(double emissionLineFwhm) {
        if (this.emissionLineFwhm != emissionLineFwhm)
            modified = true;
        this.emissionLineFwhm = emissionLineFwhm;
    }
    
    @Override
    public String getEmissionLineFwhmUnit() {
        return emissionLineFwhmUnit;
    }
    
    public void setEmissionLineFwhmUnit(String emissionLineFwhmUnit) {
        if (this.emissionLineFwhmUnit != null && !this.emissionLineFwhmUnit.equals(emissionLineFwhmUnit))
            modified = true;
        this.emissionLineFwhmUnit = emissionLineFwhmUnit;
    }

    @Override
    public SurfaceBrightnessProfile getExtendedSourceProfile() {
        return extendedSourceProfile;
    }

    @Override
    public void setExtendedSourceProfile(SurfaceBrightnessProfile extendedSourceProfile) {
        if (this.extendedSourceProfile != extendedSourceProfile)
            modified = true;
        this.extendedSourceProfile = extendedSourceProfile;
    }

    @Override
    public double getExtendedSourceRadius() {
        return extendedSourceRadius;
    }

    @Override
    public void setExtendedSourceRadius(double extendedSourceRadius) {
        if (this.extendedSourceRadius != extendedSourceRadius)
            modified = true;
        this.extendedSourceRadius = extendedSourceRadius;
    }

    @Override
    public String getExtendedSourceRadiusUnit() {
        return extendedSourceRadiusUnit;
    }

    public void setExtendedSourceRadiusUnit(String extendedSourceRadiusUnit) {
        if (this.extendedSourceRadiusUnit != null && !this.extendedSourceRadiusUnit.equals(extendedSourceRadiusUnit))
            modified = true;
        this.extendedSourceRadiusUnit = extendedSourceRadiusUnit;
    }

    @Override
    public double getMagnitude() {
        return magnitude;
    }

    @Override
    public void setMagnitude(double magnitude) {
        if (this.magnitude != magnitude)
            modified = true;
        this.magnitude = magnitude;
    }

    @Override
    public double getRedshift() {
        return redshift;
    }

    @Override
    public void setRedshift(double redshift) {
        if (this.redshift != redshift)
            modified = true;
        this.redshift = redshift;
    }
    
    @Override
    public double getTemperature(){
        return temperature;
    }
    
    @Override
    public void setTemperature(double temperature){
        if (this.temperature != temperature)
            modified = true;
        this.temperature = temperature;
    }
    
    @Override
    public String getTemperatureUnit(){
        return temperatureUnit;
    }
    
    public void setTemperatureUnit(String temperatureUnit){
        if (this.temperatureUnit != null && !this.temperatureUnit.equals(temperatureUnit))
            modified = true;
        this.temperatureUnit = temperatureUnit ;
    }

    @Override
    public SpatialDistributionType getSpatialDistributionType() {
        return spatialDistributionType;
    }

    @Override
    public void setSpatialDistributionType(SpatialDistributionType spatialDistributionType) {
        if (this.spatialDistributionType != spatialDistributionType)
            modified = true;
        this.spatialDistributionType = spatialDistributionType;
    }

    @Override
    public SpectralDistributionType getSpectralDistributionType() {
        return spectralDistributionType;
    }

    @Override
    public void setSpectralDistributionType(SpectralDistributionType spectralDistributionType) {
        if (this.spectralDistributionType != spectralDistributionType)
            modified = true;
        this.spectralDistributionType = spectralDistributionType;
    }

    public DatasetInfoEntity getSpectralDistributionTemplateEntity() {
        return spectralDistributionTemplateEntity;
    }

    public void setSpectralDistributionTemplateEntity(DatasetInfoEntity spectralDistributionTemplate) {
        this.spectralDistributionTemplateEntity = spectralDistributionTemplate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SourceEntity)) {
            return false;
        }
        SourceEntity other = (SourceEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.cnrs.lam.dis.etc.persistence.database.entities.SourceEntity[id=" + id + "]";
    }

    @Override
    public ComponentInfo getInfo() {
        return new ComponentInfo(getName(), getDescription());
    }

    @Override
    public DatasetInfo getSpectralDistributionTemplate() {
        if (getSpectralDistributionTemplateEntity() == null)
            return null;
        return new DatasetInfo(getSpectralDistributionTemplateEntity().getName(), getSpectralDistributionTemplateEntity().getNamespace(), getSpectralDistributionTemplateEntity().getDescription());
    }

    @Override
    public void setSpectralDistributionTemplate(DatasetInfo template) {
        if ((this.spectralDistributionTemplateEntity == null && template != null) ||
                (this.spectralDistributionTemplateEntity != null && template == null) ||
                (this.spectralDistributionTemplateEntity != null && template != null &&
                    !(this.spectralDistributionTemplateEntity.getName().equals(template.getName()) &&
                        (this.spectralDistributionTemplateEntity.getNamespace() == null ?
                            template.getNamespace() == null :
                            this.spectralDistributionTemplateEntity.getNamespace().equals(template.getNamespace())
                        ))
                ))
            modified = true;
        if (template == null) {
            setSpectralDistributionTemplateEntity(null);
            return;
        }
        DatasetInfoEntityJpaController controller = new DatasetInfoEntityJpaController();
        setSpectralDistributionTemplateEntity(controller.findDatasetEntity(Type.SPECTRAL_DIST_TEMPLATE, template));
    }

    @Override
    public double getEmissionLineFlux() {
        return emissionLineFlux;
    }

    @Override
    public void setEmissionLineFlux(double emissionLineFlux) {
        if (this.emissionLineFlux != emissionLineFlux)
            modified = true;
        this.emissionLineFlux = emissionLineFlux;
    }

    @Override
    public String getEmissionLineFluxUnit() {
        return emissionLineFluxUnit;
    }

    public void setEmissionLineFluxUnit(String emissionLineFluxUnit) {
        if (this.emissionLineFluxUnit != null && !this.emissionLineFluxUnit.equals(emissionLineFluxUnit))
            modified = true;
        this.emissionLineFluxUnit = emissionLineFluxUnit;
    }

    @Override
    public double getMagnitudeWavelength() {
        return magnitudeWavelength;
    }

    @Override
    public void setMagnitudeWavelength(double lambda) {
        if (this.magnitudeWavelength != lambda)
            modified = true;
        this.magnitudeWavelength = lambda;
    }

    @Override
    public String getMagnitudeWavelengthUnit() {
        return magnitudeWavelengthUnit;
    }
    
    public void setMagnitudeWavelengthUnit(String magnitudeWavelengthUnit) {
        if (this.magnitudeWavelengthUnit != null && !this.magnitudeWavelengthUnit.equals(magnitudeWavelengthUnit))
            modified = true;
        this.magnitudeWavelengthUnit = magnitudeWavelengthUnit;
    }

    @Override
    public ExtendedMagnitudeType getExtendedMagnitudeType() {
        return extendedMagnitudeType;
    }

    @Override
    public void setExtendedMagnitudeType(ExtendedMagnitudeType type) {
        if (this.extendedMagnitudeType != type)
            modified = true;
        this.extendedMagnitudeType = type;
    }

}
