/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.persistence.database;

import java.util.ArrayList;
import java.util.List;
import org.cnrs.lam.dis.etc.datamodel.ComponentInfo;
import org.cnrs.lam.dis.etc.datamodel.Dataset.Type;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;
import org.cnrs.lam.dis.etc.datamodel.Instrument;
import org.cnrs.lam.dis.etc.datamodel.ObsParam;
import org.cnrs.lam.dis.etc.datamodel.Session;
import org.cnrs.lam.dis.etc.datamodel.Site;
import org.cnrs.lam.dis.etc.datamodel.Source;
import org.cnrs.lam.dis.etc.persistence.InUseException;
import org.cnrs.lam.dis.etc.persistence.NameExistsException;
import org.cnrs.lam.dis.etc.persistence.NameNotFoundException;
import org.cnrs.lam.dis.etc.persistence.PersistenceDeviceException;
import org.cnrs.lam.dis.etc.persistence.SessionManager;
import org.cnrs.lam.dis.etc.persistence.database.entities.DatasetInfoEntity;
import org.cnrs.lam.dis.etc.persistence.database.entities.DatasetInfoEntityJpaController;
import org.cnrs.lam.dis.etc.persistence.database.entities.InstrumentEntity;
import org.cnrs.lam.dis.etc.persistence.database.entities.InstrumentEntityJpaController;
import org.cnrs.lam.dis.etc.persistence.database.entities.ObsParamEntity;
import org.cnrs.lam.dis.etc.persistence.database.entities.ObsParamEntityJpaController;
import org.cnrs.lam.dis.etc.persistence.database.entities.SiteEntity;
import org.cnrs.lam.dis.etc.persistence.database.entities.SiteEntityJpaController;
import org.cnrs.lam.dis.etc.persistence.database.entities.SourceEntity;
import org.cnrs.lam.dis.etc.persistence.database.entities.SourceEntityJpaController;
import org.cnrs.lam.dis.etc.persistence.database.entities.TransactionController;

/**
 *
 * @author Nikolaos Apostolakos
 */
public class SessionManagerImpl implements SessionManager {

    @Override
    public List<String> getAvailableSessionList() throws PersistenceDeviceException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<ComponentInfo> getAvailableInstrumentList() throws PersistenceDeviceException {
        List<ComponentInfo> result = new ArrayList<ComponentInfo>();
        InstrumentEntityJpaController instrumentController = new InstrumentEntityJpaController();
        for (InstrumentEntity entity : instrumentController.findInstrumentEntityEntities()) {
            result.add(new ComponentInfo(entity.getName(), entity.getDescription()));
        }
        return result;
    }

    @Override
    public List<ComponentInfo> getAvailableSiteList() throws PersistenceDeviceException {
        List<ComponentInfo> result = new ArrayList<ComponentInfo>();
        SiteEntityJpaController siteController = new SiteEntityJpaController();
        for (SiteEntity entity : siteController.findSiteEntityEntities()) {
            result.add(new ComponentInfo(entity.getName(), entity.getDescription()));
        }
        return result;
    }

    @Override
    public List<ComponentInfo> getAvailableSourceList() throws PersistenceDeviceException {
        List<ComponentInfo> result = new ArrayList<ComponentInfo>();
        SourceEntityJpaController sourceController = new SourceEntityJpaController();
        for (SourceEntity entity : sourceController.findSourceEntityEntities()) {
            result.add(new ComponentInfo(entity.getName(), entity.getDescription()));
        }
        return result;
    }

    @Override
    public List<ComponentInfo> getAvailableObsParamList() throws PersistenceDeviceException {
        List<ComponentInfo> result = new ArrayList<ComponentInfo>();
        ObsParamEntityJpaController obsParamController = new ObsParamEntityJpaController();
        for (ObsParamEntity entity : obsParamController.findObsParamEntityEntities()) {
            result.add(new ComponentInfo(entity.getName(), entity.getDescription()));
        }
        return result;
    }

    @Override
    public boolean isInstrumentPersisted(Instrument instrument) throws PersistenceDeviceException {
        if ((isInstrumentInDatabase(instrument) || instrument.getInfo().getName().equals("new"))
                && !((InstrumentEntity) instrument).isModified())
            return true;
        else
            return false;
    }

    @Override
    public boolean isInstrumentInDatabase(Instrument instrument) {
        if (instrument instanceof InstrumentEntity) {
            InstrumentEntity instrumentEntity = (InstrumentEntity) instrument;
            if (instrumentEntity.getId() != null)
                return true;
        }
        return false;
    }

    @Override
    public boolean isSourcePersisted(Source source) throws PersistenceDeviceException {
        if ((isSourceInDatabase(source) || source.getInfo().getName().equals("new"))
                && !((SourceEntity) source).isModified())
            return true;
        else
            return false;
    }

    @Override
    public boolean isSourceInDatabase(Source source) {
        if (source instanceof SourceEntity) {
            SourceEntity sourceEntity = (SourceEntity) source;
            if (sourceEntity.getId() != null)
                return true;
        }
        return false;
    }

    @Override
    public boolean isSitePersisted(Site site) throws PersistenceDeviceException {
        if ((isSiteInDatabase(site) || site.getInfo().getName().equals("new"))
                && !((SiteEntity) site).isModified())
            return true;
        else
            return false;
    }

    @Override
    public boolean isSiteInDatabase(Site site) {
        if (site instanceof SiteEntity) {
            SiteEntity siteEntity = (SiteEntity) site;
            if (siteEntity.getId() != null)
                return true;
        }
        return false;
    }

    @Override
    public boolean isObsParamPersisted(ObsParam obsParam) throws PersistenceDeviceException {
        if ((isObsParamInDatabase(obsParam) || obsParam.getInfo().getName().equals("new"))
                && !((ObsParamEntity) obsParam).isModified())
            return true;
        else
            return false;
    }

    @Override
    public boolean isObsParamInDatabase(ObsParam obsParam) {
        if (obsParam instanceof ObsParamEntity) {
            ObsParamEntity obsParamEntity = (ObsParamEntity) obsParam;
            if (obsParamEntity.getId() != null)
                return true;
        }
        return false;
    }

    @Override
    public Session loadSession(String name) throws NameNotFoundException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Instrument loadInstrument(ComponentInfo instrumentInfo) throws NameNotFoundException, PersistenceDeviceException {
        InstrumentEntityJpaController instrumentController = new InstrumentEntityJpaController();
        Instrument instrument = instrumentController.findInstrumentEntity(instrumentInfo);
        if (instrument == null) {
            throw new NameNotFoundException();
        }
        TransactionController.getEntityManager().refresh(instrument);
        return instrument;
    }

    @Override
    public Site loadSite(ComponentInfo siteInfo) throws NameNotFoundException, PersistenceDeviceException {
        SiteEntityJpaController siteController = new SiteEntityJpaController();
        Site site = siteController.findSiteEntity(siteInfo);
        if (site == null) {
            throw new NameNotFoundException();
        }
        TransactionController.getEntityManager().refresh(site);
        return site;
    }

    @Override
    public Source loadSource(ComponentInfo sourceInfo) throws NameNotFoundException, PersistenceDeviceException {
        SourceEntityJpaController sourceController = new SourceEntityJpaController();
        Source source = sourceController.findSourceEntity(sourceInfo);
        if (source == null) {
            throw new NameNotFoundException();
        }
        TransactionController.getEntityManager().refresh(source);
        return source;
    }

    @Override
    public ObsParam loadObsParam(ComponentInfo obsParamInfo) throws NameNotFoundException, PersistenceDeviceException {
        ObsParamEntityJpaController obsParamController = new ObsParamEntityJpaController();
        ObsParam obsParam = obsParamController.findObsParamEntity(obsParamInfo);
        if (obsParam == null) {
            throw new NameNotFoundException();
        }
        TransactionController.getEntityManager().refresh(obsParam);
        return obsParam;
    }

    @Override
    public boolean saveSession(Session session) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void saveInstrument(Instrument instrument) throws PersistenceDeviceException, NameExistsException {
        InstrumentEntity entity = (InstrumentEntity) instrument;
        InstrumentEntityJpaController instrumentController = new InstrumentEntityJpaController();
        if (entity.getId() == null) {
            if (instrumentController.findInstrumentEntity(entity.getInfo()) != null) {
                throw new NameExistsException();
            }
            instrumentController.create(entity);
        } else {
            try {
                instrumentController.edit(entity);
            } catch (Exception ex) {
                throw new PersistenceDeviceException(ex);
            }
        }
    }

    @Override
    public void saveSite(Site site) throws PersistenceDeviceException, NameExistsException {
        SiteEntity entity = (SiteEntity) site;
        SiteEntityJpaController siteController = new SiteEntityJpaController();
        if (entity.getId() == null) {
            if (siteController.findSiteEntity(entity.getInfo()) != null) {
                throw new NameExistsException();
            }
            siteController.create(entity);
        } else {
            try {
                siteController.edit(entity);
            } catch (Exception ex) {
                throw new PersistenceDeviceException(ex);
            }
        }
    }

    @Override
    public void saveSource(Source source) throws PersistenceDeviceException, NameExistsException {
        SourceEntity entity = (SourceEntity) source;
        SourceEntityJpaController sourceController = new SourceEntityJpaController();
        if (entity.getId() == null) {
            if (sourceController.findSourceEntity(entity.getInfo()) != null) {
                throw new NameExistsException();
            }
            sourceController.create(entity);
        } else {
            try {
                sourceController.edit(entity);
            } catch (Exception ex) {
                throw new PersistenceDeviceException(ex);
            }
        }
    }

    @Override
    public void saveObsParam(ObsParam obsParam) throws PersistenceDeviceException, NameExistsException {
        ObsParamEntity entity = (ObsParamEntity) obsParam;
        ObsParamEntityJpaController obsParamController = new ObsParamEntityJpaController();
        if (entity.getId() == null) {
            if (obsParamController.findObsParamEntity(entity.getInfo()) != null) {
                throw new NameExistsException();
            }
            obsParamController.create(entity);
        } else {
            try {
                obsParamController.edit(entity);
            } catch (Exception ex) {
                throw new PersistenceDeviceException(ex);
            }
        }
    }

    @Override
    public void saveSessionAs(Session session, boolean override) throws NameExistsException, PersistenceDeviceException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Instrument saveInstrumentAs(Instrument instrument, ComponentInfo newInfo) throws NameExistsException, PersistenceDeviceException {
        try {
            TransactionController.beginTransaction();
            InstrumentEntity newInstrument = (InstrumentEntity) createNewInstrument(newInfo);
            newInstrument.setDark(instrument.getDark());
            newInstrument.setDarkUnit(instrument.getDarkUnit());
            newInstrument.setDiameter(instrument.getDiameter());
            newInstrument.setDiameterUnit(instrument.getDiameterUnit());
            newInstrument.setFiberDiameter(instrument.getFiberDiameter());
            newInstrument.setFiberDiameterUnit(instrument.getFiberDiameterUnit());
            if (instrument.getFilterTransmission() == null || instrument.getFilterTransmission().getNamespace() == null) {
                newInstrument.setFilterTransmission(instrument.getFilterTransmission());
            } else {
                DatasetInfo datasetInfo = instrument.getFilterTransmission();
                if (!newInfo.getName().equals(datasetInfo.getNamespace()))
                    datasetInfo = copyDatasetInNamespace(Type.FILTER_TRANSMISSION, instrument.getFilterTransmission(), newInfo.getName());
                newInstrument.setFilterTransmission(datasetInfo);
            }
            if (instrument.getFwhm() == null || instrument.getFwhm().getNamespace() == null) {
                newInstrument.setFwhm(instrument.getFwhm());
            } else {
                DatasetInfo datasetInfo = instrument.getFwhm();
                if (!newInfo.getName().equals(datasetInfo.getNamespace()))
                    datasetInfo = copyDatasetInNamespace(Type.FWHM, instrument.getFwhm(), newInfo.getName());
                newInstrument.setFwhm(datasetInfo);
            }
            newInstrument.setInstrumentType(instrument.getInstrumentType());
            newInstrument.setNSlit(instrument.getNSlit());
            newInstrument.setObstruction(instrument.getObstruction());
            newInstrument.setPixelScale(instrument.getPixelScale());
            newInstrument.setPixelScaleUnit(instrument.getPixelScaleUnit());
            newInstrument.setPsfType(instrument.getPsfType());
            newInstrument.setRangeMax(instrument.getRangeMax());
            newInstrument.setRangeMaxUnit(instrument.getRangeMaxUnit());
            newInstrument.setRangeMin(instrument.getRangeMin());
            newInstrument.setRangeMinUnit(instrument.getRangeMinUnit());
            newInstrument.setReadout(instrument.getReadout());
            newInstrument.setReadoutUnit(instrument.getReadoutUnit());
            newInstrument.setSlitLength(instrument.getSlitLength());
            newInstrument.setSlitLengthUnit(instrument.getSlitLengthUnit());
            newInstrument.setSlitWidth(instrument.getSlitWidth());
            newInstrument.setSlitWidthUnit(instrument.getSlitWidthUnit());
            newInstrument.setStrehlRatio(instrument.getStrehlRatio());
            newInstrument.setRefWavelength(instrument.getRefWavelength());
            newInstrument.setRefWavelengthUnit(instrument.getRefWavelengthUnit());
            if (instrument.getSpectralResolution() == null || instrument.getSpectralResolution().getNamespace() == null) {
                newInstrument.setSpectralResolution(instrument.getSpectralResolution());
            } else {
                DatasetInfo datasetInfo = instrument.getSpectralResolution();
                if (!newInfo.getName().equals(datasetInfo.getNamespace()))
                    datasetInfo = copyDatasetInNamespace(Type.SPECTRAL_RESOLUTION, instrument.getSpectralResolution(), newInfo.getName());
                newInstrument.setSpectralResolution(datasetInfo);
            }
            newInstrument.setSpectrographType(instrument.getSpectrographType());
            if (instrument.getTransmission() == null || instrument.getTransmission().getNamespace() == null) {
                newInstrument.setTransmission(instrument.getTransmission());
            } else {
                DatasetInfo datasetInfo = instrument.getTransmission();
                if (!newInfo.getName().equals(datasetInfo.getNamespace()))
                    datasetInfo = copyDatasetInNamespace(Type.TRANSMISSION, instrument.getTransmission(), newInfo.getName());
                newInstrument.setTransmission(datasetInfo);
            }
            newInstrument.setDeltaLambdaPerPixel(instrument.getDeltaLambdaPerPixel());
            newInstrument.setDeltaLambdaPerPixelUnit(instrument.getDeltaLambdaPerPixelUnit());
            newInstrument.setSpectralResolutionType(instrument.getSpectralResolutionType());
            newInstrument.setPsfDoubleGaussianMultiplier(instrument.getPsfDoubleGaussianMultiplier());
            if (instrument.getPsfDoubleGaussianFwhm1() == null || instrument.getPsfDoubleGaussianFwhm1().getNamespace() == null) {
                newInstrument.setPsfDoubleGaussianFwhm1(instrument.getPsfDoubleGaussianFwhm1());
            } else {
                DatasetInfo datasetInfo = instrument.getPsfDoubleGaussianFwhm1();
                if (!newInfo.getName().equals(datasetInfo.getNamespace()))
                    datasetInfo = copyDatasetInNamespace(Type.PSF_DOUBLE_GAUSSIAN_FWHM_1, instrument.getPsfDoubleGaussianFwhm1(), newInfo.getName());
                newInstrument.setPsfDoubleGaussianFwhm1(datasetInfo);
            }
            if (instrument.getPsfDoubleGaussianFwhm2() == null || instrument.getPsfDoubleGaussianFwhm2().getNamespace() == null) {
                newInstrument.setPsfDoubleGaussianFwhm2(instrument.getPsfDoubleGaussianFwhm2());
            } else {
                DatasetInfo datasetInfo = instrument.getPsfDoubleGaussianFwhm2();
                if (!newInfo.getName().equals(datasetInfo.getNamespace()))
                    datasetInfo = copyDatasetInNamespace(Type.PSF_DOUBLE_GAUSSIAN_FWHM_2, instrument.getPsfDoubleGaussianFwhm2(), newInfo.getName());
                newInstrument.setPsfDoubleGaussianFwhm2(datasetInfo);
            }
            newInstrument.setPsfSizeType(instrument.getPsfSizeType());
            newInstrument.setFixedPsfSize(instrument.getFixedPsfSize());
            newInstrument.setFixedPsfSizeUnit(instrument.getFixedPsfSizeUnit());
            if (instrument.getPsfSizeFunction()== null || instrument.getPsfSizeFunction().getNamespace() == null) {
                newInstrument.setPsfSizeFunction(instrument.getPsfSizeFunction());
            } else {
                DatasetInfo datasetInfo = instrument.getPsfSizeFunction();
                if (!newInfo.getName().equals(datasetInfo.getNamespace()))
                    datasetInfo = copyDatasetInNamespace(Type.PSF_SIZE_FUNCTION, instrument.getPsfSizeFunction(), newInfo.getName());
                newInstrument.setPsfSizeFunction(datasetInfo);
            }
            newInstrument.setPsfSizePercentage(instrument.getPsfSizePercentage());
            
            InstrumentEntityJpaController instrumentController = new InstrumentEntityJpaController();
            instrumentController.create(newInstrument);
            TransactionController.getEntityManager().flush();
            TransactionController.commitTransaction();
            return newInstrument;
        } catch (Exception ex) {
            TransactionController.rollbackTransaction();
            if (ex instanceof NameExistsException)
                throw (NameExistsException) ex;
            else if (ex instanceof PersistenceDeviceException)
                throw (PersistenceDeviceException) ex;
            else
                throw new PersistenceDeviceException(ex);
        }
    }

    @Override
    public Site saveSiteAs(Site site, ComponentInfo newInfo) throws NameExistsException, PersistenceDeviceException {
        try {
            TransactionController.beginTransaction();
            SiteEntity newSite = (SiteEntity) createNewSite(newInfo);
            newSite.setAirMass(site.getAirMass());
            newSite.setGalacticContributing(site.isGalacticContributing());
            newSite.setSeeingLimited(site.getSeeingLimited());
            if (site.getGalacticProfile() == null || site.getGalacticProfile().getNamespace() == null) {
                newSite.setGalacticProfile(site.getGalacticProfile());
            } else {
                DatasetInfo datasetInfo = site.getGalacticProfile();
                if (!newInfo.getName().equals(datasetInfo.getNamespace()))
                    datasetInfo = copyDatasetInNamespace(Type.GALACTIC, site.getGalacticProfile(), newInfo.getName());
                newSite.setGalacticProfile(datasetInfo);
            }
            newSite.setLocationType(site.getLocationType());
            newSite.setSeeing(site.getSeeing());
            newSite.setSeeingUnit(site.getSeeingUnit());
            if (site.getSkyAbsorption() == null || site.getSkyAbsorption().getNamespace() == null) {
                newSite.setSkyAbsorption(site.getSkyAbsorption());
            } else {
                DatasetInfo datasetInfo = site.getSkyAbsorption();
                if (!newInfo.getName().equals(datasetInfo.getNamespace()))
                    datasetInfo = copyDatasetInNamespace(Type.SKY_ABSORPTION, site.getSkyAbsorption(), newInfo.getName());
                newSite.setSkyAbsorption(datasetInfo);
            }
            if (site.getSkyEmission() == null || site.getSkyEmission().getNamespace() == null) {
                newSite.setSkyEmission(site.getSkyEmission());
            } else {
                DatasetInfo datasetInfo = site.getSkyEmission();
                if (!newInfo.getName().equals(datasetInfo.getNamespace()))
                    datasetInfo = copyDatasetInNamespace(Type.SKY_EMISSION, site.getSkyEmission(), newInfo.getName());
                newSite.setSkyEmission(datasetInfo);
            }
            newSite.setSkyEmissionSelectedOption(site.getSkyEmissionSelectedOption());
            if (site.getSkyExtinction() == null || site.getSkyExtinction().getNamespace() == null) {
                newSite.setSkyExtinction(site.getSkyExtinction());
            } else {
                DatasetInfo datasetInfo = site.getSkyExtinction();
                if (!newInfo.getName().equals(datasetInfo.getNamespace()))
                    datasetInfo = copyDatasetInNamespace(Type.SKY_EXTINCTION, site.getSkyExtinction(), newInfo.getName());
                newSite.setSkyExtinction(datasetInfo);
            }
            newSite.setZodiacalContributing(site.isZodiacalContributing());
            if (site.getZodiacalProfile() == null || site.getZodiacalProfile().getNamespace() == null) {
                newSite.setZodiacalProfile(site.getZodiacalProfile());
            } else {
                DatasetInfo datasetInfo = site.getZodiacalProfile();
                if (!newInfo.getName().equals(datasetInfo.getNamespace()))
                    datasetInfo = copyDatasetInNamespace(Type.ZODIACAL, site.getZodiacalProfile(), newInfo.getName());
                newSite.setZodiacalProfile(datasetInfo);
            }
            if (site.getAtmosphericTransmission()== null || site.getAtmosphericTransmission().getNamespace() == null) {
                newSite.setAtmosphericTransmission(site.getAtmosphericTransmission());
            } else {
                DatasetInfo datasetInfo = site.getAtmosphericTransmission();
                if (!newInfo.getName().equals(datasetInfo.getNamespace()))
                    datasetInfo = copyDatasetInNamespace(Type.ATMOSPHERIC_TRANSMISSION, site.getAtmosphericTransmission(), newInfo.getName());
                newSite.setAtmosphericTransmission(datasetInfo);
            }
            newSite.setAtmosphericTransmissionType(site.getAtmosphericTransmissionType());
            SiteEntityJpaController siteController = new SiteEntityJpaController();
            siteController.create(newSite);
            TransactionController.getEntityManager().flush();
            TransactionController.commitTransaction();
            return newSite;
        } catch (Exception ex) {
            TransactionController.rollbackTransaction();
            if (ex instanceof NameExistsException)
                throw (NameExistsException) ex;
            else if (ex instanceof PersistenceDeviceException)
                throw (PersistenceDeviceException) ex;
            else
                throw new PersistenceDeviceException(ex);
        }
    }

    @Override
    public Source saveSourceAs(Source source, ComponentInfo newInfo) throws NameExistsException, PersistenceDeviceException {
        try {
            TransactionController.beginTransaction();
            SourceEntity newSource = (SourceEntity) createNewSource(newInfo);
            newSource.setMagnitudeWavelength(source.getMagnitudeWavelength());
            newSource.setMagnitudeWavelengthUnit(source.getMagnitudeWavelengthUnit());
            newSource.setEmissionLineWavelength(source.getEmissionLineWavelength());
            newSource.setEmissionLineWavelengthUnit(source.getEmissionLineWavelengthUnit());
            newSource.setEmissionLineFwhm(source.getEmissionLineFwhm());
            newSource.setEmissionLineFwhmUnit(source.getEmissionLineFwhmUnit());
            newSource.setExtendedSourceProfile(source.getExtendedSourceProfile());
            newSource.setExtendedSourceRadius(source.getExtendedSourceRadius());
            newSource.setExtendedSourceRadiusUnit(source.getExtendedSourceRadiusUnit());
            newSource.setMagnitude(source.getMagnitude());
            newSource.setRedshift(source.getRedshift());
            newSource.setTemperature(source.getTemperature());
            newSource.setTemperatureUnit(source.getTemperatureUnit());
            newSource.setSpatialDistributionType(source.getSpatialDistributionType());
            if (source.getSpectralDistributionTemplate() == null || source.getSpectralDistributionTemplate().getNamespace() == null) {
                newSource.setSpectralDistributionTemplate(source.getSpectralDistributionTemplate());
            } else {
                DatasetInfo datasetInfo = source.getSpectralDistributionTemplate();
                if (!newInfo.getName().equals(datasetInfo.getNamespace()))
                    datasetInfo = copyDatasetInNamespace(Type.SPECTRAL_DIST_TEMPLATE, source.getSpectralDistributionTemplate(), newInfo.getName());
                newSource.setSpectralDistributionTemplate(datasetInfo);
            }
            newSource.setSpectralDistributionType(source.getSpectralDistributionType());
            newSource.setEmissionLineFlux(source.getEmissionLineFlux());
            newSource.setEmissionLineFluxUnit(source.getEmissionLineFluxUnit());
            newSource.setExtendedMagnitudeType(source.getExtendedMagnitudeType());
            SourceEntityJpaController sourceController = new SourceEntityJpaController();
            sourceController.create(newSource);
            TransactionController.getEntityManager().flush();
            TransactionController.commitTransaction();
            return newSource;
        } catch (Exception ex) {
            TransactionController.rollbackTransaction();
            if (ex instanceof NameExistsException)
                throw (NameExistsException) ex;
            else if (ex instanceof PersistenceDeviceException)
                throw (PersistenceDeviceException) ex;
            else
                throw new PersistenceDeviceException(ex);
        }
    }

    @Override
    public ObsParam saveObsParamAs(ObsParam obsParam, ComponentInfo newInfo) throws NameExistsException, PersistenceDeviceException {
        try {
            TransactionController.beginTransaction();
            ObsParamEntity newObsParam = (ObsParamEntity) createNewObsParam(newInfo);
            newObsParam.setDit(obsParam.getDit());
            newObsParam.setDitUnit(obsParam.getDitUnit());
            newObsParam.setExposureTime(obsParam.getExposureTime());
            newObsParam.setExposureTimeUnit(obsParam.getExposureTimeUnit());
            newObsParam.setFixedParameter(obsParam.getFixedParameter());
            newObsParam.setNoExpo(obsParam.getNoExpo());
            newObsParam.setSnr(obsParam.getSnr());
            newObsParam.setSnrLambda(obsParam.getSnrLambda());
            newObsParam.setSnrLambdaUnit(obsParam.getSnrLambdaUnit());
            newObsParam.setSnrUnit(obsParam.getSnrUnit());
            newObsParam.setTimeSampleType(obsParam.getTimeSampleType());
            newObsParam.setSpectralQuantumType(obsParam.getSpectralQuantumType());
            newObsParam.setExtraBackgroundNoiseType(obsParam.getExtraBackgroundNoiseType());
            newObsParam.setExtraSignalType(obsParam.getExtraSignalType());
            if (obsParam.getExtraBackgroundNoiseDataset() == null || obsParam.getExtraBackgroundNoiseDataset().getNamespace() == null) {
                newObsParam.setExtraBackgroundNoiseDataset(obsParam.getExtraBackgroundNoiseDataset());
            } else {
                DatasetInfo datasetInfo = obsParam.getExtraBackgroundNoiseDataset();
                if (!newInfo.getName().equals(datasetInfo.getNamespace()))
                    datasetInfo = copyDatasetInNamespace(Type.EXTRA_BACKGROUND_NOISE, obsParam.getExtraBackgroundNoiseDataset(), newInfo.getName());
                newObsParam.setExtraBackgroundNoiseDataset(datasetInfo);
            }
            if (obsParam.getExtraSignalDataset() == null || obsParam.getExtraSignalDataset().getNamespace() == null) {
                newObsParam.setExtraSignalDataset(obsParam.getExtraSignalDataset());
            } else {
                DatasetInfo datasetInfo = obsParam.getExtraSignalDataset();
                if (!newInfo.getName().equals(datasetInfo.getNamespace()))
                    datasetInfo = copyDatasetInNamespace(Type.EXTRA_SIGNAL, obsParam.getExtraSignalDataset(), newInfo.getName());
                newObsParam.setExtraSignalDataset(datasetInfo);
            }
            newObsParam.setExtraBackgroundNoise(obsParam.getExtraBackgroundNoise());
            newObsParam.setExtraBackgroundNoiseUnit(obsParam.getExtraBackgroundNoiseUnit());
            newObsParam.setExtraSignal(obsParam.getExtraSignal());
            newObsParam.setExtraSignalUnit(obsParam.getExtraSignalUnit());
            newObsParam.setFixedSnrType(obsParam.getFixedSnrType());
            ObsParamEntityJpaController obsParamController = new ObsParamEntityJpaController();
            obsParamController.create(newObsParam);
            TransactionController.getEntityManager().flush();
            TransactionController.commitTransaction();
            return newObsParam;
        } catch (Exception ex) {
            TransactionController.rollbackTransaction();
            if (ex instanceof NameExistsException)
                throw (NameExistsException) ex;
            else if (ex instanceof PersistenceDeviceException)
                throw (PersistenceDeviceException) ex;
            else
                throw new PersistenceDeviceException(ex);
        }
    }

    @Override
    public Session createNewSession(String name) throws NameExistsException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Instrument createNewInstrument(ComponentInfo newInstrumentInfo) throws NameExistsException, PersistenceDeviceException {
        InstrumentEntityJpaController instrumentController = new InstrumentEntityJpaController();
        Instrument dbInstrument = instrumentController.findInstrumentEntity(newInstrumentInfo);
        if (dbInstrument != null) {
            throw new NameExistsException();
        }
        return instrumentController.createNewUnpersistedInstrumentEntity(newInstrumentInfo);
    }

    @Override
    public Site createNewSite(ComponentInfo newSiteInfo) throws NameExistsException, PersistenceDeviceException {
        SiteEntityJpaController siteController = new SiteEntityJpaController();
        Site dbSite = siteController.findSiteEntity(newSiteInfo);
        if (dbSite != null) {
            throw new NameExistsException();
        }
        return siteController.createNewUnpersistedSiteEntity(newSiteInfo);
    }

    @Override
    public Source createNewSource(ComponentInfo newSourceInfo) throws NameExistsException, PersistenceDeviceException {
        SourceEntityJpaController sourceController = new SourceEntityJpaController();
        Source dbSource = sourceController.findSourceEntity(newSourceInfo);
        if (dbSource != null) {
            throw new NameExistsException();
        }
        return sourceController.createNewUnpersistedSourceEntity(newSourceInfo);
    }

    @Override
    public ObsParam createNewObsParam(ComponentInfo newObsParamInfo) throws NameExistsException, PersistenceDeviceException {
        ObsParamEntityJpaController obsParamController = new ObsParamEntityJpaController();
        ObsParam dbObsParam = obsParamController.findObsParamEntity(newObsParamInfo);
        if (dbObsParam != null) {
            throw new NameExistsException();
        }
        return obsParamController.createNewUnpersistedObsParamEntity(newObsParamInfo);
    }

    @Override
    public boolean deleteSession(String name) throws NameNotFoundException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteInstrument(ComponentInfo instrumentInfo) throws NameNotFoundException, InUseException, PersistenceDeviceException {
        InstrumentEntityJpaController instrumentController = new InstrumentEntityJpaController();
        InstrumentEntity instrument = instrumentController.findInstrumentEntity(instrumentInfo);
        if (instrument == null) {
            throw new NameNotFoundException();
        }
        try {
            TransactionController.beginTransaction();
            instrumentController.destroy(instrument.getId());
            DatasetInfoEntityJpaController datasetController = new DatasetInfoEntityJpaController();
            for (DatasetInfoEntity datasetInfo : datasetController.findInstrumentDatasetsInNamespace(instrumentInfo.getName())) {
                datasetController.deleteDatasetInfo(datasetInfo);
            }
            TransactionController.getEntityManager().flush();
            TransactionController.commitTransaction();
        } catch (Exception ex) {
            TransactionController.rollbackTransaction();
            throw new PersistenceDeviceException(ex);
        }
    }

    @Override
    public void deleteSite(ComponentInfo siteInfo) throws NameNotFoundException, InUseException, PersistenceDeviceException {
        SiteEntityJpaController siteController = new SiteEntityJpaController();
        SiteEntity site = siteController.findSiteEntity(siteInfo);
        if (site == null) {
            throw new NameNotFoundException();
        }
        try {
            TransactionController.beginTransaction();
            siteController.destroy(site.getId());
            DatasetInfoEntityJpaController datasetController = new DatasetInfoEntityJpaController();
            for (DatasetInfoEntity datasetInfo : datasetController.findSiteDatasetsInNamespace(siteInfo.getName())) {
                datasetController.deleteDatasetInfo(datasetInfo);
            }
            TransactionController.getEntityManager().flush();
            TransactionController.commitTransaction();
        } catch (Exception ex) {
            TransactionController.rollbackTransaction();
            throw new PersistenceDeviceException(ex);
        }
    }

    @Override
    public void deleteSource(ComponentInfo sourceInfo) throws NameNotFoundException, InUseException, PersistenceDeviceException {
        SourceEntityJpaController sourceController = new SourceEntityJpaController();
        SourceEntity source = sourceController.findSourceEntity(sourceInfo);
        if (source == null) {
            throw new NameNotFoundException();
        }
        try {
            TransactionController.beginTransaction();
            sourceController.destroy(source.getId());
            DatasetInfoEntityJpaController datasetController = new DatasetInfoEntityJpaController();
            for (DatasetInfoEntity datasetInfo : datasetController.findSourceDatasetsInNamespace(sourceInfo.getName())) {
                datasetController.deleteDatasetInfo(datasetInfo);
            }
            TransactionController.getEntityManager().flush();
            TransactionController.commitTransaction();
        } catch (Exception ex) {
            TransactionController.rollbackTransaction();
            throw new PersistenceDeviceException(ex);
        }
    }

    @Override
    public void deleteObsParam(ComponentInfo obsParamInfo) throws NameNotFoundException, InUseException, PersistenceDeviceException {
        ObsParamEntityJpaController obsParamController = new ObsParamEntityJpaController();
        ObsParamEntity obsParam = obsParamController.findObsParamEntity(obsParamInfo);
        if (obsParam == null) {
            throw new NameNotFoundException();
        }
        try {
            TransactionController.beginTransaction();
            obsParamController.destroy(obsParam.getId());
            DatasetInfoEntityJpaController datasetController = new DatasetInfoEntityJpaController();
            for (DatasetInfoEntity datasetInfo : datasetController.findObsParamDatasetsInNamespace(obsParamInfo.getName())) {
                datasetController.deleteDatasetInfo(datasetInfo);
            }
            TransactionController.getEntityManager().flush();
            TransactionController.commitTransaction();
        } catch (Exception ex) {
            TransactionController.rollbackTransaction();
            throw new PersistenceDeviceException(ex);
        }
    }

    private DatasetInfo copyDatasetInNamespace(Type type, DatasetInfo datasetInfo, String namespace) {
        DatasetInfoEntityJpaController datasetInfoController = new DatasetInfoEntityJpaController();
        DatasetInfoEntity oldEntity = datasetInfoController.findDatasetEntity(type, datasetInfo);
        DatasetInfoEntity newEntity = new DatasetInfoEntity();
        newEntity.setDatasetMap(oldEntity.getDatasetMap());
        newEntity.setDescription(oldEntity.getDescription());
        newEntity.setName(oldEntity.getName());
        newEntity.setNamespace(namespace);
        newEntity.setType(type);
        newEntity.setXUnit(oldEntity.getXUnit());
        newEntity.setYUnit(oldEntity.getYUnit());
        datasetInfoController.create(newEntity);
        TransactionController.getEntityManager().flush();
        return new DatasetInfo(newEntity.getName(), newEntity.getNamespace(), newEntity.getDescription(), datasetInfo.getOptionList());
    }

    @Override
    public boolean checkConnection() {
        try {
            TransactionController.getEntityManager();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}
