/*
 * =============================================================================
 * 
 *   Copyright (c) 2013, CeSAM / LAM / Pytheas
 * 
 *   This software is governed by the CeCILL  license under French law and
 *   abiding by the rules of distribution of free software.  You can  use, 
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info". 
 *   
 *   As a counterpart to the access to the source code and  rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty  and the software's author,  the holder of the
 *   economic rights,  and the successive licensors  have only  limited
 *   liability. 
 *   
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading,  using,  modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean  that it is complicated to manipulate,  and  that  also
 *   therefore means  that it is reserved for developers  and  experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or 
 *   data to be ensured and,  more generally, to use and operate it in the 
 *   same conditions as regards security. 
 *   
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 * 
 * =============================================================================
 */
package org.cnrs.lam.dis.etc.persistence;

import java.util.List;
import java.util.Map;
import org.cnrs.lam.dis.etc.datamodel.Dataset;
import org.cnrs.lam.dis.etc.datamodel.DatasetInfo;

/**
 *
 * @author Nikolaos Apostolakos
 */
public interface DatasetManager {

    /**
     * Retrieves a list of the available datasets for the specified type, for the
     * given namespace. If there are no available datasets it returns an empty list.
     * The result contains the datasets with the specific namespace and also the
     * generic datasets with namespace null. If the parameter namespace is null
     * only the generic datasets are returned. If the parameter namespace is the
     * empty string ("") then all the datasets of this type are returned. Note that
     * the returned DatasetInfos can represent a single or multi dataset.
     * @param type The type of datasets to search for
     * @return A list of the available datasets
     * @throws PersistenceDeviceException If there was an error while retrieving the datasets
     */
    public List<DatasetInfo> getAvailableDatasetList(Dataset.Type type, String namespace)
            throws PersistenceDeviceException;
    
    /**
     * This method should return a list with the different options available for the
     * given dataset info. If the dataset info represents a single dataset, it returns null.
     * @param type The type of the dataset
     * @param datasetInfo The name and namespace of the dataset
     * @return A list with the dataset options for multi-dataset or null for single
     * datasets
     */
    public List<String> getDatasetOptionList(Dataset.Type type, DatasetInfo datasetInfo)
            throws NameNotFoundException, PersistenceDeviceException;

    /**
     * Retrieves the specified dataset. The first argument is the type of the dataset,
     * the second is the name and namespace of the dataset and the third one is
     * the option for the case of multi-dataset or null if the requested dataset
     * is a single dataset. If the specific dataset does not exist an exception
     * is thrown. The same exception is thrown if the given datasetInfo represents
     * a multi dataset but the option is null.
     * @param type The type of the dataset
     * @param datasetInfo The name and namespace of the dataset
     * @param option The option of a multi-dataset or null for single datasets
     * @return An object representing the dataset
     * @throws NameNotFoundException If there is no such dataset in the persistence device
     * @throws PersistenceDeviceException If there was an error while retrieving the dataset
     */
    public Dataset loadDataset(Dataset.Type type, DatasetInfo datasetInfo, String option)
            throws NameNotFoundException, PersistenceDeviceException;

    /**
     * Persist the given dataset to the persistence device. If the dataset
     * namespace is null then the dataset is stored as a generally available
     * dataset. If it is not null, then the dataset is stored as related to
     * the specific component. Note that the option field of the parameter defines
     * if the dataset is a part of a multi-dataset or if it is a single dataset.
     * In the second case it should be null.
     * @param dataset The dataset to save
     * @throws NameExistsException If there is another dataset with the same
     * name (and option for multi datasets) in the same namespace
     * @throws PersistenceDeviceException If there is an error while storing the dataset.
     */
    public void saveDataset(Dataset dataset) throws NameExistsException,
            PersistenceDeviceException;
    
    /**
     * Persists the given multi-dataset to the persistence device. If the namespace
     * of the info is null then the dataset is stored as a generally available
     * dataset. Otherwise it is stored as related to the specific component.The
     * keys of the optionList parameter declare the different options of the
     * multi-dataset and its values are contain the data of the datasets. Note
     * that null and empty string are not accepted as options.
     * @param type The type of dataset to save
     * @param info The info of the multi-dataset to save
     * @param xUnit The unit of the X axis of the dataset
     * @param yUnit The unit of the Y axis of the dataset
     * @param optionList The data of the multi-dataset
     * @param dataType the type of the data of the dataset
     * @throws NameExistsException If there is another dataset with the same name
     * in the same namespace
     * @throws PersistenceDeviceException If there is an error while storing the
     * dataset
     */
    public void saveMultiDataset(Dataset.Type type, DatasetInfo info, String xUnit,
            String yUnit, Map<String, Map<Double, Double>> optionList, Dataset.DataType dataType)
            throws NameExistsException, PersistenceDeviceException;

    /**
     * Deletes from the persistence device the specified dataset. If the dataset
     * is used by any component it throws an exception. If there is no dataset
     * with such name for the given namespace it throws an exception. Note that
     * if the datasetInfo represents a multi-dataset all its options will be deleted.
     * @param type The type of the dataset
     * @param datasetInfo The name and namespace of the dataset
     * @throws NameNotFoundException If there is no dataset stored with such a
     * name in the specified namespace
     * @throws InUseException If there is any component currently using the dataset
     * @throws PersistenceDeviceException If there is any error while removing
     * the dataset from the persistence device.
     */
    public void deleteDataset(Dataset.Type type, DatasetInfo datasetInfo)
            throws NameNotFoundException, InUseException, PersistenceDeviceException;
    
    /**
     * This method copies a dataset to a different namespace.
     * @param type The type of the dataset to copy
     * @param datasetInfo The namespace and name of the dataset
     * @param namespace The new namespace to copy the dataset to
     * @throws NameNotFoundException If there is no dataset stored with such a
     * name in the specified namespace
     * @throws NameExistsException If there is another dataset with the same name
     * in the new namespace
     * @throws PersistenceDeviceException If there is any error while copying
     * the dataset
     */
    public void copyToNamespace(Dataset.Type type, DatasetInfo datasetInfo, DatasetInfo newInfo)
            throws NameNotFoundException, NameExistsException, PersistenceDeviceException;
}
